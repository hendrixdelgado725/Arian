#!/bin/sh

if   pgrep -f sidekiq > /dev/null
then
    kill -9 $(pgrep -f sidekiq)
    echo "Sidekiq has been killed"
    sudo chmod 777 /dev/ttyUSB0
    cd ~/cola_impresion
    sidekiq -r ./worker.rb &
else
    echo "Sidekiq no se ha encontrado"
    sudo chmod 777 /dev/ttyUSB0
    cd ~/cola_impresion
    sidekiq -r ./worker.rb &
fi

echo "Executed succefully"
