<?php
Route::group(['prefix' => '/modulo/Fasvit'], function(){

  Route::get('/Inicio',                        'Fasvit\InformacionFamiliarController@inicio')->name('fasvit.inicio');

  Route::get('/Familiares',                    'Fasvit\InformacionFamiliarController@index')->name('fasvit.familiares');
  Route::get('/Registro/Familiares',           'Fasvit\InformacionFamiliarController@create')->name('fasvit.registro');
  Route::post('/Registro/Familiares/store',    'Fasvit\InformacionFamiliarController@store')->name('fasvit.store');
  Route::get('/Registro/Familiares/edit/{id}', 'Fasvit\InformacionFamiliarController@edit')->name('fasvit.edit');

//  Route::post('/getEmpleado',                  'Fasvit\InformacionFamiliarController@getEmpleado')->name('fasvit.getEmpleado');  
//  Route::get('/pdf',                           'Fasvit\InformacionFamiliarController@pdfFamilia')->name('fasvit.pdfFamilia');

  Route::get('/Tratamientos',                  'Fasvit\TratamientoController@index')->name('fasvit.tratamientolist');
  Route::get('/Tratamientos/Registro',         'Fasvit\TratamientoController@create')->name('fasvit.tratamiento');
  Route::post('/Tratamientos/Registro/store',  'Fasvit\TratamientoController@store')->name('fasvit.tratamiemtostore');
  Route::get('/Tratamientos/edit/{id}',        'Fasvit\TratamientoController@edit')->name('fasvit.tratamiemtoedit');
  Route::put('/Tratamientos/accion/{id}',      'Fasvit\TratamientoController@accion')->name('fasvit.tratamiemtoaccion');
  Route::get('/getPats',                       'Fasvit\TratamientoController@getPats')->name('fasvit.getPats');
  Route::get('/Tratamientos/PDF/{id}',         'Fasvit\TratamientoController@solicitudPDF')->name('fasvit.tratamientopdf');

  Route::get('/Gerencia',                      'Fasvit\GerenciaFasvitController@index')->name('fasvit.gerencia');
  Route::get('/Gerencia/Listado_gerencia',     'Fasvit\GerenciaFasvitController@listadoger')->name('fasvit.listadoger');
  Route::post('/Gerencia/Filtro',              'Fasvit\GerenciaFasvitController@filtro')->name('fasvit.filtroger');

  Route::post('/getEmpleado', 'Fasvit\InformacionFamiliarController@getEmpleado')->name('fasvit.getEmpleado');
  Route::post('/Registro/Familiares/store', 'Fasvit\InformacionFamiliarController@store')->name('fasvit.store');
  Route::post('/Registro/Familiares/approve', 'Fasvit\InformacionFamiliarController@approve')->name('fasvit.approve');
  Route::post('/Registro/Familiares/reject', 'Fasvit\InformacionFamiliarController@reject')->name('fasvit.reject');
  Route::post('/pdf', 'Fasvit\InformacionFamiliarController@pdfFamilia')->name('fasvit.pdfFamilia');

  Route::get('/Familiares/Reporte', 'Fasvit\InformacionFamiliarController@reporte')->name('fasvit.reporte_view');
  Route::post('/Familiares/Reporte/pdf', 'Fasvit\InformacionFamiliarController@pdf')->name('fasvit.reporte_view.pdf');


  Route::get('/Patologias', 'Fasvit\PatologiaController@index')->name('fasvit.patologias.index');
  Route::get('/Patologias/Registro', 'Fasvit\PatologiaController@registro')->name('fasvit.registro.patologia');
  Route::get('/Patologias/Registro/getData', 'Fasvit\PatologiaController@getData')->name('fasvit.patologia.getData');
  Route::post('/Patologias/Registro/getPat', 'Fasvit\PatologiaController@getPat')->name('fasvit.patologia.getPat');
  Route::post('/Patologias/Registro/sendData', 'Fasvit\PatologiaController@store')->name('fasvit.patologia.store');
  Route::get('/Patologias/Registro/edit/{id}', 'Fasvit\PatologiaController@edit')->name('fasvit.patologia.edit');
  Route::put('/Patologias/Registro/sendDataEdit/{id}', 'Fasvit\PatologiaController@update')->name('fasvit.patologia.update');
  Route::delete('/Patologias/Registro/destroy/{id}', 'Fasvit\PatologiaController@destroy')->name('fasvit.patologia.destroy');


});