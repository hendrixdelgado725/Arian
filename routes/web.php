<?php
use App\Sucursal;
use App\Models\Fiscal;
use App\Almacen;
use Illuminate\Contracts\Auth\Guard;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	$sucursal = Sucursal::get();
	$anio = Fiscal::where('codemp','>','010')->get();
     return view('auth.login')->with(['sucursal'=>$sucursal,'year' =>$anio]);
})->name('logins')->middleware('guest');//para que no me rediriga al login despues de haber sido logueado

//roles
 
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/modulo/ConfiguracionGenerales/Droles','RolesController@index')->name('rolesIndex')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Droles');

Route::get('/modulo/ConfiguracionGenerales/Droles/Delete/{id?}','RolesController@deleteRol')->name('rolesBorrar');
Route::get('/modulo/ConfiguracionGenerales/Droles/create/{id?}','RolesController@createRol')->name('rolescreate');
Route::post('/modulo/ConfiguracionGenerales/Droles/createRol','RolesController@createRoles')->name('rolescreates')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Droles');

///
// CAJA
Route::get('/modulo/ConfiguracionGenerales/Cajas/createcaja', 'CajaController@create')->name('cajaCreate')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Cajas');

Route::get('/modulo/ConfiguracionGenerales/Cajas/cajas', 'CajaController@index')->name('cajaList')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Cajas');

Route::post('/modulo/ConfiguracionGenerales/Cajas/insertcaja', 'CajaController@store')->name('cajaSave')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Cajas');

Route::get('/modulo/ConfiguracionGenerales/Cajas/editcaja/{id}', 'CajaController@show')->name('cajaEdit2')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Cajas');
// CAJA //CAJAS no caja

Route::get('/modulo/ConfiguracionGenerales/Cajas', 'CajaController@index')->name('cajaList2')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Cajas');

Route::post('/modulo/ConfiguracionGenerales/Cajas/cajaSave', 'CajaController@store')->name('cajaSave2')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Cajas');

Route::get('/modulo/ConfiguracionGenerales/Cajas/cajaEdit/{id}', 'CajaController@show')->name('cajaEdit');

Route::put('/modulo/ConfiguracionGenerales/Cajas/cajaUpdate/{id}', 'CajaController@update')->name('cajaUpdate');

Route::get('/modulo/ConfiguracionGenerales/Cajas/cajaDelete/{id}', 'CajaController@destroy')->name('cajaDelete');

Route::get('/modulo/ConfiguracionGenerales/Cajas/cajaConf/{id}', 'CajaController@getconfview')->name('cajaConf');
Route::get('/modulo/ConfiguracionGenerales/Cajas/cajaConf/cajaReporte/{id}/{job}', 'CajaController@Reporte')->name('cajaReporte');

Route::post('/modulo/ConfiguracionGenerales/Cajas/cajaConf/getStatus/{id}', 'CajaController@getStatus')->name('getStatus');

Route::post('/modulo/ConfiguracionGenerales/Cajas/cajaConf/sendTickera/{id}', 'CajaController@sendTickera')->name('sendTickera');

Route::post('/modulo/ConfiguracionGenerales/Cajas/cambiarIva/{id}', 'CajaController@cambiarIva')->name('cambiarIva');


Route::get('/modulo/ConfiguracionGenerales/Cajas/buscarCaja', 'CajaController@filtro')->name('buscarCaja');

Route::get('/modulo/ConfiguracionGenerales/Cajas/IvaAdm/{id}', 'CajaController@IvaAdm')->name('IvaAdm')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Cajas/IvaAdm');


//////// CATEGORIAS

Route::get('/modulo/ConfiguracionGenerales/Categorias', 'CategoriasController@index')->name('categorias.index')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Categorias');

Route::get('/modulo/ConfiguracionGenerales/Categorias/create', 'CategoriasController@create')->name('categorias.create')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Categorias');

Route::post('/modulo/ConfiguracionGenerales/Categorias/sendCat', 'CategoriasController@store')->name('categorias.sendCat')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Categorias');

Route::post('/modulo/ConfiguracionGenerales/Categorias/getCat', 'CategoriasController@getCat')->name('categorias.getCat')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Categorias');

Route::get('/modulo/ConfiguracionGenerales/Categorias/edit/{id}', 'CategoriasController@edit')->name('categorias.edit')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Categorias');

Route::put('/modulo/ConfiguracionGenerales/Categorias/update/{id}', 'CategoriasController@update')->name('categorias.update');

Route::delete('/modulo/ConfiguracionGenerales/Categorias/destroy/{id}', 'CategoriasController@destroy')->name('categorias.destroy');

// PAIS
Route::get('/modulo/ConfiguracionGenerales/DTerritorial/Pais','PaisController@index')->name('paisList')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/DTerritorial/Pais');

Route::get('/modulo/ConfiguracionGenerales/DTerritorial/Pais/filtro','PaisController@filtro')->name('paisFiltro')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/DTerritorial/Pais');

Route::get('/modulo/ConfiguracionGenerales/DTerritorial/Pais/paisCreate','PaisController@create')->name('paisCreate')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/DTerritorial/Pais');

Route::get('/modulo/ConfiguracionGenerales/DTerritorial/Pais/paisEdit/{id}','PaisController@show')->name('paisEdit')->middleware('permission:contenidos.delete-,/modulo/ConfiguracionGenerales/DTerritorial/Pais');

Route::post('/modulo/ConfiguracionGenerales/DTerritorial/Pais/paisUpdate/{id}','PaisController@update')->name('paisUpdate')->middleware('permission:contenidos.delete-,/modulo/ConfiguracionGenerales/DTerritorial/Pais');

Route::post('/modulo/ConfiguracionGenerales/DTerritorial/Pais/paisSave','PaisController@store')->name('paisSave')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/DTerritorial/Pais');

Route::get('/modulo/ConfiguracionGenerales/DTerritorial/Pais/paisDelete/{id}','PaisController@destroypais')->name('paisDelete')->middleware('permission:contenidos.delete-,/modulo/ConfiguracionGenerales/DTerritorial/Pais');
// ESTADO
Route::get('/modulo/ConfiguracionGenerales/DTerritorial/getEstJson/{id?}','EstadoController@getestadojson')->name('getEstJson')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/DTerritorial/Estado');

Route::get('/modulo/ConfiguracionGenerales/DTerritorial/Estado','EstadoController@index')->name('estadoList')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/DTerritorial/Estado');

Route::get('/modulo/ConfiguracionGenerales/DTerritorial/Estado/Filtro','EstadoController@filtro')->name('estadoFiltro');

Route::get('/modulo/ConfiguracionGenerales/DTerritorial/Estado/estadoCreate','EstadoController@create')->name('estadoCreate')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/DTerritorial/Estado');//vista

Route::post('/modulo/ConfiguracionGenerales/DTerritorial/Estado/estadoSave','EstadoController@store')->name('estadoSave')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/DTerritorial/Estado');//vista

Route::get('/modulo/ConfiguracionGenerales/DTerritorial/Estado/estadoShow/{id}','EstadoController@show')->name('estadoShow')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/DTerritorial/Estado');//vista

Route::put('/modulo/ConfiguracionGenerales/DTerritorial/estadoUpdate/{id}','EstadoController@update')->name('estadoUpdate')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/DTerritorial/Estado');//vista

Route::get('/modulo/ConfiguracionGenerales/DTerritorial/estadoDelete/{id}','EstadoController@destroyest')->name('estadoDelete')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/DTerritorial/Estado');//vista


//CONFIGURACIONES GENERALES PROMOCIONES

Route::get('/modulo/Almacen/Promociones','PromocionesController@index')->name('promociones')->middleware('permission:contenidos.index-,/Almacen/Promociones');
Route::get('/modulo/Almacen/Promociones/Edit/{id}','PromocionesController@infoupdate')->name('editpromociones')->middleware('permission:contenidos.edit-,/Almacen/Promociones');
Route::get('/modulo/Almacen/Promociones/Detalles/{id}','PromocionesController@detalles')->name('detalles.promociones')->middleware('permission:contenidos.edit-,/Almacen/Promociones');
Route::post('/modulo/Almacen/Promociones/ArticuloAlmacen','PromocionesController@getArticuloAlmacen')->name('articuloPorAlamacen.promociones');
Route::get('/modulo/Almacen/Promociones/Buscarpromocion','PromocionesController@filtro')->name('filtro.promociones');
Route::get('/modulo/Almacen/CrearPromociones','PromocionesController@create')->name('promociones.crear')->middleware('permission:contenidos.create-,/Almacen/Promociones');
Route::post('/modulo/Almacen/Promociones/Anular','PromocionesController@anularArticulo')->name('promociones.anular')->middleware('permission:contenidos.edit-,/Almacen/Promociones');
Route::post('/modulo/Almacen/Promociones/Activar','PromocionesController@activateArticulo')->name('promociones.activo')->middleware('permission:contenidos.edit-,/Almacen/Promociones');
Route::get('/modulo/Almacen/CrearPromociones/getinfo','PromocionesController@getinfo')->name('getinfo.promociones');
Route::post('/modulo/Almacen/CrearPromociones/create','PromocionesController@createPromo')->name('create.promociones')->middleware('permission:contenidos.create-,/Almacen/Promociones');
Route::post('/modulo/Almacen/EliminarPromocion/{id}','PromocionesController@destroy')->name('promocionDelete')->middleware('permission:contenidos.delete-,/Almacen/Promociones');
Route::post('/modulo/Almacen/promociones/update','PromocionesController@update')->name('promocionedit')->middleware('permission:contenidos.delete-,/Almacen/Promociones');
//Route::get('/getpais/{id}','PaisController@index')->name('getPais');
//Route::get('/getpais/{id}','PaisController@index')->name('getPais');


// SUCURSAL
Route::get('/modulo/ConfiguracionGenerales/Sucursales','SucursalController@index')->name('sucursalList')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Sucursales');

Route::get('/modulo/ConfiguracionGenerales/Sucursales/SucursalCreate','SucursalController@create')->name('sucursalCreate')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Sucursales');
//vista

Route::post('/modulo/ConfiguracionGenerales/Sucursales/SucursalSave','SucursalController@store')->name('sucursalSave')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Sucursales');
//vista

Route::get('/modulo/ConfiguracionGenerales/Sucursales/SucursalShow/{id}','SucursalController@show')->name('sucursalShow')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Sucursales');
;//vista

Route::put('/modulo/ConfiguracionGenerales/Sucursales/SucursalUpdate/{id}','SucursalController@update')->name('sucursalUpdate');
//vista

Route::get('/modulo/ConfiguracionGenerales/Sucursales/SucursalDelete/{id}','SucursalController@destroy')->name('sucursalDelete')->middleware('permission:contenidos.delete-,/modulo/ConfiguracionGenerales/Sucursales');
//vista

Route::get('/modulo/ConfiguracionGenerales/Sucursales/SucursalFind','SucursalController@filtro')->name('buscarSucursal')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Sucursales');//vista

//EMPRESA
Route::get('/modulo/ConfiguracionGenerales/DdeEmpresa','EmpresaController@index')->name('EmpresaList')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/DdeEmpresa');
Route::get('/modulo/ConfiguracionGenerales/DdeEmpresa/EmpresaCreate','EmpresaController@create')->name('EmpresaCreate')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/DdeEmpresa');//vista
Route::post('/modulo/ConfiguracionGenerales/DdeEmpresa/EmpresaSave','EmpresaController@store')->name('EmpresaSave')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/DdeEmpresa');//vista
Route::get('/modulo/ConfiguracionGenerales/DdeEmpresa/EmpresaShow/{id}','EmpresaController@show')->name('EmpresaShow')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/DdeEmpresa');//vista
Route::post('/modulo/ConfiguracionGenerales/DdeEmpresa/EmpresaUpdate/{id}','EmpresaController@update')->name('EmpresaUpdate')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/DdeEmpresa');//vista
Route::get('/modulo/ConfiguracionGenerales/DdeEmpresa/EmpresaDelete/{id}','EmpresaController@destroy')->name('EmpresaDelete')->middleware('permission:contenidos.delete-,/modulo/ConfiguracionGenerales/DdeEmpresa');//vista
Route::post('/modulo/ConfiguracionGenerales/DdeEmpresa','EmpresaController@filtro')->name('buscar');

/* Facturacion Views */
Route::get('/modulo/Facturacion/Facturacionv1','FacturacionController@view1')->name('Facturacionv1')->middleware('permission:contenidos.index-,/modulo/Facturacion/Facturacionv1');
Route::get('/modulo/Facturacion/Facturacionv2','FacturacionController@view2')->name('Facturacionv2')->middleware('permission:contenidos.index-,/modulo/Facturacion/Facturacionv2');
Route::get('/modulo/Facturacion/FacturacionMa','FacturacionController@view3')->name('FacturacionMa')->middleware('permission:contenidos.index-,/modulo/Facturacion/FacturacionMa');

Route::get('/modulo/Facturacion/Lfacturas','FacturacionController@index')->name('Facturas')
           ->middleware('permission:contenidos.index-,/modulo/Facturacion/Lfacturas');


Route::get('/modulo/Facturacion/Lfacturas/Detalle/{id}','FacturacionController@detallar')->name('detallesFactura')->middleware('permission:contenidos.index-,/modulo/Facturacion/Lfacturas');
Route::get('/modulo/Facturacion/Lfacturas/Anular2/{id}','FacturacionController@anular2')->name('anularFactura2');
Route::get('/modulo/Facturacion/Lfacturas/Anular','FacturacionController@devFiscal')->name('anularFactura')->middleware('permission:contenidos.delete-,/modulo/Facturacion/Lfacturas');
Route::get('/modulo/Facturacion/Lfacturas','FacturacionController@filtro')->name('filtroFactura');
/*Route::get('/modulo/Facturacion/Lfacturas/Activar/{id}','FacturacionController@activar')->name('activarFactura');*/
Route::get('/modulo/ConfiguracionGenerales/Clientes/getPersonaJsonFactura','ClienteController@cne2')->name('getPersonaF');/*->middleware('permission:contenidos.create,/modulo/ConfiguracionGenerales/Clientes');*/
Route::get('/modulo/Facturacion/Facturacionv2/getPrecio','FacturacionController@getPrecio')->name('getPrecio');
Route::get('/modulo/Facturacion/Facturacionv2/getTallas','FacturacionController@getTallas')->name('getTallas');
Route::get('/modulo/Facturacion/Facturacionv2/getRecargo','FacturacionController@getRecargo')->name('getRecargo');
Route::get('/modulo/Facturacion/Facturacionv2/getArt','FacturacionController@getArt')->name('getArt');
Route::get('/modulo/Facturacion/Facturacionv2/getExistenciaArt','FacturacionController@getExistenciaArt')->name('getExistenciaArt');
Route::get('/modulo/Facturacion/Facturacionv2/buscarFactura','FacturacionController@buscarFactura')->name('buscarFactura');
Route::get('/modulo/Facturacion/Facturacionv2/getCliente','ClienteController@Cliente')->name('getCliente');
Route::get('/modulo/Facturacion/Facturacionv2/getSeriales','FacturacionController@getSeriales')->name('getSeriales');
Route::get('/modulo/Facturacion/Facturacionv2/searchSeriales','FacturacionController@getSeriales');

Route::post('/modulo/Facturacion/Facturacionv2/validacionContraseña','FacturacionController@validacionContraseña')->name('validacionContraseña');

//Route::get('/modulo/Facturacion/Facturacionv2/facturar','FacturacionController@sendFiscal')->name('facturar');

Route::post('/modulo/Facturacion/Facturacionv2/facturar','FacturacionController@create')->name('facturar')/*->middleware('permission:contenidos.create-,/modulo/Facturacion/Facturacionv2/facturar')*/;

Route::get('/modulo/Facturacion/Facturacionv2/cajaUser','FacturacionController@getCajas')->name('getCajas');
Route::get('/modulo/Facturacion/Facturacionv2/buscarPresupuestoFac','FacturacionController@buscarPresupuesto')->name('buscarPresupuestoFac');
Route::get('/modulo/Facturacion/Facturacionv2/seleccionarPre','FacturacionController@seleccionarPre')->name('seleccionarPre');
Route::get('/modulo/Facturacion/FacturacionMa/getEscala','FacturacionController@getEscala')->name('getEscala');
Route::get('/modulo/Facturacion/FacturacionMa/getEscalas','FacturacionController@getEscalas')->name('getEscalas');
Route::get('/modulo/Facturacion/FacturacionMa/getEscalaST','FacturacionController@getEscalaST')->name('getEscalaST');
Route::get('/modulo/Facturacion/FacturacionMa/getEscalaCant','FacturacionController@cantEscala')->name('getEscalaCant');
Route::get('/modulo/Facturacion/Facturacionv2/getMoneda','FacturacionController@getMoneda')->name('getMoneda');
Route::get('/modulo/Facturacion/Facturacionv2/getTasaMoneda','FacturacionController@getTasaMoneda')->name('getTasaMoneda');
Route::get('/modulo/Facturacion/Facturacionv2/getColores','FacturacionController@getColores')->name('getColores');
Route::get('/modulo/Facturacion/Facturacionv2/buscarNotaFac','FacturacionController@buscarNota')->name('buscarNotaFac');
Route::get('/modulo/Facturacion/Facturacionv2/seleccionarNota','FacturacionController@seleccionarNota')->name('seleccionarNota');
Route::post('/modulo/Facturacion/Facturacion/printFiscal', 'FacturacionController@printFactura')->name('printFactura');
Route::post('/modulo/Facturacion/Facturacion/Serial', 'FacturacionController@seriales')->name('seriales');
Route::get('/modulo/Facturacion/search', 'FacturacionController@search')->name('Serial');
Route::get('/modulo/Facturacion/arqueoCaja', 'FacturacionController@arqueoCaja')->name('arqueoCaja');


//////////////////// TURNOS //////////////////

Route::get('/modulo/Facturacion/Turnos','TurnosController@index')->name('turnos')->middleware('permission:contenidos.index-,/modulo/Facturacion/Turnos/');

Route::get('/modulo/Facturacion/Turnos/Create','TurnosController@create')->name('TurnosCreate')->middleware('permission:contenidos.create-,/modulo/Facturacion/Turnos/');

Route::get('/modulo/Facturacion/Turnos/Create/getData', 'TurnosController@getData')->name('getDataTurnos')->middleware('permission:contenidos.create-,/modulo/Facturacion/Turnos/');

Route::post('/modulo/Facturacion/Turnos/Create/getCajeros', 'TurnosController@getCajeros')->name('getDataCajeros')->middleware('permission:contenidos.create-,/modulo/Facturacion/Turnos/');

Route::post('/modulo/Facturacion/Turnos/Create/sendturno','TurnosController@store')->name('TurnosStore')->middleware('permission:contenidos.store-,/modulo/Facturacion/Turnos/');

Route::get('/modulo/Facturacion/Turnos/edit/{id}','TurnosController@edit')->name('TurnosEdit')->middleware('permission:contenidos.edit-,/modulo/Facturacion/Turnos/');

Route::post('/modulo/Facturacion/Turnos/update/{id}','TurnosController@update')->name('TurnosUpdate')->middleware('permission:contenidos.edit-,/modulo/Facturacion/Turnos/');

Route::get('/modulo/Facturacion/Turnos/destroy/{id}','TurnosController@destroy')->name('TurnosDestroy')->middleware('permission:contenidos.store-,/modulo/Facturacion/Turnos/');

Route::post('/modulo/Facturacion/Turnos/getTurno','TurnosController@getTurno')->name('getTurno')->middleware('permission:contenidos.edit-,/modulo/Facturacion/Turnos/');

Route::get('/modulo/Facturacion/Turnos/filtro','TurnosController@filtroTurnos')->name('filtroTurnos')->middleware('permission:contenidos.index-,/modulo/Facturacion/Turnos/');




/****************  Proforma  ******************/

Route::get('/modulo/Facturacion/NotaEntrega', 'FaNotaEntregaController@index')->name('FNEIndex')->middleware('permission:contenidos.index-,/modulo/Facturacion/NotaEntrega');
Route::get('/modulo/Facturacion/NotaEntrega/create', 'FaNotaEntregaController@create')->name('FNECreate')->middleware('permission:contenidos.create-,/modulo/Facturacion/NotaEntrega');
Route::get('/modulo/Facturacion/NotaEntrega/getData', 'FaNotaEntregaController@getData')->name('FNEgetData')->middleware('permission:contenidos.create-,/modulo/Facturacion/NotaEntrega');
Route::post('/modulo/Facturacion/NotaEntrega/newCliente', 'FaNotaEntregaController@newCliente')->name('FNEnewCliente')->middleware('permission:contenidos.create-,/modulo/Facturacion/NotaEntrega');
Route::post('/modulo/Facturacion/NotaEntrega/store', 'FaNotaEntregaController@store')->name('FNEstore')->middleware('permission:contenidos.create-,/modulo/Facturacion/NotaEntrega');
Route::get('/modulo/Facturacion/NotaEntrega/show/{id}', 'FaNotaEntregaController@show')->name('FNEshow')->middleware('permission:contenidos.edit-,/modulo/Facturacion/NotaEntrega');
Route::get('/modulo/Facturacion/NotaEntrega/closeCaja', 'FaNotaEntregaController@closeCaja')->name('FNEcloseCaja')->middleware('permission:contenidos.edit-,/modulo/Facturacion/NotaEntrega');
Route::get('/modulo/Facturacion/NotaEntrega/checkCaja', 'FaNotaEntregaController@checkCaja')->name('FNEcheckCaja')->middleware('permission:contenidos.edit-,/modulo/Facturacion/NotaEntrega');
Route::post('/modulo/Facturacion/NotaEntrega/devolucion', 'FaNotaEntregaController@devolucion')->name('FNEdevol')->middleware('permission:contenidos.edit-,/modulo/Facturacion/NotaEntrega');
Route::get('/modulo/Facturacion/NotaEntrega/pdf/{nota}', 'FaNotaEntregaController@reportePdf')->name('FNEpdf')->middleware('permission:contenidos.edit-,/modulo/Facturacion/NotaEntrega');
/**********Configuraciones Generales: Cliente********************/
Route::get('/modulo/ConfiguracionGenerales/Clientes','ClienteController@lista')->name('listaCliente')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Clientes');

Route::get('/modulo/ConfiguracionGenerales/Clientes/nuevocliente','ClienteController@nuevocliente')->name('nuevoCliente')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Clientes');

Route::post('/modulo/ConfiguracionGenerales/Clientes/registrarCliente','ClienteController@create')->name('registrarCliente')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Clientes');

Route::get('/modulo/ConfiguracionGenerales/Clientes/editarClienteForm/{id}','ClienteController@edit')->name('editarClienteForm')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Clientes');

Route::put('/modulo/ConfiguracionGenerales/Clientes/editarCliente/{id}','ClienteController@update')->name('actualizarCliente');

Route::get('/modulo/ConfiguracionGenerales/Clientes','ClienteController@filtro')->name('buscarCliente')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Clientes');

Route::get('/modulo/ConfiguracionGenerales/Clientes/eliminarCliente/{id}','ClienteController@delete')->name('eliminarCliente')->middleware('permission:contenidos.delete-,/modulo/ConfiguracionGenerales/Clientes');

Route::get('/modulo/ConfiguracionGenerales/Clientes/getPersonaJson','ClienteController@cne')->name('getPersona')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Clientes');

Route::get('/modulo/ConfiguracionGenerales/Clientes/registrarClientePre','ClienteController@createClienteModal')->name('registrarclienteModal')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Clientes');

/***********Configuraciones Generales: Cargo de Empleado*******************/
Route::get('/modulo/ConfiguracionGenerales/CdeEmpleado','EmpleadoCargoController@listaCempleado')->name('listaCargosE')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/CdeEmpleado');

Route::get('/modulo/ConfiguracionGenerales/CdeEmpleado/nuevoCargo','EmpleadoCargoController@nuevoCargo')->name('nuevoCargoE')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/CdeEmpleado');

Route::post('/modulo/ConfiguracionGenerales/CdeEmpleado/registrarCargoE','EmpleadoCargoController@create')->name('registrarCargoE')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/CdeEmpleado');

Route::get('/modulo/ConfiguracionGenerales/CdeEmpleado/editarCargoForm{id}','EmpleadoCargoController@edit')->name('editarCargoForm')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/CdeEmpleado');

Route::put('/modulo/ConfiguracionGenerales/CdeEmpleado/editarCargo{id}','EmpleadoCargoController@update')->name('actualizarCargo')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/CdeEmpleado');

Route::get('/modulo/ConfiguracionGenerales/CdeEmpleado/eliminarCargo{id}','EmpleadoCargoController@delete')->name('eliminarCargo')->middleware('permission:contenidos.delete-,/modulo/ConfiguracionGenerales/CdeEmpleado');

Route::get('/modulo/ConfiguracionGenerales/CdeEmpleado','EmpleadoCargoController@filtro')->name('buscarCargoE');
/************Configuraciones Generales: Empleado****************/
Route::get('modulo/ConfiguracionGenerales/Empleado','EmpleadoController@index')->name('lista.empleado')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Empleado');

Route::get('/modulo/ConfiguracionGenerales/nuevoempleado','EmpleadoController@nuevoempleado')->name('nuevoEmpleado')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Empleado');

Route::post('/modulo/ConfiguracionGenerales/Empleado/registrarEmpleado','EmpleadoController@create')->name('registrarEmpleado')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Empleado');

Route::get('/modulo/ConfiguracionGenerales/Empleado/editarEmpleadoForm/{id}','EmpleadoController@edit')->name('editarEmpleado')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Empleado');

Route::put('/modulo/ConfiguracionGenerales/Empleado/editarEmpleado/{id}','EmpleadoController@update')->name('actualizarEmpleado')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Empleado');

Route::get('/modulo/ConfiguracionGenerales/Empleado/eliminarEmpleado/{id}','EmpleadoController@delete')->name('eliminarEmpleado')->middleware('permission:contenidos.delete-,/modulo/ConfiguracionGenerales/Empleado');

Route::get('/modulo/ConfiguracionGenerales/Empleado','EmpleadoController@filtro')->name('buscarEmpleado');
/************Configuraciones Generales: Moneda*******************/
Route::get('/modulo/ConfiguracionGenerales/Moneda','MonedaController@index')->name('listaMoneda')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Moneda');

Route::get('/modulo/ConfiguracionGenerales/Moneda/nuevaMoneda','MonedaController@nuevaMoneda')->name('nuevaMoneda')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Moneda');

Route::post('/modulo/ConfiguracionGenerales/Moneda/registrarMoneda','MonedaController@create')->name('registrarMoneda')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Moneda');

Route::get('/modulo/ConfiguracionGenerales/Moneda/editarMonedaForm/{id}','MonedaController@edit')->name('editarMoneda')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Moneda');

Route::put('/modulo/ConfiguracionGenerales/Moneda/editarMoneda/{id}','MonedaController@update')->name('actualizarMoneda')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Moneda');

Route::get('/modulo/ConfiguracionGenerales/Moneda/eliminarMoneda/{id}','MonedaController@delete')->name('eliminarMoneda')->middleware('permission:contenidos.delete-,/modulo/ConfiguracionGenerales/Moneda');

Route::get('/modulo/ConfiguracionGenerales/Moneda','MonedaController@filtro')->name('buscarMoneda');
//quede aqui

Route::put('/modulo/ConfiguracionGenerales/Moneda/status','MonedaController@changeStatusMoneda')->name('changeStatusMoneda');


/*************Configuraciones Generales: Tasa de cambio************/
Route::get('/modulo/ConfiguracionGenerales/TdeCambio','TasaCambioController@index')->name('listaTCambio')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/TdeCambio');

Route::get('/modulo/ConfiguracionGenerales/TdeCambio/nuevaTasa','TasaCambioController@nuevaTasa')->name('nuevaTasa')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/TdeCambio');

Route::post('/modulo/ConfiguracionGenerales/TdeCambio/registrarTasa','TasaCambioController@create')->name('registrarTasa')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/TdeCambio');

Route::get('/modulo/ConfiguracionGenerales/TdeCambio/editarTasaForm/{id}','TasaCambioController@edit')->name('editarTasa')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/TdeCambio');

Route::post('/modulo/ConfiguracionGenerales/TdeCambio/editarTasa/{id}','TasaCambioController@update')->name('actualizarTasa')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/TdeCambio');

Route::get('/modulo/ConfiguracionGenerales/TdeCambio','TasaCambioController@filtro')->name('buscarTasa')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/TdeCambio');

/*************Configuraciones Generales: Demografia de Productos****/
/* Route::get('/modulo/ConfiguracionGenerales/DemoProducto','DemoProductoController@index')->name('listaDemoProducto')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/DemoProducto');

Route::get('/modulo/ConfiguracionGenerales/DemoProducto/nuevaDemografiaProducto','DemoProductoController@nuevaDemoP')->name('nuevaDemoP')
			->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/DemoProducto');

Route::post('/modulo/ConfiguracionGenerales/DemoProducto/registrarDemoProducto','DemoProductoController@create')->name('registrarDemoProducto')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/DemoProducto');

Route::get('/modulo/ConfiguracionGenerales/DemoProducto/editarDemoProductoForm/{id}','DemoProductoController@edit')->name('editarDemoProducto')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/DemoProducto');

Route::put('/modulo/ConfiguracionGenerales/DemoProducto/editarDemoProduco/{id}','DemoProductoController@update')->name('actualizarDemoProducto')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/DemoProducto');

Route::get('/modulo/ConfiguracionGenerales/DemoProducto/eliminarDemoProducto/{id}','DemoProductoController@delete')->name('eliminarDemoProducto')->middleware('permission:contenidos.delete-,/modulo/ConfiguracionGenerales/DemoProducto');

Route::get('/modulo/ConfiguracionGenerales/DemoProducto/buscarDemoProducto','DemoProductoController@filtro')->name('buscarDemografia');

 */
/*************Configuraciones Generales: Categoria del producto*****/
Route::get('/modulo/ConfiguracionGenerales/CdeProducto','CProductoController@index')->name('listaCdeProducto')->
       middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/CdeProducto');

Route::get('/modulo/ConfiguracionGenerales/CdeProducto/nuevaCategoria','CProductoController@nuevacategoria')->name('nuevaCdeProducto')
		->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/CdeProducto');


Route::post('/modulo/ConfiguracionGenerales/CdeProducto/registrarCdeProducto','CProductoController@create')->name('registrarCdeProducto')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/CdeProducto');

Route::get('/modulo/ConfiguracionGenerales/CdeProducto/editarCdeProductoForm/{id}','CProductoController@edit')->name('editarCdeProducto')
		->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/CdeProducto');


Route::put('/modulo/ConfiguracionGenerales/CdeProducto/editarCdeProducto/{id}','CProductoController@update')->name('actualizarCdeProducto')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/CdeProducto');

Route::get('/modulo/ConfiguracionGenerales/CdeProducto/eliminarCdeProducto/{id}','CProductoController@delete')->name('eliminarCdeProducto')->middleware('permission:contenidos.delete-,/modulo/ConfiguracionGenerales/CdeProducto');;


Route::get('/modulo/ConfiguracionGenerales/CdeProducto/buscarCdeProducto','CProductoController@filtro')->name('buscarCdeProducto');



/*************Configuraciones Generales: ******************* */
Route::get('/modulo/ConfiguracionGenerales/SCdeProducto','SCProductoController@index')->name('listaSubCategoriaP')
      ->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/SCdeProducto');

Route::get('/modulo/ConfiguracionGenerales/SCdeProducto/nuevoSCProducto','SCProductoController@nuevoSubCat')->name('nuevoSubCat')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/SCdeProducto');

Route::post('/modulo/ConfiguracionGenerales/SCdeProducto/registrarSCProducto','SCProductoController@create')->name('registrarSCdeProducto')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/SCdeProducto');

Route::get('/modulo/ConfiguracionGenerales/SCdeProducto/editarSCProductoForm/{id}','SCProductoController@edit')->name('editarSCProducto')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/SCdeProducto');

Route::put('/modulo/ConfiguracionGenerales/SCdeProducto/editarSCProducto/{id}','SCProductoController@update')->name('actualizarSCProducto')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/SCdeProducto');

Route::get('/modulo/ConfiguracionGenerales/SCdeProducto/buscarSCProducto','SCProductoController@filtro')->name('buscarSCProducto');

Route::get('/modulo/ConfiguracionGenerales/SCdeProducto/eliminarSCProducto/{id}','SCProductoController@delete')->name('eliminarSCProducto')->middleware('permission:contenidos.delete-,/modulo/ConfiguracionGenerales/SCdeProducto');


/*************Configuraciones Generales: Unidad de Medida*************************** */

Route::get('/modulo/ConfiguracionGenerales/UnidadMedida','UnidadMedidaController@index')->name('Unidadmedida.index')
       ->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/UnidadMedida');

Route::get('/modulo/ConfiguracionGenerales/UnidadMedida/create','UnidadMedidaController@create')->name('Unidadmedida.create')
       ->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/UnidadMedida');

Route::post('/modulo/ConfiguracionGenerales/UnidadMedida/senduni','UnidadMedidaController@store')->name('Unidadmedida.store')
       ->middleware('permission:contenidos.store-,/modulo/ConfiguracionGenerales/UnidadMedida');

Route::get('/modulo/ConfiguracionGenerales/UnidadMedida/edit/{id}','UnidadMedidaController@edit')->name('Unidadmedida.edit')
       ->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/UnidadMedida');

Route::post('/modulo/ConfiguracionGenerales/UnidadMedida/getUni','UnidadMedidaController@getUni')->name('Unidadmedida.getUni')
       ->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/UnidadMedida');

Route::post('/modulo/ConfiguracionGenerales/UnidadMedida/update/{id}','UnidadMedidaController@update')->name('Unidadmedida.update')
       ->middleware('permission:contenidos.update-,/modulo/ConfiguracionGenerales/UnidadMedida');

Route::get('/modulo/ConfiguracionGenerales/UnidadMedida/destroy/{id}','UnidadMedidaController@destroy')->name('Unidadmedida.destroy')
       ->middleware('permission:contenidos.destroy-,/modulo/ConfiguracionGenerales/UnidadMedida');




/*************Configuraciones Generales: Color*************************** */
Route::get('/modulo/ConfiguracionGenerales/Color','ColorProController@index')->name('listaColores')
       ->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Color');

Route::get('/modulo/ConfiguracionGenerales/Color/nuevoColor','ColorProController@nuevoColor')->name('nuevoColor')
       ->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Color');

Route::post('/modulo/ConfiguracionGenerales/Color/registrarColor','ColorProController@create')->name('registrarColor')
       ->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Color');

Route::get('/modulo/ConfiguracionGenerales/Color/editarColorForm/{id}','ColorProController@edit')->name('editarColor')
       ->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Color');

Route::put('/modulo/ConfiguracionGenerales/Color/editarColor/{id}','ColorProController@update')->name('actualizarColor')
       ->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Color');

Route::get('/modulo/ConfiguracionGenerales/Color/eliminarColor/{id}','ColorProController@delete')->name('eliminarColor')
       ->middleware('permission:contenidos.delete-,/modulo/ConfiguracionGenerales/Color');

Route::get('/modulo/ConfiguracionGenerales/Color/buscarColor','ColorProController@filtro')->name('buscarColor');

/**************Configuraciones Generales: Presupuesto***************/


Route::get('/modulo/ConfiguracionGenerales/Presupuesto','PresupuestoController@index')->name('listaPresupuesto')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Presupuesto');

Route::post('/modulo/ConfiguracionGenerales/Presupuesto/getTasa','PresupuestoController@cargarTasa')->name('getTasa')
       ->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Presupuesto');

Route::get('/modulo/ConfiguracionGenerales/Presupuesto/getArts','PresupuestoController@cargarArts')->name('getArts')
      ->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Presupuesto');

Route::get('/modulo/ConfiguracionGenerales/Presupuesto/buscarPresupuesto','PresupuestoController@filtro')->name('buscarPresupuesto');

Route::get('/modulo/ConfiguracionGenerales/Presupuesto/pdfPresupuesto/{presupuesto}','PresupuestoController@pdfPresupuesto')
       ->name('pdfPresupuesto')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Presupuesto');

Route::get('/modulo/ConfiguracionGenerales/Presupuesto/aprobar','PresupuestoController@aprobar')->name('presupuestoAprobar')
       ->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Presupuesto');

Route::get('/modulo/ConfiguracionGenerales/Presupuesto/anular','PresupuestoController@anular')->name('presupuestoAnular')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Presupuesto');

Route::get('/modulo/ConfiguracionGenerales/Presupuesto/Detalle/{id}','PresupuestoController@detallar')->name('detallesPresupuesto')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Presupuesto');
Route::post('modulo/ConfiguracionGenerales/Presupuesto/getArticulos', 'PresupuestoController@getArticulos')->name('getArticulosPresupuesto')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Presupuesto');
Route::get('/modulo/ConfiguracionGenerales/Presupuesto/getClientes', 'PresupuestoController@getClientes')->name('getClientes');

Route::get('/modulo/ConfiguracionGenerales/Presupuesto/nuevoPresupuesto','PresupuestoController@nuevoPresupuesto')->name('nuevoPresupuesto')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Presupuesto');

Route::post('/modulo/ConfiguracionGenerales/Presupuesto/registrarPresupuesto','PresupuestoController@create')->name('registrarPresupuesto')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Presupuesto');

Route::get('/modulo/ConfiguracionGenerales/Presupuesto/editarPresupuestoForm/{id}','PresupuestoController@edit')->name('editarPresupuesto')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Presupuesto');

Route::post('/modulo/ConfiguracionGenerales/Presupuesto/editarPresupuesto/{id}/{id2}','PresupuestoController@update')->name('actualizarPresupuesto')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Presupuesto');
Route::get('/modulo/ConfiguracionGenerales/Presupuesto/getTalla','PresupuestoController@cargarTalla')->name('getTalla');

/**************Configuraciones Generales: Duracion Presupuesto*******/
Route::get('/modulo/ConfiguracionGenerales/DPresupuesto','DPresupuestoController@index')->name('listaDPresupuesto')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/DPresupuesto');

Route::get('/modulo/ConfiguracionGenerales/DPresupuesto/nuevaDPresupuesto','DPresupuestoController@nuevoDPresupuesto')->name('nuevoDPresupuesto')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/DPresupuesto');

Route::post('/modulo/ConfiguracionGenerales/DPresupuesto/registrarDPresupuesto','DPresupuestoController@create')->name('registrarDPresupuesto')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/DPresupuesto');

Route::get('/modulo/ConfiguracionGenerales/DPresupuesto/eliminarDuracion/{codigoid}','DPresupuestoController@delete')->name('eliminarDPresupuesto')->middleware('permission:contenidos.delete-,/modulo/ConfiguracionGenerales/DPresupuesto');

Route::get('/modulo/ConfiguracionGenerales/DPresupuesto','DPresupuestoController@filtro')->name('buscarDPresupuesto');

/************** Configuraciones Generales: Centro de costo *******/

Route::get('/modulo/ConfiguracionGenerales/Costo', 'CostoController@index')->name('listaCCosto')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Costo');

Route::get('/modulo/ConfiguracionGenerales/Costo/CreateCCostos', 'CostoController@create')->name('createCCosto')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Costo');

Route::post('/modulo/ConfiguracionGenerales/Costo/StoreCCostos', 'CostoController@store')->name('storeCCosto')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Costo');;

Route::get('/modulo/ConfiguracionGenerales/Costo/EditCCostos/{id}', 'CostoController@edit')->name('editCCosto')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Costo');

Route::put('/modulo/ConfiguracionGenerales/Costo/UpdateCCostos/{id}', 'CostoController@update')->name('updateCCosto')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Costo');

Route::get('/modulo/ConfiguracionGenerales/Costo/DeleteCCostos/{id}', 'CostoController@destroy')->name('destroyCCosto')->middleware('permission:contenidos.delete-,/modulo/ConfiguracionGenerales/Costo');

Route::get('/modulo/ConfiguracionGenerales/Costo/buscarCCosto', 'CostoController@filtro')->name('buscarCCosto');

//Rutas para el menu dinamico a traves del menu
/*************************************************************/
	Route::get('/modulo/{id}',[

	  'uses' => 'almacen\Registrouser.editAlmacenController@show',
	  'as'   => 'registro'

	  ])->where('id','[A-Za-z]+');//
/*********************************************************************/

/* COMPRAS */

/* Compras: Emisión de Punto de Cuenta */

Route::get('/modulo/Compras/Emiptocta', 'EmipuntoController@index')->name('PtoctaIndex')->middleware('permission:contenidos.index-,/modulo/Compras/Emiptocta');
Route::get('/modulo/Compras/Emiptocta/EmiptoctaCreate', 'EmipuntoController@create')->name('PtoctaCreate')->middleware('permission:contenidos.create-,/modulo/Compras/Emiptocta');
Route::get('/modulo/Compras/Emiptocta/api/datosCreate', 'EmipuntoController@datosCreate')->name('datosCreate')->middleware('permission:contenidos.create-,/modulo/Compras/Emiptocta');
Route::post('/modulo/Compras/Emiptocta/EmiptoctaStore', 'EmipuntoController@store')->name('puntoStore')->middleware('permission:contenidos.create-,/modulo/Compras/Emiptocta');
/* Compras: Requisiciones por Departamento */

Route::get('/modulo/Compras/RequisicionD', 'RequisicionController@index')->name('listaReq')->middleware('permission:contenidos.index-,/modulo/Compras/RequisicionD');
Route::get('/modulo/Compras/RequisicionD/RequisicionDCreate', 'RequisicionController@create')->name('createReq')->middleware('permission:contenidos.create-,/modulo/Compras/RequisicionD');
Route::post('/modulo/Compras/RequisicionD/RequisicionDStore', 'RequisicionController@store')->name('storeReq')->middleware('permission:contenidos.create-,/modulo/Compras/RequisicionD');
Route::get('/modulo/Compras/RequisicionD/RequisicionDShow/{reqart}', 'RequisicionController@edit')->name('editReq')->middleware('permission:contenidos.edit-,/modulo/Compras/RequisicionD');
Route::post('/modulo/Compras/RequisicionD/RequisicionNewArt', 'RequisicionController@newArt')->name('newArtReq')->middleware('permission:contenidos.create-,/modulo/Compras/RequisicionD');
Route::get('/modulo/Compras/RequisicionD/RequisicionDCreate/getData', 'RequisicionController@getData')->name('getDataReq');
Route::post('/modulo/Compras/RequisicionD/RequisicionDCreate/getPart', 'RequisicionController@getPart')->name('getPartArt');
Route::post('/modulo/Compras/RequisicionD/RequisicionDCreate/artExist', 'RequisicionController@artExist')->name('artExist');
Route::post('/modulo/Compras/RequisicionD/RequisicionDCreate/reqExist', 'RequisicionController@reqExist')->name('reqExist');
Route::post('/modulo/Compras/RequisicionD/RequisicionDCreate/getGerente', 'RequisicionController@getGerente')->name('getGerente');
Route::post('/modulo/Compras/RequisicionD/RequisicionDCreate/aceptarReq', 'RequisicionController@aceptar')->name('aceptarReq');
Route::get('/modulo/Compras/RequisicionD/ReenviarReq/{reqart}', 'RequisicionController@reenviar')->name('reenviarReq');
Route::get('/modulo/Compras/RequisicionD/PresupuestoReq/{reqart}', 'RequisicionController@reqpres')->name('reqPres');
Route::get('/modulo/Compras/RequisicionD/PresupuestoMail/{reqart}', 'RequisicionController@sendpre')->name('sendpreReq');
Route::post('/modulo/Compras/RequisicionD/PresupuestoReq/action', 'RequisicionController@preAction')->name('preActionReq');
Route::post('/modulo/Compras/RequisicionD/PresupuestoReq', 'RequisicionController@Reqarticles')->name('ReqArticulos');

Route::get('/modulo/Compras/RequisicionD/Sendemail', 'RequisicionController@gerMail')->name('reqMail');

/*** PAGOS ***/

Route::get('/modulo/Compras/Pagos', 'PagosController@index')->name('pagos')->middleware('permission:contenidos.index-,/modulo/Compras/Pagos');
Route::get('/modulo/Compras/Pagos/Create', 'PagosController@create')->name('pagoCreate')->middleware('permission:contenidos.create-,/modulo/Compras/Pagos');
Route::get('/modulo/Compras/Pagos/Create/loadOrdenes', 'PagosController@loadOrdenes')->name('pagoLoadOrd')->middleware('permission:contenidos.create-,/modulo/Compras/Pagos');
Route::get('/modulo/Compras/Pagos/Create/getData', 'PagosController@getData')->name('pagoData')->middleware('permission:contenidos.create-,/modulo/Compras/Pagos');
Route::post('/modulo/Compras/Pagos/PagoStore', 'PagosController@store')->name('storePago')->middleware('permission:contenidos.create-,/modulo/Compras/Pagos');
Route::get('/modulo/Compras/Pagos/EditPago/{id}', 'PagosController@edit')->name('editPago')->middleware('permission:contenidos.edit-,/modulo/Compras/Pagos');

Route::get('/modulo/Compras/Pagos/PDF/{id}', 'PagosController@generarReportePagos')->name('generarReportePagos');
/* Compra: Precompromisos */

Route::get('/modulo/Compras/Precompromisos', 'PrecomController@index')->name('listPrecom')->middleware('permission:contenidos.index-,/modulo/Compras/Precompromisos');
Route::get('/modulo/Compras/Precompromisos/GeneratePrecom/{reqart}', 'PrecomController@genPrecom')->name('genPrecom');
Route::get('/modulo/Compras/Precompromisos/Detalles/{refprc}', 'PrecomController@show')->name('detPrecom');

/* Compras: Aprobación de solicitud de egresos por lotes */

Route::get('/modulo/Compras/Aprosolegr', 'RequisicionController@aprsolegr')->name('aprsolIndex')->middleware('permission:contenidos.index-,/modulo/Compras/Aprsolegr');

//Rutas del modulo Registro de Almacen
/**********************************************************************************/
Route::get('/modulo/Almacen/RAlmacen','Almacen_Controlador\RegistroAlmacenController@show')
->name('Listado.Almacen')->middleware('permission:contenidos.index-,/modulo/Almacen/RAlmacen');

Route::get('/modulo/Almacen/RAlmacen/registro','Almacen_Controlador\RegistroAlmacenController@registrar')->name('Registro.Almacen')->middleware('permission:contenidos.create-,/modulo/Almacen/RAlmacen');


Route::post('/modulo/Almacen/RAlmacen/registro/Almacen','Almacen_Controlador\RegistroAlmacenController@registro')->name('Registrar.Almacen')->middleware('permission:contenidos.create-,/modulo/Almacen/RAlmacen');

Route::get('/modulo/Almacen/RAlmacen/eliminar/{id?}','Almacen_Controlador\RegistroAlmacenController@eliminar')->name('Eliminar.Almacen')->middleware('permission:contenidos.delete,/modulo/Almacen/RAlmacen');



Route::get('/modulo/Almacen/RAlmacen/actualizarDatos/{id}','Almacen_Controlador\RegistroAlmacenController@ActualizarDatos')->
     name('ActualizarDatos.Almacen')->middleware('permission:contenidos.edit,/modulo/Almacen/RAlmacen');


Route::post('/modulo/Almacen/RAlmacen/update/{id}','Almacen_Controlador\RegistroAlmacenController@update')->name('Update.Almacen')->middleware('permission:contenidos.edit,/modulo/Almacen/RAlmacen');


Route::post('/modulo/Almacen/RAlmacen','Almacen_Controlador\RegistroAlmacenController@filtro')->name('Filtro.Almacen');
/*********************************************************************************/
Route::middleware('auth:sanctum')->get('/user/permissions', function (Request $request) {
       return $request->user()->getAllPermissions()->pluck('name');
});

//Rutas de Almacen - Articulos
/*********************************************************************************/
Route::get('/modulo/Almacen/Artculos','Almacen_Controlador\RegistroArticulosController@index')->name('listadoArticulos.Almacen')->middleware('permission:contenidos.index-,/modulo/Almacen/Artculos');
Route::get('/modulo/Almacen/Artculos/getinformation','Almacen_Controlador\RegistroArticulosController@informationArticulos')->name('infolistadoArticulos.Almacen');
Route::get('/modulo/Almacen/Artculos/getpermiso','Almacen_Controlador\RegistroArticulosController@getpermiso')->name('getpermiso');
Route::get('/modulo/Almacen/Artculos/getContabb', 'Almacen_Controlador\RegistroArticulosController@getContabb')->name('getContabb.Almacen');
Route::get('/modulo/Almacen/Artculos/getContabb1', 'Almacen_Controlador\RegistroArticulosController@getContabb1')->name('getContabb1.Almacen');
Route::get('/modulo/Almacen/Artculos/getCideftit', 'Almacen_Controlador\RegistroArticulosController@getCideftit')
	->name('getCideftit');
Route::get('/modulo/Almacen/Artculos/getPartida', 'Almacen_Controlador\RegistroArticulosController@getPartida')->name('getPartida.Almacen');

Route::get('/modulo/Almacen/Artculos/filtro','Almacen_Controlador\RegistroArticulosController@filtro')->name('listadoLLamadoArticulos.Almacen');
Route::get('/modulo/Almacen/Artculos/filtroTexto','Almacen_Controlador\RegistroArticulosController@filtroTexto')->name('listadoLLamadoArticulos.Almacen');
Route::post('/modulo/Almacen/Artculos/multipleupdate','Almacen_Controlador\RegistroArticulosController@multiple_update')->name('multiple_accion.Almacen');

Route::get('/modulo/Almacen/Artculos/registro','Almacen_Controlador\RegistroArticulosController@registrarArticulo')->name('RegistroArticulos.Almacen')->middleware('permission:contenidos.create-,/modulo/Almacen/Artculos');
Route::get('/modulo/Almacen/Artculos/getdetalle/{id?}','Almacen_Controlador\RegistroArticulosController@detalleArticulo')->name('getdetalle.Almacen');

Route::post('/modulo/Almacen/Artculos/registrar','Almacen_Controlador\RegistroArticulosController@RegistroArticulos')->name('RegistrarArticulos.Almacen')->middleware('permission:contenidos.create-,/modulo/Almacen/Artculos');

Route::get('/modulo/Almacen/Artculos/precio','Almacen_Controlador\RegistroArticulosController@preciosArticulo')->name('RegistroPrecio.Almacen')->middleware('permission:contenidos.create-,/modulo/Almacen/Artculos');

Route::get('/modulo/Almacen/Artculos/update/{id}','Almacen_Controlador\RegistroArticulosController@updateArticulo')->name('updateArticulos.Almacen')->middleware('permission:contenidos.edit-,/modulo/Almacen/Artculos');

Route::post('/modulo/Almacen/Artculos/editarArticulo/{id}','Almacen_Controlador\RegistroArticulosController@actualizarArticulo')->name('editarArticulos.Almacen')->middleware('permission:contenidos.edit-,/modulo/Almacen/Artculos');

Route::post('/modulo/Almacen/Artculos/actualizado','Almacen_Controlador\RegistroArticulosController@IngresarpreciosArticulos')->name('ingresarPrecios.Almacen')->middleware('permission:contenidos.create-,/modulo/Almacen/Artculos');

Route::get('/modulo/Almacen/Artculos/eliminar/{id?}','Almacen_Controlador\RegistroArticulosController@deleteArticulo')->name('eliminarArticulos.Almacen')->middleware('permission:contenidos.delete-,/modulo/Almacen/Artculos');

Route::get('/modulo/Almacen/Artculos/filtrosprecios/{id?}','Almacen_Controlador\RegistroArticulosController@filtrosprecios')
       ->name('filtroPrecios.Almacen');

Route::get('/modulo/Almacen/Artculos/visualizarArticulos/{id}','Almacen_Controlador\RegistroArticulosController@ingresarPrecio')->name('ingresarPrecio.Almacen')->middleware('permission:contenidos.index-,/modulo/Almacen/Artculos');
Route::post('/modulo/Almacen/Artculos/visualizarArticulos/{id}','Almacen_Controlador\RegistroArticulosController@ingresarPrecioP')->name('ingresarPrecioP.Almacen')->middleware('permission:contenidos.index-,/modulo/Almacen/Artculos');

Route::get('/modulo/Almacen/Artculos/asoc/{id?}','Almacen_Controlador\RegistroArticulosController@mostrarAsoc')->name('listadoArticulos.Almacenista')->middleware('permission:contenidos.index-,/modulo/Almacen/Artculos');

Route::get('/modulo/Almacen/Artculos/asocTallas/{id?}','Almacen_Controlador\RegistroArticulosController@getTallas')->name('listadoArticulos.getTallas')->middleware('permission:contenidos.index-,/modulo/Almacen/Artculos');
Route::get('/modulo/Almacen/Artculos/searchCode/{id?}','Almacen_Controlador\RegistroArticulosController@searchCode')->name('search.code');


//Almacén Tipo de Movimiento
/*********************************************************************************/
Route::get('/modulo/Almacen/TdeMovimiento','Almacen_Controlador\MovimientosInventarioController@index')->name('movimientos.inventario.almacen')->middleware('permission:contenidos.index-,/modulo/Almacen/TdeMovimiento');

Route::get('/modulo/Almacen/TdeMovimiento/nuevoTipoMovimiento','Almacen_Controlador\MovimientosInventarioController@nuevoTipoMovimiento')->name('nuevoTipoMovimiento')->middleware('permission:contenidos.create-,/modulo/Almacen/TdeMovimiento');

Route::post('/modulo/Almacen/TdeMovimiento/registrarTdeMovimiento','Almacen_Controlador\MovimientosInventarioController@create')->name('registrarTdeMovimiento');

Route::get('/modulo/Almacen/TdeMovimiento/editarTdeMovimientoForm/{id}','Almacen_Controlador\MovimientosInventarioController@edit')->name('editarTdeMovimientoForm')->middleware('permission:contenidos.edit-,/modulo/Almacen/TdeMovimiento');

Route::post('/modulo/Almacen/TdeMovimiento/editarTdeMovimiento/{id}','Almacen_Controlador\MovimientosInventarioController@update')->name('actualizartipmovimiento');

Route::get('/modulo/Almacen/TdeMovimiento/eliminarTipoMovimiento/{id}','Almacen_Controlador\MovimientosInventarioController@delete')->name('eliminarTipoMovimiento')->middleware('permission:contenidos.delete-,/modulo/Almacen/TdeMovimiento');

Route::post('/modulo/Almacen/TdeMovimiento/buscarMovimiento','Almacen_Controlador\MovimientosInventarioController@filtro')->name('buscarMovimiento');

// Route::get('/modulo/Almacen/TdeMovimiento/registro','Almacen_Controlador\MovimientosInventarioController@registro')->name('entradaSalida.almacen');

Route::get('/modulo/Almacen/searchAlmacen', 'Entrada_controlador\EntradaController@searchAlmacen')->name('searchAlmacen');


//modulo de entrada y salida
/************************************************************************************************/
Route::get('/modulo/Almacen/EySalida/Entrada','Entrada_controlador\EntradaController@index')->name('EntradaYsalida')->middleware('permission:contenidos.index-,/modulo/Almacen/EySalida/Entrada');
Route::get('/modulo/Reportes/ESeriales','Entrada_controlador\EntradaController@GenerarPdfSeriales')->name('EReportes')->middleware('permission:contenidos.index-,/modulo/Reportes/ESeriales');
Route::post('/modulo/Reportes/ESeriales','Entrada_controlador\EntradaController@GenerarPdfSerialesReportes')->name('EReportespdf')->middleware('permission:contenidos.index-,/modulo/Reportes/ESeriales');

Route::get('/modulo/Almacen/EySalida/getDetalle/{codmov?}','Entrada_controlador\EntradaController@getDetalle')->name('EntSalDetalle');

 Route::get('/modulo/Almacen/EySalida/Entrada/Create','Entrada_controlador\EntradaController@createEntrada')->name('CreateEntrada')->middleware('permission:contenidos.create-,/modulo/Almacen/EySalida/Entrada');

 Route::post('/modulo/Almacen/EySalida/RegistroEntrada','Entrada_controlador\EntradaController@saveEntrada')->name('SaveEntrada');

 Route::get('/modulo/Almacen/EySalida/ListadoEntrada','Entrada_controlador\EntradaController@ListadoEntrada')->name('ListadoEntrada')->middleware('permission:contenidos.index-,/modulo/Almacen/EySalida/ListadoEntrada');

 Route::post('/modulo/Almacen/EySalida/Entrada','Entrada_controlador\EntradaController@filtro')->name('filtroEntradas')->middleware('permission:contenidos.index-,/modulo/Almacen/EySalida/Entrada');

 Route::get('/modulo/Almacen/EySalida/sendMail/{codmov}','Entrada_controlador\EntradaController@sendMail')->name('entradaMail')->middleware('permission:contenidos.index-,/modulo/Almacen/EySalida/Entrada');

 Route::post('/modulo/Almacen/EySalida/{url}/ajax','Entrada_controlador\EntradaController@ajax')->name('actualizarRefEntrada');

 Route::post('/modulo/Almacen/EySalida/cargaSerial','Entrada_controlador\EntradaController@cargaSerial')->name('cargaSerial');


 /* GET ARTICULO */
Route::get('/modulo/Almacen/EySalida/getartjson/{codart?}/{codalm?}/{movinvet?}','Entrada_controlador\EntradaController@getArtInfo')->name('getArtInfo');
Route::get('/modulo/Facturacion/searchArt','Entrada_controlador\EntradaController@searchArt')->name('detallar');
Route::get('/modulo/Facturacion/searchSeriales','Entrada_controlador\EntradaController@searchSeriales')->name('searchSeriales');

// ANULAR O HACER RECEPCION DE MOVIMIENTO
Route::get('/modulo/Almacen/EySalida/AprobarMov/{codmov?}','Entrada_controlador\EntradaController@aprobMov')->name('aprobMov');

Route::get('/modulo/Almacen/EySalida/AnularMov/{codmov?}/{desanu?}','Entrada_controlador\EntradaController@anularMov')->name('anularMov');

Route::get('/modulo/Almacen/EySalida/getListArt/{codalm?}/{movinvent?}','Entrada_controlador\EntradaController@getListArt')->name('getListArt');
Route::get('/modulo/Almacen/EySalida/queryCodart','Entrada_controlador\EntradaController@queryCodart')->name('queryCodart');

/* Route::get('/modulo/Almacen/EySalida/getartjson/{alm?}','Entrada_controlador\EntradaController@getalmubi')->name('getalmubi'); */
/************************************************************************************************/
//Traspasos


Route::post('/modulo/Almacen/Traspaso/ajax/','TraspasoController@ajax')->name('traspasoAjax');
Route::get('/modulo/Almacen/Traspaso/getArtInfo/{codart?}/{almori?}/{almdes?}','TraspasoController@getArtTra')->name('getArtInfoTr');

// Route::post('modulo/Almacen/Traspaso/ajax','TraspasoController@ajax')->name('traspasoAjax');

Route::get('/modulo/Almacen/Traspaso','TraspasoController@index')->name('traspasosAlmacen')
->middleware('permission:contenidos.index-,/modulo/Almacen/Traspaso');
Route::get('/modulo/Almacen/Traspaso/Create','TraspasoController@create')->name('traspasosAlmacenCreate')->middleware('permission:contenidos.create-,/modulo/Almacen/Traspaso');
Route::post('/modulo/Almacen/Traspaso/Save','TraspasoController@store')->name('traspasosAlmacenSave');
Route::get('/modulo/Almacen/Traspaso/Aprobar/{codmov?}','TraspasoController@recepcionTras')->name('traspasosAlmacenAprob');
Route::get('/modulo/Almacen/Traspaso/getDetalle/{codmov?}','TraspasoController@getDetalle')->name('trgetDetalle');
Route::get('/modulo/Almacen/Traspaso/Anular/{codmov}/{desanu?}','TraspasoController@anularTras')->name('traspasosAlmacenAnul');
Route::get('/modulo/Almacen/Traspaso/SendMail/{codmov}','TraspasoController@sendMail')->name('sendMail');


Route::get('modulo/Almacen/Traspaso/EmailAction/{codmov}/{accion}','TraspasoController@emailAction')->name('emailAction');
Route::post('modulo/Almacen/Traspaso/EmailAction/Login/{codmov}/{accion}','TraspasoController@emailAction')->name('emailLogin');

Route::get('modulo/Almacen/EmailChoise/{codmov}/{mov}','EmailActionController@putChoise')->name('emailChoise');
Route::get('modulo/Almacen/EmailAction/APR/{codmov}/{mov}','EmailActionController@aprobarEmail')->name('aprobacionEmail');
Route::get('modulo/Almacen/EmailAction/REC/{codmov}/{mov}/{desanu?}','EmailActionController@anularEmail')->name('anulacionEmail');


/* Estatus Iventario */
Route::get('/modulo/Almacen/Inventario','EstatusInventarioController@index')->name('inventarioView')->middleware('permission:contenidos.index-,/modulo/Almacen/Inventario');
Route::post('/modulo/Almacen/Inventario/Estatus','EstatusInventarioController@view')->name('inventarioEstatus')->middleware('permission:contenidos.index-,/modulo/Almacen/Inventario');
//Route::post('/modulo/Almacen/Inventario/Filtrar','EstatusInventarioController@view')->name('inventarioFiltroArt');
Route::get('/modulo/Almacen/Inventario/ajax','EstatusInventarioController@ajax')->name('inventarioAjax');

//UBICACIONES
// Route::get('/modulo/Almacen/Ubicacion/getArtInfo/{codart?}/{almori?}/{almdes?}','UbicacionController@getArtTra')->name('ubi');
Route::get('/modulo/Almacen/Ubicaciones','UbicacionesAlm\UbiAlmController@index')->name('ubicacionesList')->middleware('permission:contenidos.index-,/modulo/Almacen/Ubicaciones');
Route::get('/modulo/Almacen/Ubicaciones/Create','UbicacionesAlm\UbiAlmController@create')->name('ubiCreate')->middleware('permission:contenidos.create-,/modulo/Almacen/Ubicaciones');
Route::post('/modulo/Almacen/Ubicaciones/Guardar','UbicacionesAlm\UbiAlmController@store')->name('ubiSave');
Route::get('/modulo/Almacen/Ubicaciones/Mostrar/{id}','UbicacionesAlm\UbiAlmController@show')->name('ubiShow')->middleware('permission:contenidos.index-,/modulo/Almacen/Ubicaciones');
Route::post('/modulo/Almacen/Ubicaciones/Editar/{idubi}/{almubi}','UbicacionesAlm\UbiAlmController@update')->name('ubiEdit')->middleware('permission:contenidos.edit-,/modulo/Almacen/Ubicaciones');
Route::get('/modulo/Almacen/Ubicaciones/Eliminar/{id}','UbicacionesAlm\UbiAlmController@destroy')->name('ubiDelete')->middleware('permission:contenidos.delete-,/modulo/Almacen/Ubicaciones');
Route::get('/modulo/Almacen/Ubicaciones/Filtrar','UbicacionesAlm\UbiAlmController@filtro')->name('ubicacionFind');

/******Almacen - NOTA DE ENTREGA *****************************/
Route::get('/modulo/Almacen/NdEntrega','NotaEntregaController@index')->name('notaEntregaListado')->middleware('permission:contenidos.index-,/modulo/Almacen/NdEntrega');
Route::get('/modulo/Almacen/NdEntrega/nuevaNota','NotaEntregaController@create')->name('nuevaNotaEntrega')->middleware('permission:contenidos.create-,/modulo/Almacen/NdEntrega');
Route::post('/modulo/Almacen/NdEntrega/getArticulos', 'NotaEntregaController@getArticulos')->name('getArticulos');
Route::post('/modulo/Almacen/NdEntrega/guardarNota','NotaEntregaController@store')->name('guardarNota')->middleware('permission:contenidos.create-,/modulo/Almacen/NdEntrega');
Route::get('/modulo/Almacen/NdEntrega/Detalle/{id}','NotaEntregaController@detalles')->name('detalleNota')->middleware('permission:contenidos.edit-,/modulo/Almacen/NdEntrega');
Route::get('/modulo/Almacen/NdEntrega/{id}','NotaEntregaController@pdfNota')->name('pdfNota')->middleware('permission:contenidos.edit-,/modulo/Almacen/NdEntrega');
Route::get('/modulo/Almacen/NdEntrega','NotaEntregaController@filtro')->name('buscarNota')->middleware('permission:contenidos.index-,/modulo/Almacen/NdEntrega');

Route::get('/modulo/Almacen/NdEntrega/listado/descontar','NotaEntregaController@descontar')->name('descontarArt');



//Configuraccion Generales - Forma de Pago
/************************************************************************************/

Route::get('/modulo/ConfiguracionGenerales/FdePago','RegistroFormaPagoController@listado')->name('listaFormaPago')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/FdePago');

Route::get('/modulo/ConfiguracionGenerales/FdePago/nuevaForma','RegistroFormaPagoController@nuevaForma')->name('nuevaForma')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/FdePago');

Route::post('/modulo/ConfiguracionGenerales/FdePago/registrarFormaPago','RegistroFormaPagoController@create')->name('registrarFormaPago')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/FdePago');

Route::get('/modulo/ConfiguracionGenerales/FdePago/editarFormaPagoForm/{id}','RegistroFormaPagoController@edit')->name('editarFormaPagoForm')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/FdePago');;

Route::post('/modulo/ConfiguracionGenerales/FdePago/editarFormaPago/{id}','RegistroFormaPagoController@update')->name('actualizarformapago')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/FdePago');;

Route::get('/modulo/ConfiguracionGenerales/FdePago/eliminarFormaPago/{id}','RegistroFormaPagoController@delete')->name('eliminarFormaPago')->middleware('permission:contenidos.delete-,/modulo/ConfiguracionGenerales/FdePago');

Route::post('/modulo/ConfiguracionGenerales/FdePago','RegistroFormaPagoController@filtro')->name('buscarFormaPago');

Route::get('/modulo/ConfiguracionGenerales/FdePago/registroForma','RegistroFormaPagoController@nuevaForma')->name('registroForma')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/FdePago');

/************************************************************************************/

//Configuraccion Generales - Recargo
/************************************************************************************/
Route::get('/modulo/ConfiguracionGenerales/Recargo','RegistroRecargoController@listado')->name('listaRecargo')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Recargo');

Route::get('/modulo/ConfiguracionGenerales/Recargo/nuevoRecargo','RegistroRecargoController@nuevoRecargo')->name('nuevoRecargo')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Recargo');;

Route::post('/modulo/ConfiguracionGenerales/Recargo/registrarRecargo','RegistroRecargoController@create')->name('registrarRecargo')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Recargo');

Route::get('/modulo/ConfiguracionGenerales/Recargo/editarRecargoForm/{id}','RegistroRecargoController@edit')->name('editarRecargoForm')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Recargo');;

Route::post('/modulo/ConfiguracionGenerales/Recargo/editarRecargo/{id}','RegistroRecargoController@update')->name('actualizarrecargo')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Recargo');;

Route::get('/modulo/ConfiguracionGenerales/Recargo/eliminarRecargo/{id}','RegistroRecargoController@delete')->name('eliminarRecargo')->middleware('permission:contenidos.delete-,/modulo/ConfiguracionGenerales/Recargo');

Route::post('/modulo/ConfiguracionGenerales/Recargo','RegistroRecargoController@filtro')->name('buscarRecargo');
/************************************************************************************/

//Configuracion Generales - Descuento
/***********************************************************************************/
Route::get('/modulo/ConfiguracionGenerales/Descuento','RegistroDescuentoController@listado')->name('listaDescuento')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Descuento');;

Route::get('/modulo/ConfiguracionGenerales/Descuento/nuevoDescuento','RegistroDescuentoController@nuevoDescuento')->name('nuevoDescuento')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Descuento');

Route::post('/modulo/ConfiguracionGenerales/Descuento/registrarDescuento','RegistroDescuentoController@create')->name('registrarDescuento')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Descuento');;

Route::get('/modulo/ConfiguracionGenerales/Descuento/editarDescuentoForm/{id}','RegistroDescuentoController@edit')->name('editarDescuentoForm')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Descuento');

Route::post('/modulo/ConfiguracionGenerales/Descuento/editarDescuento/{id}','RegistroDescuentoController@update')->name('actualizardescuento')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Descuento');

Route::get('/modulo/ConfiguracionGenerales/Descuento/eliminarDescuento/{id}','RegistroDescuentoController@delete')->name('eliminarDescuento')->middleware('permission:contenidos.delete-,/modulo/ConfiguracionGenerales/Descuento');;

Route::post('/modulo/ConfiguracionGenerales/Descuento','RegistroDescuentoController@filtro')->name('buscarDescuento');
/***********************************************************************************/
//quede aqui
//Seguridad - Usuarios
/***********************************************************************************/

Route::get('/modulo/Seguridad/Usuario','UsuariosController@listado')->name('listaUsuario')->middleware('permission:contenidos.index-,/modulo/Seguridad/Usuario');

Route::get('/modulo/Seguridad/Usuario/nuevoUsuario','UsuariosController@nuevoUsuario')->name('nuevoUsuario')->middleware('permission:contenidos.create-,/modulo/Seguridad/Usuario');

Route::post('/modulo/Seguridad/Usuario/ajax','UsuariosController@ajax')->name('buscarDatos');

Route::post('/modulo/Seguridad/Usuario/registrarUsuario','UsuariosController@create')->name('registrarUsuario')
      ->middleware('permission:contenidos.create-,/modulo/Seguridad/Usuario');



Route::get('/modulo/Seguridad/Usuarios/editarUsuarioForm/{id}','UsuariosController@edit')->name('editarUsuarioForm')->middleware('permission:contenidos.edit-,/modulo/Seguridad/Usuario');
#Route::get('/modulo/Seguridad/Usuario/editarUsuarioForm/{id}','UsuariosController@edit')->name('editarUsuarioForm');
Route::post('/modulo/Seguridad/Usuario/editarUsuarios/{id}','UsuariosController@update')->name('actualizarusuarios')->middleware('permission:contenidos.edit-,/modulo/Seguridad/Usuario');
Route::post('/modulo/Seguridad/Usuario/buscarUsuario','UsuariosController@filtro')->name('buscarUsuario2');

/***********************************************************************************/


// Route::get('/modulo/Seguridad/Usuario/editarUsuarioForm/{id}','UsuariosController@edit')->name('editarUsuarioForm')->middleware('permission:contenidos.edit,/modulo/Seguridad/Usuario');

Route::post('/modulo/Seguridad/Usuario/editarUsuarios/{id}','UsuariosController@update')->name('actualizarusuarios')->middleware('permission:contenidos.edit-,/modulo/Seguridad/Usuario');

Route::post('/modulo/Seguridad/Usuario','UsuariosController@filtro')->name('buscarUsuario');

/***********************************************************************************/

//seguridad - Parametrizacion

/***********************************************************************************/

Route::get('/modulo/Seguridad/Parametrizacion','ParametrizacionController@index')->name('Parametrizacion')->middleware('permission:contenidos.index-,/modulo/Seguridad/Parametrizacion');

Route::post('/modulo/Seguridad/Parametrizacion','ParametrizacionController@store')->name('Parametrizacion.store')->middleware('permission:contenidos.index-,/modulo/Seguridad/Parametrizacion');


/***********************************************************************************/

//seguridad - permisologia

/***********************************************************************************/

	//falta en la tabla permission_user que ingrese en campo codigoid y codsuc
	Route::get('/modulo/Seguridad/Permisos','permisos\PermisologiaController@index')->name('list.permisos')
	->middleware('permission:contenidos.index-,/modulo/Seguridad/Permisos');

	Route::post('/modulo/Seguridad/Permisos','permisos\PermisologiaController@filtro')->name('Buscarr');

	Route::get('/modulo/Seguridad/Permisos/rolPermisos/{id}','permisos\PermisologiaController@addRolPermisos')->name('addRolPermisos')->middleware('permission:contenidos.create-,/modulo/Seguridad/Permisos');

	Route::get('/modulo/Seguridad/Permisos/rolPermisos/Asig/{id?}','permisos\PermisologiaController@addPermisos')->name('modPer')->middleware('permission:contenidos.create-,/modulo/Seguridad/Permisos');

	Route::post('/modulo/Seguridad/Permisos/rPermisos/{id}','permisos\PermisologiaController@rPermisos')
	->name('rPermisos')->middleware('permission:contenidos.create-,/modulo/Seguridad/Permisos');

	Route::get('/modulo/Seguridad/Permisos/rolPermisos/search/{id}','permisos\PermisologiaController@Search')->name('search')->middleware('permission:contenidos.index-,/modulo/Seguridad/Permisos');

    Route::get('/modulo/Seguridad/Permisos/rolPermisos/delete','permisos\PermisologiaController@delete')->name('delete2')->middleware('permission:contenidos.delete-,/modulo/Seguridad/Permisos');


	Route::get('/modulo/Seguridad/Permisos/rolPermisos/Admin/{id?}','permisos\PermisologiaController@Admin')->name('admin')->middleware('permission:contenidos.index-,/modulo/Seguridad/Permisos');

	Route::post('/modulo/Seguridad/Permisos/rolPermisos/regAdmin/{id?}/{id2?}','permisos\PermisologiaController@regAdmin')->name('regAdmin')->middleware('permission:contenidos.index-,/modulo/Seguridad/Permisos');

	Route::post('/modulo/Seguridad/Permisos/rolPermisos/delPermisos/{id?}','permisos\PermisologiaController@delPermisos')->name('delPermisos');

	Route::get('/modulo/Seguridad/Permisos/rolPermisos/vacio/{id?}/{id3?}','permisos\PermisologiaController@vacio')->name('vacio');

	Route::get('/modulo/Seguridad/Permisos/rolPermisos/delete/{id?}/{id3?}','permisos\PermisologiaController@delete')->name('delete');
/***********************************************************************************/


//***Reportes********************* */
/**********Cliente************ */
Route::get('/modulo/Reportes/Clientes','ClienteController@vistaReporte')->name('reportesClientes')->middleware('permission:contenidos.index-,/modulo/Reportes/Clientes');
Route::post('/modulo/Reportes/Clientes','ClienteController@reporteCliente')->name('generarReporteCliente')->middleware('auth');
/******Presupuestos****** */
Route::get('/modulo/Reportes/Presupuestos','PresupuestoController@vistaReporte')->name('reportesPresupuestos')->middleware('permission:contenidos.index-,/modulo/Reportes/Presupuestos');
Route::post('/modulo/Reportes/Presupuestos','PresupuestoController@reportePresupuesto')->name('generarReportePresupuesto')->middleware('auth');

Route::get('/modulo/Reportes/Fdetalladas','FacturacionController@vistaReporte')->name('reportesFacturas')->middleware('permission:contenidos.index-,/modulo/Reportes/Fdetalladas');

Route::post('/modulo/Reportes/Fdetalladas','FacturacionController@reporteFactura')->name('generarReporteFactura')->middleware('auth');

Route::get('/modulo/Reportes/Pvendidos','ProductoVendidoController@vistaReporte')->name('reportesProductosV')->middleware('permission:contenidos.index-,/modulo/Reportes/Pvendidos');

Route::post('/modulo/Reportes/Pvendidos','ProductoVendidoController@reporteProductoV')->name('generarReporteProductoV')->middleware('auth');

Route::post('/modulo/Reportes/Pvendidos22','ProductoVendidoController@reporteProductoV22')->name('reporteProductoV22')->middleware('auth');

Route::get('/modulo/Reportes/RPvendidos','ProductoVendidoController@vistaReporteP')->name('reportesRPvendidos')->middleware('permission:contenidos.index-,/modulo/Reportes/RPvendidos');

Route::post('/modulo/Reportes/RPvendidos','ProductoVendidoController@reporteRProductoV')->name('generarResumenProductoV')->middleware('auth');

Route::get('/modulo/Reportes/Provendidos2','ProductoVendidoController@vistaReporteVS')->name('reportesProductosV2')->middleware('permission:contenidos.index-,/modulo/Reportes/Pvendidos');

Route::post('/modulo/Reportes/Provendidos2','ProductoVendidoController@reporteProductoVS')->name('generarReporteProductoV2')->middleware('auth');

Route::get('/modulo/Reportes/LdeVentas','LibroVentasController@index')->name('libroVentas')->middleware('permission:contenidos.index-,/modulo/Reportes/LdeVentas');
Route::post('/modulo/Reportes/LdeVentas','LibroVentasController@generarLibro')->name('generarLibroV');

//Route::get('/modulo/Reportes/LibrodeVentas2','LibroVentasController@index2')->name('libroVentas')->middleware('permission:contenidos.index-,/modulo/Reportes/LdeVentas');
//Route::post('/modulo/Reportes/LibrodeVentas2','LibroVentasController@generarLibro2')->name('generarLibroV2');

Route::get('/modulo/Reportes/EAlmacen','Entrada_controlador\EntradaController@vistaReporteEntrada')->name('reporteEAlmacen')->middleware('permission:contenidos.index-,/modulo/Reportes/EAlmacen');
Route::post('/modulo/Reportes/EAlmacen','Entrada_controlador\EntradaController@reporteEntrada')->name('generarReporteEntrada');

Route::get('/modulo/Reportes/SAlmacen','Entrada_controlador\EntradaController@vistaReporteSalida')->name('reporteSAlmacen')->middleware('permission:contenidos.index-,/modulo/Reportes/SAlmacen');
Route::post('/modulo/Reportes/SAlmacen','Entrada_controlador\EntradaController@reporteSalida')->name('generarReporteSalida');

Route::get('/modulo/Reportes/Traspasos','TraspasoController@vistaReporte')->name('reportesTraspasos')->middleware('permission:contenidos.index-,/modulo/Reportes/Traspasos');
Route::post('/modulo/Reportes/Traspasos','TraspasoController@generarReporte')->name('generarReporteTraspaso')->middleware('auth');

Route::get('/modulo/Reportes/ArtCategorias', 'CProductoController@vistaReporte')->name('vistaArtCategorias')->middleware('permission:contenidos.index-,/modulo/Reportes/ArtCategorias');
Route::post('/modulo/Reportes/ArtCategorias/Reporte', 'CProductoController@generarReporte')->name('generarArtCategorias')->middleware('permission:contenidos.index-,/modulo/Reportes/ArtCategorias');

//Route::post('/modulo/TFL/Reportes/Traspasos','TraspasoController@generarReporte')->name('generarReporteTraspasoFromList');

Route::get('/modulo/Reportes/Traspasos/{id}','TraspasoController@getReporteMail')->name('generarReporteTraspasoMail');

Route::get('/modulo/Reportes/ERAlmacen','EstatusInventarioController@vistaReporte')->name('reportesERAlmacen');
Route::post('/modulo/Reportes/ERAlmacen','EstatusInventarioController@generarReporte')->name('generarReporteERAlmacen');

Route::get('/modulo/Reportes/Minventario','Almacen_Controlador\MovimientosInventarioController@vistaReporte')->name('reportesMinventario')->middleware('permission:contenidos.index-,/modulo/Reportes/Minventario');

Route::post('/modulo/Reportes/Minventario','Almacen_Controlador\MovimientosInventarioController@generarReporte')->name('generarReporteMinventario')->middleware('auth');

Route::get('/modulo/Reportes/getFecha','LibroVentasController@buscarFecha')->name('getFecha');
Route::get('/modulo/Reportes/getCajasP','LibroVentasController@buscarCajas')->name('getCajasPre');

/* Cierre de Caja */

Route::get('/modulo/Reportes/CierreCaja','FacturacionController@getCierreCajaView')->name('CierreCajaView')->middleware('permission:contenidos.index-,/modulo/Reportes/CierreCaja');
Route::post('/modulo/Reportes/CierreCaja','FacturacionController@getCierreCaja')->name('CierreCaja')->middleware('auth');



////////////////////////////////////  COMPRAS / LISTADO RESUMEN ORDENES COMPRAS  //////////////////////////////////////////////////


Route::get('/modulo/Reportes/ListadoResumenOrdenesCompras','OrdenesController@ListadoView')->name('ListadoResumenOrdenesCompras')->middleware('permission:contenidos.index-,/modulo/Reportes/ListadoResumenOrdenesCompras');
Route::post('/modulo/Reportes/ListadoResumenOrdenesCompras/ReporteCompra','OrdenesController@reporteCompra')->name('generarReporteCompra')->middleware('auth');
Route::post('/modulo/Reportes/ListadoResumenOrdenesCompras/ReporteServicio','OrdenesController@reporteServicio')->name('generarReporteServicio')->middleware('auth');

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Seguridad - Cambiar Contraseña
/**********************************************************************************/
Route::get('/modulo/Seguridad/CambPassword/editarContrasenaForm/{id}','UsuariosController@editContrasena')->name('editarContrasenaForm')->middleware('permission:contenidos.index-,/modulo/Seguridad/CambPassword');
Route::get('/modulo/Seguridad/CambPassword','UsuariosController@editPassw')->name('editarPasswForm')->name('editarContrasenaForm')->middleware('permission:contenidos.edit-,/modulo/Seguridad/CambPassword');
Route::post('/modulo/Seguridad/CambPassword/editarPassw/{id}','UsuariosController@updatePassw')->name('actualizarPassw');
/**********************************************************************************/

//Almacen - Escala
/*********************************************************************************/
Route::get('/modulo/Almacen/Escala','Almacen_Controlador\EscalaController@listado')->name('listaEscala')->middleware('permission:contenidos.index-,/modulo/Almacen/Escala');
Route::get('/modulo/Almacen/Escala/nuevaEscala','Almacen_Controlador\EscalaController@nuevaEscala')->name('nuevaEscala')->middleware('permission:contenidos.create-,/modulo/Almacen/Escala');
Route::post('/modulo/Almacen/Escala/ajax','Almacen_Controlador\EscalaController@ajax')->name('buscarTallas');
Route::post('/modulo/Almacen/Escala/registrarEscala','Almacen_Controlador\EscalaController@create')->name('registrarEscala');
Route::get('/modulo/Almacen/Escala/editarEscalaForm/{id}','Almacen_Controlador\EscalaController@edit')->name('editarEscalaForm')->middleware('permission:contenidos.edit-,/modulo/Almacen/Escala');
Route::post('/modulo/Almacen/Escala/editarEscala/{id}','Almacen_Controlador\EscalaController@update')->name('actualizarEscala');
Route::get('/modulo/Almacen/Escala/eliminarEscala/{id}','Almacen_Controlador\EscalaController@delete')->name('eliminarEscala')->middleware('permission:contenidos.delete-,/modulo/Almacen/Escala');
Route::post('/modulo/Almacen/Escala/buscarEscala','Almacen_Controlador\EscalaController@filtro')->name('buscarEscala');
/********************************************************************************/

//Config. Producto - Talla
/********************************************************************************/
Route::get('/modulo/ConfiguracionGenerales/Talla','TallaController@listado')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Talla')->name('listaTalla');

Route::get('/modulo/ConfiguracionGenerales/nuevaTalla','TallaController@nuevaTalla')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Talla')->name('nuevaTalla');

Route::post('/modulo/ConfiguracionGenerales/registrarTalla','TallaController@create')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Talla')->name('registrarTalla');

Route::get('/modulo/ConfiguracionGenerales/edit/{id}','TallaController@edit')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Talla')->name('editTalla');
Route::post('/modulo/ConfiguracionGenerales/editarTalla/{id}','TallaController@editarTalla')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Talla')->name('editarTalla');

Route::get('/modulo/ConfiguracionGenerales/eliminarTalla/{id}','TallaController@delete')->middleware('permission:contenidos.delete-,/modulo/ConfiguracionGenerales/Talla')->name('eliminarTalla');

Route::post('/modulo/ConfiguracionGenerales/buscarTalla','TallaController@filtro')->name('buscarTalla');
/***********************************************************************************/



Route::get('/modulo/ConfiguracionGenerales/Compras/Retenciones','RetencionesController@index')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Compras/Retenciones')->name('retenciones.index');
Route::get('/modulo/ConfiguracionGenerales/Compras/Retenciones/Create','RetencionesController@create')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Compras/Retenciones/Create')->name('retenciones.create');
Route::get('/modulo/ConfiguracionGenerales/Compras/Retenciones/getData','RetencionesController@getData')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Compras/Retenciones/getData');
Route::get('/modulo/ConfiguracionGenerales/Compras/Retenciones/edit/{id}','RetencionesController@edit')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Compras/Retenciones/edit')->name('retenciones.edit');
Route::post('/modulo/ConfiguracionGenerales/Compras/Retenciones/update/{id}','RetencionesController@update')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Compras/Retenciones/update')->name('retenciones.update');
Route::post('/modulo/ConfiguracionGenerales/Compras/RetencionesStore','RetencionesController@Store')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Compras/RetencionesStore')->name('retenciones.store');
Route::get('/modulo/ConfiguracionGenerales/Compras/Retenciones/{id}','RetencionesController@destroy')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Compras/Retenciones')->name('retenciones.destroy');




//***Reportes********************* */
/**********Cliente************ */
// Route::get('/modulo/Reportes/Clientes','ClienteController@vistaReporte')->name('reportesClientes');
// Route::post('/modulo/Reportes/Clientes','ClienteController@reporteCliente')->name('generarReporteCliente');
/******Presupuestos****** */
// Route::get('/modulo/Reportes/Presupuestos','PresupuestoController@vistaReporte')->name('reportesPresupuestos');
// Route::post('/modulo/Reportes/Presupuestos','HomeController@reportePresupuesto')->name('generarReportePresupuesto');
//


//Modulo de Auditoria
/**********************************************************************************/
Route::get('/modulo/Seguridad/Auditoria','auditoria\AuditoriaController@index')->middleware('permission:contenidos.index-,/modulo/Seguridad/Auditoria')->name('Auditoria.lista');

Route::get('/modulo/Seguridad/Auditoria/actividad/{id?}','auditoria\AuditoriaController@getActividas')->middleware('permission:contenidos.index-,/modulo/Seguridad/Auditoria')->name('Auditoria.actividad');
Route::get('/modulo/Seguridad/Auditoria/filtro','auditoria\AuditoriaController@filtro')->name('filtroAuditoria.lista');

/**********************************************************************************/

//Modulo de Permisologia Por Caja
/**********************************************************************************/

/*Route::group(['middleware' => 'permission:contenidos.index,/modulo/Seguridad/ReiniciarContraCaja'],function()
{*/

Route::get('/modulo/Seguridad/ReiniciarContraCaja','reiniciarCajaController@index')->name('reiniCaja.lista')->middleware('permission:contenidos.index-,/modulo/Seguridad/ReiniciarContraCaja');

Route::get('/modulo/Seguridad/ReiniciarContraCaja/RegistroContraCaja','reiniciarCajaController@create')->name('regCaja.lista')->middleware('permission:contenidos.create-,/modulo/Seguridad/ReiniciarContraCaja');

Route::get('/modulo/Seguridad/ReiniciarContraCaja/CedulaUSer/{id?}','reiniciarCajaController@search')->name('searchCaja.lista');

Route::post('/modulo/Seguridad/ReiniciarContraCaja/CedulaUSer','reiniciarCajaController@registro')->name('registroCaja.lista');

Route::get('/modulo/Seguridad/ReiniciarContraCaja/CedulaUSer/delete/{id?}','reiniciarCajaController@delete')->name('deleteCaja.lista');

Route::get('/modulo/Seguridad/ReiniciarContraCaja/CedulaUSer/update/{id?}','reiniciarCajaController@update')->name('updateCaja.lista');

Route::post('/modulo/Seguridad/ReiniciarContraCaja/CedulaUSer/updateCaja/{id?}','reiniciarCajaController@updateCaja')->name('updatesCaja.lista');

Route::get('/modulo/Seguridad/ReiniciarContraCaja/CedulaUSer/loginCaja/{id?}','reiniciarCajaController@abrirCaja')->name('abrirCaja');


Route::post('/modulo/Seguridad/ReiniciarContraCaja/CedulaUSer/loginCajas/{id?}/{id2?}','reiniciarCajaController@authenticated')->name('loginCaja');

Route::get('/modulo/Seguridad/ReiniciarContraCaja/CedulaUSer/updatedWindows/{id?}','reiniciarCajaController@updatedWindows')->name('updatedWindows');

Route::get('/modulo/Seguridad/ReiniciarContraCaja/CedulaUSer/closeBox/{id?}','reiniciarCajaController@closeBox')->name('closeBox');

Route::post('/modulo/Seguridad/ReiniciarContraCaja','reiniciarCajaController@busqueda')->name('filtro');

#Route::get('/modulo/Seguridad/ReiniciarContraCaja/getUser/{id?}','reiniciarCajaController@getUser')->name('getUser');

/**********************************************************************************/
/*********GRAFICOS******************************************************************/
Route::get('/modulo/Graficos/Vcajas','GraficosVentasController@indexVentasCajas')->name('GVentasCajas')->middleware('permission:contenidos.index-,/modulo/Graficos/Vcajas');

Route::get('/modulo/Graficos/Vcajas/filtro','GraficosVentasController@filtroGVCaja')->name('buscarGVC');

Route::get('/modulo/Graficos/Vsucursales','GraficosVentasController@indexVentasSucursal')->name('GVentasSucursal')->middleware('permission:contenidos.index-,/modulo/Graficos/Vsucursales');

Route::get('/modulo/Graficos/Vsucursales/filtro','GraficosVentasController@filtroGVSucursal')->name('buscarGVS');

Route::get('/modulo/Graficos/Vanuales','GraficosVentasController@indexVentasAnuales')->middleware('permission:contenidos.index-,/modulo/Graficos/Vanuales')->name('GVentasAnuales');

Route::get('/modulo/Graficos/Vanuales/filtro','GraficosVentasController@filtroGVAnual')->name('buscarGVA');

/*});*/

Route::get('/modulo/ConfiguracionGenerales/ModeloMarcas','definirModeloController@index')->name('definirMarcas');


Route::get('/modulo/ConfiguracionGenerales/ModeloMarcas/edit/{id?}','definirModeloController@editModelo')->name('definirMarcas.edit');


Route::post('/modulo/ConfiguracionGenerales/ModeloMarcas/update/{id?}','definirModeloController@updateMarcas')->name('updateMarcas');

Route::get('/modulo/ConfiguracionGenerales/ModeloMarcas/delete/{id?}','definirModeloController@borrarModelo')->name('definirMarcas.delete');

Route::get('/modulo/ConfiguracionGenerales/ModeloMarcas/create','definirModeloController@crear')->name('definirMarcas.crear')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/ModeloMarcas/create');

Route::post('/modulo/ConfiguracionGenerales/ModeloMarcas/create','definirModeloController@crearModelos')->name('definirMarcas.crearMarcas');

Route::get('/modulo/ConfiguracionGenerales/ModeloMarcas/filtro','definirModeloController@filtro')->name('definirMarcas.filtro');

//PRECIOS
Route::get('/modulo/Finanza/Precios','PrecioController@index')->name('precios');

Route::get('/modulo/Finanza/Precios/getlastprecios','PrecioController@getlastprecios')->name('getlastprecios');
Route::get('/modulo/Finanza/Precios/filtro','PrecioController@filtro')->name('buscarPrecios');
Route::post('/modulo/Finanza/Precios/updateprecio','PrecioController@updateprecio')->name('actualizarPrecios');

//COMPRAS

//Requisicion

Route::get('/modulo/Compras/RequisicionP','RequisicionPController@index')->name('requisicionP')->middleware('permission:contenidos.index-,/modulo/Compras/RequisicionP');
Route::get('/modulo/Compras/createRequisicionP','RequisicionPController@create')->name('createRequisicionP')->middleware('permission:contenidos.create-,/modulo/Compras/RequisicionP');
Route::get('/modulo/Compras/createRequisicionP/getData', 'RequisicionPController@getData')->name('getDataReqPriv')->middleware('permission:contenidos.create-,/modulo/Compras/RequisicionP');
Route::get('/modulo/Compras/createRequisicionP/getPendientes', 'RequisicionPController@getPendientes')->name('getPendientes')->middleware('permission:contenidos.create-,/modulo/Compras/RequisicionP');
Route::post('/modulo/Compras/RequisicionP/RequisicionPStore', 'RequisicionPController@store')->name('storeReqPriv')->middleware('permission:contenidos.create-,/modulo/Compras/RequisicionP');
Route::post('/modulo/Compras/RequisicionP/RequisicionPSave', 'RequisicionPController@save')->name('saveReq')->middleware('permission:contenidos.create-,/modulo/Compras/RequisicionP');
Route::get('/modulo/Compras/RequisicionP/RequisicionPDetails/{id}', 'RequisicionPController@show')->name('showReqP')->middleware('permission:contenidos.edit-,/modulo/Compras/RequisicionP');
Route::post('/modulo/Compras/RequisicionP/RequisicionPAction', 'RequisicionPController@action')->name('actionReqP')->middleware('permission:contenidos.edit-,/modulo/Compras/RequisicionP');
Route::post('/modulo/Compras/RequisicionP','RequisicionPController@generarReporteRequisicion')->name('generarReporteRequisicion');


Route::get('/modulo/Compras/RequisicionP/Sendemail', 'RequisicionPController@gerMail')->name('reqMailPriv');

Route::get('/modulo/Compras/RequisicionP/ReenviarReq/{reqart}', 'RequisicionPController@reenviar')->name('reenviarReqPriv');

///////////////////////////////////////////////////////////////////////////////////////////////

// COTIZACIONES

Route::get('/modulo/Compras/Cotizaciones', 'CotizaController@index')->name('cotIndex')->middleware('permission:contenidos.index-,/modulo/Compras/Cotizaciones');
Route::get('/modulo/Compras/Cotizaciones/Create', 'CotizaController@create')->name('cotCreate')->middleware('permission:contenidos.create-,/modulo/Compras/Cotizaciones');
Route::get('/modulo/Compras/Cotizaciones/loadPedidos', 'CotizaController@loadPedidos')->name('cotLoadPed');

//ORDENES

Route::get('/modulo/Compras/Ordenes', 'OrdenesController@index')->name('Ordenes')->middleware('permission:contenidos.index-,/modulo/Compras/Ordenes');
Route::get('/modulo/Compras/Ordenes/show/{id}', 'OrdenesController@show')->name('showOrden')->middleware('permission:contenidos.edit-,/modulo/Compras/Ordenes');
Route::get('/modulo/Compras/Ordenes/Create', 'OrdenesController@create')->name('OrdenesCreate')->middleware('permission:contenidos.create-,/modulo/Compras/Ordenes/Create');
Route::post('/modulo/Compras/OrdenesStore', 'OrdenesController@store')->name('OrdenesStore')->middleware('permission:contenidos.create-,/modulo/Compras/OrdenesStore');
Route::get('/modulo/Compras/Ordenes/getData', 'OrdenesController@getData')->name('getDataOrdenes');
Route::post('/modulo/Compras/Ordenes/getArticulos', 'OrdenesController@getArticulos')->name('getArticulos');
Route::post('/modulo/Compras/Ordenes/OrdenesSave', 'OrdenesController@save')->name('saveOrd')->middleware('permission:contenidos.create-,/modulo/Compras/Ordenes');
Route::get('/modulo/Compras/Ordenes/getPendientes', 'OrdenesController@getPendientes')->name('getPendientesOrd')->middleware('permission:contenidos.create-,/modulo/Compras/Ordenes');
Route::get('/modulo/Compras/Ordenes/getPedidos', 'OrdenesController@getPedidos')->name('getPedidos');
Route::post('/modulo/Compras/OrdenesAction', 'OrdenesController@action')->name('actionOrd')->middleware('permission:contenidos.edit-,/modulo/Compras/Ordenes');
Route::get('/modulo/Compras/OrdenesDestroy/{id}', 'OrdenesController@destroy')->name('destroyOrden')->middleware('permission:contenidos.delete-,/modulo/Seguridad/Permisos');

Route::get('/modulo/Compras/Ordenes/getAlmacen', 'OrdenesController@getAlmacen')->name('getAlmacen');
Route::get('/modulo/Compras/Ordenes/NotaEntrega/{id}', 'OrdenesController@NotaEntrega')->name('GenerarNotaEntrega');
Route::post('/modulo/Compras/Ordenes/getOrden', 'OrdenesController@getOrden')->name('getOrden');


Route::post('/modulo/Compras/OrdenesNotaStore', 'OrdenesController@sendnota')->name('OrdenesNotaStore')->middleware('permission:contenidos.create-,/modulo/Compras/RequisicionP');
Route::post('/modulo/Compras/Ordenes/getArticulosPedidos', 'OrdenesController@getArticulosPedido')->name('getArticulosPedidos');
Route::post('/modulo/Compras/Ordenes', 'OrdenesController@generarReporteOrdenes')->name('generarReporteOrdenes');

///////////////////////////////////////////////////////////////////////////////////////////////

//Proveedores

Route::get('/modulo/ConfiguracionGenerales/Proveedores', 'ProveedorController@index')->name('proveedores')->middleware('permission:contenidos.index-,/modulo/ConfiguracionGenerales/Proveedores');
Route::get('/modulo/ConfiguracionGenerales/Proveedores/ProvCreate', 'ProveedorController@create')->name('provCreate')->middleware('permission:contenidos.create-,/modulo/ConfiguracionGenerales/Proveedores/ProvCreate');
Route::get('/modulo/ConfiguracionGenerales/Proveedores/Edit/{id}', 'ProveedorController@edit')->name('editProv')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Proveedores');
Route::post('/modulo/ConfiguracionGenerales/ProveedoresUpdate/{id}', 'ProveedorController@update')->name('updateProveedor')->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/ProveedoresUpdate');
Route::post('/modulo/ConfiguracionGenerales/ProveedoresStore', 'ProveedorController@store')->name('provStore');
Route::get('/modulo/ConfiguracionGenerales/ProveedoresDestroy/{id}', 'ProveedorController@destroy')->name('destroyProveedor')->middleware('permission:contenidos.delete-,/modulo/Seguridad/Permisos');

///////////////////////////////////////////////////////////////////////////////////////////////

Route::get('/ChangeStatus','Almacen_Controlador\RegistroArticulosController@changeStatusArt')->name('changeStatusArticulo');


Route::get('/modulo/Reportes/CatalogoProductos','CatalogoProductosController@index')->name('catalogoProductos')->middleware('permission:contenidos.index-,/modulo/Reportes/CatalogoProductos');
// Route::post('/modulo/Reportes/LdeVentas','LibroVentasController@generarLibro')->name('generarLibroV');
Route::get('/modulo/Reportes/getSubCateg','CatalogoProductosController@buscarSubCateg')->name('getSubCateg');
/*  */

Route::get('/Registro/Familiares/Fasvit', 'Fasvit\InformacionFamiliarController@create')->name('fasvit.registro');
Route::post('/Registro/Familiares/Fasvit/getEmpleado', 'Fasvit\InformacionFamiliarController@getEmpleado')->name('fasvit.getEmpleado');
// Route::get('/Registro/Familiares/Fasvit/fasvit.edicion/{id}','Fasvit\InformacionFamiliarController@edit')->name('fasvit.edicion');
// ->middleware('permission:contenidos.edit-,/modulo/ConfiguracionGenerales/Clientes');
Route::get('/Registro/Familiares/Fasvit/pdf', 'Fasvit\InformacionFamiliarController@pdfFamilia')->name('fasvit.pdfFamilia');
Route::put('/changeStatusMoneda', 'MonedaController@changeStatusMoneda')->name('changeStatusMoneda');


include ('fasvit_routes.php');
include ('seriales.php');
include ('detalles_factura.php');

Route::get('/filterCliente/sucursal/{id?}', 'ClienteController@FilterClienteSucursal')->name('FilterClienteSucursal');
Route::get('/updateCorrelativo', 'FacturacionController@updateCorrelativo')->name('updateCorrelativo');
Route::get('/modulo/Reportes/ProductosVendidosResumidos', 'ProductoVendidoController@vistaReporte2')->name('updateCorrelativo');
Route::Post('/modulo/Reportes/ProductosVendidosResumidos', 'ProductoVendidoController@vistaReporte3')->name('ProductosVendidosResumidos');
Route::Post('/modulo/Facturacion/cerrar', 'FacturacionController@cerrarTurnos')->name('cerrarTurnos');
Route::Post('/modulo/Facturacion/cierre2', 'FacturacionController@cierre2')->name('cierre2');


Route::get('/test', function () {
   

});

