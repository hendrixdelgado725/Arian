<?php
Route::group(['prefix' => '/modulo/Reportes/LDetalloF'], function(){
    Route::get('/','FacturacionController@DetallesFactura')->name('LDetalloF');
    Route::post('/generarPdf','FacturacionController@generarPdfDetalles')->name('generarPdf')
           ->middleware('auth');
    Route::get('/generarPdf','FacturacionController@generarPdfDetalles')->name('generarPdf')
           ->middleware('auth');

});
?>