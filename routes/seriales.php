<?php 
Route::group(['prefix' => '/modulo/Facturacion'], function(){
    Route::get('Seriales','SerialesController@index')->name('SerialesFilter');
    Route::post('Seriales','SerialesController@filter')->name('Filter');
    Route::get('VisualizarSerial/{id}','SerialesController@VisualizarSerial')->name('VisualizarSerial');
    Route::get('/getSeriales', 'SerialesController@getSeriales')->name('getSeriales')->middleware('permission:contenidos.create-,/modulo/Facturacion/getSeriales');
    Route::get('/getSerialesDisponibles', 'SerialesController@getSerialesDisponibles')->name('getSerialesDisponibles')->middleware('permission:contenidos.create-,/modulo/Facturacion/getSerialesDisponibles');
    Route::post('/getSerialesFiltro', 'SerialesController@filtro')->name('getSerialesFiltro');
    Route::post('/getSerialesFiltroDisponibles', 'SerialesController@filtro2')->name('getSerialesFiltroDisponibles');

    Route::get('DetalleSerial/{id}','SerialesController@detallar')->name('detallar');
    Route::get('Seriales2','Entrada_controlador\EntradaController@getSearchSeriales');
});
 
?>