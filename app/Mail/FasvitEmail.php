<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FasvitEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $nomemp;
    public $codemp;
    public $id;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($object)
    {
        $this->nomemp = $object->nomemp;
        $this->codemp = $object->codemp;
        $this->id = $object->id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('layouts/emails/solicitud_familiares_fasvit')->with([
            'nomemp' => $this->nomemp,
            'codemp' => $this->codemp,
            'id'    => $this->id
        ]);
    }
}
