<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Carbon\Carbon;
use Helper;



class SolicitudTraspasoMail extends Mailable
{
    use Queueable, SerializesModels;



    public $movimiento;
    public $tipo;
    public $articulos;
    public $subject;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($movimiento,$articulos)
    {
        $this->movimiento = $movimiento;
        $this->articulos = $articulos;
        $this->tipo = $this->movimiento->nommov ?? 'TRA';
    }

    /**
     * Build the message.
     *
     * @return $this
     */

    public function getSubject()
    {
        switch ($this->tipo) {
          case 'ENT':
            return "Solicitud de Ingreso a Almacen";
            break;
          
          case 'SAL':
            return "Solicitud de Salida de Almacen";
            break;

            case 'TRA':
            return "Solicitud de Traspaso";
          break;
        }
    }

    public function getText()
    {
        switch ($this->tipo) {
          case 'ENT':
            return "una Solicitud de Ingreso a Almacen";
          
          case 'SAL':
            return "una Solicitud de Salida de Almacen";

            case 'TRA':
            return "una Solicitud de Traspaso";
        }
    }

    public function build()
    {
        $this->from('a1win@admin.com');
        $this->subject($this->getSubject());
        return $this->view('layouts.emails.traspasoEmail')
                ->with(['movimiento' => $this->movimiento,
                        'articulos' => $this->articulos,
                        'subject' => $this->subject,
                        'text' => $this->getText(),
                        'tipo' => $this->tipo,
                        'hora' => $this->movimiento->created_at->format('h:i:s A'),
                        'fechaFormat' => Helper::getSpanishDate($this->movimiento->created_at->format('l j n F Y')
                        )
                      ]);
    }
}
