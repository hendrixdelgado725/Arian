<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;
class Farecarg extends Model implements Auditable
{
    
    use SoftDeletes,\OwenIt\Auditing\Auditable;


    protected $connection='pgsql2';
    protected $table = 'farecarg';

    protected $fillable =['codrgo','nomrgo','tiprgo','monrgo','calcul','codcta','id','status','defecto','created_at','updated_at','deleted_at', 'codsuc_reg', 'codigoid'];

    protected static $logAttributes = ['codrgo','nomrgo','tiprgo','monrgo','calcul'];
    protected static $logName = 'farecarg';
    protected static $logFillable = true;

    public static function VerificarDefecto()
    {
    	return self::where([['defecto','S'],['deleted_at',NULL]])->first();
    }

    public static function ultimoRegistro()
    {
    	return self::where([['defecto','N'],['deleted_at',NULL]])->orderbydesc('id')->first();
    }

    public function sucursalRec()
    {
        return $this->hasOne('\App\Fasuc_rgo','codrgo','codrgo');
    }
}

