<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Sucursal;
use Caffeinated\Shinobi\Concerns\HasRoles;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;
use App\Segususuc;
use App\Models\Segcenusu;

class User extends Authenticatable implements Auditable
{
    use Notifiable;
    use \OwenIt\Auditing\Auditable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "usuarios";
    protected $connection = 'pgsql';
    protected $fillable = [
        'loguse', 'nomuse','apluse','pasuse','cedemp','diremp','telemp','id','feccad','numemp','stablo','created_at','updated_at','deleted_at','emaemp','codigoid','codsuc_reg','codroles'
    ];
    // protected $primaryKey = 'codigoid';
    protected static $logAttributes = ['stablo','loguse','nomuse'];
    protected static $logName = 'Usuarios';
    protected $primaryKey = 'codigoid';
    protected $keyType = 'string';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function __toString()
    {
        return $this->loguse;
    }

    public function sucursal()
   {
    return $this->belongsTo(Sucursal::class,'codsuc_reg');
   }

   public function getNombreSucursal()
   {
    return Sucursal::where([['codsuc',$this->codsuc_reg]])->first();
   }

   public function userSucursal()
   {
    return $this->hasOne(Sucursal::class,'codsuc','codigoid');
   }
   
   public function userSegSuc()
   {
        
       return $this->hasOne(Segususuc::class,'loguse','loguse')
              ->where('codsuc',session('codsuc'));//modifique aui
       //return $this->hasOne(Segususuc::class,'codusuario','codigo_id');
   }
  

   public function role()
    {
      return $this->hasOne(rolesusuarios::class,'codroles','codroles');
    }

   public function getsucUse()
   {
      return Segususuc::where('loguse',$this->loguse)->first();//Es codigoid no codigo_id tiene que ver el migrations
   }

   public function getUserSeg()
   {
      return $this->hasOne(Segususuc::class,'codusuario','codigoid');
   }
   public function isAdmin()
   {
    
      return $this->userSegSuc->role->nombre === 'ADMIN'; 
   }

   public function roles()
   {  
      return $this->hasOne(rolesusuarios::class,'codroles','codroles'); 
     // return $this->userSegSuc->role->roles === 'ADMIN'; 
   }

   public function getUsuSuc()
   {
      $valor=$this->hasMany('App\Sucursal','codsuc','codsuc');
      return $valor? $valor : 0 ;
   }

   public function getActiveS(){
      return Segususuc::where('codusuario',$this->codigoid)->where('codsuc',session('codsuc'))->whereNull('deleted_at')->first()->getSucursal->nomsucu;
   }

   public function getCodigoActiveS(){
    #dd($this->codigoid,session('codsuc'));
      return Segususuc::where('loguse',$this->loguse)->where('codsuc',session('codsuc'))->whereNull('deleted_at')->first()->getSucursal->codsuc;
   }

   public function getCaja()
   {
       return $this->hasMany('\App\cajaUser','loguse','loguse');
   }

   public function getActiveS2(){
      return Segususuc::where('loguse',$this->loguse)->where('codsuc',session('codsuc'))->whereNull('deleted_at')->first()->getSucursal->codsuc;
   }

   public function getCaja2()
   {
       return $this->hasOne('\App\cajaUser','loguse','loguse');
   }

   public function segcen(){
      return $this->hasMany(Segcenusu::class, 'loguse', 'loguse')->select('codcen');
   }

   public function usersuc()
   {
    return $this->belongsTo(Sucursal::class,'codsuc_reg', 'codsuc');
   }

   function permisos() {
      return $this->hasMany(permission_user::class,'codusuarios','loguse');
   }
   
   public function npasicaremp (){
      return $this->hasOne('App\Npasicaremp','codemp','cedemp')->orderBy('id','DESC');
  }
}
