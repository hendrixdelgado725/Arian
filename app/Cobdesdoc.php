<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


class Cobdesdoc extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public $timestamps = false;
    protected $connection='pgsql2';
    protected $table = 'cobdesdoc';

    protected $fillable = [
        'refdoc','codcli','coddes','fecdes','mondes',
   ];
}
