<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class ColorPro extends Model implements Auditable
{
    use SoftDeletes,\OwenIt\Auditing\Auditable;
    protected $connection = 'pgsql2';
	protected $table = 'colorpro';
    protected $fillable = ['color','nomencolor','codigoid','codsuc'];
    
    protected static $logAttributes = ['color','nomencolor','codigoid','codsuc'];
    protected static $logName = 'colorpro';
    protected static $logFillable = true;

    public function articulos(){
        return $this->hasMany('App\Caregart','codcolor','codigoid');
        
    }
}


