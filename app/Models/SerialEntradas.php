<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\Şerial;
use Illuminate\Database\Eloquent\SoftDeletes;

class SerialEntradas extends Model implements Auditable
{
    use SoftDeletes;
    use HasFactory;
    use \OwenIt\Auditing\Auditable;


    public $connection = 'pgsql2';
    public $table = 'serial_entrada';

    public $fillable = ['serial','codart','entrada','codalm','salida','deleted_at'];

    public function SerialesFacturados()
    {
        return $this->hasOne('App\Models\Serial','serial','serial');
    }

    function SerialEntradas() {
        return $this->hasOne('App\Caentalm','codmov','entrada');
    }
    public function SerialesDisponible()
    {
        return $this->hasOne('App\Models\serialDisponible','seriales','serial');
    }

    public function Articulos()
    {
        return $this->hasOne('App\Caregart','codart','codart');
    }

    public function SerialAceptado(){
        return $this->hasOne('App\Caentalm', 'codmov', 'entrada')->where('status', 'APR');
    }
}
