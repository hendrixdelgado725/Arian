<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;


class cierre_log extends Model implements Auditable
{


    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql';
    protected $table = 'logs_cierre_turnos';

    protected $fillable = ['caja','numero','tipoturno','estatus','fecha'];
}
