<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Fanotaentrega extends Model implements Auditable
{
    use HasFactory, SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql2';
    protected $table = 'fanotaentrega';
    protected $fillable = [
        'codnota', 'fecnota', 'status', 'codcli', 'monto', 'montasa',
        'tippag', 'tipmon', 'subtotal', 'monrecargo', 'usunot', 'mondes', 'codsuc', 'observ',
        'tasa', 'banco_id', 'codcaj'
    ];

    public function cliente()
    {
        return $this->belongsTo('App\Facliente', 'codcli', 'codpro');
    }

    public function articulos()
    {
        return $this->hasMany(Fanotaentregaart::class, 'codnota', 'codnota');
    }
    
    public function cajero()
    {
        return $this->hasOne('App\User', 'loguse', 'usunot');
    }

    public function moneda()
    {
        return $this->hasOne('App\Famoneda', 'codigoid', 'tipmon');
    }

    public function tasa()
    {
        return $this->hasOne('App\Fatasacamb', 'codigoid', 'tasa');
    }

    public function banco()
    {
        return $this->hasOne('App\Bancos', 'id', 'banco_id');
    }

    public function caja()
    {
        return $this->hasOne(Caja::class, 'id', 'codcaj');
    }

    public function getStatusAttribute($value)
    {
        switch($value)
        {
            case 'A': 
                return 'PROCESADA';
                break;
            
            case 'N': 
                return 'ANULADA';
                break;
        }
    }

    public function getFecnotaAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }
}
