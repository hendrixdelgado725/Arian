<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Cadetpro extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $connection = 'pgsql2';

    protected $table = 'cadetpro';

    protected $fillable = ['codprom','codartprom','codart','cantidad', 'id'];

    public function cadefpro(){
        return $this->hasOne(Cadefpro::class,'codprom','codprom');
    }

    public function caregart(){
        return $this->hasMany('App\Caregart','codart','codart');
    }
  
}
