<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoTraslado extends Model
{
    use HasFactory;

    protected $connection = 'pgsql2';

    protected $table = "tipotraslado";

    protected $fillable = [
        'nombre',
        'nomcat',
        'codigoid',
        'codsuc'
    ];

}
