<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Nphojint extends Model implements Auditable
{
    use SoftDeletes;
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql2';
    protected $table = 'nphojint';

    protected $appends = ['maingerencia'];

    public function npasicaremp (){
        return $this->hasMany('App\Npasicaremp','codemp','codemp')->where('status','V');
    }

    public function familiares()
    {
        return $this->hasMany(Npinffam::class,'codemp','codemp');
    }

    public function getMainGerenciaAttribute()
    {
        return optional(Npstorg::whereCodniv(substr($this->codniv,0,3))->first())->desniv ?? '';
    }

    public function gerencia()
    {
        return $this->hasOne(Npstorg::class, 'codniv', 'codniv');
    }

    public static function getEmp($codemp)
    {
        return self::select(['codemp','nomemp','staemp','emaemp'])->where('codemp',$codemp)->where('staemp','A')->first();
    }

}
