<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Sucursal;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Caja extends Model implements Auditable
{
    //
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql2';
    protected $table = "fadefcaj";
    protected $fillable = [
        'descaj',
        'corcaj',
        'id',
        'corfac',
        'cornumctr',
        'codalm',
        'conpag',
        'impfisname',
        'impfishost',
        'impserial',
        'dircaj',
        'numcue',
        'serie',
        'cornotcre',
        'idsucur',
        'codsuc',
        'codsuc_reg',
        'codigoid',
        'correlativo',
        'cornotcre',
        'cornot',
        'is_fiscal',
        'codiva',
        'acepiva'
    ]; 

    public function sucursales(){
      return $this->belongsTo(Sucursal::class,'codsuc','codsuc');
    }

    public static function getcount(){
      return self::withTrashed()->get()->count();
    }

    public static function getCountCorre(){
      return self::withTrashed()->get()->count();
    }

    public static function findbycod($codigoid){

      return self::where('deleted_at',NULL)->where('id',$codigoid)->firstOrFail();
    }

    public function sucur()
   {  
      return $this->hasOne(Sucursal::class,'codsuc','codsuc'); 
     // return $this->userSegSuc->role->roles === 'ADMIN'; 
   }

   public function ventas(){
      return $this->hasMany('App\Fafactur','codcaj','id');
   }

   public function almacen()
   {
      return $this->hasOne('App\Almacen','codalm','codalm');
   }

   public function lastfact(){
    return $this->hasOne('App\Fafactur','codcaj','id')->latest();
    }

    function turno() {
      
      return $this->hasOne('App\Models\Turno','caja','id');
    }

    function cajaUser(){
      return $this->hasOne('App\cajaUser','codcaja','id');
    }

}
