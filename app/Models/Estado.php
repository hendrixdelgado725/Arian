<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Estado extends Model implements Auditable
{
    use SoftDeletes,\OwenIt\Auditing\Auditable;
    protected $connection = 'pgsql2';
    protected $table = "faestado";
    protected $fillable = [
      'fapais_id',
      'nomedo',
      'id',
      'codsuc',
      'codigoid'
    ];
  

    public function pais(){
      return $this->belongsTo(Pais::class,'fapais_id');
    }

    public function sucursales(){
      return $this->belongsTo(\App\Sucursal::class,'id');
    }

    public static function candelete($id){
      return self::where('id',$id)->has('sucursales')->get();
  }
}
