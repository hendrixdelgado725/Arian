<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Npinffam extends Model implements Auditable
{
    use SoftDeletes;
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql2';

    protected $table = 'npinffam';
    protected $fillable = ['codemp','cedfam', 'nomfam', 'sexfam','edafam','fecnac','parfam','porcecdm',
    'seghcm',
    'fecing',
    'fecinghcm'];

    public function parentesco()
    {
        return $this->belongsTo(Nptippar::class, 'parfam','tippar');
    }
    
}

