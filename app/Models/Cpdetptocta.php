<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cpdetptocta extends Model
{
    use HasFactory;

    protected $connection = 'pgsql2';

    protected $table = 'cpdetptocta';

    public function punto(){
        return $this->belongsTo(Cpptocta::class, 'numpta', 'numpta');
    }
}
