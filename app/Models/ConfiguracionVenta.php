<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ConfiguracionVenta extends Model
{
	public function fechaItinerarios()
	{
		return $this->belongsTo(FechaItinerario::class);
	}

	public function taquillas()
	{
		return $this->belongsTo(Taquilla::class);
	}

	public function itinerarios()
	{
		return $this->belongsTo(Itinerario::class);
	}

	public function clases()
	{
		return $this->belongsTo(Clase::class);	
	}

	public function ferry()
   {
   	return $this->belongsTo(Ferry::class,'ferrys_id');
   }

	public function getCantidadTaquilla()
	{
		return ConfiguracionVenta::where([['clases_id',$this->clases_id],['fecha_itinerarios_id',$this->fecha_itinerarios_id],['itinerarios_id',$this->itinerarios_id]])->sum('cantidad');
	}

   // public function getCantidadTaquillaVehiculo()
   // {
   //    return ConfiguracionVenta::where([['clases_id',$this->clases_id],['fecha_itinerarios_id',$this->fecha_itinerarios_id],['itinerarios_id',$this->itinerarios_id],['deleted_at',NULL]])->sum('cantidad_vehiculo');
   // }

	public function getNombreTaquilla()
	{
		return $this->taquillas->nombre;
	}

	public function getNombreClase()
	{
		return $this->clases->nombre;
	}

	public function getFechaItinerario()
	{
		return date("d/m/Y", strtotime($this->fechaItinerarios->fecha));

	}

	public function getDetalleItinerario()
	{
		 return $this->itinerarios->__toString();
	}

	public function getDisponibilidad()
	{
		$disp = $this->cantidad - $this->cantidad_reservada - $this->cantidad_vendida /*- $this->cant_boleto_infante*/;

		return ($disp > 0) ? 1 : 0;
	}

	public function getDisponibles()
	{
		return ($this->cantidad - $this->cantidad_reservada - $this->cantidad_vendida /*- $this->cant_boleto_infante*/);		
	}

   public function getDisponiblesDisc($max)
   {
      return $max - $this->cant_boleto_discap;
   }

	public function getDisponiblesV()
	{
      $cv = ConfiguracionVenta::where('taquillas_id',$this->taquillas_id)
                                ->where('fecha_itinerarios_id',$this->fecha_itinerarios_id)->get();
  
      if(count($cv) > 0)
      {
         $cant_veh = 0;
         $asig_veh = 0;
         foreach($cv as $conf)
         {
            $cant_veh += ($conf->cant_reserv_vehiculo + $conf->cant_vendida_veh);
            $asig_veh += $conf->cantidad_vehiculo;
         }

         return $asig_veh - $cant_veh;
      }
      else
         return 0;
	}

  public function getDisponiblesCamiones()
  {
      $cv = ConfiguracionVenta::where('taquillas_id',$this->taquillas_id)
                                ->where('fecha_itinerarios_id',$this->fecha_itinerarios_id)->get();
  
      if(count($cv) > 0)
      {
         $cant_cam = 0;
         $asig_cam = 0;
         foreach($cv as $conf)
         {
            $cant_cam += ($conf->cant_reserv_camiones + $conf->cant_vendida_cam);
            $asig_cam += $conf->cantidad_camiones;
         }

         return $asig_cam - $cant_cam;
      }
      else
         return 0;
  }

	public function getDisponiblesM()
	{
		return ($this->cantidad_mascota - $this->cantidad_reserv_mascota - $this->cant_vendida_masc);			
	}	

	 public function getNombreFerry()
    {
       return $this->ferry->__toString();
    }

    public function posicionesDisponibles()
    {
    	// dd($this->id);
    	$posiciones = ItinerarioPosicion::where([['configuracion_ventas_id',$this->id],['is_active',0],['deleted_at',NULL]])->get();
		
		return $posiciones->count();
    	//return $this->hasMany(ItinerarioPosicion::class,'configuracion_ventas_id')->where([['is_active',0],['deleted_at',NULL]]);
    }

    public function obtenerPosicionDisponible()
    {
    	return ItinerarioPosicion::where([['configuracion_ventas_id',$this->id],['is_active',0],['deleted_at',NULL]])->first();
    }

    public function obtenerPosicionDisponibleV2($ids)
    {
      return ItinerarioPosicion::where([
                                          ['configuracion_ventas_id',$this->id],
                                          ['is_active',0],
                                          ['deleted_at',NULL]])
                                    ->whereNotIn('id',$ids)->first();
    }

    public function getRuta()
    {
    	return $this->itinerarios->ruta;
    }


    public function getCantidadTaquillaVehiculo()
	{
		return ConfiguracionVenta::where([['fecha_itinerarios_id',$this->fecha_itinerarios_id],['itinerarios_id',$this->itinerarios_id]])->sum('cantidad_vehiculo');
	}

	public function getCantidadReservVeh()
	{
		return ConfiguracionVenta::where([['fecha_itinerarios_id',$this->fecha_itinerarios_id],['itinerarios_id',$this->itinerarios_id]])->sum('cant_reserv_vehiculo');
	}

	public function getCantidadVendidaVeh()
	{
		return ConfiguracionVenta::where([['fecha_itinerarios_id',$this->fecha_itinerarios_id],['itinerarios_id',$this->itinerarios_id]])->sum('cant_vendida_veh');
	}

    public function boletosPendientes()
    {
         $boletos = DB::select("SELECT * from boletos where (configuracion_ventas_id=:cv or conf_venta_id=:cvr) and estatus=:estatus",array(':cv' => $this->id, ':cvr' => $this->id, ':estatus'=>0));
         return $boletos;
    }

    public function mantenerEliminado()
    {
      if($this->estatus == 2)
        return true;
      
        $fechait   = $this->fechaItinerarios;
        $hoy       = date_create(date('Y-m-d H:i:s'));
        $fecha2    = date_create($fechait->fecha.' '.$fechait->getHoraSalida());
        $interval  = date_diff($hoy, $fecha2);
        $multiplo  = ($interval->invert == 0) ? 1 : -1;
        $dif_horas = ($interval->d * $multiplo * 24) + $interval->h;

        if($this->taquillas_id == 4 && $dif_horas < 24) //ventas web
            return true;

        if($this->taquillas_id != 4 && $dif_horas < -4)
            return true;

        return false;
    }

    public function obtenerPosicionesDisponibles()
    {
      return ItinerarioPosicion::where([['configuracion_ventas_id',$this->id],['is_active',0],['deleted_at',NULL]])->get();
    }

    public function getCantidadTaquillaCamiones()
   {
     return ConfiguracionVenta::where([['fecha_itinerarios_id',$this->fecha_itinerarios_id],['itinerarios_id',$this->itinerarios_id]])->sum('cantidad_camiones');
   }

}
