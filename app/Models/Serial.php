<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Serial extends Model implements Auditable
{
    use HasFactory;
    use SoftDeletes,\OwenIt\Auditing\Auditable;
    protected $connection = 'pgsql2';
    protected $table = "serial";
    protected $fillable = ['factura','serial','codart','facturar_serial'];

    public function SerialesFacturados()
    {
        
         return $this->hasOne('App\Models\Serial','codart','codart');
    }

    public function getFactura()
    {
        return $this->belongsTo('App\Fafactur','factura','reffac');
    }
    public function getArticulo()
    {
        return $this->hasOne('\App\Faartfac','codart','codart');
    }

    public function serialesDisponibles()
    {
        return $this->belongsTo('App\Models\SerialesDisponibles','codart','codart');

    }



}
