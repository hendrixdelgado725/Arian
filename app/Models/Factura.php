<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    use HasFactory;

    protected $connection = 'pgsql2';

    protected $table = 'cafacpag';

    protected $fillable = [
        'numord', 'fecfac', 'cedrif', 'nomben', 'numctr', 'tiptra', 'totfac', 'totfac', 'exeiva', 'basimp', 'poriva', 'moniva', 'monret', 'rifalt', 'fecrecfac', 'tasa', 'montasa'
    ];
}
