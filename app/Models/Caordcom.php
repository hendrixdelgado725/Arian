<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Caordcom extends Model implements Auditable
{
    use HasFactory;
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql2';

    protected $table = 'caordcom';

    protected $fillable = [
        'ordcom', 'fecord', 'codpro', 'desord', 'codcen', 'tipord', 'monord',
        'staord', 'afepre', 'conpag', 'forent', 'tipmon', 'valmon', 
        'tipo', 'codemp', 'afepro', 'doccom', 'tipfin', 'motanu', 'usuroc',
        'refsol', 'tasa', 'montasa', 'tasaid', 'subtotal', 'montasa', 'moniva',
    ];

    public function articulos(){
        return $this->hasMany(Caartord::class, 'ordcom', 'ordcom');
    }

    public function solicitante(){
        return $this->hasOne(Nphojint::class, 'codemp', 'codemp');
    }

    public function ccosto(){
        return $this->hasOne(Costos::class, 'codcen', 'codcen');
    }

    public function proveedor(){
        return $this->hasOne('App\Caprovee', 'codpro', 'codpro');
    }
    
    public function pedido(){
        return $this->hasOne(Casolart::class, 'reqart', 'refsol');
    }

    public function condicionPago(){
        return $this->hasOne(Caconpag::class, 'codconpag', 'conpag');
    }

    public function formaEntrega(){
        return $this->hasOne(Caforent::class, 'codforent', 'forent');
    }

    public function tipoFinancia(){
        return $this->hasOne(Fortipfin::class, 'codfin', 'tipfin');
    }

    public function tipoMoneda(){
        return $this->hasOne('App\Tsdefmon', 'codmon', 'tipmon');
    }

    public function tasa(){
        return $this->hasOne('App\Fatasacamb', 'codigoid', 'tasaid');
    }

    public function getStaordAttribute($value)
    {
        switch($value)
        {
            case 'P': 
                return 'PENDIENTE POR REGISTRAR';
                break;

            case 'U': 
                return 'PENDIENTE POR APROBACIÓN DEL GERENTE';
                break;

            case 'A': 
                switch($this->afepre){
                    case 'P':
                        return 'APROBADA/PENDIENTE POR NOTA DE ENTREGA';
                        break;

                    case 'G':
                        return 'NOTA DE ENTREGA REGISTRADA';
                        break;
                    
                    case 'S':
                        return 'NOTA DE ENTREGA FACTURADA';
                        break;

                    case 'N':
                        return 'NOTA DE ENTREGA ANULADA';
                        break;
                }
            
            case 'N': 
                return 'ANULADO';
                break;
        } 
    }

    public function getTipordAttribute($value)
    {
        if($value === 'S'){
            return 'SERVICIO';
        } else {
            return 'COMPRA';
        }
    }
}
