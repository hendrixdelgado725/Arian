<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\Model;

class Caartsol extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    
    protected $connection = 'pgsql2';

    protected $table = 'caartsol';

    protected $fillable = [
        'reqart', 'codart', 'codcat', 'canreq', 'canrec',
        'montot', 'costo', 'monrgo', 'canord', 'mondes',
        'unimed', 'codpar', 'desart'
    ];

   
}
