<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Capagord extends Model implements Auditable
{
    use HasFactory;
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql2';

    protected $table = 'capagord';

    protected $fillable = [
        'numpag', 'numord', 'despag', 'fecemi',
        'cedrif', 'nomben', 'desord', 'numref', 'ctaban',
        'status', 'monord', 'montot', 'mondes', 'monexe',
        'monbas', 'moniva', 'monret', 'monpag', 'tasa',
        'montasa', 'tasaid', 'fecpag', 'fecanu', 'desanu',
        'loguse', 'usuarioreg', 'codmon', 'valmon', 'obspag',
        'monipb'
    ];

    public function orden()
    {
        return $this->belongsTo(Caordcom::class, 'ordcom', 'numord');
    }

    public function facturas()
    {
        return $this->hasMany(Cafacpag::class, 'numpag', 'numpag');
    }

    public function banco()
    {
        return $this->hasOne('App\Bancos', 'codban', 'ctaban');
    }

    public function register()
    {
        return $this->hasOne('App\User', 'cedemp', 'usuarioreg');
    }

    public function beneficiario()
    {
        return $this->hasOne('App\Caprovee', 'codpro', 'cedrif');
    }

    public function tasa()
    {
        return $this->hasOne('App\Fatasacamb', 'codigoid', 'tasaid');
    }
}
