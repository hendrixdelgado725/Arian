<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Serial;


class Fanotaentregaart extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'pgsql2';
    protected $table = 'fanotaentregaart';

    protected $fillable = [
        'codnota', 'codart', 'cantart',
        'subtotal', 'mondes', 'totrgo',
        'totart', 'descto', 'monrgo', 
        'montasa', 'codalm'
    ];

    public function articulo()
    {
        return $this->hasOne('App\Caregart', 'codart', 'codart');
    }

    public function costos()
    {
        return $this->hasOne('App\faartpvp','codart','codart')->where('status','A');
    }

    public function seriales()
    {
        $seriales = Serial::where('factura', $this->codnota)->where('codart', $this->codart)->withTrashed()->select('serial')->get()->toArray();
        $values = array_column($seriales, 'serial');

        return $values;
    }
}