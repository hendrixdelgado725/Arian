<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Famoneda;

class Casolart extends Model implements Auditable
{
    use HasFactory;
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql2';

    protected $table = 'casolart';

    protected $fillable = [
        'reqart', 'fecreq', 'desreq', 'monreq', 
        'mondes', 'unires', 'tipmon', 'stareq',
        'tipfin', 'tipreq', 'aprreq', 'valmon', 
        'loguse', 'empsol', 'proreq', 'codcen', 
    ];

    public function ccosto(){
        return $this->hasOne(Costos::class, 'codcen', 'codcen');
    }

    public function articulos(){
        return $this->hasMany(Caartsol::class, 'reqart', 'reqart');
    }

    public function unidad(){
        return $this->hasOne(Npcatpre::class, 'codcat', 'unires');
    }

    public function fin(){
        return $this->hasOne(Fortipfin::class, 'codfin', 'tipfin');
    }

    public function orden(){
        return $this->hasOne(Caordcom::class, 'refsol', 'reqart');
    }

    public function solicitante(){
        return $this->hasOne(Nphojint::class, 'codemp', 'empsol');
    }
    
    public function moneda(){
        return $this->hasOne('App\Tsdefmon', 'codmon', 'tipmon');
    }

    public function proveedor(){
        return $this->hasOne('App\Caprovee', 'codpro', 'proreq');
    }

    public function pagos(){
        return $this->hasMany(Capagord::class, 'numord', 'ordcom');
    }

    public function aprobador(){
        return $this->hasOne('App\User', 'cedemp', 'gerapru');
    }
    
    public function getStareqAttribute($value)
    {
        switch($value){
            case 'P':
                return 'PENDIENTE DE REGISTRAR';
                break;

            case 'U':
                return 'PENDIENTE POR APROBACIÓN DEL GERENTE DE ÁREA';
                break;

            case 'A':
                switch($this->aprreq){
                    case 'G':
                        return 'APROBADO POR GERENTE DE ÁREA / PENDIENTE POR O/C';
                        break;

                    case 'O':
                        return 'ORDEN DE COMPRA ASIGNADA';
                        break;
                    
                    case 'P':
                        return 'APROBADA POR PRESUPUESTO / PENDIENTE POR COMPRA';
                        break;

                    case 'S':
                        return 'APROBADA';
                        break;
                    
                    case 'N':
                        return 'PENDIENTE POR REVISIÓN';
                        break;
                }
            case 'N':
                return 'ANULADA';
                break;
            
            case 'X':
                return 'RECHAZADO POR EL GERENTE DE ÁREA';
                break;
                        
        }
    } 
}
