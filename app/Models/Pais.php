<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;
class Pais extends Model implements Auditable
{
    use SoftDeletes,\OwenIt\Auditing\Auditable;
    protected $connection = 'pgsql2';
    protected $table = "fapais";
    protected $fillable = [
        'nompai',
        'id',
        'codint'
    ];


    protected static $logAttributes = ['nompai','id','codint'];
    protected static $logName = 'fapais';
    protected static $logFillable = true;

    public function estados(){
        return $this->hasMany(Estado::class,'fapais_id','id');
    }

    public static function candelete($id){
        return self::where('id',$id)->has('estados')->get();
    }

}
