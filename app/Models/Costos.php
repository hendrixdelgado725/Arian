<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Costos extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql2';

    protected $table = 'cadefcen';

    protected $fillable = [
        'codcen',
        'descen',
        'dircen',
        'codpai',
        'codemp',
        // 'nomcar',
        'cedenc',
    ];

    public function estado(){
        
        return $this->hasOne(Estado::class, 'id', 'codpai');
    }

    public function responsable(){
        return $this->hasOne(Nphojint::class, 'codemp', 'codemp');
    }

    public function encargado(){
        return $this->hasOne(Nphojint::class, 'codemp', 'cedenc');
    }
}
