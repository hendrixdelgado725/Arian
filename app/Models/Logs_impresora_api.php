<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Logs_impresora_api extends Model
{

    use HasFactory;
    protected $connection = "pgsql";
    protected $table      = "log_impresora_api";
    protected $fillable   = [
        "factura_sistema",
        "devolucion_fiscal",
        "factura_fiscal",
        "tipo",
        "createdAt",
        "updatedAt"
    ];

}
