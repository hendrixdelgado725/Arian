<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Diagnostico extends Model
{
    use HasFactory;

    protected $connection = 'pgsql2';

    protected $table = 'diagnosticos';

    protected $fillable = [
        'codfam',
        'codigo_diag',
        'codigo_sol',
        'diagnostico',
        'fecdsdinf',
        'fechstinf',
        'fecdsdrec',
        'fechstrec',
        'observaciones',
        'soltratam_id'
    ];

    public function solitratam(){ //opcional 
        return $this->belongsTo(Soltratam::class, 'soltratam_id', 'id');
    }

    public function beneficiarios(){ 
        return $this->belongsTo(Npinffam::class); //Codfam es la cédula del familiar
    }

    public function medicinas(){
        return $this->hasMany(Medicamento::class); //
    }
}
