<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Caarttal extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql2';

    protected $table = 'caarttal';

    protected $fillable = ['codtalla','codart'];

    public function articulos()
    {
        return $this->belongsTo('App\Caregart', 'codart','codart');
    }
    
    public function tallas()
    {
        return $this->belongsTo('App\Tallas', 'codtalla','codtallas');
    }
}
