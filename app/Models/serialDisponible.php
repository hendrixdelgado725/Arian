<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;


class serialDisponible extends Model implements Auditable
{
    use SoftDeletes;
    use HasFactory;
    use \OwenIt\Auditing\Auditable;


    public $connection = 'pgsql2';
    public $table = 'seriales_disponible';

    public $fillable = ['seriales','codart','disponible','codalm'];

    public function SerialesFacturados()
    {
        return $this->hasOne('App\Models\Serial','serial','seriales');
    }
}
