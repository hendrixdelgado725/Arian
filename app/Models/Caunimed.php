<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Caunimed extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $connection = 'pgsql2';

    protected $table = 'caunimed';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'nombre', 'nomenclatura', 'codigo', 
    ];
}
