<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Npsolinffam extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql2';

    protected $table = 'npsolinffam';

    protected $fillable = [
        'codigo',
        'codemp',
        'estatus',
        'codemp_apro',
        'fec_apro',
        'fec_anu',
    ];

    public function trabajador()
    {
        return $this->belongsTo(Nphojint::class, 'codemp','codemp');
    }

    public function familiares()
    {
        return $this->hasMany(Npsolinffamper::class, 'codigo', 'codigo');
    }

    public function scopenomemp($query, $nomemp = null)
    {
        return ($nomemp) ? $query->whereHas('trabajador', function($query) use ($nomemp){
            return $query->where('nomemp','like','%'.$nomemp.'%');
        }) : $query;
    }

    /* public function nomempScope($nomemp = null)
    {
        return ($nomemp) ? $query->whereHas('trabajador', function($query) use ($nomemp){
            return $query->where('nomemp','like','%'.$nomemp.'%');
        });
    } */
}
