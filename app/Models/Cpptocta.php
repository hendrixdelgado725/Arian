<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cpptocta extends Model
{
    use HasFactory;

    protected $connection = 'pgsql2';

    protected $table = 'cpptocta';

    protected $fillable = [
        'numpta', 'fecpta', 'codubiori', 'codubides', 'asunto', 
        'motivo', 'reccon', 'loguse', 'aprpto', 'usuapr', 'fecapr'
    ];

    public function imputs(){
        return $this->hasMany(Cpdetptocta::class, 'numpta', 'numpta');
    }
}
