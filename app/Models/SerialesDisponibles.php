<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SerialesDisponibles extends Model
{
    use HasFactory;

    protected $connection = 'pgsql2';
    protected $table = "seriales_disponible";
    protected $fillable = ['seriales','codart','disponible', 'codalm'];

    public function almacen(){
        return $this->belongsTo('App\Almacen','codalm','codalm');
    }
}
