<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cpprecom extends Model
{
    use HasFactory;

    protected $connection = 'pgsql2';

    protected $table = 'cpprecom';

    protected $fillable = [
        'refprc', 'tipprc', 'fecprc', 'anoprc', 'desprc',
        'desanu', 'monprc', 'salcom', 'salcau', 'salpag',
        'salaju', 'staprc', 'staprc', 'fecanu', 'cedrif',
    ];

    public function articles(){
        return $this->hasMany(Cpimpprc::class, 'refprc', 'refprc');
    }
}
