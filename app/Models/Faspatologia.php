<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Faspatologia extends Model
{
    use HasFactory;

    protected $connection = 'pgsql2';

    protected $table = 'faspatologia';

    protected $fillable = [
        'id_espec', 'monto', 'nombre', 'codigoid', 'codsuc'
    ];

    public function especialidad(){
        return $this->belongsTo(Fasespecialidad::class,'id_espec');
    }
}
