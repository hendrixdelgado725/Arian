<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Retenciones extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql2';

    protected $table = 'optipret';

    protected $fillable = [
        'codtip', 'destip', 'codcon', 'basimp', 
        'factor', 'porsus', 'unitri', 'porret',
        'id', 'pagmin', 'tipotasa', 'codtipsen', 
        'codsuc', 'codigoid', 'mbasma', 'mbasmi', 'mansus', 
    ];
}
