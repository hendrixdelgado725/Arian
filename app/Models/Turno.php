<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;


class Turno extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'turnos';
    protected $connection ='pgsql2';

    protected $fillable = ([
        'caja','cajero','tipoturno','supervisor', 'inicio', 'estatus', 'numtur'
    ]);



    public function Cajero()
    {
        return $this->hasOne('App\cajaUser', 'cedula', 'cajero');
    }

    public function getTipoturnoAttribute($value)
    {

        
        switch(strtolower($value)){
            case 'matutino':
                return 'MATUTINO';
                break;

            case 'vespertino':
                return 'VESPERTINO';
                break;

            case 'completo':
                return 'COMPLETO';
                break;
                        
        }
    }

    public function getInicioAttribute($value)
    {
        return $value ? Carbon::parse($value)->format('d-m-Y') : null;
    }

    public function Supervisor()
    {
        return $this->hasOne('App\User', 'cedemp', 'supervisor');
    }

    public function Caja()
    {
        return $this->hasOne(Caja::class, 'id', 'caja');
    }

}
