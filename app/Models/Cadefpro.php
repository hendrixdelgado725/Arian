<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Cadefpro extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $connection = 'pgsql2';

    protected $table = 'cadefpro';

    protected $fillable = [
        'codprom','codartprom','codalm' ,'codmon','agotexis','motanu','nomcre','fecdiprom','fechasprom','fedesprom','precio','codmon','status','desprom'
    ];

    public function cadetpro(){
        return $this->hasOne(Cadetpro::class,'codprom','codprom');
    }

    public function caregart(){
        return $this->hasOne('App\Caregart','codart','codartprom');
    }
        public function moneda(){
            return $this->hasMany('App\Famoneda','codigoid','codmon');
        }
        public function almacen(){
            return $this->hasMany('App\Almacen','codalm','codalm');
        }
    
}
