<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fiscal extends Model
{
    use HasFactory;
    protected $connection = 'pgsql';
    protected $table = 'empresa';
    protected $fillable = ['passemp'];
}
