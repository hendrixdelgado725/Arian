<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medicamento extends Model
{
    use HasFactory;

    protected $connection = 'pgsql2';

protected $table = 'medicamentos';

protected $fillable = [
    'codigo_med',
    'nombre',
    'dosis',
    'frecuencia',
    'duracion',
    'cantidad',
];

public function diagnostics(){
    return $this->belongsTo(Diagnostico::class, 'diagnostico_id', 'id');
}

}
