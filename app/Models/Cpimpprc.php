<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cpimpprc extends Model
{
    use HasFactory;

    protected $connection = 'pgsql2';

    protected $table = 'cpimpprc';

    protected $fillable = ['refprc', 'codpre', 'monimp', 'moncom', 'moncau', 'monpag', 'monaju', 'staimp'];

    public function cpdeftit(){
        return $this->belongsTo('App\Cpdeftit', 'codpre', 'codpre');
    }

    public function asignado(){
        return $this->hasOne(Cpasiini::class, 'codpre', 'codpre');
    }
    public function partida(){
        return $this->hasOne(Nppartidas::class, 'codpar', 'codpre');
    }
}
