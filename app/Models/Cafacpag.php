<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Cafacpag extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    
    protected $connection = 'pgsql2';

    protected $table = 'cafacpag';

    protected $fillable = [
        'numop', 'numpag', 'numord', 'numfac', 'numctr',
        'fecfac', 'fecrecfac', 'rifalt', 'tiptra', 'porret',
        'totfac', 'exeiva', 'mondes', 'basimp', 'poriva',
        'moniva', 'monret', 'monpag', 'tasa', 'montasa',
        'tasaid', 'monipb',
    ];

    public function proveedor()
    {
        return $this->hasOne('App\Caprovee', 'codpro', 'rifalt');
    }
}
