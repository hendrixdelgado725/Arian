<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Caartord extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql2';

    protected $table = 'caartord';

    protected $fillable = [
        'desart', 'unimed', 'exitot', 'preart', 'totart',
        'reqart','codart', 'codcat', 'canrec', 'canord', 
        'ordcom', 'montasa'
    ];

    public function getRgoartAttribute($value){
        $op = $value * $this->canord * $this->preart;

        return number_format($op, 2);
    }
    
}
