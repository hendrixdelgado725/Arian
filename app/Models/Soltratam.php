<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Soltratam extends Model
{
    use HasFactory;

    protected $connection = 'pgsql2';

    protected $table = "soltratam";

    protected $fillable = [
        'codigo',
        'codemp',
        'status',
        'codespec',
        'fec_apro',
        'fec_anu',
        'codniv',
    ];

    protected $nullable = [

    ];

    public function titular(){
        return $this->belongsTo(Nphojint::class, 'codemp','codemp');
    }

    public function diagnostic(){
        return $this->hasOne(Diagnostico::class);
    }

    public function scopenomemp($query, $nomemp = null)
    {
        return ($nomemp) ? $query->whereHas('titular', function($query) use ($nomemp){
            return $query->where('nomemp','like','%'.$nomemp.'%');
        }) : $query;
    }

    public function oficina(){
        return $this->hasOne(Npestorg::class,'codniv','codniv');
    }

    public function especialidad(){
        return $this->hasOne(Fasespecialidad::class,'id','codespec');
    }
}
