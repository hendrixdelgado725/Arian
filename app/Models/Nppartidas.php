<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nppartidas extends Model
{
    use HasFactory;

    protected $connection = 'pgsql2';

    protected $table = 'nppartidas';

    public function articlesR(){
        return $this->hasMany(Caartsol::class, 'codpar', 'codpar');
    }
}
