<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Npsolinffamper extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'npsolinfamper';

    protected $connection = 'pgsql2';

    protected $fillable = [
        'codigo',
        'codemp',
        'nomfam',
        'cedfam',
        'sexfam',
        'fecnac',
        'edadfam',
        'parfam',
        'porcecdm',
        'npinffam_id'
    ];

    // public function tratamiento(){
    //     return $this->hasOne(Diagnostico::class, 'idsolic');
    // }


    public function parentesco()
    {
        return $this->belongsTo(Nptippar::class, 'parfam','tippar');
    }
}
