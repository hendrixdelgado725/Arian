<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Cacotiza extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $connection = 'pgsql2';
    protected $table = 'cacotiza';
}
