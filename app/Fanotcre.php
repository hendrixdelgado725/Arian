<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Fanotcre extends Model implements Auditable
{
    use SoftDeletes,\OwenIt\Auditing\Auditable;

    protected $connection='pgsql2';
    protected $table = 'fanotcre';
    protected $fillable = [
        'reffac','correl','monto','id','fecnot'
   ];

   public function perteneceFactura()
   {
      return $this->belongsTo('App\Fafactur','reffac','reffac');
   }
}
