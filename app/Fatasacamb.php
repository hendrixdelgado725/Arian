<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Fatasacamb extends Model implements Auditable
{
    use SoftDeletes,\OwenIt\Auditing\Auditable;
    protected $connection='pgsql2';
    protected $table = 'fatasacamb';
    protected $fillable = ([
        'id_moneda','id_moneda2','valor','activo','codigoid','codsuc'
    ]);

    public function moneda(){
          return $this->belongsTo('App\Famoneda','id_moneda','codigoid');
    }

    public function moneda2(){
          return $this->belongsTo('App\Famoneda','id_moneda2','codigoid');
    }
}
