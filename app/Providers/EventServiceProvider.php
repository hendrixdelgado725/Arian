<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Events\Auditoria;
use App\Caregart;
use App\Observers\EventosObserver;
use App\faartpvp;
use App\Almacen;
use App\Fadurpre;
use App\Facliente;
use App\Alma_ubi;
use App\Bancos;
use App\caartalm;
use App\Models\Caarttal;
use App\Caartalmubi;
use App\Caartalmubimov;
use App\Cadefubi;
use App\Cadetent;
use App\Caentalm;
use App\cajaUser;
use App\Catipent;
use App\Catraalm;
use App\ColorPro;
use App\Contenido;
use App\DemoProducto;
use App\Facategoria;
use App\Fadefesc;
use App\Fadescto;
use App\Fadetesc;
use App\Fafactuart;
use App\Fafactur;
use App\Fafacturpago;
use App\Faforpag;
use App\Famoneda;
use App\Fapreserart;
use App\Fapresup;
use App\Farecarg;
use App\Fargoart;
use App\Fasubcategoria;
use App\Fasuc_rgo;
use App\Fatasacamb;
use App\Fatippag;
use App\Logs_impresoras;
use App\Logs_reportes;
use App\Npasicaremp;
use App\Npcargos;
use App\Nphojint;
use App\permission;
use App\permission_user;
use App\Recargo;
use App\rolesusuarios;
use App\Segususuc;
use App\Sucursal;
use App\Tallas;
use App\User;
use App\Models\Caja;
use App\Models\Estado;
use App\Models\Pais;
use App\Models\Costos;
use App\Models\Retenciones;
use App\Models\ConfiguracionVenta;
use App\Models\Casolart;
use App\Models\Caartsol;
use App\Models\Cacotiza;
use App\Models\Caordcom;
use App\Models\Caartord;    
use App\Models\Capagord;    
use App\Models\Cafacpag;    
use App\Models\Fanotaentrega;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
       /* Registered::class => [
            SendEmailVerificationNotification::class,
        ],*/

        'App\Events\Auditoria' => [
            'App\Ļisteners\EnviarRegistroAuditoria'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        rolesusuarios::observe(EventosObserver::class);
        Caregart::observe(EventosObserver::class);
        faartpvp::observe(EventosObserver::class);
        Almacen::observe(EventosObserver::class);
        Fadurpre::observe(EventosObserver::class);
        Facliente::observe(EventosObserver::class);
        Caartalmubi::observe(EventosObserver::class);
        Alma_ubi::observe(EventosObserver::class);
        Bancos::observe(EventosObserver::class);
        caartalm::observe(EventosObserver::class);
        ColorPro::observe(EventosObserver::class);
        Contenido::observe(EventosObserver::class);
        DemoProducto::observe(EventosObserver::class);
        Facategoria::observe(EventosObserver::class);
        Fadefesc::observe(EventosObserver::class);
        Fadescto::observe(EventosObserver::class);
        Fadetesc::observe(EventosObserver::class);
        Fafactuart::observe(EventosObserver::class);
        Fafactur::observe(EventosObserver::class);
        Fafacturpago::observe(EventosObserver::class);
        Faforpag::observe(EventosObserver::class);
        Famoneda::observe(EventosObserver::class);
        Fapreserart::observe(EventosObserver::class);
        Fapresup::observe(EventosObserver::class);
        Farecarg::observe(EventosObserver::class);
        Fargoart::observe(EventosObserver::class);
        Fasubcategoria::observe(EventosObserver::class);
        Fasuc_rgo::observe(EventosObserver::class);
        Fatasacamb::observe(EventosObserver::class);
        Fatippag::observe(EventosObserver::class);
        Logs_impresoras::observe(EventosObserver::class);
        Logs_reportes::observe(EventosObserver::class);
        Npasicaremp::observe(EventosObserver::class);
        Npcargos::observe(EventosObserver::class);
        Nphojint::observe(EventosObserver::class);
        permission::observe(EventosObserver::class);
        permission_user::observe(EventosObserver::class);
        Recargo::observe(EventosObserver::class);
        Segususuc::observe(EventosObserver::class);
        Tallas::observe(EventosObserver::class);
        User::observe(EventosObserver::class);
        Caja::observe(EventosObserver::class);
        Estado::observe(EventosObserver::class);
        ConfiguracionVenta::observe(EventosObserver::class);
        Estado::observe(EventosObserver::class);
        Pais::observe(EventosObserver::class);
        cajaUser::observe(EventosObserver::class);
        Costos::observe(EventosObserver::class);
        Retenciones::observe(EventosObserver::class);
        Casolart::observe(EventosObserver::class);
        Caartsol::observe(EventosObserver::class);
        Cacotiza::observe(EventosObserver::class);
        Caordcom::observe(EventosObserver::class);
        Caartord::observe(EventosObserver::class);
        Capagord::observe(EventosObserver::class);
        Cafacpag::observe(EventosObserver::class);
        Fanotaentrega::observe(EventosObserver::class);
        Caarttal::observe(EventosObserver::class);
        Cafacpag::observe(EventosObserver::class);

    }

    public function shouldDiscoverEvents()
    {
        return true;
    }

}
