<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;


class Fadefesc extends Model implements Auditable
{
    
    use SoftDeletes,\OwenIt\Auditing\Auditable;

    protected $connection='pgsql2';
    protected $table = 'fadefesc';

    protected $fillable =['codesc','nomesc','id','codigoid','codsuc_reg','created_at','updated_at','deleted_at','codart'];

    protected static $logAttributes = ['codesc','codtallas','id','cantidad','codigoid','codsuc_reg'];
    protected static $logName = 'fadefesc';
    protected static $logFillable = true;

    public function caregart()
   {
    return $this->belongsTo(Caregart::class,'codart');
   }

   public function fadetesc()
   {
   	return $this->hasMany(Fadetesc::class,'coddefesc')->whereDeletedAt(NULL);
   }

   public function esctallas(){
     return $this->hasMany('App\Fadetesc','codesc','codesc');
   }

   public function esctalla(){
     return $this->hasOne('App\Fadetesc','codesc','codesc');
   }

   public function getTallasAsociadas()
   {
   		return Fadetesc::where([['coddefesc',$this->codigoid],['deleted_at',NULL]])->orderby('codtallas')->get();
   }

    public function getArticulo()
   {
       return Caregart::where([['codart',$this->codart],['deleted_at',NULL]])->first();
   }

   
}
