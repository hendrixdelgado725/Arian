<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Fasubcategoria extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;
	protected $connection = 'pgsql2';
	protected $table = 'fasubcategoria';
    protected $fillable = ['nomsubcat','nomensubcat','id_facategoria','codigoid','codsuc'];
    

    protected static $logAttributes = ['nomsubcat','nomensubcat','id_facategoria','codigoid','codsuc'];
    protected static $logName = 'fasubcategoria';
    protected static $logFillable = true;

    public function categoria(){
        return $this->belongsTo('App\Facategoria','id_facategoria','codigoid');
    }

    public function articulos(){
        return $this->hasMany('App\Caregart','codsubcat','codigoid');
    }
}
