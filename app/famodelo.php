<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class famodelo extends Model implements Auditable
{	
	    use SoftDeletes,\OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql2';
    protected $table = 'famodelo';
    protected $fillable =['nomenclatura','defmodelo','nomencolor','nomencategoria','imgphoto','nomsubcat','codigomarca', 'codsuc'];



    public function scopeMarcas($query, $marcas)
	{
		return (!$marcas || count($marcas)=== 0 ) ? $query : $query->whereIn('codigoid',$marcas);
	}
    
}
