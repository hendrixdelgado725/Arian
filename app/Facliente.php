<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Facliente extends Model implements Auditable
{
    //
    use SoftDeletes,\OwenIt\Auditing\Auditable;

    protected $connection='pgsql2';
    protected $table = 'facliente';

    protected $fillable = [
        'codpro','nompro','rifpro','dirpro','telpro','email','codigoid','codsuc', 'stacli','deleted_at'
   ];


    protected static $logAttributes = ['codalm','nomalm','diralm'];
    protected static $logName = 'facliente';
    protected static $logFillable = true;



   public function factura(){
       return $this->hasMany('App\Fafactur','codcli','codpro');
   }

   public function presupuesto(){
       return $this->hasMany('App\Fapresup','codcli','codpro');
   }

   public function sucursal(){
       return $this->belongsTo('App\Sucursal','codsuc','codsuc');
   }



 
}
