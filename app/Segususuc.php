<?php

namespace App;
use App\Sucursal;

use Illuminate\Database\Eloquent\Model;

use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Segususuc extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    protected $table = "segususuc";
    protected $connection = 'pgsql';
    protected $fillable = [

        'loguse', 'codsuc','codigoid','codsuc_reg','id','created_at','updated_at','deleted_at','codroles','codusuario','activo'
    ];


    public function SucursalUse()
    {
    	return Sucursal::where([['codsuc',$this->codsuc]])->first();
    }

    public function role()
    {
    	return $this->hasOne(rolesusuarios::class,'codroles','codroles');
    }

    public function getSucursal()
    {
        return $this->hasOne(Sucursal::class,'codsuc','codsuc');
    }

    /*public function getrolUse()
    {
        return rolesusuarios::where('deleted_at',null)->first();
    }*/

    public function permission()
    {
        return $this->hasMany(permission_user::class,'codusuarios','codusuario');
    }

    public function user()
    {
        return $this->hasMany(permission_user::class,'codusuarios','codusuario');
    }
    public function usuario()
    {
        return $this->hasOne(User::class,'codigoid','codusuario');
    }

  
    public function getrolUse()
    {
        return rolesusuarios::where('deleted_at',null)->first();
    }
}
