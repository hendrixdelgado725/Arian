<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class permission_user extends Model implements Auditable
{
      use SoftDeletes,\OwenIt\Auditing\Auditable;


    protected $connection = 'pgsql';
    protected $table = 'permission_user';
    protected static $logAttributes = ['codpermission','codusuarios','rutasaccesso'];
    protected $fillable = ['codpermission','codusuarios','rutasaccesso', 'codigoid','codsuc_reg',
          'created_at'];
    protected static $logName = 'permission_user';
    protected static $logFillable = true;
    protected $dates = ['deleted_at'];

    public function user()
    {
    	return $this->belongsTo(Segususuc::class,'codusuarios','codusuario');
    }

    protected $guarded = [];
}
