<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Tstipmov extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $timestamps = false;
    protected $connection='pgsql2';
    protected $table = 'tstipmov';

    protected $fillable = [
        'codtip','destip','debcre','orden','escheque','codcon','tipo','pagnom','codtiptra','genmov','carban','esrein','estran'
   ];
}
