<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Famoneda extends Model implements Auditable
{
    //
    use SoftDeletes,\OwenIt\Auditing\Auditable;

    protected $table = 'famoneda';
    protected $connection ='pgsql2';

    protected $fillable = ([
        'nombre','nomenclatura','codigoid','codsuc','activo','deleted_at'
    ]);

    protected static $logAttributes = ['nombre','nomenclatura','codigoid','codsuc'];
    protected static $logName = 'famoneda';
    protected static $logFillable = true;

    public function precios(){
        return $this->hasMany('App\faartpvp','codmone','codigoid'); 
    }

    public function tasaM1(){
        return $this->hasMany('App\Fatasacamb','id_moneda','codigoid');
    }
    
    public function tasaM2(){
        return $this->hasMany('App\Fatasacamb','id_moneda2','codigoid');
    }
}
