<?php

namespace App\Policies;

use App\permission_users;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;



class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any posts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    private $guard;
    private $request;
    public function __construct(Guard $guard,Request $re)
    {
        $this->guard = $guard;
        $this->request = $re;
       
    }
    public function viewAny($seg)
    {
        
               $indice = 0;
       for ($i=0; $i < count($seg->userSegSuc->user); $i++) { 
            #for ($t=0; $t < count($this->guard->user()->userSegSuc->permission); $t++) { 

        #$temp = explode('-',json_decode($this->guard->user()->userSegSuc->permission[$t]->codpermission));
     
         if (strcmp($this->guard->user()->userSegSuc->permission[$indice]->rutasaccesso, "/".$this->request->path()) === 0 && strcmp($this->guard->user()->userSegSuc->codusuario, $seg->userSegSuc->user[$i]->codusuarios) === 0 && Str::contains($this->guard->user()->userSegSuc->permission[$indice]->codpermission,'contenidos.index-') && is_null($this->guard->user()->userSegSuc->permission[$indice]->deleted_at)) {
                    
                    return true;
                }
                $indice++;
            #}
        }
            return false;
    }

    /**
     * Determine whether the user can view the post.
     *
     * @param  \App\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function vista(User $user,permission_user $seg)
    {

        
        $paso = $this->guard->user()->permisos->filter(function($q){
            
            return $q->rutasaccesso === '/'.$this->request->path() && Str::contains($q->codpermission,'contenidos.index-');
        });
        if($paso->count() !== 0) return true;
        
        return false;
            
    }

 /*   public function filtrar($seg)
    {
         //dd();

         for ($i=0; $i < count($seg->userSegSuc->user); $i++) { 
            for ($t=0; $t < count($this->guard->user()->userSegSuc->permission); $t++) { 
                
                if (strcmp($this->guard->user()->userSegSuc->permission[$t]->rutasaccesso,"/".$this->request->path()) === 0 && strcmp($this->guard->user()->userSegSuc->codusuario, $seg->userSegSuc->user[$t]->codusuarios) === 0 && $this->guard->user()->userSegSuc->permission[$t]->codpermission === 'contenidos.show') {
                    return true;
                }
            }
        }

        
            return false;
            
    }
*/

    /*(strcmp($this->guard->user()->userSegSuc->permission[$t]->rutasaccesso, "/".$this->request->path()) === 0) && (strcmp($this->guard->user()->userSegSuc->codusuario, $seg->userSegSuc->user[$t]->codusuarios) === 0) && (Str::contains($this->guard->user()->userSegSuc->permission[$t]->codpermission,'contenidos.edit-') === 0) && (strcmp($this->guard->user()->userSegSuc->permission[$t]->deleted_at,null)=== 0)*/

    /*Str::contains($seg->userSegSuc->user[$i]->codpermission,$this->guard->user()->userSegSuc->permission[$indice]->codpermission)
        $this->guard->user()->userSegSuc->permission[$indice]->rutasaccesso '/'.$this->request->path(),


    */

    public function edit($seg)
    {     
    
        if($this->guard->user()->isAdmin()) return true;
        $paso = $this->guard->user()->permisos->filter(function($q){
            
            return $q->rutasaccesso === '/'.$this->request->path() && Str::contains($q->codpermission,'contenidos.edit-');
        });
        if($paso->count() !== 0) return true;
        
               

        
        
            return false;
            
       // $this->buscar($seg,null);
    }

    /**
     * Determine whether the user can create posts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create($seg)
    {
        if($this->guard->user()->isAdmin()) return true;
        
        $paso = $this->guard->user()->permisos->filter(function($q){
            
            return $q->rutasaccesso === '/'.$this->request->path() && Str::contains($q->codpermission,'contenidos.create-');
        });
        
        if($paso->count() !== 0) return true;
        
        return false;        
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param  \App\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function update(User $user, Post $post)
    {

        $this->authorize('update',$post);
    }

    /**
     * Determine whether the user can delete the post.
     *
     * @param  \App\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function delete($seg)
    {   
            

        if($this->guard->user()->isAdmin()) return true;

        $paso = $this->guard->user()->permisos->filter(function($q){
            
            return $q->rutasaccesso === '/'.$this->request->path() && Str::contains($q->codpermission,'contenidos.delete-');
        });
        if($paso->count() !== 0) return true;
        
        return false;
    }

    /**
     * Determine whether the user can restore the post.
     *
     * @param  \App\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function restore(User $user, Post $post)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the post.
     *
     * @param  \App\User  $user
     * @param  \App\Post  $post
     * @return mixed
     */
    public function forceDelete(User $user, Post $post)
    {
        //
    }
}
