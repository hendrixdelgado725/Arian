<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class NotaEntrega extends Model
{
  use SoftDeletes;
    protected $connection='pgsql2';
    protected $table = 'nota_entrega';
    protected $fillable = [
       'id','numero','almori','almdes','cod_cliente','fatasacamb','codigoid','total','observacion','created_by','codsuc','cambio','facturado','reffac',
       'extraido', 'estatus', 'vencimiento', 'tipo', 'documento', 'descuento',
   ];


  public function articulos(){
  	return $this->hasMany('App\NotaEntregaArt','numero_nota','numero');
  }

  public function almorigen (){
  	return $this->belongsTo('App\Almacen','almori','codalm');
  }

  public function almdestino(){
  	return $this->belongsTo('App\Almacen','almdes','codalm');
  }

  public function cliente(){
  	return $this->belongsTo('App\Facliente','cod_cliente','codpro');
  }

  public function autor(){
    return $this->belongsTo('App\User','created_by','codigoid');
  }

  public function tasa(){
    return $this->belongsTo('App\fatasacamb','fatasacamb','codigoid');
  }

 public function sucursales(){
    return $this->belongsTo('App\Sucursal','codsuc','codsuc');
  }
}
