<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;


class Npcargos extends Model implements Auditable
{
    use SoftDeletes,\OwenIt\Auditing\Auditable;
    
    protected $connection='pgsql2';
    protected $table = 'npcargos';

    protected $fillable = [
          'nomcar','codcar','suecar','stacar','codigoid','codsuc'
    ];

    protected static $logAttributes = ['nomcar','codcar','suecar','stacar','codigoid','codsuc'];
    protected static $logName = 'npcargos';
    protected static $logFillable = true;

    public function empleadosCargos(){
        return $this->hasOne('App\Npasicaremp','codcar','codcar');
    }


}
