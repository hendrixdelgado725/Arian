<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Tsdefban extends Model implements Auditable
{
    use SoftDeletes,\OwenIt\Auditing\Auditable;

    public $timestamps = false;
    protected $connection='pgsql2';
    protected $table = 'tsdefban';

    protected $fillable = [
        'numcue','nomcue','tipcue','codcta','fecreg','fecven','fecper','renaut','porint','tipint','numche','antban','debban','creban','antlib','deblib','crelib','valche','concil','plazo','fecape','usocue','tipren','desenl','porsalmin','monsalmin','codctaprecoo','codctapreord','trasitoria','salact','fecaper','temnumcue','cantdig','endosable','salmin','nomrep','codcom','codtiptra','codadi','codubi','coddirec','conformable','monconfor','agenban'
   ];
}
