<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Alma_ubi extends Model implements Auditable
{	
	use SoftDeletes;
	use \OwenIt\Auditing\Auditable;

	protected $connection = 'pgsql2';
    protected $table = "caalmubi";
	protected $fillable = ['id','codalm','codubi','codsuc'];

	public static function getByCod($codalm,$codubi,$id){
		return self::where('codalm',$codalm)->where('codubi',$codubi)->where('id',$id)->first();
	}
}