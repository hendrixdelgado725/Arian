<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class contabb extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $timestamps = false;
    protected $connection='pgsql2';
    protected $table = 'contabb';

    protected $fillable = [
        'codcta','descta','fecini','feccie','salant','debcre','cargab','salprgper','salacuper','salprgperfor','codigoid','codsuc'
   ];
}
