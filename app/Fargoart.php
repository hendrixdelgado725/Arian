<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fargoart extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    protected $connection='pgsql2';
    protected $table = 'fargoart';

    protected $fillable =['codrgo','codart','refdoc','monrgo','tipdoc','id','desart','codcaj','codrecarg','codigoid','codsuc_reg'];

   public function tiporecargo(){
    return $this->hasOne('App\Farecarg','codrgo','codrgo');
   }
}
