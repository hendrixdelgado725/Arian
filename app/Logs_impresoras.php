<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
//use Spatie\Activitylog\Traits\LogsActivity;
//use OwenIt\Auditing\Contracts\Auditable;

class Logs_impresoras extends Model
{
	//use SoftDeletes;

    protected $connection='pgsql';
    protected $table = 'logs_impresoras';
    protected $fillable = [
      'factura_id',
      'numero_factura',
      'numero_devolucion',
      'error',
      'serial_impresora',
      'fecha',
      'hora',
      'created_at',
      'updated_at',
      'id',
      'ajuste_id',
   ];


}
