<?php

namespace App;

use App\Scopes\SucursalScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Contracts\Auditable;

class Caentalm extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    protected static function boot()
    {
        parent::boot();

       // static::addGlobalScope(new SucursalScope);
    }

    protected $connection = 'pgsql2';

    protected $table = 'caentalm';

    protected $dates = ['fecapro'];

    protected $fillable = [
        'codmov',//ENT SAL TRA
        'rcpart',
        'fecrcp',
        'desrcp',
        'codpro',
        'monrcp',
        'starcp',
        'codalm',
        'codubi',
        'tipmov',
        'codcen',
        'dphart',
        'numcom',
        'codalmusu',
        'created_by_user',
        'updated_by_user',
        'codsada',
        'nroentdes',
        'nrocarveh',
        'nrocontro',
        'coddirec',
        'fecanu',
        'usuanu',
        'nommov',
        'stamov',//estatus del movimiento
        'aproved_by_user',
        'fecapro',
        'status',
        'codigoid',
        'codsuc',
        'moneda',
        'fecven'
    ];


    public static function GetLastCode(){
        return self::get()->count();
    }

    public function almacenes(){
        return $this->hasOne(Almacen::class,'codalm','codalm');
    }

    public function proveedor(){
      return $this->hasOne(Caprovee::class,'codigoid','codpro');
    }

    public function coin(){
      return $this->hasOne(Famoneda::class,'codigoid','moneda');
    }

    public function movimientos(){
        return $this->hasOne(Catipent::class,'codtipent','tipmov');
    }
    public static function getFull(){
        return self::where('codsuc',session('codsuc'))->with('almacenes')->with('movimientos')
        ->select('*',
                DB::raw("to_char(fecrcp,'dd/mm/yyyy') as fecrcpnormal"),
                DB::raw("(CASE WHEN nommov = 'ENT' THEN 'ENTRADA'
                            WHEN nommov = 'SAL' THEN 'SALIDA' END)as movimiento "))
        ->orderByDesc('id')->paginate(10);
    }

    public static function findByCod($codmov){
        return self::where('codmov',$codmov)->first();
    }

    public static function findByCodDetalle($codmov){
        return self::select([
            '*',
            DB::raw("to_char(fecrcp,'dd/mm/yyyy') as fecrcpnormal"),
            DB::raw("to_char(fecrcp,'dd/mm/yyyy') as fecanuhuman"),
            DB::raw("(CASE WHEN nommov = 'ENT' THEN 'ENTRADA' WHEN nommov = 'SAL' THEN 'SALIDA' END)as movimiento ")
        ])->where('codmov',$codmov)->with('proveedor')->with('coin')->first();
    }

    public function movarticulos(){
      return $this->hasMany('App\Caartalmubimov','codmov','codmov');
    }

    public function articulosPro()
    {
        return $this->hasOneThrough(\App\Caregart::class,\App\Caartalmubimov::class,'codmov','codart','codmov','codart');
    }

    public function tallasPro()
    {
        return $this->hasOneThrough(\App\Tallas::class,\App\Caartalmubimov::class,'codmov','codtallas','codmov','codtalla');
    }

    public function articulos(){
      return $this->hasManyThrough('App\Caregart','App\Caartalmubimov','codmov','codart','codmov','codart');
    }

    public function tallas(){
      return $this->hasManyThrough('App\Tallas','App\Caartalmubimov','codmov','codtallas','codmov','codtalla');
    }

    public static function findByCodEmail($codmov){
        return self::with(['almacenes' => function($q){
            $q->select(['codalm','nomalm']);
        }
        ])->where('codmov',$codmov)->first();
    }


    public function scopeDate($query,$minDate = null,$maxDate = null){
        return ($minDate != null && $maxDate != null) ? $query->whereDate('created_at','>=',$minDate)->whereDate('created_at','<=',$maxDate) : $query;
    }

    public function scopeAlmacen($query,$codalm = null){
        return ($codalm != null) ? $query->where('codalm',$codalm) : $query;
    }

    public function scopeCodigo($query,$codFrom = null,$codTo = null){
        return ($codFrom != null && $codTo != null) ? $query->where('codmov','>=',$codFrom)->where('codmov','<=',$codTo) : $query;
    }

    public function scopeTipmov($query,$tipmov = null){
        return ($tipmov !== null) ? $query->where('nommov',$tipmov) : $query;
    }


}
