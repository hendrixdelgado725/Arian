<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\Serial;
use Carbon\Carbon;

class Fafactur extends Model implements Auditable
{
	use SoftDeletes,\OwenIt\Auditing\Auditable;

    protected $connection='pgsql2';
    protected $table = 'fafactur';
    protected $fillable = [
        'reffac','codcli','monfac','mondesc','reapor','recargo',
        'monrecargo','monsubtotal','fecaun','status','tipmon',
        'valmon','id','codcaj','codsuc','caja','impserial','codigoid','cod_recargo','tasa',
        'num_fiscal','num_dev_fiscal','cajaid','num_fiscal_dev','is_fiscal','created_at','cierre'
   ];

   public function cliente(){
   	return $this->belongsTo('App\Facliente','codcli','codpro');
   }

   public function defcaja(){
    return $this->belongsTo('App\Models\Caja','codcaj','id');
   }

   public function caja1(){
   	return $this->belongsTo('App\Models\Caja','caja','descaj');
   }
   public function caja(){
    return $this->belongsTo('App\Models\Caja','caja','descaj');
    }

   public function sucursales(){
   	return $this->belongsTo('App\Sucursal','codsuc','codsuc');
   }

/*   public function articulos(){
   	return $this->hasMany('App\Fafactuart','reffac','reffac');
   }*/

   public function articulos(){
    return $this->hasMany('App\Faartfac','reffac','reffac');
   }

/*   public function pagos(){
   	return $this->hasMany('App\Fafacturpago','reffac','reffac');
   }
*/
   public function pagos(){
    return $this->hasMany('App\Faforpag','reffac','reffac');
   }

   public function cajero(){
    return $this->belongsTo('App\User','reapor','loguse');
   }

   public function recargo(){
    return $this->belongsTo('App\Fadescto','cod_recargo','codigoid');
   }

  public function fargoarticulo(){
    return $this->belongsTo('App\Fargoart','reffac','refdoc');
   }

   public function tasaCambio()
   {
      return $this->belongsTo(Fatasacamb::class,'tasa','codigoid')->select(['valor','codigoid']);
   }

   public function scopeDate($query,$minDate = null,$maxDate = null){
      return ($minDate != null && $maxDate != null) ? $query->whereDate('created_at','>=',date($minDate))->whereDate('created_at','<=',date($maxDate)) : $query;

   }

   public function scopeCajafac($query,$codcaj){

      return ($codcaj != 0) ? $query->where('codcaj',$codcaj) : $query;
   }

   public function scopeSucursal($query,$codsuc){
      //return (!Auth::user()->isAdmin() || session('codsuc')) ? $query->where('codsuc',$codsuc) : $query;
   }

   public function Serial()
   {
      return $this->hasMany(Serial::class,'id_factura','id');
   }
   public function Serial2()
   {
      return $this->hasMany(Serial::class,'factura','reffac');
   }
   public function notaCredito()
   {
      return $this->hasMany('App\Fanotcre','reffac','reffac');
   }
   public function notaCredito2()
   {
      return $this->hasOne('App\Fanotcre','reffac','reffac');
   }

   public function scopeDate2($query,$minDate = null,$maxDate = null)
   {
      return ($minDate != null && $maxDate != null) ? $query->where('fecfac','>=',date($minDate))
                                                      ->where('fecfac','<=',date($maxDate))
                                                      ->orWhere('fecanu','>=',date($minDate))
                                                      ->where('fecanu','<=',date($maxDate))
                                                        : $query;

   }
}
