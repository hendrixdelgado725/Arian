<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Cobrecdoc extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public $timestamps = false;
    protected $connection='pgsql2';
    protected $table = 'cobrecdoc';

    protected $fillable = [
        'refdoc','codcli','codrec','fecrec','monrec',
   ];
}
