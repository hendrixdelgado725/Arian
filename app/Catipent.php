<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Catipent extends Model implements Auditable
{
	 use SoftDeletes,\OwenIt\Auditing\Auditable;
    protected $connection = 'pgsql2';

    protected $table = 'catipent';

    protected $fillable = ['codtipent','destipent','id','created_at','updated_at','deleted_at','codigoid','codsuc_reg'];
}
