<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Caprovee extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'caprovee';
    protected $connection = 'pgsql2';
    protected $fillable = ['codpro','nompro','rifpro','codigoid','codsuc', 'nitpro', 'nacpro', 'email', 'tipo', 'estpro', 'dirpro', 'telpro', 'fecreg'];



    public static function getcodigoid(){
        return self::withTrashed()->get()->isEmpty() ? '1'  : self::withTrashed()->get()->count()+1;
    }

}
