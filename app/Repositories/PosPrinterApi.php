<?php


namespace App\Repositories;

use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\CapabilityProfile;
use Mike42\Escpos\Printer;
use App\Fafactur;
use App\Sucursal;
use App\Models\Caja;
use App\Events\SendToPrint;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;
use App\Models\logs_reportes;
use App\Models\Logs_impresora_api;

class PosPrinterApi
{
  public function __construct()
  {

  }


  public function cutString($direccion = '')
  {
    return $direccion !== '' ? substr($direccion,0, 28) : 'PUNTO FIJO';
  }


  public function sendRequest(string $reffac, string $caja)
  {
    
    
    
    try {
      $newCaja = Caja::where('id',$caja)->first();
    if(!$newCaja){
      return response()->json([
        "msg"       => "Error Critico, No se obtuvo la caja para facturar",
        "errorCode" => 500
      ]);
    }
    
    
    $factura = Fafactur::where([
      ['reffac','=',$reffac],
      
      ])
    ->where('is_fiscal',false)
    //->where('numero_ticket','<>',null)
    ->with('cliente','articulos.articulo','pagos.monedas','Serial2','tasaCambio')
    ->first();
      
    

      $sucursal  = Sucursal::where('codsuc',$factura->codsuc)->first();

      if(!$factura)throw new Exception("Factura No se encuentra");
    
      (array) $array_to_send = [];/* info a enviar a la impresora fiscal */

    $sucursal  = Sucursal::where('codsuc',$factura->codsuc)->select('nomsucu','dirfis')->first();
    
    
    $array_to_send["factura"] = [
      'numero'         => $factura->reffac,//para que salga el número de factura devuelto
      'factura_fiscal' => $factura->num_fiscal,
      'fecha'          => $factura->created_at->format('d/m/y'),
      'razon_social'   => optional($factura->cliente)->nompro,
      'documento'      => optional($factura->cliente)->rifpro,
      'direccion'      => utf8_encode($this->cutString(optional($factura->cliente)->dirpro)),
      'tasa'           => optional($factura->tasaCambio)->valor
    ];
    
    
    
    foreach ($factura->articulos as $articulo){

      $iva = $factura->porrecargo;
      //$calculo = $iva != 0 ? ($articulo["costo"]*$iva)/100 : 0;
      $calculo = ($articulo->articulo->ivastatus === "true") ? ($articulo["costo"]*$iva)/100 : 0;

      /* Se calcula la cantidad recargada por el iva */
      $unitDiscount = $articulo["mondes"] != "0.00" ?  (float) $articulo["porcentajedescuento"] : (int) 0;
      /* Se calcula el nuevo precio de producto según descuento */
      
      $precioUnit = (float)$articulo["precio"]-(($unitDiscount*$articulo["precio"])/100);
      $desProd = is_numeric(substr($articulo["desart"],0,1)) ? 'P'.$articulo["desart"] : $articulo["desart"];
      //$desProd = is_numeric(substr($articulo["descrip_art"],0,1)) ? 'P'.$articulo["descrip_art"] : $articulo["descrip_art"];

      /* solo hace falta el precio unitario asi tenga descuento la impresora fiscal ejecuta la operacion */
      $desProd = substr($desProd,0, 90);
      $factura = Fafactur::where('reffac',$articulo['reffac'])->first();
      $serial = \App\Models\Serial::where('codart',$articulo['codart'])
      ->where('factura',$factura->reffac)
      ->get();//muestra los seriales a facturar

      $seriales = '';
      $cpt = "";
      if($serial->isEmpty() === false){

          foreach($serial as $s){


          if($s->serial !== "") $cpt .= $s->serial.',';
          else $cpt .= '';
          }
          $seriales .= $cpt;

      }else{
        $seriales = ',';
      }

      $productos[] = [
        "descripcion" => (string) $desProd,
        "cantidad"    => (float)  $articulo['cantot'],
        "precio"      => (float)  number_format($precioUnit,2,'.',''),
        "iva"         => (string) $articulo->articulo->ivastatus === "true" ? $factura->porrecargo : "empty",
        "seriales"    => (string) $seriales
      ];


    }
    
    
    $array_to_send["detalle"] = $productos;
    

    /* Esta ruta es estática */   
    $response = Http::get('http://'.$newCaja->impfishost.'/print',$array_to_send);
    $getResponse = $this->getResponse($response->json()['codeError']);
    
  //  dd($getResponse);
    if($getResponse !== true){
      $getResponse["errorCode"] = $response->json()['codeError'];
      throw new \Exception( $getResponse['msg']);
      
    }

    $search = Fafactur::where([
      ['reffac','=',$reffac],
      ['status','A']
    ])->update([
      
      'numero_ticket' =>  str_replace($caja.'T',str_pad("0",3,0,STR_PAD_LEFT),$factura->reffac)
    ]);

    Logs_impresora_api::create([
      'factura_sistema' => $factura->reffac,
      'factura_fiscal'  => str_replace($caja.'T',str_pad("0",3,0,STR_PAD_LEFT),$factura->reffac),
      'tipo'            => 'ticket'
    ]);

    $newCaja->corfac = str_replace($caja.'T',str_pad("0",3,0,STR_PAD_LEFT),$factura->reffac);
    $newCaja->save();
    
    return $response->successful();

  } catch (\Exception $th) {
      return [
               'errorCode' => 500,
               'msg' => $th->getMessage()
             ];
    }

  }

  function cierre($idCaja,$diaAnt=null) {
    
      
      

    
    
    try {
      
        $newCaja = Caja::where('id',(int)$idCaja)->first();
        
        if(!$newCaja) throw new Exception("Error Critico, No se obtuvo la caja para facturar",0);
        
        $info = [
          'caja' => $idCaja,
          'user' => Auth::user()->loguse,
          'diant' => $diaAnt
        ];
        
        
        $response = Http::get('http://'.$newCaja->impfishost.':8000/cierre',$info);
        
        $getResponse = $this->getResponse($response->status());

        if($getResponse !== true){
          
          throw new \Exception("Error al momento de iniciar el cierre");
          
        }

        return [
          'msg'   => 'Cierre con exito',
          'error' => false
        ];

      } catch (\Exception $th) {
          return [

            'error' => true,
            'msg' => $th->getMessage()
          ];
      }

      

  }



  public function getResponse($response)
  {
    switch ($response) {
      case 200:
        return true;
      case 404:
        return ['msg' => 'Ip no encontrada'];
      case 500 || 400:
        return ['msg' =>'Error al facturar en el cliente, revise la configuracion de la caja'];
    }
  }

}