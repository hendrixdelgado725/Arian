<?php


namespace App\Repositories;

use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\CapabilityProfile;
use Mike42\Escpos\{Printer,EscposImage};
/*  */
use App\Fafactur;
use App\Sucursal;
use App\Models\Caja;
use App\Events\SendToPrint;
use Illuminate\Support\Arr;
use App\Empresa;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\cierre_log;
class PosPrinterRepository
{

  protected $connector;
  protected $printer;
  protected $port;
  protected $data;
  protected $jump;

  protected function setConnectionConfig()
  {
    #$connector     = new WindowsPrintConnector("/dev/usb/lp0");
    $profile = CapabilityProfile::load("simple");
    $connector = new FilePrintConnector("/dev/usb/lp1");
    $this->printer = New Printer($connector,$profile);

  }

  protected function setMainPrinterConfig()
  {
    $this->printer->initialize();
    
  }
  /*NOTA: CAMBIAR IVA CUANDO SE TENGA QUE CAMBIAR 
  POR EJEMPLO:  IVA 16% = 0.16
  DONDE DICE 0.16 CAMBIAR EN CASO DE QUE CAMBIEN EL IVA
  
  */

  public function printInfo($reffac,$caja,$isFiscal)
  {
      
    //$this->
    try{

      //dd((int)$caja);

       
      
      //falta incrementacion de correlativo de tickera
      $factura = Fafactur::where([
        ['reffac','=',$reffac],
        
        ])->with('cliente','articulos.articulo','pagos.monedas','Serial2')
        ->first();
        #dd(Fafactur::where('is_fiscal',false)->get()->count());
        $search = Fafactur::where([
          ['reffac','=',$reffac]
        ])->update([
          'is_fiscal' => false,
          'numero_ticket' =>  str_replace($caja.'T',str_pad("0",3,0,STR_PAD_LEFT),Fafactur::where('is_fiscal',false)->where('codcaj',$caja)->orderby('id','DESC')->first()->reffac)
        ]);
      $sucursal  = Sucursal::where('codsuc',$factura->codsuc)->first();

      if(!$factura)throw new Exception("Factura Not Found");
     

      $this->setConnectionConfig();
      $this->setMainPrinterConfig();
      $this->header($factura, $sucursal);
      $this->printer->setEmphasis(false);
      $this->clientData($factura);
      
      $this->printer->text("                                               \n");
      $this->printer->selectPrintMode(Printer::MODE_FONT_A);
      $this->printer->setTextSize(1,1);
      
      $total = 0;
      $totalD = 0;
      $cant = 0;
      $this->printer->setJustification(Printer::JUSTIFY_RIGHT);
      $this->printer->setTextSize(2,1);
      
      $this->printer->text("RECIBO DE VENTA\n");
      $this->printer->setTextSize(1,1);
      $this->printer->selectPrintMode(Printer::MODE_FONT_A);
      $this->printer->setJustification(Printer::JUSTIFY_LEFT);
      $this->printer->text("RECIBO:                                 ".str_pad($factura->reffac,7,'0',STR_PAD_LEFT)."\n");
      $this->printer->text("FECHA EMISION:                        ".$factura->fecfac."\n");

      $this->printer->setJustification(Printer::JUSTIFY_CENTER);
      $this->printer->text("\n");
      $this->printer->selectPrintMode(Printer::MODE_FONT_A);
      $this->printer->setUnderline(2);
      $this->printer->text("CANT              PRECIO                NETO\n");
      foreach ($factura->articulos as $key => $value) {

        $this->printer->setJustification(Printer::JUSTIFY_LEFT);
        $this->printer->setUnderline(0);

        
          $this->printer->text("  ".$value->cantot."              ".number_format($value->precio - (($value->precio * $value->porcentajedescuento)/100),2,',','.')."            \n");  
          
          $this->printer->text("  ".substr(optional($value->articulo)->desart,0,20)." (E)".str_pad("",14).number_format((($value->cantot * ($value->precio - (($value->precio * $value->porcentajedescuento) / 100)))),2,',','.')."\n");
          
          $total += (($value->cantot * ($value->precio - (($value->precio * $value->porcentajedescuento) / 100))));

          $totalD += (($value->cantot * ($value->precio - (($value->precio * $value->porcentajedescuento) / 100)))) / $factura->tasaCambio->valor;
        

        // if(optional($value->articulo)->ivastatus === "true"){
        //   $this->jump(
        //     optional($value->articulo)->desart." X ".$value->cantot." X ". "IVA 16%". number_format(floatval(($value->precio * 0.16) + $value->precio),2,',','.'),
        //     number_format($value->precio,2,',','.').'Bs.S'
        //   );
        // }else{
        //   $this->jump(
        //     optional($value->articulo)->desart." X ".$value->cantot,
        //     number_format($value->precio,2,',','.').'Bs.S'
        //   );
        // }

        
        $this->printer->text(
          $this->calculateString(
            optional($value->articulo)->desart." X ".$value->cantot,
            number_format($value->precio,2,',','.').'Bs.S'
        )."\n");

        if($factura->Serial2 !== null){
          $serial = 'S/N: ';
          $seriales = \App\Models\Serial::where('factura',$factura->reffac)->where('codart',optional($value->articulo)->codart)->get();
          foreach ($seriales as $key2 => $value2) {
              $serial .= $value2->serial."\n";
          }
          //$this->printer->text($serial."\n"); imprimir seriales
        }
        $cant++;
        // if(optional($value->articulo)->ivastatus === "true"){
        //   $p = ($value->precio * $value->cantot);
        //   $this->printer->text(number_format(($p * 0.16) + $p,2,',','.').' Bs.S');

        // }else{
        //   //$this->printer->text(number_format($value->precio * $value->cantot,2,',','.').' Bs.S');

        // }
        $this->printer->text("\n");
        
      }
      $this->printer->feed(1);
      $this->printer->setUnderline(2);
      $this->printer->text("                                               \n");
      $this->printer->setUnderline(2);
      $this->printer->setJustification(Printer::JUSTIFY_LEFT);
      //$this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
      $this->printer->text("Total de articulos de esta Venta: ".$cant."\n");
      $this->printer->feed(2);
      $this->printer->setUnderline(0);
      $this->printer->text(
 
          "TOTAL BS:                             ".
          number_format($total,2,',','.')
 
        ."\n");
      $this->printer->feed(1);
      $this->printer->text(
          
            "TOTAL $:                               ".
            number_format($totalD,2,',','.')
          
          ."\n");
        
        $this->printer->setUnderline(1);
        $this->printer->text("\n");
        
        $this->printer->text("! Gracias por su compra ¡- Vuelva Pronto");
        $this->printer->feed(2);
        $this->printer->cut(Printer::CUT_PARTIAL,35);
        //$this->printer->feed(25);
        $this->printer->close();
        
        $int = (int)$caja > 10 ? 5 : 6;

        $caja1 = Caja::find((int)$caja);
        $caja1->corfac = str_replace($caja.'T',str_pad("0",3,0,STR_PAD_LEFT),Fafactur::where('is_fiscal',false)->where('codcaj',$caja)->orderby('id','DESC')->first()->reffac);
        $caja1->save();
        

    }catch(\Exception $e){
        \Log::info($e);
      return $e->getMessage();
    }finally{
        $this->printer->close();
    }
    return true;
  }

  public function header($factura, $sucursal)
  {
    $this->printer->setJustification(Printer::JUSTIFY_CENTER);
    $img = EscposImage::load(__DIR__."/logo1.png");
    $this->printer->bitImage($img);
    $this->printer->text("\nJ503879726"."\n");
    //$this->printer->text($sucursal->nomsucu." C.A."."\n");
    $this->printer->text("MR PETS PLACE C.A\n");
    $this->printer->text("CALLE ESCUQUE E/GENERAL PELAYO Y OLLARVIDES CC VICTORIA MALL NIVEL P/A LOCAL 209 \nSECTOR PUERTA MARAVEN PUNTA CARDON FALCON ZONA POSTAL 4154\n");
    $this->printer->setEmphasis(true);
    $this->printer->text("Telefono:  04124829526"."\n");
    $this->printer->text("Instagram: @mr.petpf"."\n");
//    $this->printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $this->printer->selectPrintMode();
    $this->printer->feed(1);
  }


  public function jump(string $desc, string $costo)
  {
    strlen($desc)+strlen($costo) < 47 ?: $this->printer->text("\n");
  }

  public function calculateString( string $desart, string $precio, bool $total = false)
  {
    $maxLength  = $total ? 47 : 47;//desabilitado
    $longitud   = strlen($desart) + strlen($precio);
    if($longitud < $maxLength){
      $string   = array_fill(0,$maxLength - (int)$longitud,' ');
      $string   = implode('',$string);
    }else{
      $string   = array_fill(0,$maxLength - strlen($precio),' ');
      $string   = implode('',$string);
    }

    return $string;

  }
  
  public function clientData($factura)
  {
    $cliente = $factura->cliente;
    $this->printer->setJustification(Printer::JUSTIFY_LEFT);
    $this->printer->selectPrintMode(Printer::MODE_FONT_A);
    $this->printer->setTextSize(1,1);
    $this->printer->text("CLIENTE: ".$cliente->nompro."\n");
    $this->printer->text("RIF: ".$cliente->rifpro."\n");
    $this->printer->text("DIRECCION: ".$cliente->dirpro."\n");
    $this->printer->text("TASA DEL DIA: ".$factura->tasaCambio->valor."\n");
  }


  function cierre($idCaja) {
    $loguse = Auth::user()->loguse;
    $caja = \App\Models\Turno::where('caja',$idCaja)->with(['caja.cajaUser' => function($q) use ($loguse){
        $q->where('loguse',$loguse);
    }])->first();
    
    $Info['Info'] = [
      'caja'     => $caja->caja,
      'inicio'   => $caja->inicio,
      'Turno'    => $caja->tipoturno,
      'nombre'   => Auth::user()->nomuse,
      'numero'    => $caja->numtur,
    ]
    ;

    $newCaja = Caja::where('id',(int)$caja->caja)->first();
    

    try {
      
      if(!$newCaja) throw new Exception("Error Critico, No se obtuvo la caja para facturar",0);
      
      
      $facturas = Fafactur::
        with(
        'tasaCambio',
        'caja',
        'sucursales',
        'pagos.tipomoneda',
        'pagos.tasamoneda',
        'fargoarticulo'
        )
        ->whereHas('pagos')
        ->where('codcaj',(int)$caja->caja)
        ->where('is_fiscal',$newCaja->is_fiscal)
        ->where('turnos',strtolower($Info['Info']['Turno']))
        ->whereDate('fecfac','>=',Carbon::today()->toDateString())
        ->whereDate('fecfac','<=',Carbon::today()->toDateString())
        ->orWhereDate('fecanu','>=',Carbon::today()->toDateString())
        ->whereDate('fecanu','<=',Carbon::today()->toDateString())
        ->where('is_fiscal',$newCaja->is_fiscal)
        ->where('turnos',strtolower($Info['Info']['Turno']))
        ->where('codcaj',(int)$caja->caja)
        ->orderBy('created_at','asc')->get();

        
          
        

        // if($facturas->isEmpty()) throw new \Exception(
        //    "Error, No ha facturado por eso no se puede general el cierre ",0
        // );
        
        /*$pagosE = $factura->map(function ($q) {
            return $q->pagos;
        });*/

        $fechas = $facturas->groupBy(function ($val)
        {
           return Carbon::parse($val->created_at)->format('Y-m-d');
        });
        
        
        $this->setConnectionConfig();
        $this->setMainPrinterConfig();
        $this->printer->setJustification(Printer::JUSTIFY_CENTER);

        $img = EscposImage::load(__DIR__."/logo1.png");
        $this->printer->bitImage($img);
        $leng = cierre_log::where('caja',$caja->caja)->count(); //toma el tamaño de los registro de esa caja para colocarlo de correlativo de cierre
        $int = $caja->caja > 10 ? 5 : 6;
        
        //dd($facturas->isEmpty());
        $this->printer->text("\nJ503879726"."\n");
        $this->printer->text("MR PETS PLACE C.A\n");
        $this->printer->text("CALLE ESCUQUE E/GENERAL PELAYO Y OLLARVIDES CC VICTORIA MALL NIVEL P/A LOCAL 209 \nSECTOR PUERTA MARAVEN PUNTA CARDON FALCON ZONA POSTAL 4154\n");
        $this->printer->feed(1);
        $this->printer->setJustification(Printer::JUSTIFY_LEFT);
        $this->printer->setTextSize(1,1);
        $this->printer->text('CIERRE Nº: '. str_pad($leng,$int,'0',STR_PAD_LEFT));
        $this->printer->feed(1);
        $this->printer->text('caja: '.$Info['Info']['caja']);
        $this->printer->feed(1);
        $this->printer->text('Fecha: '.Carbon::now()->format('Y-m-d'));
        $this->printer->feed(1);
        $this->printer->text('Turnos: '.$Info['Info']['Turno']);
        $this->printer->feed(1);
        $this->printer->text('Cajero: '.$Info['Info']['nombre']);
        $this->printer->feed(1);
        $this->printer->text('Numero Turno: '.$Info['Info']['numero']);
        $this->printer->feed(2);
        
        if(!$facturas->isEmpty()){
          
          $this->printer->text('Primera Factura: '.$facturas->first()->reffac);
          $this->printer->feed(2);
          $this->printer->text('Ultima Factura:  '.$facturas->last()->reffac);
        }
        $this->printer->feed(4);
        $this->printer->text('---------------Denominaciones--------------');
        $this->printer->feed(4);

        if($facturas->isEmpty()) {
          $this->printer->feed(1);
          $this->printer->text("RESUMEN");
          $this->printer->feed(1);
          $this->printer->text('TOTAL: '. number_format(0,2,',','.'));
          $this->printer->feed(2);
          $this->printer->cut(Printer::CUT_PARTIAL,35);
          $this->printer->feed(2);
          $this->printer->close();
          cierre_log::create([
            'caja'      => $Info['Info']['caja'],
            'numero'    => $Info['Info']['numero'],
            'tipoturno' => $Info['Info']['Turno']
          ]); 
  
          
          $caja->numtur = str_pad($Info['Info']['numero']+1,$int,'0',STR_PAD_LEFT);
          $caja->save();
          return [
            'error' => false,
            'msg' => 'Se ha generado el Cierre con Exito'];
        }


        foreach ($fechas as $key => $value) {


          $pagosBs = 0;
          $pagosD = 0;
          $tpago = '';
     
     
          foreach ($value->flatten() as $pago) {
             $tasa = \App\Fatasacamb::where('codigoid',$pago->tasa)->first()->valor;
     
           if($pago->status === 'N'){//facturas NC Y ANULADAS
              if($pago->fecfac === $pago->fecanu){
     
                 foreach ($pago->pagos->flatten() as $pagos) {
                   
     
         //          $pdf->Cell(40,8,utf8_decode($key),1,0,'C');
                   $this->printer->text($pagos->monpag.'         '.$pagos->montocambio.'        '.$pagos->fpago);
                   $this->printer->feed(1);           
                   $this->printer->text(($pagos->monpag*-1).'       '.($pagos->montocambio*-1).'        '.$pagos->fpago);
                   $this->printer->feed(1);
                   
                   
                }
     
             }else{
                    if($pago->fecfac !== $pago->fecanu && $desde === $hasta){//en caso de se halla devuelto la factura y consulta un dia especifico
                        if ($desde >= $pago->fecanu && $desde > $pago->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                          foreach ($pago->pagos->flatten() as $pagos) {
                            $this->printer->text($pagos->monpag.'       '.$pagos->montocambio.'       '.$pagos->fpago);
                            $this->printer->feed(1);
                          }
                        }else if($desde <= $pago->fecfac){
                          
                          foreach ($pago->pagos->flatten() as $pagos) {
     
                            $this->printer->text($pagos->monpag.'       '.$pagos->montocambio.'       '.$pagos->fpago);
                            $this->printer->feed(1);
                          }
     
                        }
     
                  }else if($pago->fecfac !== $pago->fecanu && $desde !== $hasta){//en caso de se halla devuelto la factura y consulta en dias diferentes
     
     
                      if ($desde <= $pago->fecfac && $desde <= $pago->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                       foreach ($pago->pagos->flatten() as $pagos) { //quede aqui
                           
                        $this->printer->text($pagos->monpag.'       '.$pagos->montocambio.'       '.$pagos->fpago);

                        $this->printer->feed(1);
                      
                        $this->printer->text(($pagos->monpag*-1).'        '.($pagos->montocambio*-1).'        '.$pagos->fpago);
                        $this->printer->feed(1);
                       }
                      }else if($desde <= $pago->fecanu){
                         //quede aqui
                      
                        foreach ($pago->pagos->flatten() as $pagos) {
                          $this->printer->text(($pagos->monpag*-1).'        '.($pagos->montocambio*-1).'        '.$pagos->fpago);
                          $this->printer->feed(1);
                        }
                      }
                    }
     
             }
           }else{//FACTURA ACTIVAS
              foreach ($pago->pagos->flatten() as $pagos) {
                $this->printer->text($pagos->monpag.'       '.$pagos->montocambio.'       '.$pagos->fpago);

                $this->printer->feed(1);
           }
     
          }
     
           }
     
     
     
        }

        $this->printer->feed(2);

         
  
       
       
        $metodosPago = $facturas->map(function($q)
        {
           return $q->pagos;
        });
       
        $pagos = $metodosPago->flatten()->groupBy('fpago');//agrupamos los objetos por forma de pagos
        
        $this->printer->text(utf8_decode('METODOS DE PAGO'));
        $this->printer->feed(2);
          //dd($pagos);

         $i = 0;
         $montoTotal = 0;
         $montoTotalA = 0;
        // $pdf->SetFont('arial','',10);
         $montoBsF = 0;
         $montoBsA = 0;
         
         foreach ($pagos as $key => $value) {
            $montoBsF = 0;
            $montoBsA = 0;
            #dd($value);
             foreach ($value as $pago) {
                $facturA = Fafactur::where('reffac',$pago->reffac)->first();
       
                if($facturA->status === 'N'){
                   if($facturA->fecfac === $facturA->fecanu){
                     # var_dump("llego 2");
       
                      $montoBsF += $pago->monpag;
                      $montoBsA += $pago->monpag;
                      $montoTotal += $pago->monpag;
                      $montoTotalA += $pago->monpag;

                      
                   }else{
                    if($facturA->fecfac !== $facturA->fecanu && $desde === $hasta){//en caso de se halla devuelto la factura y consulta un dia especifico
                       if ($desde >= $facturA->fecanu && $desde > $facturA->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                         $montoBsA += $pago->monpag;
                         $montoTotalA += $pago->monpag;
                      
                      #   var_dump("llego 3");
       
                       }else if($desde <= $facturA->fecfac){
                         $montoBsF += $pago->monpag;
                         $montoTotal += $pago->monpag;
                      
       
                       }
                    }else if($facturA->fecfac !== $facturA->fecanu && $desde !== $hasta){//en caso de se halla devuelto la factura y consulta en dias diferentes
                       if ($desde <= $facturA->fecfac && $desde <= $facturA->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                         $montoBsF += $pago->monpag;
                         $montoBsA += $pago->monpag;
                         $montoTotal += $pago->monpag;
                         $montoTotalA += $pago->monpag;
                        # var_dump("llego 5");
                      
                       }else if($desde <= $facturA->fecanu){
                         $montoBsF += $pago->monpag*-1;
                         $montoTotal += $pago->monpag*-1;
                      
                      
                       }
                    }
                   }
               }else{
                $montoBsF += $pago->monpag;
                $montoTotal += $pago->monpag;
                
               }
               
              }
              
              $key = ($key === 'NINGUNO') ? 'EFECTIVO' : $key;
              $this->printer->text($key.': '. number_format(abs($montoBsF-$montoBsA),2,',','.'));
              $this->printer->feed(1);
         
          }
       
          
          $this->printer->feed(1);
       
         $this->printer->text(utf8_decode('TRANSFERENCIAS BANCARIAS'));
         $this->printer->feed(2);
         $bancos = $metodosPago->flatten()->groupBy('nomban');//agrupamos los objetos por bancos
         
         #dd($bancos);
       
         foreach ($bancos as $key => $value) {
          $montoBsAB = 0;
          $montoBsFB = 0;
          $key = ($key === 'NINGUNO') ? 'EFECTIVO' : $key;

          foreach ($value as $banco) {
             $facturA = Fafactur::where('reffac',$banco->reffac)->first();
             if($facturA->status === 'N'){
                if($facturA->fecfac === $facturA->fecanu){
                   $montoBsFB += $banco->monpag;
                   $montoBsAB += $banco->monpag;
             
                }else{
                 if($facturA->fecfac !== $facturA->fecanu && $desde === $hasta){//en caso de se halla devuelto la factura y consulta un dia especifico
                    if ($desde >= $facturA->fecanu && $desde > $facturA->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                      $montoBsAB += $banco->monpag;
             
                    }else if($desde <= $facturA->fecfac){
                      $montoBsFB += $banco->monpag;
  
       
                    }
                 }else if($facturA->fecfac !== $facturA->fecanu && $desde !== $hasta){//en caso de se halla devuelto la factura y consulta en dias diferentes
                    if ($desde <= $facturA->fecfac && $desde <= $facturA->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                      $montoBsFB += $banco->monpag;
                      $montoBsAB += $banco->monpag;
             
                    }else if($desde <= $facturA->fecanu){
                      $montoBsFB += $banco->monpag*-1;
                    }
                 }
              }
            }else{
             $montoBsFB += $banco->monpag;
             
            }
            }
            $this->printer->text(utf8_decode($key.': '. number_format(abs($montoBsFB-$montoBsAB),2,',','.')));
            $this->printer->feed(2);
         
         }
         $this->printer->text(utf8_decode('MONEDAS'));
         $this->printer->feed(2);
         
         $moneda = $metodosPago->flatten()->groupBy('moneda');//agrupamos los objetos por moneda
         #dd($moneda);
        // $pdf->SetFont('arial','',10);
          foreach ($moneda as $key => $value) {
             $montoBsAM = 0;
             $montoBsFM = 0;
             $mon = \App\Famoneda::where('codigoid',$key)->first();
       
             if($mon->nombre === 'BOLIVAR'){
                foreach ($value as $monedas) {
                 $facturA = Fafactur::where('reffac',$monedas->reffac)->first();
                 if($facturA->status === 'N'){
                   if($facturA->fecfac === $facturA->fecanu){
                      $montoBsFM += $monedas->monpag;
                      $montoBsAM += $monedas->monpag;
          
                   }else{
                    if($facturA->fecfac !== $facturA->fecanu && $desde === $hasta){//en caso de se halla devuelto la factura y consulta un dia especifico
                       if ($desde >= $facturA->fecanu && $desde > $facturA->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                         $montoBsAM += $monedas->monpag;
          
                       }else if($desde <= $facturA->fecfac){
                         $montoBsFM += $monedas->monpag;
       
                       }
                    }else if($facturA->fecfac !== $facturA->fecanu && $desde !== $hasta){//en caso de se halla devuelto la factura y consulta en dias diferentes
                       if ($desde <= $facturA->fecfac && $desde <= $facturA->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                         $montoBsFM += $monedas->monpag;
                         $montoBsAM += $monedas->monpag;
          
                        }else if($desde <= $facturA->fecanu){
                         $montoBsFM += $monedas->monpag*-1;
          
                       }
                    }
                 }
                }else{
                   $montoBsFM += $monedas->monpag;
                }
       
                }
       
             }else{
               // arreglar sumatoria BS
                foreach ($value as $monedas) {
                   $facturA = Fafactur::where('reffac',$monedas->reffac)->first();
                   if($facturA->status === 'N'){
                      if($facturA->fecfac === $facturA->fecanu){
                         $montoBsFM += $monedas->montocambio;
                         $montoBsAM += $monedas->montocambio;
                      
                         
                      }else{
                       if($facturA->fecfac !== $facturA->fecanu && $desde === $hasta){//en caso de se halla devuelto la factura y consulta un dia especifico
                          if ($desde >= $facturA->fecanu && $desde > $facturA->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                            $montoBsAM += $monedas->montocambio;
                      
                          }else if($desde <= $facturA->fecfac){
                            $montoBsFM += $monedas->montocambio;
                      
       
                          }
                       }else if($facturA->fecfac !== $facturA->fecanu && $desde !== $hasta){//en caso de se halla devuelto la factura y consulta en dias diferentes
                          if ($desde <= $facturA->fecfac && $desde <= $facturA->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                            $montoBsFM += $monedas->montocambio;
                            $montoBsAM += $monedas->montocambio;
                      
                          }else if($facturA->fecfac <= $facturA->fecanu){
                            $montoBsFM += $monedas->montocambio*-1;
                      
                          }
                       }
                    }
                   }else{
                      $montoBsFM += $monedas->montocambio;
                      
                   }
                  }
                  
                }
                $this->printer->text($mon->nomenclatura.': '. number_format(abs($montoBsFM-$montoBsAM),2,',','.'));
                $this->printer->feed(1);
                
          }
        $this->printer->feed(1);
        $this->printer->text("RESUMEN");
        $this->printer->feed(1);
        $this->printer->text('TOTAL: '. number_format(abs($montoTotal-$montoTotalA),2,',','.'));
        $this->printer->feed(2);
        $this->printer->cut(Printer::CUT_PARTIAL,35);
        $this->printer->feed(2);
        $this->printer->close();






        cierre_log::create([
          'caja'      => $Info['Info']['caja'],
          'numero'    => $Info['Info']['numero'],
          'tipoturno' => $Info['Info']['Turno']
        ]); 

        
        $caja->numtur = str_pad($Info['Info']['numero']+1,$int,'0',STR_PAD_LEFT);
        $caja->save();

        
        //tr_pad($Info['Info']['numero']+1,$int,'0',STR_PAD_LEFT);
            
        return [
        'error' => false,
        'msg' => 'Se ha generado el Cierre con Exito'];

    } catch (\Exception $th) {

        
        
          
          return [
                  'error' => true,
                  'msg' => $th->getMessage()
                ];
        

    }

    
    

}

}
