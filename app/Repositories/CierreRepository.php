<?php

namespace App\Repositories;

use Session;
use Carbon\Carbon;
use \Fpdf;
use App\Http\Controllers\generadorPDF;
use App\Fafactur;
use App\Faforpag;
use Illuminate\Support\Arr;
use App\Sucursal;

class CierreRepository{


 public function getCierreCajaR($request){

   

 $diames = $request->diames;

 $desde = $request->desde;
 $hasta = $request->hasta;


 switch ($diames) {
 case 'diario':
 $tipo = 'DIARIO';
 break;
 case 'ayer':
 $tipo = 'DÍA ANTERIOR';
 break;
 case 'semanal':
 $tipo = "SEMANAL";
 break;
 case 'semant':
 $tipo = "SEMANA ANTERIOR";
 break;
 case 'mensual':
 $tipo = 'MENSUAL';
 break;
 case 'mesant':
 $tipo = 'MES ANTERIOR';
 break;
 case 'anual':
 $tipo = 'ANUAL';
 break;
 case 'antyear':
 $tipo = 'AÑO ANTERIOR';
 break;
 case 'byCod':
 $tipo = $request->codfrom;
 break;
 default:
 $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
 break;
 }

 try{
   
   $turno = null;
   if(isset($request->all()['turno']))
      $turno = strtoupper($request->all()['turno']);
   

   
   $cajas = \App\Models\Caja::where('descaj',$request->caja)->first();

   

   $facturas = Fafactur::
   with(
   'tasaCambio',
   'caja',
   'sucursales',
   'pagos.tipomoneda',
   'pagos.tasamoneda',
   'fargoarticulo'
   )
   ->whereHas('pagos')
   ->where('codcaj',$cajas->id)
   ->where('is_fiscal',$cajas->is_fiscal)
   ->when(isset($request->all()['turno']) === true,function ($q) use ($turno){
      switch ($turno) {
        case 'vespertino':
          $q->where('turnos',$turno);
          break;
        case 'matutino':
          $q->where('turnos',$turno);
          break;
        case 'completo':
         $q->where('turnos',$turno);
         break;
         
         case 'general':     
                          
            return ;
           break;
      }
   })       
   ->whereDate('fecfac','>=',$desde)
   ->whereDate('fecfac','<=',$hasta)
   ->orWhereDate('fecanu','>=',$desde)
   ->whereDate('fecanu','<=',$hasta)
   ->where('is_fiscal',$cajas->is_fiscal)
   ->where('codcaj',$cajas->id)
   ->when(isset($request->all()['turno']) === true,function ($q) use ($turno){
      switch ($turno) {
        case 'vespertino':
          $q->where('turnos',$turno);
          break;
        case 'matutino':
          $q->where('turnos',$turno);
          break;
        case 'completo':
         $q->where('turnos',$turno);
         break;
         
         case 'general':     
                          
            return ;
           break;
      }
   })       
   ->orderBy('created_at','asc')->get();

      

   if($cajas->is_fiscal === "true"){
      $facturaAnul = \App\Fanotcre::whereDate('created_at','>=',carbon::parse($desde)->format('Y-m-d'))
      ->with('perteneceFactura.pagos')
      ->with('perteneceFactura.pagos.tipomoneda')
      ->whereDate('created_at','<=',carbon::parse($hasta)->format('Y-m-d'))
      ->get();
      
      $facturaAnul = $facturaAnul->map(function($val)
      {
         return $val->perteneceFactura;
      });
      $facturas = $facturas->merge($facturaAnul)->sortBy('reffac');
      
   }
   
   $fechas = $facturas->groupBy(function ($val)
   {
      return Carbon::parse($val->created_at)->format('Y-m-d');
   });
   
   $pdf = new generadorPDF();
   $titulo = 'CIERRE DE CAJA ';
   $fecha = date('d-m-Y');
   $pdf->AddPage('L');
   $pdf->SetTitle('Cierre de '.$titulo);
   $pdf->SetFont('arial','B',16);
   $pdf->SetWidths(array(90,90));
   $pdf->Ln();
   $pdf->Cell(0,22,utf8_decode($titulo),0,0,'C');
   $pdf->Ln();
   $sucursal = Sucursal::where('codsuc','=',session('codsuc'))->first();
   #dd($sucursal);
   $pdf->SetFont('arial','B',10);
   
   $pdf->Cell(120,0,utf8_decode('SUCURSAL: '.$sucursal->nomsucu),0,0,'L');
   $pdf->Ln(5);
   $pdf->Cell(120,0,utf8_decode($sucursal->dirfis),0,0,'L');
   $pdf->Ln(4);
   $pdf->Cell(60,0,utf8_decode('FECHA REPORTE: '.$fecha),0,0,'L');
   $pdf->Ln(4);
   $pdf->Cell(60,5,utf8_decode('REPORTE DESDE '.$desde.' HASTA '.$hasta),0,1,'L');
   $pdf->Ln(1);
   if($turno !== null)
      $pdf->Cell(120,5,utf8_decode('TURNO: '.strtoupper($turno)),0,1,'L');

   $pdf->Ln(1);   
   $first = $facturas->first()->reffac;
   $last  = $facturas->last()->reffac;
   $pdf->Cell(120,5,utf8_decode('PRIMERA FACTURA: '.$first),0,1,'L');
   $pdf->Cell(120,5,utf8_decode('ULTIMA FACTURA:  '.$last),0,1,'L');
   $pdf->Ln(4);
   $pdf->SetFont('arial','B',8,5);
   $pdf->SetFillColor(2,157,116);
   
   
   $pdf->Cell(40,8,utf8_decode('Fecha Creada'),0,0,'C');
   $pdf->Cell(40,8,utf8_decode('Pagos Bs'),0,0,'C');
   $pdf->Cell(40,8,utf8_decode('Pagos $'),0,0,'C');
   $pdf->Cell(40,8,utf8_decode('Tipo de Pago'),0,0,'C');
   $pdf->Cell(40,8,utf8_decode('Tasa del Dia'),0,0,'C');
   $pdf->Cell(40,8,utf8_decode('Fecha Anulada'),0,0,'C');
   $pdf->Cell(40,8,utf8_decode('Factura'),0,0,'C');
   $pdf->ln();
   
   
   foreach ($fechas as $key => $value) {
      
      
      $pagosBs = 0;
      $pagosD = 0;
      $tpago = '';
      
      
      foreach ($value->flatten() as $pago) {
         
        $tasa = \App\Fatasacamb::where('codigoid',$pago->tasa)->first()->valor;

      if($pago->status === 'N'){//facturas NC Y ANULADAS
         if($pago->fecfac === $pago->fecanu){

            foreach ($pago->pagos->flatten() as $pagos) {
              $pdf->SetTextColor(0,0,0);

              $pdf->Cell(40,8,utf8_decode($key),1,0,'C');
              $pdf->Cell(40,8,utf8_decode($pagos->monpag),1,0,'C');
              $pdf->Cell(40,8,utf8_decode($pagos->montocambio),1,0,'C');
              $pdf->Cell(40,8,utf8_decode($pagos->fpago),1,0,'C');
              $pdf->Cell(40,8,utf8_decode($tasa),1,0,'C');
              $pdf->Cell(40,8,utf8_decode('N/A'),1,0,'C');
              $pdf->Cell(40,8,utf8_decode($pagos->reffac),1,0,'C');

              $pdf->ln();
              $pdf->SetTextColor(255,0,0);
              $pdf->Cell(40,8,utf8_decode($key),1,0,'C');
              $pdf->Cell(40,8,utf8_decode($pagos->monpag),1,0,'C');
              $pdf->Cell(40,8,utf8_decode($pagos->montocambio),1,0,'C');
              $pdf->Cell(40,8,utf8_decode($pagos->fpago),1,0,'C');
              $pdf->Cell(40,8,utf8_decode($tasa),1,0,'C');
              $pdf->Cell(40,8,utf8_decode($pago->fecanu),1,0,'C');
              $pdf->Cell(40,8,utf8_decode($pagos->reffac),1,0,'C');
              $pdf->ln();
           }

        }else{
               if($pago->fecfac !== $pago->fecanu && $desde === $hasta){//en caso de se halla devuelto la factura y consulta un dia especifico
                   if ($desde >= $pago->fecanu && $desde > $pago->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                     foreach ($pago->pagos->flatten() as $pagos) {
                     $pdf->SetTextColor(255,0,0);
                     $pdf->Cell(40,8,utf8_decode($key),1,0,'C');
                     $pdf->Cell(40,8,utf8_decode($pagos->monpag),1,0,'C');
                     $pdf->Cell(40,8,utf8_decode($pagos->montocambio),1,0,'C');
                     $pdf->Cell(40,8,utf8_decode($pagos->fpago),1,0,'C');
                     $pdf->Cell(40,8,utf8_decode($tasa),1,0,'C');
                     $pdf->Cell(40,8,utf8_decode($pago->fecanu),1,0,'C');
                     $pdf->Cell(40,8,utf8_decode($pagos->reffac),1,0,'C');
                     $pdf->ln();
                     }
                   }else if($desde <= $pago->fecfac){
                     $pdf->SetTextColor(0,0,0);
                     foreach ($pago->pagos->flatten() as $pagos) {

                        $pdf->Cell(40,8,utf8_decode($key),1,0,'C');
                        $pdf->Cell(40,8,utf8_decode($pagos->monpag),1,0,'C');
                        $pdf->Cell(40,8,utf8_decode($pagos->montocambio),1,0,'C');
                        $pdf->Cell(40,8,utf8_decode($pagos->fpago),1,0,'C');
                        $pdf->Cell(40,8,utf8_decode($tasa),1,0,'C');
                        $pdf->Cell(40,8,utf8_decode('N/A'),1,0,'C');
                        $pdf->Cell(40,8,utf8_decode($pagos->reffac),1,0,'C');
                        $pdf->ln();
                     }

                   }

             }else if($pago->fecfac !== $pago->fecanu && $desde !== $hasta){//en caso de se halla devuelto la factura y consulta en dias diferentes


                 if ($desde <= $pago->fecfac && $desde <= $pago->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                  foreach ($pago->pagos->flatten() as $pagos) { //quede aqui
                      $pdf->SetTextColor(0,0,0);
                   $pdf->Cell(40,8,utf8_decode($key),1,0,'C');
                   $pdf->Cell(40,8,utf8_decode($pagos->monpag),1,0,'C');
                   $pdf->Cell(40,8,utf8_decode($pagos->montocambio),1,0,'C');
                   $pdf->Cell(40,8,utf8_decode($pagos->fpago),1,0,'C');
                   $pdf->Cell(40,8,utf8_decode($tasa),1,0,'C');
                   $pdf->Cell(40,8,utf8_decode('N/A'),1,0,'C');
                   $pdf->Cell(40,8,utf8_decode($pagos->reffac),1,0,'C');
                   $pdf->ln();

                   $pdf->SetTextColor(255,0,0);

                   $pdf->Cell(40,8,utf8_decode($key),1,0,'C');
                   $pdf->Cell(40,8,utf8_decode($pagos->monpag),1,0,'C');
                   $pdf->Cell(40,8,utf8_decode($pagos->montocambio),1,0,'C');
                   $pdf->Cell(40,8,utf8_decode($pagos->fpago),1,0,'C');
                   $pdf->Cell(40,8,utf8_decode($tasa),1,0,'C');
                   $pdf->Cell(40,8,utf8_decode($pago->fecanu),1,0,'C');
                   $pdf->Cell(40,8,utf8_decode($pagos->reffac),1,0,'C');
                   $pdf->ln();
                  }
                 }else if($desde <= $pago->fecanu){
                    //quede aqui
                   $pdf->SetTextColor(255,0,0);
                   foreach ($pago->pagos->flatten() as $pagos) {
                   $pdf->Cell(40,8,utf8_decode($key),1,0,'C');
                   $pdf->Cell(40,8,utf8_decode($pagos->monpag),1,0,'C');
                   $pdf->Cell(40,8,utf8_decode($pagos->montocambio),1,0,'C');
                   $pdf->Cell(40,8,utf8_decode($pagos->fpago),1,0,'C');
                   $pdf->Cell(40,8,utf8_decode($tasa),1,0,'C');
                   $pdf->Cell(40,8,utf8_decode('N/A'),1,0,'C');
                   $pdf->Cell(40,8,utf8_decode($pagos->reffac),1,0,'C');
                   $pdf->ln();
                   }
                 }
               }

        }
      }else{//FACTURA ACTIVAS
         foreach ($pago->pagos->flatten() as $pagos) {
       $pdf->SetTextColor(0,0,0);
       $pdf->Cell(40,8,utf8_decode($key),1,0,'C');
       $pdf->Cell(40,8,utf8_decode($pagos->monpag),1,0,'C');
       $pdf->Cell(40,8,utf8_decode($pagos->montocambio),1,0,'C');
       $pdf->Cell(40,8,utf8_decode($pagos->fpago),1,0,'C');
       $pdf->Cell(40,8,utf8_decode($tasa),1,0,'C');
       $pdf->Cell(40,8,utf8_decode('N/A'),1,0,'C');
       $pdf->Cell(40,8,utf8_decode($pagos->reffac),1,0,'C');
       $pdf->ln();
      }

     }

      }



   }

 $pdf->AddPage('L');
 $pdf->ln(3);
 $pdf->SetFont('arial','B',11);
 $pdf->SetTextColor(0,0,0);
 $pdf->Cell(60,8,utf8_decode('METODOS DE PAGOS'),0,0,'L');
 $pdf->ln(5);


 $metodosPago = $facturas->map(function($q)
 {
    return $q->pagos;
 });

 $pagos = $metodosPago->flatten()->groupBy('fpago');//agrupamos los objetos por forma de pagos

  $i = 0;
  $montoTotal = 0;
  $montoTotalA = 0;
  $pdf->SetFont('arial','',10);
  $montoBsF = 0;
  $montoBsA = 0;
  foreach ($pagos as $key => $value) {
     $montoBsF = 0;
     $montoBsA = 0;
     #dd($value);
      foreach ($value as $pago) {
         $facturA = Fafactur::where('reffac',$pago->reffac)->first();

         if($facturA->status === 'N'){
            if($facturA->fecfac === $facturA->fecanu){
              # var_dump("llego 2");

               $montoBsF += $pago->monpag;
               $montoBsA += $pago->monpag;
               $montoTotal += $pago->monpag;
               $montoTotalA += $pago->monpag;
            }else{
             if($facturA->fecfac !== $facturA->fecanu && $desde === $hasta){//en caso de se halla devuelto la factura y consulta un dia especifico
                if ($desde >= $facturA->fecanu && $desde > $facturA->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                  $montoBsA += $pago->monpag;
                  $montoTotalA += $pago->monpag;

               #   var_dump("llego 3");

                }else if($desde <= $facturA->fecfac){
                  $montoBsF += $pago->monpag;
                  $montoTotal += $pago->monpag;
                #  var_dump("llego 4");

                }
             }else if($facturA->fecfac !== $facturA->fecanu && $desde !== $hasta){//en caso de se halla devuelto la factura y consulta en dias diferentes
                if ($desde <= $facturA->fecfac && $desde <= $facturA->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                  $montoBsF += $pago->monpag;
                  $montoBsA += $pago->monpag;
                  $montoTotal += $pago->monpag;
                  $montoTotalA += $pago->monpag;
                 # var_dump("llego 5");

                }else if($desde <= $facturA->fecanu){
                  $montoBsF += $pago->monpag*-1;
                  $montoTotal += $pago->monpag*-1;
                  #var_dump("llego 6 ");

                }
             }
            }
        }else{
         $montoBsF += $pago->monpag;
         $montoTotal += $pago->monpag;
         #var_dump("llego 1 ");
        }

      }
     # dd($montoBsF);
      $pdf->Cell(60,8,utf8_decode($key.': '. number_format(abs($montoBsF-$montoBsA),2,',','.')),0,0,'L');
      $pdf->ln();
   }


  $pdf->SetFont('arial','B',11);
  $pdf->cell(60,8,utf8_decode('TRANSFERENCIAS BANCARIAS'),0,0,'L');
  $pdf->ln(5);

  $bancos = $metodosPago->flatten()->groupBy('nomban');//agrupamos los objetos por bancos
  $pdf->SetFont('arial','',10);
  #dd($bancos);

  foreach ($bancos as $key => $value) {
   $montoBsAB = 0;
   $montoBsFB = 0;
     foreach ($value as $banco) {
      $facturA = Fafactur::where('reffac',$banco->reffac)->first();
      if($facturA->status === 'N'){
         if($facturA->fecfac === $facturA->fecanu){
            $montoBsFB += $banco->monpag;
            $montoBsAB += $banco->monpag;
         }else{
          if($facturA->fecfac !== $facturA->fecanu && $desde === $hasta){//en caso de se halla devuelto la factura y consulta un dia especifico
             if ($desde >= $facturA->fecanu && $desde > $facturA->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
               $montoBsAB += $banco->monpag;
             }else if($desde <= $facturA->fecfac){
               $montoBsFB += $banco->monpag;

             }
          }else if($facturA->fecfac !== $facturA->fecanu && $desde !== $hasta){//en caso de se halla devuelto la factura y consulta en dias diferentes
             if ($desde <= $facturA->fecfac && $desde <= $facturA->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
               $montoBsFB += $banco->monpag;
               $montoBsAB += $banco->monpag;
             }else if($desde <= $facturA->fecanu){
               $montoBsFB += $banco->monpag*-1;
             }
          }
       }
     }else{
      $montoBsFB += $banco->monpag;
     }
     }
     $key = ($key === 'NINGUNO') ? 'EFECTIVO' : $key;

   $pdf->Cell(60,8,utf8_decode($key.': '. number_format(abs($montoBsFB-$montoBsAB),2,',','.')),0,0,'L');
   $pdf->ln();
  }

  $pdf->SetFont('arial','B',11);
  $pdf->cell(60,8,utf8_decode('MONEDAS'),0,0,'L');
  $pdf->ln(5);
  $moneda = $metodosPago->flatten()->groupBy('moneda');//agrupamos los objetos por moneda
  #dd($moneda);
  $pdf->SetFont('arial','',10);
   foreach ($moneda as $key => $value) {
      $montoBsAM = 0;
      $montoBsFM = 0;
      $mon = \App\Famoneda::where('codigoid',$key)->first();

      if($mon->nombre === 'BOLIVAR'){
         foreach ($value as $monedas) {
          $facturA = Fafactur::where('reffac',$monedas->reffac)->first();
          if($facturA->status === 'N'){
            if($facturA->fecfac === $facturA->fecanu){
               $montoBsFM += $monedas->monpag;
               $montoBsAM += $monedas->monpag;
            }else{
             if($facturA->fecfac !== $facturA->fecanu && $desde === $hasta){//en caso de se halla devuelto la factura y consulta un dia especifico
                if ($desde >= $facturA->fecanu && $desde > $facturA->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                  $montoBsAM += $monedas->monpag;
                }else if($desde <= $facturA->fecfac){
                  $montoBsFM += $monedas->monpag;

                }
             }else if($facturA->fecfac !== $facturA->fecanu && $desde !== $hasta){//en caso de se halla devuelto la factura y consulta en dias diferentes
                if ($desde <= $facturA->fecfac && $desde <= $facturA->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                  $montoBsFM += $monedas->monpag;
                  $montoBsAM += $monedas->monpag;
                }else if($desde <= $facturA->fecanu){
                  $montoBsFM += $monedas->monpag*-1;
                }
             }
          }
         }else{
            $montoBsFM += $monedas->monpag;
         }

         }

      }else{
         //arreglar sumatoria BS
         foreach ($value as $monedas) {
            $facturA = Fafactur::where('reffac',$monedas->reffac)->first();
            if($facturA->status === 'N'){
               if($facturA->fecfac === $facturA->fecanu){
                  $montoBsFM += $monedas->montocambio;
                  $montoBsAM += $monedas->montocambio;
               }else{
                if($facturA->fecfac !== $facturA->fecanu && $desde === $hasta){//en caso de se halla devuelto la factura y consulta un dia especifico
                   if ($desde >= $facturA->fecanu && $desde > $facturA->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                     $montoBsAM += $monedas->montocambio;
                   }else if($desde <= $facturA->fecfac){
                     $montoBsFM += $monedas->montocambio;

                   }
                }else if($facturA->fecfac !== $facturA->fecanu && $desde !== $hasta){//en caso de se halla devuelto la factura y consulta en dias diferentes
                   if ($desde <= $facturA->fecfac && $desde <= $facturA->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                     $montoBsFM += $monedas->montocambio;
                     $montoBsAM += $monedas->montocambio;
                   }else if($facturA->fecfac <= $facturA->fecanu){
                     $montoBsFM += $monedas->montocambio*-1;
                   }
                }
             }
            }else{
               $montoBsFM += $monedas->montocambio;
            }
           }

      }
      #dd($montoBsFM,$montoBsAM);
      $pdf->Cell(60,8,utf8_decode($mon->nomenclatura.': '. number_format(abs($montoBsFM-$montoBsAM),2,',','.')),0,0,'L');
      $pdf->ln();
   }

   $pdf->SetFont('arial','B',11);
   $pdf->cell(60,8,utf8_decode('RESUMEN'),0,0,'L');
   $pdf->ln();
   $pdf->SetFont('arial','',10);

   $pdf->Cell(60,8,utf8_decode('TOTAL: '.number_format(abs($montoTotal-$montoTotalA),2,',','.')),0,0,'L');
   $pdf->Output('I','Reporte de Facturas-.pdf');
 exit;

}catch(\Exception $e){
    
    return redirect()->route('CierreCajaView');
}

}

 public function explodeDate($date){
 $date = \explode('?',$date);

 $formatDate = Carbon::parse($date[1])->isoFormat('DD-MM-Y h:mm:ss a');
 $monto = number_format((float)$date[0],2,',','.').' Bs.S';

 return $monto." ".$formatDate;
 }

}
