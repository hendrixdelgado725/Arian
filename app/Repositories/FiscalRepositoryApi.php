<?php


namespace App\Repositories;

use App\Fafactur;
use App\Logs_impresoras;
use App\Models\Caja;
use App\Models\Logs_impresora_api;
use App\Events\SendToPrintFactura;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use ElephantIO\Client;
use App\Models\logs_reportes;

class FiscalRepositoryApi
{


  /**
   *
   * Usuario debe estar estricatemente logueado para poder ejecutar esta accion
   *
   */

  public function __construct()
  {
    //$this->mi
  }

  public function getResponse($response)
  {
    
    switch ($response) {
      case 200:
        return true;
      case 404:
        return ['msg' => 'Ip no encontrada'];
      case 500 || 400:
        return $response->json();
    }
  }


  /**
   * Metodo para imprimir factura ya guardada en el sistema En caso que no haya sido impresa
   *
   * @param \Factura Objeto factura que tenga toda la informacion para generar
   *  */

  public function printFiscal(String $reffac,String $caja)
  {


    try{
        //$validatePrinted = Fafactur::where([])

        //$printed = Logs_impresoras::where('')
        //$printed = Logs_impresoras::where('')

        $checkToFac = $this->checkToFac($reffac);

        if(!$checkToFac) throw new \Exception("La Factura no está activa");


        $newCaja = Caja::where('id',$caja)->first();
        
        if(!$newCaja){ 
          return [
            "msg"       => "Error Critico, No se obtuvo la caja para facturar",
            "errorCode" => 500
          ];
        }
        /* if(!$newCaja){/* error }*/
          $factura = Fafactur::where([
            ['reffac','=',$reffac],
            ['cajaid',$newCaja->codigoid]
          ])->with('cliente','articulos','pagos.monedas')
          ->first();/* Valido para ser impreso por la impresora */

      if(!$factura){
        return [
          "msg"       => "Error Critico, No se obtuvo la factura",
          "errorCode" => 500
        ];
      }

        (array) $array_to_send = [];/* info a enviar a la impresora fiscal */

        $array_to_send["factura"] = [
          'numero'       => $factura->reffac,
          'fecha'        => $factura->created_at->format('d/m/y'),
          'razon_social' => optional($factura->cliente)->nompro,
          'documento'    => optional($factura->cliente)->rifpro,
          'adicional'    => [
            /* aquí la data del cliente */
            ["DIRECCION" => utf8_encode($this->cutString(optional($factura->cliente)->dirpro))],//cambie aqui

            ["TELEFONO"  =>  optional($factura->cliente)->telpro ? : 'N/A']
          ]
        ];
      #  dd($factura->articulos);
        foreach ($factura->articulos as $articulo) {
          $iva = $factura->porrecargo;
          //$calculo = $iva != 0 ? ($articulo["costo"]*$iva)/100 : 0;
          $calculo = ($articulo->articulo->ivastatus === "true") ? ($articulo["costo"]*$iva)/100 : 0;
          /* Se calcula la cantidad recargada por el iva */
          $unitDiscount = $articulo["mondes"] != "0.00" ?  (float) $articulo["porcentajedescuento"] : (int) 0;
          /* Se calcula el nuevo precio de producto según descuento */
          
          $precioUnit = (float)$articulo["precio"]-(($unitDiscount*$articulo["precio"])/100);
          $desProd = is_numeric(substr($articulo["desart"],0,1)) ? 'P'.$articulo["desart"] : $articulo["desart"];
          //$desProd = is_numeric(substr($articulo["descrip_art"],0,1)) ? 'P'.$articulo["descrip_art"] : $articulo["descrip_art"];
          /* solo hace falta el precio unitario asi tenga descuento la impresora fiscal ejecuta la operacion */
          $desProd = substr($desProd,0, 90);
          $factura = Fafactur::where('reffac',$articulo['reffac'])->first();
          $serial = \App\Models\Serial::where('codart',$articulo['codart'])
                                       ->where('factura',$factura->reffac)
                                       ->get();//muestra los seriales a facturar

            $seriales = '';
            $cpt = "";
            if($serial->isEmpty() === false){

                foreach($serial as $s){


                if($s->serial !== "") $cpt .= $s->serial.',';
                else $cpt .= '';
                }
                $seriales .= $cpt;

            }else{
              $seriales = ',';
            }


          $productos[] = [
            "descripcion" => (string) $desProd,
            "cantidad"    => (float)  $articulo['cantot'],
            "precio"      => (float)  number_format($precioUnit,2,'.',''),
            "iva"         => (string) $articulo->articulo->ivastatus === "true" ? $factura->porrecargo : "empty",
            "seriales"    => (string) $seriales
          ];


        }


        $array_to_send["detalle"] = $productos;
        $url = 'http://'.$newCaja->impfishost.':3500';

        $options = ['client' => Client::CLIENT_4X];
        $client = Client::create($url, $options);
        $client->connect();
        
        $client->emit('sendFactura',$array_to_send);
        $packet = $client->wait('response');

        if($packet){

          $data = $packet->data;
          $args = $packet->args;
          // access data
          $response = $data;
        }
       
        
        $client->disconnect();
       // $response = Http::post('http://'.$newCaja->impfishost.':3500/sendFactura',$array_to_send);
        $getResponse = $this->getResponse($response['status']);
        

        if($getResponse !== true){
          $factura_actual = Fafactur::where('reffac', $reffac)->first();
          $factura_actual->status = 'N';
          $factura_actual->motanu = 'IMP';
          $factura_actual->save();
          $getResponse["errorCode"] = $response->status();
          return $getResponse;
        }

        $factura_actual = Fafactur::where('reffac', $reffac)->first();
        $factura_actual->is_fiscal   = true;
        $factura_actual->num_fiscal  = $response["ultima_factura"];
        $newCaja->corfac = str_replace($caja.'F',str_pad("0",3,0,STR_PAD_LEFT),$factura_actual->reffac);
        $factura_actual->save();
        $newCaja->save();

        Logs_impresora_api::create([
          'factura_sistema' => $response['factura_sistema'],
          'factura_fiscal'  => $response['factura_fiscal'],
          'tipo'            => $response['tipo']
        ]);

        return true;
        /*     "factura"  : {
        "numero"       : "482994",
        "fecha"         : "40/12/2021",
        "razon_social" : "MIGUEL PAEZ",
        "documento"    : "V-21203010",
        "adicional"    : [
            {"CAJERO"    : "VIT"},
            {"DIRECCION" : "PUNTO FIJO"},
            {"TELÉFONO"  : "04121087825"}
        ]
    },
    "detalle" : [
        {
            "iva" : "0",
            "precio" : 1,
            "cantidad" : 1,
            "descripcion" : "ITEM EXENTO"
        },
        {
            "iva" : 16,
            "precio" : 1,
            "cantidad" : 1,
            "codigo" : "000123",
            "descripcion" : "ITEM TASA 1"
        },
        {
            "iva" : 8,
            "precio" : 1.00,
            "cantidad" : 1,
            "codigo" : "000123",
            "descripcion" : "ITEM TASA 2"
        },
        {
            "iva" : 32,
            "precio" : 1,
            "cantidad" : 1,
            "codigo" : "1123",
            "descripcion" : "ITEM TASA 3"
        }
    ], */
      }catch(\Exception $e){
        dd($e);
        return [
          'error'     => true,
          'msg'       => $e->getMessage(),
          'errorCode' => 500
        ];
      }
        return true;
      /* BroadCast Event */
  }

  public function cutString($direccion = '')
  {
    return $direccion !== '' ? substr($direccion,0, 28) : 'PUNTO FIJO';
  }

  public function printDevolucion(String $reffac,String $caja)
  {
    $newCaja = Caja::where('id',$caja)->first();
    $checkToDev = $this->checkToDev($reffac);

 if(!$checkToDev){
      return [
        "msg"       => "Error Critico, La factura no es una factura fiscal",
        "errorCode" => 500
      ];
    }

    if(!$newCaja){
      return [
        "msg"       => "Error Critico, No se obtuvo la caja para facturar",
        "errorCode" => 500
      ];
    }
    $factura = Fafactur::where([
      ['reffac','=',$reffac],
      /* ['cajaId',$newCaja->codigoId] */
    ])->with('cliente','articulos','pagos.monedas')
    //->where('is_fiscal',true)
    //->where('num_fiscal','<>',null)
    ->first();/* Valido para ser impreso por la impresora */

    if(!$factura){
      return [
        "msg"       => "Error Critico, No se obtuvo la factura",
        "errorCode" => 500
      ];
    } /* throw new \Exception("Factura Not Found") */;

    (array) $array_to_send = [];/* info a enviar a la impresora fiscal */

    $array_to_send["factura"] = [
      'numero'       => $factura->reffac,//para que salga el número de factura devuelto
      'factura_fiscal' => $factura->num_fiscal,
      'fecha'        => $factura->created_at->format('d/m/y'),
      'razon_social' => optional($factura->cliente)->nompro,
      'documento'    => optional($factura->cliente)->rifpro,
    ];
    foreach ($factura->articulos as $articulo) {
      $iva = $factura->porrecargo;
      //$calculo = $iva != 0 ? ($articulo["costo"]*$iva)/100 : 0;
      $calculo = ($articulo->articulo->ivastatus === "true") ? ($articulo["costo"]*$iva)/100 : 0;

      /* Se calcula la cantidad recargada por el iva */
      $unitDiscount = $articulo["mondes"] != "0.00" ?  (float) $articulo["porcentajedescuento"] : (int) 0;
      /* Se calcula el nuevo precio de producto según descuento */
      
      $precioUnit = (float)$articulo["precio"]-(($unitDiscount*$articulo["precio"])/100);
      $desProd = is_numeric(substr($articulo["desart"],0,1)) ? 'P'.$articulo["desart"] : $articulo["desart"];
      //$desProd = is_numeric(substr($articulo["descrip_art"],0,1)) ? 'P'.$articulo["descrip_art"] : $articulo["descrip_art"];

      /* solo hace falta el precio unitario asi tenga descuento la impresora fiscal ejecuta la operacion */
      $desProd = substr($desProd,0, 90);
      $factura = Fafactur::where('reffac',$articulo['reffac'])->first();
      $serial = \App\Models\Serial::where('codart',$articulo['codart'])
      ->where('factura',$factura->reffac)
      ->get();//muestra los seriales a facturar

      $seriales = '';
      $cpt = "";
      if($serial->isEmpty() === false){

          foreach($serial as $s){


          if($s->serial !== "") $cpt .= $s->serial.',';
          else $cpt .= '';
          }
          $seriales .= $cpt;

      }else{
        $seriales = ',';
      }

      $productos[] = [
        "descripcion" => (string) $desProd,
        "cantidad"    => (float)  $articulo['cantot'],
        "precio"      => (float)  number_format($precioUnit,2,'.',''),
        "iva"         => (string) $articulo->articulo->ivastatus === "true" ? $factura->porrecargo : "empty",
        "seriales"    => (string) $seriales
      ];
    }
    $array_to_send["detalle"] = $productos;


    
    $url = 'http://'.$newCaja->impfishost.':3500';

    $options = ['client' => Client::CLIENT_4X];
    $client = Client::create($url, $options);
    $client->connect();
    $data = ['username' => 'my-user'];
    $client->emit('sendDevolucion',$array_to_send);
    $packet = $client->wait('response');

    if($packet){

      $data = $packet->data;
      $args = $packet->args;
      // access data
      $response = $data;
    }
    $client->disconnect();
    $getResponse = $this->getResponse($response['status']);

    if($getResponse !== true){
      $factura_actual = Fafactur::where('reffac', $reffac)->first();
      $getResponse["errorCode"] = $response->status();
      return $getResponse;
    }

    $factura_actual = Fafactur::where('reffac', $reffac)->first();
    $factura_actual->status      = 'N';
    $factura_actual->motanu      = 'N/C';
    $factura_actual->num_dev_fiscal  = $response["ultima_nota_credito"];
    $newCaja->cornot = $response["ultima_nota_credito"];
    $factura_actual->save();
    $newCaja->save();
    

    Logs_impresora_api::create([
      'factura_sistema' => $response['factura_sistema'],
      'devolucion_fiscal'  => $response['devolucion_fiscal'],
      'tipo'            => $response['tipo'],
    ]);

    return true;
  }

  /**
   *
   * function para verificar que la factura a imprimir o devolver cumpla con las
   * condiciones para ser devuelta o facturada por la impresora fiscal
   * @return bool
  */

  protected function checkToDev(string $reffac) :bool
  {
    $log = Logs_impresora_api::where('factura_sistema',$reffac)->first();

    if($log === null) return false;

    $fac = Fafactur::where([
      ['reffac','=',$reffac],
      ['num_fiscal','<>',null],
      ['status','=','A']
    ])->first();
    return $fac /*&& $log->where('tipo','dev')->where('devolucion_fiscal','<>',null)->isEmpty()*/ ? true : false;
  }

    protected function checkToFac(string $reffac) :bool
  {
    //$log = Logs_impresora_api::where('factura_sistema',$reffac)->get();
    $fac = Fafactur::where([
      ['reffac','=',$reffac],
      ['num_fiscal','=',null],
      ['status','=','A']
    ])->first();

    return /*$log->isEmpty() &&*/ $fac ? true : false;
  }

  protected function putArticulos($factura)
  {
    foreach ($factura->articulos as $articulo) {
      $iva = $factura->porrecargo;
      $calculo = $iva != 0 ? ($articulo["costo"]*$iva)/100 : 0;
      /* Se calcula la cantidad recargada por el iva */
      $unitDiscount = $articulo["mondes"] != "0.00" ?  (float) $articulo["precio"]/(int)$articulo["cantot"] : (int) 0;
      /* Se calcula el nuevo precio de producto según descuento */
      $precioUnit = (float)$articulo["precio"]-$unitDiscount;
     # $desProd = is_numeric(substr($articulo["desart"],0,1)) ? 'P'.$articulo["desart"] : $articulo["desart"];
     $desProd = is_numeric(substr($articulo["descrip_art"],0,1)) ? 'P'.$articulo["descrip_art"] : $articulo["descrip_art"];

      /* solo hace falta el precio unitario asi tenga descuento la impresora fiscal ejecuta la operacion */
      $array_json["detalle"][] = [
        "descripcion" => $desProd,
        "cantidad"    => $articulo['cantot'],
        "precio"      => number_format($precioUnit,2,'.',''),
        "iva"         => $calculo,
      ];
    }
    return $array_json["detalle"];
  }

  protected function putPayments($factura)
  {
    if($factura->pagos->isEmpty()){/* error */}
    //codigos se van a imprimir como strings Directos
    (array) $array_json = [];
    $total = 0;
    foreach($factura->pagos as $key => $pago)
    {
      $array_json["payments"][] = [
        //"tipoPago" => self::getPaymentString($pago),
        "monto"    => $pago->monpag,
        "montoThka"=> $this->formatAmmount($pago)
      ];

      $total += (float) $pago->monpag;
    }
    //dd($total,$factura->monfac);

    return round($total,2) != round((float) $factura->monfac,2) ? ['montoThka' => '101'] : $array_json["payments"];
  }

  protected function formatAmmount(object $pago)
  {

    return  str_pad((string) intval(((float)$pago->monpag*100)),12,'0',STR_PAD_LEFT);
  }

  protected function getPaymentString(object $pago)
  {
    $debito        = ['DÉBITO'];
    $transferencia = ['TRANSFERENCIA'];
    $pagomovil     = ['PAGO MÓVIL'];
    $efectivo      = ['EFECTIVO'];
    /* faltaría añadir petro */
    /*
      Falta asociar tramas con impresora
    */

    if(Arr::exists($debito,$pago->fpago)){// es debito
      $string = '03';/* comando segun trama */
    }elseif(Arr::exists($transferencia,$pago->fpago)){// transferencia
      $string = '10';/* comando segun trama */
    }elseif(Arr::exists($pagomovil,$pago->fpago)){ // pago movil
      $string = '12';/* comando segun trama */
    }elseif(Arr::exists($efectivo,$pago->fpago) && optional($pago->monedas)->nomenclatura === '$'){ // efectivo DOLARES
      $string = '02';/* comando segun trama */
    }elseif(Arr::exists($efectivo,$pago->fpago) && optional($pago->monedas)->nomenclatura === 'BS'){
      /* EFECTIVO BOLIVARES */
      $string = '01';/* comando segun trama */
    }

    return $string.$this->formatAmmount($pago);

  }

  protected function printed($reffac)
  {
    return $this->printedFactur($reffac) && $this->printedLog($reffac);
  }

  protected function printedFactur($reffac)
  {
    return Fafactur::where('reffac',$reffac)->where('impfissta','I')->first() ? true : false;
  }

  protected function printedLog($reffac)
  {
    return Logs_impresoras::where('numero_factura',$reffac)->whereNull('error')->first() ? true : false;
  }
  public function getStatus($caja)  {
    
    try {
      $response = Http::post('http://'.$caja.':3500/getStatus',['status' =>'S1']);
      $getResponse = $this->getResponse($response);
      
      if($response->status() === 500) 
         throw new \Exception("Error la impresora no esta conectada verifique conexion", $response->status());

    } catch (\Exception $th) {
        return [
                 'msgError' => $th->getMessage(),
                 'status'   => $response->status()
                ];
    }
   
    
    return [
      'msgError' => "La impresora esta conectada",
      'status'   => $response->status()
     ];

  }

}
