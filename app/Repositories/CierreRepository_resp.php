<?php

namespace App\Repositories;

use Session;
use Carbon\Carbon;
use \Fpdf;
use App\Http\Controllers\generadorPDF;
use App\Fafactur;
use App\Faforpag;
use Illuminate\Support\Arr;

class CierreRepository{


  public function getCierreCajaR($request){


    $diames = $request->diames;

    $desde = $request->desde;
    $hasta = $request->hasta;


    switch ($diames) {
      case 'diario':
        $tipo = 'DIARIO';
      break;
      case 'ayer':
          $tipo = 'DÍA ANTERIOR';
      break;
      case 'semanal':
         $tipo = "SEMANAL";
      break;
      case 'semant':
         $tipo = "SEMANA ANTERIOR";
      break;
      case 'mensual':
          $tipo = 'MENSUAL';
      break;
      case 'mesant':
        $tipo = 'MES ANTERIOR';
      break;
      case 'anual':
        $tipo = 'ANUAL';
      break;
      case 'antyear':
        $tipo = 'AÑO ANTERIOR';
      break;
      case 'byCod':
        $tipo = $request->codfrom;
      break;
      default:
        $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
      break;
  }

    try{

      $facturas = Fafactur::with(
      'tasaCambio',
      'caja',
      'sucursales',
      'pagos.tipomoneda',
      'pagos.tasamoneda'
    ) 
    ->whereHas('pagos')

    #->date($request->desde,$request->hasta)
    ->cajafac(intval($request->caja))
    //->where('impfissta','I')
    #->where('status','A') 
    ->whereDate('created_at','>=',$desde)
    ->whereDate('created_at','<=',$hasta)
    ->orWhereDate('updated_at','>=',$desde)
    ->whereDate('updated_at','<=',$hasta)
    ->orderBy('created_at','asc')->get();
    
    $caja = \App\Models\Caja::find(intval($request->caja));
    
    if($facturas->isEmpty()) throw new \Exception('no existe registro');
    

  $facturasActivas = $facturas->filter(function($q) use ($desde,$hasta,$caja){//filtramos las facturas activas con su caja seleccionada
      return ($q->fecfac >= $desde && $q->fecfac <= $hasta && $caja->id === $q->codcaj);
  });
  $facturasAnul = $facturas->filter(function($q) use ($desde,$hasta,$caja){//filtramos las facturas anuladas con su caja seleccionada
      return ($q->fecanu >= $desde && $q->fecanu <= $hasta && $caja->id === $q->codcaj);
  });
  $facturas = $facturasActivas->merge($facturasAnul)->unique('reffac'); //unimos los array Activos y anulados pone los array unicos por su factura
   
  $base = $facturas->pluck('pagos')->flatten();
  
  
    $arrayBase =  $facturasActivas->toArray();
    $firstBill = head($arrayBase);
    $lastBill = last($arrayBase);
   
    if($tipo === 'DIARIO') $date = "Cierre de Caja de ".Carbon::parse($firstBill["created_at"])->isoFormat('DD-MM-Y  h:mm:ss a');
      $firstDate = Carbon::parse($firstBill["created_at"])->isoFormat('DD-MM-Y  h:mm:ss a');
      $lastDate  = Carbon::parse($lastBill["created_at"])->isoFormat('DD-MM-Y  h:mm:ss a');
    /*  
      desde $base tenemos todos los pagos habría que tomar una variable 
      para la totalizacion y hacer el group by de lo requerido
    
    */
   

    $base = $base->map(function($q) use ($desde,$hasta)
    {
          
          $fac = Fafactur::where('reffac',$q->reffac)->first();
          $fac2 = Faforpag::where('reffac', $q->reffac)->where('id',$q->id)->first();
          $fac1 = Faforpag::where('reffac', $q->reffac)->where('id',$q->id)->first();


          if($fac->status === 'A'){
            return  $q->setAttribute('monpag',$q->monpag);
          }else{
                
              if($fac->fecanu === $fac->created_at->format('Y-m-d')){
                $q->replicate();
                $fac2->setAttribute('monpag',$q->monpag * -1);
                $fac2->setAttribute('montocambio',$q->montocambio * -1);

                return [$fac2,$q];
               
              }elseif($fac->fecfac !== $desde){
                if($fac->status === 'N' && $hasta !== $desde){//condicion se cumple si no es el mismo dia
                  $q->replicate();
                  $fac2->setAttribute('monpag',$q->monpag * -1);
                  $fac2->setAttribute('montocambio',$q->montocambio * -1);
                  return [$fac2,$q];
                }else{//se cumple si los dias son diferentes
                  $fac2->setAttribute('monpag',$q->monpag * -1);
                  $fac2->setAttribute('montocambio',$q->montocambio * -1);
                }
                return $fac2;
              }else {
                $fac2->setAttribute('monpag',$q->monpag);
                $fac2->setAttribute('montocambio',$q->montocambio);
                return $fac2;
              }
          }
                  
        });

    
    $totalFac = $facturasActivas->count();
   
    $pagos =$base->flatten()->groupBy([
      'moneda',
      function($q){
        return $q['tipomoneda']['nomenclatura'];
      },
      function ($q){
        return ($q["tasamoneda"]  !== null) ? $q["tasamoneda"]['valor'].'?'.$q["tasamoneda"]["created_at"] : $q["tasamoneda"] ;
      }
    ]);
    
    #dd($pagos)  ;
    /*$byCoin = $base->groupBy([//OBTENGO LA SUMATORIA POR MONEDA
      function($q){
        return  $q['tipomoneda']['nomenclatura'];
      }
      ]);*/

    $totalG = $base->flatten()->sum('monpag');//OBTENGO LA SUMATORIA TOTAL EN BOLIVARES
    
    $totalMoneda =0;/*$byCoin->map(function ($row){
        return [$row->sum('monpag'),$row->sum('montocambio')];
    });*/
    
    $detallado = $pagos->map(
      function($grandpa){
        return $grandpa->map(
          function($father){
              return $father->map(
                function($son){
                  return [
                    'montoBs' => $son->sum('monpag'),
                    'montoCoin' => $son->sum('montocambio'),
                    'tasaCambio' => $son->pluck('tasamoneda')
                  ];

              });
        });
        
    });
    #dd($detallado); 
    #$cashExist = Arr::exists($detallado->keys()->flip()->toArray(),'EFECTIVO');

    /* si cashExist entonces tenemos que  Validar que no existe banco*/
    //dd($detallado,$totalMoneda,"TOTAL GLOBAL : ".number_format($totalG,2,',','.'));
    #dd($cashExist);
    if($facturas->isEmpty()) return 'EMPTY';

    $pdf = new generadorPDF();
    $titulo = 'CIERRE DE CAJA ';
    $fecha = date('d-m-Y');

    $pdf->AddPage('L');
    $pdf->SetTitle('Cierre de '.$caja->descaj);
    $pdf->SetFont('arial','B',16);
    $pdf->SetWidths(array(90,90));
    $pdf->Ln();
    $pdf->Cell(0,22,utf8_decode($date ?? 'CIERRE DE '.$caja->descaj),0,0,'C');
    $pdf->Ln();
    $pdf->SetFont('arial','B',10);
    $pdf->Ln(4);
    $pdf->Cell(60,0,utf8_decode('FECHA REPORTE: '),0,0,'L');
    $pdf->Cell(30,0,utf8_decode($fecha),0,1,'L');
    $pdf->Ln(4);
    $pdf->Cell(60,0,'TOTAL: ',0,0,'L');
    $pdf->Cell(30,0,number_format($totalG,2,',','.'),0,1,'L');
    $pdf->Ln(4);
    $pdf->Cell(60,0,'TOTAL FACTURAS EMITIDAS: ',0,0,'L');
    $pdf->Cell(30,0,$totalFac,0,1,'L');
    $pdf->Ln(4);
    $pdf->Cell(60,0,'PRIMERA FACTURA EMITIDA : ',0,0,'L');
    $pdf->Cell(30,0,'Nro. '.$firstBill["reffac"].' Fecha: '.$firstDate,0,1,'L');
    $pdf->Ln(4);
    $pdf->Cell(60,0,'ULTIMA FACTURA EMITIDA : ',0,0,'L');
    $pdf->Cell(30,0,'Nro. '.$lastBill["reffac"].' Fecha: '.$lastDate,0,1,'L');
    $pdf->Ln(4);
    $pdf->SetFont('arial','B',8,5);
    $pdf->SetFillColor(2,157,116);
  #  $pdf->Cell(45,8,utf8_decode('Tipo de Pago'),1,0,'C');
   # $pdf->Cell(40,8,utf8_decode('Banco'),1,0,'C');
    $pdf->Cell(40,8,utf8_decode('Moneda'),1,0,'C');
    $pdf->Cell(40,8,utf8_decode('Monto Moneda'),1,0,'C');
    $pdf->Cell(55,8,utf8_decode('Monto Bolivares'),1,0,'C');
    $pdf->Cell(40,8,utf8_decode('Tasa Cambio'),1,0,'C');
    $pdf->Ln();
    $pdf->SetWidths(array(40,40,55,40));
    $pdf->SetAligns(['C','C','C','C','C','C']);
    $pdf->SetFont('arial','',12);

    $pagos = '';
    $espacio= ', ';
    $MontoTotal = 0;

    /* 
      "TRANSFERENCIA" => Illuminate\Support\Collection {#1043 ▼
      #items: array:1 [▼
        "BANESCO BANCO UNIVERSAL" => Illuminate\Support\Collection {#1082 ▼
          #items: array:1 [▼
            "BS" => Illuminate\Support\Collection {#1142 ▼
              #items: array:1 [▼
                "" => array:3 [▼
                  "montoBs" => 27860000.0
                  "montoCoin" => 0
                  "tasaCambio" => ""
                ]
              ]
            }
          ]
        }
      ]
    */
    
    foreach($detallado as $moneda => $arrayMoneda){
        foreach ($arrayMoneda as $banco => $arrayBanco) {
          foreach($arrayBanco as $monto => $montoValue){
            
          if($montoValue['montoBs'] > 0){
            $pdf->row(
              [
                __(utf8_decode($banco)),
                \number_format($montoValue["montoCoin"],2,',','.'),
                \number_format((string)$montoValue['montoBs'],2,',','.').' Bs.D',
               ($banco != 'BS') ? $this->explodeDate($monto) : 'N/A'
              ]);
           }
            
           

            
          }
        }
    }

    
    $dollas = isset($dollas) ? collect($dollas) : collect([]);
    $sumas = $dollas->map(function($q){
        return ['sum' => explode('?',$q)[0]];
    });

   $suma      = ( float )$sumas->sum('sum');
    $count     =  ( int )$sumas->count();
    $avgDollas = $count !== 0 ? $suma/$count : $facturas[0]->tasaCambio->valor;
    //dd(round($avgDollas,2));
    // $pdf->SetTextColor(0,0,0);
    // $pdf->Ln(5);
    // $pdf->SetFont('arial','B',12,5);
    // $pdf->Cell(180,6,utf8_decode('TOTAL MONTO POR MONEDAS'),1,1,'C');
    // $pdf->SetFont('arial','B',10,5);
    // $pdf->Cell(60,8,utf8_decode('Moneda'),1,0,'C');
    // $pdf->Cell(60,8,utf8_decode('Bolivares'),1,0,'C');
    // $pdf->Cell(60,8,utf8_decode('Monto Moneda'),1,1,'C');
    // $pdf->SetWidths(array(60,60,60));
    // $pdf->SetAligns(['C','C','C']);
    // $pdf->SetFont('arial','',12);
  /*  foreach ($totalMoneda as $key => $value) {
      $arr = [];
      $arr[] = $key;
      foreach ($value as $index => $value2) {
        $moneda  = $index == 0 ? ' Bs.D ': $key;
        $arr[] = number_format($value2,2,',','.').$moneda;
      }
      $pdf->row($arr);
    }*/

    $pdf->SetTextColor(0,0,0);
    $pdf->Ln(5);
    $pdf->SetFont('arial','B',10,5);
    $pdf->SetFillColor(2,157,116);
    $pdf->SetTextColor(0,0,0);
    $desde = Carbon::parse($firstDate)->format('d/m/yy');
    $hasta = Carbon::parse($lastDate)->format('d/m/yy');
    $pdf->Cell(120,5,utf8_decode('TOTAL GLOBAL Desde '.$desde.' Hasta '.$hasta),1,1,'L');
    $pdf->Cell(60,5,utf8_decode('Bolivares'),1,0,'L');
    $pdf->Cell(60,5,number_format($totalG,2,',','.')." Bs.D",1,1,'L');
    $pdf->Cell(60,5,utf8_decode('Dolares'),1,0,'L');
    $totalDollas = ( float )$totalG / ( float ) $avgDollas;
    $pdf->Cell(60,5,number_format(round($totalDollas,0),0,',','.').' $',1,1,'L');

    $pdf->Output('I','Reporte de Facturas-'.$fecha.'.pdf');

    }catch(\Exception $e){
      
       return redirect()->route('CierreCajaView');
    }
  }

  public function explodeDate($date){
    $date = \explode('?',$date);

    $formatDate = Carbon::parse($date[1])->isoFormat('DD-MM-Y  h:mm:ss a');
    $monto = number_format((float)$date[0],2,',','.').' Bs.S';

    return $monto." ".$formatDate;
  }

}



?>