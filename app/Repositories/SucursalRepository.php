<?php


namespace App\Repositories;

use Illuminate\Support\Facades\Auth;


class SucursalRepository{

	public $auth; 
	public function __construct(){
		
	}

	public function sucursalAuth()
	{
		return Auth::user()->getActiveS2();
	}

}
?>