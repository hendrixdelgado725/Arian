<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fatippag extends Model implements Auditable
{
    use softDeletes,\OwenIt\Auditing\Auditable;

    protected $connection='pgsql2';
    protected $table = 'fatippag';
    
    protected static $logAttributes = ['codtippag','destippag','codigo_id'];
    protected static $logName = 'fatippag';
    protected static $logFillable = true;

    protected $fillable = [
        'codtippag','destippag','tipcan','genmov','id','gening','gennotcre','gendetche','pagret','codcta','estdt','estdc','created_at','updated_at','deleted_at','codigoid','codsuc_reg', 'codsuc'
   ];
}
