<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class caartalm extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;


    protected $connection = 'pgsql2';

    protected $table = 'caartalm';

    protected $fillable = ['codalm','codart','codsuc'];

    public function articulos()
    {
            return $this->belongsTo('App\Caregart', 'codart','codart');
    }
    
    public function almacenes()
    {
            return $this->belongsTo('App\Almacen', 'codalm','codalm');
    }
}
