<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Sucursal extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    protected $connection = 'pgsql2';
    protected $table = "fadefsu";
    protected $fillable = [
        'nomsucu',
        'dirfis',
        'codpai',
        'codedo',
        'codalm',
        'codrif',
        'codsuc',
        'codigoid',
        'codempre',
        'status'
    ];

    protected static $logAttributes = ['nomsucu','dirfis','codpai','codedo','codalm','codrif','codsuc','codigoid','codemp','codempre'];
    protected static $logName = 'fadefsu';
    protected static $logFillable = true;

    protected $appends = ['recargoCount'];

    public function __toString()
    {
      return $this->nomsucu;
    }

    public function pais(){
      return $this->hasOne(\App\Models\Pais::class,'id','codpai');
    }
    public function estados(){
      return $this->hasOne(\App\Models\Estado::class,'id','codedo');
    }
    
    public function recargos(){
      return $this->belongsToMany(
          'App\Farecarg',//tabla EXTREMO
          'App\Fasuc_rgo',//tabla pivote
          'codsuc',//pivote key , llave de tabla pivote que se relaciona a ESTE MODELO <-**
          'codrgo',//llave table pivote que se RELACIONA A MODELO EXTREMO              <-*
          'codsuc',//llave principal de ESTE modelo que relaciona con llave pivote     <-**
          'codrgo'//llave de tabla extremo a comprar con llave de tabla pivote         <-*
          )->withPivot(['codsuc','codrgo','codigoid'])->wherePivot('deleted_at',null);
    }

    public function cajas(){
      return $this->hasMany('App\Models\Cajas','codsuc','codsuc');
    }

    public static function getlastcode(){
      // return self::where('deleted_at',NULL)->orderbydesc('id')->first();
      return self::withTrashed()->get()->count();
    }

    public static function getcount(){
      return self::withTrashed()->get()->count();
    }

    public static function findbycod($codigoid){
      return self::where('deleted_at',NULL)->where('codigoid',$codigoid)->firstOrFail();
    }
    // public static function findbycod($codigoid){
    //   return self::where('deleted_at',NULL)->where('codigoid',$codigoid)->firstOrFail();
    // }

    public function company(){
      return $this->belongsTo(Empresa::class,'codempre','codrif');
    }

    // public function bancos(){
    //   return $this->hasManyThrough(Bancos::class,Empresa::class,'');
    // }
    
    public function getRecargoCountAttribute(){
      return $this->recargos->count();
    }

    public function Ventas(){
      return $this->hasMany('App\Fafactur','codsuc','codsuc');
    }

    public function transporCost(){
      return $this->belongsTo('App\Caregart','codsuc_reg','codsuc');
    }


}
