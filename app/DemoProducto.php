<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class DemoProducto extends Model implements Auditable
{
    use SoftDeletes,\OwenIt\Auditing\Auditable;
    protected $connection='pgsql2';
    protected $table = 'demoproducto';

    protected $fillable = [
        'nombredemo','nomendemo','codigoid','codsuc'
   ];
    protected static $logAttributes = ['nombredemo','nomendemo','codigoid','codsuc'];
    protected static $logName = 'demoProducto';
    protected static $logFillable = true;

   public function articulos(){
        return $this->hasMany('App\Caregart','codemo','codigoid');
   }
}
