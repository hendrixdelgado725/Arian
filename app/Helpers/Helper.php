<?php
namespace App\Helpers;

use Cache;
use DB;
use Image;
use Request;
use Route;
use Schema;
use Session;
use Storage;
use Validator;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;


class Helper
{
    public static function strUp(string $string)
    {
        return strtoupper($string);
    }

    public static function redirect($to, $message, $type = 'warning')
    {

        if (Request::ajax()) {
            $resp = response()->json(['message' => utf8_encode($message), 'message_type' => $type, 'redirect_url' => $to])->send();
            exit;
        } else {
            $resp = redirect($to)->with(['message' => utf8_encode($message), 'message_type' => $type]);
            Session::driver()->save();
            $resp->send();
            exit;
        }
    }

    public static function redirectBack($message, $type = 'warning')
    {
        if (Request::ajax()) {
            $resp = response()->json(['message' => utf8_encode($message), 'message_type' => $type, 'redirect_url' => $_SERVER['HTTP_REFERER']])->send();
            exit;
        } else {
            $resp = redirect()->back()->with(['message' => utf8_encode($message), 'message_type' => $type]);
            Session::driver()->save();
            $resp->send();
            exit;
        }
    }

    public static function adminPath($path = null)
    {
       // return url(config('crudbooster.ADMIN_PATH').'/'.$path);
       return route($path);
        //return $path ? url(config('crudbooster.ADMIN_PATH').'/'.$path) : '/';
    }

    public static function mainpath($path = null)
    {
        // $controllername = str_replace(["\crocodicstudio\crudbooster\controllers\\", "App\Http\Controllers\\"], "", strtok(Route::currentRouteAction(), '@'));
        // $route_url = route($controllername.'GetIndex');

      $route_url = route($path);

        if ($path) {
            if (substr($path, 0, 1) == '?') {
                return trim($route_url, '/').$path;
            } else {
                return $route_url.'/'.$path;
            }
        } else {
            return trim($route_url, '/');
        }
    }

    public static function EncriptarDatos($str)
   {
     $a= array('1' => array('z', 'v', 'n', 'X'),
               '2' => array('x', 'c', '1', 'e'),
               '3' => array('5', '8', 'h', 'T'),
               '4' => array('0', 'Q', 'g', '9'),
               '5' => array('L', 'P', 'm', 'q'),
               '6' => array('3', 'A', 'u', 'Y'),
               '7' => array('y', 'k', 'B', 'V'),
               '8' => array('w', 'W', '6', 'r'),
               '9' => array('U', 'o', 'J', 'O'),
               '0' => array('s', 'D', 'H', 'F'),
               'E' => array('Z', '2', '4', 'M'),
               'P' => array('d', 'i', 'f', 'a')
            );
            
        $k1=  'dsLkFsifuW90158La';
        $k2=  'mgfFZagx0498wXTdi';

     // Proceso de encriptado
     $n= strlen($str);
     for($i = 0; $i < $n; $i++)
     {
         $fg=false;
         foreach($a as $idx => $v)
         {
            $idx= $idx.'';
            if($idx===$str[$i]){
              $fg=true;
              break;
            }
         }

         if($fg){
            $str[$i]= $a[$idx][rand(0,3)];
         }
      }//for

      return bin2hex($str ^ $k1 ^ $k2);
   }//EncriptarDatos


   public static function desencriptar($cipher)
   {                                                           
      $a= array('1' => array('z', 'v', 'n', 'X'),              
               '2' => array('x', 'c', '1', 'e'),               
               '3' => array('5', '8', 'h', 'T'),               
               '4' => array('0', 'Q', 'g', '9'),               
               '5' => array('L', 'P', 'm', 'q'),               
               '6' => array('3', 'A', 'u', 'Y'),               
               '7' => array('y', 'k', 'B', 'V'),               
               '8' => array('w', 'W', '6', 'r'),               
               '9' => array('U', 'o', 'J', 'O'),               
               '0' => array('s', 'D', 'H', 'F'),               
               'E' => array('Z', '2', '4', 'M'),               
               'P' => array('d', 'i', 'f', 'a')                
            );                                                 
                                                               
      $k1=  'dsLkFsifuW90158La';                               
      $k2=  'mgfFZagx0498wXTdi';                               
      $cipher = hex2bin($cipher);  //urldecode($cipher);       
      $cipher= $cipher ^ $k1 ^ $k2;                            
                                                               
      $n= strlen($cipher);                                     
      
           for($i=0; $i<$n; $i++)
      {
         $c= $cipher[$i];
         $fg= false;
         foreach($a as $idx => $v)
         {
            $idx= $idx.'';
            foreach($v as $t)
            {
               if($t===$c)
               {
                  $fg= true;
                  break;
               }

            }

            if($fg) break;
         }

         if($fg){
               $cipher[$i]= $idx;
         }
      }//for

      return $cipher;
   }//desc

   public static function crypt($tocrypt){
    $rand  = rand(1, 9999);
    $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
    $id=self::EncriptarDatos($nrand.'-'.$tocrypt);
    return $id;
   }

   public static function decrypt($cipher){
    $desc = self::desencriptar($cipher);
    $sep  = explode('-',$desc);
    $id= (int)$sep[1];
    return $id;
   }

    public static function filtroBold($string,$filtro = null){
    if(!$filtro) return $string;
    return \str_replace($filtro,"<b>".$filtro."</b>",$string);
    }

    public static function getSpanishDate($fechaeng,$tipo = ''){
        $dia = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];
        $days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
        $meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiempre','Octubre','Noviembre','Diciembre'];
        //$months = ['January','February','March','April','May','June','July','August','September','October' ,'November','December'];
        $fechaeng = explode(' ',$fechaeng);//0:dia/1:dialetras/2:mesNumero/3:mesletra/4:año
        $index = \array_search($fechaeng[0],$days);
        $fechaeng[3] = $meses[$fechaeng[2]-1];
        return $dia[$index]." ".$fechaeng[1]." de ".$fechaeng[3]." del ".$fechaeng[4];
        
    }

    public static function getmenu(){
      $yaml = new Parser();
      $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
      return $menu['menu'];
    }

    public static function getcodigoid(){
        $model_name = 'User';
        $model = app("App\Model\{$model_name}");
        $model->where('id', $id)->first();
    }

    public static function getActiveLink($ruta,$rutamodulo)
    {
        $uri = "modulo/".print_r($rutamodulo,true);
        return $uri === $ruta ? 'active': '';
    }

    public static function getActive($array,$ruta){

        //dd($array,$ruta);

        // foreach ($array as $modulos => $menu) {
        //     foreach ($variable as $key => $value) {
        //         foreach ($variable as $key => $value) {
        //             # code...
        //         }
        //     }
        // }

    }

}

?>