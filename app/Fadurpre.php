<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Fadurpre extends Model implements Auditable
{
    use SoftDeletes,\OwenIt\Auditing\Auditable;

    protected $connection='pgsql2';
    protected $table = 'fadurpre';
    protected $fillable = [
        'duracion','codigoid','codsuc'
   ];

    protected static $logAttributes = ['duracion','codigoid','codsuc'];
    protected static $logName = 'fadurpre';
    protected static $logFillable = true;
    

   public function presupuesto(){
       return $this->hasMany('App\Fapresup','duracion_id','codigoid');
   }
}
