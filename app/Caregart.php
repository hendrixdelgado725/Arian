<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;
use App\famodelo;
use App\Faartfac;
use Carbon\Carbon;

class Caregart extends Model implements Auditable
{
	    use SoftDeletes,\OwenIt\Auditing\Auditable;


    protected $connection = 'pgsql2';
    protected $table = 'caregart';

    protected $fillable = ['codcat','codcolor','id','codart','desart', 'nomart','codcta','codpar','ramart','cosult','cospro','exitot', 'tipo',
                           'unimed','codtallas','codigomarca','codsubcat','tipreg','codemo','codsuc_reg','codigoid','artprecio',
                           'desfac','ctadef','codcta','ctatra','tipreg','codpar','codsgp','isserial','ivastatus'];

	protected static $logAttributes = ['codart','desart','codtallas'];
    protected static $logName = 'caregart';
    protected static $logFillable = true;

    public static function getValidArts(){
        return self::where('tipreg','F')
			/* ->whereNotNull('talla')->whereNotNull('codemo') */
			/* ->whereNotNull('codsubcat')->whereNotNull('codcolor')
			->whereNotNull('codcat') */
			->with('faartpvp')->whereHas('faartpvp',function($q)
    	{
    		$q->where('status','A')->whereNull('deleted_at');

			})->get();
    }

    public function faartpvp()
    {
    	return $this->hasOne('App\faartpvp','codart','codart');
    }

    public function costoUnit(){
        return $this->hasOne('App\faartpvp','codart','codart')->orderBy('id','DESC');
    }

    public function costoPro(){
        return $this->faartpvp()->with(['almacen', 'moneda'])->where('status','A');
    }

    // public function costoAtTime($){
    //     return $this->faartpvp()->where();
    // }

    public function facategoria()
    {
        return $this->hasOne('App\Facategoria','codigoid','codcat');
    }
    public function color()
    {
        return $this->hasOne('App\ColorPro','codigoid','codcolor');
    }
    public function colores()
    {
        return $this->hasMany('App\ColorPro','codigoid','codcolor');
    }
    public function subcategoria()
    {
        return $this->hasOne('App\Fasubcategoria','codigoid','codsubcat');
    }
    public function demoProducto(){

        return $this->hasOne('App\DemoProducto','codigoid','codemo');
    }

    public function inventario(){
        return $this->hasOne(Caartalmubi::class,'codart','codart');
    }

    public function existAlm($codalm){
        return $this->with('facategoria')->whereHas('facategoria')->with('inventario')->whereHas('inventario',function ($q) use ($codalm){
            $q->where('codalm',$codalm)->where('exiact','>',0);
        })->get();
    }

    public function almacen()
    {
        return $this->hasOneThrough(Almacen::class,faartpvp::class,'codart','codigoid','codart','codalm');
    }
    public function almacen2()
    {
        return $this->hasOneThrough(Almacen::class,faartpvp::class,'codart','codalm','codart','codalm');
    }
    public function moneda()
    {
        return $this->hasOneThrough(
            'App\Famoneda',/*modelo fianl*/
            'App\faartpvp',/*modelo intermedio*/
            'codart',
            'codigoid',
            'codart',
            'codmone'
        );
    }

    public function getartubi(){//obtiene la ubicacion del articulo si existe en un almacen
        return $this->belongsToMany(
            'App\Cadefubi',
            'App\Caartalmubi',
            'codart',
            'codubi',
            'codart',
            'codubi'
        )->withPivot('exiact','codalm');
    }

    public function getartubiwalm($codart,$codalm){//obtiene la ubicacion del articulo si existe en un almacen
        return $this->where('codart',$codart)->with('getartubi')->whereHas('getartubi', function ($q) use($codalm){
            $q->where('codalm',$codalm);
        })->get();
    }

    public function almacenubi()
    {
        return $this->hasMany('App\Caartalmubi','codart','codart');
    }

    public function cantSalida()
    {
        return $this->hasMany('App\Caartalmubimov','codart','codart');
    }

    public function tallas($a)
    {

        $talla = null;
        $temp = [];
        for ($i=0; $i < count($a); $i++) {
        $talla = \App\Tallas::where('codtallas','=',$a[$i])->first();
        if(!is_null($talla))
            array_push($temp,$talla->codtallas);
        }
        return $temp;
    }

    public static function getFirstArt($codart){
        return self::where('codart',$codart)->first();
    }

    public function costos(){
        return $this->hasMany('App\faartpvp','codart','codart')->where('status','A');
    }

    public function getartinfo($codart){
        return  $this->with('faartpvp')->where('codart',$codart)->whereHas('faartpvp',function($q) {
            $q->where('status','A')->whereNull('deleted_at');
            })->with('almacenubi')->whereHas('almacenubi',function ($q){
            $q->whereNull('deleted_at');
            })->with('almacenexist')->whereHas('almacenexist',function ($q){
            $q->whereNull('deleted_at');
            });
    }

    public function scopeDesart($query,$desart){
        return ($desart!== null ) ? $query->where('desart','ilike','%'.$desart.'%') : $query;
    }

    public function scopeCodart($query, $codart){
        return ($codart !== null ) ? $query->where('codart','ilike','%'.$codart.'%') : $query;
    }

    // public static function getValidCat($cat){

    //     $validCategories = [
    //         'ROPA' => [
    //             'CALZADO',
    //             'TEXTILES'
    //         ],
    //         'VIVERES',//estatica
    //     ];

    // }

    public function getMarca()
    {
        return $this->hasOne(famodelo::class,'codigoid','codigomarca');
    }


    public static function notDeleteBill($codigoArt)
    {
           $factura = faartfac::where('codart',$codigoArt)->first();
           if ($factura !== null) {
               return true;
           }

           return false;

    }

    public function seriales()
    {
        return $this->hasMany('App\Models\SerialEntradas','codart','codart');
    }

    public function serialesFacturados()
    {
        
         return $this->hasOne('App\Models\Serial','codart','codart');
    }

    protected $casts = [
        'fecult' => 'string',
        'deleted_at' => 'date',
        'almacen_id' => 'integer'
    ];

    public function cadetpro()
    {
        
         return $this->hasOne('App\Models\Cadetpro','codart','codart');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function almacenes()
    {
            return $this->hasMany(caartalm::class, 'codart','codart');
    }

    public function allTallas()
    {
            return $this->hasMany('App\Models\Caarttal', 'codart','codart');
    }

    public function getTipoAttribute($value)
    {
        $value === 'A' ? $var = 'COMPRA' : $var = 'SERVICIO';
        return $var;
    }

}
