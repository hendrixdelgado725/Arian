<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Nphojint extends Model implements Auditable
{
    use SoftDeletes,\OwenIt\Auditing\Auditable;
    
    protected $connection='pgsql2';
    protected $table = 'nphojint';
    protected $fillable = [
        'codemp','nomemp','cedemp','rifemp','codcar',
        'nomcar','dirhab','celemp', 'telhab','emaemp',
        'fecing','fecret','staemp','codigoid','codsuc'
   ];

    public function npasicaremp (){
        return $this->hasOne('App\Npasicaremp','codemp','codemp')->orderBy('id','DESC');
    }

    public function familiares()
    {
        return $this->hasMany(Npinffam::class,'codemp','codemp');
    }

    public function getMaingerenciaAttribute()
    {
        return optional(Npstorg::whereCod_niv(substr($this->cod_niv,0,3))->first())->desniv ?? '';
    }

    public function gerencia()
    {
        return $this->hasOne('App\Models\Npstorg', 'codniv', 'codniv');
    }

    public static function getEmp($codemp)
    {
        return self::select(['codemp','nomemp','staemp','emaemp'])->where('codemp',$codemp)->where('staemp','A')->first();
    }
    
}
