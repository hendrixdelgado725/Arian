<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Contracts\Auditable;


class Cadefubi extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql2';

    protected $table = 'cadefubi';

    protected $fillable = ['codubi','nomubi','tipreg'];

    public function Almacenes(){
        return $this->belongsToMany(
            'App\Almacen',//tabla EXTREMO
            'App\Alma_ubi',//tabla pivote
            'codubi',//pivote key , llave de tabla pivote que se relaciona a ESTE MODELO <-**
            'codalm',//llave table pivote que se RELACIONA A MODELO EXTREMO              <-*
            'codubi',//llave principal de ESTE modelo que relaciona con llave pivote     <-**
            'codalm'//llave de tabla extremo a comparar con llave de tabla pivote         <-*
        )->wherePivot('deleted_at',null);
    }

    public static function GetList(){
        return self::orderby('id','desc')->where('deleted_at',null)->with('Almacenes')->whereHas('Almacenes')->paginate(10);
    }

    public static function getByName($nomubi,$idubi){
        return self::where('nomubi',$nomubi)->where('id','<>',$idubi)->first();
    }
    
}
