<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Caregart;
use App\faartpvp;
use \DB;


class update_pvpart_precios extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:update_pvp_art_precios';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualizar los precios del sistema y activar el precio mas cercano';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::connection('pgsql2');

        $articulos = Caregart::where('tipreg','F')->get();

        foreach ($articulos as $key => $value) {
            $precios = faartpvp::where('codart', $value->codart)->orderBy('id','desc')->get();
            if(!$precios->isEmpty()){
                $articulo = $precios->first();
                $articulo->status = 'A';
                $articulo->save();
                $this->info("Articulo : [".$value->codart."] Actualizado correctamente");
            }
        }
    }
}
