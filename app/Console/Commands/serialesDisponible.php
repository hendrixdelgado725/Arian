<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\SerialEntradas;
use App\Models\serialDisponible;
class serialesDisponible extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:seriales';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'busca los seriales disponible';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
         $s = SerialEntradas::where('salida',false)->whereNull('deleted_at')
		     ->doesntHave('SerialesFacturados')->get();
		 
             foreach ($s as $key => $value) {
                
                serialDisponible::create([
                    'seriales' => $value->serial,
                    'codart' => $value->codart,
                    'disponible' => true,
                    'codalm' => $value->codalm,
                ]);

             }

             $this->info('Se ha registrado los seriales');
        return 0;
    }
}
