<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Cadefpro;
use Carbon\Carbon;
use App\Caregart;
class PromocionesAnular extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'promocion:anular';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Anula la promocion cuando se cumpla el tiempo de existencia establecido';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
    */
    public function handle()
    {
         $hoy = Carbon::now();
  
         $promociones = Cadefpro::where('staprom', '=', 'A')->where('fechasprom', '<=', $hoy)->get();
       
        
         foreach ($promociones as $promocion) {
          
           $prom = Cadefpro::where('codartprom', '=', $promocion->codartprom)->update(['staprom' => 'R', 'status' => 'I' ,'motanu' => 'EXPIRO EL TIEMPO DE CREACIÓN', 
                                                                                                             'nomcre' => 'ANULADO POR EL SISTEMA']);
           $caregart = Caregart::where('codart', $promocion->codartprom)->update(['staart' => 'I']);
        }

        return 0;
       
       
    }

}
