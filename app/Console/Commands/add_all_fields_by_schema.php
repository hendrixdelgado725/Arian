<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class add_all_fields_by_schema extends Command
{
    /** hendrix estuvo por aqui 27-11-2023
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:add_all_fields_by_schema';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Este comando añade los campos requeridos para que laravel funcione');

        $schema = $this->ask('Introduzca el Esquema a actualizar');

        if(!$schema){
            $this->error('Debe insertar un esquema');
        }

        $schemas = DB::connection('pgsql_public')
        ->select("SELECT schema_name FROM information_schema.schemata WHERE schema_name like 
        '%".$schema."%'");

        if(count($schemas) === 0 || count($schemas) > 1){
            $this->error('No hubo coincidencias de busqueda, o debes ser mas específico');
            return;
        }

        $schema = !($s = $schemas[0]->schema_name) ? false : $s;

        $tables = DB::connection('pgsql_public')
        ->select("SELECT * FROM information_schema.tables WHERE table_schema = '".$schema."'");

        if(count($tables) === 0){
            $this->error('No existen tablas en este esquema');
        }

        
        foreach ($tables as $key => $value) {

            if($value->table_type != 'BASE TABLE'){
                continue;
            }
            DB::connection('pgsql_public')->select("ALTER TABLE \"".$schema."\".".$value->table_name." drop column if exists created_at");

            DB::connection('pgsql_public')->select("ALTER TABLE \"".$schema."\".".$value->table_name." drop column if exists updated_at");

            DB::connection('pgsql_public')->select("ALTER TABLE \"".$schema."\".".$value->table_name." drop column if exists deleted_at");

            DB::connection('pgsql_public')->select("ALTER TABLE \"".$schema."\".".$value->table_name." drop column if exists codigoid");

            DB::connection('pgsql_public')->select("ALTER TABLE \"".$schema."\".".$value->table_name." drop column if exists codsuc");
            
            DB::connection('pgsql_public')->select("ALTER TABLE \"".$schema."\".".$value->table_name." ADD COLUMN created_at timestamp without time zone");
            
            DB::connection('pgsql_public')->select("ALTER TABLE \"".$schema."\".".$value->table_name." ADD COLUMN updated_at timestamp without time zone");

            DB::connection('pgsql_public')->select("ALTER TABLE \"".$schema."\".".$value->table_name." ADD COLUMN deleted_at timestamp without time zone");

            DB::connection('pgsql_public')->select("ALTER TABLE \"".$schema."\".".$value->table_name." ADD COLUMN codigoid character varying(40)");

            DB::connection('pgsql_public')->select("ALTER TABLE \"".$schema."\".".$value->table_name." ADD COLUMN codsuc character varying(40)");
        }

        
        
        $this->info("Done");


    }
}
