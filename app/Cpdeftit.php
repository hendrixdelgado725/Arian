<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cpdeftit extends Model
{
    use HasFactory;

    protected $connection = 'pgsql2';
    protected $table = 'cpdeftit';

    protected $fillable = ['codpre','nompre','codcta','stacod','coduni','estatus'];

    public function imputs(){
        return $this->hasMany(Cpimprc::class, 'codpre', 'codpre');
    }
}
