<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Support\Facades\Auth;


class Almacen extends Model implements Auditable
{	
	use SoftDeletes;
  use \OwenIt\Auditing\Auditable;
	

	  protected $connection = 'pgsql2';
    protected $table = "cadefalm";
    protected $fillable = ['id','codalm','nomalm','diralm','created_at','update_at','deleted_at','codigoid','codsuc_reg','codsuc_asoc','tipreg'];
	
    protected static $logAttributes = ['codalm','nomalm','diralm'];
    protected static $logName = 'cadefalm';
    protected static $logFillable = true;



	public function ubicaciones(){
		return $this->hasMany('App\Alma_ubi','codalm','codalm');
	}

	public function ubinames (){
		return $this->hasOneThrough('App\Cadefubi','App\Alma_ubi','codalm','codubi','codalm','codubi');
	}
	public function ubinamesx(){
		return $this->hasManyThrough('App\Cadefubi','App\Alma_ubi','codalm','codubi','codalm','codubi');
	}

	public static function getubi($codalm){
		return self::with('ubinames')->whereHas('ubinames')->where('codalm',$codalm)->get();
  }
  
	public static function getubix($codalm){
		return self::with('ubinamesx')->whereHas('ubinamesx')->where('codalm',$codalm)->get();
	}

	public function sucursal()
   {
    return $this->belongsTo(Sucursal::class,'codsuc_asoc','codsuc');
   }

   public function getNombreSucursal()
   {
    return Sucursal::where([['codsuc',$this->codsuc_asoc]])->first();
   }

   public function sucu()
    {
      return $this->hasOne(Sucursal::class,'codsuc','codsuc_asoc');
    }

  public static function getValid(){
    return self::whereNull('codsuc_asoc')->whereNull('deleted_at')->get();
  }


  public function existencia(){
    return $this->hasMany('App\Caartalmubi','codalm','codalm');
  }


  public static function getAlmCreateUbi(){
    return self::whereNotNull('codsuc_asoc')->get();
  }

  public static function getWithExist(){
    
    return self::where('codsuc_asoc',session('codsuc'))->with('existencia')->whereHas('existencia',function($q){
      $q->where('exiact','>=',0);
    })->whereNotNull('codsuc_asoc')->orderby('codalm','ASC')->get();
  }

   public static function getWithExist2(){
    
    return self::where('codsuc_asoc',session('codsuc'))->with('existencia')->whereHas('existencia',function($q){
      $q->where('exiact','>=',0);
    })->whereNotNull('codsuc_asoc')->orderby('codalm','ASC')->get();
  }

  public function scopeSucursal($query)
  {
    return $query->where('codsuc_asoc',session('codsuc'));
  }

  public function articulos()
    {
       return $this->hasMany('App\caartalm', 'codalm','codalm');
    }

}