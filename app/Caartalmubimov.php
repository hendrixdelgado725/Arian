<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;


class Caartalmubimov extends Model implements Auditable
{

    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql2';

    protected $table = 'caartalmubimov';

    protected $fillable = ['codmov','codart','codubi','cantmov','desmov','codalmfrom','codalmto','tipmov','codubifrom','codubito','codigoid','codtalla','codsuc','preunit'];


    public function almacenfrom(){
      return $this->hasOne(Almacen::class,'codalm','codalmfrom');
    }

    public function almacento(){
      return $this->hasOne(Almacen::class,'codalm','codalmto');
    }

    public function codubifrom(){
      return $this->hasOne(Cadefubi::class,'codubi','codubifrom');
    }

    public function codubito(){
      return $this->hasOne(Cadefubi::class,'codubi','codubito');
    }
    ///Agregados porque las relaciones codubito y codubifrom tienen conflicto por tener nombres de campos 
    public function ubifrom(){
      return $this->hasOne(Cadefubi::class,'codubi','codubifrom');
    }

    public function ubito(){
      return $this->hasOne(Cadefubi::class,'codubi','codubito');
    }
    ///

    public function articulo(){
      return $this->hasOne(\App\Caregart::class,'codart','codart');
    }

    public function talla(){
      return $this->hasOne(Tallas::class,'codtallas','codtalla');
    }

    public static function getCount()
    {
      return self::withTrashed()->get()->count();
    }

    public static function findArts($codubi,$codalm){
      return self::where('codubifrom',$codubi)
                    ->orWhere('codalmfrom',$codalm)
                    ->orWhere('codubito',$codubi)
                    ->orWhere('codalmto',$codalm)
                    ->orWhere('codubito',$codubi)
                    ->get();
  }

    public static function findByMovDetail($codmov,$tipo){
      return self::where('codmov',$codmov)->where('desmov',$tipo)
                      ->with(['almacenfrom' => function($q){
                        $q->select(['codalm','nomalm']);
                      }])->with(['almacento' => function($q){
                        $q->select(['codalm','nomalm']);
                      }])
                      ->with(['codubifrom' => function($q){
                        $q->select(['codubi','nomubi']);
                      }])
                            ->with(['codubito' => function($q){
                              $q->select(['codubi','nomubi']);
                            }])
                              ->with('articulo.costoPro')
                              ->with('articulo.moneda')
                              ->with('talla')
                              ->with('color')
                              ->orderBy('codart','ASC')->get();
    }

    public static function findByMov($codmov){
      return self::where('codmov',$codmov)->orderBy('codart','ASC')->get();
    }

    public static function findByMovType($codmov,$movtype){
      return self::where('codmov',$codmov)->where('desmov',$movtype)->get();
    }
    
    public function demografia(){
      return $this->hasOneThrough(
          DemoProducto::class,//tabla final
          Caregart::class,//tabla pivote
          'codart',// Foreign key on pivote table***
          'codigoid', // Foreign key final table*
          'codart', // Local key on this table table***
          'codemo'// Local key on pivote table*
      );
    }

    public function subCategoria(){
      return $this->hasOneThrough(
          Fasubcategoria::class,//tabla final
          Caregart::class,//tabla pivote
          'codart',// Foreign key on pivote table*
          'codigoid',// Foreign key final table***
          'codart',// Local key on this table table*
          'codsubcat'// Local key on pivote table***
      );
    }

    public function color(){
      return $this->hasOneThrough(
          ColorPro::class,//tabla EXTREMO
          Caregart::class,//tabla pivote
          'codart',
          'codigoid',//LLAVE FORANEA EXTREMO
          'codart', //LOCAL
          'codcolor' //LOCAL EN PIVOTE
      );
    }

    public function categoria(){
      return $this->hasOneThrough(
          Facategoria::class,//tabla EXTREMO
          Caregart::class,//tabla pivote
          'codart',
          'codigoid',
          'codart',
          'codcat'
      );
    }
    public function scopeDate($query,$minDate = null,$maxDate = null){
      
      return ($minDate != null && $maxDate != null) ? $query->whereDate('created_at','>=',date($minDate))->whereDate('created_at','<=',date($maxDate)) : $query;
      
   }
    public static function  getArtToEmail($codmov,$tipo = '')
    {
      return self::where('codmov',$codmov)//->where('desmov',$tipo)
                      ->with(['almacenfrom' => function($q){
                        $q->select(['codalm','nomalm']);
                      }])
                      ->with(['almacento' => function($q){
                        $q->select(['codalm','nomalm']);
                      }])
                      ->with(['codubifrom' => function($q){
                        $q->select(['codubi','nomubi']);
                      }])
                      ->with(['codubito' => function($q){
                        $q->select(['codubi','nomubi']);
                      }])
                      ->with(['articulo' => function($q){
                        $q->select(['codart','desart']);
                      }])
                      // ->with(['demografia' => function ($q){
                      //   $q->select(['codigoid','nombreDemo','nomenDemo']);
                      // }])
                      // ->with(['subCategoria' => function ($q){
                      //   $q->select(['codigoid','nomsubcat','nomensubcat']);
                      // }])
                      // ->with(['color' => function ($q){
                      //   $q->select(['codigoid','color','nomencolor']);
                      // }])
                      // ->with(['categoria' => function ($q){
                      //   $q->select(['codigoid','nombre','nomencat']);
                      // }])
                      ->with('talla')
                      ->orderBy('codart','ASC')->get();
    }

}
