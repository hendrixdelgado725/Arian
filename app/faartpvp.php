<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;
use App\Fatasacamb;


class faartpvp extends Model implements Auditable
{
	   use SoftDeletes,\OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql2';
    protected $table = 'faartpvp';
    protected static $logName = 'faartpvp';
    protected static $logAttributes = ['codart','pvpart','status','codmone','codalm'];
    protected static $logFillable = true;
		protected $fillable = ['pvpart','codmone','codmone','status','codart','codigoid','codsuc_reg','codalm'];

    public function articulo()
    {
        return $this->belongsTo('App\Caregart','codart','codart');
    }

    public function moneda()
    {
    	return $this->hasOne('App\famoneda','codigoid','codmone');
    }

    public function getTasa()
    {
        return $this->hasOne(Fatasacamb::class,'id_moneda','codmone');
    }

    public function almacen()
    {
        return $this->hasOne('App\Almacen','codalm','codalm');
    }
}
