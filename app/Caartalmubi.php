<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use OwenIt\Auditing\Contracts\Auditable;

class Caartalmubi extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql2';

    protected $table = 'caartalmubi';

    protected $fillable = ['codalm','codart','codubi','exiact','id','numlot','fecela','fecven','codtalla','created_by','codigoid',
    'codsuc'];

    protected $appends = ["diasAlmacenCount"];

    public static function getartubi($codart,$codalm){
        return self::where('codart',$codart)->where('codalm',$codalm);
    }

    public static function getCount()
    {
      return self::withTrashed()->get()->count();
    }

    public function ubicaciones(){
        return $this->hasOne('App\Cadefubi','codubi','codubi');
    }

    public function articulos(){
        return $this->belongsTo('App\Caregart','codart','codart');
    }

    public function precioArt(){
        return $this->hasOneThrough(
            'App\faartpvp',//
            'App\Caregart',
            'codart',
            'codart',
            'codart',
            'codart'
        );
    }
    public function serialesDisponible()
    {
        return $this->hasMany('App\Models\serialDisponible','codart','codart');
    }

    public function seriales()
    {
        return $this->hasMany('App\Models\SerialEntradas','codart','codart');
    }

    public function tallaArt(){
        return $this->hasOne('App\Tallas','codtallas','codtalla');
    }

    public function almacen(){
        return $this->belongsTo('App\Almacen','codalm','codalm');
    }

    public static function findQty($codart,$codalm,$codubi,$codtalla = null){
        return self::where('codart',$codart)->where('codalm',$codalm)->where('codubi',$codubi)->where('codtalla',$codtalla)->with('articulos')->first();
    }

    public static function findQtyAll($codart,$codalm,$codubi,$codtalla = null){
        return self::where('codart',$codart)->where('codalm',$codalm)->where('codubi',$codubi)->where('codtalla',$codtalla)->with('articulos')->first();
    }

    public static function findQtyArtAlm($codart,$codalm){
        return self::where('codart',$codart)->where('codalm',$codalm)->first();
    }

    public static function findArts($codubi,$codalm){
        return self::where('codubi',$codart)->where('codalm',$codalm)->get();
    }

    public static function findQtyUbi($codubi){
        return self::where('codubi',$codubi)->where('deleted_at',null)->where('exiact','<>','0')->first();
    }

    public function getDiasAlmacenCountAttribute(){
        $valor=$this->created_at ? $this->created_at->diffInDays(Carbon::now()) : 0;
        // dd($this->fecela);
        return $valor;
    }

    public function getUbiArtAlm($codart,$codalm){
        return $this->where('codart',$codart)
                    ->where('codalm',$codalm)
                    ->where('exiact','<>',0)
                    ->with('ubicaciones')
                    ->whereHas('ubicaciones')//trae los nombres de las ubicaciones
                    ->get();
    }


    public function tallas(){
        return $this->hasOne('App\Tallas','codtallas','codtalla');
    }
    
    public function scopeFindCant($query,$codtalla){
        return !($codtalla=== '') ? $query->where('codtalla',$codatalla) : $query;
    }

    public function scopeCategory($query,$codcat = null){
        return $codcat ? $query->whereHas('articulos',function($q) use($codcat){
            $q->where('codcat',$codcat);
        }) : $query;
    }

    public function scopeTalla($query, $codtalla = null)
    {
        return (isset($codtalla)) ? $query->where('codtalla',$codtalla) : $query; 
    }

    public function demografia(){
        return $this->hasOneThrough(
            DemoProducto::class,//tabla final
            Caregart::class,//tabla pivote
            'codart',// Foreign key on pivote table***
            'codigoid', // Foreign key final table*
            'codart', // Local key on this table table***
            'codemo'// Local key on pivote table*
        );
      }
  
      public function subCategoria(){
        return $this->hasOneThrough(
            Fasubcategoria::class,//tabla final
            Caregart::class,//tabla pivote
            'codart',// Foreign key on pivote table*
            'codigoid',// Foreign key final table***
            'codart',// Local key on this table table*
            'codsubcat'// Local key on pivote table***
        );
      }
  
      public function color(){
        return $this->hasOneThrough(
            ColorPro::class,//tabla EXTREMO
            Caregart::class,//tabla pivote
            'codart',
            'codigoid',
            'codart', 
            'codcolor'
        );
      }

      public function categoria(){
        return $this->hasOneThrough(
            Facategoria::class,//tabla EXTREMO
            Caregart::class,//tabla pivote
            'codart',
            'codigoid',
            'codart',
            'codcat'
        );
      }

}



