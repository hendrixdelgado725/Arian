<?php
namespace App\Scopes;


use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


class SucursalScope implements Scope
{

    public function apply(Builder $builder, Model $model)
    {
        // if() no es admin aplicar de lo contrario no
    		if(optional(auth()->user()->role)->nombre !== 'ADMIN' || false)$builder->where('codsuc', session('codsuc'));
    }

}