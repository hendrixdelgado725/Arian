<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Cobdocume extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public $timestamps = false;
    protected $connection='pgsql2';
    protected $table = 'cobdocume';

    protected $fillable = [
        'refdoc','codcli','codmov','fecemi','fecven','oridoc','desdoc','mondoc','recdoc','dscdoc','abodoc','saldoc','desanu','fecanu','stadoc','numcom','feccom','reffac','fatipmov_id','totret','totant','fadescripfac_id','reftra','coddirec','tradoc','refdocnc','monexo','refdocnd','numcon','tipcon','reftipcon','codgru','codper','dencom'
   ];
}
