<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Tsdefmon extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $timestamps = false;
    protected $connection='pgsql2';
    protected $table = 'tsdefmon';

    protected $fillable = [
        'codmon','nommon','aumdis'
   ];
}
