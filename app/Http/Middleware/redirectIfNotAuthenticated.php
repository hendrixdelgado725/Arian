<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class redirectIfNotAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $auth;
    public function __construct(Guard $user)
    {
        $this->auth = $user;
 }
    protected $except = [
       '/',
       'login',
       'logout',
       'error'
    ];
    public function handle(Request $request, Closure $next)
    {   
            $response = $next($request);
            if(!$this->checkExceptRoute($request->path())){
                return $response;
            }else{
                
                if(Auth::check()) return $response;
                 
            }

            return $response;
    }
    public function checkExceptRoute(string $route) :bool
    {
        return (bool) in_array($route,$this->except);
    }
}
