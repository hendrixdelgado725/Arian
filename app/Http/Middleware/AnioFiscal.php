<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
use Exception;
use App\Sucursal;
use App\Models\Fiscal;


class AnioFiscal
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $guard;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        '/',
        'login',
        'logout',
        'error',
        'modulo/Fasvit/Inicio',
        'modulo/Fasvit/Registro/Familiares',
        'modulo/Fasvit/getEmpleado',
        'modulo/Fasvit/Registro/Familiares/store',
        'modulo/Fasvit/Tratamientos/Registro',
        'modulo/Fasvit/Tratamientos/Registro/store',
        'modulo/Fasvit/getPats',
    ];
    

    public function handle(Request $request, Closure $next)
    {
            if($this->checkExceptRoute($request->route()->uri)){
                return $next($request);
            }

            $anio = (session('anio') !== null) ? session('anio') : ''.session('anio').'';
            $ESQUEMA = DB::select("SELECT schema_name FROM information_schema.schemata WHERE schema_name ='$anio'");//encuentra el squema

            if(empty($ESQUEMA) === false){//condicion que busca el esquema en bd
                $capture = get_object_vars($ESQUEMA[0]);
                Config::set(['database.connections.pgsql2.schema' => $capture['schema_name'] ]);
                Config::set(['database.default', 'pgsql2']);
                DB::purge('pgsql2');
                DB::reconnect('pgsql2');
                DB::connection('pgsql2');
            }else{
                Auth::logout();

                session(['anio' => ''.session('anio').'']);
                return abort(401);

            }
            
            
            return $next($request);
    }

    public function checkExceptRoute(string $route) :bool
    {
        return (bool) in_array($route,$this->except);
    }


}
