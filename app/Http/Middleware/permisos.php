<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Str;

class permisos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    protected $auth;
    protected $except = [
        '/',
        'login',
        'logout',
        'error',
        'Registro/Familiares/Fasvit'
     ];
    
    public function __construct(Guard $auth){

            $this->auth = $auth;

    }
    public function handle($request, Closure $next,$role,$rutas)
    {
        if($this->auth->user()->isAdmin()){
            return $next($request);
        }
        $P_user = \App\permission_user::where('codusuarios',$this->auth->user()->loguse)->where('rutasaccesso',$rutas)->whereNull('deleted_at')->first();
           # dd($rutas); 
        
        if(is_object($P_user)) {
                $temp = Str::contains($P_user->codpermission,$role);
                
                     if ($temp === true) {
                         return $next($request);
                     }
          }                   
         return abort(403);
        
    }
}
