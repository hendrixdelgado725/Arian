<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $urlBase = url()->to('/');
            $urlPrevious = url()->previous();
            if($urlPrevious === $urlBase || $request->path() == "/"){
                return redirect()->route('home');
            }else{
                return redirect()->intended('/');
            }
        }

        return $next($request);
    }
}
