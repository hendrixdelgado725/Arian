<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TasaCambioRequest;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\Fatasacamb;
use App\Famoneda;
use Helper;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Contracts\Auth\Guard;
use App\permission_user;
use App\Segususuc;

class TasaCambioController extends Controller
{
    private $guard;
    public function __construct(Guard $guard)
    {
        $this->middleware('auth');
        $this->guard = $guard;
        
    }

    public function index(){
        /*        $seg = permission_user::where('codusuarios',$this->guard->user()->userSegSuc->codusuario)->get();
         
         if (is_object($seg)) {
            
              for ($i=0; $i < count($seg); $i++) {    

                     $this->authorize('vista',$seg[$i]);
              }

         }*/
        $menu = new HomeController();
        $tasas = Fatasacamb::with('moneda','moneda2')->orderBy('id','DESC')->paginate(8);
        return view('/configuracionGenerales/tasaCambio')->with('menus',$menu->retornarMenu())->with('tasas',$tasas);
    }

    public function nuevaTasa(){
        $menu = new HomeController();
        $monedas = Famoneda::get();
        if($monedas->isEmpty())
        {
            return redirect('/modulo/ConfiguracionGenerales/TdeCambio')->with(['error' => 'Para iniciar el proceso debe registrar las Monedas en el Sistema']); 
        }
        return view('/configuracionGenerales/registroTasaCambio')->with('menus',$menu->retornarMenu())->with('monedas',$monedas);
    }

    public function create(TasaCambioRequest $request){

        if($request->moneda === $request->moneda2){
            Session::flash('error', 'Las monedas no pueden ser iguales');
            return redirect()->back();
        }
        $moneda = Famoneda::where('nombre','=',$request->moneda)->first();
        $moneda2 = Famoneda::where('nombre','=',$request->moneda2)->first();

            $request->cantidad = str_replace('.','',$request->cantidad);
            $request->cantidad = str_replace(',','.',$request->cantidad);

            $guion ='-';
            $cont = Fatasacamb::withTrashed()->get()->count();
            $cont = $cont +1;
            // $suc = Auth::user()->codsuc_reg;
            $suc=Auth::user()->getCodigoActiveS();
            $codsuc_reg = explode('-',$suc);
            $cod = $cont.$guion.$codsuc_reg[1];


        // $existe = Fatasacamb::with('moneda')->whereHas('moneda',function($q) use ($moneda){
        //     $q->where('codigoid','=',$moneda->codigoid);
        // })->whereHas('moneda2',function($q) use ($moneda2){
        //     $q->where('codigoid','=',$moneda2->codigoid);   
        // })->orderBy('id','DESC')->first();
        $existe = Fatasacamb::with('moneda')->where('activo',true)->orderBy('id','DESC')->first();
 
        if($existe){
            $cont = $existe->count();
            if($cont > 0){
                $tasavieja = Fatasacamb::where('codigoid','=',$existe->codigoid)->update(
                    ['activo' =>  false]
                );
            }
        }

        Fatasacamb::create([
            'codigoid'=>$cod,
            'id_moneda'=>$moneda->codigoid,
            'id_moneda2'=>$moneda2->codigoid,
            'valor'=>$request->cantidad,
            'activo'=>true,
            'codsuc'=>$suc,
        ]);

        Session::flash('success', 'La tasa de cambio, se registro Exitosamente');
        return redirect('/modulo/ConfiguracionGenerales/TdeCambio');
    }

    
    public function update(TasaCambioRequest $request, $id){

        $tasa = Fatasacamb::find($id);
        $tasa->valor = $request->cantidad;
        $tasa->save();

        Session::flash('success', 'La tasa de cambio, se actualizó Exitosamente');
        return redirect('/modulo/ConfiguracionGenerales/TdeCambio');
    }

    public function delete(){

    }

    public function filtro(Request $request){
        $menu = new HomeController();
        $tasas = Fatasacamb::with('moneda','moneda2')->whereHas('moneda',function($q) use ($request){
            $q->where('nombre','ilike','%'.$request->filtro.'%')->orWhere('nomenclatura','ilike','%'.$request->filtro.'%');
        })->orWhereHas('moneda2',function($q) use ($request){
            $q->where('nombre','ilike','%'.$request->filtro.'%')->orWhere('nomenclatura','ilike','%'.$request->filtro.'%');   
        })->orderBy('id','DESC')->paginate(8);

        return view('/configuracionGenerales/tasaCambio')->with('menus',$menu->retornarMenu())->with('tasas',$tasas);
    }
}
