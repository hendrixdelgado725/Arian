<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Models\Menu;
use App\Almacen;
use App\Caentalm;
use App\Caregart;
use App\Caartalmubimov;
use App\Caartalmubi;
use App\Catraalm;
use App\Tallas;
use App\Sucursal;
use App\NotaEntrega;
use App\NotaEntregaArt;
use App\Famoneda;
use App\Fatasacamb;



use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Helper;
use Session;
use Exception;
use Illuminate\Validation\Rule;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use App\Mail\SolicitudTraspasoMail;
use \Fpdf;
use App\Http\Controllers\generadorPDF;
use Illuminate\Support\Facades\Route;
use App\Segususuc;

class TraspasoController extends Controller
{
    protected $temp = [];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index()
    {
        
        return view('almacen.listadoTraspasos')->with(['menus'=> $this->getmenu(),
        'traspasos' => Catraalm::getFull()]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getcombo(){
        $tmovi = \App\Catipent::all();
        $alm = \App\Almacen::whereNotNull('codsuc_asoc')->whereHas('ubicaciones')->whereNull('deleted_at')->get();
        $alm = $alm->groupBy([
          function($q){
            return $q['sucursal']['nomsucu'];
          }
        ]);
        if(count($alm) === 0){
          Session::flash('warning','Primero debe ingresar un almacen para poder hacer movimientos de inventario');
        }
        $menu = $this->getmenu();
        $control=Catraalm::Orderby('codigoid','Desc')->whereNull('deleted_at')->first();
        $sucuse_reg=Auth::user()->getsucUse()->codsuc;
        $sep= explode('-', $sucuse_reg);
          if($control==Null)
            {$codigo='TRA'.'-1-'.$sep[1];}
          else
            {
            $ent = Catraalm::get()->count();
            $codigo='TRA-'.($ent+1).'-'.$sep[1];
            }
        return ['tmovi' => $tmovi , 'alm' => $alm,'menus'=> $menu,'reftra' => $codigo];
    }

    public function create(){
        return view('almacen.traspasoCreate')->with($this->getcombo())->with('sec',Segususuc::where('codusuario',Auth::user()->codigoid)->where('activo','TRUE')->first()->getSucursal->nomsucu);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
      // dd($request);
        $request->validate([
            "almori" => "required",
            "almdes" => "required",
            "obstra" => "required",
        ]);
        $art = array_filter($request->articulos);
        $cant = array_filter($request->cantidad);
        $ubiori = array_filter($request->ubicacionori);
        $ubides = array_filter($request->ubicaciondes);
        $tallasx = array_filter($request->tallasArtx);
        $sucuse_reg=Auth::user()->getsucUse()->codsuc;
        $sep= explode('-', $sucuse_reg);
        $cea=Catraalm::get()->count();
        $codigoid='TRA-'.($cea+1).'-'.$sep[1];
				$codigoidMov = Caartalmubimov::getCount()+1;
        $codSuc = Auth::user()->getsucUse()->codsuc;

          DB::beginTransaction();

        try{
            $codtra = 'TRA'.'-'.$this->createcodmov();
            foreach ($art as $key => $value) {
              $tallas = $tallasx[$key] !== 'N/D' ? explode(',',$tallasx[$key]) : null;
              if($tallas !== null){
                foreach ($tallas as $talla){
                  $tosave[] = [
                    'codmov' =>$codigoid,//$codtra,
                    'codart' => $value,
                    'cantmov' => $cant[$key],
                    'codubifrom' => $ubiori[$key],
                    'codubito' =>$ubides[$key],
                    'codalmfrom' =>$request->almori,
                    'codalmto' =>$request->almdes,
                    'tipmov' =>$request->tipmov,
                    'created_at' => Carbon::now(),
                    'codtalla' => $talla,
                    'desmov' => 'TRA',
                    'codsuc' => $codSuc,
                    'codigoid' => $codigoidMov.'-'.$sep[1]
                  ];
                }
              }else {
                $tosave[] = [
                  'codmov' =>$codigoid,//$codtra,
                    'codart' => $value,
                    'cantmov' => $cant[$key],
                    'codubifrom' => $ubiori[$key],
                    'codubito' =>$ubides[$key],
                    'codalmfrom' =>$request->almori,
                    'codalmto' =>$request->almdes,
                    'tipmov' =>$request->tipmov,
                    'created_at' => Carbon::now(),
                    'desmov' => 'TRA',
                    'codtalla' => '',
                    'codsuc' => $codSuc,
                    'codigoid' => $codigoidMov.'-'.$sep[1]
                ];
              }
            }
            Caartalmubimov::insert($tosave);
                $trasp = new \App\Catraalm;
                $trasp->codtra = $codigoid;
                $trasp->fectra = Carbon::now();
                //$trasp->destra = $request->destra;
                $trasp->almori = $request->almori;
                //$trasp->codubiori = ;
                $trasp->almdes = $request->almdes;
                //$trasp->codubides = ;
                $trasp->statra = 'E';
                $trasp->obstra = $request->obstra;
                $trasp->codemptra = Auth::user()->loguse;
                $trasp->codsuc = Auth::user()->getsucUse()->codsuc;
                $trasp->codigoid = $codigoid;
                //$trasp->fadefveh_id = ;
                //$trasp->fadefcho_id = ;
                $trasp->save();
                 
                ////  ******NOTA DE ENTREGA  ****//////
                $notas = NotaEntrega::where('codsuc',$codSuc)->get()->count();
                $notas = $notas+1;
                $codsuc = explode('-',$codSuc);
                $created_by = Auth::user()->codigoid;
 
                $numero = 'N'.$codsuc[1].'-'.$notas;
                $codigoid = $notas.'-'.$codsuc[1];

                $nota = new NotaEntrega();
                $nota->numero = $numero;
                $nota->almori = $request->almori;
                $nota->almdes = $request->almdes;
                $nota->observacion = $request->obstra;
                $nota->created_at = Carbon::now();
                $nota->created_by = $created_by;
                /*$nota->total = $total;*/
                $nota->codsuc = $codSuc;
                $nota->codigoid = $codigoid;
                $nota->facturado = false;
                $nota->save();

                /***Aticulos Nota ***/
                $total = 0;
                $totalCambio = 0;
                $tasaId = ' ';

                foreach ($art as $key => $value) {
                 $notasA = NotaEntregaArt::where('codsuc',$codSuc)->get()->count();
                 $count = $notasA +1;
                 $codigoid2 = $count.'-'.$codsuc[1];

                 $articulo = Caregart::where('codart','=',$value)->with('costos')->first();
                 $precio = $articulo->costoUnit->pvpart;
                 $costo = $precio*$cant[$key];
                 $desart = $articulo->desart;


                 $coin = $articulo->costoUnit->codmone;
                 $moneda = Famoneda::where('codigoid',$coin)->first();
        
                  if($moneda->nombre != 'BOLIVAR SOBERANO' || $moneda->nombre != 'BOLIVAR'){
                    $bolivar = Famoneda::where('activo',true)->first();//cambio para la moneda activa
                    
                    $tasa = Fatasacamb::with('moneda','moneda2')->where('id_moneda',$moneda->codigoid)->orWhere('id_moneda2',$bolivar->codigoid)->where('activo',true)->first();
              
                    $tasaId = $tasa->codigoid;
                    $cambio = $tasa->valor;
                    $total = $total + ($costo * $cambio);
                    $totalCambio = $total / $cambio;

                   }
                 else{
                   $bolivar = Famoneda::where('activo',true)->first();//cambio para la moneda activa
                   $tasa = Fatasacamb::with('moneda','moneda2')->where('id_moneda2',$bolivar->codigoid)->where('activo',true)->orderBy('id','DESC')->first();
                   $cambio = $tasa->valor;
                 /*  $tasaId = ' ';*/
                   $total = $total + $precio;
                   $pd = $precio/$cambio;
                   $totalCambio = $totalCambio + $pd;
                  }

                   $coinName = Famoneda::where('codigoid',$coin)->first()->nombre;


              /*   $total = $total + $costo;*/


                 $tallas = $tallasx[$key] !== 'N/D' ? explode(',',$tallasx[$key]) : null;

                  if($tallas !== null){
                     $tosave2[] = [
                      'numero_nota' =>$numero,//$codtra,
                      'codart' => $value,
                      'desart' => $desart,
                      'preunit' => $precio,
                      'costo'=> $costo,
                      'cantidad' => $cant[$key],
                      'created_at' => Carbon::now(),
                      'codtalla' => $tallasx[$key],
                      'codsuc' => $codSuc,
                      'codigoid' => $codigoid2,
                      'moneda' => $coin,
                      'coin_name'=> $coinName,
                     ];
                  }else {
                    $tosave2[] = [
                      'numero_nota' =>$numero,//$codtra,
                      'codart' => $value,
                      'desart' => $desart,
                      'preunit' => $precio,
                      'costo'=> $costo,
                      'cantidad' => $cant[$key],
                      'created_at' => Carbon::now(),
                      'codsuc' => $codSuc,
                      'codigoid' => $codigoid2,
                      'codtalla' => '',
                      'moneda' => $coin,
                      'coin_name'=> $coinName,
                     ];
                   }
                  }

                  NotaEntregaArt::insert($tosave2);
                  $NE = NotaEntrega::where('numero',$numero)->first();
                  $NE->total = $total;
                  $NE->fatasacamb = $tasaId;
                  $NE->cambio = $totalCambio;
                  $NE->save();

               DB::commit();
                /* 
                  Buscar jefe de almacen o supervisor de almacen origen para enviar el correo
                */
                // Mail::to('hendrixdelgado725@gmail.com')->send(new SolicitudTraspasoMail());

              //  Mail::to(['miguelodav93@gmail.com','mpaez@vit.gob.ve','soportesiga@vit.gob.ve'])->send(new SolicitudTraspasoMail());
                Session::flash('success','Traspaso Registrada Exitosamente');
            }catch(Exception $e){
      
              DB::rollback();
              return redirect()->route('traspasosAlmacenCreate')->with(['error' => $e->getMessage()]);
            }
              try{
                $correo = $this->sendMail($codigoid);
              }catch(\Exception $e){

              }

              return redirect()->route('traspasosAlmacen')->with($this->getcombo());

    }

    public function recepcionTras(Request $request,$codmov){
      $array = [];
      /* 
        codalmfrom => almacenorigen 
        codalmto => almacendestino
        codubifrom => ubicacion del almacen origen
        codubito => ubicacion del almacen destino
      */
      
      $objeto = Caartalmubimov::findByMov($codmov);
        try{
              $movimiento = Catraalm::findByCodDetalle($codmov);
              if($movimiento->statra !== 'E') throw new Exception("Traspaso ya no esta en espera");
              
              foreach ($objeto as $articulo) {//validando que no exista un articulo con una existencia menor a la que debe ser traspasada
                
                $cantOrigen = Caartalmubi::findQty($articulo->codart,$articulo->codalmfrom,$articulo->codubifrom,$articulo->codtalla);
                //Buscamos la cantidad que hay en esa ubicacion para así obtener la cantidad exacta
                if(!$cantOrigen) throw new Exception("Error al cargar articulos desde almacen origen");
                if(floatval($cantOrigen->exiact) < floatval($articulo->cantmov)) throw new Exception("Articulo : {$articulo->articulo->desart} tiene una existencia : {$cantOrigen->exiact}
                y la cantidad a mover es de: {$articulo->cantmov}
                ");
                }
                foreach($objeto as $articulo){
                  $cantOrigen = Caartalmubi::findQty($articulo->codart,$articulo->codalmfrom,$articulo->codubifrom,$articulo->codtalla);
                  if(!$cantOrigen) throw new Exception("Error al cargar articulos desde almacen origen");
                  $suma = floatval($cantOrigen->exiact)-floatval($articulo->cantmov);//resta de la existencia
                  $cantOrigen->exiact = $suma;
                  $cantOrigen->save();//Resta de el almacen origen
                  $cant = Caartalmubi::findQty($articulo->codart,$articulo->codalmto,$articulo->codubito,$articulo->codtalla);//suma o crea dependiendo si existe o no el articulo en ese almacen
                  $suma = !isset($cant) ? floatval($articulo->cantmov) :  floatval($cant->exiact)+floatval($articulo->cantmov);
                  if($suma == 0){$beta = 'betica';}
                  $movart = Caartalmubi::updateOrCreate(
                    [
                      'codalm' => $articulo->codalmto,
                      'codart' => $articulo->codart,
                      'codubi' => $articulo->codubito,
                      'codtalla' => $articulo->codtalla
                    ],
                    [
                      'exiact' => $suma,
                      'created_at' => Carbon::now(),
                      'codsuc' => Auth::user()->getsucUse()->codsuc,
                    ]
                  );
                }
				$movimiento = Catraalm::findByCod($objeto[0]->codmov);//retorna objeto	
				$movimiento->apro_by_user = Auth::user()->loguse;
				$movimiento->fecapro = Carbon::now();
				$movimiento->statra = 'A';
				$movimiento->save();
				return response()->json(['exito' => true]);

			}catch(Exception $e){
				return response()->json(['error' => $e->getMessage()]);
			}
    }

    public function anularTras(Request $request,$codmov,$desanu){
      $trasp = Catraalm::findByCod($codmov);
      try{
        if($trasp->statra !== 'E') throw new Exception("Traspaso Ya se encuentra actualizado");
        $trasp->statra = 'R';
        $trasp->fecanu = Carbon::now();
        $trasp->usuanu = Auth::user()->loguse;
        $trasp->desanu = $desanu;
        $trasp->save();
        return response()->json(["exito" => true]);
      }catch(Exception $e){
        return response()->json(['error' => $e->getMessage()]);
      }
    }

    public function getDiffUbi($array,$art,$artalmubi){
      return $array = [
          'codart' => $art->codart,
          'codubiexist' => $artalmubi->codubi,
          'codubimov' => $art->codubito];
    }

    public function sendMail($codmov){
      /* 
        Buscando el destinatario
      */
      //$employee = Nphojint::getEmp($cedemp);
      
      try{
        $arts =  Caartalmubimov::getArtToEmail($codmov);
        $mov = Catraalm::findByCod($codmov);
        Mail::to(['miguelodav93@gmail.com'])->send(new SolicitudTraspasoMail($mov,$arts));
      }catch(Exception $e){
        return response()->json(["error" => "error {$e->getMessage()}"]);
      }
      return response()->json(["exito" => true,'correo' => 'miguelodav93@gmail.com']);

    }

    public function ajax()
	  {
      $data = request();
      $sucuse_reg=Auth::user()->getsucUse()->codsuc;
      $sep=explode('-', $sucuse_reg);
      
      switch ($data['ajax']) {
        case 1://selecionar el almacen origen llena todos los selects de los articulos
            $art = new Caregart;
            $articulos = $art->existAlm($data["almori"]);
            return response()->json(["options" => $this->makeHtmlArts($articulos)]);
        break;
        case 2://obtener los articulos para hacer traspaso
          if($data["tiptras"] === 'INT'){//TIPO DE MOVIMIENTO INTERNO
            $alm = Almacen::where("codalm",$data["almori"])->whereNotNull('codsuc_asoc')->first();
            $almdes = Almacen::where('codsuc_asoc',$alm->codsuc_asoc)->whereHas('ubicaciones')->get();
            $almdes = $almdes->groupBy([
              function($q){
                return $q['sucursal']['nomsucu'];
              }
            ]);
            return response()->json(["almacenes" => $this->makeHtmlAlm($almdes)]);

          }elseif($data["tiptras"] === 'EXT'){//TIPO DE MOVIMIENTO EXTERNO
            // $alm = Almacen::where("codalm",$data["almori"])->whereNotNull('codsuc_asoc')->first();
            $almdes = Almacen::where('codalm','<>',$data["almori"])
            ->where('deleted_at',null)->whereHas('ubicaciones')
            ->whereNotNull('codsuc_asoc')
            //->where('codsuc_asoc','<>',$alm->codsuc_asoc)
            ->get();
            $almdes = $almdes->groupBy([
              function($q){
                return $q['sucursal']['nomsucu'];
              }
            ]);
            return response()->json(["almacenes" => $this->makeHtmlAlm($almdes)]);
          }

        break;

        case 3:///OBTIENE LAS TALLAS
          try {
            if($data["codart"] == '')return;
            $articulo=Caregart::where('codart',$data['codart'])->with('facategoria')->whereHas('facategoria')->first();

            $reqTalla =  $articulo->facategoria->talla === true ? true : null;

            $Cartalmubi = new Caartalmubi;
            $ubicaciones = $Cartalmubi->getUbiArtAlm($articulo->codart,$data["almori"]);/* 
            obtiene los articulos con ubicaciones y almacen*/
            if($ubicaciones->isEmpty()) throw new Exception(" Articulo no existe en almacen limpie la linea");
            
            $ubiori = $this->makeHtml($ubicaciones,true);//ubicacion origen del articulo si existe
            $ubicdes = $Cartalmubi->getUbiArtAlm($articulo->codart,$data["almdes"]);

            if($ubicdes->isEmpty()){// si no existe el articulo en el almacen destino Buscar todas las ubicaciones
              $sugerida = Catraalm::where('almdes',$data["almdes"])->where('statra','E')->with('artmov')
              ->whereHas('artmov', function ($q) use($articulo){
                $q->where('codart',$articulo->codart);
              })->get();
              //dd($sugerida);
            }
            $ubides = !$ubicdes->isEmpty() ? $this->makeHtml($ubicdes,true) : $this->makeHtml(Almacen::getubix($data["almdes"]));//ubicacion destino si existe en el almacen destino
            $arts = Caartalmubi::where('codalm','=',$data["almori"])->where('codart','=',$articulo->codart)->where('exiact','>',0)->get();
            $tallas = [];
            
            if($reqTalla){
              foreach($arts as $art){
                $talla = Tallas::where('codtallas','=',$art->codtalla)->first();
                $talla->exiact = $art->exiact;
                array_push($tallas, $talla);
              }
              $options = $this->makeHtmlTallas($tallas,true);
            }else{
              $options = $this->makeHtmlNoTallas($arts);
            }
            
            if(count($tallas) === 0 && $reqTalla) $msgError = "Este articulo : {$articulo->desart} no tiene tallas definidas verifique Categoría";
            
            return response()->json([
              'status' => isset($msgError) ? 'error' : 'success',
              'options'=> $options,
              'ubiori' => $ubiori,
              'ubides' =>$ubides,
              'sugerida' => $sugerida ?? '']);
          } catch (Throwable $th) {
            return response()->json(["error" => $th->getMessage()]);
          }
          break;
        
        case 4:
          $trasp = Catraalm::findByCod($codmov);
          try{
            if($trasp->statra !== 'E') throw new Exception("Traspaso Ya se encuentra actualizado");
            $trasp->statra = 'R';
            $trasp->fecanu = Carbon::now();
            $trasp->usuanu = Auth::user()->loguse;
            $trasp->desanu = $desanu;
            $trasp->save();
            return response()->json(["exito" => true]);
          }catch(Exception $e){
            return response()->json(['error' => $e->getMessage()]);
          }
        break;

      }

    }

    public function makeHtmlNoTallas($collection){
      return count($collection) === 0 ? "<option value=N/D selected >Sin talla Existencia : 0</option>" : "<option value=N/D selected >Sin talla Existencia : {$collection[0]->exiact}</option>";
      
    }

    public function makeHtmlAlm($collection){
      $string = '<option value=""> Seleccione Almacen</option>';
        foreach($collection as $index => $value){
          $string.='<optgroup label="Sucursal '.$index.'">';
            foreach($value as $item){
              $factur = $item->pfactur === true ? ' -- Facturacion' : '';
              $string.= "<option value={$item->codalm}>{$item->nomalm}{$factur}</option>";
            }
          $string.="</optgroup>";
        }
      return $string === '<option value=""> Seleccione Almacen</option>' ? '0' : $string;
    }
    
    public function makeHtmlArts($collection){
      //dd()
      //if($collection === '0') return $collection;
      $string = '<option value=""> Seleccione Artículo </option>';
  
      foreach ($collection as $value) {
        $string.="<option value={$value->codart}>{$value->codart} {$value->desart}</option>";
      }
      return $string === '<option value=""> Seleccione Artículo </option>' ? '0' : $string;
    }

    public function makeHtml($collection,$existe = false){
      //var_dump($collection);
      $string= '';
      $string.='<option value="" disabled> Seleccione Ubicacion </option>';
      if(!$existe){//no existe en el almacen
        foreach ($collection as $value) {
          foreach ($value->ubinamesx as $ubicacion){
            $string.="<option value={$ubicacion->codubi}>{$ubicacion->nomubi}</option>";
          }
        }
      }elseif($existe){//existe en el almacen
        foreach ($collection as $value) {
					$string.="<option value={$value->ubicaciones->codubi} data-exiact={$value->exiact} data-talla={$value->codtalla}>{$value->ubicaciones->nomubi}</option>";
			}
      }else{
        return '0';
      }
      return $string;// === '' ? '<option>Seleccione Ubicacion</option>';
    }
  
    /* CREA EL HTML DE LAS TALLAS DE LOS ARTICULOS */
    public function makeHtmlTallas($collection,$exist = null){
      $string= '';
      $string.='<option disabled> Seleccione la talla</option>';
      $cantidad ='';
      foreach ($collection as $value) {
        $cantidad = $exist ? "Existencia : ".$value->exiact : '';
        $string.="<option value={$value->codtallas} data-exiact={$value->exiact}>{$value->tallas}   {$cantidad}</option>";
      }

      return $string;
  
    }



    public function makeUbiDiff($array){
      $string = '';
      $string2 = '';
      //dd($array);
      $string.='
                <b>Existe un articulo/s que ya existe en una ubicacion y en este Movimiento tiene una diferente</b>
                <table class="table">
                <thead>
                <th>Articulo</th>
                <th>Ubicacion Actual</th>
                <th>Ubicacion a Mover</th>
                </thead>
                <tbody>
                ?
                </tbody>
                </table>';
      foreach($array as $key => $value){
        $string2.="<tr>
        <td>{$value['codart']}</td>
        <td>{$value['codubiexist']}</td>
        <td>{$value['codubimov']}</td>
        </tr>";
      }
      $string = \str_replace('?',$string2,$string);
      //dd($string);
      return $string;
    }

    public function getDetalle(Request $request,$codmov){
      $detalle = Catraalm::findByCodDetalle($codmov);
      $articulos = Caartalmubimov::findByMovDetail($codmov,'TRA');
      return !$articulos->isEmpty() ? response()->json(['articulos' => $this->makelist($articulos),'detalle' => $this->getHtmlDetail($detalle)]) : response()->json(false);
    }

    public function getmenu(){
        return $this->menu = Menu::getmenu();
    }
    /* 
    <th>Articulo</th>
                    <th>Descripcion</th>
                    <th>Almacen Origen</th>
                    <th>Almacen Destino</th>
                    <th>Cantidad Movilizada</th>
                     */
    public function getHtmlDetail($detalle){
      switch ($detalle->statra) {
        case 'A':
          $fecha = Carbon::parse($detalle->fecapro)->format('l j n F Y');
          $hora = Carbon::parse($detalle->fecapro)->format('h:i:s A');
					$fecha = Helper::getSpanishDate($fecha);
          $html = '<div class="alert alert-success ">
          <h5><i class="icon fas fa-check"></i>Aprobado</h5>
          <strong>'."Traspaso Aprobado ".'</strong><br>
          <strong>'."Fecha Aprobacion: ".$fecha.' a las '.$hora.'</strong>
          </div>';
          break;
        case 'E':
          $html= '<div class="alert alert-info ">
          <h5><i class="icon fas fa-info"></i> En Espera</h5>
          <strong>'."Traspaso En Espera de Aprobacion ".$detalle->fecapro.'</strong>
          </div>';
          break;
        case 'R':
          $fecha = Carbon::parse($detalle->fecanu)->format('l j n F Y');
          $hora = Carbon::parse($detalle->fecanu)->format('h:i:s A');
					$fecha = Helper::getSpanishDate($fecha);
          $html = '<div class="alert alert-danger">
          <h5><i class="icon fas fa-ban"></i> Anulado</h5>
          <strong>'."Traspaso Rechazado".'</strong>
          <strong>'."Observacion : ".$detalle->desanu.'</strong><br>
          <strong>'."Fecha Anulacion: ".$fecha.' a las '.$hora.'</strong>
          </div>';
          break;
      }
      return $html;
    }
    
    public function makeList($articulos){
        //$articulos = $this->repetidosBest($articulos);
            $string = '';
        foreach($articulos as $key => $value){
          $tallas = isset($value->talla) ? $value->talla->tallas : 'N/D';
          $color = isset($value->color) ?  $value->color->color."-".$value->color->nomen :'NO APLICA';
          $string .= "<tr><td>{$value->codart}</td>
                      <td>{$value->articulo->desart}</td>
                      <td>{$color}</td>
                      <td>{$value->almacenfrom->nomalm}</td>
                      <td>{$value->almacento->nomalm}</td>
                      <td>{$tallas}</td>
                      <td>{$value->cantmov}</td></tr>
                      ";
        }
        return $string !== '' ? $string : null;
    }

    public function createcodmov(){
        $date =  explode('-',Carbon::now()->toDateString());
        $date = $date[2].$date[1].$date[0];
        $num =  $this->GetFormatNum(Catraalm::GetLastCode());
        return $date.'-'.$num;
    }

    public function GetFormatNum($modelo){
        $numero= ($modelo ? str_pad($modelo+1, 4, '0', STR_PAD_LEFT):'0001');
        return $numero;
}

    public function vistaReporte(){

        $menu = new HomeController();
        $almacenes = Almacen::get();
        $traspasos = Catraalm::where('statra','<>','R')->orderBy('codtra','asc')->get();
        $traspasos->isEmpty() ? Session::flash('warning','No existen traspasos que mostrar') : null;
        return view('/reportes/traspasoReportes')
        ->with('menus',$menu->retornarMenu())->with('almacenes',$almacenes)->with('traspasos',$traspasos);
    }

    public function generarReporte(Request $request){
         $desde = $request['desde'] ?? null;
         $hasta = $request['hasta'] ?? null;
         $diames = $request['diames'];
         $codfrom = $request->codfrom ?? null;
         $codto = $request->codto ?? null;
         switch ($diames) {
            case 'diario':
              $tipo = 'DIARIO';
            break;
            case 'ayer':
                $tipo = 'DÍA ANTERIOR';
            break;
            case 'semanal':
               $tipo = "SEMANAL";
            break;
            case 'semant':
               $tipo = "SEMANA ANTERIOR";
            break;
            case 'mensual':
                $tipo = 'MENSUAL';
            break;
            case 'mesant':
              $tipo = 'MES ANTERIOR';
            break;
            case 'anual':
              $tipo = 'ANUAL';
            break;
            case 'antyear':
              $tipo = 'AÑO ANTERIOR';
            break;
            case 'byCod':
              $tipo = $request->codfrom;
            break;
            default:
              $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
            break;
        }
      ///EVALUA LOS PERIODOS DE TIEMPO SELECCIONADOS Y SI SERA UNA CONSULTA GENERALIZADA O POR SUCURSAL
          $traspasos = Catraalm::with('almorides','almdesdes','tallas','artmov.color')->date($desde,$hasta)->almacen($codalm ?? null)->codigo($codfrom,$codto)->get();

          if($traspasos->isEmpty()){
                Session::flash('error', 'No existen registros con los parametros de busqueda ingresados');
                return redirect()->route('reportesTraspasos');
              }

            $pdf = new generadorPDF();

            $titulo = 'REPORTE DE TRASPASOS';
            $total = $traspasos->count();
            $fecha = date('Y-m-d');
    
            $pdf->AddPage('L');
            $pdf->SetTitle('REPORTE DE TRASPASOS ALMACEN');
            $pdf->SetFont('arial','B',16);
            $pdf->Ln();
            $pdf->Cell(0,22,utf8_decode($titulo),0,0,'C');
            $pdf->Ln();
            $pdf->SetFont('arial','B',10);
            if($tipo == 'Intervalo'){
                $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo.' '.$desde.' - '.$hasta),0,0,'L');
            }
            else{
                $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo),0,0,'L');
            }
            $pdf->Ln(5);
            $pdf->Cell(0,0,utf8_decode('FECHA: '.$fecha),0,0,'L');
            $pdf->Ln(5);
            $pdf->Cell(0,0,utf8_decode('TOTAL: '.$total),0,0,'L');
            $pdf->Ln(10);
            $pdf->SetFont('arial','B',7,5);
            $pdf->SetFillColor(2,157,116);
            $pdf->Cell(25,8,utf8_decode('COD.TRASPASO'),1,0,'C');
            $pdf->Cell(15,8,utf8_decode('FECHA'),1,0,'C');
            $pdf->Cell(25,8,utf8_decode('ALM. ORIGEN'),1,0,'C');
            $pdf->Cell(25,8,utf8_decode('ALM. DESTINO'),1,0,'C');
            $pdf->Cell(30,8,utf8_decode('OBSERVACION'),1,0,'C');
            $pdf->Cell(15,8,utf8_decode('ESTATUS'),1,0,'C');
            $pdf->Cell(25,8,utf8_decode('COD.EMPLEADO'),1,0,'C');
            $pdf->Cell(20,8,utf8_decode('FECHA APR.'),1,0,'C');
            $pdf->Cell(25,8,utf8_decode('APROB. POR'),1,0,'C');
            $pdf->Cell(20,8,utf8_decode('FECHA ANUL'),1,0,'C');
            $pdf->Cell(25,8,utf8_decode('ANUL. POR'),1,0,'C');
            $pdf->Cell(30,8,utf8_decode('MOTIVO ANUL.'),1,0,'C');
            $pdf->Ln();
            $pdf->SetWidths(array(25,15,25,25,30,15,25,20,25,20,25,30));
            $pdf->SetAligns(['C','C','C','C','C','C','C','C','C','C','C','C']);
            $pdf->SetFont('arial','',7,5);

            $last=1;
            $limite=count($traspasos);
            foreach($traspasos as $traspaso){      

            $pdf->SetWidths(array(25,15,25,25,30,15,25,20,25,20,25,30));
            $pdf->SetAligns(['C','C','C','C','C','C','C','C','C','C','C','C']);
            $pdf->SetFont('arial','',7,5);

                $pdf->Row(array(
                  $traspaso->codtra,
                  $traspaso->fectra,
                  $traspaso->almorides->nomalm ?? '',
                  $traspaso->almdesdes->nomalm ?? '',
                  $traspaso->obstra,
                  $traspaso->statra,
                  $traspaso->codemptra,
                  $traspaso->fecapro ?? '-',
                  $traspaso->apro_by_user ?? '-',
                  $traspaso->fecanu ?? '-',
                  $traspaso->usuanu ?? '-',
                  $traspaso->desanu ?? '-'
                ));

                $pdf->SetFont('arial','B',9);
                // $pdf->SetFillColor(2,157,116);
                $pdf->Cell(280,6,'ARTICULOS '.$traspaso->codtra,1,1,'C');
                $limit = (int)280 / 5;
                $pdf->Cell($limit,6,utf8_decode('COD.ARTICULO'),1,0,'C');
                $pdf->Cell($limit,6,utf8_decode('DESCRIPCION'),1,0,'C');
                $pdf->Cell($limit,6,utf8_decode('COLOR'),1,0,'C');
                $pdf->Cell($limit,6,utf8_decode('TALLA'),1,0,'C');
                $pdf->Cell($limit,6,utf8_decode('CANTIDAD'),1,1,'C');
                
                //$pdf->SetWidths(array());
                $pdf->SetWidths(array($limit,$limit,$limit,$limit,$limit));
                $pdf->SetAligns(['C','C','C','C','C']);
                $pdf->SetFont('arial','',9);
                foreach($traspaso->artmov as $key => $articulo){
                  $pdf->Row(
                    array(
                      $articulo->codart,
                      $traspaso->articulos[$key]->desart,
                      $articulo->color->color ?? 'N/A',
                      $traspaso->tallas[$key]->tallas ?? 'N/A',
                      $articulo->cantmov,
                    ));
                  }
                  $pdf->Ln();
                  if($last !== $limite){
                    if($pdf->getY()>= 175 && $pdf->getY()<= 180) $pdf->AddPage('L');
                    $pdf->SetFont('arial','B',7.5);
                    $pdf->Cell(25,8,utf8_decode('COD.TRASPASO'),1,0,'C');
                    $pdf->Cell(15,8,utf8_decode('FECHA'),1,0,'C');
                    $pdf->Cell(25,8,utf8_decode('ALM. ORIGEN'),1,0,'C');
                    $pdf->Cell(25,8,utf8_decode('ALM. DESTINO'),1,0,'C');
                    $pdf->Cell(30,8,utf8_decode('OBSERVACION'),1,0,'C');
                    $pdf->Cell(15,8,utf8_decode('ESTATUS'),1,0,'C');
                    $pdf->Cell(25,8,utf8_decode('COD.EMPLEADO'),1,0,'C');
                    $pdf->Cell(20,8,utf8_decode('FECHA APR.'),1,0,'C');
                    $pdf->Cell(25,8,utf8_decode('APROB. POR'),1,0,'C');
                    $pdf->Cell(20,8,utf8_decode('FECHA ANUL'),1,0,'C');
                    $pdf->Cell(25,8,utf8_decode('ANUL. POR'),1,0,'C');
                    $pdf->Cell(30,8,utf8_decode('MOTIVO ANUL.'),1,0,'C');
                    $pdf->Ln();
                  }
                  $last++;


            }
            $pdf->SetTextColor(0,0,0);
            $pdf->Ln(5);
            $pdf->Output('I','TRASPASOS-'.$fecha.'pdf');
            
           exit;
    }
    
    public function getReporteMail($id)
    {
      $id = Helper::decrypt($id);
      $traspaso = Catraalm::findOrFail($id);
      $request = new Request(
        [
          'codtra' => $traspaso->codtra,
          'diames' => 'byCod',
          'almacen' => 'TODAS',
          'desde' => $traspaso->created_at->format('d-m-Y'),
          'hasta' => $traspaso->created_at->format('d-m-Y')
        ]);
      $this->generarReporte($request);
    }
}
