<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EmpleadoCargoRequest;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\Npcargos;
use Helper;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Contracts\Auth\Guard;
use App\permission_user;
use App\Segususuc;

class EmpleadoCargoController extends Controller
{

    private $guard;
    public function __construct(Guard $guard)
    {
        $this->middleware('auth');
        $this->guard = $guard;
    }

    public function listaCempleado(){


        $menu = new HomeController();
        $cargos = Npcargos::orderBy('id','DESC')->paginate(8);
        return view('/configuracionGenerales/cargosEmpleado')->with('menus',$menu->retornarMenu())->with('cargos',$cargos);

    }

    public function nuevoCargo(){
        $menu = new HomeController();
        return view('/configuracionGenerales/registroCargoE')->with('menus',$menu->retornarMenu());
    }

    public function create(EmpleadoCargoRequest $request){

        $precod='';
        // $ultimoid = Npcargos::orderBy('id','DESC')->first()->id;
        $ultimoid = Npcargos::orderBydesc('codcar')->first();
        $codcar= ($ultimoid ? str_pad($ultimoid->codcar+1, 3, '0', STR_PAD_LEFT) : '001');
        // $codcar = $ultimoid +1;

        // if($ultimoid < 10){
        //     $precod='00';
        // }
        // elseif($ultimoid > 10 && $ultimoid < 100){
        //     $precod='0';
        // }

        // $codcar = $precod.$codcar;

        $suecar = 0.00;
        $stacar = 'E';

        $guion ='-';
        $cont = Npcargos::withTrashed()->get()->count();
        $cont=$cont+1;
        // $suc = Auth::user()->codsuc_reg;
        $suc=Auth::user()->getCodigoActiveS();
        $codsuc_reg = explode('-',$suc);
        $cod = $cont.$guion.$codsuc_reg[1];


        Npcargos::create([
            'nomcar'=>strtoupper($request['nombre']),
            'codcar'=>$codcar,
            'suecar'=>$suecar,
            'stacar'=>$stacar,
            'codigoid'=>$cod,
            'codsuc'=>$suc,
        ]);

        Session::flash('success', 'El cargo '.$request['nombre'].', se registro Exitosamente');
        return redirect('/modulo/ConfiguracionGenerales/CdeEmpleado');
    }

    public function edit($id){
        $menu = new HomeController();
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $cargo = Npcargos::find($ID);

        return view('/configuracionGenerales/editarCargoE')->with('menus',$menu->retornarMenu())->with('cargo',$cargo);
        
    }

    public function update (EmpleadoCargoRequest $request, $id){

        $cargo = Npcargos::find($id);
        $cargo->nomcar = $request->nombre;
        $cargo->save();

        Session::flash('success', 'El cargo '.$request['nombre'].', se actualizó Exitosamente');
        return redirect('/modulo/ConfiguracionGenerales/CdeEmpleado');
    }

    public function delete($id){
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $cargo = Npcargos::find($ID);
        $registro = Npcargos::with('empleadosCargos')->where('id','=',$ID)->has('empleadosCargos')->get();

        if($registro->isEmpty()){
            $cargo->delete();
            return response()->json([
                'titulo' => 'Exito se elimino'
            ]);
        }
        else{
            return response()->json([
                'titulo' => 'error Este cargo no puede ser eliminado'
                ]);
            }
    }

    public function filtro (Request $request){
        $menu = new HomeController();
        $cargos = Npcargos::where('nomcar','ilike','%'.$request->filtro.'%')->orWhere('codcar','ilike','%'.$request->filtro.'%')->paginate(8);
        return view('/configuracionGenerales/cargosEmpleado')->with('menus',$menu->retornarMenu())->with('cargos',$cargos);
    }
}
