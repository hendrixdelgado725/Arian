<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Caffeinated\Shinobi\Models\Role;
use App\Sucursal;
use Illuminate\Contracts\Auth\Guard;
use App\sessiones;
use App\Segususuc;
use App\Models\Fiscal;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $guard;
    public function __construct(Guard $guard)
    {
      
        $this->middleware('guest')->except('logout');
        $this->guard = $guard;

    }

    public function login(Request $request)
    {
       # dd("llgo");
        $usuarios = \App\User::where('pasuse','md5'.md5(strtoupper($request->texto).$request->password))->where('loguse',strtoupper($request->texto))->with(['userSegSuc' => function($q) use ($request)
        {
            $q->where('codsuc',$request->sucursal)->where('loguse',strtoupper($request->texto));
        }])->first();
        
        $SegSuc = \App\Segususuc::where('loguse',strtoupper($request->texto))
        ->where('codsuc',$request->sucursal)
        ->with('getSucursal')
        ->first();
       # dd($SegSuc);
        $anio = \App\Models\Fiscal::where('passemp',$request->anio)->first();

        if($usuarios != null && $SegSuc != null && $anio != null){
            $npperm= array('123456',$usuarios->cedemp,'1234567','12345678','123456789','qwerty','000000','password','abc123','ABC123','Abc123','987654','654321');
            Auth::login($usuarios);
            session(['codsuc' => $SegSuc->getSucursal->codsuc]);
            session(['sucu' => $SegSuc->getSucursal->nomsucu]);
            session(['anio' => $request->anio]);
            session(['fiscal' => $anio->nomemp]);
            /*$SegSuc->activo = 'TRUE';
            $SegSuc->save();*/
            return session()->has('url.intended') ? redirect()->intended('/') : redirect($this->redirectTo);
        }else{
            Session::flash('flash_message', 'Clave o Usuario Incorrecto Contacte al Administrador');
            return redirect('/');
        }

    }

    public function logout(Request $request)
    {   
       /* $sec = Segususuc::where('codusuario',Auth::user()->codigoid)->where('activo','TRUE')->where('codsuc',session('codsuc'))->first();   
        $sec->activo = 'FALSE';
        $sec->save();*/
        Auth::logout();
        session(['anio' => null]);
        session()->forget('codsuc');
        
        return redirect('/');
    }
    //se agrego en caso de terminar la session lo redirecciona
    public function showLoginForm(Request $request)
    {
    // Get URLs
        $urlPrevious = url()->previous();
        $urlBase = url()->to('/');
        // Set the previous url that we came from to redirect to after successful login but only if is internal
        if(($urlPrevious != $urlBase ) && (substr($urlPrevious, 0, strlen($urlBase)) === $urlBase)) {
            session()->put('url.intended', $urlPrevious);
        }
        // $sucursal = Sucursal::where('codalm','A')->get();
        // $anio = Fiscal::where('codemp','>','007')->get();
        // return view('auth.login')
        //         ->with(['sucursal'=>$sucursal,
        //                 'year' => $anio]);
        return redirect('/');
    }


   
}
