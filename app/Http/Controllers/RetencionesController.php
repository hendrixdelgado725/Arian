<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PrecomController;
use App\Mail\RequisicionMail; 
use App\Mail\PedidoMailable; 
use App\Mail\ReqPresupuestoMail;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Session;
use Helper;
use App\User;
use App\Almacen;
use App\Caprovee;
use App\Famoneda;
use App\Farecarg;
use App\Cpdeftit;
use App\Caregart;
use App\Nphojint;
use App\Tsdefmon;
use App\Npasicaremp;
use App\Models\Costos;
use App\Models\Casolart;
use App\Models\Caartsol;
use App\Models\Npcatpre;
use App\Models\Cpprecom;
use App\Models\Cpimpprc;
use App\Models\Segcenusu;
use App\Models\Nppartidas;
use App\Models\Retenciones;

class RetencionesController extends Controller
{   
    public function retornarMenu()
    {
        $yaml = new Parser();
        $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
        return $menu['menu'];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filtro = trim($request->get('filtro'));

        $retenciones = Retenciones::where('codtip', 'LIKE', '%'.$filtro.'%')
        ->orWhere('destip', 'iLIKE', '%'.$filtro.'%')
        ->orWhere('codcon', 'iLIKE', '%'.$filtro.'%')
        ->orderBy('id', 'DESC')->paginate(10);

        $menus = $this->retornarMenu(); 

        return view('configuracionGenerales.Compras.retenciones')->with(
            [
                'retenciones'  => $retenciones,
                'menus' => $menus,
                'filtro' => $filtro,
            ]);
    }

    public function create(){

        $menus = $this->retornarMenu(); 

        return view('configuracionGenerales.Compras.retencionesCreate')->with(
            [
                'menus' => $menus,
            ]);

    }

    public function getData() //Envío de datos a la ruta de creación de requisición
    {

        $ultimoCodigo = Retenciones::orderBy('id', 'desc')->first();

        $codtip = $ultimoCodigo->codtip + 1;
 
        return response()->json([
            'codtip' => $codtip,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //dd($request->codtip);
        $valor = $request->manejaSustraendo == "true" ? true : false; // convertimos el string en un caracter booleano

        $request->validate([
            'codtip' => 'required',
            'destip' => 'required',
            'codtipsen' => 'max:3',
        ]);

        $validacion = Retenciones::where( 'codtipsen', $request->codtipsen)->first();

        if($validacion == null){

            DB::beginTransaction();


            $retenciones = Retenciones::create([
    
                'codtip' => strval($request->codtip),
                'destip' => $request->destip,
                'mansus' => $valor,
                'codcon' => '',
                'basimp' => $request->basimp,
                'porret' => $request->porret,
                'unitri' => $request->unitri,
                'porsus' => $request->porsus,
                'factor' => $request->factor,
                'codtipsen' => $request->codtipsen ?? '',
    
            ]);
    
            DB::commit();
    
            return response()->json([
                'success' => true,
                'Información suministrada correctamente',
                'redirect' => route('retenciones.index'),
            ]);
          
        } else { 

            if($validacion->basimp == $request->basimp && 
            $validacion->unitri == $request->unitri && 
            $validacion->porsus == $request->porsus && 
            $validacion->factor == $request->factor){  
    
                return response()->json([
                    'Error' => true,
                    'errors' => 'Ya existe una retención con esos sustraendo',
                    'redirect' => route('retenciones.create'),
                ]);
    
            }

        }




      
    }

    public function edit($id)
    {
        $menu = $this->retornarMenu();
        $retenciones = Retenciones::find($id);

        return view('configuracionGenerales.Compras.retencionesEdit')->with([
            'retenciones' => $retenciones,
            'menus' => $menu,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $valor = $request->tipo == "true" ? true : false;

        $retenciones = Retenciones::find($id);
        
        $retenciones->codtipsen = $request['codtipsen'] ? $request['codtipsen'] : '' ;
        $retenciones->destip = $request['destip'] ? $request['destip'] : '' ;
        $retenciones->mansus = $valor;
        $retenciones->unitri = $request['unitri'] ? $request['unitri'] : '0.0' ;
        $retenciones->factor  = $request['factor'] ? $request['factor'] : '0.0' ;
        $retenciones->porsus   = $request['porsus'] ? $request['porsus'] : '0.0' ;
        $retenciones->basimp = $request['basimp'] ? $request['basimp'] : '0.0' ;
        $retenciones->porret = $request['porret'] ? $request['porret'] : '0.0' ;

        $retenciones->save();

        return redirect()->route('retenciones.index', $id)->with('message', 'Retencion editado exitosamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $retenciones = Retenciones::find($id);

        if($retenciones === null){
            return response()->json([
                'titulo' => 'Error eliminando'
            ]);
        }

        $retenciones->delete();

        Session::flash('success', 'Registro eliminado con exito.');

        return response()->json([
            'success' => true,
            'message' => 'Información suministrada correctamente',
            'redirect' => route('retenciones.index'),
        ]);
    }
}
