<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PrecomController;
use App\Mail\RequisicionMail; 
use App\Mail\ReqPresupuestoMail;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Session;
use Helper;
use App\User;
use App\Famoneda;
use App\Farecarg;
use App\Cpdeftit;
use App\Caregart;
use App\Nphojint;
use App\Tsdefmon;
use App\Models\Casolart;
use App\Models\Segcenusu;
use App\Models\Costos;
use App\Npasicaremp;
use App\Models\Caartsol;
use App\Models\Npcatpre;
use App\Models\Nppartidas;
use App\Models\Cpprecom;
use App\Models\Cpimpprc;

class RequisicionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function retornarMenu()
    {
        $yaml = new Parser();
        $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
        return $menu['menu'];
    }

    public function index()
    {
        //
        $cods = json_decode(Auth::user()->segcen, true);

        $reqs = Casolart::orderBy('id','DESC')->paginate(8);

        #where('stareq', 'N')->

        $menu = $this->retornarMenu();
  

        return view('compras.requisicion.requisiciones')->with([
            'menus' => $menu,
            'reqs' => $reqs,
    ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $menu = $this->retornarMenu();
        

        return view('compras.requisicion.requisicionesCreate')->with([
            'menus' => $menu,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d');

        try {
           DB::beginTransaction();

            $requisicion = Casolart::create([
                'reqart' => $request->reqart,
                'fecreq' => $date,
                'desreq' => $request->desreq,
                'monreq' => $request->monreq,
                'stareq' => 'U', //Pendiente aprobación gerente de área
                'mondes' => 0.00,
                'obsreq' => $request->obsreq ?? '', //Campo pendiente
                'unires' => $request->unires,
                'tipmon' => $request->tipmon,
                'valmon' => 1.000000, //Pendiente de hacer un where para obtener el valor de la moneda que se escogió
                'tipfin' => $request->tipsel,
                'codcen' => $request->codcen,
                'tipreq' => $request->tipreq,
                'aprreq' => 'N',
                'empsol' => Auth::user()->cedemp,
                'loguse' => Auth::user()->loguse,

            ]);

            if($request->articulos && count($request->articulos) > 0){
                foreach($request->articulos as $key => $value) {
                    $articulos[] = [
                        'reqart' => $request->reqart,
                        'codart' => $value["codart"],
                        'codcat' => $request->unires,
                        'canreq' => $value["canreq"],
                        'canrec' => 0.00,
                        'montot' => floatval($value["canreq"]) * floatval($value["cosult"]), //Verificar
                        'costo' => floatval($value["cosult"]),
                        'canord' => floatval($value["canreq"]),
                        'unimed' => $value["unimed"],
                        'codpar' => $value["codpar"],
                        'desart' => $value["desart"],
                    ];
                }
            }

            Caartsol::insert($articulos); 

            DB::commit();

            //Envío de correo
            #where('reqart', $request->reqart)

            $data = Casolart::where('reqart', $request->reqart)->with('ccosto', 'articulos', 'solicitante')->first();
            $correo = $request->correo;
            $this->gerMail($correo, $data); //Enviar correo gerente

            return response()->json([
                'success' => true,
                'Información suministrada correctamente'
            ]);

            
        } catch (\Throwable $th) {
            throw $th;
            DB::rollback();
            return response()->json(['error' => true,
        'Error al registrar requisición'], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Reqarticles(Request $request)
    {
        //
        $req = Caartsol::where('reqart', $request->id)->with('partida')->get();

        return response()->json($req, 200);
    }

    public function gerMail($email, $data){ //Función de envío de correo a gerente
        
	Mail::to($email)->send(new RequisicionMail($data));
    return response()->json('Correo enviado', 200);
    }

    public function newArt(Request $request)
    {

        $search = Caregart::where('codart', $request->codart)->first();

        if ($search != null) {
            return response()->json([
                'error' => true,
                'msg' => 'Error al registrar artículo'
            ]);
        }

        try {
            DB::beginTransaction();

            Caregart::create([
                'codart' => $request->codart,
                'desart' => $request->desart,
                'cosult' => $request->cosult,
                'codpar' => $request->codpar,
                'unimed' => $request->unimed,
                'tipo' => $request->tipord
            ]);

            DB::commit();

            $articulos = Caregart::where('tipreg', null)->whereNull('deleted_at')
            ->with('color', 'facategoria', 'subcategoria', 'demoProducto', 'moneda')->get();

            return response()->json([
                'success' => true,
                'msg' => 'Artículo registrado con éxito',
                'articulos' => $articulos,
            ]);
        }
        catch(Exception $e){
            DB::rollback();

            return response()->json([
                'error' => true,
                'msg' => 'Error al registrar artículo'
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($reqart)
    {
        //
        $req = Casolart::where('reqart', $reqart)->with('ccosto', 'unidad', 'fin', 'orden', 'moneda')->first();

        $articulos = Caartsol::where('reqart', $reqart)->get();

        $artpart = Caartsol::select('codpar')->where('reqart', $reqart)->first();

        $partida = Nppartidas::where('codpar', $artpart->codpar)->first();

        $menu = $this->retornarMenu();

        $type = 'editing';

        $user = Auth::user();

        return view('compras.requisicion.requisicionesEdit')
        ->with([
            'req' => $req,
            'type' => $type,
            'menus' => $menu,
            'articulos' => $articulos,
            'partida' => $partida,
            'user' => $user,
        ]);
    }

    public function reqpres($reqart){ //Vista de presupuesto de la requisición

        $req = Casolart::where('reqart', $reqart)->with('articulos.partida', 'unidad', 'solicitante', 'ccosto', 'fin', 'moneda')->first();
        $user = Auth::user();
        $precom = Cpprecom::where('refprc', $reqart)->exists();
        #dd($precom);
        $menu = $this->retornarMenu();

        return view('compras.requisicion.reqPresupuesto')
        ->with([
            'menus' => $menu,
            'req' => $req,
            'user' => $user,
            'precom' => $precom
        ]);
    }

    public function aceptar(Request $request) //Acciones para la vista del gerente
    {

        $date = Carbon::now()->format('Y-m-d');
        $req = Casolart::where('reqart', $request->req)->first();
        $user = Nphojint::where('codemp', $request->user)->first();
        if($request->status === 'S'){

            $req->stareq = 'A';
            $req->aprreq = 'G';
            $req->gerapru = $user->codemp;
            $req->fecaprger = $date;

            $req->update();

            $reqart = $req->reqart;
            $this->sendpre($reqart);

        } else if($request->status === 'N'){
            $req->stareq = $request->status;
            $req->motreq = $request->obs;
            $req->gerapru = $user->codemp;
            $req->fecanu = $date;
            $req->fecaprger = $date;

            $req->update();
        }

        return response()->json(['success' => true, 'Procesado con éxito']);
    }

    public function preAction(Request $request) //Acción de gerente de presupuesto
    {
        $date = Carbon::now()->format('Y-m-d');
        $req = Casolart::where('reqart', $request->reqart)->first();
        $user = Auth::user();

        if($request->action === 'S')
        {
            $req->aprreq = 'P';
            $req->update();

            #$this->genPrecom($req->reqart);
            //Llamar al generador de precompromiso
            //Llamar al correo para gerente de presupuesto

            #return redirect()->route('genPrecom', [$req->reqart]);

            $precom = new PrecomController();
            $enviar = $precom->genPrecom($req->reqart);
        }

        else if($request->action === 'N')
        {
            $req->aprreq = 'R';
            $req->stareq = 'N';
            $req->fecanu = $date;
            $req->motreq = $request->obs;

            $req->update();
        }
        return response()->json(['success' => true, 'Procesado con éxito']);
        

    }

    public function reenviar($reqart) //Reenvío del correo al gerente
    {
        $req = Casolart::where('reqart', $reqart)->first();

        $gerente = new RequisicionController();
        $gerencia = new Request();
        $gerencia->gerencia = 'AC0001-010';

        $correo = $gerente->getGerente($gerencia);

        $this->gerMail($correo, $req);

        Session::flash('success', 'Correo de requisición '.$req->reqart.' reenviado con éxito.');
        return redirect()->route('listaReq');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sendpre($reqart) //Enviar correo a presupuesto
    {
       /* CORREOS DE ANALISTAS DE PRESUPUESTO
        * Yuridia Petit = ypetit@vit.gob.ve
        * Jazmín Revilla = jrevilla@vit.gob.ve

        

        *** Código para el envío de notificación a los analistas de presupuesto

        $emails = ['ypetit@vit.gob.ve', 'jrevilla@vit.gob.ve'];

        foreach($emails as $key => $email){
            Mail::to($email)->send(new ReqPresupuestoMail($data));
        }
        */
        
        $email = 'danreyes@vit.gob.ve';

        

        $data = Casolart::where('reqart', $reqart)->with('articulos.partida', 'solicitante', 'unidad')->first();
    	Mail::to($email)->send(new ReqPresupuestoMail($data));
        Session::flash('success', 'Correo de requisición '.$data->reqart.' reenviado con éxito a presupuesto.');
        return response()->json([
            'success', 
            'Correo enviado a presupuesto',
        ]);

    }

    public function aprsolegr()
    {
        $apros = Casolart::where('stareq', 'A')->where('aprreq', 'G')->paginate(8);

        $menu = $this->retornarMenu();


        return view('compras.requisicion.aprsolegr')->with([
            'menus' => $menu,
            'apros' => $apros,
        ]);
    }

    //API

    public function getGerente(Request $request) //Tomar gerente por unidad responsable
    {
        #dd($request);
        $unires = $request->gerencia;
        if($unires == 'AC0001-001'){
            #TALENTO HUMANO
            #Carmen Mendez
        } else if($unires == 'AC0001-002'){
            #AUDITORIA

        } else if($unires == 'AC0001-003'){
            #PRESIDENCIA

        } else if($unires == 'AC0001-004'){
            #PRESUPUESTO
            #Carolina Prieto
        } else if($unires == 'AC0001-005'){
            #FINANZAS
        } else if($unires == 'AC0001-006'){
            #SEGURIDAD
            #Orangel Mendoza
        } else if($unires == 'AC0001-007'){
            #COMPRAS - PROCURA DE MATERIA PRIMA - PROCURA DE PIEZAS Y PARTES
            #Milangela Quelis
        } else if($unires == 'AC0001-008'){
            #SERVICIOS GENERALES
            #Fidel Delgado
        } else if($unires == 'AC0001-009'){
            #RELACIONES PÚBLICAS
            #Yanetzi Zavala
        } else if($unires == 'AC0001-010'){
            #ATENCIÓN AL CIUDADANO
            #Neydu Lugo
            return 'danreyes@vit.gob.ve';
        } else if($unires == 'AC0001-011'){
            #CONSULTORÍA JURÍDICA
        } else if($unires == 'AC0001-012' || $unires == '009770-002'){
            #OTI
            #José Miguel
        } else if($unires == 'AC0001-013' || $unires == '009770-004'){
            #OBRAS Y PROYECTOS
            #Maria Mujica
        } else if($unires == '009770-001' || $unires == '009770-003'){
            #DESARROLLO TECNOLÓGICO
            #Rafael Marin
        } else if($unires == '009948-001'){
            #PRODUCCION
            #Jorge arias
        } else if($unires == '009948-002'){
            #ASEGURACIÓN DE CALIDAD
            #María Sierraalta
        } else if($unires == '009948-009'){
            #COMERCIALIZACIÓN
            #William López
        } else if($unires == '009948-004'){
            #PRESTACIÓN DE SERVICIO POST-VENTA
            #Doris Hernández
        } else if($unires == '009948-005'){
            return 'danreyes@vit.gob.ve';
            #ALMACEN Y GESTIÓN DESPACHO DE PRODUCTOS TERMINADOS
            #Argenis Acosta
        }
            
    }

    public function getData() //Envío de datos a la ruta de creación de requisición
    {
        $costos = Costos::whereRaw('LENGTH(codcen) = 3')->get();
        $unires = Npcatpre::whereRaw('LENGTH(codcat) > 9')->get();
        #dd($unires);
        $partidas = Nppartidas::get();
        $tipfin = DB::connection('pgsql2')->table('fortipfin')->get();
        $monedas = Tsdefmon::get();
        $recargos = Farecarg::get();

        

        $querys = null;
        $querys = Caregart::where('tipreg', null)->whereNull('deleted_at')
        ->with('color', 'facategoria', 'subcategoria', 'demoProducto', 'moneda')->get();
        
        return response()->json([
            'costos' => $costos,
            'unires' => $unires,
            'monedas' => $monedas,
            'recargos' => $recargos,
            'articulos' => $querys,
            'partidas' => $partidas,
            'tipfin' => $tipfin,
        ], 200);
    }

    public function getPart(Request $request)
    {

        $articulo = Caregart::where('codart', $request->codart)->select('codpar')->first();

        $partida = Nppartidas::where('codpar', $articulo->codpar)->first();

        return response()->json(
            ['data' => $partida]
            , 200);
    }

    public function artExist(Request $request)
    {
        $codart = $request->consulta;
    
        $art = Caregart::where('codart', $codart)->whereNull('deleted_at')->first();

        if($art)
        {
            return response()->json(['data' => true], 200); //existe
        }
        else{
            return response()->json(['data' => false], 200);
        }

    }

    public function reqExist(Request $request)
    {
        $reqart = $request->consulta;

        $req = Casolart::where('reqart', $reqart)->first();

        if($req)
        {
            return response()->json(['data' => true], 200); //existe
        }
        else{
            return response()->json(['data' => false], 200);
        }
    }

}