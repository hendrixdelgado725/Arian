<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\Caregart;
use App\Fargoart;
use App\Facliente;
use App\Fatippag;
use App\Farecarg;
use App\Fadescto;
use App\faartpvp;
use App\Bancos;
use App\Fatasacamb;
use App\Fafactur;
use App\Fafactuart;
use App\Faartfac;
use App\Fafacturpago;
use App\Faforpag;
use App\Tallas;
use App\Almacen;
use App\Caartalmubi;
use App\Fapresup;
use App\Fadefesc;
use App\Fadetesc;
use App\Famoneda;
use App\Fanotcre;
use App\ColorPro;
use App\NotaEntrega;
use App\Contabc;
use App\Contabc1;
use App\Contabb;
use App\Tsmovlib;
use App\Cobdocume;
use App\Tsdefban;
use App\Tstipmov;
use App\Cobrecdoc;
use App\Cobdesdoc;
use App\Tsdefmon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Sucursal;
use App\cajaUser;
use App\Facategoria;
use App\Models\Caja;
use App\Models\Serial;
use Helper;
use Exception;
use Session;
use Carbon\Carbon;
use \Fpdf;
use App\Http\Controllers\generadorPDF;
use App\Logs_impresoras;
use App\Models\Logs_impresora_api;
use App\Segususuc;
use App\Repositories\{
              CierreRepository,
              FiscalRepositoryApi,
              SucursalRepository,
              PosPrinterApi,
              PosPrinterRepository};
use \DB;
use App\Http\Requests\PrintFacturaRequest;
use App\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Contracts\Session\Session as SessionSession;
use App\Models\SerialEntradas;
use App\Models\serialDisponible;
use App\Models\cierre_log;


class FacturacionController extends Controller
{


    private $cierre;
    private $fiscalRepoApi;
    private $sucursalAu;
    public $posPrinter;
    public $posApi;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(
      CierreRepository $CierreRepo,
      FiscalRepositoryApi $fiscalrepo,
      SucursalRepository $sucursalAu,
      PosPrinterRepository $posPrinter,
      PosPrinterApi $posApi

      ){

      $this->cierre = $CierreRepo;
      $this->fiscalRepoApi = $fiscalrepo;
      $this->sucursalAu = $sucursalAu;
      $this->posPrinter = $posPrinter;
      $this->posApi     = $posApi;

      $this->middleware('auth');
    }
 

    public function index()
    {
        $menu = new HomeController();
        $query = Fafactur::query();

       if(Auth::user()->role->nombre == 'ADMIN')$query->orderby('reffac','ASC')->paginate(8);
        else $query->where('codsuc',Auth::user()->userSegSuc->codsuc)->orderby('reffac','DESC')->paginate(8);

        $facturas = $query;
        $facturas->load('cliente','sucursales','articulos','pagos','cajero');
        /*$facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero')->orderBy('id','DESC')->paginate(8);*/
        return view('/facturacion/factura')->with('menus',$menu->retornarMenu())->with('facturas',$facturas);

   }

    public function view1(){
        return view('facturacionv1');
    }

    public function view2(){

      $jefes = User::where('codroles','R-0001')->get();
      
      $articulos = Caregart::get();
      try{
        $sucursal = session('codsuc');
        $ip = request()->ip() === '::1' ? '127.0.0.1' : request()->ip();

        $user =cajaUser::where('loguse',Auth::user()->loguse)
        ->whereHas('caja',function($q) use ($ip,$sucursal)
        {
            $q->where('impfishost',$ip)->where('codsuc',$sucursal);

        })->with('caja')
        ->first();
        
        if($user === null) throw new Exception('Esta Usuario no tiene asociado una caja');
        $almacen = \App\Almacen::where('codsuc_asoc',$sucursal)->get();
        
        if($almacen->isEmpty()) throw new Exception('Este usuario no tiene una almacen asociado a la sucursal autenticado tienes que ir al modulo de almacen o contacte con el Administrador');
        
        $caja1 = Caja::where('impfishost',$ip)->with(['turno' => function ($q) {

              $q->where('cajero',Auth::user()->cedemp);
              
        }])->first();
        
        $fpagos = Fatippag::get();
        $recargo = Farecarg::where('status','=','S')
        ->where('defecto','=','S')
        ->where('deleted_at','=',null)
        ->whereHas('sucursalRec',function($q) use ($sucursal)
        {
          $q->where('codsuc',$sucursal)->where('deleted_at',null);
        })
        ->first();

        $recargos = Farecarg::where('deleted_at','=',null)->where('status','=','S')->get();
        $descuentos = Fadescto::get();
        /*$sucursal = Auth::user()->userSegSuc->codsuc;*/
        /*return $sucursal;*/
        /*return $almacen = Almacen::with('existencia')->get();*/
        $alm = Almacen::where('codsuc_asoc','=',session('codsuc'))->where('codalm',$user->caja->codalm)->select('codalm')->where('pfactur',true)->first();


        // $alm = Almacen::where('codalm',$user->caja->codalm)->select('codalm')->where('pfactur',true)->first();

         if($alm === null) throw new \Exception('Tienes que poner el almacen que esta asociado, a facturar en modulo de registro almacen');

        $monedas = Famoneda::has('tasaM1')->orHas('tasaM2')->get();
       /* return $alm;*/
        /*$codalm = $alm->codalm;*/
        $articulos = Caregart::has('costos')
        ->where('tipreg','F')
        //->where('staart','A')
        ->whereHas('almacenubi',function($q) use ($alm){
        //  $q->where('exiact','>',0)->whereIn('codalm',$alm);
          $q->where('exiact','>',0)->where('codalm',$alm->codalm);
        })->withSum('almacenubi','exiact')->get();
        
        
        if($articulos->isEmpty()) throw new Exception("No puedes facturar porque no tiene articulos cargado al almacen");

        $todastasas = Fatasacamb::with('moneda2')
        ->whereHas('moneda2', function($q){
          $q->where('nombre', 'like', "%BOLIVAR%");
        })->where('activo',true)
        ->get();
        /* SE BUSCA LA TASA CUYA SEGUNDA OPCION DE LA MONEDA SEA IGUAL AL BOLIVAR */

        $t = Fatasacamb::get();


       /* return Caregart::has('costos')->with('almacenubi')->whereHas('almacenubi',function($q) use ($alm){
          $q->where('exiact','>',0);
        })->get();*/
        /*$articulos = Caregart::has('costos')->get();*/
        $clientes = Facliente::orderBy('nompro')->where('codsuc',session('codsuc'))->get();
        $sucursal = Sucursal::where('codsuc',session('codsuc'))->with('company.bancos')->whereHas('company.bancos')->get();
        $categoria = Facategoria::where('nombre','TRANSPORTE')
        ->orWhere('nombre','FLETE')
        ->first();

        $transporte = Caregart::with('costoUnit')
        ->where('codsuc_reg',Auth::user()->codsuc_reg)
        ->where('artprecio','ACTIVO')
        ->where('codcat',$categoria->codigoid)
        ->first();

        if(!$sucursal->isEmpty()){
          $bancos = $sucursal->pluck('company.bancos')->flatten();
        }else{
          Session::flash('error','Debe asignar una empresa a la sucursal donde va a facturar, Si ya lo hizo entonces registre bancos a la empresa asociada');
          return redirect()->route('sucursalList');
        }

        $menu = new HomeController();
        $tasa = Fatasacamb::with('moneda','moneda2')->whereHas('moneda', function($q){
           $q->where('nombre','=','DOLLAR')->orWhere('nombre','=','DOLAR');
        })->where('activo','=',true)->first();

        if($tasa === null) throw new Exception("error debe tener una tasa activa");
        if(Auth::user()->getCaja2 === null) throw new Exception("Este Usuario no Tiene caja. llame al administrador de sistema");
        
        if($caja1 === null) throw new Exception("No tienes una caja asignada asociala a la ip de la maquina que va a facturar", 1);

        $ultimaF = null;
        $UltimaF2 = 0;
        $ultimaT = null;
        $UltimaT2 = 0;

        
        if($caja1->is_fiscal === "true"){

          $ultimaF = Fafactur::where('is_fiscal',true)->where('codcaj',$caja1->id)->orderBy('reffac','DESC')->first();

          if($ultimaF !== null) $UltimaF2 = intval(str_replace($caja1->id.'F',str_pad("0",3,0,STR_PAD_LEFT),$caja1->corfac))+1;
          else $UltimaF2 = intval(str_replace($caja1->id.'F',str_pad("0",3,0,STR_PAD_LEFT),$caja1->corfac))+1;
        }else{

          $ultimaT = Fafactur::where('is_fiscal',false)->where('codcaj',$caja1->id)->orderBy('reffac','DESC')->first();
          
          
           $UltimaT2 = intval(str_replace($caja1->id.'T',str_pad("0",3,0,STR_PAD_LEFT),$caja1->corfac))+1;

          
          $verifyCierre = cierre_log::whereDate('fecha',Carbon::yesterday()->toDateString())->where('caja',$user->codcaja)->where('tipoturno',$caja1->turno->tipoturno)->first();
          
          if($verifyCierre === null) throw new Exception("Error tienes que cerrar turno del dia anterior");
          

        }
        





        
    
        return view('facturacionv2')->with([
            'jefes'=>$jefes,
            'fpagos'=>$fpagos,
            'recargo'=>$recargo,
            'recargos'=>$recargos ?? '0.00',
            'articulos'=>$articulos,
            'bancos'=>$bancos,
            'arttojson'=> json_encode($articulos),
            'clientes'=>$clientes,
            'tasa'=>$tasa,
            'descuentos'=>$descuentos,
            'monedas'=>$monedas,
            'clienttojson'=>json_encode($clientes),
            'fpagotojson'=>json_encode($fpagos),
            'bancostojson'=>json_encode($bancos),
            'tasatojson'=>json_encode($tasa),
            'recargostojson'=>json_encode($recargos),
            'monedastojson'=>json_encode($monedas),
            'todastasastojson'=>json_encode($todastasas),
            'menus'=> $menu->retornarMenu(),
            'transporte'=>$transporte,
            'caja' => Auth::user()->getCaja2->codcaja,
            'dia' => Carbon::now()->format('d-m-Y'),
            'isfiscal' => $caja1->is_fiscal,
            'UltimaF' => $UltimaF2,
            'UltimaT' => $UltimaT2,
            'caja2' => $caja1
        ]);
      }catch(Exception $e){
        
        Session::flash('error',$e->getMessage());
        return redirect('/modulo/Facturacion/Lfacturas');
      }
    }

     public function view3(){

      try{

        $fpagos = Fatippag::get();
        $recargo = Farecarg::where('status','=','S')->where('defecto','=','S')->where('deleted_at','=',null)->first();
        $recargos = Farecarg::where('deleted_at','=',null)->where('status','=','S')->get();
        $descuentos = Fadescto::get();
        $sucursal = Auth::user()->userSegSuc->codsuc;
      /*  return $almacen = Almacen::with('existencia')->get();*/
        $alm = Almacen::where('codsuc_asoc','=',$sucursal)->select('codalm')->where('pfactur',true)->get();
        $monedas = Famoneda::has('tasaM1')->orHas('tasaM2')->get();
       /* return $alm;*/
        /*$codalm = $alm->codalm;*/
        $articulos = Caregart::has('costos')->whereHas('almacenubi',function($q) use ($alm){
          $q->where('exiact','>',0)->whereIn('codalm',$alm);
        })->get();

        $codigoM = '1-0002';
        $todastasas = Fatasacamb::with('moneda')->where('activo',true)->where('id_moneda2','=',$codigoM)->get();
        $t = Fatasacamb::get();

        $clientes = Facliente::get();
        $bancos = Bancos::get();
        $menu = new HomeController();
        $tasa = Fatasacamb::with('moneda','moneda2')->whereHas('moneda', function($q){
           $q->where('nombre','=','DOLLAR');
        })->where('activo','=',true)->first();

        return view('facturacionMa')->with([
            'fpagos'=>$fpagos,
            'recargo'=>$recargo,
            'recargos'=>$recargos,
            'articulos'=>$articulos,
            'bancos'=>$bancos,
            'arttojson'=> json_encode($articulos),
            'clientes'=>$clientes,
            'tasa'=>$tasa,
            'descuentos'=>$descuentos,
            'monedas'=>$monedas,
            'clienttojson'=>json_encode($clientes),
            'fpagotojson'=>json_encode($fpagos),
            'bancostojson'=>json_encode($bancos),
            'tasatojson'=>json_encode($tasa),
            'recargostojson'=>json_encode($recargos),
            'monedastojson'=>json_encode($monedas),
            'todastasastojson'=>json_encode($todastasas),
            'menus'=> $menu->retornarMenu(),

        ]);
      }catch(Exception $e){
        return redirect('/home', 'HomeController@index');
      }
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        try{
         
        /*exec('ls -l /dev/usb/lp0',$salida,$codigoSalida); //verifica si esta conectado el cable de la impresora

        //evalua
        if($codigoSalida !== 0) throw new Exception(strtoupper("Tienes que conectar el cable de la impresora"));
        else {
          exec('sudo chmod 777 /dev/usb/lp0',$salida1,$codigoSalida1);
        }*/

        $user = Auth::user()->loguse;
        $cajauser = cajaUser::with('caja')->where('loguse','=',$user)->first();
        $cajaNombre = $request['cdcaja'];
        
        if(is_numeric($cajaNombre)){
          $cajaO = Caja::where('id','=',$cajaNombre)->first();
        }
        else{
          $cajaO = Caja::where('descaj','=',$cajaNombre)->first();
        }
        $cajaid = $cajaO->id;
        $descaj = $cajaO->descaj;
        $impserial = $cajaO->impserial;
        $cli = $request['cliente'];

        if(ctype_alpha($cli[0])){
        /*     $cliente=Facliente::where('codpro','=',$cli)->first()->codpro;*/
             $cliente0 = Facliente::where('codpro','=',$cli)->first();
             if(!$cliente0){
               $newC = new Facliente();
               $newC->codpro = $cli;
               $newC->nompro = $request['nombreCli'];
               $newC->rifpro = $cli;
               $newC->save();
             }

          $cliente = Facliente::where('codpro','=',$cli)->first()->codpro;
        }
        else{

             $cedula = $request['tipodocumento'].$cli;
             $cliente0 = Facliente::where('codpro','=',$cedula)->first();

             $rif = $request['tipodocumento'].'-'.$cli;
             if(!$cliente0){
               $newC = new Facliente();
               $newC->codpro = $cedula;
               $newC->nompro = $request['nombreCli'];
               $newC->rifpro = $rif;
               $newC->save();
             }

          $cliente = Facliente::where('codpro','=',$cedula)->first()->codpro;
        }

        $caja = \App\Models\Turno::where('caja',$cajaid)->with(['caja.cajaUser' => function($q) use ($user){
          $q->where('loguse',$user);
      }])->first();

        $guion='-';
        $int = $cajaid > 10 ? 5 : 6;
        $cod = '';
        
        if($cajaO->is_fiscal === "true"){//fiscal

          $cont1 = Fafactur::withTrashed()->where('codcaj',$cajaO->id)->where('is_fiscal',true)->where('deleted_at',null)->orderby('id','DESC')->first();//codigo generado en sendfiscal
          if($cont1 !== null){
            
           $verIfExistLog = Logs_impresora_api::where('factura_sistema',$cont1->reffac)->first();
            
            if($verIfExistLog === null) throw new Exception("No puedes facturar ya que la anterior factura hubo un error, y no completò el proceso, llamen al administrador de sistema", 1);
          }


         //$cont1 = Fafactur::withTrashed()->where('codcaj',$cajaO->id)->where('is_fiscal',true)->where('deleted_at',null)->orderby('id','DESC')->first();//codigo generado en sendfiscal
        $cont = $cajaO->corfac+1;//toma el ultimo correaltivo que se actualizado en la caja


        $cod =$cajaid.'F'.str_pad($cont,$int,"0",STR_PAD_LEFT);//.$guion.$codsuc_reg[1];
        }
        else{// tickera
          
          $cont1 = Fafactur::withTrashed()->where('codcaj',$cajaO->id)->where('is_fiscal',false)->where('deleted_at',null)->orderby('id','DESC')->first();//codigo generado en sendfiscal
          if($cont1 !== null){
            
            //$verIfExistLog = Logs_impresora_api::where('factura_sistema',$cont1->reffac)->first();
            
            
            //if($verIfExistLog === null) throw new Exception("No puedes facturar ya que la anterior factura hubo un error, y no completò el proceso, llamen al administrador de sistema", 1);
          }


          $cont = $cajaO->corfac+1;

          if($cont === null){
            $cod =  $cajaid.'T'.str_pad($cont,$int,'0',STR_PAD_LEFT);
          }else $cod =  $cajaid.'T'.str_pad($cont,$int,'0',STR_PAD_LEFT);
        }
        //dd(str_replace($cajaid.'T',str_pad("0",3,0,STR_PAD_LEFT),Fafactur::where('is_fiscal',false)->where('codcaj',$cajaO->id)->orderby('id','DESC')->first()->reffac));
         $cont2 = Fafactur::withTrashed()->count();
        //$cont = Fafactur::withTrashed()->get()->count(); //codigo
        #$cont =  $cont+1;
        ##dd(Fafactur::where('is_fiscal',false)->get()->count(),$cod);
        $cont2 = $cont2+1;
        $suc = Auth::user()->userSegSuc->codsuc;

       
        $codsuc_reg = explode('-',$suc);
        if(isset($request->all()['tasaP'])){

          if($request->all()['tasaP'] === "true"){//si pudo tomar la tasa del bcv
            
            $t = Fatasacamb::with('moneda','moneda2')->whereHas('moneda', function($q){
            
            $q->where('nombre','=','DOLLAR');


          })->where('activo','=',true)->first();
          
          if(strcmp($t->valor,$request->all()['valorT']['valor']) !== 0){

          $guion ='-';
          $cont1 = Fatasacamb::withTrashed()->get()->count();
          $cont1 = $cont1 +1;
          // $suc = Auth::user()->codsuc_reg;
          $suc=Auth::user()->getCodigoActiveS();
          $codsuc_reg = explode('-',$suc);
          $cod1 = $cont1.$guion.$codsuc_reg[1];
          
          $t->update(['activo' => false]);
          
          //quede aqui
          Fatasacamb::updateOrCreate([
            
            'id_moneda' => $request->all()['valorT']['id_moneda'],
              'id_moneda2' => $request->all()['valorT']['id_moneda2'],
              'activo' => true,
              'valor' => $request->all()['valorT']['valor'],
              'codsuc' => $request->all()['valorT']['codsuc'],
              'codigoid' => $cod1
            
            
          ]);

        }
        
          
          
      }
    }

        
        #$cod = $cajaid.'F'.'07277';
        //$cod = $cajaO->correlativo.$cont.$guion.$codsuc_reg[1];

        
        

        $cod2 = $cont2.$guion.$codsuc_reg[1];

        if($request['fadescuento']==0){
            $descuento = $request['fadescuentoG'];
        }
        else{
              $descuento = $request['fadescuento'];
        }

        $status = 'A';

        $codigoIva = Farecarg::where('monrgo','=',$request['ivaporcentaje'])->first();
        if(!$codigoIva) {
          $codigoIva = null;
        }else{
          $codigoIva= $codigoIva->codigoid;
        }



        
        $t = Fatasacamb::with('moneda','moneda2')->whereHas('moneda', function($q){
              $q->where('nombre','=','DOLLAR');
            })->where('activo','=',true)->first();


        //dd($request->all());
        //dd($descuento,$request['fatotal']);
        $factura = new Fafactur();
        $factura->reffac = $cod;
        $factura->codcli = $cliente;
        $factura->fecfac = Carbon::now()->format('Y-m-d');
        $factura->tipref = 'V';
        $factura->monfac =  $request['fatotal'];
        $factura->monsubtotal = $request['fasubtotal'];
        $factura->mondesc = $descuento;
        $factura->monrecargo =  $request['faiva'];
        $factura->porrecargo = $request['ivaporcentaje'];
        $factura->cod_recargo = $codigoIva;
        $factura->valmon = $request['fatasa'];
        $factura->codsuc = $suc;
        $factura->reapor = $user;
        $factura->codcaj = $cajaid;
        $factura->caja = $descaj;
        $factura->cajaid = $cajaO->codigoid;
        $factura->status = $status;
        $factura->impserial = $impserial;
        $factura->codigoid = $cod2;
        $factura->tasa = $t->codigoid ?? null;
        $factura->is_fiscal = $cajaO->is_fiscal === "true" ? true : false;
        $factura->turnos = $caja->tipoturno ?? '';
        $factura->save();
        $conta = Faartfac::withTrashed()->count();
        $conta_recargo = Fargoart::withTrashed()->count();
        $reffac = $cod;
        //$toSerial[] = [];
        //$updateSerial[] = [];

        foreach($request['articulos'] as $art){
          $totart = 0;
            if(($art['descuento'] === "true" || $art['descuento'] === true)){
              $codDescon = Fadescto::where('mondesc','=',$art['mondescuento'])->first()->codigoid;
              $costoDescuento = $art['costo'] - $art['descontado'];
            }
            else{
              $codDescon = '';
              $costoDescuento = 0;
            }

            $conta = $conta+1;
            $coda = $conta.$guion.$codsuc_reg[1];

            $iva = $request['ivaporcentaje'];

            $calculo = ($art['ivaStatus'] === "true") ? ($art['costo'] - $art['descontado']) * $iva : 0; //si tiene descuento le resta y multiplica con el iva
            
            $calculo = $calculo !== 0  ? $calculo/100 : 0;
            
            $totart = ($art['costo'] - $art['descontado']) + $calculo;
            //$totart = $totart - $art['descontado'];
            //$art['mondescuento']
            $cptArticulos[] = [
              'reffac' => $cod,
              'codart' => $art['codart'],
              'desart' => $art['articulo'],
              'cantot' => $art['cantidad'],
              'descuento' => $art['descuento'],
              'mondes' => $art['descontado'],
              'coddescuento' => $codDescon ?? '',
              'precio' =>  $art['preunit'],
              'costo' =>  ($art['descuento'] === "true" || $art['descuento'] === true) ? $art['costo'] - $art['descontado'] : $art['costo'],
              'monrgo' => ($art['ivaStatus'] === "true") ? $calculo : 0,
              'totart' => $totart,
              'recargo' => ($art['ivaStatus'] === "true") ? $request['ivaporcentaje'] : 0,
              'talla' => $art['talla'] ?? null,
              //codalm = $codalm;
              'codsuc' => $suc,
              'codigoid' => $coda,
              'predivisa' => $art['predivisa'],
              'costodescuento' =>$art['costo'] -  $art['descontado'],
              'descrip_art' => (isset($art['descripcion'])) ? $art['descripcion'] : '',
              'porcentajedescuento' => $art['mondescuento']
              ];
          
          
              /*$articulo = new Faartfac;
              $articulo->reffac = $cod;
              $articulo->codart = $art['codart'];
              $articulo->desart = $art['articulo'];
              $articulo->cantot = $art['cantidad'];
              $articulo->descuento = $art['descuento'];
              $articulo->mondes = $art['descontado'];
              $articulo->coddescuento = $codDescon;
              $articulo->precio = $art['preunit'];
              $articulo->costo = $art['costo'];
              $articulo->monrgo = ($art['ivaStatus'] === "true") ? $calculo : 0;
              $articulo->totart = $totart;
              $articulo->recargo = ($art['ivaStatus'] === "true") ? $request['ivaporcentaje'] : 0;
              $articulo->talla = $art['talla'] ?? null;
                $articulo->codalm = $codalm;
              $articulo->codsuc = $suc;
              $articulo->codigoid = $coda;
              $articulo->predivisa = $art['predivisa'];
              $articulo->costodescuento = $costoDescuento;
              $articulo->save();//guardar 
              $articulo->descrip_art = (isset($art['descripcion'])) ? $art['descripcion'] : '';*/
            if(isset($art["serial"])){
              if(count($art["serial"]) !== 0){

                  foreach($art["serial"] as $serial){
                    $toSerial[] = [
                      'serial' => $serial,
                      'factura' => $cod,
                      'codart' => $art['codart'],
                      'facturar_serial' => false
                    ];
                  }
                  $updateSerial[] = $serial;
              }
            }



            if($art['ivaStatus'] === "true"){
              $farecarg = new Fargoart;
              $farecarg->refdoc = $reffac;
              $farecarg->codrgo = $request->recargo_data["codrgo"] ?? '';
              $farecarg->codart = $art["codart"];
              $farecarg->desart = $art["articulo"];
              $farecarg->tipdoc = "F";
             // $farecarg->monrgo = ((float) $request->recargo_data["monrgo"] * $art['costo']) / 100;
              $farecarg->codcaj = $cajaid;
              $farecarg->save();
            }

          }
        
          
        if(isset($toSerial)){

          if(count($toSerial) !== 0){

            Serial::insert($toSerial);
            serialDisponible::whereIn('seriales',$updateSerial)->update(['disponible' => false]);
          }
        }


        if(isset($cptArticulos))
           Faartfac::insert($cptArticulos);


           foreach($request['pagos'] as $pago){
            if($pago['fpago'] == 'CREDITO'){
                $tipocredito = $pago['tipocredito'];
            }
            else{
                $tipocredito = 'NINGUNO';
            }
           /* $contp = Fafacturpago::withTrashed()->get()->count();*/
            $contp = Faforpag::withTrashed()->get()->count();
            $contp = $contp+1;
            $codp = $contp.$guion.$codsuc_reg[1];
            if($pago['moneda'] == 'BOLIVAR')
            {
              //$pago['moneda'] = 'BOLIVAR SOBERANO';
            }
            $monedacod = Famoneda::where('nombre','=',$pago['moneda'])->first()->codigoid;
            $codpago = Fatippag::where('destippag','=',$pago['fpago'])->first();
            $pagoFac = new Faforpag();
            $pagoFac->reffac = $cod;
            $pagoFac->fpago = $pago['fpago'];
            $pagoFac->codpago = $codpago->codigoid;
            $pagoFac->tippag = $codpago->id;
            $pagoFac->nropag = '000';
            /*$pagoFac->codigoCaja = $cajaid;*/
            $pagoFac->monpag = round(floor($pago['monto']*1000)/1000,2,PHP_ROUND_HALF_DOWN);
            $pagoFac->montocambio = $pago['montoc'];
            $pagoFac->moneda = $monedacod;
            $pagoFac->nomban = $pago['banco'];
            $pagoFac->tipocredito = $tipocredito;
            $pagoFac->codsuc = $suc;
            $pagoFac->codigoid = $codp;
            $pagoFac->refbillete = $pago['refbillete'];
            $pagoFac->tasacod = $pago['tasaCod'];
            $pagoFac->save();

   /*            if($caja->tipo == 'CO'){

                    $libro = new Tsmovlib();
                    $fpago = Fatippag::where('destippag','=',$pago['fpago'])->first();
                    $banco = Tsdefban::find($bancos[$i]);
                    $monedap = Famoneda::where('nombre','=',$pago['moneda'])->first();
                    $monedap = Tsdefmon::where('nombre','=',$pago['moneda'])->first();
                    $tipomovimiento = Tstipmov::where('destip','=',$pago['fpago'])->first();

                   $libro->numcue = $banco->numcue;
                   $libro->reflib = $pago['refbillete']; ?? '';
                   $libro->feclib = date('Y-m-d');
                   $libro->tipmov = $tipomovimiento->codtip;
                   $libro->deslib = $factura->desfac;
                   $libro->monmov = $pago['monpag'];
                   $libro->codcta = $banco->codcta;
                   $libro->numcom = $factura->reffac;
                   $libro->status = 'C';
                   $libro->feccom = date('Y-m-d');
                   $libro->stacon = 'N';
                   $libro->codmon = $monedap->codigoid;
                   $libro->save();
              }*/

        }


          $articulos = Faartfac::where('reffac','=',$cod)->get();
          $almacenes = Almacen::where('codsuc_asoc','=',$suc)->where('pfactur',true)->get();
          $existencia = '';
          foreach($articulos as $art){

          foreach($almacenes as $almacen){
            if($art->talla){
               $existencia = Caartalmubi::where('codalm','=',$almacen->codalm)
               ->where('codart','=',$art->codart)
               ->where('codtalla','=',$art->talla)
               ->where('exiact','>',0)
               ->first();
              if($existencia){
                $codalm = $almacen->codalm;
              break;
              }
            }//if talla
             else{
             $existencia = Caartalmubi::where('codalm','=',$almacen->codalm)
             ->where('codart','=',$art->codart)
             ->where('exiact','>',0)
             ->first();


              if($existencia){
                $codalm = $almacen->codalm;
              break;
              }
            }//else
          }//foreach
            if(isset($existencia)){
              $cantEnInventario = $existencia->exiact;
              $cantSustraer = $art->cantot;
              $cantEnInventario = $cantEnInventario-$cantSustraer;
              $existencia->exiact = $cantEnInventario;
              $existencia->save();
              $aral = Faartfac::where('reffac','=',$art->reffac)
                      ->where('codart','=',$art->codart)->first();
              $aral->codalm = $codalm;
              $aral->save();
            }

           } //foreach articulos



           if($request['presuMode'] === true){
            $presupuesto = Fapresup::where('codpre','=',$request['presuNum'])->first();
            $presupuesto->facturado = true;
            $presupuesto->reffac = $cod;
            $presupuesto->save();
           }

           if($request['notaEntregaMode'] === true){
            $nota = NotaEntrega::where('numero',$request['notaNum'])->first();
            $nota->facturado = true;
            $nota->reffac = $cod;
            $nota->extraido = true;
            $nota->save();
           }

           $factura = Fafactur::where('reffac',$cod)->first();
           $articulos = Faartfac::where('reffac',$cod)->get();

           /*$this->registrarCuenta($factura,$articulos);*/

        /*   if($caja->tipo == 'CR'){

                  $cob = new Cobdocume();
                  $cob->refdoc = $factura->reffac;
                  $cob->codcli = $factura->codcli;
                  $cob->fecemi = date('Y-m-d');
                  $cob->fecven = $factura->vencimiento;
                  $cob->oridoc = 'NOT';
                  $cob->desdoc = $factura->desfac;
                  $cob->mondoc = $factura->monfac;
                  $cob->recdoc = $factura->monrecargo;
                  $cob->desdoc = $nota->descuento;
                  $cob->abodoc = 0;
                  $cob->saldoc = $factura->monfac;

                  $asiento = Contabc::where('reftra',$factura->reffac)->first();

                  $cob->numcom = $asiento->numcom;
                  $cob->feccom = $asiento->feccom;
                  $cob->save();

                  if($factura->cod_recargo){

                    $recdoc = new Cobrecdoc();
                    $recdoc->refdoc = $factura->reffac;
                    $recdoc->codcli = $factura->codcli;
                    $recdoc->codrec = $factura->porrecargo;
                    $recdoc->monrec = $factura->monrecargo;
                    $recdoc->save();

                  }

                  if($factura->mondesc > 0){
                    $desdoc = new Cobdesdoc();
                    $desdoc->refdoc = $factura->reffac;
                    $desdoc->codcli = $factura->codcli;
                    $desdoc->mondes = $factura->mondesc;
                  }
            }*/
           # dd(Fafactur::where('is_fiscal',false)->get()->count(),$cod);

      return response()->json([
        'exito'    => $cod,
        'codcaja'  => $cajaid
      ], 200);

    }catch(\Exception $e){
      #dd($e);
        return response()->json(['error' => $e->getMessage()]);
    }

    }

    public function view22(Request $request)
    {


        $fpagos = Fatippag::get();
        $recargo = Farecarg::where('status','=','S')->first();
        $descuentos = Fadescto::get();
        $articulos = Caregart::has('costos')->get();
        $clientes = Facliente::get();
        $bancos = Bancos::get();
        $menu = new HomeController();
        $tasa = Fatasacamb::with('moneda','moneda2')->whereHas('moneda', function($q){
           $q->where('nombre','=','DOLLAR');
        })->where('activo','=',true)->first();

        return view('facturacionv22')->with([
            'fpagos'=>$fpagos,
            'recargo'=>$recargo,
            'articulos'=>$articulos,
            'bancos'=>$bancos,
            'arttojson'=> json_encode($articulos),
            'clientes'=>$clientes,
            'tasa'=>$tasa,
            'descuentos'=>$descuentos,
            'clienttojson'=>json_encode($clientes),
            'fpagotojson'=>json_encode($fpagos),
            'bancostojson'=>json_encode($bancos),
            'tasatojson'=>json_encode($tasa),
            'menus'=> $menu->retornarMenu()
        ]);
    }

    public function sendFiscal(Request $request,$devolver = false){

     /*  return "sendfiscal";*/

       //obtenemos info del cajero y la caja en la que se ha facturado
       $result =  $this->create($request)->getData();
      if(!isset($result->exito)) return $result;

        $user = Auth::user()->loguse;
        $cajauser = cajaUser::with('caja')->where('loguse','=',$user)->first();
        $cajaNombre = $request['cdcaja'];
          if(is_numeric($cajaNombre)){
           $cajaO = Caja::where('id','=',$cajaNombre)->first();
          }
          else{
          $cajaO = Caja::where('descaj','=',$cajaNombre)->first();
          }
          //dd($cajaO);
        $cajaid = $cajaO->id;
        $cajaCodId = $cajaO->codigoid;
        $descaj = $cajaO->descaj;
        $cli = $request['cliente'];

        ///obtenemos la info completa del cliente
        if(ctype_alpha($cli[0])){
             $cliente=Facliente::where('codpro','=',$cli)->first();
        }
        else{
             $cedula = $request['tipodocumento'].$cli;
             $cliente = Facliente::where('codpro','=',$cedula)->first();
        }

        //Creamos la reffac
        $status = 'A';
        $guion='-';
        $suc = Auth::user()->userSegSuc->codsuc;
        $codsuc_reg = explode('-',$suc);
        $cod =  $result->exito;
        $lastId = Fafactur::where('reffac',$cod)->first()->id;
        $factura = Fafactur::with('cliente','articulos')->where('reffac','=',$cod)->first();
        $array_json = [];
        $descuentoG = $request->fadescuentoG == "0" ? (int) $request->fadescuengG : (float)$request->fadescuentoG;

        $obser = $descuentoG === 0 ? "**": "Factura con ".(int)$descuentoG."% Descuento";
        $array_json["maestro"] = array(
           /* "factura" => $factura->reffac,
            "rif" => $factura->cliente->rifpro,
            "razon_social" => $factura->cliente->nompro,
            "telefono" => $factura->cliente->telpro,
            "direccion" => $factura->cliente->dirpro,
            "observacion" => "Factura Prueba",
            "id" => $factura->id,*/
             "factura" => $cod,
            "rif" => $cliente->rifpro,
            "razon_social" => $cliente->nompro,
            "telefono" => $cliente->telpro,
            "direccion" => $cliente->dirpro,
            "observacion" => $obser,
            "id" => $lastId,
          );

            foreach ($request['articulos'] as $articulo) {
              //dd($articulo->recargo);
              $iva = $request['ivaporcentaje'];
              if($iva != "0"){
              $calculo = $articulo['costo']*$iva;
              $calculo = $calculo/100;
              }else{
                $calculo= 0;
              }
              $tallaEz = Tallas::where('codtallas',$articulo['talla'])->first();
              if($tallaEz){
                  if($tallaEz->tallas === 'N/A') $tallaEz = null;
              }
              $desFact = substr($articulo["codart"],0,14);//soloa1win por que es pnp
              //$desFact = substr($articulo["codart"],0,15);//soloa1win por que es pnp
              //dd(substr($articulo["articulo"],0,15));//
              $desartP = $tallaEz ? $desFact."T".$tallaEz->tallas : $desFact;
              if($articulo["descuento"] === "true"){
                // $descuento = (int)$articulo["mondescuento"]/100;
                // $porcentaje = (float)$articulo["preunit"] * $descuento;
                $unitDiscount = (float) $articulo["descontado"]/(int)$articulo["cantidad"];
              }else{
                (int)$unitDiscount = 0;
              }
              $precioUnit = (float)$articulo["preunit"]-$unitDiscount;
              // $total = ($articulo["preunit"]*$articulo["cantidad"])-$unitDiscount;
              /* dd("TOTAL SIN IVA",$total,"TOtal IVA", $total*0.16,
              "Precio Unitario",$precioUnit,'Descuento',$unitDiscount,
              "Number_format",number_format($precioUnit,2,'.',''));
              dd("beta"); */
            $array_json["detalle"][] = array(
                      /*  "articulo" => $articulo->codart,
                        "descripcion" => $articulo->descripcion."/T:".$articulo->talla,
                        //"unidad" => ,
                        "cantidad" => $articulo->cantidad,
                        "precio" => $articulo->preunit,
                        "recargo" => $articulo->monrecargo,
                        "iva" => $articulo->recargo,
                        "descuento" => $articulo->descontado,
                        "total" => $articulo->costo,*/

                        "articulo" => $articulo['codart'],
                        "descripcion" => $desartP,
                        //"unidad" => ,
                        "cantidad" => $articulo["cantidad"],
                        "precio" => number_format($precioUnit,2,'.',''),
                        #"precio" => (float) number_format((float) $precioUnit + 0.001,3,'.',''),
                        "recargo" => $calculo,
                        "iva" => $iva,
                        "descuento" => $articulo['descontado'],
                        "total" => $articulo['costo'],
            );
        }
        $caja = Caja::where('codigoid',$cajaCodId)->first();//missing
        $redis = new \Predis\Client(array(
        'host' => $caja->impfishost,
        'port' => '6379',
        ));

        $sidekiq = new \SidekiqJobPusher\Client($redis, $caja->impfisname);
        $sidekiq->perform('ImprimirFacturaWorker', array($array_json));

        sleep(20);
        $test = Logs_impresoras::where('numero_factura',$cod)->where('error',null)->first();
        $test = true;
        $factura = Fafactur::where('reffac',$cod)->first();
        if($test){
            $factura->impfissta = 'I';
            $factura->save();
            return response()->json(["exito" => "Factura Emitida Correctamente"]);
          }else{
            $factura->impfissta = 'E';
            $factura->save();
          $errormsg = Logs_impresoras::where('numero_factura',$cod)->orderby('id','desc')->first();
          if(!$errormsg){
            $errormsg = "Verifique la configuracion de la impresora fiscal";
          }else{
            $errormsg = $errormsg->error;
          }
          return response()->json(['error' => $errormsg]);
        }
    }

    public function sendFiscalAn($codfac,$codcaja){

        $factura = Fafactur::with('cliente','articulos')
                   ->where('reffac','=',$codfac)
                   ->where('status','N')->first();

        $log = Logs_impresoras::where('factura_id',$factura->id)->whereNull('numero_devolucion')->first();

        if(is_numeric($codcaja)){
          $cajaO = Caja::where('id','=',$codcaja)->first();
          }
          else{
          $cajaO = Caja::where('descaj','=',$codcaja)->first();
          }

          $array_json["maestro"] = array(
            "factura" => $codfac,
            "rif" => $factura->cliente->rifpro,
            "razon_social" => $factura->cliente->nompro,
            "telefono" => $factura->cliente->telpro,
            "direccion" => $factura->cliente->dirpro,
            "observacion" => "Factura Prueba",
            "id" => $factura->id,
          );
              if($log){
                $array_json["maestro"]['numero_factura'] = $log->numero_factura;
                $array_json["maestro"]['fecha'] = $log->fecha;
                $array_json["maestro"]['hora'] = $log->hora;
                $array_json["maestro"]['serial_maquina'] = $log->serial_impresora;
              }else{
                return response()->json(['error' => 'No se puede anular por que no existe en logs impresoras']);
              }

              // $descuentoG = $request->fadescuentoG == "0" ? (int) $request->fadescuengG : (float)$request->fadescuentoG;
              foreach ($factura->articulos as $articulo) {
              $iva = $factura->porrecargo;
              if($iva != 0){
              $calculo = $articulo['costo']*$iva;
              $calculo = $calculo/100;
              }else{
                $calculo= 0;
              }
              $tallaEz = Tallas::where('codtallas',$articulo['talla'])->first();
              if($tallaEz){
                  if($tallaEz->tallas === 'N/A') $tallaEz = null;
              }
              $desFact = substr($articulo["codart"],0,14);//soloa1win por que es pnp
              //$desFact = substr($articulo["codart"],0,15);//soloa1win por que es pnp
              //dd(substr($articulo["articulo"],0,15));//

              $desartP = $tallaEz ? $desFact."T".$tallaEz->tallas : $desFact;
              if($articulo["mondes"] != "0.00"){
                // $descuento = (int)$articulo["mondescuento"]/100;
                // $porcentaje = (float)$articulo["preunit"] * $descuento;
                $unitDiscount = (float) $articulo["precio"]/(int)$articulo["cantot"];
              }else{
                (int)$unitDiscount = 0;
              }
              $precioUnit = (float)$articulo["precio"]-$unitDiscount;
              $array_json["detalle"][] = array(
                      /*  "articulo" => $articulo->codart,
                        "descripcion" => $articulo->descripcion."/T:".$articulo->talla,
                        //"unidad" => ,
                        "cantidad" => $articulo->cantidad,
                        "precio" => $articulo->preunit,
                        "recargo" => $articulo->monrecargo,
                        "iva" => $articulo->recargo,
                        "descuento" => $articulo->descontado,
                        "total" => $articulo->costo,*/

                        "articulo" => $articulo['codart'],
                        "descripcion" => $desartP,
                        //"unidad" => ,
                        "cantidad" => $articulo['cantot'],
                        "precio" => number_format($precioUnit,2,'.',''),
                        "recargo" => $calculo,
                        "iva" => $iva,
                        "descuento" => $articulo['descontado'],
                        "total" => $articulo['costo'],
                       );
        }

        $caja = Caja::where('codigoid',$cajaO->codigoid)->first();//missing
        $redis = new \Predis\Client(array(
        'host' => $caja->impfishost,
        'port' => '6379',
        ));

        $sidekiq = new \SidekiqJobPusher\Client($redis, $caja->impfisname);
        $sidekiq->perform('ImprimirDevolucionWorker', array($array_json));
        sleep(20);
        $test = Logs_impresoras::where('numero_factura',$codfac)->where('error',null)->first();
        if($test){
          return response()->json(["exito" => "Hecho"]);
        }else{
          $errormsg = Logs_impresoras::where('numero_factura',$codfac)->where('error','<>',null)->orderby('id','desc')->first();
          if(!$errormsg){
            $errormsg = "Verifique la conexión de la impresora";
          }
          return response()->json(['error' => $errormsg]);
        }

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getSeriales(Request $request)
    {
      try {
        $sucursal   = Auth::user()->userSegSuc->codsuc;
      $alm = Almacen::where('codsuc_asoc','=',$sucursal)->select('codalm')->where('pfactur',true)->first();

      $serialesEntradas = serialDisponible::when($request->input('serial') === null,function($q) use ($request,$alm){
        return $q->where('codart',$request->codart)
        ->where('disponible',true)
        ->doesntHave('SerialesFacturados')
        ->where('codalm',$alm->codalm);

      })->when($request->input('serial') !== null,function ($q) use ($request,$alm) {

        return $q->where('codart',$request->codart)
        ->where('disponible',true)
        ->where('seriales','ilike','%'.$request->serial.'%')
        ->doesntHave('SerialesFacturados')
        ->where('codalm',$alm->codalm);

      })->get();


        /*en facturacion arreglar seleccionar seriales a facturar */
        if($serialesEntradas->isEmpty()) throw new Exception("NO SE HAN ENCONTRADO EL SERIAL", 500);

        $serialesEntradasCpt = $serialesEntradas->map(function ($value)
        {
            return $value['seriales'];
        });
        $serialesFinales = collect();
        foreach ($serialesEntradasCpt as $key) {
          $serialesFinales->push($key);
        }

        return [
          'seriales' => $serialesFinales
        ];

      } catch (\Exception $th) {
            return ['msg' => $th->getMessage()];
      }

    }

    public function getPrecio(Request $request){

        /*$sucursal = Auth::user()->userSegSuc->codsuc;
        $alm = Almacen::where('codsuc_asoc','=',$sucursal)->select('codalm')->where('pfactur',true)->first();
        $existencia = Caartalmubi::where('codart',$request["articulo"])
          ->where('exiact','>',0)
          ->where('codalm',$alm->codalm)
          ->get();

        if($existencia->isEmpty())return response()->json([
          'error' => true,
          'msg'   => 'Ya este articulo no existe!'
        ]);*/


        $precio     = faartpvp::where('codart','=',$request['articulo'])->where('status','A')->with('moneda')->first();

        $sucursal   = Auth::user()->userSegSuc->codsuc;
        $art        = Caregart::where('codart','=',$request['articulo'])->first();
        $tallas     = [];
        $codtallas  = explode(' ',$art->codtallas);

        /* foreach($codtallas as $codtalla){
          $talla = Caartalmubi::where('codart'   , $request["articulo"])
                              ->where('codtalla' , $codtalla)
                              ->where('exiact','>',0)
                              ->with('tallas')
                              ->first();
          //$talla = Tallas::where('codtallas','=',$codtalla)->get();
          if($talla){
            array_push($tallas, $talla->tallas);
          }
        } */

        $sucursal = Auth::user()->userSegSuc->codsuc;//$request["articulo"]
        $alm = Almacen::where('codsuc_asoc','=',$sucursal)->select('codalm')->where('pfactur',true)->first();



        if($precio->moneda === null) return response()->json([
          'moneda' => "no tiene moneda"
        ]);
          #dd($precio->moneda->codigoid);
        $tasa = Fatasacamb::where('activo',true)->whereHas('moneda',function($q) use ($precio){
           $q->where('codigoid',$precio->moneda->codigoid);
        })->first();

        #$tasa = Fatasacamb::where('id_moneda',$precio->moneda->codigoid)->where('activo',true)->first();//cambio momentaneo
        // $codart = $art->codart;
        // $almacenes = Almacen::has('existencia')->whereHas('existencia',function($q) use ($codart){
        //   $q->where('codart','=',$codart);
        // })->where('codsuc_asoc','=',$sucursal)->get();

        if(count($tallas) === 0){
          return response()->json([
          'precio' => $precio,
          'tasa'=>$tasa,
          ]);
        }
        else{
          return response()->json([
          'precio' => $precio,
          'tallas' => $tallas,
          'tasa'=>$tasa,
         ]);
        }

       /* return response()->json($precio);*/


    }

    public function getTallas(Request $request){
   /*   $codart = $request['articulo'];
      $almacen = $request['almacen'];
      $codalm = Almacen::where('nomalm','=',$almacen)->first()->codalm;
      $arts = Caartalmubi::where('codalm','=',$codalm)->where('codart','=',$codart)->where('exiact','>',0)->get();

      $tallas = [];
        foreach($arts as $art){
          $talla = Tallas::where('codtallas','=',$art->codtalla)->get();
          array_push($tallas, $talla);
        }
*/
        $codart = $request['articulo'];
        $sucursal = Auth::user()->userSegSuc->codsuc;
        $almacenes = Almacen::where('codsuc_asoc','=',$sucursal)->get();
        $tallas = [];
        foreach($almacenes as $almacen){

           $arts = Caartalmubi::where('codalm','=',$almacen->codalm)->where('codart','=',$codart)->where('exiact','>',0)->get();
    /*        $arts = Caartalmubi::where('codalm','=',$almacen->codalm)->get();*/
         /*   return response()->json($arts);*/

           if($arts->isEmpty() || $arts == null){

           }
           else{
          /*  return gettype($arts);*/
              foreach($arts as $art){
               $talla = Tallas::where('codtallas','=',$art->codtalla)->get();
               array_push($tallas, $talla);
              }//foreach
             return response()->json($tallas);
           }//if
        }//foreach
    }

    public function getArt(Request $request){

        $articulo = Caregart::where('codart','=',$request['articulo'])->first();
        return response()->json($articulo);
/* No se valida inventario */
        $sucursal = Auth::user()->userSegSuc->codsuc;

        $alm = Almacen::where('codsuc_asoc','=',$sucursal)
        ->select('codalm')
        ->where('pfactur',true)
        ->first();

        $existencia = Caartalmubi::where('codart',$request["articulo"])
          ->where('exiact','>',0)
          ->where('codalm',$alm->codalm)
          ->first();
        //dd($request);
        if($request["inventario"]){
          foreach($request["inventario"] as $key => $item){
            if(
              $item["codart"] === $request["articulo"] && $item["talla"]  === $request["talla"]
              ){// SI EL ARTICULO A INSERTAR EXISTE EN EL GRID A GUARDAR
                if( $item["cantidad"] === $existencia->cantidad ||
                    $item["cantidad"] > $existencia->cantidad
                  ){//SI LA CANTIDAD ES IGUAL O EN CASO DE QUE SEA MAYOR
                  return response()->json([
                    'error' => true ,
                    'msg' => 'El articulo Excede la cantidad en existencia'
                    ]);
                }
              }
          }
        }

        if(!$existencia)return response()->json([
          'error' => true,
          'msg'   => 'Ya este articulo no existe!'
        ]);
        //$request["inventario"];
        $articulo = Caregart::where('codart','=',$request['articulo'])->first();
        return response()->json($articulo);
    }

    public function detallar($id){

        $menu = new HomeController();
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $factura = Fafactur::with('cliente','sucursales','articulos','pagos')->find($ID);
        $log = Logs_impresora_api::where('factura_sistema',$factura->reffac)->first();
        $noEncontrado = false;
        $ivaStatus = false;
        $monrec = 0;

        
        $monrec = $factura->monrecargo;
        


        if($log === null) $noEncontrado = true;

        return view('/facturacion/facturaDetalle')
        ->with('menus',$menu->retornarMenu())
        ->with('factura',$factura)
        ->with('noEncontrado',$noEncontrado)
        ->with('monrec',$monrec);
    }

/*    public function anular($id){
        $menu = new HomeController();
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID = (int)$sep[1];
        $factura = Fafactur::with('cliente','sucursales')->find($ID);
        $factura->status = 'N';
        $factura->save();

        return response()->json([
                'titulo' => 'Hecho se ha anulado'
            ]);
    }*/

    public function anular(Request $request){
        //return response()->json($request);
        $factura = Fafactur::where('reffac','=',$request['codfactura'])->first();

        if($factura->status == 'N'){
           return response()->json(["error" => "Factura se encuentra Anulada en sistema"]);
        }
        else{
       /* $this->sendFiscalAn($factura->reffac,$request->codcaja);*/

        $factura->status = 'N';
        $status = $factura->save();
        $not = new Fanotcre();
        $contnot = Fanotcre::withTrashed()->get()->count();

        $cor= ($contnot ? str_pad($contnot, 8, '0', STR_PAD_LEFT):'00000001');
        $not->reffac = $factura->reffac;
        $not->correl = $cor;
        $not->monto = $factura->monpre;
        $not->save();
        $suc = Auth::user()->userSegSuc->codsuc;

        $articulos = Faartfac::where('reffac','=',$factura->reffac)->get();
         /*   $almacenes = Almacen::where('codsuc_asoc','=',$suc)->get(); */
          foreach($articulos as $art){
            $codalm = $art->codalm;
              if($art->talla){
                $existencia = Caartalmubi::where('codalm','=',$codalm)->where('codart','=',$art->codart)->where('codtalla','=',$art->talla)->orderBy('id','DESC')->first();
                 }
              else{
                $existencia = Caartalmubi::where('codalm','=',$codalm)->where('codart','=',$art->codart)->orderBy('id','DESC')->first();
                }
             $cantEnInventario = $existencia->exiact;
             $cantSumar = $art->cantot;
             $cantEnInventario = $cantEnInventario+$cantSumar;
             $existencia->exiact = $cantEnInventario;
             $existencia->save();
          }//foreach
          /*   return response()->json(["exito" => "HECHO"]);*/
          $this->sendFiscalAn($factura->reffac,$request->codcaja);
       /* }*/
      } //else
    }//anular
    // }

/*    public function activar($id){
        $menu = new HomeController();
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID = (int)$sep[1];
        $factura = Fafactur::with('cliente','sucursales')->find($ID);
        $factura->status = 'A';
        $factura->save();

        return response()->json([
                'titulo' => 'Hecho se ha activado'
            ]);
    }*/

    public function filtro(Request $request){
            $menu = new HomeController();
            $filtro = $request->filtro;

            #dd($this->sucursalAu->sucursalAuth());

            $facturas = Fafactur::where('codsuc',$this->sucursalAu->sucursalAuth())->where('reffac','ilike','%'.$filtro.'%')->orderBy('reffac','DESC')
            /*->orWhere('codcli','ilike','%'.$filtro.'%')->orWhereHas('cliente',function($q) use ($filtro){
                $q->where('nompro','ilike','%'.$filtro.'%');
            })*/->paginate(8);
            #dd($facturas);
          return view('/facturacion/factura')->with('menus',$menu->retornarMenu())->with('facturas',$facturas);
    }

    public function getRecargo(Request $request){
             $recar = Farecarg::where('monrgo','=',$request->iva)->first();
             return response()->json($recar);
    }

    ///////////////REPORTES
    public function vistaReporte(){
        $menu = new HomeController();
        if(Auth::user()->role->nombre == 'ADMIN')$sucursales = Sucursal::get();
        else $sucursales = Sucursal::where('codsuc',Auth::user()->userSegSuc->codsuc)->get();
        $facturas = Fafactur::get();
        return view('/reportes/facturasReportes')->with('menus',$menu->retornarMenu())->with('sucursales',$sucursales)->with('facturas',$facturas);
    }

public function reporteFactura(Request $request){

  $status = $request['status'];
  $desde = $request['desde'];
  $hasta = $request['hasta'];
  $diames = $request['diames'];
  $caja = $request['caja'];

  switch ($diames) {
     case 'diario':
        $tipo = 'DIARIO '.$desde;
     break;
     case 'ayer':
         $tipo = 'DÍA ANTERIOR '.$desde;
     break;
     case 'semanal':
        $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
     break;
     case 'semant':
        $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
     break;
     case 'mensual':
         $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
     break;
     case 'mesant':
       $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
     break;
     case 'anual':
       $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
     break;
     case 'antyear':
       $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
     break;
     default:
       $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
     break;

 }

 if($request['factura1']){
   $creafac = Fafactur::where('reffac','=',$request['factura1'])->first();
 }
  try{
   if(!$request['factura1']){
      if(!$request->sucursal)throw new Exception("Debe seleccionar una sucursal");
   }
     if($request->sucursal =="TODAS"){
         $general = true;
     }
     else{
         $general = false;
         $codsuc = $request->sucursal;
     }
     if(!$general && !$request['caja']){
       throw new Exception("Debe seleccionar una caja");
     }
     else {

    }
 }
 catch(Exception $e){
     return redirect()->route('reportesFacturas')->with('error',$e->getMessage());
 }

///EVALUA LOS PERIODOS DE TIEMPO SELECCIONADOS Y SI SERA UNA CONSULTA GENERALIZADA O POR SUCURSAL
try{
    $cajas = Caja::wheredescaj($caja)->first();
        if($general){
         if($status == 'todas'){
           $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero','notaCredito')->whereDate('created_at','>=',$desde)->whereDate('created_at','<=',$hasta)->where('codsuc','=',$codsuc)->orderby('reffac')->where('caja','=',$caja)->get();
         }
         elseif($status == 'activa'){
           $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero')->where('status','=','A')->whereDate('created_at','>=',$desde)->whereDate('created_at','<=',$hasta)->where('codsuc','=',$codsuc)->orderby('reffac')->where('caja','=',$caja)->get();
         }
         else{
          $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero')->where('status','=','N')->whereDate('created_at','>=',$desde)->whereDate('created_at','<=',$hasta)->where('codsuc','=',$codsuc)->orderby('reffac')->where('caja','=',$caja)->get();
         }

        }
        else{
          
          $turno = null;
          if(isset($request->all()['turno']) === true) {
            $turno = $request->all()['turno'];
          }

         if($status == 'todas'){
          
          $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero','fargoarticulo')
                      ->where('is_fiscal',$cajas->is_fiscal)
                      ->where('fecfac','>=',$desde)
                      ->where('fecfac','<=',$hasta)
                      ->when(isset($request->all()['turno']) === true,function ($q) use ($turno){
                        switch ($turno) {
                          case 'vespertino':
                            $q->where('turnos',$turno);
                            break;
                          case 'matutino':
                            $q->where('turnos',$turno);
                            break;
                          case 'completo':
                           $q->where('turnos',$turno);
                           break;
                           
                           case 'general':     
                          
                            return ;
                           break;
                        }
                  })       
                     
                      ->orWhere('fecanu','>=',$desde)
                      ->where('fecanu','<=',$hasta)
                      ->where('is_fiscal',$cajas->is_fiscal)
                      ->when(isset($request->all()['turno']) === true,function ($q) use ($turno){
                        switch ($turno) {
                          case 'vespertino':
                            $q->where('turnos',$turno);
                            break;
                          case 'matutino':
                            $q->where('turnos',$turno);
                            break;
                          case 'completo':
                           $q->where('turnos',$turno);
                           break;
                           
                           case 'general':     
                          
                            return ;
                           break;
                        }
                     })    
                      ->whereCodsuc($codsuc)
                      ->wherecodcaj($cajas->id)
                      ->orderby('reffac')
                      ->get();


         }
         elseif($status == 'activa'){
          $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero')->where('status','=','A')->whereDate('created_at','>=',$desde)->whereDate('created_at','<=',$hasta)->orwhereDate('fecanu','>=',$desde)->whereDate('fecanu','<=',$hasta)->where('codsuc','=',$codsuc)->orderby('reffac')->where('caja','=',$caja)->get();

         }
         else{
          $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero')->where('status','=','N')->whereDate('created_at','>=',$desde)->whereDate('created_at','<=',$hasta)->orwhereDate('fecanu','>=',$desde)->whereDate('fecanu','<=',$hasta)->where('codsuc','=',$codsuc)->orderby('reffac')->where('caja','=',$caja)->get();

         }
         if($cajas->is_fiscal === "true"){
           
           $FacturasAnul = Fanotcre::where('fecnot','>=',$desde)
                           ->with('perteneceFactura.pagos')
                           ->where('fecnot','<=',$hasta)
                           ->get();
  
           $FacturasAnul = $FacturasAnul->map(function ($q)
           {
              return $q->perteneceFactura;
           });
           //cualquier dupla de correlativos poner unique
           $facturas = $facturas->merge($FacturasAnul)->sortBy('reffac');
         }
         
         
         
         

        }

     if($facturas->isEmpty()){
         Session::flash('error', 'No existen registros con los parametros de busqueda ingresados');
         return redirect()->route('reportesFacturas');}
 }//try
 catch(Exception $e){
     Session::flash('error', 'Se presentó un problema en la generación del reporte, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
     return redirect()->route('reportesFacturas');
 }
   $nomsucu = Sucursal::where('codsuc','=',$codsuc)->first()->nomsucu;
   $dirfis = Sucursal::where('codsuc','=',$codsuc)->first()->dirfis;
     $pdf = new generadorPDF();

     $titulo = 'REPORTE DE FACTURAS';
     $total = $facturas->count();
     $fecha = date('Y-m-d');

     $pdf->AddPage('L');
     $pdf->SetTitle('REPORTE DE FACTURAS');
     $pdf->SetFont('arial','B',16);
     $pdf->SetWidths(array(90,90));
     $pdf->Ln();
     $pdf->Cell(0,10,utf8_decode($titulo),0,0,'C');
     $pdf->Ln();
     $pdf->SetFont('arial','B',10);
     $pdf->Ln(5);
     $pdf->Cell(280,5,utf8_decode('TIENDA: '.$nomsucu),0,0,'C');
     $pdf->Ln(5);
     $pdf->Cell(280,5,utf8_decode('CAJA: '.$cajas->descaj),0,0,'C');
     $pdf->Ln(5);
     $pdf->Cell(280,5,utf8_decode('SUCURSAL: '.$dirfis),0,0,'C');
     $pdf->Ln(5);
     // $pdf->Cell(280,5,utf8_decode('DIRECCIÓN: AV BOLIVAR, MESETA DE GUARANAO, CALLE DE SERVICIOS Nº 4 GALPONES 4-17 y 4-18'),0,0,'C');
     $pdf->Ln(15);
     if($tipo == 'Intervalo'){
         $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo.' '.date("d-m-Y", strtotime($desde)).' - '.date("d-m-Y", strtotime($hasta))),0,0,'L');
     }
     else{
         $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo),0,0,'L');
     }
     $pdf->Ln(5);
     $pdf->Cell(0,0,utf8_decode('REPORTE EMITIDO: '.date("d-m-Y", strtotime($fecha))),0,0,'L');
     $pdf->Ln(5);
     $pdf->Cell(0,0,utf8_decode('TOTAL FACTURA: '.$total),0,0,'L');
     $pdf->Ln(2);

     $first = $facturas->first()->reffac;
     $last  = $facturas->last()->reffac;
     $pdf->Cell(280,5,utf8_decode('PRIMERA FACTURA: '.$first),0,1,'L');
     $pdf->Cell(280,5,utf8_decode('ULTIMA FACTURA:  '.$last),0,1,'L');
     if($turno !== null)
         $pdf->Cell(120,5,utf8_decode('TURNO: '.strtoupper($turno)),0,1,'L');

     $pdf->SetFont('arial','B',8,5);
     $pdf->SetFillColor(2,157,116);
     $pdf->Cell(27,8,utf8_decode('N° FACTURA'),1,0,'C');
     $pdf->Cell(20,8,utf8_decode('EMISIÓN'),1,0,'C');
     $pdf->Cell(48,8,utf8_decode('CLIENTE'),1,0,'C');
     $pdf->Cell(35,8,utf8_decode('FORMA DE PAGO'),1,0,'C');
     $pdf->Cell(20,8,utf8_decode('ANULADA'),1,0,'C');
     $pdf->Cell(25,8,utf8_decode('ESTATUS'),1,0,'C');
     $pdf->Cell(35,8,utf8_decode('SUCURSAL'),1,0,'C');
     $pdf->Cell(30,8,utf8_decode('MONTO'),1,0,'C');
     $pdf->Cell(35,8,utf8_decode('VENDEDOR'),1,0,'C');

     $pdf->Ln();
     $pdf->SetWidths(array(27,20,48,35,20,25,35,30,35));
     $pdf->SetAligns(['C','C','C','C','C','C','C']);
     $pdf->SetFont('arial','',8,5);



     $MontoTotal = collect();


     

     foreach ($facturas as $factura) {
       $pagos = '';
       //$descuentoPP = ($factura->mondesc !== 0) ? ($factura->monsubtotal-$factura->mondesc) : 0;
       foreach ($factura->pagos as $pago ) {
         $pagos .= ($pago->nomban === 'NINGUNO') ? 'EFECTIVO, ' : $pago->nomban.', ';
       }
       if($factura->status === 'N'){//facturas NC Y ANULADAS

         if($factura->fecfac === $factura->fecanu){

           $pdf->SetTextColor(0,0,0);
           $pdf->Row(array($factura->reffac,date("d-m-Y", strtotime($factura->fecfac)),utf8_decode($factura->cliente->nompro),
           utf8_decode($pagos),'',
           'A',$factura->sucursales->nomsucu ?? '',number_format($factura->fargoarticulo !== null ? $factura->monfac : $factura->monsubtotal,2,',','.'),utf8_decode($factura->cajero->nomuse)));
           $MontoTotal->push($factura->monfac);

           $pdf->SetTextColor(255,0,0);
           $pdf->Row(array($factura->reffac,date("d-m-Y", strtotime($factura->fecfac)),utf8_decode($factura->cliente->nompro),
           utf8_decode($pagos),utf8_decode($factura->fecanu),
           $factura->status,$factura->sucursales->nomsucu ?? '',number_format($factura->fargoarticulo !== null ? $factura->monfac : $factura->monsubtotal,2,',','.'),utf8_decode($factura->cajero->nomuse)));
           $MontoTotal->push($factura->monfac*-1);

          }else{
           if($factura->fecfac !== $factura->fecanu && $desde === $hasta){//en caso de se halla devuelto la factura y consulta un dia especifico
             if ($desde >= $factura->fecanu && $desde > $factura->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
               $pdf->SetTextColor(255,0,0);

               if($factura->notaCredito2 === null) $estatusFactura = 'ANUL';
               elseif ($factura->notaCredito2 !== null) $estatusFactura = 'NC';

               $pdf->Row(array($factura->reffac,date("d-m-Y", strtotime($factura->fecfac)),utf8_decode($factura->cliente->nompro),
               utf8_decode($pagos),utf8_decode($factura->fecanu),
               $factura->status,$factura->sucursales->nomsucu ?? '',number_format($factura->fargoarticulo !== null ? $factura->monfac : $factura->monsubtotal,2,',','.'),utf8_decode($factura->cajero->nomuse)));
               $MontoTotal->push($factura->monfac*-1 );

             }else if($desde <= $factura->fecfac){
               $pdf->SetTextColor(0,0,0);

               $pdf->Row(array($factura->reffac,date("d-m-Y", strtotime($factura->fecfac)),utf8_decode($factura->cliente->nompro),
               utf8_decode($pagos),'',
               'A',$factura->sucursales->nomsucu ?? '',number_format($factura->fargoarticulo !== null ? $factura->monfac : $factura->monsubtotal,2,',','.'),utf8_decode($factura->cajero->nomuse)));
               $MontoTotal->push($factura->monfac);
             }

          }else if($factura->fecfac !== $factura->fecanu && $desde !== $hasta){//en caso de se halla devuelto la factura y consulta en dias diferentes

           if ($desde <= $factura->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
             $pdf->SetTextColor(0,0,0);

             $pdf->Row(array($factura->reffac,date("d-m-Y", strtotime($factura->fecfac)),utf8_decode($factura->cliente->nompro),
             utf8_decode($pagos),'',
             'A',$factura->sucursales->nomsucu ?? '',number_format($factura->fargoarticulo !== null ? $factura->monfac : $factura->monsubtotal,2,',','.'),utf8_decode($factura->cajero->nomuse)));
             $MontoTotal->push($factura->monfac);

             /*$pdf->SetTextColor(255,0,0);
             $pdf->Row(array($factura->reffac,date("d-m-Y", strtotime($factura->fecfac)),utf8_decode($factura->cliente->nompro),
             utf8_decode($pagos),utf8_decode($factura->fecanu),
             $factura->status,$factura->sucursales->nomsucu ?? '',number_format($factura->monfac,2,',','.'),utf8_decode($factura->cajero->nomuse)));
             $MontoTotal->push($factura->monfac*-1);*/
             if($factura->fecfac <= $factura->fecanu){
              $pdf->SetTextColor(255,0,0);
              $pdf->Row(array($factura->reffac,date("d-m-Y", strtotime($factura->fecfac)),utf8_decode($factura->cliente->nompro),
              utf8_decode($pagos),utf8_decode($factura->fecanu),
              'A',$factura->sucursales->nomsucu ?? '',number_format($factura->monfac,2,',','.'),utf8_decode($factura->cajero->nomuse)));
              $MontoTotal->push($factura->monfac*-1);
             }
              }elseif($factura->fecfac <= $factura->fecanu){
                $pdf->SetTextColor(255,0,0);
                $pdf->Row(array($factura->reffac,date("d-m-Y", strtotime($factura->fecfac)),utf8_decode($factura->cliente->nompro),
                utf8_decode($pagos),utf8_decode($factura->fecanu),
                'A',$factura->sucursales->nomsucu ?? '',number_format($factura->monfac,2,',','.'),utf8_decode($factura->cajero->nomuse)));
                $MontoTotal->push($factura->monfac*-1);
  
              }
           }
          }
       }else{//FACTURA ACTIVAS
        
         $pdf->SetTextColor(0,0,0);
         $pdf->Row(array($factura->reffac,date("d-m-Y", strtotime($factura->fecfac)),utf8_decode($factura->cliente->nompro),
         utf8_decode($pagos),'',
         'A',$factura->sucursales->nomsucu ?? '',number_format($factura->monfac,2,',','.'),utf8_decode($factura->cajero->nomuse)));

         $MontoTotal->push($factura->monfac);
       }

     }

    # dd($MontoTotal);
     $pdf->SetTextColor(0,0,0);
     $pdf->Ln(5);
     $pdf->SetFont('arial','B',10,5);
     $pdf->SetFillColor(2,157,116);
     $pdf->SetTextColor(0,0,0);
     $pdf->Cell(0,0,utf8_decode('MONTO TOTAL: '.number_format($MontoTotal->sum(),2,',','.')),0,0,'R');
     $pdf->Output('I','Reporte de Facturas-'.$fecha.'pdf');

    exit;
       }


//lo hizo hendrix
       public function getCajas(Request $request){


          $cajaU = cajaUser::whereNull('deleted_at')->with('caja')->where('loguse',Auth::user()->loguse)->get();

          $string = '<option disabled selected="selected" value="0">Eliges Una caja asociadas</option>';
          foreach ($cajaU as $key) {
            $string .= '<option value="'.$key->caja->id.'">'.$key->caja->descaj.'</option>';
          }

          return response()->json([

            'cajas' => !$cajaU->isEmpty() ? $string : null

          ]);

       }



       public function getExistenciaArt(Request $request){
        $cantidad = $request['cantidad'];
       /* $almacen = $request['almacen'];*/
        $articulo = $request['articulo'];
        $codtalla = $request['talla'];
        $sucursal = Auth::user()->userSegSuc->codsuc;
        $ip = request()->ip() === '::1' ? '127.0.0.1' : request()->ip();

        $user =cajaUser::where('loguse',Auth::user()->loguse)->with('caja')->whereHas('caja',function($q) use ($ip)
        {
          $q->where('impfishost',$ip);
        })->first();


        $almacenes = Almacen::where('codalm','=',$user->caja->codalm)->where('pfactur',true)->get();
        $mayor = 0;
        $Canexistencia = 0;

        foreach($almacenes as $almacen){

         if($codtalla){
           $cantidades = Caartalmubi::where('codart','=',$articulo)->where('codalm','=',$almacen->codalm)->where('codtalla','=',$codtalla)->where('exiact','>',0)->sum('exiact');
         }
         else{
           $cantidades = Caartalmubi::where('codart','=',$articulo)->where('codalm','=',$almacen->codalm)->where('exiact','>',0)->sum('exiact');
           
         }


           if(intval($cantidades) === 0){

           }
           else{
          /*  return gettype($arts);*/
              // foreach($cantidades as $can){
              // /* $talla = Tallas::where('codtallas','=',$art->codtalla)->get();
              //  array_push($tallas, $talla);*/
              //  if($can->exiact > $mayor)
              //   $Canexistencia = $can->exiact;
              // }//foreach
                break;
           }//if
        }//foreach

        $Canexistencia = intval($cantidades);
         return response()->json($Canexistencia);

/*
        $codalm = Almacen::where('nomalm','=',$almacen)->first()->codalm;*/
/*
        $Canexistencia = Caartalmubi::where('codart','=',$articulo)->where('codalm','=',$codalm)->where('codtalla','=',$codtalla)->get();
        return response()->json($Canexistencia);
        */
       }

       public function buscarFactura(Request $request){
        $factura = Fafactur::with('cliente')->where('reffac','ilike','%'.$request['codfactura'].'%')->where('status','=','A')->get();
         return response()->json($factura);
       }

        public function buscarPresupuesto(Request $request){
        $presupuesto = Fapresup::with('cliente')->where('codpre','ilike','%'.$request['codpresupuesto'].'%')->where('facturado',false)->where('estatus','=','APROBADO')->get();
         return response()->json($presupuesto);
       }

        public function seleccionarPre(Request $request){
          $presupuesto = Fapresup::with('cliente','articulos','recargo')->where('codpre','=',$request['codpresupuesto'])->first();
        /*  return $presupuesto;*/

          $length = $presupuesto->articulos->count();
          $i = 0;

          while($i < $length){
               $presupuesto->articulos[$i]->setAttribute('mondescuento','');
               if($presupuesto->articulos[$i]->descuento_id){
                $codd = Fadescto::where('codigoid','=',$presupuesto->articulos[$i]->descuento_id)->first();
                $presupuesto->articulos[$i]->mondescuento = $codd->mondesc;
               }
               else{
                $presupuesto->articulos[$i]->mondescuento = 0;
               }
     /*          dd($codd);*/

               $i++;
          }
          return response()->json($presupuesto);
       }

       public function getEscala(Request $request){
          $codart = $request['articulo'];
          $talla = $request['talla'];

          $codesc = Fadefesc::where('codart','=',$codart)->first()->codesc;
          $escala = Fadetesc::where('codesc','=',$codesc)->where('codtallas','=',$talla)->first();
          return response()->json($escala);

       }

        public function getEscalas(Request $request){
          $codart = $request['articulo'];
          $talla = $request['talla'];
          /*$codesc = Fadetesc::where('codtallas','=',$talla)->first()->codigoid;*/
/*          return $escala = Fadefesc::with('escalastallas')->get();*/

          $escala = Fadefesc::where('codart','=',$codart)->whereHas('esctalla',function($q) use ($talla){
          $q->where('codtallas','=',$talla);
        })->first();
          if($escala)return response()->json(['exito' => $escala]);
          else return response()->json(['error']);

       }

         public function cantEscala(Request $request){
          $codart = $request['articulo'];
          $codesc = $request['escala'];
          $talla = $request['talla'];

          if($talla){
            $cant= Fadetesc::where('codesc','=',$codesc)->where('codtallas','=',$talla)->first();
          }
          else{
            $cant= Fadetesc::where('codesc','=',$codesc)->orderby('id','DESC')->first();
          }

          return response()->json($cant);

       }

       public function getMoneda(Request $request){
        $codmoneda = $request['codmoneda'];
        $moneda = Famoneda::where('codigoid','=',$codmoneda)->first();
        return response()->json($moneda);
       }

       public function getTasaMoneda(Request $request){
        $codmoneda = $request['codmoneda'];
        /*$codart = $request['codart'];*/
        $moneda = Famoneda::where('activo',true)->first();//cambio de moneda reconversion

        //dd($moneda);
   /*     $moneda = faartpvp::where('codart','=',$codart)->where('status','=','A')->first();*/

        $nommoneda = Famoneda::where('codigoid','=',$moneda->codigoid)->first();
        $arreglo = [];
        $tasa = Fatasacamb::where('id_moneda2',$moneda->codigoid)
        ->where('id_moneda',$codmoneda)
        ->where('activo',true)->first();
  /*      dd($tasa);*/


        array_push($arreglo, $tasa);
        array_push($arreglo, $nommoneda);
       /* dd($tasa);*/

        if($tasa != null){
         return response()->json(['exito' => $arreglo]);
        }
        else{
          return response()->json('error');
        }

       }


       public function getEscalaST(Request $request){
        $codart = $request['articulo'];
        $sucursal = Auth::user()->userSegSuc->codsuc;
        $almacenes = Almacen::where('codsuc_asoc','=',$sucursal)->get();
        $tallas = [];
        foreach($almacenes as $almacen){

           $arts = Caartalmubi::where('codalm','=',$almacen->codalm)->where('codart','=',$codart)->where('exiact','>',0)->get();
           if($arts->isEmpty() || $arts == null){

           }
           else{
              foreach($arts as $art){
               $talla = Tallas::where('codtallas','=',$art->codtalla)->get();
               array_push($tallas, $talla);
              }

           }//if
        }//foreach

        if($tallas->count() < 1){
          $escalas = Fadefesc::where('codart','=',$codart)->get();
          if(!$escala->isEmpty())return response()->json(['exito' => $escalas]);
          else return response()->json(['error']);
        }
       }

       public function getColores(Request $request){
          $art = $request['art'];
    /*      $artcolors = Caregart::where('codart',$art)->with('colores')->get();
          return response()->json($artcolors);*/
          $colors = ColorPro::has('articulos')->whereHas('articulos',function($q) use ($art){
            $q->where('codart',$art);
          })->get();
          if(!$colors->isEmpty())return response()->json(['exito'=> $colors]);
          else return response()->json(['error']);
       }


      public function buscarNota(Request $request){
       /* dd($request->codnota);*/
        $nota = NotaEntrega::with('cliente')->where('numero','ilike','%'.$request['codnota'].'%')->get();
         return response()->json($nota);
       }

        public function seleccionarNota(Request $request){
          /*dd('aca');*/
          $nota = NotaEntrega::with('cliente','articulos')->where('numero','=',$request['codnota'])->first();
          return response()->json($nota);
       }


       public function getCierreCajaView(){

          $menu = new HomeController();
           if(Auth::user()->role->nombre == 'ADMIN')$cajas = Caja::get();
           else $cajas = Caja::where('codsuc',Auth::user()->userSegSuc->codsuc)->get();
          
          return view('reportes.cierreCajaReportes')
          ->with('menus',$menu->retornarMenu())->with('cajas',$cajas);
       }

       public function getCierreCaja(Request $request){


        $error = $this->cierre->getCierreCajaR($request);
        
        if($error){
          Session::flash('warning','No existen datos que mostrar');
          return redirect()->route('CierreCajaView');
        }

       }



      public function anular2(Request $request,$id){

        $menu = new HomeController();
        $desc = Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID = (int)$sep[1];

        $factura = Fafactur::find($ID);
        $cajaO = Caja::where('id','=',$factura->codcaj)->first();
        if($factura->status == 'N'){
           return response()->json(["error" => "Factura se encuentra Anulada en sistema"]);
        }
        else{

          $seriales = Serial::where('factura',$factura->reffac)->get();

          $seriales2 = $seriales->map(function ($q){
            return $q->only(['serial','codart']);
          });



          $serialesCpt = Serial::where('factura',$factura->reffac)->update(['deleted_at' => Carbon::now()]);

          if($seriales2->isNotEmpty()) {

            foreach ($seriales2 as $key => $value) {

              serialDisponible::updateOrCreate([
                'seriales' => $value['serial'],
                'codart'   => $value['codart']
              ],['disponible' => true]);

            }
          }



        $factura->num_fiscal = str_replace($cajaO->id.'F',str_pad("0",3,0,STR_PAD_LEFT),$factura->reffac);
        $factura->status = 'N';
        $factura->refanu = $factura->reffac;
        $factura->cierre = true;
        $factura->fecanu = Carbon::now()->format('Y-m-d');
        $factura->motanu = $request->all()['desc'];

        if($cajaO->is_fiscal === "true")//cuando es fiscal
          $cajaO->corfac = str_replace($cajaO->id.'F',str_pad("0",3,0,STR_PAD_LEFT),$factura->reffac);
        //else
          //$cajaO->corfac = str_replace($cajaO->id.'T',str_pad("0",3,0,STR_PAD_LEFT),$factura->reffac);

        $cajaO->save();
        $status = $factura->save();
        // $not = new Fanotcre();
        // $contnot = Fanotcre::withTrashed()->get()->count();

        // $cor= ($contnot ? str_pad($contnot, 8, '0', STR_PAD_LEFT):'00000001');

        // $not->reffac = $factura->reffac;
        // $not->correl = $cor;
        // $not->monto = $factura->monpre;
        // $not->save();

            $suc = Auth::user()->userSegSuc->codsuc;
            $articulos = Faartfac::where('reffac','=',$factura->reffac)->get();

         /*   $almacenes = Almacen::where('codsuc_asoc','=',$suc)->get(); */

          $existencia = '';

          foreach($articulos as $art){
          $codalm = $art->codalm;



            if($art->talla){
              $existencia = Caartalmubi::where('codalm','=',$codalm)->where('codart','=',$art->codart)->where('codtalla','=',$art->talla)->orderBy('id','DESC')->first();
               }
            else{
              $existencia = Caartalmubi::/*where('codalm','=',$codalm)->*/where('codart','=',$art->codart)->orderBy('id','DESC')->first();
              }

              $cantEnInventario = $existencia->exiact;
              $cantSumar = $art->cantot;
              $cantEnInventario = $cantEnInventario+$cantSumar;
              $existencia->exiact = $cantEnInventario;
              $existencia->save();

          }//foreach




          } //else
        }//anular


        

    public function printFactura(Request $request)
    {
      try{
        $success = false;
        if($request->isFiscal === "true"){
          $success = $this->fiscalRepoApi->printFiscal($request->reffac, $request->caja);
          //$success = $this->fiscalRepo->printFiscal($request->reffac, $request->caja);
        }else if($request->isFiscal === "false"){

          $success = $this->posApi->sendRequest($request->reffac, $request->caja);
          
          //$success = $this->posPrinter->printInfo($request->reffac, $request->caja,$request->isFiscal);
          
        }
        
        return $success === 200 || $success === true ?
        response()->json(['success' => true], 200) :
        response()->json(['error' => true,'msg' => $success["msg"]], $success["errorCode"]);

      }catch(\Exception $e){
        return response()->json([
          'error' => true,
          'msg'   =>  $e->getMessage()
        ],500);
      }
    }

    public function devFiscal(Request $request)
    {
      try{

        $cajaId = Caja::where('descaj', $request->codcaja)->first();
        $suc = Auth::user()->userSegSuc->codsuc;

        if(!$cajaId) throw new Exception ("No existe caja para devolver", 500);

        $success = $this->fiscalRepoApi->printDevolucion($request->codfactura, $cajaId->id);


        if($success === true){

          $this->createNotaCredito($request->codfactura,$cajaId);
          $articulos = Faartfac::where('reffac','=',$request->codfactura)->get();
          $almacenes = Almacen::where('codsuc_asoc','=',$suc)->where('pfactur',true)->get();

          foreach($articulos as $art){

            foreach($almacenes as $almacen){


            /*  if($art->talla){
               $existencia = Caartalmubi::where('codalm','=',$almacen->codalm)
               ->where('codart','=',$art->codart)
               ->where('codtalla','=',$art->talla)
               ->where('exiact','>',0)
               ->first();
              if($existencia){
                $codalm = $almacen->codalm;
              break;
              }
            }//if talla*/
             //else{
             $existencia = Caartalmubi::where('codalm','=',$almacen->codalm)
             ->where('codart','=',$art->codart)
             ->where('exiact','>=',0)
             ->first();
          #   dd($existencia);
              if($existencia){
                $codalm = $almacen->codalm;
              break;
              }
            //}//else
          }//foreach

            //->update(['deleted_at' => Carbon::now()]);
            $serialD = Serial::where('factura',$request->codfactura)->select('serial')->get(); //DEVUELVE EL SERIAL DISPONIBLE EN INVENTARIO(DEVOLUCION)

            if($serialD->isNotEmpty()){
              foreach ($serialD as $key => $value) {

                $seriales[] = $value->serial;
              }

              Serial::where('factura',$request->codfactura)
              ->update([
                'deleted_at' => Carbon::now()
              ]);

                serialDisponible::whereIn('seriales',$seriales)
                                  ->update([
                                            'disponible' => true
                                          ]);

              }


            if(isset($existencia)){
              $cantEnInventario = $existencia->exiact;
              $cantSustraer = $art->cantot;
              $cantEnInventario = $cantEnInventario+$cantSustraer;
              $existencia->exiact = $cantEnInventario;
              $existencia->save();
              $aral = Faartfac::where('reffac','=',$art->reffac)
                      ->where('codart','=',$art->codart)->first();
              $aral->codalm = isset($codalm);
              $aral->save();
            }

           }
        }
        return $success === true ?
        response()->json(['success' => true], 200) :
        response()->json(['error' => $success["msg"],'msg' => $success["msg"], $success["errorCode"]]);

      }catch(\Exception $e){

        return response()->json([
          'error' => true,
          'msg'   =>  $e->getMessage()
        ],500);
      }
    }

    public function createNotaCredito($factura = null,$cajaId)
    {
      if($factura === null){
        return false;
      }
      $factura = Fafactur::where('reffac',$factura)->first();


     // $not = new Fanotcre();
      //$contnot = Fanotcre::withTrashed()->first()->orderby('id','DESC')->correl;
      //$contnot = Fafactur::withTrashed()->where('reffac',$factura->reffac)->where('codcaj',$cajaId->id)->first()->num_dev_fiscal;//codigo generado en

      $caja = Caja::find($cajaId->id);
      $cor = 0;
      /*if($contnot === null){
         $cor = '00000804';//esto es la siguiente nota de credito genrada
      }else{
      }*/
      $cor= str_pad($caja->cornot, 8, '0', STR_PAD_LEFT);

        Fanotcre::create([
          'reffac' => $factura->reffac,
          'correl' => $cor,
          'fecnot' => Carbon::now()->format('Y-m-d'),
          'monto'  => $factura->monpre
        ]);

      //$not->reffac = $factura->reffac;
      //$not->correl = $cor;
      //$not->fecnot = Carbon::now()->format('Y-m-d');
     // $not->monto  = $factura->monpre;
      $factura->refanu = "NC".substr($cor,-6);
      $factura->fecanu = Carbon::now()->format('Y-m-d');
      //$not->save();
      $factura->save();

      return true;
    }

      public function registrarCuenta($factura,$articulos){
/*
      dd($articulos);*/

       DB::transaction(function () use ($factura, $articulos) {

          $existe = Contabc::whereYear('created_at', date('Y'))->whereMonth('created_at',date('m'))->get();

          $contador = $existe->count() + 1;

          if($contador < 9){
            $correlativo = '00000'.$contador;
          }

          elseif($contador > 9 && $contador < 99){
            $correlativo = '0000'.$contador;
          }
          elseif($contador > 99 && $contador < 999){
            $correlativo = '000'.$contador;
          }
          elseif($contador > 999 && $contador < 9999){
            $correlativo = '00'.$contador;
          }
          elseif($contador > 9999 && $contador < 99999){
            $correlativo = '0'.$contador;
          }
          else $correlativo = $contador;


          $contabc = new Contabc();
          $year = date('Y');
          $year2 = substr( $year, -2);
          $month = date('m');
          $numcom = $year2.$month.$correlativo;

          $contabc->numcom = $numcom;
          $contabc->feccom = date('Y-m-d');
          $contabc->descom = $factura->desfac;
          $contabc->stacom = 'D';
          $contabc->tipcom = '';
          $contabc->reftra = $factura->reffac;
          $contabc->loguse = Auth::User()->nomuse;

          $tasa = Fatasacamb::where('codigoid',$factura->cod_recargo)->first();
          $contabc->codtasa = $tasa->codigoid;
          $contabc->tasa = $tasa->valor;

          /*$moncom = $factura->monsubtotal * $tasa->valor;*/
          $contabc->moncom = $factura->monfac;

          $contabc->save();

          $cliente = Facliente::with('cuentaContable')->where('codpro',$factura->codcli)->first();


          ///asiento contable de la cuenta del cliente

          $contabc1cliente = new Contabc1();
          $contabc1cliente->numcom = $numcom;
          $contabc1cliente->feccom = date('Y-m-d');
          $contabc1cliente->debcre = $cliente->cuentaContable->debcre;
          $contabc1cliente->codcta = $cliente->cuentaContable->codcta;
          $contabc1cliente->refasi = $factura->reffac;
          $contabc1cliente->desasi = $factura->desfac;
          $contabc1cliente->monasi = $factura->monsubtotal;
          $contabc1cliente->monasimone = $factura->monrecargo;
          $contabc1cliente->save();

          ///asientos contables para las cuentas de los articulos

          foreach ($articulos as $articulo) {

            $art = Caregart::with('cuentaContable')->where('codart',$articulo->codart)->first();

              $contabc1 = new Contabc1();
              $contabc1->numcom = $numcom;
              $contabc1->feccom = date('Y-m-d');
              if($art->cuentaContable)$contabc1->debcre = $art->cuentaContable->debcre;
              $contabc1->codcta = $art->codcta;
              $contabc1->refasi = $factura->reffac;
              $contabbart = Contabb::where('codcta',$art->codcta)->first();
              $contabc1->desasi = $contabbart->descta;
              $contabc1->monasi = $articulo->totart;
              $contabc1->monasimone = $articulo->monrgo;
              $contabc1->save();


              //asiento para la cuenta contable del iva

              if($articulo->recargoAr){
                    $monrgo = $articulo->recargoiva->monrgo;
                    $contabc1iva = new Contabc1();
                    $recargo = Farecarg::where('codigoid',$factura->cod_recargo)->first();
                    $iva = Contabb::where('descta',$recargo->descta)->first();
                    $contabc1iva->numcom = $numcom;
                    $contabc1iva->feccom = date('Y-m-d');
                    $contabc1iva->debcre = $iva->debcre;
                    $contabc1iva->codcta = $iva->codcta;
                    $contabc1iva->refasi = $nota->nronot;
                    $contabc1iva->desasi = $iva->descta;
                    $contabc1iva->monasi = $monrgo;
                    $contabc1iva->monasimone = $contabc1iva->monasi * $tasa->cambio;
                    $contabc1iva->save();
              }

          } //foreach

       });

    }

    public function seriales(Request $request){

        $seriales = Serial::withTrashed()->where('factura',$request->factura)->where('codart',$request->codart)->get();

        if($seriales->isEmpty() === false){
          $cpt = collect([]);
          foreach($seriales as $serial){

            $cpt->push(["serial" => $serial->serial,"facturado" => $serial->deleted_at]);

          }
        }else{
           return ['seriales'=>'NO TIENE SERIALES REGISTRADO!'];
        }

        return ['seriales' => $cpt];
    }

    function search(Request $request)
    {
       $serialRegiFac = Serial::where('serial','ilike','%'.$request->serial.'%')
                        ->whereHas('getFactura')
                        ->whereHas('getArticulo')
                        ->with('getFactura','getArticulo')
                        ->first();


        if($serialRegiFac !== null){
            return [
              'factura' => $serialRegiFac->getFactura->reffac,
              'articuloFac' => ['descrip' => $serialRegiFac->getArticulo->desart,
                                'codigo' => $serialRegiFac->getArticulo->codart
                               ],
               'serial' => $serialRegiFac->serial,
               'status' => 500
          ];
        }else{
           return ['status' => 200];
        }


    }

    public function buscarCajas1(Request $request){

         $sucursal = $request['sucursal'];
         if($sucursal != 'TODAS' ){
           $cajas = Caja::where('codsuc','=',$sucursal)->get();
           return response()->json($cajas);
         }

       }
    public function DetallesFactura()
    {
      $menus = new HomeController();
      $menus = $menus->retornarMenu();
      $cajas = \App\Models\Caja::all();
      return view('reportes.detallesFactura',compact('cajas','menus'));
    }


    public function generarPdfDetalles(Request $request)
    {

      $status = $request['status'];
      $desde = $request['desde'];
      $hasta = $request['hasta'];
      $diames = $request['diames'];
      $caja = $request['caja'];



     try{
      switch ($diames) {
        case 'diario':
           $tipo = 'DIARIO '.$desde;
        break;
        case 'ayer':
            $tipo = 'DÍA ANTERIOR '.$desde;
        break;
        case 'semanal':
           $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
        break;
        case 'semant':
           $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
        break;
        case 'mensual':
            $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
        break;
        case 'mesant':
          $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
        break;
        case 'anual':
          $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
        break;
        case 'antyear':
          $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
        break;
        default:

         if($desde === null) throw new Exception("Selecciona la fecha");

          $fc=explode(' - ', $desde);
          list($y,$m,$d) = explode("-",$fc[0]);
          $desde1=$d.'-'.$m.'-'.$y;
          $fch=explode(' - ', $hasta);
          list($y,$m,$d) = explode("-",$fch[0]);
          $hasta1=$d.'-'.$m.'-'.$y;
          $tipo = 'INTERVALO ENTRE '.$desde1.' HASTA '.$hasta1;
        break;

    }
      if(!$request['caja']){
        throw new Exception("Debe seleccionar una caja");
      }
      else { $caja = $request['caja']; }

      $cajas = Caja::where('descaj',$request->caja)->first();
      
      $codsuc = session('codsuc');
      
      
      
      $turno = null;
      if(isset($request->all()['turno']) === true) {
        $turno = $request->all()['turno'];
      }

      /* */
      $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero',
                        'notaCredito','fargoarticulo')
                        ->where('is_fiscal',$cajas->is_fiscal)
                        ->where('codsuc',$codsuc)
                        ->where('fecfac','>=',$desde)
                        ->where('fecfac','<=',$hasta)
                        ->when(isset($request->all()['turno']) === true,function ($q) use ($turno){
                          switch ($turno) {
                            case 'vespertino':
                              $q->where('turnos',$turno);
                              break;
                            case 'matutino':
                              $q->where('turnos',$turno);
                              break;
                            case 'completo':
                             $q->where('turnos',$turno);
                             break;
                             
                             case 'general':     
                          
                              return ;
                             break;
                          }
                       })        
                        ->orWhere('fecanu','>=',$desde)
                        ->where('fecanu','<=',$hasta)
                        ->where('is_fiscal',$cajas->is_fiscal)
                        ->when(isset($request->all()['turno']) === true,function ($q) use ($turno){
                          switch ($turno) {
                            case 'vespertino':
                              $q->where('turnos',$turno);
                              break;
                            case 'matutino':
                              $q->where('turnos',$turno);
                              break;
                            case 'completo':
                             $q->where('turnos',$turno);
                             break;
                             
                             case 'general':     
                          
                              return ;
                             break;
                          }
                       })        
                        ->wherecodcaj($cajas->id)                    
                    ->orderBy('reffac','ASC')
                    ->get();

       if($facturas->isEmpty()) throw new Exception("No Existen Registro");


      } catch(Exception $e){
          return redirect()->route('LDetalloF')->with('error',$e->getMessage());
      }






$pdf = new generadorPDF();

$titulo = 'LISTADOS DETALLADO DE FACTURA';
//$total = $facturas->count();
$fecha = date('d-m-Y');


$pdf->AddPage('L');

$pdf->SetFont('arial','B',16);
$pdf->SetWidths(array(90,90));
$pdf->Ln(4);
$pdf->Cell(0,22,utf8_decode($titulo),0,0,'C');
$pdf->Ln(10);
$pdf->SetFont('arial','B',12);
$pdf->cell(0,22,utf8_decode($tipo),0,0,'C');
$pdf->Ln(20);
$first = $facturas->first()->reffac;
$last  = $facturas->last()->reffac;
$pdf->Cell(280,5,utf8_decode('PRIMERA FACTURA: '.$first),0,1,'L');
$pdf->Cell(280,5,utf8_decode('ULTIMA FACTURA:  '.$last),0,1,'L');

if($turno !== null)
   $pdf->Cell(120,5,utf8_decode('TURNO: '.strtoupper($turno)),0,1,'L');

$totalGeneral = collect();
$restSubtotal = collect();
$cantidad = 0;
$resCantidad = 0;
$recargoGeneral = collect();
$restRecargo = collect();
$montoTotalG = collect();
$restMontoTotal = collect();
$descuentoSum = 0;
$estatusFactura = '';
$resDescuentoP = 0;

foreach ($facturas as $factura) {

  if($factura->status === 'N'){//facturas NC Y ANULADAS

    if($factura->fecfac === $factura->fecanu){
      $color = 0;

      $this->InsertPdf($factura,$estatusFactura = 'ACTIVO',$pdf,$color,$cantidad,$resCantidad,$desde,$hasta,$descuentoSum,$resDescuentoP);
      $totalGeneral->push($factura->monfac);
      $recargoGeneral->push($factura->monrecargo);
      $montoTotalG->push($factura->monfac);
      $pdf->AddPage('L');

     if($factura->notaCredito2 === null) $estatusFactura = 'ANULADA';
     elseif ($factura->notaCredito2 !== null) $estatusFactura = 'NOTAS CREDITO';

     $color = 255;

     $this->InsertPdf($factura,$estatusFactura,$pdf,$color,$cantidad,$resCantidad,$desde,$hasta,$descuentoSum,$resDescuentoP);
     $totalGeneral->push($factura->monfac*-1);
     $restRecargo->push($factura->monrecargo*-1);
     $restMontoTotal->push($factura->monfac*-1);
     $pdf->AddPage('L');

    }else{
      if($factura->fecfac !== $factura->fecanu && $desde === $hasta){//en caso de se halla devuelto la factura y consulta un dia especifico
        if ($desde >= $factura->fecanu) {//cuando se consulta la factura que sea mayor o igual del dia

          $color = 255;
          if($factura->notaCredito2 === null) $estatusFactura = 'ANULADA';
          elseif ($factura->notaCredito2 !== null) $estatusFactura = 'NOTAS CREDITO';
                $this->InsertPdf($factura,$estatusFactura,$pdf,$color,$cantidad,$resCantidad,$desde,$hasta,$descuentoSum,$resDescuentoP);
                $totalGeneral->push($factura->monfac*-1);
                $restRecargo->push($factura->monrecargo*-1);
                $restMontoTotal->push($factura->monfac*-1);
                $pdf->AddPage('L');

                }else if($desde <= $factura->fecfac){

                 $color = 0;
                 $this->InsertPdf($factura,$estatusFactura='ACTIVO',$pdf,$color,$cantidad,$resCantidad,$desde,$hasta,$descuentoSum,$resDescuentoP);
                 $totalGeneral->push($factura->monfac);
                 $recargoGeneral->push($factura->monrecargo);
                 $montoTotalG->push($factura->monfac);
                 $pdf->AddPage('L');

                }

              }else if($factura->fecfac !== $factura->fecanu && $desde !== $hasta){//en caso de se halla devuelto la factura y consulta en dias diferentes
                #dd($factura->reffac,$factura->fecfac,$factura->fecanu);

             if ($desde <= $factura->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia

              $color = 0;
              $this->InsertPdf($factura,$estatusFactura='ACTIVO',$pdf,$color,$cantidad,$resCantidad,$desde,$hasta,$descuentoSum,$resDescuentoP);
              $totalGeneral->push($factura->monfac);
              $recargoGeneral->push($factura->monrecargo);
              $montoTotalG->push($factura->monfac);
              $pdf->AddPage('L');

               if($factura->fecanu >= $desde && $factura->fecanu <= $hasta){

                 if($factura->notaCredito2 === null) $estatusFactura = 'ANULADA';
                elseif ($factura->notaCredito2 !== null) $estatusFactura = 'NOTAS CREDITO';

                $color = 255;

                $this->InsertPdf($factura,$estatusFactura,$pdf,$color,$cantidad,$resCantidad,$desde,$hasta,$descuentoSum,$resDescuentoP);
                $restSubtotal->push($factura->monsubtotal*-1);
                $restRecargo->push($factura->monrecargo*-1);
                $restMontoTotal->push($factura->monfac*-1);
                $pdf->AddPage('L');

               }

             }else if($desde <= $factura->fecfac){
               $color = 0;

               $this->InsertPdf($factura,$estatusFactura='ACTIVO',$pdf,$color,$cantidad,$resCantidad,$desde,$hasta,$descuentoSum,$resDescuentoP);
               $totalGeneral->push($factura->monfac*-1);
               $recargoGeneral->push($factura->monrecargo);
               $montoTotalG->push($factura->monfac);
               $pdf->AddPage('L');

              }else if($factura->fecfac <= $factura->fecanu){


                $color = 255;
              if($factura->notaCredito2 === null) $estatusFactura = 'ANULADA';
              elseif ($factura->notaCredito2 !== null) $estatusFactura = 'NOTAS CREDITO';
              $this->InsertPdf($factura,$factura->status,$pdf,$color,$cantidad,$resCantidad,$desde,$hasta,$descuentoSum,$resDescuentoP);
              $restSubtotal->push($factura->monsubtotal*-1);
              $restRecargo->push($factura->monrecargo*-1);
              $restMontoTotal->push($factura->monfac*-1);
              $pdf->AddPage('L');

             }

          }

    }

  }else{//FACTURA ACTIVAS

    $color = 0;
    $this->InsertPdf($factura,$factura->status='ACTIVO',$pdf,$color,$cantidad,$resCantidad,$desde,$hasta,$descuentoSum,$resDescuentoP);
    $totalGeneral->push($factura->monfac);
    $recargoGeneral->push($factura->monrecargo);
    $montoTotalG->push($factura->monfac);
    $pdf->AddPage('L');


 }


}

$ivaGeneral = (collect($facturas)->last()->fargoarticulo !== null) ? collect($facturas)->last()->porrecargo : 0;

$pdf->Ln(5);
$pdf->SetFont('arial','B',10);
$pdf->SetTextColor(0,0,0);

$pdf->cell(50,6,utf8_decode("Sub Total: ".number_format($totalGeneral->sum()+$restSubtotal->sum(),2,',','.')),0,0,'L');
$pdf->cell(50,6,utf8_decode("Cantidad: ".number_format($cantidad+$resCantidad,2,',','.')),0,0,'L');
$pdf->cell(50,6,utf8_decode("Descuento: ".number_format($descuentoSum,2,',','.')),0,0,'L');
$pdf->cell(65,6,utf8_decode("Recargo({$ivaGeneral}%): ".number_format($recargoGeneral->sum()+$restRecargo->sum(),2,',','.')),0,0,'L');
$pdf->cell(50,6,utf8_decode("Monto Total: ".number_format($montoTotalG->sum()+$restMontoTotal->sum(),2,',','.')),0,0,'L');

$pdf->Output('I','LIBRO DE VENTAS-'.$fecha.'pdf');

exit;

}

  public function arqueoCaja()
  {
    $factura = Fafactur::whereDate('created_at','>=',Carbon::now()->format('Y-m-d'))->whereDate('created_at','<=',Carbon::now()->format('Y-m-d'))
                         ->where('cierre',null)->sum('monfac');


    $devoluciones = Fafactur::whereDate('fecanu','>=',Carbon::now()->format('Y-m-d'))->whereDate('fecanu','<=',Carbon::now()->format('Y-m-d'))
    ->where('status','N')->get();



    $devoluciones = $devoluciones->sum(function($val)
    {
      return $val->monfac;
    });

    return [
            'total' => $factura,
            'devoluciones' => $devoluciones
          ];
  }

  public function InsertPdf($factura,$estatus,$pdf,$color,&$cantidad,&$cantidadRes,$desde,$hasta,&$descuentoSum,&$resDescuentoP)
  {


  $pdf->SetFont('arial','B',12);
  $pdf->SetTextColor($color,0,0);

  $subtotal = collect();
  $cant = collect();
  $precioUnitario = collect();
  $resCantidad = collect();
  $descuento = collect();
  $resDescuento = collect();
  $pdf->cell(0,6,'Factura: '.$factura->reffac,0,0,'C');
  $pdf->SetFont('arial','',9);
  $pdf->ln();
  $pdf->cell(138,6,'Cod.Cliente: '.$factura->cliente->codpro,1,0,'L');
  $pdf->cell(80,6,'Fecha de Emision: '.$factura->fecfac,1,0,'L');
  $pdf->cell(60,6,utf8_decode('Estatus: '.$estatus),1,0,'L');
  $pdf->ln();
  $pdf->cell(138,6,'Rif.Cliente: '.$factura->cliente->rifpro,1,0,'L');
  $pdf->cell(140,6,'Nit.Cliente: ',1,0,'L');
  $pdf->ln();
  $pdf->cell(278,6,'Cliente: '.utf8_decode($factura->cliente->nompro),1,0,'L');
  $pdf->ln();
  $pdf->cell(278,6,'Direccion: '.utf8_decode($factura->cliente->dirpro),1,0,'L');
  $pdf->Ln(10);
  $pdf->SetFont('arial','B',9);
  $pdf->cell(34,6,'Cod. Articulo',0,0,'L');
  $pdf->cell(60,6,utf8_decode('Descripción'),0,0,'L');
  $pdf->cell(30,6,'Nro. Control',0,0,'L');
  $pdf->cell(20,6,'Cantidad',0,0,'L');
  $pdf->cell(25,6,'Precio/Unitario',0,0,'L');
  $pdf->cell(20,6,'Descuento',0,0,'L');
  $pdf->cell(25,6,'Sub Total',0,0,'L');
  $pdf->cell(20,6,'Total',0,0,'L');
  $pdf->cell(20,6,'IVA',0,0,'L');
  $pdf->cell(70,6,'Descuento %',0,0,'L');
  $pdf->ln(-5);
  $pdf->cell(0,22,'______________________________________________________________________________________________________________________________________________________________',0,0,'C');
  $pdf->ln(15);

  $muestraIva = $factura->fargoarticulo !== null ? $factura->porrecargo : 0;
  //$iva = intval($factura->porrecargo) !== 0 ? $factura->porrecargo/100 : 0;
  $iva = 0;
  foreach ($factura->articulos as $articulosF) {
    
    $iva += $factura->fargoarticulo !== null ? $articulosF->monrgo : 0; //evalue si hay articulos con iva
    $iva2 = $factura->fargoarticulo !== null ? $articulosF->monrgo : 0; //evalue si hay articulos con iva
    $pdf->SetFont('arial','',9);
    $pdf->cell(35,6,utf8_decode($articulosF->codart),0,0,'L');
    $pdf->cell(60,6,utf8_decode(substr($articulosF->desart,0,25)),0,0,'L');
    $pdf->cell(32,6,utf8_decode($articulosF->reffac),0,0,'L');
    $pdf->cell(21,6,utf8_decode($articulosF->cantot),0,0,'L');
    $pdf->cell(23,6,utf8_decode(number_format($articulosF->precio,2,',','.')),0,0,'L');
    $pdf->cell(22,6,utf8_decode(number_format($articulosF->mondes,2,',','.')),0,0,'L');
    $pdf->cell(22,6,utf8_decode(number_format((($articulosF->cantot * $articulosF->precio)) ,2,',','.')),0,0,'L');
    $pdf->cell(17,6,utf8_decode(number_format(($articulosF->totart) ,2,',','.')),0,0,'L');
    $pdf->cell(23,6,utf8_decode(number_format(($iva2) ,2,',','.')),0,0,'L');
    $pdf->cell(18,6,utf8_decode(number_format(($articulosF->Descuento->mondesc ?? 0) ,2,',','.')),0,0,'L');
    $pdf->Ln();

    $subtotal->push($articulosF->totart);
    $precioUnitario->push($articulosF->cantot * $articulosF->precio);

    if($factura->status === 'N' ){

      if($factura->fecfac === $factura->fecanu){

        $cant->push($articulosF->cantot);
        $resCantidad->push($articulosF->cantot*-1);
        $resDescuento->push($articulosF->mondes*-1);
       }else{
              if($factura->fecfac !== $factura->fecanu && $desde === $hasta){//en caso de se halla devuelto la factura y consulta un dia especifico
                  if ($desde >= $factura->fecanu) {//cuando se consulta la factura que sea mayor o igual del dia

                    $resCantidad->push($articulosF->cantot*-1);
                    $resDescuento->push($articulosF->mondes*-1);
                  }else if($desde <= $factura->fecfac){


                    $cant->push($articulosF->cantot);

                  }




            }else if($factura->fecfac !== $factura->fecanu && $desde !== $hasta){//en caso de se halla devuelto la factura y consulta en dias diferentes
                #dd($factura->reffac,$factura->fecfac,$factura->fecanu);

                if ($desde <= $factura->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia


                  $cant->push($articulosF->cantot);
                  if($factura->fecanu >= $desde && $factura->fecanu <= $hasta){

                    $pdf->SetTextColor(255,0,0);
                    $resCantidad->push($articulosF->cantot*-1);
                    $resDescuento->push($articulosF->mondes*-1);
                  }

                }else if($desde <= $factura->fecfac){


                  $cant->push($articulosF->cantot);
                }else if($factura->fecfac <= $factura->fecanu){



                    $resCantidad->push($articulosF->cantot*-1);
                    $resDescuento->push($articulosF->mondes*-1);
                }
            }

       }

    }else{
      $cant->push($articulosF->cantot);
      $descuento->push($articulosF->mondes);
    }

   # $iva->push(); $factura->porrecargo
  }
  $cantidad += $cant->sum();
  $cantidadRes += $resCantidad->sum();
  $descuentoSum += $descuento->sum();
  $resDescuentoP += $resDescuento->sum();
  
  $pdf->Ln();
  $pdf->SetFont('arial','B',9);
  $pdf->cell(50,6,utf8_decode("Sub Total: ". number_format($precioUnitario->sum(),2,',','.')),0,0,'L');
  $pdf->cell(50,6,utf8_decode("Cantidad: ".number_format($cant->sum(),2,',','.')),0,0,'L');
  $pdf->cell(50,6,utf8_decode("Descuento: ".number_format($descuento->sum()-$resDescuento->sum(),2,',','.')),0,0,'L');
  $pdf->cell(65,6,utf8_decode("Recargo({$muestraIva}%): ".number_format($iva,2,',','.')),0,0,'L');
  $pdf->cell(50,6,utf8_decode("Monto Total: ".number_format($iva+$subtotal->sum(),2,',','.')),0,0,'L');
  $pdf->Ln();


  $pdf->cell(0,22,'______________________________________________________________________________________________________________________________________________________________',0,0,'C');
  $pdf->Ln(20);
  }


  function updateCorrelativo(Request $request) {

      
      if($request->all()['fiscal'] === "fiscal")
        $utlimoCo = Fafactur::where('is_fiscal',true)->orderBy('reffac','DESC')->first()->num_fiscal+1;
      else
        $utlimoCo = Fafactur::where('is_fiscal',false)->orderBy('reffac','DESC')->first()->numero_ticket+1;

    return [
          'correlativo' => $utlimoCo,
          'caja' => $request->all()['fiscal']
        ];

  }

  function cerrarTurnos(Request $request) {
    
    
      $success =  $this->posApi->cierre($request->all()['caja']);
      
      if($success['error'] === false){
        $User = \App\cajaUser::where('loguse',Auth::user()->loguse)->where('codcaja',$request->all()['caja'])->first();
        $User->activo = 'FALSE';
        $User->save();
      }

      return $success['error'] === true ? response()->json(['error' => $success['error'],'msg' => $success['msg']]) : response()->json(['error' => $success['error'],'msg' => $success['msg']]);

  }


  function cierre2(Request $request) {

    
    
    $caja = Caja::where('descaj',$request->all()['id'])->first();
    
    return [
      'is_fiscal' => $caja->is_fiscal
    ];
  }

}
