<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\MonedaRequest;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\Famoneda;
use App\Referencia;
use App\permission_user;
use App\Segususuc;
use App\faartpvp;
use App\Caregart;

use Helper;
use Session;
use DB;

class PrecioController extends Controller
{


  private $guard;
  public function __construct(Guard $guard)
  {
    $this->middleware('auth');
    $this->guard = $guard;
  }
   
     
    public function index(Request $request){
       $menu = new HomeController();
       //$precios = faartpvp::with('moneda','articulo')->withTrashed()->latest()->paginate(8);
       //$articulos = Caregart::get();

       return view('/finanza/precios')->with([
        	'menus'=>$menu->retornarMenu(),
          //   'precios'=>$precios,
           // 'articulos'=>$articulos,
       ]);
    }
   
    public function getlastprecios(){ 

      // $articulos = Caregart::all()->random(1)->first();
      // $precios = faartpvp::where('codart','ilike','%'.$articulos->codart.'%')->orderBy('id', 'desc')->paginate(30);

      $precios = faartpvp::whereHas('articulo')->with('articulo', 'moneda')->orderBy('id', 'desc')->take(30)->paginate(8);
    
      
      return response()->json([
          'precios' => $precios,  
          // 'articulos' => $articulos,
      ]);

    }


    public function filtro(Request $request){ 
      $articulo = Caregart::where('codart','ilike','%'.$request["filtro"].'%')
      ->orWhere('desart','ilike','%'.$request["filtro"].'%')
      ->orWhere('nomart','ilike','%'.$request["filtro"].'%')
      ->select('desart', 'codart', 'nomart')->first();
      if(!$articulo){
        return response()->json([
          'status' => 'error',
          'message'   => 'No se encontró el artículo'
        ], 500);
      }
      $precios = faartpvp::where('codart','ilike','%'.$articulo->codart.'%')->with('moneda')->orderBy('id', 'desc')->paginate(8);
      

      return response()->json([
        'precios' => $precios,  
        'articulo' => $articulo,
    ]);
  }
    public function updateprecio(Request $request){
        $codart  = faartpvp::where('id','=', $request->id)->value('codart');
        $precios = faartpvp::where('id','=', $request->id)->update(['status' => "A"]);
        $precios = faartpvp::where('codart', '=', $codart)
                    ->where('id', '!=', $request->id)
                    ->update(['status' => 'I']);
       // $precios = faartpvp::where('id','!=', $request->id)->update(['status' => "I"]);   
   }
}
