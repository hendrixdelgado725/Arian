<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\Serial;
use App\Models\SerialesDisponibles;
use App\Fafactur;
use App\Segususuc;
use App\Models\Logs_impresora_api;

class SerialesController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {   
        return view('seriales/seriales')->with(['menus' => $this->getmenu(),'seriales' => false]);
    }
    public function getmenu(){
        return $this->menu = Menu::getmenu();
    }

    public function filtro(Request $request)
    {
        $sucursalUserLog = Auth()->user()->codsuc_reg;
        $filtro = $request->filtro;
        $seriales = Serial::whereHas('getFactura.sucursales', function ($q) use ($sucursalUserLog) {
            $q->where('codsuc', $sucursalUserLog);
        })->when($filtro !== null,function ($query) use ($filtro)
        {
            return $query->where('serial','ilike','%'.$filtro.'%')
                         ->orWhere('factura','ilike','%'.$filtro.'%');
        })->when($filtro === null,function ($query) use ($filtro)
        {
            return $query;
        })->paginate(10);
        
        return response()->json([
            'seriales' => $seriales,
        ], 200);
    }

    public function filtro2(Request $request)
    {
        $filtroD = $request->filtroD;
        $loguser = Auth()->user()->loguse;
        $codsucsUsers = Segususuc::where('loguse', $loguser)->pluck('codsuc');

        $seriales_disponibles = SerialesDisponibles::whereHas('almacen', function ($q) use ($codsucsUsers) {
            $q->whereIn('codsuc_asoc', $codsucsUsers);
        })->when($filtroD !== null,function ($query) use ($filtroD)
        {
            return $query->where('seriales','ilike','%'.$filtroD.'%')
                         ->orWhere('codart','ilike','%'.$filtroD.'%');
        })->when($filtroD === null,function ($query) use ($filtroD)
        {
            return $query;
        })->paginate(10);
        
        return response()->json([
            'seriales_disponibles' => $seriales_disponibles,
        ], 200);
    }

    public function getSeriales(Request $request){

        $sucursalUserLog = Auth()->user()->codsuc_reg;
        
        if($request->filtro != null){
            $filtro = $request->filtro;
            $seriales = Serial::whereHas('getFactura.sucursales', function ($q) use ($sucursalUserLog) {
                $q->where('codsuc', $sucursalUserLog);
            })->where('serial', 'LIKE', "%$filtro%")
            ->orWhere('factura', 'LIKE', "%$filtro%")
            ->paginate(10);
        }

        if($request->filtro === null){
            $seriales = Serial::whereHas('getFactura.sucursales', function ($q) use ($sucursalUserLog) {
                $q->where('codsuc', $sucursalUserLog);
            })->paginate(10);
        }
        
        return response()->json([
            'seriales' => $seriales,
        ], 200);

    }

    public function getSerialesDisponibles(Request $request){
 
        $loguser = Auth()->user()->loguse;
        $codsucsUsers = Segususuc::where('loguse', $loguser)->pluck('codsuc');

        if($request->filtroD != null){
            $filtroD = $request->filtroD;
            $seriales_disponibles = SerialesDisponibles::whereHas('almacen', function ($q) use ($codsucsUsers) {
                $q->whereIn('codsuc_asoc', $codsucsUsers);
            })->where('seriales', 'LIKE', "%$filtroD%")
            ->orWhere('codart', 'LIKE', "%$filtroD%")
            ->paginate(10);
        }

        if($request->filtroD === null){
            $seriales_disponibles = SerialesDisponibles::whereHas('almacen', function ($q) use ($codsucsUsers) {
                $q->whereIn('codsuc_asoc', $codsucsUsers);
            })->paginate(10);   
        }
        
        return response()->json([
            'seriales_disponibles' => $seriales_disponibles,
        ], 200);
    }

    public function VisualizarSerial($id)
    {
        $serial = Serial::withTrashed()->whereHas('getFactura')
                  ->whereHas('getArticulo')
                  ->with('getFactura','getArticulo')
                  ->find($id);
       
       return $serial;

    }

    public function detallar($id){

        $factura = Fafactur::with('cliente','sucursales','articulos','pagos')->where('reffac',$id)->first();        
        $log = Logs_impresora_api::where('factura_sistema',$factura->reffac)->first();
        $noEncontrado = false;
        
        if($log === null) $noEncontrado = true;

        return view('/facturacion/facturaDetalle')->with('menus',$this->getmenu())
        ->with('factura',$factura)->with('noEncontrado',$noEncontrado);;
    }
}
