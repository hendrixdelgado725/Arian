<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ClienteRequest;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\Facliente;
use Helper;
use Illuminate\Support\Facades\Auth;
use App\CedulaVE;
use App\User;
use Session;
use App\Sucursal;
use Carbon\Carbon;
use \Fpdf;
use App\Http\Controllers\generadorPDF;
use Illuminate\Contracts\Auth\Guard;
use App\permission_user;
use App\Segususuc;

class ClienteController extends Controller
{
    private $guard;
    public function __construct(Guard $guard)
    {
        $this->middleware('auth');
        $this->guard = $guard;
        
        
    }
    
    public function create(ClienteRequest $request){
        //return ['message'=>'hola'];
        $guion='-';
        $documento = $request->tipodocumento.$guion.$request->cedularif;
        $documento2 = $request->tipodocumento.$request->cedularif;
        
        $comprobar=Facliente::where('codpro',$documento2)->first();
        // dd($comprobar);
        if($comprobar)
        {
            if($comprobar['deleted_at']==NULL)
            {
                Session::flash('error', ' El número de Documento '.$documento2.' posee un registro Activo en el sistema, verificar información.');
                return redirect()->route('nuevoCliente');
            }
            else
            {
                $comprobar->update(
                    [
                        'deleted_at'=>NULL,
                        'codpro'=>$documento2,
                        'nompro'=>$request['nombre'],
                        'rifpro'=>$documento,
                        'dirpro'=>$request['direccion'],
                        'telpro'=>$request['telefono'],
                        'email'=>$request['email']
                    ]
                );
                Session::flash('success', 'El cliente '.$request['nombre'].', de registro Exitosamente');
                return redirect('/modulo/ConfiguracionGenerales/Clientes');
            }
            
        }
        
        //codigoid
        // $pre = '';
       /* $cont = Facliente::get()->count();*/
        $cont = Facliente::withTrashed()->get()->count();

        $cont = $cont+1;
        // $d=date('ds');
        // $idUser = Auth::user()->id;
        // $cod = $pre.$cont.$idUser.$d;
        // $suc = Auth::user()->codsuc_reg;
        $suc=Auth::user()->getCodigoActiveS();
        $codsuc_reg = explode('-',$suc);
        $cod = $cont.$guion.$codsuc_reg[1];

        if($request['email']==null){
            $request['email'] = '';
        }

            
        Facliente::create([
            'codpro'=>$documento2,
            'nompro'=>$request['nombre'],
            'rifpro'=>$documento,
            'dirpro'=>$request['direccion'],
            'telpro'=>$request['telefono'] ?? '',
            'email'=>$request['email'] ?? '',
            'codigoid'=>$cod,
            'codsuc'=>$suc,
            'staclif'=>'A',
            
        ]);

        Session::flash('success', 'El cliente '.$request['nombre'].', se registro Exitosamente');
        return redirect('/modulo/ConfiguracionGenerales/Clientes');
    }  


    public function update(ClienteRequest $request, $id){
        // dd($request);
        $cliente = Facliente::find($id);
        // $guion='-';
        // $documento = $request->tipodocumento.$guion.$request->cedularif;
        // $documento2 = $request->tipodocumento.$request->cedularif;
        
 
        // $cliente->codpro = $documento2;
        $cliente->nompro = $request->nombre;
        // $cliente->rifpro = $documento;
        $cliente->telpro = $request->telefono;
        $cliente->dirpro = $request->direccion;
        $cliente->email = $request->email;
        $cliente->save();

        Session::flash('success', 'El cliente '.$request->nombre.', se actualizó Exitosamente');
        return redirect('/modulo/ConfiguracionGenerales/Clientes');
        

    }

    public function filtro(Request $request){
        $menu = new HomeController();
        $clientes=Facliente::whereNULL('deleted_at')->where('codpro','ilike','%'.$request->filtro.'%')->Orwhere('nompro','ilike','%'.$request->filtro.'%')->paginate(8);
        // $clientes = Facliente::where('deleted_at',NULL)->where('nompro','ilike','%'.$request->filtro.'%')
        // ->orWhere('codpro','ilike','%'.$request->filtro.'%')->orWhere('rifpro','ilike','%'.$request->filtro.'%')->paginate(8);
        // dd($clientes);
        return view('/configuracionGenerales/cliente')->with('menus',$menu->retornarMenu())->with('clientes',$clientes);
    }

    public function lista()
    {  

     $menu = new HomeController();
        //return Facliente::has('factura')->limit(20)->get();
        $clientes = Facliente::orderBy('id','DESC')->where('deleted_at',null)->paginate(8);
        // Facliente::orderBy('id','DESC')->paginate(8);
        return view('/configuracionGenerales/cliente')->with('menus',$menu->retornarMenu())->with('clientes',$clientes);
    }

    public function nuevocliente(){
        $menu = new HomeController();
        return view('/configuracionGenerales/registroCliente')->with('menus',$menu->retornarMenu());
    }

    public function edit($id){
        $menu = new HomeController();
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $cliente = Facliente::where('id','=',$ID)->first();

        $documento = explode('-',$cliente->rifpro); 
        $tipodocumento = $documento[0];    //obteniendo el tipo de documento para returnarlo al formulario
        $cliente->setAttribute('cedula',$documento[1]); //para devolver la cedula sin el tipo de documento
        $cliente->setAttribute('tipodocumento',$tipodocumento);
        //return $cliente;
        return view('/configuracionGenerales/editarCliente')->with('menus',$menu->retornarMenu())->with('cliente',$cliente);
    }

    public function delete($id){
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $cliente = Facliente::find($ID);
        $codcli = $cliente->codpro;
        //
        

        $factura = Facliente::where('id','=',$ID)->with('factura')
        ->whereHas('factura',function($q) use ($codcli){
            $q->where('codcli','=',$codcli);
        })->get();

        $factura = Facliente::where('id','=',$ID)->with('factura')->has('factura')->get();

        $presupuesto = Facliente::where('id','=',$ID)->with('presupuesto')->has('presupuesto')->get();

            $cliente->delete();
            // $cliente->update(['deleted_at'=>date('Y-m-d h:m:i')]);
        
        if($factura->isEmpty()&&$presupuesto->isEmpty()){
            $cliente->delete();
            return response()->json([
                'titulo' => 'Exito se elimino'
            ]);
        }
        else{
            return response()->json([
            
                'titulo' => 'error Este cliente no puede ser eliminado'

            ]);

        }
            
    } //delete
    public function cne(Request $request)
    {
        if($request['cedularif']!=null){
            $cneResultado = CedulaVE::get($request['tipodocumento'], $request['cedularif']);
            if($cneResultado==null){
            }
            else{
                // if($cneResultado->getStatusCode() == 200){
                   return response()->json($cneResultado);
                // }
            }
        }
        
    }

    public function cne2(Request $request)
    {
        if($request['cedularif']!=null){
            $codpro = $request['tipodocumento'].$request['cedularif'];
            $cliente = Facliente::where('codpro','=',$codpro)->first();
            $base = "base";
            if($cliente!=null){
               // return response()->json($cliente);

                return response()
                ->json([
                'titulo' => 'encontrado',
                'cliente' => $cliente
            ]);
                
            }
            else{
                $cneResultado = CedulaVE::get($request['tipodocumento'], $request['cedularif']);
                if($cneResultado==null){
                }
                else{
                    // if($cneResultado->getStatusCode() == 200){
                       return response()->json($cneResultado);
                    // }
                }
            }//isEmpty
       

           
        }
        
    }

    public function Cliente(Request $request){
        $cliente = Facliente::where('codpro','=',$request['codpro'])->first();
        return response()->json($cliente);
    }

    public function createClienteModal(Request $request){

        $guion='-';
        $cont = Facliente::withTrashed()->get()->count();
        // $cont = Facliente::get()->count();

        $cont = $cont+1;
        $documento = $request->tipodocumento.$guion.$request->cedularif;
        $documento2 = $request->tipodocumento.$request->cedularif;

        //codigoid
        $suc = Auth::user()->codsuc_reg;
        $codsuc_reg = explode('-',$suc);
        $cod = $cont.$guion.$codsuc_reg[1];
  
        
        Facliente::updateOrCreate([
            'rifpro' => $documento,
            
        ],[
            'codpro' => $documento2,
            'nompro' => $request['nombre'],
            'dirpro' => $request['direccion'] ?? '',
            'telpro' => $request['telefono'] ?? '',
            'email' => $request['email'] ?? '',
            'codigoid' => $cod,
            'codsuc' => session('codsuc'),//cambia la sucursal donde esté
        ]);
        
        
        $clientes = Facliente::orderBy('nompro')->where('codsuc',session('codsuc'))->get();
        return response()->json($clientes);
    }

    public function vistaReporte(){
        $menu = new HomeController();
        $sucursales = Sucursal::where('codsuc',session('codsuc'))->get();
        
        return view('/reportes/clientesReportes')->with('menus',$menu->retornarMenu())->with('sucursales',$sucursales);
    }


    ////REPORTES///////////////////////
    public function reporteCliente(Request $request){

         $desde = $request['desde'];
         $hasta = $request['hasta'];
         $diames = $request['diames'];
         switch ($diames) {
            case 'diario':
               $tipo = 'DIARIO';
            break;
            case 'ayer':
                $tipo = 'DÍA ANTERIOR';
            break;
            case 'semanal':
               $tipo = "SEMANAL";
            break;
            case 'semant':
               $tipo = "SEMANA ANTERIOR";
            break;
            case 'mensual':
                $tipo = 'MENSUAL';
            break;
            case 'mesant':
              $tipo = 'MES ANTERIOR';
            break;
            case 'anual':
              $tipo = 'ANUAL';
            break;
            case 'antyear':
              $tipo = 'AÑO ANTERIOR';
            break;
            default:
              $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
            break;

        }

        try{
            if(!$request->sucursal)throw new Exception("Debe seleccionar una sucursal");
            if($request->sucursal =="TODAS"){
                $general = true;
            }
            else{
                $general = false;
                $codsuc = $request->sucursal;
            }

        }
        catch(Exception $e){
            return redirect()->route('reportesClientes')->with('error',$e->getMessage());
        }
        
      ///EVALUA LOS PERIODOS DE TIEMPO SELECCIONADOS Y SI SERA UNA CONSULTA GENERALIZADA O POR SUCURSAL
      try{  
                $intervalos = collect([$desde,$hasta]);     
                $desde = $intervalos->min();
                $hasta = $intervalos->max();

               if($general){
                    $clientes = Facliente::with('sucursal')->where('codsuc','<>',null)->get();
               }
               else{
                    $clientes = Facliente::with('sucursal')->whereBetween('codpro',[$desde,$hasta])->where('codsuc','=',$codsuc)->get();
               }
               
               

            if($clientes->isEmpty()){
                Session::flash('error', 'No existen registros con los parametros de busqueda ingresados');
                return redirect()->route('reportesClientes');
            }
        }//try
        catch(Exception $e){
            Session::flash('error', 'Se presentó un problema en la generación del reporte, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
            return redirect()->route('reportesClientes');
        }
            $pdf = new generadorPDF();

            $titulo = 'REPORTE DE CLIENTES';
            $total = $clientes->count();
            $fecha = date('Y-m-d');
    
		    $pdf->AddPage('L');
            $pdf->SetTitle('REPORTE DE CLIENTES');
            $pdf->SetFont('arial','B',16);
		    $pdf->SetWidths(array(90,90));
		    $pdf->Ln();
		    $pdf->Cell(0,22,utf8_decode($titulo),0,0,'C');
		    $pdf->Ln();
		    $pdf->SetFont('arial','B',10);
            if($tipo == 'Intervalo'){
                $pdf->Cell(0,0,utf8_decode('Tipo: '.$tipo.' '.$desde.' - '.$hasta),0,0,'L');
            }
            else{
                $pdf->Cell(0,0,utf8_decode('Tipo: '.$tipo),0,0,'L');
            }
            $pdf->Ln(5);
            $pdf->Cell(0,0,utf8_decode('Fecha actual: '.$fecha),0,0,'L');
            $pdf->Ln(5);
            $pdf->Cell(0,0,utf8_decode('Total: '.$total),0,0,'L');
            $pdf->Ln(10);
            $pdf->SetFont('arial','B',8,5);
	     	$pdf->SetFillColor(2,157,116);
	     	$pdf->Cell(50,8,utf8_decode('N° DOC / RIF'),1,0,'C');
            $pdf->Cell(90,8,utf8_decode('NOMBRE'),1,0,'C');
            $pdf->Cell(88,8,utf8_decode('DIRECCIÓN'),1,0,'C');
            $pdf->Cell(50,8,utf8_decode('SUCURSAL'),1,0,'C');
            $pdf->Ln();
            $pdf->SetWidths(array(50,90,88,50));
            $pdf->SetAligns(['C','C','C','C']);
            $pdf->SetFont('arial','',8,5);
            foreach($clientes as $cliente){
                if(!$cliente->nomsucu){
                 $sucursal = '.';
                 $pdf->Row(array($cliente->codpro,utf8_decode($cliente->nompro),utf8_decode($cliente->dirpro),$cliente->sucursal->nomsucu ?? ''));
                }
                else{
                 $pdf->Row(array($cliente->codpro,utf8_decode($cliente->nompro),utf8_decode($cliente->dirpro),$cliente->sucursal->nomsucu ?? ''));
                }

            }
            $pdf->Ln(5);
            $pdf->Output('I','Reporte de Clientes-'.$fecha.'pdf');
            
           exit;
        }


        public function FilterClienteSucursal($id)
        {
            
            if($id === 'TODAS')
               $cliente = Facliente::select('codpro','nompro')->get();
            else
               $cliente = Facliente::where('codsuc',$id)->select('codpro','nompro')->get();

            return [
                'cliente' => $cliente
            ];
        }

    
}
