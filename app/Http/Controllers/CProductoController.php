<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CProductoRequest;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\DemoProducto;
use App\Facategoria;
use App\Caregart;
use Helper;
use Illuminate\Support\Facades\Auth;
use User;
use Session;
use App\Almacen;
use App\Caartalmubi;
use App\permission_user;
use App\Segususuc;

class CProductoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        
        
    }
    
    public function index(){
        $menu = new HomeController();
        $categorias = Facategoria::orderBy('id','DESC')->paginate(8);

        return view('/configuracionGenerales/categoriaProducto')->with('menus',$menu->retornarMenu())->with('categorias',$categorias);
    }

    public function nuevacategoria(){
        $menu = new HomeController();
        $demografias = DemoProducto::get();

        return view('/configuracionGenerales/registroCdeProducto')->with('menus',$menu->retornarMenu())->with('demografias',$demografias);
    }

    public function create(CProductoRequest $request){
       

            $guion='-';
            $cont = Facategoria::withTrashed()->get()->count();
            $cont = $cont+1;
        
            // $suc = Auth::user()->codsuc_reg;
            $suc=Auth::user()->getCodigoActiveS();
            $codsuc_reg = explode('-',$suc);
            $cod = $cont.$guion.$codsuc_reg[1];

     /*       if($request['talla']) $talla = true;
            else $talla = false;*/
            /*return 'hi';*/
            
            $comprobar=Facategoria::withTrashed()->where('nomencat',$request['nomenclatura'])->whereOr('nombre',$request['nombre'])->first();
            if($comprobar)
            {
                if($comprobar['deleted_at']==NULL)
                {
                    if ($comprobar['nomencat']==$request['nomenclatura'])
                    {
                        Session::flash('error', ' La Nomeclatura  '.$request['nomenclatura'].' posee un registro Activo en el sistema, verificar información.');
                    }
                    else
                    {
                        Session::flash('error', ' La Categoría  '.$request['nombre'].' posee un registro Activo en el sistema, verificar información.');   
                    }
                    return redirect()->route('listaCdeProducto');
                } 
                else
                {
                    $comprobar->restored();
                    $comprobar->update([
                      'nombre'=>$request['nombre'],
                      'codsuc'=>$suc,
                      'talla'=> $request->talla == 'on' ? 'TRUE' : 'FALSE',
                    ]);

                    Session::flash('success', 'La demografia '.$request['nombre'].', se registro Exitosamente');
                    return redirect('/modulo/ConfiguracionGenerales/CdeProducto');
                }   
            }

        Facategoria::create([
            'nombre'=>$request['nombre'],
            'nomencat'=>$request['nomenclatura'],
            'codigoid'=>$cod,
            'codsuc'=>$suc,
            'talla'=> $request->talla == 'on' ? 'TRUE' : 'FALSE',
        ]);

        Session::flash('success', 'La categoría '.$request['nombre'].', se registro Exitosamente');
        return redirect('/modulo/ConfiguracionGenerales/CdeProducto');
    }

    public function edit($id){
        $menu = new HomeController();
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $categoria = Facategoria::find($ID);
        $sec = Segususuc::where('codusuario',Auth::user()->codigoid)->where('activo','TRUE')->first();   

        return view('/configuracionGenerales/editarCdeProducto')->with('menus',$menu->retornarMenu())->with('categoria',$categoria);
            
    }

    public function update(CProductoRequest $request, $id){
/* return $request;*/
      /*  if($request['talla']) $talla = true;
        else $talla = false;*/

        $categoria = Facategoria::find($id);
        $categoria->nombre = $request->nombre;
        $categoria->nomencat = $request->nomenclatura;
        $categoria->talla = $request->talla == 'on' ? 'TRUE' : 'FALSE';
        $categoria->save();

        Session::flash('success', 'La categoría '.$request['nombre'].', se actualizó Exitosamente');
       return redirect('/modulo/ConfiguracionGenerales/CdeProducto');
    }

    public function filtro (Request $request){
        $menu = new HomeController();
        $categorias = Facategoria::where('nombre', 'ilike','%'.$request->filtro.'%')->orWhere('nomencat', 'ilike', '%'.$request->filtro.'%')->orderBy('id','DESC')->paginate(8);
        return view('/configuracionGenerales/categoriaProducto')->with('menus',$menu->retornarMenu())->with('categorias',$categorias);

    }

    public function delete($id){

        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $categoria = Facategoria::find($ID);

        $registro = Facategoria::find($ID)->load(['subCategoria', 'categoryArt']);
        if($registro->subCategoria->isEmpty() && $registro->categoryArt === null){
            $categoria->delete();
            return response()->json([
                'titulo' => 'Exito se elimino'
            ]);
        }
        else{
            return response()->json([
                'titulo' => 'error Esta categoria no puede ser eliminado'
                ]);
            }

    }

    public function vistaReporte()
    {
        $almacenes = Almacen::get();
        $menu = new HomeController();
        $categorias = Facategoria::get();

        return view('/reportes/articulosCategorias')->with('menus',$menu->retornarMenu())->with(['categorias' => $categorias, 'almacenes' => $almacenes]);
    }

    public function generarReporte(Request $request)
    {   
        if($request->codalm === null) {
            Session::flash('error', 'Debe seleccionar un almacén');
            return redirect()->route('vistaArtCategorias');
        }        

        if($request->codcat === null) {
            Session::flash('error', 'Debe seleccionar una categoría');
            return redirect()->route('vistaArtCategorias');
        }        
            $nomalm=Almacen::where('codalm',$request->codalm)->first()->nomalm;
            $pdf = new generadorPDF;    
            $titulo = 'REPORTE DE ARTICULOS POR CATEGORÍA';
            $totalExist = 0;
            $fecha = date('Y-m-d');
    
            $pdf->AddPage('L');
            $pdf->SetTitle('REPORTE DE ARTICULOS POR CATEGORIA');
            $pdf->SetFont('arial','B',16);
            $pdf->SetWidths(array(90,90));
            $pdf->Ln();
            $pdf->Cell(0,22,utf8_decode($titulo),0,0,'C');
            $pdf->Ln();
            $pdf->SetFont('arial','B',10);

            $pdf->Ln(5);
            $pdf->Cell(0,0,utf8_decode('ALMACÉN: '.$nomalm),0,0,'L');
            $pdf->Ln(5);
            $pdf->Cell(0,0,utf8_decode('FECHA: '.date("d-m-Y", strtotime($fecha))),0,0,'R');
            $pdf->Ln(5);
            if($request->codcat !== 'TODAS') //Hay categoría
            {
            $nomcat = Facategoria::where('codigoid', $request->codcat)->first()->nombre;
            $articulos = Caartalmubi::where('codalm',$request->codalm)
              ->where('exiact','>=',0)
              ->with('ubicaciones')
              ->whereHas('ubicaciones')
              ->with('articulos')
              ->whereHas('articulos', function ($q) use($request){
                  $q->where('codcat',$request->codcat);
              })->with('precioArt')->whereHas('precioArt',function($q){
                  $q->where('faartpvp.status','A');
              })
              ->with('almacen')
              ->category()
              ->orderby('codart')
              ->get();

            $pdf->SetFont('arial','B',9,5);
            $pdf->SetFillColor(2,157,116);
            $pdf->Cell(275,8,utf8_decode('CATEGORÍA: '.$nomcat),1,0,'J');
            $pdf->Ln();
            $pdf->Cell(30,8,utf8_decode('COD. ARTÍCULO'),1,0,'C');
            $pdf->Cell(210,8,utf8_decode('ARTÍCULO'),1,0,'C');
            $pdf->Cell(35,8,utf8_decode('EXISTENCIA'),1,0,'C');
            $pdf->Ln();
            $pdf->SetWidths(array(30,210,35));
            $pdf->SetAligns(['C','C','C']);
            $pdf->SetFont('arial','',8,8);


            foreach($articulos as $articulo){
                
                $pdf->Row(array(
                    $articulo->articulos->codart,
                    $articulo->articulos->desart,
                    $articulo->exiact
                ));
            } 
        }
        else{
            $articulos = [];
            $categorias = Facategoria::with('categoryArt')->get();
                    foreach($categorias as $categoria){
                        $articulos = [];
                        $pdf->SetFont('arial','B',9,5);
                        $pdf->SetFillColor(2,157,116);
                        $pdf->Cell(275,8,utf8_decode('CATEGORÍA: '.$categoria->nombre),1,0,'C');
                        $pdf->Ln();
                        $pdf->Cell(30,8,utf8_decode('COD. ARTÍCULO'),1,0,'C');
                        $pdf->Cell(210,8,utf8_decode('ARTÍCULO'),1,0,'C');
                        $pdf->Cell(35,8,utf8_decode('EXISTENCIA'),1,0,'C');
                        $pdf->Ln();
                        $pdf->SetWidths(array(30,210,35));
                        $pdf->SetAligns(['C','C','C']);
                        $pdf->SetFont('arial','',8,5);
            
                        $articulos = Caregart::where('codcat', $categoria->codigoid)->whereHas('inventario')->with('inventario')->get();
                         foreach($articulos as $articulo){
                              $pdf->Row(array(
                                $articulo->codart,
                                $articulo->desart,
                                $articulo->inventario->exiact
                            ));
                        } 
                    $pdf->Ln(5);

                    }
        }
            $pdf->Ln(5);
            $pdf->SetFont('arial','B',10,5);
	     	    $pdf->SetFillColor(2,157,116);
            $pdf->SetTextColor(0,0,0);
            $pdf->SetTextColor(0,0,0);
            $pdf->Ln(5);
            $pdf->Output('I','ARTICULOS CATEGORÍA-'.$fecha.'pdf');
            
           exit;
    }
        
}
