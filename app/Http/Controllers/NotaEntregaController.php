<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NotaEntregaRequest;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\Caregart;
use App\Facliente;
use App\Fadescto;
use App\faartpvp;
use App\Fafactur;
use App\Fafactuart;
use App\Faartfac;
use App\Fafacturpago;
use App\Faforpag;
use App\Tallas;
use App\Almacen;
use App\Caartalmubi;
use App\Nphojint;
use App\Famoneda;
use Illuminate\Support\Facades\Auth;
use App\Sucursal;
use App\cajaUser;
use App\Models\Caja;
use Helper;
use Exception;
use Session;
use Carbon\Carbon;
use \Fpdf;
use App\Http\Controllers\generadorPDF;
use App\Logs_impresoras;
use App\Segususuc;
use App\NotaEntrega;
use App\NotaEntregaArt;
use App\Fatasacamb;

class NotaEntregaController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }
    public function index(){
    	$menu = new HomeController();
      $notas = NotaEntrega::with('articulos','almdestino','almorigen','cliente','autor','tasa')->orderBy('id', 'DESC')->paginate(8);

        return view('/almacen/listadoNotaEntrega')->with([
          'menus'=>$menu->retornarMenu(),
          'notas'=>$notas,
        ]);
    }

    public function create(Request $request){

    	$menu = new HomeController();
    	$articulos = Caregart::has('costos')->get();
        $clientes = Facliente::orderBy('nompro')->get();
        $almacenes = Almacen::get();
        $empleados = Nphojint::get();

    	return view('/almacen/registrarNotaEntrega')->with([
    		'menus'=>$menu->retornarMenu(),
    		'clientes'=>$clientes,
    		'articulos'=>$articulos,
    		'almacenes'=>$almacenes,
            'empleados'=>$empleados,
    	]);
    }

    public function getArticulos(Request $request){
      $articulos = Caregart::whereHas('costoPro')->where('tipreg','F')
      ->whereHas('almacenubi',function($q) use ($request){
        $q->where('exiact','>',0)->where('codalm',$request->codalm);
      })->withSum('almacenubi','exiact')->get();
      if(empty($articulos->toArray())) return response()->json(['status' => 'error', 'message' => 'No se encontraron artículos con precio en esta moneda'], 500);
    return response()->json($articulos);
    }




    public function store(Request $request){

         $request->validate([
          'codcli'=>'required',
          'codalm'=>'required',
          'arts' => 'required|array|min:1',
          'arts.*.cantidad' => 'required|min:1',
        ]);


      try{

          /* $cantidades = array_filter($request->cantidad);
          $articulos = array_filter($request->articulos);

          if(count($articulos)>count($cantidades)||count($articulos)<count($cantidades)){
             Session::flash('error', 'Debe seleccionar un articulo y su cantidad');
            return redirect()->route('nuevaNotaEntrega');
          } */

          
          $i = 0;
          $total = 0;
          $totalCambio = 0;

          $lenght = count($request->arts);
          $moneda = '';
          $tasaId = '';

            while ($i < $lenght) {


               $art = Caregart::where('codart','=',$request->arts[$i]["codart"])->with('costos')->first();

               $precio = $art->costoPro->pvpart*$request->arts[$i]["cantidad"];
               $total = $total + $precio;
               $i = $i+1;

              /* dd($art->costoPro);*/
               $coin = $art->costoPro->codmone;
              /* dd($coin);*/
               $moneda = Famoneda::where('codigoid',$coin)->first();
           
               
               if($moneda->nombre != "BOLIVAR SOBERANO" || $moneda->nombre != "BOLIVAR"){
                $bolivar = Famoneda::where('nombre','BOLIVAR SOBERANO')->orWhere('nombre', 'BOLIVAR')->first();
                
                $tasa = Fatasacamb::with('moneda','moneda2')->where('id_moneda',$moneda->codigoid)->orWhere('id_moneda2',$bolivar->codigoid)->where('activo',true)->first();
                $tasaId = $tasa->codigoid;
                $cambio = $tasa->valor;
                $total = $total + ($precio * $cambio);

                $totalCambio = $total / $cambio;

               }
               else{
                $bolivar = Famoneda::where('nombre','BOLIVAR SOBERANO')->orWhere('nombre', 'BOLIVAR')->first();
                $tasa = Fatasacamb::with('moneda','moneda2')->where('id_moneda2',$bolivar->codigoid)->where('activo',true)->orderBy('id','DESC')->first();
                $cambio = $tasa->valor;
                /*$tasaId = ' ';*/
                $total = $total + $precio;
                $pd = $precio/$cambio;
                $totalCambio = $totalCambio + $pd;
               }

          /*     dd($cambio);*/
             }
         


            $observacion = $request->obsent;
            $user = Auth::user();
            $created_by = $user->codigoid;
            $sucursal = $user->userSegSuc->codsuc;
            $codsuc_reg = explode('-',$sucursal);
            $codsuc = $codsuc_reg[1];

            $notas = NotaEntrega::where('codsuc',$sucursal)->withTrashed()->get()->count();
            $notas = $notas+1;
            $numero = 'N'.$codsuc.'-'.$notas;
            $codigoid = $notas.'-'.$codsuc;

            $nota = new NotaEntrega();
            $nota->numero = $numero;
            $nota->cod_cliente = $request->codcli;
            $nota->almori = $request->codalm;
            $nota->observacion = $observacion;
            $nota->created_by = $created_by;
            $nota->total = $total;
            $nota->fatasacamb = $tasaId;
            $nota->codsuc = $sucursal;
            $nota->codigoid = $codigoid;
            $nota->facturado = false;
            $nota->cambio = $totalCambio;
            $nota->save();


            $i = 0;

            while ($i < $lenght) {
               $notasA = NotaEntregaArt::where('codsuc',$codsuc)->get()->count();
               $count = $notasA +1;
               $codigoid2 = $count.'-'.$codsuc_reg[1];


               $art = Caregart::where('codart','=',$request->arts[$i]["codart"])->with('costos')->first();
               $precio = $art->costoUnit->pvpart;
               $coin2 = $art->costoUnit->codmone;
               $coinName = Famoneda::where('codigoid',$coin2)->first()->nombre;
    
               $costo = $precio*$request->arts[$i]["cantidad"];

               $articuloN = new NotaEntregaArt();
               $articuloN->codart = $art->codart;
               $articuloN->desart = $art->desart;
               $articuloN->codtalla = '';
               $articuloN->cantidad = $request->arts[$i]["cantidad"];
               $articuloN->preunit = $art->costoUnit->pvpart;
               $articuloN->costo = $costo;
               $articuloN->codsuc = $sucursal;
               $articuloN->numero_nota = $numero;
               $articuloN->codigoid = $codigoid2;
               $articuloN->moneda = $coin2;
               $articuloN->coin_name = $coinName;
               $articuloN->descuento_id = '';
               $articuloN->monto_descuento = 0;
               $articuloN->cambio = 0;
               $articuloN->save();
               $i = $i+1;


             }

             // if($cambio > 0){
             //   $cambioTotal = $total * $cambio;
             // }
             // else{
             //   $cambioTotal = 0;
             // }

         /*     $Not = NotaEntrega::where('numero',$numero)->first();
              $Not->cambio = $cambioTotal;
              $Not->fatasacamb = $tasaId;
              $Not->save();*/


              Session::flash('success', 'Registro creado con exito.');
              return response()->json([
                  'success' => true,
                  'message' => 'Información suministrada correctamente',
                  'redirect' => '/modulo/Almacen/NdEntrega',
              ]);
        }catch(Exception $e){
            Session::flash('error', 'Se presentó un problema en la generación del reporte, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
              return redirect('/modulo/Almacen/NdEntrega')->with(['error' => $e->getMessage()]);
        }

    
    }

    public function detalles($id){
        $menu = new HomeController();
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $nota = NotaEntrega::with('articulos','almdestino','almorigen','cliente','autor','tasa','sucursales')->find($ID);
        $articulos = NotaEntregaArt::with('moneda')->where('numero_nota',$nota->numero)->get();

  /*      if($nota->tasa){
          $tasa = $nota->tasa->valor;
          $cambio = $nota->total * $tasa;

            return view('/almacen/detallesNotaEntrega')->with([
             'menus'=>$menu->retornarMenu(),
             'nota'=>$nota,
             'cambio'=>$cambio,
            ]);
        }
        else{*/
            return view('/almacen/detallesNotaEntrega')->with([
             'menus'=>$menu->retornarMenu(),
             'nota'=>$nota,
             'articulos'=>$articulos,
            ]);
        /*}*/

    }

        public function pdfNota($nota){

        $desc=Helper::desencriptar($nota);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $nota = NotaEntrega::with('articulos','almdestino','almorigen','cliente','autor','tasa','sucursales')->find($ID);
        $articulos = NotaEntregaArt::where('numero_nota',$nota->numero)->get();
       
        $titulo = 'NOTA DE ENTREGA';
        $date = date('Y-m-d');

        $pdf = new generadorPDF();

       $pdf->AddPage('P');
       $pdf->SetTitle($nota->numero);
       $pdf->SetFont('arial','B',16);
       $pdf->SetWidths(array(90,90));
       $pdf->Ln();
       $pdf->Cell(0,22,utf8_decode($titulo.' N° '.$nota->numero),0,0,'C');
       $pdf->Ln();
       $pdf->SetFont('arial','B',10);
        // $pdf->Cell(0,0,utf8_decode('Sucursal: '.$presupuestoIn->codsuc),0,0,'L');
        $pdf->Cell(0,0,utf8_decode('AUTOR: '.$nota->autor->nomuse),0,0,'L');        
        $pdf->Ln(5);
        $pdf->Cell(0,0,utf8_decode('SUCURSAL: '.$nota->sucursales->nomsucu),0,0,'L');        
        $pdf->Ln(5);
        $pdf->Cell(0,0,utf8_decode('CREADO EL: '.date_format($nota->created_at, 'Y-m-d')),0,0,'L');
        $pdf->Ln(5);   
        ///INFORMACION DEL CLIENTE

        $pdf->Cell(0,0,utf8_decode('Información del Origen, Cliente/Destino'),0,0,'C'); 
        $pdf->Ln(4);       
        $pdf->SetFont('arial','B',8,5);
        $pdf->SetFillColor(2,157,116);
        if($nota->cod_cliente){
          $pdf->Cell(63,8,utf8_decode('Doc. Indentidad / RIF'),1,0,'C');
          $pdf->Cell(63,8,utf8_decode('Cliente'),1,0,'C');
          $pdf->Cell(63,8,utf8_decode('Numero de Teléfono'),1,0,'C');
        }
        else{
          $pdf->Cell(63,8,utf8_decode('Cod Almacen'),1,0,'C');
          $pdf->Cell(63,8,utf8_decode('Nombre Almacen'),1,0,'C');
          $pdf->Cell(63,8,utf8_decode('Dirección'),1,0,'C');
        }


       $pdf->Ln();
       $pdf->SetWidths(array(63,63,63));
       $pdf->SetAligns(['C','C','C']);
       $pdf->SetFont('arial','',8,5);
       if($nota->cod_cliente){
         $pdf->Row(array($nota->cod_cliente,$nota->cliente->nompro,$nota->cliente->telpro));
       }
       else{
         $pdf->Row(array($nota->almdes,$nota->almdestino->nomalm ?? 'N/A',$nota->almdestino->diralm ?? 'N/A'));
       }


       ////INFORMACION DE ARTICULOS
       $pdf->Ln(20);
       $pdf->SetFont('arial','B',10);
       $pdf->Cell(0,0,utf8_decode('Información de los articulos'),0,0,'C'); 
       $pdf->Ln(4);
       $pdf->SetFont('arial','B',8,5);
       $pdf->SetFillColor(2,157,116);
       $pdf->Cell(38,8,utf8_decode('Cod. Articulo'),1,0,'C');
       $pdf->Cell(38,8,utf8_decode('Descripcion'),1,0,'C');
       $pdf->Cell(38,8,utf8_decode('Talla'),1,0,'C');
       $pdf->Cell(38,8,utf8_decode('Cantidad'),1,0,'C');
       $pdf->Cell(38,8,utf8_decode('Costo Unit'),1,0,'C');


       $pdf->Ln();
       $pdf->SetWidths(array(38,38,38,38,38));
       $pdf->SetAligns(['C','C','C','C','C']);
       $pdf->SetFont('arial','',8,5);


       foreach($articulos as $articulo){

            $pdf->Row(array($articulo->codart,$articulo->desart,$articulo->codtalla,$articulo->cantidad,number_format($articulo->preunit)));
     
       
       }
       

    //   INFORMACION DE FINANZA
       $pdf->Ln(20);
       $pdf->SetFont('arial','B',10);
       $pdf->Cell(0,0,utf8_decode('Costos'),0,0,'C'); 
       $pdf->Ln(4);
       $pdf->SetFont('arial','B',8,5);  
       $pdf->SetFillColor(2,157,116);

             $pdf->SetFont('arial','B',8,5);
             $pdf->SetFillColor(2,157,116);

           
                $pdf->Cell(60,8,utf8_decode('Total Bs'),1,0,'C');
                $pdf->Cell(69,8,utf8_decode('Tasa'),1,0,'C');
                $pdf->Cell(60,8,utf8_decode('Cambio'),1,0,'C');


                $pdf->Ln();
                $pdf->SetWidths(array(70,70,70));
                $pdf->SetAligns(['C','C','C']);    
                $pdf->SetFont('arial','',8,5);

                /* $pdf->Cell(50,8,utf8_decode($presupuestoIn->fpago->destippag),1,0,'C');*/
                $pdf->Cell(60,8,utf8_decode(number_format($nota->total ?? 0,2,',','.')),1,0,'C');
                $pdf->Cell(69,8,utf8_decode(number_format($nota->tasa->valor ?? 0,2,',','.')),1,0,'C');
                $pdf->Cell(60,8,utf8_decode(number_format($nota->cambio,2,',','.')),1,0,'C');
       

  

             $pdf->Ln(20);
             $pdf->SetFont('arial','B',10);
             $pdf->Cell(0,0,utf8_decode('RESPONSABLES'),0,0,'C'); 
             $pdf->SetFont('arial','B',8,5);
             $pdf->SetFillColor(2,157,116);
             $pdf->Ln(10);
             $pdf->Cell(0,0,utf8_decode('Realizado Por:'),0,0,'L'); 
             $pdf->Cell(0,0,utf8_decode('Recibido Por:'),0,0,'R'); 
             $pdf->Ln(5);
             $pdf->Cell(0,0,utf8_decode($nota->autor->nomuse),0,0,'L'); 
             $pdf->Ln(5);
             $pdf->Cell(0,0,utf8_decode('____________________________'),0,0,'L'); 
             $pdf->Cell(0,0,utf8_decode('____________________________'),0,0,'R'); 
             $pdf->ln(10);



        $pdf->Output('I','NOTA ENTREGA '.$nota->numero.'.pdf');
      exit;
    }


    public function filtro(Request $request){
        $menu = new HomeController();
        $notas=NotaEntrega::whereNULL('deleted_at')->where('numero','ilike','%'.$request->filtro.'%')->Orwhere('cod_cliente','ilike','%'.$request->filtro.'%')->orderBy('id', 'DESC')->paginate(8);
        // $clientes = Facliente::where('deleted_at',NULL)->where('nompro','ilike','%'.$request->filtro.'%')
        // ->orWhere('codpro','ilike','%'.$request->filtro.'%')->orWhere('rifpro','ilike','%'.$request->filtro.'%')->paginate(8);
        // dd($clientes);
                return view('/almacen/listadoNotaEntrega')->with([
          'menus'=>$menu->retornarMenu(),
          'notas'=>$notas,
        ]);
    }


    public function descontar(Request $request){
        $menu = new HomeController();
        $desc=Helper::desencriptar($request->id);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];

        $nota = NotaEntrega::find($ID);

        $articulos = NotaEntregaArt::where('numero_nota',$nota->numero)->get();

        $almacen = Almacen::where('codalm','=',$nota->almori)->first();

          foreach($articulos as $art){

             if($art->codtalla){
                $existencia = Caartalmubi::where('codalm','=',$nota->almori)->where('codart','=',$art->codart)->where('codtalla','=',$art->codtalla)->where('exiact','>',0)->first();

             }//if talla
             else{
                $existencia = Caartalmubi::where('codalm','=',$nota->almori)->where('codart','=',$art->codart)->where('exiact','>',0)->first();
              }
            

             if($existencia){
               $cantEnInventario = $existencia->exiact;
               $cantSustraer = $art->cantidad;
               $cantEnInventario = $cantEnInventario-$cantSustraer;
               $existencia->exiact = $cantEnInventario;
               $existencia->save();
            }

         
            }

            $nota->extraido = true;
            $nota->save();

            return response()->json(['exito' => $nota->numero]);

   
    }
}
