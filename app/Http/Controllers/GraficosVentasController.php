<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use Helper;
use Carbon\Carbon;
use App\Sucursal;
use App\Charts\Ventas;
use App\Fafactur;
use App\Models\Caja;
use Exception;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Segususuc;

class GraficosVentasController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');
    
    }

    public function indexVentasSucursal(){
         $menu = new HomeController();
         $sucursales = Sucursal::get();
    	 $chart = new Ventas();
         $chart->title("VENTAS DEL MES DE TODAS LAS SUCURSALES", 18);
         Carbon::setWeekStartsAt(Carbon::MONDAY);

         $registros = Sucursal::with('Ventas')->whereHas('Ventas',function($q){
           $q->whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month);
        })->get();


        $labels = collect();
        $valores = collect();
        $coloresFondo = collect();

        $cont = 0;
        foreach ($registros as $registro) {
          $labels->push($registro->nomsucu);
          $valores->push($registro->ventas->count());
         }
         $chart->labels($labels);
         $dataset = $chart->dataset('VENTAS', 'doughnut', $valores); 
         $dataset->backgroundColor(collect([
            '#7158e2','#3ae374', '#ff3838','#FFFF00','#808000','#FF00FF','#FF6347','#FA8072','#FFC0CB','#EE82EE','#4B0082','#7d5fff','#32ff7e','#3f8ec7','#3d6530','#93f916','#9cf8d0','#00594f','#532b23','#ffff99'
        ]));
        $dataset->color(collect(['#7d5fff','#32ff7e', '#ff4d4d']));
        return view('Graficos/GraVentasSucursal')->with([
        	'menus'=>$menu->retornarMenu(),
        	'chart'=>$chart,
        	'sucursales'=>$sucursales,
            'sec' => Segususuc::where('codusuario',Auth::user()->codigoid)->where('activo','TRUE')->first()->getSucursal->nomsucu
        ]);
    }

    public function indexVentasCajas(){
         $menu = new HomeController();
         $suc = Auth::user()->codsuc_reg;
         $sucursales = Sucursal::get();
    	 $chart = new Ventas();
         $chart->title("VENTAS DEL MES", 18);
         Carbon::setWeekStartsAt(Carbon::SUNDAY);

         $registros = Caja::with('ventas')->whereHas('Ventas',function($q) use($suc){
            $q->whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->where('codsuc','=',$suc);
         })->get();


        $labels = collect();
        $valores = collect();
        $coloresFondo = collect();

        $cont = 0;
        foreach ($registros as $registro) {
          $labels->push($registro->descaj);
          $valores->push($registro->ventas->count());
         }
         $chart->labels($labels);
         $dataset = $chart->dataset('VENTAS', 'doughnut', $valores); 
         $dataset->backgroundColor(collect([
           '#7158e2','#3ae374', '#ff3838','#FFFF00','#808000','#FF00FF','#FF6347','#FA8072','#FFC0CB','#EE82EE','#4B0082','#7d5fff','#32ff7e','#3f8ec7','#3d6530','#93f916','#9cf8d0','#00594f','#532b23','#ffff99'
        ]));
        $dataset->color(collect(['#7d5fff','#32ff7e', '#ff4d4d']));
        return view('Graficos/GraVentasCajas')->with([
        	'menus'=>$menu->retornarMenu(),
        	'chart'=>$chart,
        	'sucursales'=>$sucursales,
        	'suc'=>$suc
        ]);
    }

    public function filtroGVSucursal(Request $request){
    	 $tiempo = $request['fecha'];
    	 $filtro = $request;

    	 $menu = new HomeController();
         $sucursales = Sucursal::get();
         $sucursal = $request['sucursales'];
         if($sucursal=='TODAS'){
         	$nomsucu = 'TODAS';
         }
         else{
         	$nomsucu = Sucursal::where('codsuc','=',$sucursal)->first()->nomsucu;
         }


    	 $chart = new Ventas();
    	
    	 Carbon::setWeekStartsAt(Carbon::MONDAY);

    	 if($tiempo=='M'){
    	 	$chart->title("VENTAS DEL MES EN SUCURSAL: ".$nomsucu, 18);
    	 	if($sucursal == 'TODAS'){
    	 	  $registros = Sucursal::with('ventas')->whereHas('Ventas',function($q) use($sucursal){
              $q->whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month);
              })->get();
    	 	}
    	 	else{
    	 	  $registros = Sucursal::with('ventas')->whereHas('Ventas',function($q) use($sucursal){
              $q->whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->where('codsuc','=',$sucursal);
              })->get();
    	 	}
    	 }
    	 elseif($tiempo=='S'){
    	 	$chart->title("VENTAS DE LA SEMANA EN SUCURSAL: ".$nomsucu, 18);
    	 	if($sucursal == 'TODAS'){
    	 	 $registros = Sucursal::with('ventas')->whereHas('Ventas',function($q) use($sucursal){
             $q->whereBetween('created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()]);
              })->get();
    	 	}
    	 	$registros = Sucursal::with('ventas')->where('codsuc','=',$sucursal)->whereHas('Ventas',function($q) use($sucursal){
            $q->whereBetween('created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()]);
            })->get();
    	 }
    	 else{
    	 	$chart->title("VENTAS DEL AÑO EN SUCURSAL: ".$nomsucu, 18);
    	 	if($sucursal=='TODAS'){
    	 	 $registros = Sucursal::with('ventas')->whereHas('Ventas',function($q) use($sucursal){
             $q->whereYear('created_at', Carbon::now()->year);
             })->get();
    	 	}
    	 	else{
    	 	 $registros = Sucursal::with('ventas')->where('codsuc','=',$sucursal)->whereHas('Ventas',function($q) use($sucursal){
             $q->whereYear('created_at', Carbon::now()->year);
             })->get();
    	 	}
    	 }
         
 
        $labels = collect();
        $valores = collect();
        $coloresFondo = collect();

        $cont = 0;
        foreach ($registros as $registro) {
          $labels->push($registro->nomsucu);
          $valores->push($registro->ventas->count());
         }
         $chart->labels($labels);
         $dataset = $chart->dataset('VENTAS', 'doughnut', $valores); 
         $dataset->backgroundColor(collect([
           '#7158e2','#3ae374', '#ff3838','#FFFF00','#808000','#FF00FF','#FF6347','#FA8072','#FFC0CB','#EE82EE','#4B0082','#7d5fff','#32ff7e','#3f8ec7','#3d6530','#93f916','#9cf8d0','#00594f','#532b23','#ffff99'
        ]));
        $dataset->color(collect(['#7d5fff','#32ff7e', '#ff4d4d']));
        return view('Graficos/GraVentasSucursal')->with([
        	'menus'=>$menu->retornarMenu(),
        	'chart'=>$chart,
        	'sucursales'=>$sucursales,
        	'filtro'=>$filtro
        ]);
    }

    public function filtroGVCaja(Request $request){
    	 $tiempo = $request['fecha'];
    	 $filtro = $request;

    	 $menu = new HomeController();
         $sucursales = Sucursal::get();
         $sucursal = $request['sucursales'];
         if($sucursal=='TODAS'){
         	$nomsucu = 'TODAS';
         }
         else{
         	$nomsucu = Sucursal::where('codsuc','=',$sucursal)->first()->nomsucu;
         }


    	 $chart = new Ventas();
    	
    	 Carbon::setWeekStartsAt(Carbon::MONDAY);

    	 if($tiempo=='M'){
    	 	$chart->title("VENTAS DEL MES POR CAJA EN SUCURSAL: ".$nomsucu, 18);
    	 	if($sucursal == 'TODAS'){
    	 	  $registros = Caja::with('ventas')->whereHas('Ventas',function($q) use($sucursal){
              $q->whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month);
              })->get();
    	 	}
    	 	else{
    	 	  $registros = Caja::with('ventas')->whereHas('Ventas',function($q) use($sucursal){
              $q->whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->where('codsuc','=',$sucursal);
              })->get();
    	 	}
    	 }
    	 elseif($tiempo=='S'){
    	 	$chart->title("VENTAS DE LA SEMANA POR CAJA EN SUCURSAL: ".$nomsucu, 18);
    	 	if($sucursal == 'TODAS'){
    	 	 $registros = Caja::with('ventas')->whereHas('Ventas',function($q) use($sucursal){
             $q->whereBetween('created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()]);
              })->get();
    	 	}
    	 	$registros = Caja::with('ventas')->whereHas('Ventas',function($q) use($sucursal){
            $q->whereBetween('created_at', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])->where('codsuc','=',$sucursal);
            })->get();
    	 }
    	 else{
    	 	$chart->title("VENTAS DEL AÑO POR CAJA EN SUCURSAL: ".$nomsucu, 18);
    	 	if($sucursal=='TODAS'){
    	 	 $registros = Caja::with('ventas')->whereHas('Ventas',function($q) use($sucursal){
             $q->whereYear('created_at', Carbon::now()->year);
             })->get();
    	 	}
    	 	else{
    	 	 $registros = Caja::with('ventas')->whereHas('Ventas',function($q) use($sucursal){
             $q->whereYear('created_at', Carbon::now()->year)->where('codsuc','=',$sucursal);
             })->get();
    	 	}
    	 }
         
 
        $labels = collect();
        $valores = collect();
        $coloresFondo = collect();

        $cont = 0;
        foreach ($registros as $registro) {
          $labels->push($registro->descaj);
          $valores->push($registro->ventas->count());
         }
         $chart->labels($labels);
         $dataset = $chart->dataset('VENTAS', 'doughnut', $valores); 
         $dataset->backgroundColor(collect([
            '#7158e2','#3ae374', '#ff3838','#FFFF00','#808000','#FF00FF','#FF6347','#FA8072','#FFC0CB','#EE82EE','#4B0082','#7d5fff','#32ff7e'
        ]));
        $dataset->color(collect(['#7d5fff','#32ff7e', '#ff4d4d']));
        return view('Graficos/GraVentasCajas')->with([
        	'menus'=>$menu->retornarMenu(),
        	'chart'=>$chart,
        	'sucursales'=>$sucursales,
        	'filtro'=>$filtro
        ]);
    }

    public function indexVentasAnuales(){
        $menu = new HomeController();
         $suc = Auth::user()->codsuc_reg;
         $sucursales = Sucursal::get();
         $chart = new Ventas();
         $chart->title("VENTAS ANUALES", 18);
         Carbon::setWeekStartsAt(Carbon::SUNDAY);
/*
         $registros = Fafactur::with('ventas')->whereHas('Ventas',function($q) use($suc){
            $q->whereYear('created_at', Carbon::now()->year)->where('codsuc','=',$suc);
         })->groupby('month')->get();*/

         $registros = Fafactur::where('codsuc','=',$suc)->whereYear('created_at', Carbon::now()->year)->get()->groupBy(function($val) {
               return Carbon::parse($val->created_at)->format('m');
           });


        $labels = collect();
        $valores = collect();
        $coloresFondo = collect();

       /* $cont = 0;*/
        foreach ($registros as $registro => $mes) {
            $i=1;
            $i = $registro;

          switch ($i) {
            case 01:
              $labels->push('Enero');
            break;
            case 02:
               $labels->push('Febrero');
            break;
            case 03:
               $labels->push('Marzo');
            break;
             case 04:
              $labels->push('Abril');
            break;
            case 05:
               $labels->push('Mayo');
            break;
            case 06:
               $labels->push('Junio');
            break;
            case 07:
              $labels->push('Julio');
            break;
            case '08':
               $labels->push('Agosto');
            break;
            case '09':
               $labels->push('Septiembre');
            break;
            case 10:
              $labels->push('Octubre');
            break;
            case 11:
               $labels->push('Noviembre');
            break;
            case 12:
               $labels->push('Diciembre');
            break;
          }
          /*$i += 1;*/
        /*  $labels->push($registro->descaj);*/
          $valores->push($mes->count());
         }
         $chart->labels($labels);
         $dataset = $chart->dataset('VENTAS', 'line', $valores); 
         $dataset->backgroundColor(collect([
           '#7158e2','#3ae374', '#ff3838','#FFFF00','#808000','#FF00FF','#FF6347','#FA8072','#FFC0CB','#EE82EE','#4B0082','#7d5fff','#32ff7e','#3f8ec7','#3d6530','#93f916','#9cf8d0','#00594f','#532b23','#ffff99'
        ]));
        $dataset->color(collect(['#7d5fff','#32ff7e', '#ff4d4d']));
        return view('Graficos/VentasAnuales')->with([
            'menus'=>$menu->retornarMenu(),
            'chart'=>$chart,
            'sucursales'=>$sucursales,
            'suc'=>$suc
        ]);
    }

    public function filtroGVAnual(Request $request){
        $filtro = $request;

         $menu = new HomeController();
         $suc = Auth::user()->codsuc_reg;
         $sucursales = Sucursal::get();
         $sucursal = $request['sucursales'];
         if($sucursal=='TODAS'){
            $nomsucu = 'TODAS';
         }
         else{
            $nomsucu = Sucursal::where('codsuc','=',$sucursal)->first()->nomsucu;
         }


         $chart = new Ventas();
         $titu = '';
         
          Carbon::setWeekStartsAt(Carbon::MONDAY);
          if($sucursal == 'TODAS'){
            $registros = Fafactur::where('codsuc','=',$suc)->whereYear('created_at', Carbon::now()->year)->get()->groupBy(function($val) {
               return Carbon::parse($val->created_at)->format('m');
           });
               $titu = "EN TODAS LAS SUCURSALES";

            }
          else{
            $registros = Fafactur::where('codsuc','=',$sucursal)->whereYear('created_at', Carbon::now()->year)->get()->groupBy(function($val) {
               return Carbon::parse($val->created_at)->format('m');
           });
            $titu = "EN SUCURSAL ".$nomsucu;

            }

        $labels = collect();
        $valores = collect();
        $coloresFondo = collect();

       /* $cont = 0;*/
        foreach ($registros as $registro => $mes) {
            $i=1;
            $i = $registro;

          switch ($i) {
            case 01:
              $labels->push('Enero');
            break;
            case 02:
               $labels->push('Febrero');
            break;
            case 03:
               $labels->push('Marzo');
            break;
             case 04:
              $labels->push('Abril');
            break;
            case 05:
               $labels->push('Mayo');
            break;
            case 06:
               $labels->push('Junio');
            break;
            case 07:
              $labels->push('Julio');
            break;
            case '08':
               $labels->push('Agosto');
            break;
            case '09':
               $labels->push('Septiembre');
            break;
            case 10:
              $labels->push('Octubre');
            break;
            case 11:
               $labels->push('Noviembre');
            break;
            case 12:
               $labels->push('Diciembre');
            break;
          }
          /*$i += 1;*/
        /*  $labels->push($registro->descaj);*/
          $valores->push($mes->count());
         }
         $chart->labels($labels);
         $dataset = $chart->dataset('VENTAS ANUALES '.$titu, 'line', $valores); 
         $dataset->backgroundColor(collect([
           '#7158e2','#3ae374', '#ff3838','#FFFF00','#808000','#FF00FF','#FF6347','#FA8072','#FFC0CB','#EE82EE','#4B0082','#7d5fff','#32ff7e','#3f8ec7','#3d6530','#93f916','#9cf8d0','#00594f','#532b23','#ffff99'
        ]));
        $dataset->color(collect(['#7d5fff','#32ff7e', '#ff4d4d']));
        return view('Graficos/VentasAnuales')->with([
            'menus'=>$menu->retornarMenu(),
            'chart'=>$chart,
            'sucursales'=>$sucursales,
            'suc'=>$suc
        ]);
    }
}
