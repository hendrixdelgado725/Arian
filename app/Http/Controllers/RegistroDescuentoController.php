<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\Fadescto;
use App\Fafactuart;
use App\User;
use Helper;
use Exception;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
use App\permission_user;
use App\Segususuc;

class RegistroDescuentoController extends Controller
{
    //
    private $guard;
    public function __construct(Guard $guard)
    {
        $this->middleware('auth');
        $this->guard = $guard;
    }

    /*Función que ejecuta la inserción de los archivos en la base de datos*/
    public function create(Request $request)
    {
        $valor=request()->all();
        
        $sucuse_reg=Auth::user()->getsucUse()->codsuc;
        $sep=explode('-', $sucuse_reg);
        $request->validate([
            'descripcion'=>'required|string|min:5|max:100',
            'valor'=> 'required|numeric'
        ]);

        try{
            
            $search = Fadescto::where('mondesc',$valor['valor'])->get();
            if($search->isNotEmpty()) throw new Exception("Este porcentaje esta registrado, registre otro o busque y editelo", 500);
            
            $dsc=Fadescto::withTrashed()->get()->count();
            $codigoid=($dsc+1).'-'.$sep[1];

            $ultCoddesc=Fadescto::Orderbydesc('coddesc')->first();
            $coddesc= ($ultCoddesc ? str_pad($ultCoddesc->coddesc+1, 4, '0', STR_PAD_LEFT) : '0001');




            Fadescto::create(
                [
                    'coddesc' => $coddesc,
                    'desdesc' => $valor['descripcion'],
                    'tipdesc' => 'P',
                    'mondesc' => $valor['valor'],
                    'diasapl' => 0,
                    'codigoid' => $codigoid,
                    'codsuc_reg' => $sucuse_reg,
                    'codsuc' => $sucuse_reg
                ]
            );
            Session::flash('success', 'El Descuento '.$valor['descripcion'].', se registro Exitosamente');
            return redirect()->route('listaDescuento');
        }catch(Exception $e){
            Session::flash('error', 'Se presentó un problema en el registro, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
            return redirect()->route('listaDescuento');
        }
    }

    /*Funcion que realiza la edicion de los campos*/
    public function update(Request $request, $id)
    {
        $valor=request()->all();
        $sep=explode('|', $id);
        
        $request->validate([
            'descripcion'=>'required|string|min:5|max:100',
            'valor'=> 'required|numeric'
        ]);

        try{
            // $descuento= Fadescto::findOrFail($id);
            // $actualiza=Fadescto::findOrFail($id)->update(
            $actualiza=Fadescto::where([['id',$sep[0]],['codigoid',$sep[1]]])->first();


                    $actualiza->desdesc=$valor['descripcion'];
                    $actualiza->mondesc=$valor['valor'];
                    $actualiza->save();
            
            Session::flash('success', 'El Descuento '.$valor['descripcion'].' se actualizó Exitosamente');
            return redirect()->route('listaDescuento');
        }catch(Exception $e){
            Session::flash('error', 'Se presentó un problema en la Edición, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
            return redirect()->route('listaDescuento');
        }

    }

    /*Función para  ingresar a la edición del archivo*/
    public function edit ($id)
    {
        $menu = new HomeController();
        $desc=Helper::desencriptar($id);
        $sep=explode('-', $desc);
        $ID= (int)$sep[1];
        // $descuento = Fadescto::where('id',$sep[0])->where('codigo_id',$sep[1])->first();
        $descuento =Fadescto::findOrFail($ID);
        $accion='edicion';
        return view('/configuracionGenerales/registroEdicionDescuento')->with('menus',$menu->retornarMenu())->with('descuento',$descuento)->with('accion',$accion);
    }

    /*funcion de eliminar el registro*/
    public function delete ($id)
    {
        $desc=Helper::desencriptar($id);
        $sep=explode('|', $desc);
        $ID=(int)$sep[1];
        $codig=(string) $sep[2];
        $verifica=Fadescto::findOrFail($ID);
        $comprobar=Fafactuart::where('coddescuento',$codig)->first();

        if($comprobar)
        {
            return response()->json([
                'titulo' => 'El Descuento no puede ser eliminado, posee factura Asociada'
            ]);
        }
        else
        {
            $verifica->delete();
        
            return response()->json([
                'titulo' => 'El descuento se elimino'
            ]);
        }
    }

    /*Vista que genera el listado inicial para la carga de la informaci{on*/
    public function listado()
    {

    	$menu = new HomeController();
    	$descuento= Fadescto::orderBy('id','DESC')->where('deleted_at',NULL)->paginate(8);
    	return view('/configuracionGenerales/descuento')->with('menus',$menu->retornarMenu())->with('descuento',$descuento);
    }

    /*Boton registrar que aparece en el listado*/
    public function nuevoDescuento()
    {
        $menu = new HomeController();
        $accion='registro';
        return view('/configuracionGenerales/registroEdicionDescuento')->with('menus',$menu->retornarMenu())->with('accion',$accion);
    }

    /*busqueda de filtro*/
    public function filtro(Request $request)
    {
        $menu = new HomeController();

        $descuento= Fadescto::where('deleted_at',NULL)->where('desdesc','ilike','%'.$request->filtro.'%')
        ->OrWhere('mondesc','ilike','%'.$request->filtro.'%')->OrWhere('id','ilike','%'.$request->filtro.'%')->paginate(8);


        return view('/configuracionGenerales/descuento')->with('menus',$menu->retornarMenu())->with('descuento',$descuento);
    }

}
