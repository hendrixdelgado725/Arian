<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\MonedaRequest;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\Famoneda;
use Helper;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Contracts\Auth\Guard;
use App\permission_user;
use App\Segususuc;

class MonedaController extends Controller
{

  private $guard;
  public function __construct(Guard $guard)
  {
    $this->middleware('auth');
    $this->guard = $guard;
  }
   public function index(){

    $menu = new HomeController();
    $monedas=Famoneda::orderBy('id','DESC')->paginate(8);
    
    return view('/configuracionGenerales/moneda')->with('menus',$menu->retornarMenu())->with('monedas',$monedas);
   }

   public function nuevaMoneda(){
    $menu = new HomeController();
    return view('/configuracionGenerales/registroMoneda')->with('menus',$menu->retornarMenu());
   }

   public function create(MonedaRequest $request){

            // $pre = '';
            
          try{
            $guion='-';
            $cont = Famoneda::withTrashed()->get()->count();
            $cont = $cont+1;

            // $suc = Auth::user()->codsuc_reg;
            $suc=Auth::user()->getCodigoActiveS();
            $codsuc_reg = explode('-',$suc);
            $cod = $cont.$guion.$codsuc_reg[1];
            \DB::beginTransaction();
            $comprobar=Famoneda::withTrashed()->where('nomenclatura',$request['nomenclatura'])->first();

            if($comprobar)
            {
              if($comprobar['deleted_at']==NULL)
              {
                  throw new \Exception(' La nomeclatura '.$request['nomeclatura'].' posee un registro Activo en el sistema, verificar información.');
              }
              else
              {
                $comprobar->update([
                  'nombre'=>$request['nombre'],
                  'codsuc'=>$suc,
                  'activo'=>false,
                  'deleted_at' => null
                ]);

            
                \DB::commit();
                Session::flash('success', 'la moneda '.$request['nombre'].', se registro Exitosamentes');
                return redirect('/modulo/ConfiguracionGenerales/Moneda');
              }
            }
            
               Famoneda::create([
                'nombre'=>$request['nombre'],
                'nomenclatura'=>$request['nomenclatura'],
                'codigoid'=>$cod,
                'codsuc'=>$suc,
                'activo'=>false
            ]);
                \DB::commit();

          }catch(\Exception $e){
                \DB::rollback();
                Session::flash('error', $e->getMessage());

              return redirect()->route('nuevaMoneda');              
          }

      Session::flash('success', 'la moneda '.$request['nombre'].', se registro Exitosamente');
      return redirect('/modulo/ConfiguracionGenerales/Moneda');
   }

   public function edit($id){
      $menu = new HomeController();
      $desc=Helper::desencriptar($id);
      $sep  = explode('-',$desc);
      $ID= (int)$sep[1];
      $moneda = Famoneda::find($ID);

      return view('/configuracionGenerales/editarMoneda')->with('menus',$menu->retornarMenu())->with('moneda',$moneda)
            ->with('sec',Segususuc::where('loguse',Auth::user()->loguse)->where('codsuc',session('codsuc'))->first()->getSucursal->nomsucu);

   }

   public function update(MonedaRequest $request, $id){
       
       $moneda = Famoneda::find($id);
       $moneda->nombre = $request->nombre;
       $moneda->nomenclatura = $request->nomenclatura;
       $moneda->save();

       Session::flash('success', 'La moneda '.$request['nombre'].', se actualizó Exitosamente');
       return redirect('/modulo/ConfiguracionGenerales/Moneda');
   }

   public function delete($id){
       $desc=Helper::desencriptar($id);
       $sep  = explode('-',$desc);
       $ID= (int)$sep[1];
       $moneda = Famoneda::find($ID);

       $registroPrecios = Famoneda::where('id','=',$ID)->with('precios')->has('precios')->get();
       $registroMonedaTasa = Famoneda::where('id',$ID)->with('tasaM1')->has('tasaM1')->get();
       $registroMonedaTasa2 = Famoneda::where('id',$ID)->with('tasaM2')->has('tasaM2')->get();
       if($registroPrecios->isEmpty() && $registroMonedaTasa->isEmpty() && $registroMonedaTasa2->isEmpty()){
           $moneda->delete();
            return response()->json([
            'titulo' => 'Exito se elimino'
            ]);
       }
     else{
        return response()->json([
            'titulo' => 'error Esta moneda no puede ser eliminado'
            ]);
        }

       

   }

   Public function filtro(Request $request){
    $menu = new HomeController();
    $monedas = Famoneda::where('nombre','ilike','%'.$request->filtro.'%')->orWhere('nomenclatura','ilike','%'.$request->filtro.'%')->paginate(8);
   
    
    return view('/configuracionGenerales/moneda')->with('menus',$menu->retornarMenu())->with('monedas',$monedas);
   }

   public function changeStatusMoneda(Request $request)
   {
      #dd($request->all());
      try{
        \DB::beginTransaction();
      $moneda= Famoneda::
          when($request->input('status') === 'desactivar',function($q) use ($request) 
          {
              return $q->where('activo',true)->where('nombre',$request->input('moneda'));

          })->when($request->input('status') === 'activar',function($q) use ($request)
          {
              return $q->where('activo',true);

          })->first();
            
          if($moneda === null){
              $monedaUpdate = Famoneda::where('nombre',$request->input('moneda'))->update(['activo' => true]);
          
          }else if($moneda !== null){
            //dd($moneda);
            $monedaUpdate = Famoneda::where('nombre',$moneda->nombre)
              ->update([
                  'activo' => false
              ]);
              
            $monedaUpdates = Famoneda::where('nombre',$request->input('moneda'))->update(['activo' => true]);
          }

        \DB::commit();
          Session::flash('success',"Se ha activado la moneda ".$request->input('moneda'));
        return redirect()->route('buscarMoneda');  
          
          
        
      }catch(\Exception $e){
        \DB::rollback();
        Session::flash('error',$e->getMessage());
        return redirect()->route('buscarMoneda');
      }
   }
}
/* $moneda= Famoneda::when($request->moneda === 'DOLLAR',function($q) use ($request)
          {
              return $q->where('activo',true)->where('nombre',$request->moneda);

          })->when($request->moneda === 'BOLIVAR',function($q) use ($request)
          {
              return $q->where('activo',true)->where('nombre',$request->moneda);

          })->when($request->moneda === 'BOLIVAR DIGITAL',function($q) use ($request)
          {
              return $q->where('activo',true)->where('nombre',$request->moneda);

          })*/