<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\User;
use App\Sucursal;
use App\Nphojint;
use App\Segususuc;
use App\rolesusuarios;
use Helper;
use Exception;
use Session;
use Illuminate\Support\Facades\Auth;
// use Route;

class UsuariosController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*Función que ejecuta la inserción de los archivos en la base de datos*/
    public function create(Request $request)
    {
        $valor=request()->all();
        $sucuse_reg=Auth::user()->getsucUse()->codsuc;
        $sep= explode('-', $sucuse_reg);
        $url=$_SERVER['HTTP_REFERER'];
        
        $request->validate([
            'cedularif'=>'required|string|min:5|max:10',
            'nombre'=>'required|string|min:10|max:250',
            'email'=> 'required|string|min:10|max:250',
            'usuario' => 'required|string|min:6|max:30',
            'password' => 'required|string|min:6|max:30',
            'password_confirmation' => 'required|string|min:6|max:30'
        ]);

        if($valor['password']!=$valor['password_confirmation'])
        { 
            $usuario=request()->all();
            Session::flash('error', 'Las Contraseñas NO coinciden, Verifique Información');
            return redirect()->route('nuevoUsuario')->with('usuario',$usuario);
        }
        else
        {
            try{

                #\DB::beginTransaction();
                $usu=User::get()->count();
                $codigoid= ($usu+1).'-'.$sep[1];

                $clave='md5'.md5(strtoupper($valor['usuario']).$valor['password']);

                User::create(
                    [
                        'loguse' => $valor['usuario'],
                        'nomuse' => $valor['nombre'],
                        'apluse' => 'CI0',
                        'pasuse' => $clave,
                        'cedemp' => $valor['cedularif'],
                        'diremp' => $valor['direccion'],
                        'telemp' => $valor['telefono'],
                        'feccad' => $valor['fecha'],
                        'stablo' => $valor['estatus'],
                        'emaemp' => $valor['email'],
                        'codigoid' => $codigoid,
                        'codsuc_reg' => $sucuse_reg,
                        'codroles' => $valor['rol']
                    ]
                );
                
                $sucursales= array_filter($valor['sucur']);
                foreach ($sucursales as $suc)
                {
                    $ultid2 = Segususuc::get()->count();
                    $codigo_ususuc = ($ultid2+1).'-'.$sep[1];
                    Segususuc::create(
                    [
                        'loguse' => $valor['usuario'],
                        'codsuc' => $suc,
                        'codusuario' => $codigoid,
                        'codigoid' => $codigo_ususuc,
                        'codsuc_reg' => $sucuse_reg,
                        'activo' => 'TRUE',
                        'codroles' => $valor['rol']
                    ]
                    );        
                }
                #\DB::commit();
                Session::flash('success', 'El Usuario '.$valor['usuario'].' se registrado Exitosamente');
                return redirect()->route('listaUsuario');
            }catch(Exception $e){
               # \DB::rollback();
                return redirect()->route('listaUsuario')->with('error','Se presentó un problema en el registro, Comuniquese con el Administrador del Sistema'.$e->getMessage());
            }
        }
    }

    /*Función para  ingresar a la edición del archivo*/
    public function edit($id)
    {   
        $menu = new HomeController();
        $desc=Helper::desencriptar($id);
        $sep=explode('-', $desc);
        $ID= (int) $sep[1];

        $usuario=User::where('id',$ID)->whereNULL('deleted_at')->first();
        
        $ususucursal=Segususuc::where([
            ['loguse',$usuario->loguse],
          //  ['codusuario',$usuario->codigoid],
            ['deleted_at',NULL]
        ])->whereNotNull('codsuc')->get();
        
       /*  dd($usuario);*/
            
        $sucursal= Sucursal::where([['deleted_at',NULL]])->whereNotNull('codsuc')->get();
        
        $droles = rolesusuarios::where('deleted_at',null)->where('codroles','!=',$usuario->codroles)->get();
           // dd($droles);  
        $accion='edicion';

        return view('/seguridad/registroEdicionUsuario')->with([
            'menus'=> $menu->retornarMenu(),
            'usuario' => $usuario,
            'accion' => $accion,
            'sucursal' => $sucursal,
            'ususucursal' => $ususucursal, 
            'droles' => $droles,
            'sucujson'=> json_encode($sucursal),
        ]);

    }

    public function editcontrasena($id)
    {
        // dd($id);
    }

    /*Funcion que realiza la edicion de los campos*/
    public function update(Request $request, $id)
    {
        $valor=request()->all();
        
        $sep=explode('|', $id);
        
        $request->validate([
            'nombre'=>'required|string|min:10|max:250',
            'email'=> 'required|string|min:10|max:250',
           
            /* 'password' => 'required|string|min:6|max:30',
            'password_confirmation' => 'required|string|min:6|max:30' */
        ]);

        $rand  = rand(1, 9999);
        $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
        $cod=Helper::EncriptarDatos($nrand.'-'.$sep[0]);

        

        if($request->sucur==null)
        {
            Session::flash('error', 'El usuario debe poseer al menos una Sucursal Asociada');
            return redirect()->route('listaUsuario');  
        }

        

        /* if($valor['password']!=$valor['password_confirmation'])
        { 
            Session::flash('error', 'Las Contraseñas NO coinciden, Verifique Información');
            return redirect()->route('editarUsuarioForm',$cod);
        } */
        else
        {
            try{
                if($request->rol === null) throw new Exception("Debes Ingresar el rol");

                // \DB::beginTransaction();
                $fecha=$valor['fecha'];
                if($valor['estatus']=='N' && $fecha==NULL)
                {
                    $fecha=date('Y-m-d');

                }
                elseif($valor['estatus']=='S')
                {
                    $fecha=null;
                }

                $use=User::where('id',$sep[0])->first();

                $codsuc = '0001';

                if($use->codigoid === null){
                    $sep[1] = $use->id."-".$codsuc;
                    User::where('id',$sep[0])->update([
                        'codigoid' => $use->id."-".$codsuc,
                        'codsuc_reg' => session('codsuc')
                    ]);
                }

                if($request->password !== null && $request->password_confirmation !== null){
                    $clave='md5'.md5(strtoupper($use->loguse).$valor['password']);

                    $actualizar=User::where('id',$sep[0])->update([
                        'nomuse'=>$valor['nombre'],
                        'diremp'=>$valor['direccion'],
                        'telemp'=>$valor['telefono'],
                        'emaemp'=>$valor['email'],
                        'stablo'=>$valor['estatus'],
                        'feccad'=>$fecha,
                        'codroles'=> $valor['rol'],
                        'pasuse'=>$clave
                    ]);
                }else{
                    $actualizar=User::where('id',$sep[0])->update([
                        'nomuse'=>$valor['nombre'],
                        'diremp'=>$valor['direccion'],
                        'telemp'=>$valor['telefono'],
                        'emaemp'=>$valor['email'],
                        'stablo'=>$valor['estatus'],
                        'feccad'=>$fecha,
                        'codroles'=> $valor['rol'],
                    ]);
                }

                /************ELIMINA LOGICAMENTE LA SUCURSAL DESACTIVADA PARA EL USUARIO*******************/
                if ($request->borrados!=null){
                    $borrados=explode(',', $request->borrados);
                    foreach($borrados as $borrar){
                        $borrados=Segususuc::findOrFail($borrar)->update(['deleted_at'=>date('Y-m-d h:m:i')]);
                    };
                }

                if ($request->sucur!=null)
                {
                    
                    foreach ($request->sucur as $suc) {
                        
                        $verificar=Segususuc::where([
                            ['deleted_at',null],
                           // ['codusuario',$sep[1]],
                            ['codsuc',$suc],
                            ['loguse',$use->loguse]
                        ])->first();
                        
                        $sucuse_reg=session('codsuc');//sucursal autenticado
                        
                        if($verificar==null && $suc!=NULL)
                        {
                            //dd($use->id."-".$codsuc);
                            
                            $sep1= explode('-', $sucuse_reg);
                            $ultid2 = Segususuc::count();
                            $codigo_ususuc = ($ultid2+1).'-'.$sep1[1];
                            Segususuc::create(
                            [
                                'loguse' => $use->loguse,
                                'codsuc' => $suc,
                                'codusuario' => $use->id."-".$codsuc,
                                'codigoid' => $codigo_ususuc,
                                'codsuc_reg' => $sucuse_reg,
                                'activo' => 'TRUE',
                                'codroles' => $valor['rol']
                            ]
                            );  
                            
                        }else{
                           
                            $verificar=Segususuc::where([
                                ['deleted_at',null],
                               // ['codusuario',$sep[1]],
                                ['codsuc',$suc],
                                ['loguse',$use->loguse]
                            ])->update(['codroles' => $valor['rol'],'codsuc' => $sucuse_reg]);
                        }


                    }
                }
                // \DB::commit();
                Session::flash('success', 'El Usuario '.$use->loguse.' se edito Satisfatoriamente');
                return redirect()->route('listaUsuario');    
            }catch(Exception $e){
                // \DB::rollback();
                Session::flash('error', 'Se presentó un problema en la Edición, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
                return redirect()->route('listaUsuario');    
            }
        }
        

    }

    /*Vista que genera el listado inicial para la carga de la informaci{on*/
    public function listado()
    {
    	$menu = new HomeController();
    	$usuario= User::orderBy('id','DESC')->where('deleted_at',NULL)->paginate(8);
        // dd($usuario[0]->getNombreSucursal());
    	return view('/seguridad/usuario')->with('menus',$menu->retornarMenu())->with('usuario',$usuario);
    }

    /*Funcion que realiza la edicion de los campos*/
    public function updatePassw(Request $request, $id)
    {
        $valor=request()->all();
        $sep=explode('|', $id);
        $usuario=Auth::user();
        $menu = new HomeController();
        
        $request->validate([
            'password' => 'required|string|min:8|max:30',
            'password_confirmation' => 'required|string|min:8|max:30'
        ]);

        if($request->all()['password'] !== $request->all()['password_confirmation']){
            Session::flash('error', 'Contraseña no coiniciden');
            return view ('/seguridad/editarContrasena')->with([
                'menus' => $menu->retornarMenu(),
                'usuario' => $usuario
            ]);
        }

        $npperm= array('123456','1234567','12345678','123456789',$usuario->cedemp,'qwerty','000000','password','abc123','ABC123','Abc123','987654','654321');
        if (in_array($valor['password'],$npperm))
        {
            Session::flash('error', 'Contraseña no permitida , Verifique Información');
            return view ('/seguridad/editarContrasena')->with([
                'menus' => $menu->retornarMenu(),
                'usuario' => $usuario
            ]);
        }

         if($valor['password']!=$valor['password_confirmation'])
        {   
            Session::flash('error', 'Las Contraseñas NO coinciden, Verifique Información');
            return view ('/seguridad/editarContrasena')->with([
                'menus' => $menu->retornarMenu(),
                'usuario' => $usuario
            ]);
        }
        else{
            try{
                $use=User::where([['id',$sep[0]],['codigoid',$sep[1]]])->first();
                $clave='md5'.md5(strtoupper($use->loguse).$valor['password']);
                $actualizar=User::where([['id',$sep[0]],['codigoid',$sep[1]]])->update(['pasuse'=>$clave]);
                Session::flash('success', 'Se editó Satisfatoriamente');
                return redirect('/modulo/Seguridad/CambPassword'); 
            }catch(Exception $e){
                Session::flash('error', 'Se presentó un problema en la Edición, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
                return redirect('/modulo/Seguridad/CambPassword'); 
            }
        }  
    }

    /*Boton registrar que aparece en el listado*/
    public function nuevoUsuario()
    {
        $menu = new HomeController();
        $sucursal= Sucursal::where([['deleted_at',NULL]])->whereNotNull('codsuc')->get();
        $roles = rolesusuarios::where('deleted_at',null)->get();

        $accion='registro';
        return view('/seguridad/registroEdicionUsuario')->with([
            'menus' => $menu->retornarMenu(),
            'accion' => $accion,
            'sucursal' => $sucursal,
            'roles' => $roles
            ]);
    }

    public function editPassw()
    {
        $usuario=Auth::user();
        $menu = new HomeController();
        return view ('/seguridad/editarContrasena')->with([
            'menus' => $menu->retornarMenu(),
            'usuario' => $usuario        ]);
    }

    /*busqueda de filtro*/
    public function filtro(Request $request)
    {
        $menu = new HomeController();

        $usuario=User::where('loguse','ilike','%'.$request->filtro.'%')->OrWhere('id','ilike','%'.$request->filtro.'%')->OrWhere('cedemp','ilike','%'.$request->filtro.'%')->OrWhere('nomuse','ilike','%'.$request->filtro.'%')->OrWhere('stablo','ilike','%'.$request->filtro.'%')->paginate(8);

        return view('/seguridad/usuario')->with('menus',$menu->retornarMenu())->with('usuario',$usuario);
    }


    public function ajax()
    {
        $data = request();
        $doc= $data['valor'];
        //comprueba si el usuario a registrar es empleado para extraer los datos esenciales
        if ($data['ajax']==1)
        {
            $empleado= Nphojint::where([['codemp',$doc],['deleted_at',NULL]])->first();
            return response()->json(['options'=>($empleado?$empleado:'0')]);
        }
        //comprueba la  cedula ingresada tenga usuario registrado en el sistema
        if($data['ajax']==2)
        {   
            $usua=User::where([['cedemp',$doc],['deleted_at',NULL]])->first();
            return response()->json(['options'=>($usua?$usua:'0')]);   
        }

        if ($data['ajax']==3)
        {
            $logu=User::where([['loguse',$doc],['deleted_at',NULL]])->first();
            return response()->json(['options'=>($logu?$logu:'0')]);      
        }

    }

}
