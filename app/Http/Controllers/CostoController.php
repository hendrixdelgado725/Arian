<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Nphojint;
use App\Npcargos;
use App\Models\Estado;
use App\Models\Costos;
use App\Http\Requests\CostoRequest;
use Session;
use Helper;

class CostoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     */

    public function retornarMenu()
    {
        $yaml = new Parser();
        $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
        return $menu['menu'];
    }

    public function index()
    {
        //
        $centros = Costos::orderBy('id', 'desc')->with('responsable', 'encargado', 'estado')->paginate(12);
        $menu = $this->retornarMenu();

        return view('configuracionGenerales.CCosto.costos')
        ->with([
            'menus' => $menu,
            'centros' => $centros        
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estados = Estado::get();
        $cargos = Npcargos::orderBy('id', 'desc')->get();
        $empleados = Nphojint::with('npasicaremp')->where('staemp', '=', 'A')->orderBy('id','DESC')->get();

        #dd($empleados);
    
        $menu = $this->retornarMenu();

        return view('configuracionGenerales.CCosto.createCosto')->with([
            'menus' => $menu,
            'estados' => $estados,
            'cargos' => $cargos,
            'empleados' => $empleados,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CostoRequest $request)
    {
       
        $res = Nphojint::where('codemp', $request->codemp)->with('npasicaremp')->first();

        $enc = Nphojint::where('codemp', $request->cedenc)->with('npasicaremp')->first();
        
        $val = Costos::where('codcen', $request->codcen)->first();

        if($val)
        {

            Session::flash('error', 'Existe un centro registrado con este código');
            return redirect()->route('createCCosto');

        }
        else
        {

            $request->validate([
                'codcen' => 'required | min: 3 | max: 4',
                'descen' => 'required | min: 10 | max: 100',
                'codemp' => 'required | different:cedenc',
                'cedenc' => 'required'
            ]);

            try{
                DB::beginTransaction();

                $ccosto = Costos::create([
                    'codcen' => $request->codcen,
                    'descen' => $request->descen,
                    'dircen' => $request->dircen ?? '',
                    'codpai' => $request->codpai ?? '',
                    'codemp' => $res->codemp,
                    // 'nomcar' => $res->npasicaremp->nomcar ?? '',
                    'cedenc' => $enc->codemp,
                ]);


            DB::commit();
            
            Session::flash('success', 'El centro '.$request->descen.' fue registrado con exito.');
            return redirect()->route('listaCCosto');
            

            }

            catch(Exception $e){
                DB::rollback();
                return redirect()->route('createCCosto')->with('error', $e->getMessage());
                
            }

        }
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $menu = $this->retornarMenu();
        $desc = Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID = (int)$sep[1];

        $centro = Costos::find($ID);
        $estados = Estado::get();
        $cargos = Npcargos::orderBy('id', 'desc')->get();
        $empleados = Nphojint::with('npasicaremp')->where('staemp', '=', 'A')->orderBy('id','DESC')->get();

        return view('configuracionGenerales.CCosto.editCosto')->with([
            'centro' => $centro,
            'menus' => $menu,
            'estados' => $estados,
            'cargos' => $cargos,
            'empleados' => $empleados
        ]);
    }

    /** 
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CostoRequest $request, $id)
    {
        $centro = Costos::findOrFail($id);
        
        $res = Nphojint::with('npasicaremp')->where('codemp', $request['codemp'])->where('staemp', '=', 'A')->first();
        
        $enc = Nphojint::where('codemp', $request['cedenc'])->first();
        
        try {
            //code...
            $centro->codcen = $request['codcen'];
            $centro->descen = $request['descen'];
            $centro->dircen = $request['dircen'] ?? '';
            $centro->codpai = $request['codpai'] ?? '';
            $centro->codemp = $res->codemp;
            $centro->cedenc = $enc->codemp;

            $centro->save();
            
            Session::flash('success', 'El Centro de costo '.$centro->descen.' fue editado con éxito.');
            return redirect()->route('listaCCosto');
        } catch (\Throwable $th) {
            Session::flash('error', $th);
            return redirect()->route('listaCCosto');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    
        $desc = Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID = (int)$sep[1];
        $centro = Costos::findOrFail($ID);
        

        $centro->delete();
        if($centro === null){
            return response()->json([
                'titulo' => 'Error eliminando'
            ]);
        }
        return response()->json([
            'titulo' => 'Eliminado con exito'
        ]);
        
    }

    public function filtro(Request $request)
    {
        
        $menu = $this->retornarMenu();
        $centros = Costos::where('descen', 'ilike', '%'.$request->filtro.'%')->orWhere('codcen', 'ilike', '%'.$request->filtro.'%')->orderBy('id', 'desc')->paginate(8);

        return view('configuracionGenerales.CCosto.costos')->with([
            'menus' => $menu,
            'centros' => $centros,
        ]);
    }
}
