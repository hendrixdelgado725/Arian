<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SCProductoRequest;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\Facategoria;
use App\Fasubcategoria;
use Helper;
use Illuminate\Support\Facades\Auth;
use User;
use Session;
use App\Segususuc;

class SCProductoController extends Controller

{

    public function __construct()
    {
        $this->middleware('auth');
        
    }

    public function index(){

     $menu = new HomeController();
     $subcategorias = Fasubcategoria::with('categoria')->orderBy('id','DESC')->paginate(8);
     return view('/configuracionGenerales/subCategoriaProducto')->with('menus',$menu->retornarMenu())
     ->with('subcategorias',$subcategorias);
    }

    public function nuevoSubCat(){
     $menu = new HomeController();
     $categorias = Facategoria::get();
     if($categorias->isEmpty())
     {
        return redirect('/modulo/ConfiguracionGenerales/SCdeProducto')->with(['error' => 'Para iniciar el proceso debe registrar las Categorías en el Sistema']); 
     }
     return view('/configuracionGenerales/registroSCProducto')->with('menus',$menu->retornarMenu())
     ->with('categorias',$categorias);
    }

    public function create(SCProductoRequest $request){
        
            ////Generacion de codigoUnico
            // $pre = '';
            $cont = Fasubcategoria::withTrashed()->get()->count();
            $cont = $cont+1;
            // if($cont<9){
            //     $pre = '00';
            //     $cont = $cont+1;
            // }

            // elseif($cont>9 && $cont<98){
            //     $pre = '0';
            //     $cont = $cont+1;
            // }

            // else{
            //     $cont = $cont+1;
            // }
               $guion ='-';
            // $d=date('ds');
            // $idUser = Auth::user()->id;
            // $suc = Auth::user()->codsuc_reg;
            $suc=Auth::user()->getCodigoActiveS();
            $codsuc_reg = explode('-',$suc);
            $cod = $cont.$guion.$codsuc_reg[1];

            $categoria = Facategoria::where('nombre','=',$request->categoriaP)->first();
            
            $comprobar=Fasubcategoria::withTrashed()->where('nomsubcat',$request['nombreSC'])->whereOr('nomensubcat',$request['nomenSC'])->first();
            if($comprobar)
            {
                if($comprobar['deleted_at']!==NULL)
                {
                    $comprobar->restore();
                    $comprobar->update([
                      'nomsubcat'=>$request['nombreSC'],
                      'nomensubcat'=>$request['nomenSC'],
                      'id_facategoria'=>$categoria->codigoid,
                      'codsuc'=>$suc,
                      'deleted_at' => null
                    ]);
                    Session::flash('success', 'La subCategoría '.$request['nombreSC'].', se registro Exitosamente');
                    return redirect()->route('listaSubCategoriaP');
                }
            }
            

        Fasubcategoria::create([ 
          'nomsubcat'=>$request['nombreSC'],
          'nomensubcat'=>$request['nomenSC'],
          'id_facategoria'=>$categoria->codigoid,
          'codigoid'=>$cod,
          'codsuc'=>$suc,
        ]);

        Session::flash('success', 'La subCategoría '.$request['nombreSC'].', se registro Exitosamente');
        return redirect()->route('listaSubCategoriaP');
    }

    public function edit($id){
        $menu = new HomeController();
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $subcategoria = Fasubcategoria::with('categoria')->find($ID);

        return view('/configuracionGenerales/editarSCProducto')->with('menus',$menu->retornarMenu())->with('subcategoria',$subcategoria);
    }

    public function update(SCProductoRequest $request,$id){
        $comprobar=Fasubcategoria::withTrashed()->where('nomsubcat',$request['nombreSC'])->whereOr('nomensubcat',$request['nomenSC'])->first();
        $suc=Auth::user()->getCodigoActiveS();
        $categoria = Facategoria::where('nombre','=',$request->categoriaP)->first();
        $subcategoria = Fasubcategoria::find($id);
        if($comprobar)
        {
            if($comprobar['deleted_at']!==NULL)
            {
                    $comprobar->restore();
                    $comprobar->update([
                      'nomsubcat'=>$request['nombreSC'],
                      'nomensubcat'=>$request['nomenSC'],
                      'id_facategoria'=>$categoria->codigoid,
                      'codsuc'=>$suc,
                      'deleted_at' => null
                    ]);
                    $subcategoria->delete();
                    Session::flash('success', 'La subCategoría '.$request['nombreSC'].', se registro Exitosamente');
                    return redirect()->route('listaSubCategoriaP');
                }
            }

        $subcategoria->nomsubcat = $request->nombreSC;
        $subcategoria->nomensubcat = $request->nomenSC;
        $subcategoria->save();

        Session::flash('success', 'La subCategoría '.$request['nombreSC'].', se actualizó Exitosamente');
        return redirect()->route('listaSubCategoriaP');
    }

    public function delete($id){
       $desc=Helper::desencriptar($id);
       $sep  = explode('-',$desc);
       $ID= (int)$sep[1];
       $subcategoria = Fasubcategoria::find($ID);
       // $articulos = Fasubcategoria::with('articulos')->where('id','=',$ID)->get();
       $articulos = Fasubcategoria::where('id','=',$ID)->with('articulos')->whereHas('articulos',function($q) use ($subcategoria)
            {
            $q->where('codsubcat',$subcategoria->codigoid);
       })->get();
       
       if($articulos->isEmpty()){
        $subcategoria->delete();
        return response()->json([
            'titulo' => 'Exito se elimino'
        ]);
    }
    else{
        return response()->json([
            'titulo' => 'error Esta subcategoria no puede ser eliminado'
            ]);
        }

    }

    public function filtro(Request $request){
       $menu =  new HomeController();
       $subcategorias = Fasubcategoria::with('categoria')->where('nomsubcat','ilike','%'.$request->filtro.'%')->orWhere('nomensubcat','ilike','%'.$request->filtro.'%')
       ->orWhereHas('categoria',function($q) use ($request){
            $q->where('nombre','ilike','%'.$request->filtro.'%');
       })->orderBy('nomsubcat')->paginate(8);

       return view('/configuracionGenerales/subCategoriaProducto')->with('menus',$menu->retornarMenu())
       ->with('subcategorias',$subcategorias);
    }
}
