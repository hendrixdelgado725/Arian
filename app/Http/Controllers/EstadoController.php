<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pais;
use App\Models\Estado;
use App\Models\Menu;
use Session;
use Exception;
use Helper;
use App\Segususuc;
use Illuminate\Support\Facades\Auth;

class EstadoController extends Controller
{

    public function __construct(){
      $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estados = Estado::with('pais')->orderby('id','desc')->paginate(10);
        return view('pais.estadolist')
                    ->with('estados',$estados)
                        ->with('menus',$this->getmenu());
    }

    public function filtro(Request $request)
    {
        $estados =  Estado::where('nomedo','ilike','%'.$request->filtro.'%')->
                      with('pais')->orWhereHas('pais', function($q) use($request) {
                        $q->where('nompai','ilike','%'.$request->filtro.'%');
                      })->paginate(10);

        return view('pais.estadolist')
                    ->with('estados',$estados)
                        ->with('menus',$this->getmenu());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('pais.createstado')
                ->with('paises',Pais::get())
                  ->with('menus',$this->getmenu())->with('sec',Segususuc::where('loguse',Auth::user()->loguse)->where('activo','TRUE')->first()->getSucursal->nomsucu);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $maxCaracter = mb_strlen(trim($request->nomedo));
      if($maxCaracter > 30){
        Session::flash('error', 'El nombre del estado tiene mas de 30 caracteres, Verifique la Información.');
        return redirect()->route('estadoCreate');
      } else {
        $request['nomedo'] = strtoupper(Menu::eliminar_tildes($request["nomedo"]));
        $msj = ['fapais_id.required'=> 'Debe Seleccionar un Pais',
                'nomedo.required' => 'Debe insertar un Estado'];
        $request->validate([
          'fapais_id' => 'required',
          'nomedo' => 'required'
        ],$msj);
        $valid =  Estado::where('nomedo','=',$request["nomedo"])->where('fapais_id','=',$request["fapais_id"])->get();
        if(count($valid)> 0){
          Session::flash('error', 'El Estado '.$request['nomedo'].' se encuentra relacionado para el País seleccionado. Verifique la Información.');
          return redirect()->route('estadoCreate');
        }
        $r = Estado::create(['nomedo' => $request['nomedo'],
                            'fapais_id' => $request['fapais_id'],
                            'codigoid' => Estado::withTrashed()->get()->count()+1,
                            'codsuc' => $sucuse_reg=Auth::user()->getsucUse()->codsuc
                            ]);
        if($r) Session::flash('success', 'El Estado '.$request['nomedo'].' se registro Exitosamente');
        else  Session::flash('error', 'No se pudo registrar el Estado');
        
        return redirect()->route('estadoList');
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = Helper::decrypt($id);
        $estado = Estado::find($id);
        $pais = Pais::get();
        return view('pais.editestado')
                  ->with('estado',$estado)
                    ->with('paises',$pais)
                    ->with('menus',$this->getmenu());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     
      $estado = Estado::find($id);  

      if(strlen($request->nomedo) > 30){
        Session::flash('error', 'El nombre del estado tiene mas de 30 caracteres, Verifique la Información.');
      return redirect()->route('estadoShow',$id=Helper::crypt($id));
      } else {
      $request->nomedo = strtoupper(Menu::eliminar_tildes($request->nomedo));
      $msj = ['fapais_id.required'=> 'Debe Seleccionar un Pais',
                'nomedo.required' => 'Debe insertar un Estado'];
        $request->validate([
          'fapais_id' => 'required',
          'nomedo' => 'required'
        ],$msj);
        $valid =  Estado::where('nomedo','=',$request->nomedo)->where('fapais_id','=',$request->fapais_id)->get();
        if(count($valid)>0){
          if($valid->pluck('id')[0] === intval($id)){
          Session::flash('success', 'El registro no fue alterado, se conservan');
          return redirect()->route('estadoList');
          }else{
          Session::flash('error', 'El Estado '.$request['nomedo'].' se encuentra relacionado para el País seleccionado. Verifique la Información.');
          return redirect()->route('estadoShow',$id=Helper::crypt($id));
          }
        }
        $estado->nomedo = $request->nomedo;
        $estado->fapais_id = $request->fapais_id;
        if($estado->save()){
          Session::flash('success', 'El Estado '.$request->nomedo.' ,se actualizó Exitosamente');
          return redirect()->route('estadoList');
        }
      } 

        // se consiguio un repetido
    }

    /**
     * Remove the specified resource from storage.
     *ypr
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyest(Request $request, $id)
    {
        try{
        $id = Helper::decrypt($id);
          if(!$request->ajax())throw new Exception("Not an Ajax Request");
          if(!Estado::candelete($id)->isEmpty()) throw new Exception("No se Puede Eliminar el Estado hay una sucursal creada");
        $estado = Estado::find($id);
        $res = $estado->delete();
        if($res) return response()->json(["exito" => "Estado Eliminado Exitosamente"]);
      }catch(Exception $e){
        return response()->json([
          "error" => $e->getMessage()
        ]);
      }
    }

    public function getestadojson(Request $req ,$id){
      try{
        if(!$req->ajax()) throw new Exception("Not and Ajax Request");
        $result = Estado::where('fapais_id','=',$id)->get();
        return response()->json($result);
      }catch(Exception $e){
        return response()->json([
          'error' => $e->getMessage()
        ]);
      }
    }

    public function getmenu(){
        return $this->menu = \App\Models\Menu::getmenu();
    }
}
