<?php

namespace App\Http\Controllers\Entrada_controlador;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Arr;
use App\Almacen;
use App\Models\serialDisponible;

use App\Caentalm;
use App\Catipent;
use App\Caregart;
use App\Caartalmubimov;
use App\Caartalmubi;
use App\Cadetent;
use App\User;
use App\Tallas;
use App\Sucursal;
use App\Famoneda;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Helper;
use Session;
use Exception;
use Illuminate\Validation\Rule;
use \Fpdf;
use App\Http\Controllers\generadorPDF;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use App\Mail\SolicitudTraspasoMail;
use App\Segususuc;
use App\Caprovee;
use App\Models\SerialEntradas;


class EntradaController extends Controller
{
		protected $temp = [];
		private $serialess = null;
	    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
		//dd(Caentalm::getFull());

			return view('almacen.listadoArticulosEntrada')->with(['menus'=> $this->getmenu(),'entradas' => Caentalm::getFull()]);
		}

		public function filtro(Request $request){

			
			$result = Caentalm::when($request->tipo !== null,function($q) use ($request)
                                {
                                    $q->where('nommov',$request->tipo)                             
                                    ->where('codsuc',session('codsuc'));
                                })
                               ->when($request->filtro !== null,function($q) use ($request)
                               {    
                                 $q->where('codmov',$request->filtro)
                                   ->where('codsuc',session('codsuc'));

                               })->when($request->filtros === null,function($q) use ($request)
                               {
                                   
                                 $q->where('codsuc',session('codsuc'));
                                             
                               })
							   ->select('*',
                                         DB::raw("to_char(fecrcp,'dd/mm/yyyy') as fecrcpnormal"),
                                         DB::raw("(CASE WHEN nommov = 'ENT' THEN 'ENTRADA'
                                                 WHEN nommov = 'SAL' THEN 'SALIDA' END)as movimiento "))
                               ->with('almacenes')
                               ->with('movimientos')
                               ->orderBy('id','asc')
                               ->paginate(10);

							   return view('almacen.listadoArticulosEntrada')->with(['menus'=> $this->getmenu(),'entradas' => $result]);
		}



		public function getcombo(){
			$menu = new HomeController();
			$articulos = \App\Caregart::where('tipreg','F')->where('deleted_at',NULL)->get();
			$alm=Almacen::with('sucursal')
			->whereNotNull('codsuc_asoc')
			->whereNull('deleted_at')
			->sucursal()
			->orderBy('codsuc_asoc','desc')
			->get();
			$alm = $alm->groupBy([
				function($q){
					return $q['sucursal']['nomsucu'];
				}
			]);
			$moneda= Famoneda::get();
			$provee = Caprovee::get();
			if(count($alm) === 0 )Session::flash('warning','Primero debe ingresar un almacen para poder hacer movimientos de inventario');

			if(count($moneda) === 0 )Famoneda::flash('warning','Primero debe ingresar una moneda para poder hacer movimientos de inventario');

			$menu = $this->getmenu();

			return ['articulos' => $articulos , 'alm' => $alm,'menus'=> $menu,'provee' => $provee,'moneda'=>$moneda];

		}

	public function createEntrada(){
		return view('almacen.entrada')->with($this->getcombo());
	}

    public function saveEntrada(Request $request){

			
			

			$request->validate([

				'observacion' => 'required'
			]);
			$serial = $request->movinvent === 'SAL' ? session('seriales') : session('serial');


			$codart = session('codart');
			$codalm2 = session('codalm');

			$validate = Caentalm::where('codmov',$request->entrada)->get()->count();

			if($validate !== 0) throw new Exception("El codigo de movimiento ya existe");

			$art = array_filter($request->articulos);
			$cant = array_filter($request->cantidad);
			$ubi = array_filter($request->ubicacion);
			$tallasx = array_filter($request->tallasArtx);
			// $sucuse_reg=Auth::user()->getsucUse()->codsuc;
			$sucuse_reg=Auth::user()->getCodigoActiveS();
			$sep= explode('-', $sucuse_reg);
			$preunit = array_filter($request->preunit);
			$search= ['/\./','/,/'];
			$replace =['','.'];

			$monto = preg_replace($search,$replace,$request->monrcp);
			
			//dd($art,$cant,$ubi);
			DB::beginTransaction();
			//cuando van a registrar seriales repetidos
			
			if($serial !== null  && $request->movinvent !== 'SAL'){
				//POSITION('ENT' IN ENTRADA) > 0
				$validateIfExist =  serialDisponible::whereIn('seriales',$serial)->where('disponible',true)->where('codalm',$request->codalm)->get();
				
				
				

				if(collect($validateIfExist)->isNotEmpty()){
					session()->forget('seriales');
					session()->forget('serial');
					Session::flash('error','Hay seriales que estan Cargados');
				 	return redirect()->back();
				 }

				 


			}
			
			//cuando los seriales no estan registrado y le van a dar salida
			if($serial !== null && $request->movinvent === 'SAL'){
					
							
			
				foreach ($serial as $key => $value) {
					foreach(session('codart') as $key2 => $value2){
						
						if($key2 === $key){ //comparamos indices de los 2 array
							$validateIfExist = serialDisponible::where('seriales',$value)->where('codart',$value2) //verifica el serial en BD
																 ->where('codalm',$request->codalm)->where('disponible',true)->first(); 
							
							if($validateIfExist === null){

								 session()->forget('seriales');
								 session()->forget('serial');
								 session()->forget('codart');
								 Session::flash('error','No existe el serial para darle salida '. $value.' con el codigo del articulo ' .$value2);
								 return redirect()->back();

							}
						}
					}

				}			
				
			}
			try{
				//if(count($art) === 0 || count($cant) === 0)
				
				$cea=Caentalm::get()->count();
				$codigoID=($cea+1).'-'.$sep[1];
				$codmov = $request->movinvent.'-'.$this->createcodmov();
				$codigoidMov = Caartalmubimov::getCount()+1;
				
				if($serial !== null ){
					$validateIfExist2 = serialEntradas::whereIn('serial',$serial)->select('entrada',\DB::raw("POSITION('$request->movinvent' IN ENTRADA) > 0"))
	    			->where('codalm',$request->codalm)->with('SerialEntradas')->get();

					foreach ($validateIfExist2 as $key => $value) {
						
						if($value->SerialEntradas->status === 'E'){
							session()->forget('seriales');
							session()->forget('serial');
							Session::flash('error','Hay seriales que estan en espera para ser ingresado al inventario');
							return redirect()->back();
						 }


					}
				}
				
					
				
				
				
				foreach ($art as $key => $value) {

					#$tallas = $tallasx[$key] !== 'N/D' ? explode(',',$tallasx[$key]) : null;

					$codalm = $request->movinvent === 'SAL' ? 'codalmfrom' : 'codalmto';
					$codubi = $request->movinvent === 'SAL' ? 'codubifrom' : 'codubito';
					$precio = Caregart::where('codart',$value)->whereHas('faartpvp',function($q){
						$q->where('status','A');
					})->with('faartpvp')->first();

					/*	if($tallas !== null){
							foreach ($tallas as $talla){

								$tosave[] = [
									'codmov' =>$request->entrada,
									'codart' => $value,
									'cantmov' => $cant[$key],
									$codubi => $ubi[$key],
									$codalm =>$request->codalm,
									'tipmov' =>$request->tipmov,
									'created_at' => Carbon::now(),
									'codtalla' => $talla,
									'desmov' => $request->movinvent,
									'codsuc' => $sucuse_reg,
									'preunit' => $preunit[$key] ?? 0,
									'codigoid' => $codigoidMov.'-'.$sep[1]
								];
							}
						}else{*/

							$tosave[] = [
								'codmov' =>$request->entrada,
								'codart' => $value,
								'cantmov' => abs($cant[$key]),
								$codubi => $ubi[$key],
								$codalm =>$request->codalm,
								'tipmov' =>$request->tipmov,
								'created_at' => Carbon::now(),
								'codtalla' => null,
								'desmov' => $request->movinvent,
								'codsuc' => session('codsuc'),
								'preunit' => $preunit[$key] ?? 0,
								'codigoid' => $codigoidMov.'-'.$sep[1]
							];
							if($precio === null) throw new Exception('Articulo falta precio vaya al modulo de articulo y coloque el precio');
							Cadetent::create([
								'rcpart' => $codmov,
								'codart' => $value,
								'codalm' => $request->codalm,
								'canrec' => $cant[$key],
								'created_at' => Carbon::now(),
								'codsuc' => session('codsuc'),
								'codubi' => $ubi[$key],
								'cosart' => $precio->faartpvp->pvpart ?? '0.00',
								'montot' => ($precio->faartpvp->pvpart * $cant[$key])
							]);
						//}
						//dd($tosave);
					}
					//   dd($request);
					$entrada = new \App\Caentalm;
					$entrada->codmov = $request->entrada;
					$entrada->rcpart = $codmov;
					$entrada->fecrcp = Carbon::now();
					$entrada->codalm = $request->codalm;
					// $entrada->tipmov = $request->tipmov;
					$entrada->desrcp = $request->observacion;
				//	$entrada->fecven = isset($request->fecven) ? $request->fecven : null;
					$entrada->created_by_user =  Auth::user()->loguse;
					$entrada->nommov =  $request->movinvent;
					$entrada->status = 'E';
					$entrada->codigoid = $codigoID;
					$entrada->codsuc = session('codsuc');
					$entrada->nrocontro= $request->nrocontro;
					$entrada->codpro= $request->codpro;
					$entrada->fecfac= Carbon::parse($request->fecfac);
					$entrada->monrcp= doubleval($monto) ?? 0;//str_replace($request->monrcp,'');
					$entrada->moneda= $request->typeCoin;
						$entrada->save();



					Caartalmubimov::insert($tosave);
					//session('serial','codart')

					if($request->movinvent === 'SAL'){//si el movimiento es salida
						if($serial !== null){
							foreach($serial as $key=>$valor){


								$toSerial[] = $valor;

								$updateSerial[] = [

									'entrada' => $request->entrada,
									'serial'  => $valor,
								    'codart'  => $codart[$key],
									'salida'  => true,
									'codalm'  => (string)$codalm2,
									'deleted_at' => Carbon::now()

								];
							}

							$update = serialDisponible::whereIn('seriales',$toSerial)->where('disponible',true)->get();
						    if($update->isEmpty())	throw new Exception("No existe el serial para darle salida");

							// serialDisponible::whereIn('seriales',$toSerial)
							// 				  ->where('disponible',true)
							// 				  ->update([
							// 					'disponible' => false,
							// 					'deleted_at'=>Carbon::now()
							// 				   ]);

							SerialEntradas::insert($updateSerial);
						}

					}else{
					//aqui donde voy ingresar serial con su articulo en entrada

						if($serial !== null){
							foreach($serial as $key=>$valor){
								$updateSerial[]	= [
									'serial' => $valor,
									'codart' => $codart[$key],
									'entrada'=> $request->entrada,
									'codalm' => (string)$codalm2,
									'salida' => false
								];
							}
							SerialEntradas::insert($updateSerial);
						}
					}
					DB::commit();
					session()->forget('seriales');
					session()->forget('serial');
					session()->forget('codart');
	   // Session::flash('success','Entrada Registrada Exitosamente');
					// return redirect()->route('EntradaYsalida','Entrada')->with($this->getcombo());
			}catch(Exception $e){

				DB::rollBack();//regresion por si algo falla en el sistema
				session()->forget('seriales');
				session()->forget('serial');
				session()->forget('codart');

				Session::flash('error',$e->getMessage());
				return redirect()->route('CreateEntrada');
      }
      try{
        $this->sendMail($request->entrada,null);
      }catch(\Exception $e){

      }
	  $ent = $request->movinvent === 'SAL' ? 'Salida' : 'Entrada';

	  Session::flash('success', $ent.' Registrada Exitosamente');
	  return redirect()->route('EntradaYsalida')->with($this->getcombo());
		}

		public function sendMail($codmov,$json = true){
      /*
        Buscando el destinatario
      */
      try{
        $arts =  Caartalmubimov::getArtToEmail($codmov);
				$mov = Caentalm::findByCod($codmov);
        $address = ["mpaez@vit.gob.ve","miguelodav93@gmail.com"];
        Mail::to($address)->send(new SolicitudTraspasoMail($mov,$arts));
      }catch(Exception $e){
        return response()->json(["error" => "error {$e->getMessage()}"]);
			}
      return $json ? response()->json(["exito" => "miguelodav93@gmail.com"]) : true;

    }

		public function aprobMov(Request $request,$codmov){
			/* $codmov = helper::desencriptar($codmov); */

			try{
				//if(!$request->ajax()) throw new Exception("No es una peticion ajax");

				//$codmov =  Helper::decrypt($codmov);
				$nommov = Caentalm::findByCod($codmov);//retorna OBJETO ELOQUENT

						if($nommov->status === 'APR') $string = 'Aprobado';
						else if($nommov->status === 'ANL') $string = 'Anulado';
						if(isset($string)) throw new Exception ("El Movimiento ya se encuentra ".$string);


						$articulos = Caartalmubimov::findByMovType($codmov,$nommov->nommov);//retorna collection

					
						switch ($nommov->nommov) {
							case 'ENT':
							return $this->doEntrada($articulos);
							case 'SAL':
							return $this->doSalida($articulos);
						}
			}catch(Exception $e){
				return response()->json(['error' => $e->getMessage()]);
			}

		}

		public function anularMov(Request $request,$codmov,$desanu){
			try{

				//if(!$request->ajax()) throw new Exception("No es una peticion ajax");
				//$codmov = Helper::desencriptar($codmov);

				$nommov = Caentalm::findByCod($codmov);//retorna OBJETO ELOQUENT
				if($nommov->status === 'APR') $string = 'Aprobado';
				else if($nommov->status === 'ANL') $string = 'Anulado';
				if(isset($string)) throw new Exception ("El Movimiento ya se encuentra ".$string);
				$mov = Caentalm::findByCod($codmov);
				$mov->status = 'ANL';
				$mov->fecanu = Carbon::now();
				$mov->desanu = $desanu;
				$mov->usuanu = Auth::user()->loguse;


				$mov1 = \App\Caartalmubimov::where('codmov',$mov->codmov)->get();

				if($mov1 !== null){
					if(session('seriales') !== null){
					foreach ($mov1 as $key) {


							foreach (session('seriales') as $key2) {

								$seriales1 = \App\Models\SerialEntradas::
								where([['serial',$key2],['entrada',$mov->codmov]])
								 ->whereDoesntHave('serialesFacturados')
								 ->first();

								 if($seriales1 !== null) {
									$seriales1->deleted_at = Carbon::now();
									$seriales1->salida = true;
									$seriales1->save();
								 }



							}
						}

					}
				}
				$mov->save();

				session(['seriales' => []]);

				return response()->json(['exito' => true]);
			}catch(Exception $e){
				dd($e);
				return response()->json(['error' => $e->getMessage()]);
			}
		}

		public function doEntrada($objeto){


			try{
				$codsuc = session('codsuc');
				$codigoid= Caartalmubi::getCount().'-'.explode('-',$codsuc)[1];

					$serialEntradas = SerialEntradas::where('entrada',$objeto[0]->codmov)->get();
					if(!$serialEntradas->isEmpty()){
						
						foreach ($serialEntradas as $key) {
							$serialSave[] = [
								'seriales' => $key['serial'],
								'codart' => $key['codart'],
								'disponible' => true,
								'created_at' => Carbon::now(),
								'codalm' => $key['codalm']
							];
						}
						serialDisponible::insert($serialSave);
					}




					foreach($objeto as $articulo){

					$cant = Caartalmubi::findQty($articulo->codart,$articulo->codalmto,$articulo->codubito,$articulo->codtalla);
					$suma = !isset($cant) ? floatval($articulo->cantmov) :  floatval($cant->exiact)+floatval($articulo->cantmov);

					$movart = Caartalmubi::updateOrCreate(
						[
						'codalm' => $articulo->codalmto,
						'codart' => $articulo->codart,
						'codubi' => $articulo->codubito,
						'codtalla' => $articulo->codtalla
					],
					[
						'exiact' => $suma,
						'created_at' => Carbon::now(),
						'codsuc' => $codsuc,
						'codigoid' => $codigoid
					]
					);
				}
				$movimiento = Caentalm::findByCod($objeto[0]->codmov);//retorna objeto
				$movimiento->aproved_by_user = Auth::user()->loguse;
				$movimiento->fecapro = Carbon::now();
				$movimiento->status = 'APR';
				$movimiento->save();



				return response()->json(['exito' => true]);
			}catch(Exception $e){
				return response()->json(['error' => $e->getMessage()]);
			}
		}

		public function doSalida($objeto){
			try{
				DB::beginTransaction();
				foreach ($objeto as $articulo) {
					$cant = Caartalmubi::findQty($articulo->codart,$articulo->codalmfrom,$articulo->codubifrom,$articulo->codtalla);
					if(!$cant)throw new Exception ("No existen articulos en el almacen");
					if($cant->exiact < $articulo->cantmov){
						throw new Exception ("
						<p>La cantidad a mover del articulo {$articulo->articulos->desart} es mayor a la disponible<p>
						<p><b>Existencia : {$cant->exiact}</b> Solicitado : {$articulo->cantmov} </p>
						");
						$error = true;
					}
					}

				foreach ($objeto as $articulo) {

					$cant = Caartalmubi::findQty($articulo->codart,$articulo->codalmfrom,$articulo->codubifrom,$articulo->codtalla);
					$suma = floatval($cant->exiact)-floatval($articulo->cantmov);
					$seriales = \App\Models\SerialEntradas::withTrashed()->where('entrada',$articulo->codmov)->get();

					if(!$seriales->isEmpty()){

						foreach ($seriales as $key) {
							$toSerial[] = $key->serial;
							$toCodart[] = $key->codart;
						}

						$verif = \App\Models\Serial::whereIn('serial',$toSerial)
													->whereIn('codart',$toCodart)
													->whereNull('deleted_at')->get(); //verifica si esta facturado

						if($verif->isNotEmpty()) //condiciona si es verdadero retrocede
						   throw new Exception("Este serial esta facturado por lo tanto no puedes dar salida");

						 SerialDisponible::whereIn('seriales',$toSerial)
										  ->whereIn('codart',$toCodart)
										  ->where('disponible',true)
										  ->update([
										 	'disponible' => false,
										 	'deleted_at' => Carbon::now()
										   ]);
					}


					$cant->exiact = $suma;
					$cant->save();
				}


				$movimiento = Caentalm::findByCod($objeto[0]->codmov);//retorna objeto
				$movimiento->aproved_by_user = Auth::user()->loguse;
				$movimiento->fecapro = Carbon::now();
				$movimiento->status = 'APR';
				$movimiento->save();
				DB::commit();
				return response()->json(['exito' => true]);

			}catch(Exception $e){
				DB::rollback();
				return response()->json(['error' => $e->getMessage()]);
			}
		}

		public function getDetalle(Request $request,$codmov){
			$detalle = Caentalm::findByCodDetalle($codmov);
			$type = $detalle->nommov;
      		$articulos = Caartalmubimov::findByMovDetail($codmov,$detalle->nommov);
      	return !$articulos->isEmpty() ? response()->json(['articulos' => $this->makelist($articulos),'detalle' => $this->getHtmlDetail($detalle) , 'type'=>$type]) : response()->json(false);
    }

		public function getHtmlDetail($detalle){
      switch ($detalle->status) {
				case 'APR':
					$monto = isset($detalle->monrcp) ? 'Monto : '.$detalle->monrcp : '';
        	$moneda = isset($detalle->coin) ? $detalle->coin->nomenclatura : '';
					$proveedor = $detalle->proveedor->nompro ?? 'N/A';
					$rif = $detalle->proveedor->rifpro ?? 'N/A';
					$fecha = Carbon::parse($detalle->fecapro)->format('l j n F Y');
					$fecha = Helper::getSpanishDate($fecha);
					$hora = Carbon::parse($detalle->fecapro)->format('h:i:s A');
          $html = '<div class="alert alert-success ">
          <h5><i class="icon fas fa-check"></i>Aprobado</h5>
          <strong>'."{$detalle->movimiento} Aprobado".'</strong><br>
          <strong>'."Fecha Aprobacion: ".$fecha.' a las '.$hora.'</strong>
					<address>
					<strong>Observacion : </strong>'.$detalle->desrcp.'<br>
					<strong>Numero Factura : </strong>'.$detalle->nrocontro.'<br>
					<strong>Nombre Proveedor : </strong>'.$proveedor.'<br>
					<strong>Rif Proveedor : </strong>'.$rif.'
					<strong>'.$monto.' '.$moneda.'</strong>
          </address>
          </div>';
          break;
        case 'E':
        	$monto = isset($detalle->monrcp) ? 'Monto : '.$detalle->monrcp : '';
					$moneda = isset($detalle->coin) ? $detalle->coin->nomenclatura : '';
					$proveedor = $detalle->proveedor->nompro ?? 'N/A';
					$rif = $detalle->proveedor->rifpro ?? 'N/A';
          $html= '<div class="alert alert-info ">
          <h5><i class="icon fas fa-info"></i> En Espera</h5>
          <strong>'."{$detalle->movimiento} En Espera de Aprobacion".'</strong>
					</div>
					<div class="col-sm-12 invoice-col"><strong>
					<address>
					<strong>Observacion : </strong>'.$detalle->desrcp.'<br>
					<strong>Numero Factura : </strong>'.$detalle->nrocontro.'<br>
					<strong>Nombre Proveedor : </strong>'.$proveedor.'<br>
					<strong>Rif Proveedor : </strong>'.$rif.'
					</address>
					<strong>'.$monto.' '.$moneda.'</strong>
          </div>'
					;
          break;
				case 'ANL':
					$fecha = Carbon::parse($detalle->fecanu)->format('l j n F Y');
					$fecha = Helper::getSpanishDate($fecha);
					$hora = Carbon::parse($detalle->fecanu)->format('h:i:s A');
          $html = '<div class="alert alert-danger">
          <h5><i class="icon fas fa-ban"></i> Anulado</h5>
          <strong>'."{$detalle->movimiento} Rechazado".'</strong>
          <strong>'."Observacion : ".$detalle->desanu.'</strong><br>
          <strong>'."Fecha Anulacion: ".$fecha.' a las '.$hora.'</strong>
          </div>';
          break;
      }
      return $html;
		}



    public function makeList($articulos){
				//$articulos = $this->repetidosBest($articulos);
				$string = '';
						foreach($articulos as $key => $value){

							//$tallas = isset($value->talla) ? $value->talla->tallas : 'N/D'; //$value->codmov
							$caregart = SerialEntradas::withTrashed()->whereCodart($value->codart)->where('entrada',$value->codmov)->get();
							$serial = null;
							$caregart->map(function($val) use (&$serial)
							{
								$serial .= $val['serial'].PHP_EOL;
							});
							$temp = $serial ?? 'NO APLICA';

							$almacen = $value->almacento->nomalm ?? $value->almacenfrom->nomalm;
							$color = isset($value->color->color) ? $value->color->color.'-'.$value->color->nomenColor : 'NO APLICA';
							$costo = isset($value->articulo->costoPro->pvpart) ? $value->articulo->costoPro->pvpart : 'NO DEFINIDO';
							$moneda = isset($value->articulo->moneda->nomenclatura) ? $value->articulo->moneda->nomenclatura : '';
							$string .= "
													<tr>
													<td>{$value->codart}</td>
													<td>{$value->articulo->desart}</td>
													<td>{$almacen}</td>
													<td>{$color}</td>
													<td>{$value->cantmov}</td>
													<td>{$temp}</td>
													<td>{$costo}{$moneda}</td>
													</tr>
													";
												}
								return $string !== '' ? $string : null;
    }
		public function getalmubi(Request $request,$alm){
			if(!$request->ajax()) return "error";
			$alm = Helper::desencriptar($alm);
			return \App\Alma_ubi::getubi($alm);
		}

		public static function getListArt(Request $request,$codalm,$movinvent){
			if($movinvent === 'SAL' || $movinvent === 'TRA'){
			$arts = new \App\Caartalmubi;
			$art = new Caregart;
			$art =  $art->existAlm($codalm);
			return !$art->isEmpty() ? response()->json(['articulos' => $art]): response()->json(['error' => 'No existen articulos en este almacen']);
		}else if($movinvent === 'ENT'){
			return response()->json(['articulos' => Caregart::getValidArts()]);
		}else{
			return response()->json(['error' => 'Ingrese un tipo de movimiento']);
		}
		//return $arts->getUbiArtAlm($codart,$codalm)
		}

		public function createcodmov(){
			$date =  explode('-',Carbon::now()->toDateString());
			$date = $date[2].$date[1].$date[0];
			$num =  $this->GetFormatNum(Caentalm::GetLastCode());
			return $date.'-'.$num;
		}

		public function GetFormatNum($modelo){
        $numero= ($modelo ? str_pad($modelo+1, 4, '0', STR_PAD_LEFT):'0001');
        return $numero;
	}

		public function getmenu(){
			$menu = new HomeController();
			return $menu->retornarMenu();
		}

	public function ajax()
	{
		$data = request();
		
		$sucuse_reg=session('codsuc');
    $sep=explode('-', $sucuse_reg);
		$exist =false;
		switch ($data['ajax']) {
			case 1:// cambio de tipo de movimiento
				$control=caentalm::Orderby('codigoid','Desc')->whereNull('deleted_at')->first();
			if($control==Null)
				{$codigo=$data['valor'].'-1-'.$sep[1];}
			else
				{
				$ent = Caentalm::get()->count();
				$codigo=$data['valor'].'-'.($ent+1).'-'.$sep[1];
				}
				return response()->json(['options'=>($codigo?$codigo:'0')]);
				break;

			case 2:
				if(!$data["valor"]) return '';
				$articulo=caregart::where('codart',$data['valor'])->with('facategoria')->first();

				$reqTalla = self::getReqTalla($articulo);

				$Cartalmubi =  new Caartalmubi;
				$arts = $Cartalmubi->getUbiArtAlm($articulo->codart,$data["codalm"]);//TRAE VARIAS UBICACIONES
				$ubicaciones = !$arts->isEmpty() ? $this->makeHtml($arts,true,$data["tipmov"]) : $this->makeHtml(Almacen::getubix($data["codalm"]),false,$data["tipmov"]);//si existe se manda la collection conseguida, Si no se manda la collection Almacen::getubi

				if($data["tipmov"] === 'ENT'){//si es entrada puede entrar cualquier talla
					$arrayTallas= explode(' ', $articulo->codtallas);
					$tallas= Tallas::whereIn('codtallas',$arrayTallas)->get();

					$options = $this->makeHtmlTallas($tallas);//en Entrada no importa la existencia
				}elseif($data["tipmov"] === 'SAL'){//si es salida solamente mostrar las tallas en existencia
					$exist = true;
					$almacen = $data['codalm'];
					$tallas = [];
					if($reqTalla){//hacer esto si la talla es requerida
						foreach($arts as $art){
							$existExact[]=[
								'exiact' => $art->exiact,
								'codubi' => $art->codubi,
								'codart' => $art->codart
							];
							$talla = Tallas::where('codtallas','=',$art->codtalla)->first();
							$talla->exiact = $art->exiact;
							array_push($tallas, $talla);
						}
						$options = $this->makeHtmlTallas($tallas,true);
					}else{
						$options = self::makeHtmlNormalExist($arts);
					}
				}
				if(count($tallas) === 0 && $reqTalla) $msgError = "Este articulo : {$articulo->desart} no tiene tallas definidas verifique Categoría";
				return response()->json([
					'status' => isset($msgError) ? 'error' : 'success',
					'options'=> $options,
					'ubicaciones' => $ubicaciones,
					'msg' => $msgError ?? '',
					'requiredFields' => $reqTalla ? ['tallas'] : [],
					'existExact' => $existExact ?? ''
					]);

			break;

			case 3://CUANDO SE CAMBIA EL ALMACEN
				// codalm
				//movinvent
				if($data["movinvent"] === 'ENT') {
          			$ubicaciones = Almacen::getubix($data["codalm"]);
					//definir requerimientos de articulos
					$articulos = \App\Caregart::where('tipreg','F')->with('facategoria')->whereHas('facategoria')->where('deleted_at',NULL)->get();
					return response()->json(
						['options' => !$ubicaciones->isEmpty() && !$articulos->isEmpty() ? $this->makeHtmlArts($articulos) : null,
						'errorUbi' => $ubicaciones->isEmpty() ? 'No tiene ubicaciones definidas' : null,
						'errorArt'=> $articulos->isEmpty() ? 'No Existen articulos registrados con Categorias' : null
						]
						);
				}else if($data["movinvent"] === 'SAL'){
					$articulos = new Caregart;
					$articulos = $articulos->existAlm($data["codalm"]);
					return response()->json(['options' => !$articulos->isEmpty() ? $this->makeHtmlArts($articulos) : null]);
				}

			break;

			case 4://insert de proveedor
				try{
					
					if((strlen($data["rifpro"])!== 10)&&($data["rifpro"]=== '')) throw new Exception("Verifique el rif ingresado");
					if($data["nompro"] === '')throw new Exception("Debe ingresar el nombre del proveedor");
					if(!strpos($data["rifpro"],'-'))throw new Exception("Verifique el rif ingresado");

					$codpro = $data["tipodoc"].str_replace('-','',$data["rifpro"]);
					$rifpro = $data["tipodoc"].$data["rifpro"];
					$validate = Caprovee::where('codpro',$codpro)->orWhere('rifpro',$rifpro)->orWhere('nompro',$data["nompro"])->get();
					if(!$validate->isEmpty()) throw new Exception("Existe un proveedor con datos parecidos");

					$codigoid = Caprovee::getCodigoid().'-'.$sep[1];
					$cap = Caprovee::create([
						'rifpro' => $data["tipodoc"].$data["rifpro"],
						'codpro' => $data["tipodoc"].str_replace('-','',$data["rifpro"]),
						'nompro' => $data["nompro"],
						'codigoid' => $codigoid,
						'codsuc' => $sucuse_reg
					]);
				}catch(Exception $e){
					return response()->json(['error' => $e->getMessage()]);
				}
				return response()->json(['success' => 'Proveedor creado exitosamente','pro' => $cap]);

				case 5://cuando se cambia el tipo de movimiento en reportes
					if(!$data["tipmov"]) return;
					$movs = Caentalm::tipmov($data["tipmov"])->select(['codmov'])->orderBy('codmov','asc')->get();
					$to = $movs->sortByDesc('codmov');
					$msj = !$movs->isEmpty() ? true : null;
					return response()->json([
						'success'=> $msj,
						'movs' => $movs,
						'to' => $to
					]);
		}

	}

	public static function makeHtmlNormalExist($collection){
		//ASUMIENDO QUE SOLAMENTE VA A EXISTIR UN ARTICULO POR UBICACION
		return $collection->isEmpty() ? "<option value=N/D>Sin Talla Existencia:</option>" : "<option value=N/D>Sin Talla Existencia: {$collection[0]->exiact}</option>";
		//return $articulo->
		//ESTO SI Y SOLO SI SE CONTEMPLA UN SOLO ARTICULO POR ALMACEN
	}
	// Retorna el valor en caso de que requiera la talla
	public static function getReqTalla($articulo){
		return $articulo->facategoria->talla === true ? true : null;
	}

	/* CREA EL HTML DE LAS OPCIONES DE LOS ARTICULOS */
	public function makeHtmlArts($collection){

		$string = '<option value=""> Seleccione Artículo </option>';
		foreach ($collection as $value) {
			$isSerial1 = $value->isserial === null || $value->isserial === false ? "false" : "true";
			$string.="<option data-foo=$isSerial1 value={$value->codart}>{$value->codart} {$value->desart}</option>";
		}
		return $string;
	}
	/* CREA EL HTLM DE LAS UBICACIONES DE LOS ARTICULOS */
	public function makeHtml($collection,$existe = false,$tipmov){
		$string= '';
		$string.='<option value="" disabled> Seleccione Ubicacion </option>';
		if((!$existe) && $tipmov === 'ENT'){//no existe en el almacen
			foreach ($collection as $value) {
				foreach ($value->ubinamesx as $ubicacion){
					$string.="<option value={$ubicacion->codubi}>{$ubicacion->nomubi}</option>";
				}
			}
		}elseif($existe && ($tipmov === 'SAL' || $tipmov === 'ENT')){//existe en el almacen
			
			foreach ($collection as $value) {
					$string.="<option value={$value->ubicaciones->codubi} data-exiact={$value->exiact} data-talla={$value->codtalla}>{$value->ubicaciones->nomubi}</option>";
			}
		}else{
			return '0';
		}
		return $string;// === '' ? '<option>Seleccione Ubicacion</option>';
	}

	/* CREA EL HTML DE LAS TALLAS DE LOS ARTICULOS */
	public function makeHtmlTallas($collection,$exist = null){
		$string= '';
		$string.='<option disabled> Seleccione </option>';
		$cantidad ='';
		foreach ($collection as $value) {
			$cantidad = $exist ? "Existencia : ".$value->exiact : '';
			$string.="<option value={$value->codtallas} data-exiact={$value->exiact}>{$value->tallas}   {$cantidad}</option>";
		}
		return count($collection) === 0 ? "<option value=N/D selected >Sin talla</option>": $string;
	}

	public function makeHtmlNoTallas($collection){
		return count($collection) === 0 ? "<option value=N/D selected >Sin talla Existencia : 0</option>" : "<option value=N/D selected >Sin talla Existencia : {$collection->exiact}</option>";
	}

	  public function vistaReporteEntrada(){

        $menu = new HomeController();
        $sucursales = Sucursal::get();
  //       if(Auth::user()->isAdmin())
		//    $almacenes = Almacen::get();
		// else
			$almacenes = Almacen::get();
        $entradas = Caentalm::where('status','<>','R')->orderBy('codmov','asc')->get();

        return view('/reportes/entradaAlmacenReporte',[
					'menus' => $this->getmenu(),
					'sucursales' => $sucursales,
					'almacenes' => $almacenes,
					'entradas' => $entradas
				]);
    }

    public function reporteEntrada(Request $request){
    		$controlentrada= caentalm::where('codmov',$request->codto)->first();

			$desde = $request['desde'] ?? $controlentrada->created_at;
			$hasta = $request['hasta'] ?? $controlentrada->created_at;
			$diames = $request['diames'];
			$codfrom = $request->codfrom ?? $controlentrada->codmov;
			$codto = $request->codto ?? $controlentrada->codmov;
			$tipmov = $request->tipmov ?? '';
			
         switch ($diames) {
            case 'diario':
               $tipo = 'DIARIO';
            break;
            case 'ayer':
                $tipo = 'DÍA ANTERIOR';
            break;
            case 'semanal':
               $tipo = "SEMANAL";
            break;
            case 'semant':
               $tipo = "SEMANA ANTERIOR";
            break;
            case 'mensual':
                $tipo = 'MENSUAL';
            break;
            case 'mesant':
              $tipo = 'MES ANTERIOR';
            break;
            case 'anual':
              $tipo = 'ANUAL';
            break;
            case 'antyear':
              $tipo = 'AÑO ANTERIOR';
						break;
						case 'byCod':
							$tipo = 'Individual '.$request->codfrom;
						break;
            default:
              $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
            break;

				}
        $sucuse_reg=Auth::user()->getCodigoActiveS();

      ///EVALUA LOS PERIODOS DE TIEMPO SELECCIONADOS Y SI SERA UNA CONSULTA GENERALIZADA O POR SUCURSAL

				$entradas = Caentalm::with(
					'almacenes',
					'proveedor',
					'coin',
					'movarticulos',
					'movarticulos.talla',
					'movarticulos.articulo.costoPro',
					'movarticulos.articulo.moneda',
					'movarticulos.color'
				)
				->date($desde,$hasta)
				// ->whereDate('fecapro','<=',$desde)
				// ->orWhereDate('fecapro','>=',$hasta)
				->almacen($request->almacen ?? $controlentrada->codalm)
				->codigo($codfrom,$codto)
				->tipmov($request->tipmov)
				->where('codsuc',$sucuse_reg)
				->get();

				if($entradas[0]->status == 'E'){
					$estatusEntrada = 'ESPERA';
				} elseif ($entradas[0]->status == 'APR'){
					$estatusEntrada = 'APROBADO';
				} else {
					$estatusEntrada = 'ANULADO';
				}

					$titulo = $request->tipmov === 'ENT' ?  'ENTRADAS' : 'SALIDAS';
					if($entradas->isEmpty()){
						Session::flash('error', 'No existen registros con los parametros de busqueda ingresados');
						return redirect()->route('reporteEAlmacen');}

            $pdf = new generadorPDF();

            $titulo = "REPORTE {$titulo} ALMACEN";
            $total = $entradas->count();
            $fecha = date('d-m-Y');

            $pdf->AddPage('L');
            $pdf->SetTitle($titulo);
            $pdf->SetFont('arial','B',16);
            $pdf->SetWidths(array(90,90));
            $pdf->Ln();
            $pdf->Cell(0,22,utf8_decode($titulo),0,0,'C');
            $pdf->Ln();
            $pdf->SetFont('arial','B',10);
            if($tipo == 'Intervalo'){
                $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo.' '.$desde.' - '.$hasta),0,0,'L');
            }
            else{
                $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo),0,0,'L');
            }
            $pdf->Ln(5);
            $pdf->Cell(0,0,utf8_decode('FECHA EMISION: '.$fecha),0,0,'L');
            $pdf->Ln(5);
            $pdf->Cell(0,0,utf8_decode('TOTAL: '.$total),0,0,'L');
            $pdf->Ln(10);
            $pdf->SetFont('arial','B',8,5);
            $pdf->SetFillColor(2,157,116);
            $pdf->Cell(40,8,utf8_decode('COD.MOVIMIENTO'),1,0,'C');
            $pdf->Cell(20,8,utf8_decode('TIPO'),1,0,'C');
            $pdf->Cell(20,8,utf8_decode('ESTATUS'),1,0,'C');
            $pdf->Cell(40,8,utf8_decode('DESCRIPCION'),1,0,'C');
            $pdf->Cell(40,8,utf8_decode('ALMACEN'),1,0,'C');
            $pdf->Cell(30,8,utf8_decode('MONTO FACTURA'),1,0,'C');
            $pdf->Cell(40,8,utf8_decode('FECHA RECEPCION'),1,0,'C');
            $pdf->Cell(50,8,utf8_decode('APROBADO POR'),1,0,'C');
            $pdf->Ln();
            $pdf->SetWidths(array(40,20,20,40,40,20,40,50));
            $pdf->SetAligns(['C','C','C','C','C','C','C','C']);
            $pdf->SetFont('arial','',8,5);

            $last=1;
            $limite=count($entradas);



			$index = 0;


            foreach($entradas as $entrada){
					$pdf->SetWidths(array(40,20,20,40,40,30,40,50));
            		$pdf->SetAligns(['C','C','C','C','C','C','C','C']);
					$pdf->SetFont('arial','',8,5);
					$moneda = isset($entrada->coin->nomenclatura) ? $entrada->coin->nomenclatura  : '';
					$pdf->Row(
							array(
								$entrada->codmov,
								$entrada->nommov == 'ENT' ? 'ENTRADA' : 'SALIDA',
								$estatusEntrada,
								$entrada->desrcp,
								$entrada->almacenes->nomalm,
								number_format($entrada->monrcp,2,',','.').' '.$moneda,
								Carbon::parse($entrada->fecrcp)->format('d-m-Y'),
								$entrada->aproved_by_user ?? '-'
							));
								$pdf->SetFont('arial','B',9);
                // $pdf->SetFillColor(2,157,116);
                $pdf->Cell(280,6,'ARTICULOS  '.$entrada->codmov,1,1,'C');
                $limit = (int)392 / 7;
                $pdf->Cell($limit,6,utf8_decode('COD.ARTICULO'),1,0,'C');
                $pdf->Cell($limit,6,utf8_decode('DESCRIPCION'),1,0,'C');
                //$pdf->Cell($limit,6,utf8_decode('COLOR'),1,0,'C');
                $pdf->Cell($limit,6,utf8_decode('CANTIDAD'),1,0,'C');
				$pdf->Cell($limit,6,utf8_decode('PRECIO / UND'),1,0,'C');
				$pdf->Cell($limit,6,utf8_decode('PRECIO ACTUAL'),1,1,'C');
                $pdf->SetWidths(array($limit,$limit,$limit,$limit,$limit,$limit));
				$pdf->SetAligns(['C','C','C','C','C','C','C']);
				$pdf->SetFont('arial','',10);




                foreach($entrada->movarticulos as $key => $articulo){
							//dd($articulo->articulo->moneda->nomenclatura);
									/*
										'movarticulos',
										'movarticulos.talla',
										'movarticulos.articulo.costoPro',
										'movarticulos.articulo.moneda',
										'movarticulos.color',
									*/
									$costo = isset($articulo->articulo->costoPro) ? $articulo->articulo->costoPro->pvpart : 'N/A';
									$moneda = isset($articulo->articulo->moneda) ? $articulo->articulo->moneda->nomenclatura : '';
									//$talla = isset
									//dd($articulo->articulo->desart);
									if($request->tipmov === 'ENT') $sr2 = SerialEntradas::withTrashed()->where('codart',$articulo->codart)->where('entrada',$codfrom)->get();
									else $sr2 = SerialEntradas::withTrashed()->where('codart',$articulo->codart)->where('entrada',$codfrom)->get();

									$sr = '';
									$pasos = 0;
									$getY = 0;
									if($sr2 !== null){


										foreach($sr2 as $key2 => $value2){

											$sr .= $value2->serial.'  ';

											$pasos++;
										}

									}

									$pdf->Row(
										array(
											utf8_decode($articulo->codart),
											utf8_decode($articulo->articulo ? $articulo->articulo->desart: ''),
											//$articulo->color->color ?? 'N/A',
											//$articulo->talla->tallas ?? 'N/A',

											$articulo->cantmov,
											$articulo->preunit,
											$costo.' '.$moneda
                    					));
									$pdf->SetFillColor(255,255,255);
									$pdf->MultiCell(280,6,$sr,1,1,'L');
                }

									$pdf->Ln(3);

								if($last !== $limite){
									if($pdf->GetY()>= 150 && $pdf->GetY()<= 180){
										$pdf->AddPage('L');
									}
									$pdf->SetFont('arial','B',8,5);
									$pdf->SetFillColor(2,157,116);
									$pdf->Cell(40,8,utf8_decode('COD.MOVIMIENTO'),1,0,'C');
            			$pdf->Cell(20,8,utf8_decode('TIPO'),1,0,'C');
            			$pdf->Cell(20,8,utf8_decode('ESTATUS'),1,0,'C');
            			$pdf->Cell(40,8,utf8_decode('DESCRIPCION'),1,0,'C');
            			$pdf->Cell(40,8,utf8_decode('ALMACEN'),1,0,'C');
            			$pdf->Cell(30,8,utf8_decode('MONTO FACTURA'),1,0,'C');
            			$pdf->Cell(40,8,utf8_decode('FECHA RECEPCION'),1,0,'C');
            			$pdf->Cell(50,8,utf8_decode('APROBADO POR'),1,0,'C');
									$pdf->Ln();
								}
								$last++;
								$index++;

            }
            $pdf->SetTextColor(0,0,0);
            $pdf->Ln(5);
            $pdf->Output('I','ENTRADAS ALMACEN-'.$fecha.'pdf');

           exit;
    }

    // public function vistaReporteSalida(){
    //     $menu = new HomeController();
    //     $sucursales = Sucursal::get();
    //     $almacenes = Almacen::get();
    //     return view('/reportes/salidaAlmacenReporte')->with('menus',$menu->retornarMenu())->with('sucursales',$sucursales)->with('almacenes',$almacenes);
    // }

    // public function reporteSalida(Request $request){

    //      $desde = $request['desde'];
    //      $hasta = $request['hasta'];
    //      $diames = $request['diames'];
    //      switch ($diames) {
    //         case 'diario':
    //            $tipo = 'DIARIO';
    //         break;
    //         case 'ayer':
    //             $tipo = 'DÍA ANTERIOR';
    //         break;
    //         case 'semanal':
    //            $tipo = "SEMANAL";
    //         break;
    //         case 'semant':
    //            $tipo = "SEMANA ANTERIOR";
    //         break;
    //         case 'mensual':
    //             $tipo = 'MENSUAL';
    //         break;
    //         case 'mesant':
    //           $tipo = 'MES ANTERIOR';
    //         break;
    //         case 'anual':
    //           $tipo = 'ANUAL';
    //         break;
    //         case 'antyear':
    //           $tipo = 'AÑO ANTERIOR';
    //         break;
    //         default:
    //           $tipo = 'INTERVALO ENTRE '.$request->desde.' HASTA '.$request->hasta;
    //         break;

    //     }

    // try{
    //         if(!$request->almacen)throw new Exception("Debe seleccionar un almacen");
    //         if($request->almacen =="TODAS"){
    //             $general = true;
    //         }
    //         else{
    //             $general = false;
    //             $codalm = $request->almacen;
    //         }

    //     }
    //     catch(Exception $e){
    //         return redirect()->route('reporteSAlmacen')->with('error',$e->getMessage());
    //     }

    //   ///EVALUA LOS PERIODOS DE TIEMPO SELECCIONADOS Y SI SERA UNA CONSULTA GENERALIZADA O POR SUCURSAL
    //   try{

    //            if($general){
    //            $salidas = Caentalm::with('almacenes','movimientos','movarticulos','articulos','tallas')->where('nommov','SAL')->whereBetween('created_at',[$desde,$hasta])->get();

    //            }
    //            else{
    //            $salidas = Caentalm::with('almacenes','movimientos','movarticulos','articulos','tallas')->where('nommov','SAL')->whereBetween('created_at',[$desde,$hasta])->where('codalm','=',$codalm)->get();

    //            }

    //         if($salidas->isEmpty()){
    //             Session::flash('error', 'No existen registros con los parametros de busqueda ingresados');
    //             return redirect()->route('reporteSAlmacen');}
    //     }//try
    //     catch(Exception $e){
    //         Session::flash('error', 'Se presentó un problema en la generación del reporte, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
    //         return redirect()->route('reporteSAlmacen');
    //     }
    //         $pdf = new generadorPDF();

    //         $titulo = 'REPORTE SALIDAS ALMACEN';
    //         $total = $salidas->count();
    //         $fecha = date('d-m-Y');

    //         $pdf->AddPage('L');
    //         $pdf->SetTitle('REPORTE SALIDAS ALMACEN');
    //         $pdf->SetFont('arial','B',16);
    //         $pdf->SetWidths(array(90,90));
    //         $pdf->Ln();
    //         $pdf->Cell(0,22,utf8_decode($titulo),0,0,'C');
    //         $pdf->Ln();
    //         $pdf->SetFont('arial','B',10);
    //         if($tipo == 'Intervalo'){
    //             $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo.' '.$desde.' - '.$hasta),0,0,'L');
    //         }
    //         else{
    //             $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo),0,0,'L');
    //         }
    //         $pdf->Ln(5);
    //         $pdf->Cell(0,0,utf8_decode('FECHA E: '.$fecha),0,0,'L');
    //         $pdf->Ln(5);
    //         $pdf->Cell(0,0,utf8_decode('TOTAL: '.$total),0,0,'L');
    //         $pdf->Ln(10);
    //         $pdf->SetFont('arial','B',9);
    //         $pdf->SetFillColor(2,157,116);
    //         $pdf->Cell(40,8,utf8_decode('COD.MOVIMIENTO'),1,0,'C');
    //         $pdf->Cell(20,8,utf8_decode('TIPO'),1,0,'C');
    //         $pdf->Cell(20,8,utf8_decode('ESTATUS'),1,0,'C');
    //         $pdf->Cell(40,8,utf8_decode('DESCRIPCION'),1,0,'C');
    //         //$pdf->Cell(50,8,utf8_decode('ARTICULOS'),1,0,'C');
    //         $pdf->Cell(40,8,utf8_decode('ALMACEN'),1,0,'C');
    //         $pdf->Cell(23,8,utf8_decode('FECHA'),1,0,'C');
    //         $pdf->Cell(35,8,utf8_decode('FECHA APROBACION'),1,0,'C');
    //         $pdf->Cell(40,8,utf8_decode('APROBADO POR'),1,0,'C');
    //         $pdf->Ln();
    //         $last=1;
    //         $limit=count($salidas);
    //         foreach($salidas as $salida){
    //           //$pdf->setBreak();
    //           $pdf->SetWidths(array(40,20,20,40,40,23,35,40));
    //           $pdf->SetAligns(['C','C','C','C','C','C','C','C']);
    //           $pdf->SetFont('arial','',8,5);

    //           $pdf->Row(
    //             array(
    //               $salida->codmov,
    //               $salida->nommov,
    //               $salida->status,
    //               $salida->desrcp,
    //               $salida->almacenes->nomalm,
    //               $salida->created_at ? $salida->created_at->format('d/m/Y') : '.' ,
    //               $salida->fecapro ? $salida->fecapro->format('d/m/Y') : '.' ,
    //               $salida->aproved_by_user ?? '.'
    //             ));

    //             $pdf->SetFont('arial','B',9);
    //             $pdf->SetFillColor(2,157,116);

    //             $pdf->Cell($pdf->GetPageWidth()-39,4,'ARTICULOS -'.$salida->codmov,1,1,'C');
    //             $pdf->Cell(86,6,utf8_decode('COD.ARTICULO'),1,0,'C');
    //             $pdf->Cell(86,6,utf8_decode('DESCRIPCION'),1,0,'C');
    //             $pdf->Cell(86,6,utf8_decode('TALLA'),1,1,'C');


    //             //$pdf->SetWidths(array());
    //             $pdf->SetWidths(array(86,86,86));
    //             $pdf->SetAligns(['C','C','C']);
    //             $pdf->SetFont('arial','',9);
    //             foreach($salida->articulos as $articulo){
    //               $talla = '.';
    //               if($salida->tallas){
    //                 foreach($salida->tallas as $tallax){
    //                   if($tallax->codtallas === $articulo->codtallas) {
    //                     $talla = $tallax->tallas;
    //                   }
    //                 }
    //               }
    //               else{
    //               	  $talla = 'N/A';
    //               }

    //               $pdf->Row(
    //                 array(
    //                   $articulo->codart,
    //                   $articulo->desart,
    //                   $talla,
    //                 ));

    //               }
    //               $pdf->Ln();
    //               if($last !== $limit){
    //                 $pdf->SetFont('arial','B',9);
    //                 $pdf->Cell(40,8,utf8_decode('COD.MOVIMIENTO'),1,0,'C');
    //                 $pdf->Cell(20,8,utf8_decode('TIPO'),1,0,'C');
    //                 $pdf->Cell(20,8,utf8_decode('ESTATUS'),1,0,'C');
    //                 $pdf->Cell(40,8,utf8_decode('DESCRIPCION'),1,0,'C');
    //                 $pdf->Cell(40,8,utf8_decode('ALMACEN'),1,0,'C');
    //                 $pdf->Cell(23,8,utf8_decode('FECHA'),1,0,'C');
    //                 $pdf->Cell(35,8,utf8_decode('FECHA APROBACION'),1,0,'C');
    //                 $pdf->Cell(40,8,utf8_decode('APROBADO POR'),1,0,'C');
    //                 $pdf->Ln();
    //               }
    //               $last++;


    //         }

    //         $pdf->SetTextColor(0,0,0);
    //         $pdf->Ln(5);
    //         $pdf->Output('I','SALIDAS ALMACEN-'.$fecha.'pdf');

    //        exit;
		// }

		public function cargaSerial(Request $request)
		{
			try {
				$paso = null;
				if(optional($request->all())['seriales'] === null) throw new Exception("cargue los seriales Por favor");

				session()->forget('seriales');
				session()->forget('serial');
				session()->forget('codart');
				$key = $request->all()['key'];


				$seriales = collect($request->all()['seriales']);
				$serialNotNull = $seriales->some(function ($value,$index) use ($key)
				{
					return $value[$key[$index]] === null;
				});

				if($serialNotNull) throw new Exception('algunos campos de seriales estan vacio');

				$seriales = $seriales->map(function ($value,$index) use ($key){ return $value[$key[$index]];});
				$paso = true;
				session(['seriales' => $seriales,
				         'serial' => $seriales,
						 'codart' => $request->all()['codart'],
						 'codalm' => $request->all()['codalm']
						]);

				return [
					'exito' => 'Seriales Cargados',
					'paso' => $paso
				];

			} catch (\Exception $th) {
				session()->forget('seriales');
				session()->forget('serial');
				$paso = false;
				return [
					'error' => $th->getMessage(),
					'paso' => $paso
				];
			}
		}


		public function searchArt(Request $request)
		{



			$codart = $request->all()['codigosArt'];
			foreach($codart as $key => $value){
				$search = Caregart::where('codart',$value)->first();

				if($search->isserial === true){
					return [
						'msg' => "Este Articulo esta validado para tener seriales, pero usted va a dar entrada sin seriales!",
						'codart' => $value,
						'field' => (isset($request->all()['key'])) ? $request->all()['key'] : null
					];
				}else{
					return [
						'msg' => "Este Articulo no esta validado para tener seriales, pero usted va a dar entrada sin seriales!",
						'codart' => $value,
						'field' => (isset($request->all()['key'])) ? $request->all()['key'] : null
					];
				}
			}

			return ['msg' => 'Exito'];
		}

		public function queryCodart(Request $request)
		{

			$paso = collect();
			foreach($request->all()['inventario'] as $key => $value){

				$search = Caregart::where('codart',$value['codart'])->first();
				if($search->isserial === false || $search->isserial === null){
					$paso->push(false);
				}else{
					$paso->push(true);
				}
			}

			return $paso;

		}

		public function GenerarPdfSeriales()
		{

      		if(Auth::user()->role->nombre == 'ADMIN') $sucursales = Sucursal::get();
      		else $sucursales = Sucursal::where('codsuc',Auth::user()->userSegSuc->codsuc)->get();


			return view('reportes.inventarioSeriales')->with(['menus'=>$this->getmenu(),'sucursales' =>$sucursales]);
		}

		public function GenerarPdfSerialesReportes(Request $request)
		{
			#dd($request->all());



			$pdf = new generadorPDF();


		$titulo = 'INVENTARIO FISICO DE SERIALES';
		//$total = $facturas->count();
		$fecha = date('d-m-Y');


		$pdf->AddPage('L');
		$pdf->SetFont('arial','B',16);
		$pdf->Ln(4);
		$pdf->Cell(0,22,utf8_decode($titulo),0,0,'C');
		$pdf->Ln(6);
		$pdf->SetFont('arial','B',10);

		$pdf->Cell(0,22,utf8_decode("REPORTE GENERADO: ".Carbon::now()->format('d-m-Y')),0,0,'C');

		$pdf->Ln(10);

		$serialEntradas = Caartalmubi::with(['articulos','serialesDisponible' => function ($q){
			$q->where('disponible',true)
			  ->whereDoesntHave('SerialesFacturados')
			  ->orderBy('seriales','ASC');
		}])
		->where('codalm',$request->caja)
		->where('exiact','>=',0)
		->where('codsuc',session('codsuc'))
		->get();
		
		$pdf->SetFont('arial','B',10);
		$pdf->cell(0,22,utf8_decode('ESTATUS: '.strtoupper($request->status)),0,0,'C');
		$pdf->Ln(20);
		$pdf->Cell(40,7, utf8_decode("Código ") ,1,0,'L');
		$pdf->Cell(35,7, utf8_decode("Unidades ") ,1,0,'L');
		$salidas = ($request->status === 'salidas') ? 'Salidas' : 'Existencia';
		$pdf->Cell(35,7, utf8_decode($salidas) ,1,0,'L');
		$pdf->Cell(130,7, utf8_decode("Descripción ") ,1,0,'L');

		$pdf->SetAligns(array('C','C','C','C'));
		$pdf->Ln(7);
		$pdf->SetWidths(array(40,130,35,35));
		$count = 0;
		$cantGeneral = 0;
		$string1 = '';
		foreach ($serialEntradas as $key) {
			//dd($key);
			//$cant = ($request->status === 'salidas') ? 0 : $serialEntradas->sum('exiact');
			//$key['articulos']['desart']
			$pdf->SetFont('arial','',10);
			
			$pdf->Cell(40,7,utf8_decode($key['codart']),1,0,'L');
			$pdf->Cell(35,7,utf8_decode('N/A'),1,0,'L');
			$pdf->Cell(35,7,utf8_decode($key['exiact']),1,0,'L');
			$pdf->Multicell(130,7,utf8_decode($key['articulos']['desart']),1,1);
			$pdf->ln();
			$pdf->SetFont('arial','B',10);
			$pdf->Cell(68,7, utf8_decode("Seriales ".$request->status) ,1,0,'L');
			$pdf->Ln();
			$sr = '';
			$i = 0;
			//dd($key['serialesDisponible']);
			foreach ($key->serialesDisponible as $key2) {
				$sr .= $key2->seriales.' - ';
				$i++;
			}
			$cantGeneral += $i;
			$pdf->SetFillColor(255,255,255);
			$pdf->SetFont('arial','',10);
			$pdf->MultiCell(240,6,$sr,1,1,'C');
			$pdf->Ln(0);
			$pdf->Cell(68,7, utf8_decode("Total de Seriales: ".number_format($i++,2,'.',',')) ,1,0,'L');
			$pdf->Ln(13);

		}

		$pdf->Cell(68,7, utf8_decode("Total General de Seriales: ".number_format($cantGeneral,2,'.',',')) ,1,0,'L');
		$pdf->Ln(7);
		
		$link = '/modulo/Facturacion/Seriales/saveWord/'.$request->caja.'/'.session('codsuc');
		
		//$pdf->Cell(68,8,utf8_decode('Descargar archivo word'),0,0,'L',false,$link);

		$pdf->Output('I','iNVENTARIO DE SERIALES-'.$fecha.'pdf');

        exit;
	}

	function saveWord($codalm,$suc)  {
		$serialEntradas = Caartalmubi::with(['articulos','serialesDisponible' => function ($q){
			$q->where('disponible',true)
			->whereDoesntHave('SerialesFacturados')
			->orderBy('seriales','ASC');
		}])
		->where('codalm',$codalm)
		->where('exiact','>=',0)
		->where('codsuc',$suc)
		->limit(5)
		->get();
		//dd($serialEntradas);

		try {
			$phpWord = new \PhpOffice\PhpWord\PhpWord();
			$section = $phpWord->addSection();
			$tabla = $section->addTable('styleTable',['borderSize' => 6,'borderColor' => '999999']);
			foreach ($serialEntradas as $key => $value) {
				$tabla->addRow();
				$tabla->addCell(1000,['gridSpan' => 3,'vMerge' => 'continue'])->addText(utf8_decode($value['articulos']['codart']),['name' => 'Tahoma','size' => 10]);
				
				$tabla->addCell(5000,['gridSpan' => 3,'vMerge' => 'continue'])->addText(utf8_decode($value['articulos']['desart']),['name' => 'Tahoma','size' => 10]);
				

			}
			

			$ObjWritter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord,'Word2007');
			$ObjWritter->save('/var/www/holaMundo.docx');
		} catch (\Exception $th) {
			dd($th->getMessage());
		}

		echo "<button id='desc' hidden>ddd</button>";
		echo "<script >
			window.onload = function(archivo,nombre){
				
			}

		</script>";
		die;
	}


	public function searchAlmacen(Request $request)
	{

		$almacen = Almacen::where('codsuc_asoc',$request->sucursal)->get();

		return response()->json($almacen);
	}

	public function getSearchSeriales(Request $request){

		

		$search = serialDisponible::where('codart',$request->codart)
									->where('codalm',$request->codalm)
                                    ->where('disponible',true)
									->whereDoesntHave('SerialesFacturados')
									->select('seriales')->get();
		$cptSerialesSalida = [];

		foreach ($search as $key => $value) {
			$cptSerialesSalida[] = $value->seriales;
		}

		return ['seriales' => $cptSerialesSalida];
		 
	}

	public function searchSeriales(Request $request) {


		if($request->tipmov === 'ENT') {

			$verif = \App\Models\Serial::where([['serial',$request->seriales]])->whereNull('deleted_at')->get();
			$search = serialDisponible::when($verif->isEmpty() || $verif->isNotEmpty(),function ($q) use ($request){//no esta facturado pero si esta disponible, no se registra
				$q->where([['seriales',$request->seriales]])
				->where('disponible',true);	
			})->first();

			if($search !== null || $verif->isNotEmpty()){//
	
				return [
					'error' => true,
					'msg'    => 'Este serial esta facturado o disponible en algun inventario '.$request->seriales.' no se puede cargar'
				];
			}else{//no existe en inventario y ni en factura, si se registra
				return [
					'error' => false,
					'msg' => 'Este serial no esta cargado y no esta facturado, puede proceder '.$request->seriales
				];
			}
		}
		else {
			$verif = \App\Models\Serial::where([['serial',$request->seriales]])->whereNull('deleted_at')->get();
			if($verif->isNotEmpty()){//
	
				return [
					'error' => true,
					'msg'    => 'Este serial '.$request->seriales.' está facturado. No se puede cargar'
				];
			}else{//no existe en inventario y ni en factura, si se registra
				return [
					'error' => false,
					'msg' => 'Este serial '.$request->seriales.' está disponible para su salida. Puede proceder '
				];
			}
		}
		
		
		

	}

}
