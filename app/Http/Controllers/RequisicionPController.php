<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PrecomController;
use App\Mail\RequisicionMail;
use App\Mail\PedidoMailable;
use App\Mail\ReqPresupuestoMail;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Session;
use Helper;
use App\User;
use App\Almacen;
use App\Caprovee;
use App\Famoneda;
use App\Farecarg;
use App\Cpdeftit;
use App\Caregart;
use App\Nphojint;
use App\Tsdefmon;
use App\Npasicaremp;
use App\Models\Costos;
use App\Models\Casolart;
use App\Models\Caartsol;
use App\Models\Npcatpre;
use App\Models\Cpprecom;
use App\Models\Cpimpprc;
use App\Models\Segcenusu;

class RequisicionPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filtro = trim($request->get('filtro'));

        $reqs = Casolart::where('id', 'LIKE', '%' . $filtro . '%')
            ->orwhere('reqart', 'iLIKE', '%' . $filtro . '%')
            ->orwhere('desreq', 'iLIKE', '%' . $filtro . '%')
            ->orwhere('fecreq', 'iLIKE', '%' . $filtro . '%')
            ->orwhere('stareq', 'iLIKE', '%' . $filtro . '%')
            ->orderBy('id', 'DESC')->paginate(10);

        $menus = $this->retornarMenu();

        return view('compras.requisicionP')->with(
            [
                'reqs'  => $reqs,
                'menus' => $menus,
                'filtro' => $filtro,
            ]
        );
    }

    public function retornarMenu()
    {
        $yaml = new Parser();
        $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
        return $menu['menu'];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menus = $this->retornarMenu();
        $costos = Costos::get();
        $user = Auth::user();
        return view('compras.createRequisicionP')->with(['menus' => $menus, 'costos' => $costos, 'user' => $user]);
    }

    private function getNumero()
    {

        $year = Carbon::now()->format('Y');
        $numero = $year . '-' . str_pad(Casolart::withTrashed()->count() + 1, 8, 0, STR_PAD_LEFT);
        return $numero;
    }

    public function getData() //Envío de datos a la ruta de creación de requisición
    {

        $querys = null;
        $querys = Caregart::where('tipreg', null)->whereNull('deleted_at')->select('codart', 'desart', 'unimed', 'exitot', 'codcat')->get();

        return response()->json([
            'articulos' => $querys,
        ], 200);
    }

    public function getPendientes()
    {
        $items = Casolart::where('stareq', 'P')->with('articulos')->get(); //Cambiar a P para el estado de Pendiente

        return response()->json($items, 200);
    }

    public function save(Request $request)
    {

        $req = Casolart::find($request->id);
        if ($req) {

            $request->validate([
                'selArts.*.canreq' => 'required',
            ]);

            try {

                $req->fecreq = $request['date'];
                $req->reqart = $request['numero'];
                $req->codcen = $request['codcen'];
                $req->desreq = $request['desreq'];
                $req->stareq = 'P';
                $req->aprreq = 'N';
                $req->empsol = Auth::user()->cedemp;
                $req->loguse = Auth::user()->loguse;
                $req->save();

                if ($request->selArts && count($request->selArts) > 0) {
                    foreach ($request->selArts as $key => $value) {
                        $articulos[] = [
                            'reqart' => $request->numero,
                            'codart' => $value["codart"],
                            'canreq' => $value["canreq"],
                            'costo'  => $value["cosult"],
                            'unimed' => $value["unimed"],
                            'desart' => $value["desart"],
                        ];
                    }
                }

                Caartsol::where('reqart', $request->numero)->delete();
                Caartsol::insert($articulos);

                return response()->json([
                    'success' => true,
                    'message' => 'Compra actualizada con éxito',
                    'redirect' => route('requisicionP'),
                ]);
            } catch (\Throwable $th) {
                DB::rollback();
                throw $th;
                return response()->json([
                    'error' => true,
                    'Error al guardar pedido'
                ], 200);
            }
        } else {

            try {



                DB::beginTransaction();

                $requisicion = Casolart::create([
                    'fecreq' => $request->date,
                    'reqart' => $request->numero,
                    'codcen' => $request->codcen ?? '',
                    'empsol' => Auth::user()->cedemp,
                    'desreq' => $request->desreq ?? '',
                    'obsreq' => $request->obsreq ?? '', //Campo pendiente
                    'stareq' => 'P', //Pendiente aprobación gerente de área
                    'aprreq' => 'N',
                    'loguse' => Auth::user()->loguse,

                ]);

                if ($request->selArts && count($request->selArts) > 0) {
                    foreach ($request->selArts as $key => $value) {
                        $articulos[] = [
                            'reqart' => $request->numero,
                            'codart' => $value["codart"] ?? '',
                            'canreq' => $value["canreq"] ?? 0.00,
                            'costo'  => $value["cosult"] ?? 0.00,
                            'unimed' => $value["unimed"] ?? '',
                            'desart' => $value["desart"] ?? '',
                        ];
                    }
                }

                Caartsol::insert($articulos);

                DB::commit();

                return response()->json([
                    'success' => true,
                    'message' => 'Pedido guardado con éxito',
                    'redirect' => route('requisicionP'),
                ]);
            } catch (\Throwable $th) {
                throw $th;
                DB::rollback();
                return response()->json([
                    'error' => true,
                    'Error al guardar pedido'
                ], 200);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $req = Casolart::find($request->id);
        $request->validate([
            'selArts.*.canreq' => 'required',
        ]);
        
        $numero = $this->getNumero();
        
        if (!$req) {
            DB::beginTransaction();
            try {
                $requisicion = Casolart::insert([
                    'fecreq' => $request->date,
                    'reqart' => $numero,
                    'codcen' => $request->codcen,
                    'empsol' => Auth::user()->cedemp,
                    'desreq' => $request->desreq,
                    'obsreq' => $request->obsreq ?? '', //Campo pendiente
                    'stareq' => 'U', //Pendiente aprobación gerente de área
                    'aprreq' => 'N',
                    'loguse' => Auth::user()->loguse,
                ]);

                if ($request->selArts && count($request->selArts) > 0) {
                    foreach ($request->selArts as $key => $value) {
                        $articulos[] = [
                            'reqart' => $numero,
                            'codart' => $value["codart"],
                            'codcat' => $value["codcat"] ?? 'N/P',
                            'canreq' => $value["canreq"],
                            'unimed' => $value["unimed"],
                            'desart' => $value["desart"],
                        ];
                    }
                }

                Caartsol::insert($articulos);
                DB::commit();
                
                return response()->json([
                    'success' => true,
                    'message' => 'Información suministrada correctamente',
                    'redirect' => route('requisicionP'),
                ]);
            } catch (\Illuminate\Database\QueryException $e) {
                DB::rollback();
                //throw $th;
                return response()->json([
                    'error' => true,
                    'msg' => 'Error al registrar pedido: '.$e."."
                ], 500);
            }
            /*de...
                $this->gerMail('mmavo@vit.gob.ve', $data); //Enviar correo gerente
            } catch (\Throwable $th) {
                //throw $th;
                return response()->json(['success' => true, 'message' => 'Registrado con éxito, no se pudo enviar el correo', 'th' => $th, 'redirect' => route('requisicionP')]);
            } */
        } else {
            try {
                DB::beginTransaction();

                $request->validate([
                    'selArts.*.canreq' => 'required',
                ]);

                $req->fecreq = $request['date'];
                $req->reqart = $request['numero'];
                $req->codcen = $request['codcen'];
                $req->desreq = $request['desreq'];
                $req->stareq = 'U';
                $req->aprreq = 'N';
                $req->empsol = Auth::user()->cedemp;
                $req->loguse = Auth::user()->loguse;
                $req->save();

                if ($request->selArts && count($request->selArts) > 0) {
                    foreach ($request->selArts as $key => $value) {
                        $articulos[] = [
                            'reqart' => $numero,
                            'codart' => $value["codart"],
                            'canreq' => $value["canreq"],
                            'codcat' => $value["codcat"],
                            'unimed' => $value["unimed"],
                            'desart' => $value["desart"],
                        ];
                    }
                }

                Caartsol::where('reqart', $numero)->delete();
                Caartsol::insert($articulos);
                DB::commit();


                return response()->json([
                    'success' => true,
                    'message' => 'Pedido registrado con éxito',
                    'redirect' => route('requisicionP'),
                ]);
            } catch (\Throwable $th) {
                DB::rollback();
                throw $th;
                return response()->json([
                    'error' => true,
                    'Error al registrar pedido'
                ], 200);
            }
        }
    }

    public function gerMail($email, $data)
    { //Función de envío de correo a gerente

        Mail::to('dreyes@vit.gob.ve')->send(new PedidoMailable($data));
        return response()->json('Correo enviado', 200);
    }

    public function reenviar($reqart) //Reenvío del correo al gerente
    {
        $req = Casolart::where('reqart', $reqart)->with('ccosto', 'articulos', 'solicitante')->first();

        $this->gerMail('dreyes@vit.gob.ve', $req);

        Session::flash('success', 'Correo de requisición ' . $req->reqart . ' reenviado con éxito.');
        return redirect()->route('requisicionP');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $req = Casolart::where('id', $id)->with('ccosto', 'articulos', 'solicitante', 'proveedor')->first();
        //dd($req);
        $menus = $this->retornarMenu();
        return view('compras.requisicionPshow')->with(
            [
                'req'  => $req,
                'menus' => $menus
            ]
        );
    }

    public function action(Request $request)
    {
        #dd($request);
        $date = Carbon::now()->format('Y-m-d');
        $req = Casolart::where('reqart', $request->reqart)->first();
        $user = Nphojint::where('codemp', $request->user)->first();
        if ($request->param === 'S') {

            $req->stareq = 'A';
            $req->aprreq = 'G';
            $req->gerapru = $user->codemp;
            $req->fecaprger = $date;

            $req->update();
        } else if ($request->param === 'N') {
            $req->stareq = $request->param;
            $req->motreq = $request->motanu;
            $req->gerapru = $user->codemp;
            $req->fecanu = $date;
            $req->fecaprger = null;

            $req->update();
        }

        return response()->json(['success' => true, 'Procesado con éxito']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generarReporteRequisicion(Request $request)
    {

        $requisicion = Casolart::where('reqart', $request->reqart)->with('ccosto', 'articulos', 'solicitante', 'proveedor', 'aprobador')->first();

        $desde = $request['desde'] ?? $requisicion->created_at;
        $hasta = $request['hasta'] ?? $requisicion->created_at;
        $diames = $request['diames'];
        $reqart = $request->reqart;

        switch ($diames) {
            case 'diario':
                $tipo = 'DIARIO';
                break;
            case 'ayer':
                $tipo = 'DÍA ANTERIOR';
                break;
            case 'semanal':
                $tipo = "SEMANAL";
                break;
            case 'semant':
                $tipo = "SEMANA ANTERIOR";
                break;
            case 'mensual':
                $tipo = 'MENSUAL';
                break;
            case 'mesant':
                $tipo = 'MES ANTERIOR';
                break;
            case 'anual':
                $tipo = 'ANUAL';
                break;
            case 'antyear':
                $tipo = 'AÑO ANTERIOR';
                break;
            case 'byCod':
                $tipo = 'Individual ' . $request->codfrom;
                break;
            default:
                $tipo = 'INTERVALO ENTRE ' . $desde . ' HASTA ' . $hasta;
                break;
        }
        $sucuse_reg = Auth::user()->getCodigoActiveS();

        ///EVALUA LOS PERIODOS DE TIEMPO SELECCIONADOS Y SI SERA UNA CONSULTA GENERALIZADA O POR SUCURSAL

        if (!$requisicion) {
            Session::flash('error', 'No existen registros con los parametros de busqueda ingresados');
            return redirect()->route('reporteEAlmacen');
        }

        $pdf = new generadorPDF();

        $titulo = utf8_decode("REPORTE DE PEDIDO");
        $fecha = date('d-m-Y');

        $pdf->AddPage('P');
        $pdf->SetTitle($titulo);
        $pdf->SetFont('arial', 'B', 16);
        $pdf->SetWidths(array(90, 90));
        $pdf->Ln();
        $pdf->Cell(0, 22, utf8_decode($titulo), 0, 0, 'C');
        $pdf->Ln();
        $pdf->SetFont('arial', 'B', 10);
        $pdf->Ln(5);
        $pdf->Cell(0, 0, utf8_decode('FECHA EMISIÓN: ' . $fecha), 0, 0, 'L');
        // $pdf->Ln(5);
        //$pdf->Cell(0,0,utf8_decode('TOTAL: '.$total),0,0,'L');
        $pdf->Ln(10);
        $pdf->SetFont('arial', 'B', 8, 5);
        $pdf->SetFillColor(2, 157, 116);
        $pdf->Cell(47, 8, utf8_decode('SOLICITANTE'), 1, 0, 'C');
        $pdf->Cell(63, 8, utf8_decode('DEPARTAMENTO'), 1, 0, 'C');
        $pdf->Cell(40, 8, utf8_decode('NÚMERO'), 1, 0, 'C');
        $pdf->Cell(40, 8, utf8_decode('FECHA'), 1, 0, 'C');
        $pdf->Ln();
        $pdf->SetWidths(array(47, 47, 47, 47,));
        $pdf->SetAligns(['C', 'C', 'C', 'C', 'C', 'C', 'C', 'C']);
        $pdf->SetFont('arial', '', 8, 5);

        $last = 1;
        $limite = 1;
        $index = 0;


        $pdf->SetWidths(array(47, 63, 40, 40));
        $pdf->SetAligns(['C', 'C', 'C', 'C', 'C', 'C', 'C', 'C']);
        $pdf->SetFont('arial', '', 8, 5);


        $pdf->Row(
            array(
                utf8_decode($requisicion->solicitante->nomemp),
                utf8_decode($requisicion->ccosto->descen),
                $requisicion->reqart,
                $requisicion->fecreq,
            )
        );



        $pdf->Ln();
        $pdf->SetFont('arial', 'B', 8, 5);
        $pdf->SetFillColor(2, 157, 116);
        $pdf->Cell(190, 6, utf8_decode('JUSTIFICACIÓN'), 1, 0, 'C');
        $pdf->Ln();
        $pdf->SetWidths(array(190));
        $pdf->SetAligns(['C']);
        $pdf->SetFont('arial', '', 8, 5);
        $pdf->Row(
            array(
                utf8_decode($requisicion->desreq),
            )
        );



        $pdf->Ln();
        $pdf->SetFont('arial', 'B', 9);
        // $pdf->SetFillColor(2,157,116);
        $pdf->Cell(190, 6, utf8_decode('ARTÍCULOS'), 1, 1, 'C');
        $limit = (int)280 / 3;
        $pdf->Cell(110, 6, utf8_decode('DESCRIPCIÓN'), 1, 0, 'C');
        $pdf->Cell(40, 6, utf8_decode('UNIDAD DE MEDIDA'), 1, 0, 'C');
        $pdf->Cell(40, 6, utf8_decode('CANT. REQUERIDA'), 1, 1, 'C');
        /* $pdf->Cell(40,6,utf8_decode('COSTO'),1,0,'C');
        $pdf->Cell(40,6,utf8_decode('RECARGO'),1,0,'C');
        $pdf->Cell(40,6,utf8_decode('DESCUENTO'),1,0,'C');
        $pdf->Cell(40,6,utf8_decode('TOTAL'),1,1,'C');  */
        $pdf->SetWidths(array(110, 40, 40));
        $pdf->SetAligns(['C', 'C', 'C']);
        $pdf->SetFont('arial', '', 9);

        foreach ($requisicion->articulos as $key => $articulo) {


            $pdf->Row(
                array(
                    utf8_decode($articulo->desart),
                    utf8_decode($articulo->unimed),
                    $articulo->canreq,
                    /*  $articulo->costo,
                    $articulo->monrgo,
                    $articulo->mondes,
                    $articulo->montot */
                )
            );
        }
        $pdf->Ln(20);
        $pdf->SetFont('arial', 'B', 10);
        $pdf->Cell(0, 0, utf8_decode('RESPONSABLES'), 0, 0, 'C');
        $pdf->SetFont('arial', 'B', 8, 5);
        $pdf->SetFillColor(2, 157, 116);
        $pdf->Ln(10);
        $pdf->Cell(0, 0, utf8_decode('Solicitado Por: '.$requisicion->solicitante->nomemp), 0, 1, 'L');
        $pdf->Cell(0, 0, utf8_decode('Aprobado Por: '.$requisicion->aprobador->nomuse), 0, 1, 'R');
        $pdf->Ln(10);
        $pdf->Cell(40, 8, utf8_decode('SOLICITANTE'), 1, 0, 'L');
        $pdf->cell(110, 0, '', 0, 0, 'C');
        $pdf->Cell(40, 8, utf8_decode('APROBADO'), 1, 0, 'R');
        $pdf->Ln(8);
        $pdf->cell(40, 24, '', 1, 0, 'L'); //firma
        $pdf->cell(110, 0, '', 0, 0, 'C');
        $pdf->cell(40, 24, '', 1, 0, 'R'); //firma
        $pdf->Ln(20);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Ln(5);
        $pdf->Output('I', 'PEDIDO-' . $fecha . 'pdf');

        exit;
    }
}
