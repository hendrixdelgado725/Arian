<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use \Fpdf;
use App\Fapresup;
use App\Fapreserart;
use App\Facliente;
use App\Sucursal;
use Helper;
use Carbon\Carbon;
use App\Http\Controllers\generadorPDF;
use Exception;
use Session;
use OwenIt\Auditing\Models\Audit;
use App\Charts\Ventas;
use App\Fafactur;
use App\Models\Caja;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
use App\Segususuc;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        #$sec = Segususuc::where('codusuario',Auth::user()->codigoid)->where('activo','TRUE')->first();   
        return view('home')->with('menus', $this->retornarMenu());
    }

    public function retornarMenu()
    {
        $yaml = new Parser();
        $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
        return $menu['menu'];
    }

    public static function AsocIcon($NomModulos)
    {

        $arrayIconos = array(
            'Finanzas' => '<img class="mr-2" height="25px" src=' . asset('/images/arian/iconos/finanzas_negro.png') . '>',
            'Almacén' => '<img class="mr-2" height="25px" src=' . asset('/images/arian/iconos/almacen_negro.png') . '>',
            'Compras' => '<img class="mr-2" height="25px" src=' . asset('/images/arian/iconos/almacen_negro.png') . '>',
            'Facturacion' => '<img class="mr-2" height="25px" src=' . asset('/images/arian/iconos/facturacion_negro.png') . '>',
            'Reportes' => '<img class="mr-2" height="25px" src=' . asset('/images/arian/iconos/reportes_negro.png') . '>',
            'Estadisticas' => '<img class="mr-2" height="25px" src=' . asset('/images/arian/iconos/estadisticas_negro.png') . '>',
            'Seguridad' => '<img class="mr-2" height="25px" src=' . asset('/images/arian/iconos/seguridad_negro.png') . '>',
            'Configuración General' => '<img class="mr-2" height="25px" src=' . asset('/images/arian/iconos/configuracion_negro.png') . '>',
            'Fasvit' => '<img class="mr-2" height="25px" src=' . asset('/images/arian/iconos/configuracion_negro.png') . '>'
        );

        return $arrayIconos[$NomModulos];
    }

    public static function AsocIcon2($NomModulos, $NomSubModulos)
    {

        $arrayIconos = array(
            'Finanzas' => [
                'Precios' => '<i class="fas fa-money-bill"></i>',
            ],
            'Almacén' => [
                'Registro de Almacenes' => '<i class="fas fa-plus-circle"></i>',
                'Artículos' => '<i class="fas fa-box"></i>',
                'Escala' => '<i class="fas fa-align-left"></i>',
                'Movimientos de Inventario' => '<i class="fa fa-truck" aria-hidden="true"></i>',
                'Ubicaciones de Almacen' => '<i class="nav-icon fas fa-th"></i>',
                'Estatus Inventario' => '<i class="fa fa-check" aria-hidden="true"></i>',
                'Nota de Entrega' => '<i class="fa fa-file"></i>',
                'Promociones'=> '<i class="fa fa-file"></i>',
            ],
            'Facturacion' => [
                'Facturas' => '<i target="_blank" class="fa fa-list" aria-hidden="true"></i>',
                'Emision Factura V2' => '<i class="fa fa-desktop" aria-hidden="true"></i>',
                'Facturacion Mayorista' => '<i class="fa fa-desktop" aria-hidden="true"></i>',
                'Facturacion Mayorista' => '<i class="fa fa-desktop" aria-hidden="true"></i>',
                'Seriales' => '<i class="fa fa-desktop" aria-hidden="true"></i>',
                'Nota de Entrega Facturacion' => '<i class="fa fa-desktop" aria-hidden="true"></i>',
                'Turnos' => '<i class="fas fa tag"></i>',
            ],
            'Reportes' => [
                'Libro de Ventas' => '<i class="nav-icon fas fa-book"></i>',
                'Libro de VentasvS' => '<i class="nav-icon fas fa-book"></i>',
                'Productos Vendidos' => '<i class="fa fa-clipboard" aria-hidden="true"></i>',
                'Productos Vendidos Totales' => '<i class="fa fa-clipboard" aria-hidden="true"></i>',
                'Movimiento de Inventario' => '<i class="fas fa-archive"></i>',
                'Facturas Detalladas' => '<i class="fas fa-file"></i>',
                'Existencia Real por Almacén' => '<i class="far fa-file-alt"></i>',
                'Entrada a Almacén' => '<i class="fas fa-arrow-left"></i>',
                'Salida de Almacén' => '<i class="fa fa-arrow-right"></i>',
                'Reporte Clientes' => '<i class="fas fa-file-powerpoint"></i>',
                'Presupuestos' => '<i class="fa fa-file" aria-hidden="true"></i>',
                'Reporte Traspasos' => '<i class="far fa-sticky-note"></i>',
                'Cierre de Caja' => '<i class="far fa-sticky-note"></i>',
                'Artículos por Categoría' => '<i class="far fa-sticky-note"></i>',
                'Catalogo de Productos' => '<i class="far fa-sticky-note"></i>',
                'Listado Detallado de Factura' => '<i class="far fa-sticky-note"></i>',
                'Compras' => '<i class="fas fa-cog"></i>',
                'Estatus de Seriales' => '<i class="far fa-sticky-note"></i>',
                'Resumen Productos Vendidos' => '<i class="fa fa-clipboard" aria-hidden="true"></i>'


            ],
            'Estadisticas' => [
                'Ventas por Sucursales' => '<i class="nav-icon fas fa-book"></i>',
                'Ventas por Caja' => '<i class="nav-icon fas fa-book"></i>',
                'Ventas anuales' => '<i class="far fa-chart-bar"></i>'
            ],
            'Seguridad' => [
                'Usuarios' => '<i class="fas fa-users"></i>',
                'Parametrizacion' => '<i class="fas fa-lock"></i>',
                'Permisiología' => '<i class="fas fa-lock"></i>',
                'Cambiar Contraseña' => '<i class="fas fa-user-lock"></i>',
                'Auditoria' => '<i class="fas fa-door-open"></i>',
                'Reiniciar Contraseña de Caja' => '<i class="fas fa-key"></i>',
                'Definir Cargo' => '<i class="fas fa-file-prescription"></i>'
            ],
            'Configuración General' => [
                'Definición de la Empresa' => '<i class="fas fa-th-large"></i>',
                'Duración de Presupuesto' => '<i class="fas fa-tag"></i>',
                'Clientes' => '<i class="fas fa-users"></i>',
                'Caja' => '<i class="fas fa-box"></i>',
                'Forma de Pago' => '<i class="fas fa-expand"></i>',
                'Descuento' => '<i class="far fa-star"></i>',
                'Recargo' => '<i class="far fa-bookmark"></i>',
                'Moneda' => '<i class="fas fa-coins"></i>',
                'Tasa de Cambio' => '<i class="fas fa-share"></i>',
                'Config. de Producto' => '<i class="fas fa-cog"></i>',
                'Cargo de Empleado' => '<i class="fas fa-portrait"></i>',
                'Empleado' => '<i class="fas fa-walking"></i>',
                'Presupuesto' => '<i class="fas fa-arrow-circle-right"></i>',
                'División Territorial' => '<i class="fas fa-road"></i>',
                'Sucursales' => '<i class="far fa-flag"></i>',
                'Centros de Costos' => '<i class="fa fa-money"></i>',
                'Compras' => '<i class="fas fa tag></i>',
                'Proveedores' => '<i class="fas fa tag></i>',
                
            ],
            /* 'Fasvit' => [
                'Solicitudes de familiares' => '<i class="fas fa-tag"></i>',
                'Reporte Familiares asegurados' => '<i class="fas fa-tag"></i>',
                'Patologias' => '<i class="fas fa-tag"></i>'
            ],  */
            'Compras' => [
                'Pedidos' => '<i class="fas fa tag"></i>',
                /* 'Emisión de Punto de Cuenta' => '<i class="fas fa tag"></i>',
                'Requisiciones por Departamento' => '<i class="fas fa-tag"></i>',
                'Precompromisos' => '<i class="fas fa tag"></i>', --
                'Aprobación de la Solicitud de Egreso por Lotes' => '<i class="fas fa tag></i>', */
                'Cotizaciones' => '<i class="fas fa tag></i>',
                'Ordenes' => '<i class="fas fa tag></i>',
                'Pagos' => '<i class="fas fa tag></i>',
            ]
        );

        return $arrayIconos[$NomModulos][$NomSubModulos];
    }

    public static function blank($element){
        
        switch ($element) {
            case 'Emision Factura V2':
                return '_blank';
            break;   
        
        }
        return '';
    }

    public static function AsocIcon3($NomModulos, $NomSubModulos, $NomSubModulos3)
    {
        $arrayIconos = array(

        'Almacén' => [
            'Movimientos de Inventario' => [
                'Tipo de Movimiento'=> '<i class="fas fa-lightbulb"></i>',
                'Entradas/ Salidas' => '<i class="fas fa-lightbulb"></i>',
                'Traspasos'         => '<i class="fas fa-lightbulb"></i>'
            ]
        ],
        'Configuración General' => [

            'Config. de Producto' => [
                /* 'Descripcion del Producto' => '<i class="fas fa-file-signature"></i>', */
                'Categoría de Producto' => '<i class="fas fa-file-contract"></i>',
                'SubCategoría de Producto' => '<i class="fas fa-door-closed"></i>',
                'Unidad de Medida' => '<i class="fas fa-file-prescription"></i>',
                'Color' => '<i class="fas fa-feather-alt"></i>',
                'Talla' => '<i class="fas fa-file-prescription"></i>',
                'Marcas' => '<i class="fas fa-file-prescription"></i>'
            ],
            
                'División Territorial' => [

                    'Pais'   => '<i class="far fa-bell"></i>',
                    'Estado' => '<i class="far fa-bell"></i>'
                ],
                'Compras' => [

                    'Proveedores'   => '<i class="far fa-bell"></i>',
                    'Retenciones' => '<i class="far fa-bell"></i>'
                ]
            ],
            'Reportes' => [
                'Compras' => [
                    'Listado Resumen Ordenes de Compras' => '<i class="fas fa-file-prescription"></i>'
                ],
            ],
        );

        return $arrayIconos[$NomModulos][$NomSubModulos][$NomSubModulos3];
    }
}
