<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use App\Almacen;
use Exception;
use App\Empresa;
use App\Bancos;
use Session;
use Helper;
use Illuminate\Contracts\Auth\Guard;
use \App\permission_user;
use App\Segususuc;
use Illuminate\Support\Facades\Auth;

class EmpresaController extends Controller
{

    private $guard;
    public function __construct(Guard $guard)
    {
        $this->middleware('auth');
        $this->guard = $guard;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      /*$seg = permission_user::where('codusuarios',$this->guard->user()->userSegSuc->codusuario)->get();
        
         if (is_object($seg)) {
            
            //dd($seg);
              for ($i=0; $i < count($seg); $i++) { 
             
                  $this->authorize('vista',$seg[$i]);
              }

         }*/
      $empresas = Empresa::paginate(10);
      return view('configuracionGenerales.Empresa.listEmpresa')
                ->with(['menus' => $this->getmenu(),'empresas' => $empresas]);
    }             

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      return view('configuracionGenerales.Empresa.createEmpresa')
                        ->with(['menus'=> Menu::getMenu(),
                                'almacenes' => Almacen::get(),
                                  'bancos' => $this->getbancos()
                                ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'tipodoc' => 'required',
        'codrif' => 'required|between:10,10',
        'nomrazon' => 'required',
        'dirfis' => 'required',
        'tipemp' => 'required',
        'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

      if($request->hasFile('logo')){
        $fileName = time().$request->file('logo')->getClientOriginalName();
        $newLogo = $request->file('logo')->storeAs('images', $fileName, 'public');
      }
        if($request->codrif && strpos($request->codrif,'-')){
          $codrif= $request->tipodoc.str_replace('-',"",$request->codrif);
          $request->merge(['codrif'=> $codrif]);
        }

            $tobank = [];
            $entidades= array_filter($request->entidad);
            $numcuentas = array_filter($request->numcuenta);
            $numitems= 0;
          try{
            foreach ($entidades as $mainkey => $value) {
              if(($entidades[$mainkey] !== null )&&($numcuentas[$mainkey]!== null)){
                $numcuenta = str_replace('-',"",$numcuentas[$mainkey]);
                if(!(strlen($numcuenta)=== 20)) throw new Exception("Los numeros de cuenta deben tener 20 digitos exactos");
                if(!(substr($numcuentas[$mainkey],0,4) === $this->comparebancos($value)))
                throw new Exception("La Cuenta Bancaria ingresada no coincide con la nomenclatura del Banco seleccionado");
                $tobank[]=[
                  'codban' => $numcuenta,
                  'nomban' => $value,
                  'rifempre' => $codrif
                ];
                $numitems++;
              }
            }
            //if($numitems === 0)throw new Exception("La Empresa debe poseer como mínimo un Banco seleccionada con su respectiva Cuenta");
            Empresa::create([
              'codrif' => strtoupper($codrif),
              'nomrazon' => strtoupper($request->nomrazon),
              'dirfis' => strtoupper($request->dirfis),
              'tipemp' => $request->tipemp,
              'logo' => $newLogo,
            ]);
            if(isset($tobank)){
              Bancos::insert($tobank);
            }
            Session::flash('success', 'La Empresa '.$request->nomrazon.' , se registro Exitosamente');
            return redirect()->route('EmpresaList')->with($this->comboview());
          }catch(Exception $e){
            return redirect()->route('EmpresaCreate')->with('error',$e->getMessage());
          }
          ///dd($tobank);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $id = Helper::decrypt($id);
      $empresa = Empresa::findOrFail($id);
      $bancos = Bancos::where('rifempre','=',$empresa->codrif)->get();
      return view('configuracionGenerales.Empresa.editEmpresa')->with([
        'menus'=> $this->getmenu(),
        'empresa'=>$empresa,
        'bancos'=> $this->getbancos(),
        'banktojson'=> json_encode($this->getbancos()),
        'bankempre' => $bancos,
        'almacenes' => Almacen::get()
      ]);
    }
  
    public function putselect($array,$bancos){
      for ($i=0; $i < count($array); $i++) {
          foreach ($bancos as $key) {
            if($key->nomban === $array[$i]["nombanc"]) $array[$i]["selected"] = "selected";
          }
      }
      return $array;
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if($request->codrif && strpos($request->codrif,'-')){
        $codrif= $request->tipodoc.str_replace('-',"",$request->codrif);
        $request->merge(['codrif'=> $codrif]);
      }
      $empresa = Empresa::findOrFail($id);
      $request->validate(
        [
          'tipodoc' => 'required',
          'nomrazon' => 'required',
          'dirfis' => 'required',
          'tipemp' => 'required',
        ]);
      
      $tobank = [];
      $entidades= $request->entidad;
      $numcuentas = $request->numcuenta;
      $numitems= 0;
      try{
        foreach ($entidades as $mainkey => $value) {
          if(($entidades[$mainkey] !== null )&&($numcuentas[$mainkey]!== null)){
            $numcuenta = str_replace('-',"",$numcuentas[$mainkey]);
            if(!(strlen($numcuenta)=== 20)) throw new Exception("Los numeros de cuenta deben tener 20 digitos exactos");
            if(!(substr($numcuentas[$mainkey],0,4) === $this->comparebancos($value)))
            throw new Exception("La Cuenta Bancaria ingresada no coincide con la nomenclatura del Banco seleccionado");
            $tobank[]=[
              'codban' => $numcuenta,
              'nomban' => $value,
              'rifempre' => $codrif
            ];
            $numitems++;
          }
        }

        if($request->hasFile('logo')){
          $fileName = time().$request->file('logo')->getClientOriginalName();
          $newLogo = $request->file('logo')->storeAs('images', $fileName, 'public');
        }
        else {
          $newLogo = $empresa->logo;
        }
        if($numitems === 0)throw new Exception("La Empresa debe poseer como mínimo un Banco seleccionada con su respectiva Cuenta");
          $empresa->codrif = strtoupper($codrif);
          $empresa->nomrazon = strtoupper($request->nomrazon);
          $empresa->dirfis = strtoupper($request->dirfis);
          $empresa->tipemp = $request->tipemp;
          $empresa->logo = $newLogo;
          $empresa->save();
        Bancos::where('rifempre','=',$request->oldrif)->delete();
        Bancos::insert($tobank);
        Session::flash('success', 'La Empresa '.$request->nomrazon.', se actualizó Exitosamente');
        return redirect()->route('EmpresaList')->with($this->comboview());
      }catch(Exception $e){
        return redirect()->route('EmpresaShow',$id=Helper::crypt($id))->with('error',$e->getMessage());
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
/*     public function update(Request $request, $id)
    {

    } */

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {

      try{
        $id = Helper::decrypt($id);
        if(!$request->ajax())throw new Exception("Not an Ajax Request");
        $empresa =  Empresa::findOrFail($id);
        //validar que no exista algo con el pais seleccionado
        $bancos = Bancos::where('rifempre','=',$empresa->codrif);
        $bancos->delete();
        $res = $empresa->delete();
        if($res) return response()->json(["exito" => "Empresa Eliminada Exitosamente"]);
      }catch(Exception $e){
        return response()->json([
          "error" => $e->getMessage()
        ]);
      }
    }

    protected function comboview(){
      return ['menus' => $this->getmenu(),
              'empresa' => Empresa::get()];
    }

    protected function comparebancos($nombank){
        $bancos = $this->getbancos();
        foreach ($bancos as $key => $value) {
          $return = $value["nombanc"] === $nombank ? $value["num"] : null;
          if($return)break;
        }
        return $return;
    }

 /*   public function filtro(Request $request){
      $empresas = Empresa::where('nomrazon','ilike','%'.$request->filtro.'%')->orWhere('codrif','ilike','%'.$request->filtro.'%')->paginate(10);

      return view('configuracionGenerales.Empresa.listEmpresa')
                ->with(['menus' => $this->getmenu(),'empresas' => $empresas]);
    }*/

    public function getmenu(){
      return $this->menu = Menu::getmenu();
  }

    protected function getbancos(){
      return [
        0 => ['num' => '0156','nombanc' => '100%BANCO'],
        1 => ['num' => "0196" , 'nombanc' => 'ABN AMRO BANK'],
        2 => ['num' => "0172" , 'nombanc' => 'BANCAMIGA BANCO MICROFINANCIERO, C.A.'],
        3 => ['num' => "0171" , 'nombanc' => 'BANCO ACTIVO BANCO COMERCIAL, C.A.'],
        4 => ['num' => "0166" , 'nombanc' => 'BANCO AGRICOLA'],
        5 => ['num' => "0175" , 'nombanc' => 'BANCO BICENTENARIO'],
        6 => ['num' => "0128" , 'nombanc' => 'BANCO CARONI, C.A. BANCO UNIVERSAL'],
        7 => ['num' => "0001" , 'nombanc' => 'BANCO CENTRAL DE VENEZUELA.'],
        8 => ['num' => "0102" , 'nombanc' => 'BANCO DE VENEZUELA S.A.I.C.A.'],
        9 => ['num' => "0114" , 'nombanc' => 'BANCO DEL CARIBE C.A.'],
        10 => ['num' => "0163" , 'nombanc' => 'BANCO DEL TESORO'],
        11 => ['num' => "0176" , 'nombanc' => 'BANCO ESPIRITO SANTO, S.A.'],
        12 => ['num' => "0115" , 'nombanc' => 'BANCO EXTERIOR C.A.'],
        13 => ['num' => "0173" , 'nombanc' => 'BANCO INTERNACIONAL DE DESARROLLO, C.A.'],
        14 => ['num' => "0105" , 'nombanc' => 'BANCO MERCANTIL C.A.'],
        15 => ['num' => "0191" , 'nombanc' => 'BANCO NACIONAL DE CREDITO'],
        16 => ['num' => "0116" , 'nombanc' => 'BANCO OCCIDENTAL DE DESCUENTO.'],
        17 => ['num' => "0138" , 'nombanc' => 'BANCO PLAZA'],
        18 => ['num' => "0108" , 'nombanc' => 'BANCO PROVINCIAL BBVA'],
        19 => ['num' => "0104" , 'nombanc' => 'BANCO VENEZOLANO DE CREDITO S.A.'],
        20 => ['num' => "0168" , 'nombanc' => 'BANCRECER S.A. BANCO DE DESARROLLO'],
        21 => ['num' => "0177" , 'nombanc' => 'BANFANB'],
        22 => ['num' => "0146" , 'nombanc' => 'BANGENTE'],
        23 => ['num' => "0174" , 'nombanc' => 'BANPLUS BANCO COMERCIAL C.A'],
        24 => ['num' => "0190" , 'nombanc' => 'CITIBANK.'],
        25 => ['num' => "0157" , 'nombanc' => 'DELSUR BANCO UNIVERSAL'],
        26 => ['num' => "0151" , 'nombanc' => 'FONDO COMUN'],
        27 => ['num' => "0601" , 'nombanc' => 'INSTITUTO MUNICIPAL DE CRÉDITO POPULAR'],
        28 => ['num' => "0169" , 'nombanc' => 'MIBANCO BANCO DE DESARROLLO, C.A.'],
        29 => ['num' => "0137" , 'nombanc' => 'SOFITASA'],
        30 => ['num' => "0134" , 'nombanc' => 'BANESCO BANCO UNIVERSAL']
      ];
    }

    /*busqueda de filtro*/
    public function filtro(Request $request)
    {
        $menu = new HomeController();
        $empresas= Empresa::where('deleted_at',NULL)->where('nomrazon','ilike','%'.$request->filtro.'%')->OrWhere('codrif','ilike','%'.$request->filtro.'%')->paginate(8);
        
        return view('configuracionGenerales.Empresa.listEmpresa')
                ->with(['menus' => $this->getmenu(),'empresas' => $empresas]);
                
    }
}