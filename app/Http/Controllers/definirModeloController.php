<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\famodelo;
use Illuminate\Support\Facades\Auth; 
use Carbon\Carbon;
use Helper;
use Session;

class definirModeloController extends Controller
{	

 
	public function __construct()
    {
        $this->middleware('auth');
        
        
    }
    public function queryModelo()
    {
    	$modelo = \App\famodelo::all();
    	return $modelo;
    }
    public function index(){
    		
    	$menu = new HomeController();

    	 $modelo = \App\famodelo::paginate(8);

    	return view('configuracionGenerales.definirModelo')->with('menus',$menu->retornarMenu())->with('modelo',$modelo);

    }

    public function crear(){

    	$menu = new HomeController();
    	$color = \App\ColorPro::all();
    	$categoria = \App\Facategoria::all();
    	$subcategoria = \App\fasubcategoria::all();
    	return view('configuracionGenerales.definirModeloRegistrar')->with('menus',$menu->retornarMenu())->with('color',$color)
    				->with('categoria',$categoria)->with('subcategoria',$subcategoria);
    	
    }

    public function crearModelos(Request $request){
    	

    			//dd($request->file('banner'));
  				$request->validate([

  					'nomenc' => 'required|string'
  				]);

    		     $sucuse_reg=Auth::user()->getsucUse()->codsuc;
                 $guion='-';
                 $cont = famodelo::withTrashed()->get()->count();
                 $cont = $cont+1;
                 // $suc = Auth::user()->codsuc_reg;
                 $suc=Auth::user()->getCodigoActiveS();
                 $codsuc_reg = explode('-',$suc);
                 $cod = $cont.$guion.$codsuc_reg[1];    
        		
    	   
    		     famodelo::insert([
                    'defmodelo' => $request->nomenc,
                    'codigoid'  => $cod,
                    'codsuc_reg' => $sucuse_reg,
					'codsuc' => $sucuse_reg,
                    'created_at' => Carbon::now()
                ]);
    		


    		$menu = new HomeController();
    		//dd("llego");

    		return redirect()->route('definirMarcas')->with('menus',$menu->retornarMenu())->with('modelo',$this->queryModelo());
    }

	public function editModelo($id){

		$menu = new HomeController();
		$desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= $sep[1].'-'.$sep[2];
		$modelo = famodelo::where('codigoid',$ID)->first();

    	return view('configuracionGenerales.editarModeloMarcas')->with('menus',$menu->retornarMenu())->with('modelo',$modelo);

	}


	public function updateMarcas(Request $request, $id){

		$request->validate([
			'nomenc' => 'required|string'
		]);

        $modelo = famodelo::findOrFail($id);
        $modelo->defmodelo = $request->nomenc;
        $modelo->update();
        
        Session::flash('success', 'Registro Editado con exito.');
        return redirect()->route('definirMarcas');

    }

   /* public function editarModelo($id)
    {
    	$modelo = \App\famodelo::where('nomenclatura',$id)->first();
    	$menu = new HomeController();
    	$color = \App\ColorPro::all();
    	$categoria = \App\Facategoria::all();
    	$subcategoria = \App\fasubcategoria::all();
    	return view('configuracionGenerales.editarModelos')->with('menus',$menu->retornarMenu())->with('modelos',$modelo)->with('color',$color)->with('categoria',$categoria)->with('subcategoria',$subcategoria);
    }*/

/*    public function modelosEditados(Request $request)
    {		//dd($request->editarSubcategoria);
    	  	if ($request->file('banner')) {
    			//dd("entro");
    			$path = Storage::disk('public')->put('image',$request->file('banner'));
    			
    			//dd($path);
    			$modelos = \App\famodelo::where('nomenclatura',$request->nomenclatura)->first();

    			//dd($modelos);
    			$modelos->nomenclatura   = $request->nomenclatura;
    			$modelos->defmodelo 	 = $request->nombreModelo;
    			$modelos->nomencategoria = $request->editarCategoria;
    			$modelos->nomencolor 	 = $request->editarColor;
    			$modelos->nomsubcat      = $request->nomsubcategoria;
    			$modelos->imgphoto       = $path;

    			$modelos->fill(['banner'=>asset($path)])->save();
    				
    		}
			$menu = new HomeController();
    		return redirect()->route('definirModelo')->with('menus',$menu->retornarMenu())->with('modelo',$this->queryModelo());
    }
*/
    public function borrarModelo($id)
    {
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= $sep[1].'-'.$sep[2];
	    	
	    	$modelo = \App\famodelo::where('codigoid',$ID)->delete();
	    	
	    	

	    	return response()->json([

	    		'titulo' => 'SE HA ELIMINADO CON EXITO'
	    	]);
    	
    }

    public function filtro(Request $request)
    {
    	 $modelo = \App\famodelo::where('defmodelo','ilike','%'.$request->filtro.'%')->paginate(10);
    	 $menu = new HomeController();
    	 return view('configuracionGenerales.definirModelo')->with('menus',$menu->retornarMenu())->with('modelo',$modelo);
    }
    
}
