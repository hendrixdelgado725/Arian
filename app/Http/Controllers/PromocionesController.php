<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\PromocionesRequest;
use App\Famoneda;
use App\Caregart;
use App\Almacen;
use App\Models\Cadefpro;
use App\Models\Cadetpro;
use App\Facategoria;
use App\faartpvp;
use Session;
use App\Fasubcategoria;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
use App\Console\Commands\PromocionesAnular;

class PromocionesController extends Controller {

    public function index(){
        $menu = new HomeController();
        $articulopro = Cadefpro::orderBy('id','DESC')->paginate(10);
        return view('configuracionGenerales.promociones.listadoPromociones')->with([
        	'menus'=>$menu->retornarMenu(), 'articulopro'=>$articulopro]); 
    }

    public function filtro(Request $request){ 
       
        $filtro = strtoupper( $request->filtro);
        $menu = new HomeController();
        $articulopro= Cadefpro::where('deleted_at',NULL);
    
        if ($filtro == "ACTIVO") {
            $articulopro = $articulopro->where('status', 'A');
        } elseif ($filtro == "PENDIENTE") {
            $articulopro = $articulopro->where('status', 'P');
        } elseif ($filtro == "INACTIVO") {
            $articulopro = $articulopro->where('status', 'I');
        } else {
            $articulopro = $articulopro->where('codprom','ilike','%'.$filtro.'%')
            ->OrWhere('status','ilike','%'.$filtro.'%')->OrWhere('desprom','ilike','%'.$filtro.'%');
        }
    
        $articulopro = $articulopro->orderBy('id','DESC')->paginate(8);
    
           
        return view('configuracionGenerales.promociones.listadoPromociones')->with('menus',$menu->retornarMenu())->with('articulopro', $articulopro);
    }

    public function create(){
        $menu = new HomeController();
        return view('configuracionGenerales.promociones.crearPromociones')->with([
        	'menus'=>$menu->retornarMenu()]); 
    }

    public function getinfo(){
        $count = Cadefpro::withTrashed()->get()->count();
        $nomcre = Auth::user();
        $alma = Almacen::where('codsuc_asoc',session('codsuc'))->select('codalm','nomalm')->where('pfactur',true)->first(); 
        if($alma === null) throw new \Exception('Tienes que poner el almacen que esta asociado, a facturar en modulo de registro almacen');

        
        $almacen = Almacen::where('codsuc_asoc', session('codsuc'))->select('codalm','nomalm')->where('pfactur',true)->get(); 
        $moneda = Famoneda::select('codigoid', 'nombre')->get();
        return response()->json([  'almacen'=>$almacen , 'moneda'=>$moneda ,'count'=>$count, 'predef'=>$alma->codalm ]);
    }
    public function getArticuloAlmacen(Request $request){
        $alm = Almacen::where('codsuc_asoc','=',session('codsuc'))->where('codigoid', $request->codalm)->select('codalm')->where('pfactur',true)->first();
        $articulos = Caregart::with('costoPro', 'almacenubi','inventario', 'faartpvp')->where('tipreg','F')
        ->whereHas('almacenubi',function($q) use ($alm){
          $q->where('exiact','>',0)->where('codalm',$alm->codalm);
        })->whereHas('costoPro', function($q) use ($request) {
            $q->where('status','=', 'A')->where('codmone',$request->codmon);
        })->select('codart','desart')->withSum('almacenubi','exiact')->get();
        if(empty($articulos->toArray())) return response()->json(['status' => 'error', 'message' => 'No se encontraron artículos con precio en esta moneda'], 500);
        return response()->json([ 'articulos'=>$articulos ]);

     }

    public function createPromo(PromocionesRequest $request){
     
        $artprom = (object)$request->form;
        $art = $request->articulos;
       
            Cadefpro::create(
                [
                    'codprom' => $artprom->codprom,
                    'codartprom' => $artprom->codartprom,
                    'fecdiprom' => $artprom->fecdiprom,   
                    'codalm' => $artprom->codalm,
                    'codmon' => $artprom->codigoid,
                    'agotexis' =>  $artprom->tiempoPromo,
                    'fedesprom'=>$artprom->fecdesprom,
                    'fechasprom'=>$artprom->fechasprom,
                    'codartprom'=>$artprom->codartprom,
                    'precio'=>$artprom->monartpro,
                    'codmon'=>$artprom->codigoid,
                    'agotexis'=>$artprom->tiempoPromo,
                    'status'=>$artprom->status,
                    'desprom'=>$artprom->desprom
                ]
            ); 
    
            for($i=0;$i<count($art);$i++){
                Cadetpro::create([
                    'codprom' => $artprom->codprom,
                    'codartprom' => $artprom->codartprom,
                    'codart'=>$art[$i]['codigo'],
                    'cantidad'=>$art[$i]['requerido'],
                ]);
            } 

            $sucuse_reg = Auth::user()->getsucUse()->codsuc;
            $guion1 = '-';
            $cont1 = Caregart::withTrashed()->get()->count();
            $cont1 = $cont1 + 1;
            $suc1=Auth::user()->getCodigoActiveS();
            $codsuc_reg1 = explode('-', $suc1);
            $cod1 = $cont1 . $guion1 . $codsuc_reg1[1];
          
            Caregart::create([
                'codart' => $artprom->codartprom,
                'desart' => $artprom->nomart,
                'codcat' =>  '3-0001',
                'tipreg' => 'F',
                'codemo' => '1-0005',
                'codsuc_reg' => $sucuse_reg ,
                'artprecio' => 'ACTIVO',
                'desfac' =>$artprom->codartprom,
                'codigoid' => $cod1,
                'codpar' => '3-0001',
                'isserial' =>'false',
                'staart' =>'I'
                ]);
    
            faartpvp::create([
                'codart' => $artprom->codartprom,
                'status' => 'A',
                'pvpart' =>$artprom->monartpro,
                'codigoid' => $cod1,
                'codsuc_reg' => $suc1,
                'codmone' => $artprom->codigoid,
                'codalm' => $artprom->codalm
            ]);       
    }   
    public function destroy($id)
    { 
        $promociones = Cadefpro::findOrFail($id);
        $codartprom = Cadefpro::where('id',$id)->value('codartprom');
        $promodetalle = Cadetpro::where('codartprom',$codartprom)->delete();
        $articulospromo =Caregart::where('codart', $codartprom)->delete();
        $promociones->delete();
        return back();
    }

    public function infoupdate($id){
        
        $id = intval($id);
        $var = Cadefpro::where('id',$id)->value('codprom');
        $menu = new HomeController();
        $nomcre = Auth::user();
        $alma = Almacen::where('codsuc_asoc', session('codsuc'))->select('codalm','nomalm')->where('pfactur',true)->first(); 
         if($alma === null) throw new \Exception('Tienes que poner el almacen que esta asociado, a facturar en modulo de registro almacen');
        $articulo = Caregart::with('almacenubi')->has('costos')
        ->where('tipreg','F')
        ->whereHas('almacenubi',function($q) use ($alma){
          $q->where('exiact','>',0)->where('codalm',$alma->codalm);
        })->select('codart','desart')->get();
        $articulos = Cadefpro::with('cadetpro.caregart','caregart')->where('id',$id)->get();
        $almacen = Almacen::where('codsuc_asoc', session('codsuc'))->select('codalm','nomalm')->get(); 
        $moneda = Famoneda::select('codigoid', 'nombre')->get();
        $codart = Cadetpro::where('codprom', $var)->pluck('codart');
        
       
        $consulta = cadetpro::with('caregart.almacenubi')->where('codprom', $var )->get();
     
        $datos = [
            'articulospro' => $articulos,
            'articulos' => $articulo,
            'almacen' => $almacen,
            'moneda' => $moneda,
            'art' => $consulta,
        ];
        
        return view('configuracionGenerales.promociones.editpromo')->with([ 'menus'=> $menu->retornarMenu(), 'datos'=>$datos ]);
    }

    public function activateArticulo(Request $request){
  
     $aprobado = cadefpro::where('codartprom', $request->codigo)->value('staprom');
     if($aprobado === "A"){
        $artpromocion = Caregart::where('codart', $request->codigo)->update(['staart' => $request->status]);
        $promocion = Cadefpro::where('codartprom',$request->codigo )->update(['status' => $request->status]);
     }

    }

    public function anularArticulo(Request $request){
           $nomcre = Auth::user();

              if($request->status == "A"){
                   $artpromocion = Caregart::where('codart', $request->codigo)->update(['staart' => 'A']);
                   $promocion = Cadefpro::where('codartprom',$request->codigo )->update(['status' => 'A']);
                   $promocion = Cadefpro::where('codartprom',$request->codigo )->update(['staprom' => $request->status]);
              }else{
                $artpromocion = Caregart::where('codart', $request->codigo)->update(['staart' => 'I']);
                $promocion = Cadefpro::where('codartprom',$request->codigo )->update(['status' => 'I']);
                $promocion = Cadefpro::where('codartprom',$request->codigo )->update(['staprom' => $request->status, 'nomcre' => $nomcre->loguse, 'motanu'=>$request->motivo]);
              }
              
       }

    public function detalles($id){

        $id = intval($id);
        $var = Cadefpro::where('id',$id)->value('codprom');
        $menu = new HomeController();
        $articulos = Cadefpro::with('cadetpro.caregart','caregart','moneda','almacen')->where('id',$id)->get();
        $almacen = Almacen::select('codalm','nomalm')->get(); 
        $moneda = Famoneda::select('codigoid', 'nombre')->get();
        $codart = Cadetpro::where('codprom', $var)->pluck('codart');
        $art = [];
        foreach ($codart as $codart) {
            $consulta = Caregart::with('cadetpro','almacenubi')->where('codart', $codart)->select('codart','desart','exitot')->get();
            $art[] = $consulta[0];
        }
        $datos = [
            'articulospro' => $articulos,
            'almacen' => $almacen,
            'moneda' => $moneda,
            'art' => $art,
        ];
        return view('configuracionGenerales.promociones.detallesPromociones')->with([ 'menus'=> $menu->retornarMenu(), 'datos'=>$datos ]);
    }


    
    public function update(PromocionesRequest $request){

        $artprom = (object)$request->form;
        $art = $request->articulos;
        $id = $request->id;
        $codartprom = cadefpro:: where('id', $id)->value('codartprom'); 
        $codprom = cadefpro:: where('id', $id)->value('codprom'); 
        $precio = faartpvp::where('codart' , $codartprom)->update(['pvpart' =>$artprom->monartpro]);

        $promociones = Cadefpro::where('id', $id)->update(
            [
                'codprom' => $codprom,
                'codartprom' => $artprom->codartprom,
                'fecdiprom' => $artprom->fecdiprom,   
                'codalm' => $artprom->codalm,
                'codmon' => $artprom->codigoid,
                'agotexis' => $artprom->tiempoPromo,
                'fedesprom'=>$artprom->fecdesprom,
                'fechasprom'=>$artprom->fechasprom,
                'codartprom'=>$artprom->codartprom,
                'precio'=>$artprom->monartpro,
                'codmon'=>$artprom->codigoid,
                'agotexis'=>$artprom->tiempoPromo,
                'status'=>$artprom->status,
                'desprom'=>$artprom ->desprom
            
            ]
        
        );
        for($i=0;$i<count($art);$i++){

            $codart = Cadetpro::where('codartprom', $codartprom)->value('codart');
            $articulos = Cadetpro::where('codprom', $codprom )->where('codartprom', $codartprom)->where('id', $art[$i]['id'])->get();

            if($art[$i]['id'] == null){

                Cadetpro::create([
                'codprom' => $codprom,
                'codartprom' => $artprom->codartprom,
                'codart' => $art[$i]['codigo'],
                'cantidad' => $art[$i]['requerido'],
                ]);
            }

            if($art[$i]['id'] !== null){
                                    
                $articulos = Cadetpro::where('id',$art[$i]['id'])->update([
                'codprom' => $codprom,
                'codartprom' => $artprom->codartprom,
                'codart' => $art[$i]['codigo'],
                'cantidad'=> $art[$i]['requerido'],
                ]);
                
            }

        } 
        $articulos = Cadetpro::where('codprom', $codprom)->where('codartprom', $codartprom)->get();

                foreach ($articulos as $articulo) {
                $encontrado = false;
                foreach ($art as $a) {
                    if ($articulo->codart == $a['codigo']) {
                        $encontrado = true;
                        break;
                    }
                }
                if (!$encontrado){
                    if(!$encontrado)
                    $articulo->delete(); 
                }
        }
        $articuloprom = Caregart::where('codart', $codartprom)->update([
            'desart' => $artprom->nomart,
            'codcat' =>  '3-0001',
            'tipreg' => 'F',
            'codemo' => '1-0005',
            'artprecio' => 'ACTIVO',
            'desfac' =>$artprom->codartprom,
            'codpar' => '3-0001',
            'isserial' =>'false',
        
        ]);
        $precio = faartpvp::where('codart', $codartprom)->update([
            'status' => 'A',
            'pvpart' =>$artprom->monartpro,
            'codmone' => $artprom->codigoid,
            'codalm' => $artprom->codalm
        ]);
   }

}
