<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\Fatippag;
use App\Faforpag;
use App\Fafacturpago;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Helper;
use Exception;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
use App\permission_user;
use App\Segususuc;

class RegistroFormaPagoController extends Controller
{
    private $guard;
    public function __construct(Guard $guard)
    {
        $this->middleware('auth');
        $this->guard = $guard;
    }

    /*Función que ejecuta la inserción de los archivos en la base de datos*/
    public function create(Request $request)
    {
        $valor=request()->all();
        $sucuse_reg=Auth::user()->getsucUse()->codsuc;
        $sep=explode('-', $sucuse_reg);
        $request->validate([
            'tipo'=>'required|string|min:5|max:30',
        ]);
        
        try{
            // $utlCodID=Fatippag::where([['codsuc_reg',$sucuse_reg]])->OrderByDesc('codigo_id')->first();
            // $codigoid=($utlCodID ? $utlCodID->codigo_id +1 : 1);

            $ftp=Fatippag::withTrashed()->get()->count();
            $codigoid=($ftp+1).'-'.$sep[1];

            $ultCodpag=Fatippag::withTrashed()->Orderbydesc('codtippag')->first();
            $codtippag= ($ultCodpag ? str_pad($ultCodpag->codtippag+1, 4, '0', STR_PAD_LEFT) : '0001');

            $verify = Fatippag::where('destippag','ilike','%'.$valor['tipo'].'%')->get();
            
            if(!$verify->isEmpty()) throw new Exception("esta forma de pago esta registrado");
            

            Fatippag::create(
                [
                    'codtippag' => $codtippag,
                    'destippag'=>$valor['tipo'],
                    'codigoid'=>$codigoid,
                    'codsuc_reg'=> $sucuse_reg,
                    'codsuc'=> $sucuse_reg
                ]
                );
            Session::flash('success', 'La Forma de Pago '.$valor['tipo'].', se registrado Exitosamente');
            return redirect('/modulo/ConfiguracionGenerales/FdePago');
        }catch(Exception $e){
            Session::flash('error', 'Se presentó un problema en el registro, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
            return redirect('/modulo/ConfiguracionGenerales/FdePago');
        }
    }
    

    /*Vista que genera el listado inicial para la carga de la informaci{on*/
    public function listado()
    {

    
        $menu = new HomeController();
        $formapago=Fatippag::orderBy('id','DESC')->where('deleted_at',NULL)->paginate(8);
        return view('/configuracionGenerales/formapago')->with('menus',$menu->retornarMenu())->with('formapago',$formapago);

    }

    /*Boton registrar que aparece en el listado*/
    public function nuevaForma()
    {
    	$menu = new HomeController();
        $accion='registro';
        return view('/configuracionGenerales/registroFormaPago')->with('menus',$menu->retornarMenu())->with('accion',$accion);
    }

    /*Función para  ingresar a la edición del archivo*/
    public function edit($id)
    {
        $menu = new HomeController();
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $formapago= Fatippag::findOrFail($ID);

        // $formapago= Fatippag::where('id',$sep[0])->where('codigo_id',$sep[1])->first();
        $accion='edicion';

        return view('/configuracionGenerales/registroFormaPago')->with('menus',$menu->retornarMenu())->with('formapago',$formapago)->with('accion',$accion);
    }

    /*Funcion que realiza la edicion de los campos*/
    public function update(Request $request, $id)
    {
        $valor=request()->all();
        $sep=explode('|', $id);
        
        $request->validate([
            'tipo'=>'required|string|min:5|max:30',
        ]);
        try{
            $formapago= Fatippag::where([['id',$sep[0]]])->first();
            $formapago->update(['destippag'=>$valor['tipo']]);
            
            Session::flash('success', 'La Forma de Pago '.$valor['tipo'].', se actualizó existosamente');
            return redirect()->route('listaFormaPago');
        }catch(Exception $e){
            Session::flash('error', 'Se presentó un problema en la Edición, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
            return redirect()->route('listaFormaPago');
        }

    }

    /*funcion de eliminar el registro*/

    public function delete($id)
    {
        $desc=Helper::desencriptar($id);
        $sep=explode('|', $desc);
        $ID=(int)$sep[1];
        $tipopago=Fatippag::findOrFail($ID);        
        
        $comprobar= Faforpag::where('fpago',$tipopago->destippag)->get();
        
        
        // dd($comprobar);
        if (!$comprobar->isEmpty())
        {
            return response()->json([
                'titulo' => 'error La forma de pago no puede ser eliminado'
            ]);
        }
        else
        {

           $tipopago->delete();
           // $formapago=Fatippag::findOrFail($ID)->update(['deleted_at'=>date('Y-m-d h:m:i')]); 

           return response()->json([
                'titulo' => 'La forma de pago se elimino'
            ]);
        }
        

    }

    /*busqueda de filtro*/
    public function filtro(Request $request)
    {
        $menu = new HomeController();
        $formapago= Fatippag::where('deleted_at',NULL)->where('destippag','ilike','%'.$request->filtro.'%')->OrWhere('id','ilike','%'.$request->filtro.'%')->paginate(8);
        
        return view('/configuracionGenerales/formapago')->with('menus',$menu->retornarMenu())->with('formapago',$formapago);
    }

}
