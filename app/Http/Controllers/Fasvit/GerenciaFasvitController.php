<?php

namespace App\Http\Controllers\Fasvit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\generadorPDF;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades;
use Carbon\Carbon;
use PDF;

use App\Models\Npinffam;
use App\Models\Nphojint;
use App\Models\Medicamento;
use App\Models\Diagnostico;
use App\Models\Soltratam;


class GerenciaFasvitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $menus = $this->retornarMenu();

        #$solicitudes = Soltratam::with('oficina', 'especialidad', 'diagnostic', 'titular')->orderBy('id','DESC')->get();

        $gerencias = DB::connection('pgsql2')->table('npestorg')->whereRaw('LENGTH(codniv) = 6')->orderBy('desniv', 'asc')->get(); #oficinas
        
        $especs = DB::connection('pgsql2')->table('fasespecialidad')->orderBy('nombreesp', 'asc')->get(); #especialidades

        return view('fasvit/gerencia_fasvit', compact('menus', 'gerencias', 'especs'));

    }

    public function retornarMenu()
    {
        $yaml = new Parser();
        $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
        return $menu['menu'];
    }

    public function filtro(Request $request)
    {
        
        $oficina = $request->oficina;
        $espec = $request->especialidad;

        $filtro = Soltratam::with('oficina', 'especialidad', 'diagnostic', 'titular')
        ->where('codespec', '=', $espec)
        ->orWhere('codniv', '=', $oficina)->get();

        return response()->json($filtro);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
