<?php

namespace App\Http\Controllers\Fasvit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Illuminate\Validation\Rule;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Mail\FasvitEmail;
use Illuminate\Support\Facades\Mail;


use App\Models\Npinffam;
use App\Models\Nphojint;
use App\Models\Npsolinffam;
use App\Models\Npsolinffamper;
use App\Models\Npstorg;
use App\Http\Controllers\generadorPDF;


class InformacionFamiliarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inicio()
    {
        return view('fasvit/inicio');
    }
    
    public function index()
    {
        
        $solicitudes = Npsolinffam::nomemp(request()->nomemp)
        ->paginate(10);
        $menus = $this->retornarMenu();
        return view('fasvit/listado_solicitudes', compact('solicitudes', 'menus'));
    }


    public function retornarMenu()
    {
        $yaml = new Parser();
        $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
        return $menu['menu'];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $tipos_parentesco = \DB::connection('pgsql2')
        ->table('nptippar')
        ->get();
        $type = 'creating';
        return view('fasvit/registro_familiares', compact('tipos_parentesco', 'type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'cedula'  => 'required:pgsql2,codemp',
            'codver' => [
                'required',
                Rule::exists('pgsql2.nphojint')->where(function($query) use ($request) {
                    return  $query->where('codemp', $request->cedula);
                })
            ],
            'emaemp' => 'required|email'
        ]);

        try{

            $empleado = Nphojint::where('codemp', $request->cedula)->first();
            if(!$empleado) return "error";
            
            \DB::beginTransaction();

            $codigo = str_pad(Npsolinffam::count()+1,8,'0',STR_PAD_LEFT);
            
            if($request->familiares && count($request->familiares) > 0){
                foreach ($request->familiares as $key => $value) {
                    $familiares[] = [
                        'codigo'    => $codigo,
                        'codemp'    => $request->cedula ,
                        'nomfam'    => strtoupper($value["nomfam"]) ,
                        'cedfam'    => $value["cedfam"] ?? null,
                        'sexfam'    => $value["sexfam"] ,
                        'fecnac'    => $value["fecnac"] ,
                        'edadfam'   => $value["edafam"] ,
                        'parfam'    => $value["parfam"] ,
                        'porcecdm'  => $value["porcecdm"] ,
                        'npinffam_id' => $value["id"] ?? null

                    ];
                }
            }

            $solinffam = Npsolinffam::create([
                'codigo'        => $codigo,
                'codemp'        => $request->cedula,
                'estatus'       => 'E',
            ]);

            Npsolinffamper::insert($familiares);
                
            \DB::commit();

            $objeto = new \stdClass;
            $objeto->nomemp = $empleado->nomemp;
            $objeto->codemp = $empleado->codemp;
            $objeto->id = $codigo;

            Mail::to([
                "mpaez@vit.gob.ve",
                "vquero@vit.gob.ve",
                "mfrodriguez@vit.gob.ve",
                "mmavo@vit.gob.ve"
                /* "jpetit@vit.gob.ve" */
            ])->send(new FasvitEmail($objeto));
            
            return response()->json([
                'url' => route('fasvit.familiares'),
                'success' => true,
                'Información suministrada correctamente'
            ]);
        }catch(\Trhowable $th){
            \DB::rollBack();
            return response()->json([
                'error' => true,
                'Error de sistema contactar con soporte'
            ]);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request)
    {
        $request->validate([
            'codigo'  => 'required:pgsql2,npsolinffam',
        ]);

        try{

            \DB::beginTransaction();
            
            if($request->familiares && count($request->familiares) > 0){
                foreach ($request->familiares as $key => $value) {

                    $asegurar = isset($value["hcm"]) && $value["hcm"] == true;
                    $created[] = Npinffam::updateOrCreate(
                        [
                            'codemp' => $value["codemp"],
                            'cedfam' => $value["cedfam"],
                            'id'     => $value["npinffam_id"] ?? 0
                        ],
                        [
                            'codemp' => $value["codemp"],
                            'cedfam' => $value["cedfam"],
                            'nomfam' => $value["nomfam"],
                            'sexfam' => $value["sexfam"],
                            'edafam' => $value["edafam"],
                            'fecnac' => $value["fecnac"],
                            'parfam' => $value["parfam"],
                            'porcecdm' => $value["porcecdm"],
                            'seghcm'  => $asegurar ? 'S' : 'N',
                            'fecing'  => \DB::select('SELECT CURRENT_DATE')[0]->date,
                            'fecinghcm' => $asegurar ? \DB::select('SELECT CURRENT_DATE')[0]->date : null
                        ]);
                    }
                }

                Npsolinffam::where('codigo', $request->codigo)
                ->update([
                    'estatus' =>  'A',
                    'fec_apro' => \DB::select('SELECT CURRENT_DATE')[0]->date,
                    'codemp_apro' =>  Auth::user()->codemp
                ]);

                /* enviar correo */
                
                \DB::commit();
                
                return response()->json([
                    'url' => route('fasvit.familiares'),
                    'success' => true,
                    'Información suministrada correctamente'
                ]);
        }catch(\Trhowable $th){
            \DB::rollBack();
            return response()->json([
                'error' => true,
                'Error de sistema contactar con soporte'
            ]);
        }

    }

    public function reject(Request $request)
    {
        $request->validate([
            'codigo'  => 'required:pgsql2,npsolinffam',
        ]);

        try{

            \DB::beginTransaction();

            Npsolinffam::where('codigo', $codigo)
            ->update([
                'estatus' =>  'R',
                'fec_anu' => \DB::select('SELECT CURRENT_DATE')[0]->date,
            ]);

            /* enviar correo */
            
            \DB::commit();
            
            return response()->json([
                'success' => true,
                'Información suministrada correctamente'
            ]);
        }catch(\Trhowable $th){
            \DB::rollBack();
            return response()->json([
                'error' => true,
                'Error de sistema contactar con soporte'
            ]);
        }
    }

    /**
     * Funcion para guardar las solicitudes de los familiares.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save_info(Request $request)
    {
        $request->validate([
            'cedula'  => 'required:pgsql2,codemp',
            'codver' => [
                'required',
                Rule::exists('pgsql2.nphojint')->where(function($query) use ($request) {
                    return  $query->where('codemp', $request->cedula);
                })
            ],
            'email' => 'required|email'
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) #sin uso
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $info = Npsolinffam::whereCodigo($id)
        ->with('familiares')
        ->with('familiares.parentesco')
        ->first();
        
        $tipos_parentesco = \DB::connection('pgsql2')
        ->table('nptippar')
        ->get();
        
        $menus = $this->retornarMenu();
        
        if(!$info){
            return redirect()->back();
        }

        $empleado = Nphojint::whereCodemp($info->codemp)->first();

        $familiares_siga = Npinffam::where('codemp', $info->codemp)
        ->get();

        $type = 'editing';

        return view('fasvit.edit_listado', compact(
            'info',
            'tipos_parentesco',
            'familiares_siga',
            'empleado',
            'menus',
            'type'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) #sin uso
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)#sin uso
    {
        //
    }

    public function getEmpleado(Request $request)
    {
        $empleado = \DB::connection('pgsql2')->table('nphojint') /* Aquí se requieren de los datos del empleado */
        ->where('codemp', $request->cedula)
        ->whereIn('staemp',['A','V'])
        ->first();

        if(!$empleado) return response()->json([
            'status' => 'empty'
        ],400);

        $familiares = Npinffam::where('codemp', $request->cedula)
        ->withTrashed()
        ->get();

        return response()->json([
            'empleado' => $empleado,
            'familiares' => $familiares
        ],200);


    }

    public function pdfFamilia(Request $request){

        $pdf = new generadorPDF();


        $info = Npsolinffam::with('familiares')
        ->with('familiares.parentesco')
        ->where('codigo', $request->codigo)
        ->first();

        $empleado = Nphojint::where('codemp',$info->codemp)->first();

        $edocivil = [
            'C' => 'Casado',
            'S' => 'Soltero',
            'D' => 'Divorciado',
            'V' => 'Viudo'
        ];

        $estado = DB::connection('pgsql2')
        ->table('npestado')
        ->where('codedo', $empleado->codest)
        ->first();

        $gerencia = substr($empleado->codniv, 0,3);
        $gerencia = Npstorg::where('codniv', $gerencia)->first();

        $pdf->AddPage('P');
        $titulo = "FORMULARIO PARA INCLUSIÓN DE TITULAR Y BENEFICIARIO EN EL FONDO AUTO-ADMINISTRADO DE SALUD";
        $fecha = date('d-m-Y');

        $pdf->SetTitle($titulo);
        $pdf->SetFont('arial','B',10);
        $pdf->SetWidths(array(90,90));
        $pdf->Ln();
        $pdf->Cell(0,18,utf8_decode($titulo),0,0,'C');
        $pdf->Ln(5);
        $pdf->SetFont('arial','B',8);
        $pdf->Ln(10);
        $pdf->SetFont('arial','B',10);
        //$pdf->Cell(0,0,utf8_decode('FORMULARIO PARA INCLUSIÓN DE TITULAR Y BENEFICIARIO EN EL FONDO AUTO-ADMINISTRADO DE SALUD'),0,0,'C'); 
        $pdf->SetFont('arial','B',12,10);
        $pdf->SetX(150);
        $fecha = $info->created_at->format('d/m/Y');
        $pdf->Cell(38,22,utf8_decode('Fecha:'.$fecha),0,0,'C');
        $pdf->Ln(14);
        $pdf->SetFillColor(150,152,154);
        $pdf->SetXY(10,69);
        $pdf->Cell(190,7,utf8_decode('DATOS DEL TITULAR'),1,1,'C');
        $pdf->Ln(0);
        $pdf->SetFont('arial','B',9,10);
        $pdf->Cell(44,7,utf8_decode('APELLIDOS Y NOMBRES'),1,0,'C');
        $pdf->Cell(125,7,utf8_decode($empleado->nomemp),0,1,'L');
        $pdf->SetXY(54,76);
        $pdf->Cell(146,7,utf8_decode(''),1,1,'C');
        $pdf->Ln(0);
        $pdf->Cell(44,7,utf8_decode('CEDULA DE IDENTIDAD'),1,1,'C');
        $pdf->Cell(44,7,utf8_decode($empleado->codemp),1,1,'C');
        $pdf->SetXY(54,83);
        $pdf->Cell(44,7,utf8_decode('LUGAR DE NACIMIENTO'),1,1,'C');
        $pdf->SetXY(98,83);
        $pdf->Cell(44,7,utf8_decode('FECHA DE NACIMIENTO'),1,1,'C');
        $pdf->SetXY(98,90);
        $pdf->Cell(44,7,utf8_decode($empleado->fecnac),1,0,'C');
        $pdf->SetXY(142,83);
        $pdf->Cell(58,7,utf8_decode('EDAD'),1,1,'C');
        $pdf->SetXY(142,90);
        $pdf->Cell(58,7,utf8_decode($empleado->edaemp),1,1,'C');
        $pdf->SetXY(10,90);
        $pdf->Cell(44,7,utf8_decode(''),1,1,'C');
        $pdf->SetXY(54,90);
        $pdf->Cell(44,7,utf8_decode(''),1,1,'C');
        $pdf->SetXY(98,90);
        $pdf->Cell(44,7,utf8_decode(''),1,1,'C');
        $pdf->SetXY(142,90);
        $pdf->Cell(58,7,utf8_decode(''),1,1,'C');
        $pdf->Ln(0);
        $pdf->SetXY(10,97);
        $pdf->Cell(44,7,utf8_decode('ESTADO CIVIL'),1,1,'C');
        $pdf->SetXY(10,104);
        $pdf->Cell(44,11,utf8_decode($edocivil[$empleado->edociv]),1,1,'C');
        $pdf->SetXY(54,97);
        $pdf->Cell(44,7,utf8_decode('SEXO'),1,1,'C');
        $pdf->SetXY(98,97);
        $pdf->Cell(102,7,utf8_decode('TELEFONOS'),1,1,'C');
        $pdf->Ln(0);
        $pdf->Cell(44,11,utf8_decode(''),1,1,'C');
        $pdf->SetXY(54,104);
        if($empleado->sexemp === 'M'){
            $pdf->MultiCell(44,11,utf8_decode('Masculino'),1);
            $pdf->SetXY(98,104);
        }else{
            $pdf->MultiCell(44,11,utf8_decode('Femenino'),1);
            $pdf->SetXY(98,104);
        }
        $pdf->MultiCell(35,11,utf8_decode('HABITACIÓN'),1,"L");
        $pdf->SetXY(133,104);
        $pdf->MultiCell(67,11,utf8_decode('CELULAR'),1,"L");
        $pdf->SetXY(10,115);
        $pdf->Cell(44,7,utf8_decode('CORREO ELECTRONICO'),1,0,'C');
        $pdf->Cell(146,7,utf8_decode($empleado->emaemp),1,1,'L');
        $pdf->SetXY(133,104);
        $pdf->MultiCell(67,11,utf8_decode('CELULAR'),1,"L");
        $pdf->SetXY(135,104);
        $pdf->Cell(58,11,utf8_decode($empleado->celemp),0,1,'C');
        $pdf->SetXY(54,115);
        $pdf->Cell(146,7,utf8_decode(''),1,1,'C');
        $pdf->SetXY(10,122);
        $pdf->MultiCell(50,11,utf8_decode('DIRECCION DE HABITACION'),1,"L");
        $pdf->SetXY(60,122);
        $pdf->Cell(140,11,utf8_decode($empleado->dirhab),1,1,'C');
        $pdf->SetXY(10,133);
        $pdf->Cell(20,8,utf8_decode('CIUDAD'),1,1,'L');
        $pdf->SetXY(30,133);
        $pdf->Cell(70,8,utf8_decode(''),1,1,'C');
        $pdf->SetXY(100,133);
        $pdf->Cell(20,8,utf8_decode('ESTADO'),1,1,'L');
        $pdf->SetXY(120,133);
        $pdf->Cell(80,8,utf8_decode($estado->nomedo ?? 'Punto Fijo'),1,1,'L');
        $pdf->SetXY(10,141);
        $pdf->Cell(42,8,utf8_decode('GERENCIA ADSCRIPCION'),1,1,'L');
        $pdf->SetXY(52,141);
        $translation = __('gerencias.'.$gerencia->desniv);
        $gerencia_text = str_contains($translation, "gerencias.") ? $gerencia->desniv : $translation;
        $pdf->Cell(82,8,utf8_decode($gerencia_text ?? ''),1,1,'L');
        $pdf->SetXY(134,141);
        $pdf->Cell(34,8,utf8_decode('FECHA DE INGRESO'),1,1,'L');
        $pdf->SetXY(168,141);
        $pdf->Cell(32,8,utf8_decode($empleado->fecing),1,1,'L');
        $pdf->SetXY(10,149);
        $pdf->Cell(190,8,utf8_decode('BENEFICIARIOS A SER INCLUIDOS EN EL FONDO AUTO-ADMINISTRADO DE SALUD'),1,1,'C');
        $pdf->SetXY(10,157);
        $pdf->Cell(48,8,utf8_decode('APELLIDOS Y NOMBRES'),1,1,'C');
        $pdf->SetXY(58,157);
        $pdf->Cell(50,8,utf8_decode('CEDULA DE IDENTIDAD'),1,1,'C');
        $pdf->SetXY(108,157);
        $pdf->Cell(42,8,utf8_decode('PARENTESCO'),1,1,'C');
        $pdf->SetXY(150,157);
        $pdf->Cell(50,8,utf8_decode('FECHA DE NACIMIENTO'),1,1,'C');
        
        $pdf->SetWidths(array(48,50,42,50));
        $pdf->SetAligns(['L','L','L','L']);
        $pdf->SetFont('arial','',8,5);
        foreach ($info->familiares ?? [] as $key => $value) {
            if($value->porcecdm){
                $pdf->Row([
                    $value->nomfam,
                    $value->cedfam,
                    optional($value->parentesco)->despar ?? '',
                    $value->fecnac
                ]);
            }
        }
        $pdf->ln(0);
        $pdf->SetFont('arial','B',9,10);
        $pdf->Cell(190,7,utf8_decode('OBSERVACIONES:'),1,1,'L');
        $pdf->SetXY(10,197);
        $pdf->Cell(190,7,utf8_decode('BENEFICIARIOS EN CASO DE MUERTE O ACCIDENTES PERSONALES'),1,1,'C');

        $pdf->SetXY(10,204);
        $pdf->Cell(48,8,utf8_decode('APELLIDOS Y NOMBRES'),1,1,'C');
        $pdf->SetXY(58,204);
        $pdf->Cell(50,8,utf8_decode('CEDULA DE IDENTIDAD'),1,1,'C');
        $pdf->SetXY(108,204);
        $pdf->Cell(42,8,utf8_decode('PARENTESCO'),1,1,'C');
        $pdf->SetXY(150,204);
        $pdf->Cell(50,8,utf8_decode('PORCENTAJE'),1,1,'C');

        $pdf->SetWidths(array(48,50,42,50));
        $pdf->SetAligns(['L','L','L','L']);
        $pdf->SetFont('arial','',8,5);
        foreach ($info->familiares ?? [] as $key => $value) {
            if($value->porcecdm){
                $pdf->Row([
                    $value->nomfam,
                    $value->cedfam,
                    optional($value->parentesco)->despar ?? '',
                    $value->porcecdm
                ]);
            }
        }
        $pdf->ln(0);
        $pdf->SetFont('arial','B',9,10);
        $pdf->Cell(190,7,utf8_decode('OBSERVACIONES:'),1,1,'L');

        $pdf->ln(10);
        $pdf->Cell(80,null,utf8_decode(''),1,1,'L');
        $pdf->Text(20,252,'FIRMA DEL TITULAR DE LA SOLICITUD');

        $pdf->SetXY(108,249);
        $pdf->Cell(90,null,utf8_decode(''),1,1,'L');
        $pdf->Text(120,252,utf8_decode('FIRMA Y SELLO DIVISIÓN DE FASVIT'));
        $pdf->ln(2);
        $pdf->SetFont('arial','',5,5);
        $pdf->Text(10,260,'SE DEBE ANEXAR A ESTE FORMULARIO: A) COPIA SIMPLE DE LOS DOCUMENTOS FILIATORIOS, B) LAS INCLUSIONES SE REALIZARAN SOLO DENTRO DE LOS PRIMEROS 3O DÍAS DEL EJERCICIO FISCAL RESPECTIVO,');
        $pdf->Text(10,264,' SALVO EN CASOS DE NACIMIENTOS O NUPCIAS.');
        $pdf->Output('I','');
       exit;
    }

    public function reporte()
    {
        $empleados = Nphojint::whereStaemp('A')
        ->whereHas('familiares')
        ->select('nomemp', 'codemp', 'staemp')
        ->orderBy('codemp', 'asc')
        ->get();

        $first_select = $empleados;
        $second_select = $empleados->sortByDesc('codemp');

        $gerencias =  Npstorg::distinct('desniv')
        ->get()->sortBy('codniv');

        $menus = $this->retornarMenu();

        return view('reportes/asegurados', compact(
            'first_select',
            'second_select',
            'gerencias',
            'menus'
        ));

    }

    public function pdf(Request $request)
    {
        #dd($request);
        $familiares = Nphojint::where('codemp','>=',$request->desde)->where('codemp', '<=', $request->hasta)
        ->where('staemp', 'A')
        ->when($request->gerencia !== 'all', function($query) use ($request){
            $condition = strlen($request->gerencia) === 3 ? ['like', '%'.$request->gerencia.'%'] : ['=',$request->gerencia];
            $query->where('codniv', $condition[0], $condition[1]);
        })
        ->with('familiares')
        ->with('familiares.parentesco')
        ->with('gerencia')
        ->with('npasicaremp')
        ->get();

        #dd($familiares);

        $grouped = $familiares->groupBy([
            'main_gerencia',
            function($item){
                return optional($item->gerencia)->desniv ?? '';
            }
        ]);

        $pdf = new generadorPDF();

        $pdf->AddPage('P');
        $titulo = "REPORTE DE BENEFICIARIOS DE FONDO AUTOADMINISTRADO DE SALUD FASVIT";
        $fecha = date('d-m-Y');

        $pdf->SetTitle($titulo);
        $pdf->SetFont('arial','B',12);
        $pdf->SetWidths(array(90,90));
        $pdf->Ln();
        $pdf->Cell(0,18,utf8_decode($titulo),0,0,'C');
        $pdf->Ln(5);
        $pdf->SetFont('arial','B',8);
        $pdf->Ln(10);

        //$fecha = $info->created_at->format('d/m/Y');
        //$pdf->Cell(38,22,utf8_decode('Fecha:'.$fecha),0,0,'C');
        foreach ($grouped as $gerencia_principal => $sub_gerencias) {
            
            $pdf->Cell(190,4,utf8_decode($gerencia_principal),1,1,'C');
            foreach ($sub_gerencias as $nombre_sub_gerencia => $empleados) {
                
                $pdf->Cell(190,4,utf8_decode($nombre_sub_gerencia),1,1,'C');
                $pdf->Cell(70,4, 'Empleado',1,0,'C');
                $pdf->Cell(30,4, 'Cedula',1,0,'C');
                $pdf->Cell(90,4, 'Cargo',1,1,'C');

                foreach ($empleados as $key => $empleado) {
                    $pdf->Cell(70,4,utf8_decode($empleado->nomemp),0,0,'C');
                    $pdf->Cell(30,4,utf8_decode($empleado->codemp),0,0,'C');
                    $pdf->Cell(90,4,utf8_decode(optional($empleado->npasicaremp->first())->nomcar ?? ''),0,1,'C');
                    
                    if(count($empleado->familiares)> 0){
                        $pdf->Cell(190,4,utf8_decode('Familiares'),1,1,'C');
                        //"cedfam"
                        //"nomfam"
                        //"parfam"
                        //"seghcm"
                        $pdf->Cell(30, 4, "Cedula", 0,0,'C');
                        $pdf->Cell(120, 4, "Nombre", 0,0,'C');
                        $pdf->Cell(30, 4, "Parentesco", 0, 0, 'C');
                        $pdf->Cell(10, 4, "Asegurado", 0, 1, 'C');
                        $pdf->SetWidths([30,90,30,10]);
                        $pdf->SetAligns(['C','C','C','C']);
                        $pdf->SetFont('arial','',8,5);
                        foreach ($empleado->familiares as $key => $familiar) {
                            /* $nombres = explode(' ',$familiar->nomfam);
                            $string= $nombres[0]." ".$nombres[1];
                            #dd($string); */
                            $pdf->Row([
                                $familiar->cedfam,
                                $familiar->nomfam,
                                $familiar->parentesco->despar,
                                $familiar->seghcm
                            ]);
                        }
                    }


                }
            }
        }
        #$pdf->header('Content-type: application/pdf');
        $pdf->Output('I','reporte.pdf');
       // exit;

    }
}
