<?php

namespace App\Http\Controllers\Fasvit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\generadorPDF;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades;
use Carbon\Carbon;




use App\Models\Npinffam;
use App\Models\Nphojint;
use App\Models\Medicamento;
use App\Models\Diagnostico;
use App\Models\Soltratam;

class TratamientoController extends Controller
{

    public function index()
    {
        
        $solicitudes = Soltratam::nomemp(request()->nomemp)->orderBy('created_at', 'asc')->paginate(10);

        $menus = $this->retornarMenu();

        return view ('/fasvit/listado_tratamiento', compact('menus', 'solicitudes'));
    }


    public function retornarMenu()
    {
        $yaml = new Parser();
        $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
        return $menu['menu'];
    }

    public function create()
    { 
        return view('/fasvit/tratamiento');

        echo($this->pats);
    }

    public function store(Request $request)
    {

        #dd($request);

        $request->validate([
            'cedula' => 'required:pgsql2,codemp',
            'cedulafam' => 'required:pgsql2,cedfam',
            'codver' => [
                'required',
                Rule::exists('pgsql2.nphojint')->where(function($query) use ($request) {
                    return  $query->where('codemp', $request->cedula);
                })
            ],
            'emaemp' => 'required|email',
            'idespec' => 'required',

            'diagnosticos.descdiag'  => 'required | min: 4 | max:100',
            'diagnosticos.fecdsdinf' => 'required',
            'diagnosticos.fechstinf' => 'required',
            'diagnosticos.fecdsdrec' => 'required',
            'diagnosticos.fechstrec' => 'required',

            'medicamentos.*.nombremed' => 'required',
            'medicamentos.*.dosis'     => 'required',
            'medicamentos.*.frecuencia'=> 'required',
            'medicamentos.*.duracion'  => 'required',
            'medicamentos.*.cantidad'  => 'required | numeric',


            ]);


        try{

            $empleado = Nphojint::where('codemp', $request->cedula)->first();
            if(!$empleado) return "error";

            $beneficiario = Npinffam::where('cedfam', $request->cedulafam)->first();
            if(!$beneficiario) return "error";

            $codigosol = str_pad(Soltratam::count()+1,8,'0',STR_PAD_LEFT);

            $codigodiag = str_pad(Diagnostico::count()+1,8,'0',STR_PAD_LEFT);

            $codigomed = str_pad(Medicamento::count()+1,8,'0',STR_PAD_LEFT);

            $aux = $empleado->codniv;

            $codniv = substr($aux, 0, 6);

            $espec = $request->idespec;


            \DB::beginTransaction();

            
            $soltratam_id = Soltratam::create([
                'codigo' => $codigosol,
                'codemp' => $request->cedula,
                'status' => 'E',
                'codespec' => $espec,
                'codniv' => $codniv,
            ]);
            
            $diagnostico = Diagnostico::create([
                
                'codfam'        => $request['cedulafam'],
                'codigo_diag'   => $codigodiag,  
                'codigo_sol'    => $codigosol,
                'diagnostico'   => $request->diagnosticos['descdiag'], 
                'fecdsdinf'     => $request->diagnosticos['fecdsdinf'],
                'fechstinf'     => $request->diagnosticos['fechstinf'],
                'fecdsdrec'     => $request->diagnosticos['fecdsdrec'],
                'fechstrec'     => $request->diagnosticos['fechstrec'],
                'observaciones' => $request->diagnosticos['observaciones'],
                'soltratam_id'  => $soltratam_id->id
            ]);
            
            
            if($request->medicamentos && count($request->medicamentos) > 0){
                foreach ($request->medicamentos as $key => $value) {
                    $medicamentos[] = [
                        "nombre"        => $value["nombremed"],
                        "codigo_med"    => $codigomed,
                        "dosis"         => $value["dosis"],
                        "frecuencia"    => $value["frecuencia"],
                        "duracion"      => $value["duracion"],
                        "cantidad"      => $value["cantidad"],
                        "diagnostico_id" => $diagnostico->id
                    ];
                }
            }

            Medicamento::insert($medicamentos);



            if(\DB::connection('pgsql2')->table('faspatologia')->where('nombre', $request->diagnosticos['descdiag'])->doesntExist())
                {   

                    \DB::connection('pgsql2')->table('faspatologia')->insert(
                [
                    'monto' => 50000,
                    'id_espec' => $request->idespec,
                    'nombre' => $request->diagnosticos['descdiag'],
                ]);

                }

            /*if () {
                # code...
            }*/

            /*\DB::connection('pgsql2')->table('faspatologia')->insert(
                [
                    'monto' => 50000,
                    'id_espec' => $request->idespec,
                    'nombre' => $request->diagnosticos['descdiag'],
                ]);
            */


            \DB::commit();

        return response()->json([
            'success' => true,
            'Información suministrada correctamente'
        ]);


        }catch(\Trhowable $th){
            \DB::rollBack();
            return response()->json([
                'error' => true,
                'Error de sistema contactar con soporte'
            ]);
        }
    }


    public function show($id)
    {

    }

    public function edit($id)
    {   

        $info = Soltratam::whereCodigo($id)->with('diagnostic')->first();

        if(!$info){
            return redirect()->back();
        }

        $empleado = Nphojint::whereCodemp($info->codemp)->first();

        $infobeneficiario = Npinffam::where('cedfam', $info->diagnostic->codfam)->first();

        $infomedicamentos = Medicamento::where('diagnostico_id', $info->diagnostic->id)->get(); 

        $type = 'editing';

        $menus = $this->retornarMenu();

        return view('/fasvit/edit_tratamiento', compact('menus', 'type', 'info', 'empleado', 'infomedicamentos', 'infobeneficiario'));
    }



    public function accion(Request $request, $id)
    {


        $solicitud = Soltratam::find($id);

        $solicitud->status = $request->status;

        $date = Carbon::now();
        

                if ($solicitud->status == 'A') {
                    
                    $solicitud->fec_apro = $date;

                }elseif ($solicitud->status == 'R') {
                    
                    $solicitud->fec_anu = $date;

                }

        $solicitud->update();

        $menus = $this->retornarMenu();

        return view('/fasvit/tratamiento', compact('menus'));

    }

    public function getPats ()
    {
        $pats = \DB::connection('pgsql2')->table('faspatologia')->get();

        $especs = \DB::connection('pgsql2')->table('fasespecialidad')->get();

        $solicitudes = Soltratam::with('oficina', 'especialidad', 'diagnostic', 'titular')->orderBy('id','DESC')->get();

        return response()->json([
            'pats' => $pats,
            'especs' => $especs,
            'solics' => $solicitudes
        ]);
    }

    public function update(Request $request, $id)
    {
        
    }


    public function destroy($id)
    {

    }

    public function solicitudPDF($id)
    {

        $info = Soltratam::whereCodigo($id)->with('diagnostic')->first();

        if(!$info){
            return redirect()->back();
        }

        $empleado = Nphojint::whereCodemp($info->codemp)->first();

        $beneficiario = Npinffam::where('cedfam', $info->diagnostic->codfam)->first();

        $medicamentos = Medicamento::where('diagnostico_id', $info->diagnostic->id)->get(); 

        $parentesco = \DB::connection('pgsql2')->table('nptippar')->where('tippar', $beneficiario->parfam)->first();

        $date = Carbon::now();

        #Inicio PDF

        $pdf = new generadorPDF();

        $pdf->AddPage('P');

        $pdf->SetFont('arial','B',12,10);

        $pdf->Ln(8);

        $pdf->Cell(190,7,utf8_decode('VENEZOLANA DE INDUSTRIA TECNOLÓGICA'),0,0,'C');
        $pdf->Ln(6);
        $pdf->Cell(190,7,utf8_decode('FORMULARIO PARA EL REGISTRO DE TRATAMIENTOS CRÓNICOS'),0,0,'C');

        $pdf->Ln(10);

        $pdf->SetFont('arial','B',10);

        $pdf->Ln(4);
        $pdf->SetFont('arial','B',12,10);
        $pdf->SetX(150);
        $pdf->Cell(38,22,utf8_decode('Fecha: '.$date->format('d/m/y')) ,0,0,'C');
        $pdf->Ln(14);
        $pdf->SetFillColor(255,255,255);
        $pdf->Cell(190,7,utf8_decode('DATOS DEL TITULAR'),1,1,'C');
        $pdf->Ln(0);
        $pdf->SetFont('arial','B',10,10);
        $pdf->Cell(44,7,utf8_decode('APELLIDOS Y NOMBRES'),1,1,'C');
        $pdf->SetFont('Arial','',10);
        $pdf->SetXY(54,76);
        $pdf->Cell(146,7,utf8_decode($empleado->nomemp),1,1,'C');
        $pdf->SetFont('arial','B',10,10);
        $pdf->Ln(0);
        $pdf->Cell(44,7,utf8_decode('FECHA DE REGISTRO'),1,1,'C');
        $pdf->SetXY(54,83);
        $pdf->Cell(44,7,utf8_decode('CEDULA DE IDENTIDAD'),1,1,'C');
        $pdf->SetXY(98,83);
        $pdf->Cell(44,7,utf8_decode('CIUDAD'),1,1,'C');
        $pdf->SetXY(142,83);
        $pdf->Cell(58,7,utf8_decode('TELÉFONO'),1,1,'C');
        $pdf->SetFont('Arial','',10);
        $pdf->SetXY(10,90);
        $pdf->Cell(44,7,utf8_decode($info->created_at->format('d/m/y')),1,1,'C');
        $pdf->SetXY(54,90);
        $pdf->Cell(44,7,utf8_decode($empleado->cedemp),1,1,'C');
        
        $pdf->SetXY(98,90);
        $pdf->Cell(44,7,utf8_decode('PUNTO FIJO'),1,1,'C');
        
        $pdf->SetXY(142,90);
        $pdf->Cell(58,7,utf8_decode($empleado->celemp),1,1,'C');
        $pdf->SetFont('arial','B',10,10);
        $pdf->Ln(0);
        $pdf->SetXY(10,97);
        $pdf->Cell(44,7,utf8_decode('CORREO ELECTRONICO'),1,1,'C');
        $pdf->SetFont('Arial','',10);
        $pdf->SetXY(54,97);
        $pdf->Cell(146,7,utf8_decode($empleado->emaemp),1,1,'C');
        $pdf->SetFont('arial','B',10,10);
        $pdf->SetXY(10,104);
        $pdf->Cell(88,7,utf8_decode('NOMBRE DEL BENEFICIARIO'),1,1,'C');
        
        $pdf->SetXY(98,104);
        $pdf->Cell(51,7,utf8_decode('CEDULA'),1,1,'C');
        $pdf->SetXY(149,104);
        $pdf->Cell(51,7,utf8_decode('PARENTESCO'),1,1,'C');
        $pdf->SetFont('Arial','',10);
        $pdf->Ln(0);
        $pdf->Cell(88,11,utf8_decode($beneficiario->nomfam),1,1,'C');
        $pdf->SetXY(54,111);
        $pdf->SetXY(98,111);
        $pdf->MultiCell(51,11,utf8_decode($beneficiario->cedfam),1,"C");
        $pdf->SetXY(149,111);
        $pdf->MultiCell(51,11,utf8_decode($parentesco->despar),1,"C");

        $pdf->SetFont('arial','B',10,10);
        
        $pdf->Cell(190,7,utf8_decode('DIAGNÓSTICO DEL PACIENTE'),1,1,'C');
        $pdf->SetFont('Arial','',10);

        $pdf->SetXY(10,129);
        $pdf->Multicell(190,10,utf8_decode($info->diagnostic->diagnostico),1,1,'C');

        $pdf->SetFont('arial','B',10,10);        


        $pdf->SetXY(10,149);
        $pdf->Cell(190,8,utf8_decode('DATOS DEL RÉCIPE E INDICACIÓN MÉDICA'),1,1,'C');

        $pdf->SetXY(10,157);
        $pdf->Cell(38,8,utf8_decode('MEDICAMENTO'),1,1,'C');

        $pdf->SetXY(48,157);
        $pdf->Cell(38,8,utf8_decode('DOSIS'),1,1,'C');
        
        $pdf->SetXY(86,157);
        $pdf->Cell(38,8,utf8_decode('FRECUENCIA'),1,1,'C');
        
        $pdf->SetXY(124,157);
        $pdf->Cell(38,8,utf8_decode('DURACION'),1,1,'C');

        $pdf->SetXY(162,157);
        $pdf->Cell(38,8,utf8_decode('CANTIDAD'),1,1,'C');

        $pdf->SetFont('Arial','',10);
        
        $pdf->SetWidths(array(38,38,38,38,38));
        $pdf->SetAligns(['C','C','C','C', 'C']);
        $pdf->SetFont('arial','',10,5);
            foreach ($medicamentos as $row) {
            
                $pdf->Row(array($row->nombre,$row->dosis,$row->frecuencia,$row->duracion,$row->cantidad));
            }

        $pdf->SetFont('arial','B',10,10);
        $pdf->SetXY(10,197);
        $pdf->Cell(190,7,utf8_decode('VIGENCIAS'),1,1,'C');

        $pdf->SetXY(10,204);
        $pdf->Cell(95,8,utf8_decode('INFORME MÉDICO'),1,1,'C');
        
        $pdf->SetXY(105,204);
        $pdf->Cell(95,8,utf8_decode('RÉCIPE E INDICACIÓN MÉDICA'),1,1,'C');

        $pdf->SetXY(10,212);
        $pdf->Cell(47,7,utf8_decode('Fecha Desde'),1,1,'C');

        $pdf->SetXY(57,212);
        $pdf->Cell(48,7,utf8_decode('Fecha Hasta'),1,1,'C');

        $pdf->SetXY(105,212);
        $pdf->Cell(47,7,utf8_decode('Fecha Desde'),1,1,'C');

        $pdf->SetXY(152,212);
        $pdf->Cell(48,7,utf8_decode('Fecha Hasta'),1,1,'C');

        $pdf->SetFont('Arial','',12);


        $pdf->SetXY(10,219);
        $pdf->Cell(47,7,utf8_decode($info->diagnostic->fecdsdinf),1,1,'C');

        $pdf->SetXY(57,219);
        $pdf->Cell(48,7,utf8_decode($info->diagnostic->fechstinf),1,1,'C');

        $pdf->SetXY(105,219);
        $pdf->Cell(47,7,utf8_decode($info->diagnostic->fecdsdrec),1,1,'C');

        $pdf->SetXY(152,219);
        $pdf->Cell(48,7,utf8_decode($info->diagnostic->fechstrec),1,1,'C');
        

        
        $pdf->ln(0);
        $pdf->SetFont('arial','',9,10);
        $pdf->Multicell(190,7,utf8_decode('OBSERVACIONES: '.$info->diagnostic->observaciones),1,1,'L');

        $pdf->ln(20);
        $pdf->SetX(50);
        $pdf->Cell(40,null,utf8_decode(''),1,1,'C');
        $pdf->Text(40,260,'FIRMA DEL TITULAR DE LA SOLICITUD');

        $pdf->SetXY(120,240);
        $pdf->Cell(15,20,utf8_decode(''),1,1,'L');
        $pdf->Text(118,265,utf8_decode('Huella Dactilar'));
        $pdf->ln(10);
        $pdf->SetFont('arial','',5,5);
        $pdf->Text(10,270,utf8_decode('SE DEBE ANEXAR A ESTE FORMULARIO: A) COPIA DE CÉDULA DEL TRABAJADOR Y PACIENTE, B) COPIA DEL INFORME MÉDICO (FECHA NO MAYOR A 6 MESES), C) COPIA DEL RÉCIPE E INDICACIONES MÉDICAS'));
        $pdf->Ln(2);
        $pdf->SetFont('arial','',8,5); 
        $pdf->Text(10,274,utf8_decode('ENTREGAR EN LA DIVISIÓN DE FASVIT'));

        $pdf->Output('I','');
    exit;
    }

}