<?php

namespace App\Http\Controllers\Fasvit;

use Symfony\Component\Yaml\Parser;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Faspatologia;
use App\Models\Fasespecialidad;


class PatologiaController extends Controller
{   
    public function retornarMenu()
    {
        $yaml = new Parser();
        $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
        return $menu['menu'];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patologias = Faspatologia::with('especialidad')->get();
        $menus = $this->retornarMenu();
        return view('fasvit.patologias', compact('menus', 'patologias'));
    } 

    public function registro(Request $request)
    {
        $menus = $this->retornarMenu();
        return view('fasvit.patologiasRegistro', compact('menus'));
    }

    public function getData(){

        $especialidades = Fasespecialidad::get();

        return response()->json([
            'especialidades' => $especialidades,
        ], 200);

    }

    public function getPat(Request $request){

        $patologias = Faspatologia::findOrFail($request->id);

        return response()->json([
            'patologias' => $patologias,
        ], 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'id_espec' => 'required:pgsql2,Faspatologia',
            'nombre' => 'required:pgsql2,Faspatologia',
        ]);

        $patologias = Faspatologia::create([
            'id_espec' => $request->id_espec,
            'nombre' => $request->nombre,
            'monto' => 0,
        ]);

        return response()->json([
            'success' => true,
            'Información suministrada correctamente',
            'redirect' => route('fasvit.patologias.index'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menus = $this->retornarMenu();
        $type = 'editing';
        $patologias = Faspatologia::findOrFail($id);

        if(!$patologias){
            return redirect()->back();
        }

        return view('fasvit.patologiasEdit', compact('patologias', 'menus', 'type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'id_espec' => 'required:pgsql2,Faspatologia',
            'nombre' => 'required:pgsql2,Faspatologia',
        ]);

        $patologias = Faspatologia::findOrFail($id);

        $patologias->id_espec = $request->id_espec;
        $patologias->nombre = $request->nombre;

        $patologias->update();

        return response()->json([
            'success' => true,
            'Información suministrada correctamente',
            'redirect' => route('fasvit.patologias.index'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patologias = Faspatologia::findOrFail($id);
        
        $patologias->delete();

        if($patologias === null){
            return response()->json([
                'titulo' => 'Hubo Error eliminando'
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Eliminado con éxito',
            'redirect' => route('fasvit.patologias.index'),
        ]);
    }
}
