<?php

namespace App\Http\Controllers;

use App\Empresa;
use Illuminate\Http\Request;
use Symfony\Component\Yaml\Parser;


class ParametrizacionController extends Controller
{

    public function retornarMenu()
    {
        $yaml = new Parser();
        $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
        return $menu['menu'];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $menu = $this->retornarMenu();

        $empresa = Empresa::first();
        
        return view('seguridad.parametrizacion')->with([
            'menus' => $menu,
            'empresa' => $empresa,
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
