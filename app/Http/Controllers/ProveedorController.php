<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Carbon\Carbon;
use DataTables;
use Exception;
use Session;
use Helper;
use App\User;
use App\Caprovee;


class ProveedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function retornarMenu()
    {
        $yaml = new Parser();
        $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
        return $menu['menu'];
    }

    public function index(Request $request)
    {
        
        $filtro = trim($request->get('filtro'));

        $proveedores = Caprovee::where('codpro', 'LIKE', '%'.$filtro.'%')
        ->orWhere('nompro', 'iLIKE', '%'.$filtro.'%')
        ->orWhere('rifpro', 'iLIKE', '%'.$filtro.'%')
        ->orWhere('email', 'iLIKE', '%'.$filtro.'%')
        ->orderBy('id', 'desc')
        ->paginate(10);

        $menu = $this->retornarMenu();

        return view('compras.proveedores.proveedores')->with(['menus' => $menu, 'proveedores' => $proveedores, 'filtro' => $filtro]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $menu = $this->retornarMenu();

        return view('compras.proveedores.provCreate')->with(['menus' => $menu]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        $sucuse_reg=Auth::user()->getsucUse()->codsuc;
        $sep= explode('-', $sucuse_reg);
        $pro=Caprovee::withTrashed()->get()->count();
        $codigoid= ($pro+1).'-'.$sep[1];

        $request->validate([
            'nitpro' => 'required',
            'nompro' => 'required|unique:pgsql2.caprovee,nompro',
            'rifpro' => 'required|unique:pgsql2.caprovee,rifpro',
            'nacpro' => 'required',
            'email' => 'required',
            'tipo' => 'required',
            'estpro' => 'required',
            'dirpro' => 'required',
            'telpro' => 'required',
        ]);

           DB::beginTransaction();

            $proveedores = Caprovee::create([

                'nitpro' => $request->nitpro,
                'nompro' => $request->nompro,
                'rifpro' => $request->rifpro,
                'nacpro' => $request->nacpro,
                'email' => $request->email,
                'tipo' => $request->tipo,
                'codpro' => preg_replace('([^A-Za-z0-9 ])', '', $request->rifpro),
                'estpro' => $request->estpro,
                'dirpro' => $request->dirpro,
                'telpro' => $request->telpro,
                'codigoid' => $codigoid,
                'fecreg' => Carbon::now(),

            ]);

            DB::commit();

            return response()->json([
                'success' => true,
                'Información suministrada correctamente',
                'redirect' => route('proveedores'),
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = $this->retornarMenu();
        $proveedor = Caprovee::find($id);

        return view('compras.proveedores.provEdit')->with([
            'proveedor' => $proveedor,
            'menus' => $menu,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

            $request->validate([
                'nitpro' => 'required',
                'nompro' => 'required|unique:pgsql2.caprovee,nompro',
                'rifpro' => 'required|unique:pgsql2.caprovee,rifpro',
                'nacpro' => 'required',
                'email'  => 'required',
                'tipo'   => 'required',
                'estpro' => 'required',
                'dirpro' => 'required',
                'telpro' => 'required',
            ]);
            
            $proveedor = Caprovee::find($id);
            
            $proveedor->nitpro = $request['nitpro'];
            $proveedor->nompro = $request['nompro'];
            $proveedor->rifpro = $request['rifpro'];
            $proveedor->nacpro = $request['nacpro'];
            $proveedor->email  = $request['email'];
            $proveedor->tipo   = $request['tipo'];
            $proveedor->estpro = $request['estpro'];
            $proveedor->dirpro = $request['dirpro'];
            $proveedor->telpro = $request['telpro'];
    
            $proveedor->save();

            Session::flash('success', 'Registro editado con exito.');
            return redirect()->route('proveedores');

    
        
        
        
        #return redirect()->route('proveedores')->with('message', 'Editado Exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $proveedor = Caprovee::findOrFail($id);
        
        if($proveedor === null){
            return response()->json([
                'titulo' => 'Error eliminando'
            ]);
        }

        $proveedor->delete();

        Session::flash('success', 'Registro eliminado con exito.');
        
        return response()->json([
            'success' => true,
            'message' => 'Información suministrada correctamente',
            'redirect' => route('proveedores'),
        ]);
    }
}
