<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Illuminate\Support\Facades\Auth;
use Helper;
use Exception;
use Session;
use Carbon\Carbon;
use \Fpdf;
use App\Http\Controllers\generadorPDF;
use App\Sucursal;
use App\Fafactur;
use App\Fanotcre;
use App\Famoneda;
use App\Fargoart;
use App\Fatasacamb;
use App\Models\Caja;
use App\Segususuc;

class LibroVentasController extends Controller
{
 public function __construct()
 {
 $this->middleware('auth');
 
 
 }

 public function index(){
 $menu = new HomeController();

 if(Auth::user()->role->nombre == 'ADMIN')$sucursales = Sucursal::get();
 else $sucursales = Sucursal::where('codsuc',Auth::user()->userSegSuc->codsuc)->get();

 
 $monedas = Famoneda::get();
 return view('/reportes/libroVentas')->with('menus',$menu->retornarMenu())->with('sucursales',$sucursales)->with('monedas',$monedas);
 }

/*
 public function index2(){
 $menu = new HomeController();
 if(Auth::user()->role->nombre == 'ADMIN')$sucursales = Sucursal::get();
 else $sucursales = Sucursal::where('codsuc',Auth::user()->userSegSuc->codsuc)->get();
 $monedas = Famoneda::get();
 return view('/reportes/libroVentas2')->with('menus',$menu->retornarMenu())->with('sucursales',$sucursales)->with('monedas',$monedas);
 }
*/
 public function pasoFechAnul(&$fechaAnul,$fechaNota)
 {
 $fechaAnul = new Carbon($fechaNota);
 
 }

 public function generarLibro(Request $request){
 $moneda = $request['moneda'];
 /*$status = $request['status'];*/
 $status = $request['status'];
 $desde = $request['desde'];
 $hasta = $request['hasta'];
 $diames = $request['diames'];
 switch ($diames) {
 case 'diario':
 $tipo = 'DIARIO '.$desde;
 break;
 case 'ayer':
 $tipo = 'DÍA ANTERIOR '.$desde;
 break;
 case 'semanal':
 $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
 break;
 case 'semant':
 $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
 break;
 case 'mensual':
 $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
 break;
 case 'mesant':
 $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
 break;
 case 'anual':
 $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
 break;
 case 'antyear':
 $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
 break;
 default:
 $fc=explode(' - ', $desde);
 list($y,$m,$d) = explode("-",$fc[0]);
 $desde1=$d.'-'.$m.'-'.$y;
 $fch=explode(' - ', $hasta);
 list($y,$m,$d) = explode("-",$fch[0]);
 $hasta1=$d.'-'.$m.'-'.$y;
 $tipo = 'INTERVALO ENTRE '.$desde1.' HASTA '.$hasta1;
 break;

 }

 try{

 $turno = null;
 if(isset($request->all()['turno']))
 $turno = $request->all()['turno'];
 


 if(!$request->sucursal)throw new Exception("Debe seleccionar una sucursal");
 if($request->sucursal =="TODAS"){
 $general = true;
 }
 else{
 $general = false;
 $codsuc = $request->sucursal;
 }
 if(!$general && !$request['caja']){
 throw new Exception("Debe seleccionar una caja");
 }
 else { $caja = $request['caja']; }

 }
 catch(Exception $e){
 return redirect()->route('libroVentas')->with('error',$e->getMessage());
 } 
 $nomsucu = Sucursal::where('codsuc','=',$codsuc)->first()->nomsucu;
 $dirfis = Sucursal::where('codsuc','=',$codsuc)->first()->dirfis;
 // dd($codsuc);
 ///EVALUA LOS PERIODOS DE TIEMPO SELECCIONADOS Y SI SERA UNA CONSULTA GENERALIZADA O POR SUCURSAL
 try{
   $cajas = Caja::wheredescaj($caja)->first();
 if(!$general){
 
   $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero',
   'notaCredito2')
   
   ->date2($desde,$hasta)
   
   ->where('codsuc','=',$codsuc)->orderby('reffac')
   ->where('codcaj','=',$cajas->id)
   ->where('is_fiscal',$cajas->is_fiscal)
   ->when(isset($request->all()['turno']) === true,function ($q) use ($turno){
   switch ($turno) {
   case 'vespertino':
      $q->where('turnos',strtoupper($turno));
   break;
   case 'matutino':
      $q->where('turnos',strtoupper($turno));
   break;
  
   case 'completo':
      $q->where('turnos',strtoupper($turno));
   break;
   
   case 'general': 
   
   return ;
   break;
   }
   })
   ->get();
 
//  elseif($status == 'activa'){
//  $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero')
//  ->date($desde,$hasta)
//  ->where('status','=','A')->get();
//  }
//  else{
//  $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero')
//  ->date($desde,$hasta)
//  ->where('status','=','N')->get();
//  }
 }
 else{
 
 
 if($status == 'todas'){
 

 $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero',
 'notaCredito2')
 
 ->date2($desde,$hasta)
 
 ->where('codsuc','=',$codsuc)->orderby('reffac')
 ->where('codcaj','=',$cajas->id)
 ->where('is_fiscal',$cajas->is_fiscal)
 ->when(isset($request->all()['turno']) === true,function ($q) use ($turno){
 switch ($turno) {
 case 'vespertino':
    $q->where('turnos',strtoupper($turno));
 break;
 case 'matutino':
    $q->where('turnos',strtoupper($turno));
 break;

 case 'completo':
    $q->where('turnos',strtoupper($turno));
 break;
 
 case 'general': 
 
 return ;
 break;
 }
 })
 ->get();

 //dd($facturas,$turno,$cajas->id,$cajas->is_fiscal,$codsuc);
 
 }
 else if($status == 'activa'){
 $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero')
 ->date2($desde,$hasta)
 ->where('codsuc','=',$codsuc)
 ->where('caja','=',$caja)
 ->where('status','=','A')->get();
 }
 else{
 $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero')
 ->date2($desde,$hasta)
 ->where('codsuc','=',$codsuc)
 ->where('caja','=',$caja)->where('status','=','N')->get();
 }


 }
 /* }*/
 if($cajas->is_fiscal === "true"){
 $notasCredito = Fanotcre::with('perteneceFactura.cliente')
 ->where('fecnot','>=',$desde)->where('fecnot','<=',$hasta)
 ->get();
 $notasCredito = $notasCredito->filter(function ($q)
 {
 return $q->perteneceFactura !== null;
 });
 }
 

 if($facturas->isEmpty()) throw new Exception("No se ha facturado aùn");
 
 
 }//try
 catch(Exception $e){
 Session::flash('error', 'Se presentó un problema en la generación del reporte, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
 return redirect()->route('libroVentas');
 }
 

 $pdf = new generadorPDF();

 $titulo = 'LIBRO DE VENTAS';
 $total = $facturas->count();
 $fecha = date('d-m-Y');


 $pdf->AddPage('L');
 $pdf->SetTitle('LIBRO DE VENTAS');
 $pdf->SetFont('arial','B',16);
 $pdf->SetWidths(array(90,90));
 $pdf->Ln(2);
 $pdf->Cell(0,10,utf8_decode($titulo),0,0,'C');
 $pdf->Ln(4);
 $pdf->SetFont('arial','B',10);
 $pdf->Ln(7);
 $pdf->Cell(280,5,utf8_decode('TIENDA: '.$nomsucu),0,0,'C');
 $pdf->Ln(5);
 $pdf->Cell(280,5,utf8_decode('CAJA: '.$cajas->descaj),0,0,'C');
 $pdf->Ln(5);
 $pdf->Cell(280,5,utf8_decode('SUCURSAL: '.$dirfis),0,0,'C');
 $pdf->Ln(8);
 if($tipo == 'Intervalo'){
 $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo.' '.$desde.' - '.$hasta),0,0,'C');
 }
 else{
 $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo),0,0,'C');
 }
 $pdf->Ln(3);
 $first = $facturas->first()->reffac;
 $last = $facturas->last()->reffac;
 
 $pdf->Cell(280,5,utf8_decode('PRIMERA FACTURA: '.$first),0,1,'C');
 $pdf->Cell(280,5,utf8_decode('ULTIMA FACTURA: '.$last),0,1,'C');
 
 if($turno !== null)
 $pdf->Cell(280,5,utf8_decode('TURNO: '. strtoupper($turno)),0,1,'C');

 $pdf->Ln(10);
 $pdf->SetFont('arial','B',7,5);
 $pdf->SetFillColor(2,157,116);
 $pdf->SetWidths(array(10,20,20,45,10,15,15,15,17,17,17,16,16,17,17));
 
 $pdf->SetAligns(array('C','C','C','C','C','C','C','C','C','C','C','C','C','C','C'));
 $pdf->Row(array(utf8_decode('No'),
 utf8_decode('Fecha'),
 utf8_decode('RIF'),
 utf8_decode('Nombre o Razon Social'),
 utf8_decode('Tipo'),
 utf8_decode('Documento'),
 utf8_decode('No Control'),
 utf8_decode('Documento Afectado'),
 utf8_decode('Total Incluye IVA'),
 utf8_decode('Total Exento'),
 utf8_decode('Base Imponible'),
 utf8_decode('Descuentos'),
 utf8_decode('%'),
 utf8_decode('Impuesto'),
 utf8_decode('Monto Retenido')));
 
 $pdf->SetWidths(array(10,20,20,45,10,15,15,15,17,17,17,16,16,17,17));
 $pdf->SetAligns(['C','C','C','J','C','C','R','R','R','R','R','R','R','R','R','R']);
 $pdf->SetFont('arial','',7,5);
 
 
 
 $facturas = $facturas->union(isset($notasCredito) ? $notasCredito : '')->sortBy('reffac');
 //dd($facturas);
 
 if($facturas->isEmpty()){
 Session::flash('error', 'No existen registros con los parametros de busqueda ingresados');
 return redirect()->route('libroVentas');
 }
 $i = 1;
 $estatusFactura = '';
 $totalG = collect();
 $totalBaseI = 0;
 $totalI = 0;
 $totalIva = 0;
 $totalExento = 0;
 $totalDescuento = 0;
 foreach ($facturas as $factura) {
 $baseimp=0;
 $exento=0;
 if($factura !== null){
 
 if($factura->monrecargo!=0)
 {
 foreach ($factura->articulos as $art)
 {
 if($art->monrgo!=0)
 $baseimp+=$art->cantot*$art->precio;
 else
 $exento += $art->totart;

 
 }
 }
 else
 {
 $baseimp = $factura->monsubtotal;
 $exento =(intval($factura->monrecargo) == 0) ? $factura->monsubtotal:0;
 }

 

 if($factura->status === 'N'){//facturas NC Y ANULADAS
 
 if($factura->fecfac === $factura->fecanu){
 $pdf->SetTextColor(0,0,0); 

 $pdf->Row(array(
 /*numero*/$i,
 /*Fecha*/$factura->created_at->format('d/m/Y'),
 /*RIF*/$factura->cliente->codpro,
 /*Razon Social*/$factura->cliente->nompro,
 /*Tipo*/utf8_decode('FACC'),
 /*Documento*/utf8_decode($factura->reffac),
 /*N control*/utf8_decode($factura->impserial),
 /*Documento Afectado*/'',
 /*Total I/Iva*/ number_format(($factura->monfac),2,',','.') ,
 // /*Exento*/utf8_decode(number_format($factura->monrecargo==0 ? $factura->monsubtotal:0,2,',','.')),
 /*Exento*/utf8_decode(number_format( $exento,2,',','.')),
 ///*Base Imponible*/utf8_decode(number_format((intval($factura->monrecargo) !== 0) ? ($factura->monsubtotal) : 0,2,',','.')),
 /*Base Imponible*/utf8_decode(number_format(($factura->monrecargo !== "0") ? ($baseimp):0,2,',','.')),
 /*Descuento*/utf8_decode(number_format($factura->mondesc,2,',','.')),
 /*%*/$factura->porrecargo,
 /*Impuesto*/number_format($factura->monrecargo,2,',','.'),
 /*monto retenido*/'0,00'));
 $pdf->SetTextColor(255,0,0); 
 $i++;
 $totalG->push(intval($factura->mondesc) === 0 ? ($factura->monsubtotal + $factura->monrecargo) : ($factura->monsubtotal - $factura->mondesc) + $factura->monrecargo);
 $totalBaseI += ($factura->monrecargo !== "0") ? ($baseimp):0 ;
 //$totalBaseI += (intval($factura->monrecargo) !== 0) ? ($factura->monsubtotal) : 0;
 $totalI += $factura->monfac;
 $totalIva += $factura->monrecargo;
 $totalDescuento += $factura->mondesc;
 //$totalExento += $factura->monrecargo == 0 ? $factura->monsubtotal:0;
 $totalExento += $exento;
 


 if($factura->notaCredito2 === null) $estatusFactura = 'ANUL';
 elseif ($factura->notaCredito2 !== null) $estatusFactura = 'NC';
 
 $pdf->Row(array(
 /*numero*/$i,
 /*fecha*/$factura->created_at->format('d/m/Y'),
 /*rif*/$factura->cliente->codpro,
 /*razon social*/$factura->cliente->nompro,
 /*tipo*/utf8_decode($estatusFactura),
 /*documento*/utf8_decode($factura->refanu ?? '-'),
 /*control*/utf8_decode($factura->impserial),
 /*doc. afectado*/utf8_decode($factura->reffac),
 /*Total I/Iva*/ number_format(($factura->monfac*-1),2,',','.') ,
 /*exento*/utf8_decode(number_format($exento*-1,2,',','.')),
 ///*exento*/utf8_decode($factura->monrecargo == 0 ? number_format($factura->monsubtotal*-1,2,',','.'):'0,00'),
 /*base impo*/utf8_decode(number_format( ($factura->monrecargo !== "0") ? ($baseimp) :0,2,',','.')),
 /*Descuento*/utf8_decode(number_format($factura->mondesc,2,',','.')),
 ///*base impo*/utf8_decode(number_format((intval($factura->monrecargo) !== 0) ? ($factura->monsubtotal) : 0,2,',','.')),
 /*%*/$factura->porrecargo,
 /*immpuesto*/utf8_decode(number_format($factura->monrecargo*-1,2,',','.')),
 /*retenido*/'0,00'));
 $i++;
 $totalG->push(intval($factura->mondesc) === 0 ? ($factura->monsubtotal + $factura->monrecargo)*-1 : (($factura->monsubtotal - $factura->mondesc) + $factura->monrecargo)*-1); 
 $totalBaseI += ($factura->monrecargo !== "0") ?($baseimp)*-1:0;
 //$totalBaseI += (intval($factura->monrecargo) !== 0) ? ($factura->monsubtotal)*-1 : 0;
 $totalI += $factura->monfac*-1;
 $totalIva += $factura->monrecargo*-1;
 $totalDescuento += $factura->mondesc*-1;
 //$totalExento += $factura->monrecargo == 0 ?$factura->monsubtotal*-1 : 0;
 $totalExento += $exento*-1 ;
 //dd($totalG,$factura->monsubtotal , $factura->monrecargo);
 }else{
 if($factura->fecfac !== $factura->fecanu && $desde === $hasta){//en caso de se halla devuelto la factura y consulta un dia especifico
 if ($desde >= $factura->fecanu) {//cuando se consulta la factura que sea mayor o igual del dia
 $pdf->SetTextColor(255,0,0); 

 if($factura->notaCredito2 === null) $estatusFactura = 'ANUL';
 elseif ($factura->notaCredito2 !== null) $estatusFactura = 'NC';

 $pdf->Row(array(
 /*numero*/$i,
 /*fecha*/$factura->created_at->format('d/m/Y'),
 /*rif*/$factura->cliente->codpro,
 /*razon social*/$factura->cliente->nompro,
 /*tipo*/utf8_decode($estatusFactura),
 /*doc. afectado*/utf8_decode($factura->refanu ?? '-'),
 /*control*/utf8_decode($factura->impserial),utf8_decode($factura->reffac),
 /*Total I/Iva*/ number_format(($factura->monfac*-1),2,',','.') ,
 /*exento*/utf8_decode(number_format($exento*-1,2,',','.')),
 ///*exento*/utf8_decode( $factura->monrecargo == 0 ?number_format($factura->monsubtotal*-1,2,',','.'):'0,00'),
 /*base imp*/utf8_decode(number_format(($factura->monrecargo !== "0") ? ($baseimp)*-1:0,2,',','.')),
 /*Descuento*/utf8_decode(number_format($factura->mondesc,2,',','.')),
 ///*base imp*/utf8_decode(number_format((intval($factura->porrecargo) !== 0) ? ($factura->monsubtotal)*-1 : 0,2,',','.')),
 /*%*/$factura->porrecargo,
 /*impuesto*/utf8_decode(number_format($factura->monrecargo*-1,2,',','.')),
 /*retenido*/'0,00'));
 $i++;
 $totalG->push(intval($factura->mondesc) === 0 ? ($factura->monsubtotal + $factura->monrecargo) * -1 : (($factura->monsubtotal - $factura->mondesc) + $factura->monrecargo) * -1); 
 $totalBaseI += ($factura->monrecargo !== "0") ?($baseimp)*-1 :0;
 //$totalBaseI += (intval($factura->monrecargo) !== 0) ? ($factura->monsubtotal)*-1 : 0;
 $totalI += $factura->monfac*-1;
 $totalIva += $factura->monrecargo*-1;
 $totalExento += $exento*-1 ;
 $totalDescuento += $factura->mondesc*-1;
 #dd($totalExento);
 }else if($desde <= $factura->fecfac){
 $pdf->SetTextColor(0,0,0); 
 
 $pdf->Row(array(
 /*numero*/$i,
 /*fecha*/$factura->created_at->format('d/m/Y'),
 /*rif*/$factura->cliente->codpro,
 /*razon social*/$factura->cliente->nompro,
 /*tipo*/utf8_decode('FACC'),
 /*doc. afectado*/utf8_decode($factura->reffac),
 /*control*/utf8_decode($factura->impserial),'-',
 /*Total I/Iva*/ number_format(($factura->monfac),2,',','.') ,
 ///*exento*/utf8_decode($factura->monrecargo == 0 ? number_format($factura->monsubtotal,2,',','.'):'0,00'),
 /*exento*/utf8_decode(number_format($exento,2,',','.')),
 ///*base imp*/utf8_decode(number_format((intval($factura->monrecargo) !== 0) ? ($factura->monsubtotal ) : 0,2,',','.')),
 /*base imp*/utf8_decode(number_format( ($factura->monrecargo !== "0") ?($baseimp ) :0,2,',','.')),
 /*Descuento*/utf8_decode(number_format($factura->mondesc,2,',','.')),
 /*%*/$factura->porrecargo,
 /*impuesto*/number_format($factura->monrecargo,2,',','.'),
 /*retenido*/'0,00'));
 $i++;
 $totalG->push(intval($factura->mondesc) === 0 ? ($factura->monsubtotal + $factura->monrecargo) : ($factura->monsubtotal - $factura->mondesc) + $factura->monrecargo); 
 //$totalBaseI += (intval($factura->monrecargo) !== 0) ? ($factura->monsubtotal) : 0; 
 $totalBaseI +=($factura->monrecargo !== "0") ? ($baseimp) :0; 
 $totalI += $factura->monfac;
 $totalIva += $factura->monrecargo;
 //$totalExento += $factura->monrecargo==0 ? $factura->monsubtotal : 0;
 $totalExento += $exento;
 $totalDescuento += $factura->mondesc;
 }
 
 $i++;
 }else if($factura->fecfac !== $factura->fecanu && $desde !== $hasta){//en caso de se halla devuelto la factura y consulta en dias diferentes
 #dd($factura->reffac,$factura->fecfac,$factura->fecanu);
 
 if ($desde <= $factura->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
 $pdf->SetTextColor(0,0,0); 
 
 $pdf->Row(array(
 /*numero*/$i,
 /*fecha*/$factura->created_at->format('d/m/Y'),
 /*rif*/$factura->cliente->codpro,
 /*razon social*/$factura->cliente->nompro,
 /*tipo*/utf8_decode('FACC'),
 /*documento*/utf8_decode($factura->reffac),
 /*control*/utf8_decode($factura->impserial),
 /*doc. afectado*/'-',
 /*Total I/Iva*/ number_format(($factura->monfac),2,',','.') ,
 ///*exento*/utf8_decode($factura->monrecargo == 0? number_format($factura->monsubtotal,2,',','.'):'0,00'),
 /*exento*/utf8_decode(number_format($exento,2,',','.')),
 ///*base imp*/utf8_decode(number_format((intval($factura->monrecargo) !== 0) ? ($factura->monsubtotal) : 0,2,',','.')),
 /*base imp*/utf8_decode(number_format(($factura->monrecargo !== "0") ? ($baseimp) :0,2,',','.')),
 /*Descuento*/utf8_decode(number_format($factura->mondesc,2,',','.')),
 /*%*/$factura->porrecargo,
 /*impuesto*/number_format($factura->monrecargo,2,',','.'),
 /*retenido*/'0,00'));
 
 $totalG->push(intval($factura->mondesc) === 0 ? ($factura->monsubtotal + $factura->monrecargo) : (($factura->monsubtotal - $factura->mondesc) + $factura->monrecargo)); 
 //$totalBaseI += (intval($factura->monrecargo) !== 0) ? ($factura->monsubtotal ) : 0;
 $totalBaseI += ($factura->monrecargo !== "0") ?($baseimp ) :0;
 $totalI += $factura->monfac;
 $totalIva += $factura->monrecargo;
 //$totalExento += $factura->monrecargo==0 ? $factura->monsubtotal:0;
 $totalExento += $exento;
 $totalDescuento += $factura->mondesc;
 $i++;
 if($factura->fecanu >= $desde && $factura->fecanu <= $hasta){

 $pdf->SetTextColor(255,0,0); 
 if($factura->notaCredito2 === null) $estatusFactura = 'ANUL';
 elseif ($factura->notaCredito2 !== null) $estatusFactura = 'NC';



 $pdf->Row(array(
 /*numero*/$i,
 /*fecha*/$factura->created_at->format('d/m/Y'),
 /*rif*/$factura->cliente->codpro,
 /*razon social*/$factura->cliente->nompro,
 /*tipo*/utf8_decode($estatusFactura),
 /*doc*/utf8_decode($factura->refanu ?? '-'),
 /*control*/utf8_decode($factura->impserial),
 /*doc afectado*/utf8_decode($factura->reffac),
 /*Total I/Iva*/ number_format(($factura->monfac*-1),2,',','.') ,
 /*exento*/utf8_decode( number_format($exento*-1,2,',','.')),
 ///*exento*/utf8_decode($factura->monrecargo == 0 ? number_format($factura->monsubtotal*-1,2,',','.'):'0,00'),
 /*base imp*/utf8_decode(number_format((intval($factura->monrecargo) !== 0) ? ($baseimp)*-1:0,2,',','.')),
 /*Descuento*/utf8_decode(number_format($factura->mondesc,2,',','.')),
 ///*base imp*/utf8_decode(number_format((intval($factura->monrecargo) !== 0) ? ($factura->monsubtotal + $factura->monrecargo)*-1 : 0,2,',','.')),
 /*%*/$factura->porrecargo,
 /*impuesto*/utf8_decode(number_format($factura->monrecargo*-1,2,',','.')),
 /*retenido*/'0,00'));
 $i++;

 $totalG->push(intval($factura->mondesc) === 0 ? ($factura->monsubtotal + $factura->monrecargo)*-1 : (($factura->monsubtotal - $factura->mondesc) + $factura->monrecargo)*-1); 
 //$totalBaseI += (intval($factura->monrecargo) !== 0) ? ($factura->monsubtotal)*-1 : 0;
 $totalBaseI += ($factura->monrecargo !== "0") ?($baseimp)*-1:0 ;
 $totalIva += $factura->monrecargo*-1;
 $totalI += $factura->monfac*-1;
 //$totalExento += $factura->monrecargo ==0 ? $factura->monsubtotal*-1 :0;
 $totalExento += $exento*-1 ;
 $totalDescuento += $factura->mondesc*-1;
 }

 }else if($desde <= $factura->fecfac){
 $pdf->SetTextColor(0,0,0); 
 
 $pdf->Row(array(
 /*numero*/$i,
 /*fecha*/$factura->created_at->format('d/m/Y'),
 /*rif*/$factura->cliente->codpro,
 /*razon social*/$factura->cliente->nompro,
 /*tipo*/utf8_decode('FACC'),
 /*documento*/utf8_decode($factura->reffac),
 /*control*/utf8_decode($factura->impserial),'-',
 /*Total I/Iva*/ number_format(($factura->monfac),2,',','.') ,
 /*exento*/utf8_decode( number_format($exento,2,',','.')),
 ///*exento*/utf8_decode($factura->monrecargo==0 ? number_format($factura->monsubtotal,2,',','.'):'0,00'),
 /*base imp*/utf8_decode(number_format( ($factura->monrecargo !== "0") ? $baseimp:0 ,2,',','.')),
 /*Descuento*/utf8_decode(number_format($factura->mondesc,2,',','.')),
 ///*base imp*/utf8_decode(number_format((intval($factura->monrecargo) !== 0) ? $factura->monsubtotal : 0,2,',','.')),
 /*%*/$factura->porrecargo,
 /*impuesto*/number_format($factura->monrecargo,2,',','.'),
 /*retenido*/'0,00'));
 $totalG->push(intval($factura->mondesc) === 0 ? ($factura->monsubtotal + $factura->monrecargo) : ($factura->monsubtotal - $factura->mondesc) + $factura->monrecargo); 
 $totalBaseI += ($factura->monrecargo !== "0") ? $baseimp:0;
 //$totalBaseI += (intval($factura->monrecargo) !== 0) ? $factura->monsubtotal : 0;
 $totalI += $factura->monfac;
 $totalIva += $factura->monrecargo;
 //$totalExento += $factura->monrecargo?$factura->monsubtotal:0;
 $totalExento += $exento;
 $totalDescuento += $factura->mondesc;
 
 $i++;
 }else if($factura->fecfac <= $factura->fecanu){
 
 
 $pdf->SetTextColor(255,0,0); 
 
 if($factura->notaCredito2 === null) $estatusFactura = 'ANUL';
 elseif ($factura->notaCredito2 !== null) $estatusFactura = 'NC';

 $pdf->Row(array(
 /*numero*/$i,
 /*fecha*/$factura->created_at->format('d/m/Y'),
 /*rif*/$factura->cliente->codpro,
 /*razon social*/$factura->cliente->nompro,
 /*tipo*/utf8_decode($estatusFactura),
 /*doc afectado*/utf8_decode($factura->refanu ?? '-'),
 /*control*/utf8_decode($factura->impserial),
 /*documento*/utf8_decode($factura->reffac),
 /*Total I/Iva*/ number_format(($factura->monfac),2,',','.') ,
 /*exento*/utf8_decode(number_format($exento*-1,2,',','.')),
 ///*exento*/utf8_decode($factura->monrecargo==0 ? number_format($factura->monsubtotal*-1,2,',','.'):'0,00'),
 /*base imp*/utf8_decode(number_format( ($factura->monrecargo !== "0") ?($baseimp )*-1:0 ,2,',','.')),
 /*Descuento*/utf8_decode(number_format($factura->mondesc,2,',','.')),
 ///*base imp*/utf8_decode(number_format((intval($factura->monrecargo) !== 0) ? ($factura->monsubtotal )*-1 : 0,2,',','.')),
 /*%*/$factura->porrecargo,
 /*impuesto*/utf8_decode(number_format($factura->monrecargo*-1,2,',','.')),
 /*retenido*/'0,00'));
 $i++;
 $totalG->push(intval($factura->mondesc) === 0 ? ($factura->monsubtotal + $factura->monrecargo) *-1: ($factura->monsubtotal - $factura->mondesc) + $factura->monrecargo * -1); 
 $totalBaseI += ($factura->monrecargo !== "0") ? ($baseimp )*-1 :0;
 //$totalBaseI += (intval($factura->monrecargo) !== 0) ? ($factura->monsubtotal )*-1 : 0;
 $totalIva += $factura->monrecargo*-1;
 $totalI += $factura->monfac*-1;
 //$totalExento += $factura->monrecarog? $factura->monsubtotal*-1 : 0;
 $totalExento += $exento*-1 ;
 $totalDescuento += $factura->mondesc*-1;
 } 
 }

 }

 }else{//FACTURA ACTIVAS
 $pdf->SetTextColor(0,0,0); 
 // dd($facturas);
 $pdf->Row(array(
 /*Numero*/$i,
 /*Fecha*/$factura->created_at->format('d/m/Y'),
 /*RIF*/$factura->cliente->codpro,
 /*Razon social*/$factura->cliente->nompro,
 /*Tipo*/utf8_decode('FACC'),
 /*Documento*/utf8_decode($factura->reffac),
 /*No Comtrol*/utf8_decode($factura->impserial),
 /*Doc Afectado*/'-',
 /*Total I/Iva*/ number_format(($factura->monfac),2,',','.') ,
 ///*Total Exento*/(intval($factura->monrecargo) == 0) ? utf8_decode(number_format($factura->monsubtotal,2,',','.')):'0,00',
 /*Total Exento*/utf8_decode(number_format($exento,2,',','.')),
 /*Base Imponible*/utf8_decode(number_format(($factura->monrecargo !== "0") ? $baseimp : 0,2,',','.')),
 /*Descuento*/utf8_decode(number_format($factura->mondesc,2,',','.')),
 ///*Base Imponible*/utf8_decode(number_format((intval($factura->monrecargo) !== 0) ? $factura->monsubtotal : 0,2,',','.')),
 /*%*/$factura->porrecargo,
 /*Impuesto*/number_format($factura->monrecargo,2,',','.'),
 /*Monto Retenido*/'0,00'));
 $totalG->push(intval($factura->mondesc) === 0 ? ($factura->monsubtotal + $factura->monrecargo) : ($factura->monsubtotal - $factura->mondesc) + $factura->monrecargo); 
 $totalBaseI += ($factura->monrecargo !== "0") ? $baseimp : 0;
 //$totalBaseI += (intval($factura->monrecargo) !== 0) ? $factura->monsubtotal : 0;
 $totalI += (intval($factura->mondesc) === 0) ? $factura->monsubtotal + $factura->monrecargo : ($factura->monsubtotal - $factura->mondesc) + $factura->monrecargo;;
 $totalIva += $factura->monrecargo;
 $totalExento += $exento;
 $totalDescuento += $factura->mondesc;
 //$totalExento += $factura->monrecargo == 0 ? $factura->monsubtotal: 0;
 $i++;
 
 }
 }
 

 }
 
 
 
 $pdf->SetTextColor(0,0,0);
 // $pdf->Ln();
 $pdf->SetFont('arial','B',7);
 $pdf->SetFillColor(2,157,116,120);
 $pdf->SetTextColor(0,0,0);
 $pdf->Cell(143,5,utf8_decode('TOTAL GENERAL Bs'),0,0,'R'); 
 //if(intval(collect($facturas)->last()->porrecargo) !== 0)
 $pdf->Cell(25,5,number_format($totalG->sum(),2,',','.'),0,0,'R');
 // else $pdf->Cell(20,5,number_format(0,2,',','.'),0,0,'R');
 $pdf->Cell(7,5,'',0,0,'R');
 $pdf->Cell(10,5,number_format($totalExento,2,',','.'),0,0,'R');
 $pdf->Cell(7,5,'',0,0,'R');
 $pdf->Cell(10,5,number_format($totalBaseI,2,',','.'),0,0,'R');
 $pdf->Cell(28,5,'',0,0,'R');
 
 $pdf->Cell(-12,5,number_format($totalDescuento,2,',','.'),0,0,'R');
 //if(intval(collect($facturas)->last()->porrecargo) !== 0)
 $pdf->Cell(33,5,number_format($totalIva,2,',','.'),0,0,'R');
 //else 
 //$pdf->Cell(20,5,number_format(0,2,',','.'),0,0,'R');

 $pdf->SetWidths(array(10,20,20,45,10,20,20,20,20,20,20,10,20,20));
 $pdf->SetAligns(['C','C','C','J','C','C','R','R','R','R','R','R','R']);
 
 
 $pdf->AddPage('L');
 $pdf->SetTitle('LIBRO DE VENTAS');
 $pdf->SetFont('arial','B',16);
 $pdf->SetWidths(array(90,90));
 $pdf->Ln();
 $pdf->Cell(0,10,utf8_decode($titulo),0,0,'C');
 $pdf->Ln();
 $pdf->SetFont('arial','B',10);
 
 $pdf->Cell(280,5,utf8_decode('TIENDA: '.$nomsucu),0,0,'C');
 $pdf->Ln();
 $pdf->Cell(280,5,utf8_decode('CAJA: '.$cajas->descaj),0,0,'C');
 $pdf->Ln(5);
 $pdf->Cell(280,5,utf8_decode('SUCURSAL: '.$dirfis),0,0,'C');
 $pdf->Ln(8);
 if($tipo == 'Intervalo'){
 $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo.' '.$desde.' - '.$hasta),0,0,'C');
 }
 else{
 $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo),0,0,'C');
 }
 $pdf->ln(10);

 $pdf->Cell(120,5,utf8_decode(' '),0,0,'C');
 $pdf->Cell(40,5,utf8_decode('Base Imponible '),0,0,'C');
 $pdf->Cell(40,5,utf8_decode('Débito Fiscal '),0,0,'C');
 $pdf->Cell(40,5,utf8_decode('Impuesto Retenido '),0,0,'C');
 $pdf->ln();

 $pdf->Cell(40,10,utf8_decode(' '),0,0,'R');
 $pdf->Cell(80,10,utf8_decode(' Total Ventas Internas No Gravadas'),0,0,'R');
 # $pdf->Cell(40,10,number_format($exen,2,',','.'),0,0,'R');
 $pdf->Cell(40,10,number_format($totalExento,2,',','.'),0,0,'R');
 $pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 $pdf->ln(); 
 $pdf->Cell(40,10,utf8_decode(' '),0,0,'R');
 $pdf->Cell(80,10,utf8_decode(' Total Descuento Ventas Internas No Gravadas'),0,0,'R');
 $pdf->Cell(40,10,number_format($totalDescuento,2,',','.'),0,0,'R');
 
 $pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 $pdf->ln(); 

 $pdf->Cell(40,10,utf8_decode(' '),0,0,'R');
 $pdf->Cell(80,10,utf8_decode(' Total Ventas de Exportación'),0,0,'R');
 $pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 $pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 $pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 $pdf->ln(); 

 $pdf->Cell(40,10,utf8_decode(' '),0,0,'R');
 $pdf->Cell(80,10,utf8_decode(' Total Ventas Internas solo alicuota general 16 %'),0,0,'R');
 
 // if(intval(collect($facturas)->last()->porrecargo) !== 0)
 $pdf->Cell(40,10,number_format($totalBaseI,2,',','.'),0,0,'R');
 //else 
 //$pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 // if(intval(collect($facturas)->last()->porrecargo) !== 0)
 $pdf->Cell(40,10,number_format($totalIva,2,',','.'),0,0,'R');
 // else 
 // $pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 #$pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 #$pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');

 $pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 $pdf->ln(); 

 $pdf->Cell(40,10,utf8_decode(' '),0,0,'R');
 $pdf->Cell(80,10,utf8_decode(' Total Ventas Internas solo alicuota reducida 9 %'),0,0,'R');
 $pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 $pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 $pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 $pdf->ln(); 

 $pdf->Cell(40,10,utf8_decode(' '),0,0,'R');
 $pdf->Cell(80,10,utf8_decode(' Total Ventas Internas solo alicuota general 7 %'),0,0,'R');
 $pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 $pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 $pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 $pdf->ln(); 

 $pdf->Cell(40,10,utf8_decode(' '),0,0,'R');
 $pdf->Cell(80,10,utf8_decode(' Total Ventas Internas solo alicuota general otra'),0,0,'R');
 $pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 $pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 $pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 $pdf->ln(); 

 $pdf->Cell(40,10,utf8_decode(' '),0,0,'R');
 $pdf->Cell(80,10,utf8_decode(' Sub- Totales'),0,0,'R');
 $pdf->Cell(40,10,number_format(($totalExento-$totalDescuento)+$totalBaseI,2,',','.'),0,0,'R');
 //if(intval(collect($facturas)->last()->porrecargo) !== 0)
 $pdf->Cell(40,10,number_format($totalIva,2,',','.'),0,0,'R');
 //else 
 // $pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 #$pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');

 $pdf->Cell(40,10,number_format(0,2,',','.'),0,0,'R');
 $pdf->ln(); 

 $pdf->Cell(40,10,utf8_decode(' '),0,0,'R');
 $pdf->Cell(80,10,utf8_decode(' Total Incluyendo alicuotas'),0,0,'R');

 // if(intval(collect($facturas)->last()->porrecargo) !== 0)
 $pdf->Cell(40,10,number_format(($totalExento+$totalIva+$totalBaseI)-$totalDescuento,2,',','.'),0,0,'R');
 // else $pdf->Cell(40,10,number_format($totalBaseI,2,',','.'),0,0,'R');

 $pdf->Cell(40,10,'*',0,0,'R');
 $pdf->ln(); 
 $pdf->Output('I','LIBRO DE VENTAS-'.$fecha.'pdf');


 
 exit;
 

 }

 public function generarLibro2(Request $request){
 $moneda = $request['moneda'];
 /*$status = $request['status'];*/
 $status = $request['status'];
 $desde = $request['desde'];
 $hasta = $request['hasta'];
 $diames = $request['diames'];
 switch ($diames) {
 case 'diario':
 $tipo = 'DIARIO';
 break;
 case 'ayer':
 $tipo = 'DÍA ANTERIOR';
 break;
 case 'semanal':
 $tipo = "SEMANAL";
 break;
 case 'semant':
 $tipo = "SEMANA ANTERIOR";
 break;
 case 'mensual':
 $tipo = 'MENSUAL';
 break;
 case 'mesant':
 $tipo = 'MES ANTERIOR';
 break;
 case 'anual':
 $tipo = 'ANUAL';
 break;
 case 'antyear':
 $tipo = 'AÑO ANTERIOR';
 break;
 default:
 $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
 break;

 }


 try{
 if(!$request->sucursal)throw new Exception("Debe seleccionar una sucursal");
 if($request->sucursal =="TODAS"){
 $general = true;
 }
 else{
 $general = false;
 $codsuc = $request->sucursal;
 }
 if(!$general && !$request['caja']){
 throw new Exception("Debe seleccionar una caja");
 }
 else { $caja = $request['caja']; }

 }
 catch(Exception $e){
 return redirect()->route('libroVentas')->with('error',$e->getMessage());
 }
 
 ///EVALUA LOS PERIODOS DE TIEMPO SELECCIONADOS Y SI SERA UNA CONSULTA GENERALIZADA O POR SUCURSAL
 try{
 if($general){
 if($status == 'todas'){
 $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero')
 ->date($desde,$hasta) 
 ->get();
 }
 elseif($status == 'activa'){
 $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero')
 ->date($desde,$hasta)
 ->where('status','=','A')->get();
 }
 else{
 $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero')
 ->date($desde,$hasta)
 ->where('status','=','N')->get();
 }
 }
 else{
 if($status == 'todas'){
 $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero')
 ->date($desde,$hasta)
 ->where('codsuc','=',$codsuc)
 ->where('caja','=',$caja)->get();
 }
 elseif($status == 'activa'){
 $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero')
 ->date($desde,$hasta)
 ->where('codsuc','=',$codsuc)
 ->where('caja','=',$caja)
 ->where('status','=','A')->get();
 }
 else{


 $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero')
 ->date($desde,$hasta)
 ->where('codsuc','=',$codsuc)
 ->where('caja','=',$caja)->where('status','=','N')->get();
 }
 }
 if($facturas->isEmpty()){
 Session::flash('error', 'No existen registros con los parametros de busqueda ingresados');
 return redirect()->route('libroVentas');}
 }//try
 catch(Exception $e){
 Session::flash('error', 'Se presentó un problema en la generación del reporte, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
 return redirect()->route('libroVentas');
 }
 $pdf = new generadorPDF();

 $titulo = 'LIBRO DE VENTAS';
 $total = $facturas->count();
 $fecha = date('d-m-Y');

 $pdf->AddPage('L');
 $pdf->SetTitle('LIBRO DE VENTAS');
 $pdf->SetFont('arial','B',16);
 $pdf->SetWidths(array(90,90));
 $pdf->Ln();
 $pdf->Cell(0,22,utf8_decode($titulo),0,0,'C');
 $pdf->Ln();
 $pdf->SetFont('arial','B',10);
 if($tipo == 'Intervalo'){
 $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo.' '.Carbon::parse($desde)->format('d/m/Y').' - '.Carbon::parse($hasta)->format('d/m/Y')),0,0,'L');
 }
 else{
 $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo),0,0,'L');
 }
 $pdf->Ln(5);
 $pdf->Cell(0,0,utf8_decode('FECHA: '.$fecha),0,0,'L');
 $pdf->SetFont('arial','B',7,5);
 $pdf->SetFillColor(2,157,116);

 $MontoTotal = 0;

 $groupBy = $facturas->groupBy(function($q){
 return __(Carbon::parse($q->created_at)->format('D d m y'),
 ['date' => Carbon::parse($q->created_at)->format('d')]);
 });

 $notCredito = $groupBy->map(function($q){
 return $q->where('status','N')->pluck('pagos')->flatten();
 });

 $pagosAll = $groupBy->map(function($q){
 return $q->where('status','A')->pluck('pagos')->flatten();
 });

 $totalVentasDia = $groupBy->map(function($q){
 return $q->groupBy('reffac')->flatten()->where('status','A')->count();
 });

 $totalNotaCreditoDia = $groupBy->map(function($q){
 return $q->groupBy('reffac')->flatten()->where('status','N')->count();
 });

 $ingresoFromTasa = $pagosAll->map(function($q){
 return $q->groupBy('tasaCod')->map(function($q){
 return $q->sum('monpag');
 });
 });

 $devolucionesFromTasa = $notCredito->map(function($q){
 return $q->groupBy('tasaCod')->map(function($q){
 return $q->sum('monpag');
 });
 });

 //dd($ingresoFromTasa,$devolucionesFromTasa);

 /* 
 REVISAR TODAS LAS VARIABLES ANTERIORMENTE ASIGNADAS TODAS ESTAN AGRUPADAS A BASE DE LA
 AGRUPACION POR DIA
 */
 $pdf->SetTextColor(0,0,0);
 $pdf->Ln(5);
 $pdf->SetFont('arial','B',10,5);
 $pdf->SetFillColor(2,157,116);
 $pdf->SetTextColor(0,0,0);

 $pdf->Cell(20,5,utf8_decode('FECHA'),1,0,'C');
 $pdf->Cell(30,5,utf8_decode('TOTAL VENTAS'),1,0,'C');
 $pdf->Cell(30,5,utf8_decode('TASA'),1,0,'C');
 $pdf->Cell(40,5,utf8_decode('INGRESOS BS.S'),1,0,'C');
 $pdf->Cell(40,5,utf8_decode('INGRESOS $'),1,0,'C');
 $pdf->Cell(40,5,utf8_decode('AVG BOLIVARES'),1,0,'C');
 $pdf->Cell(30,5,utf8_decode('AVG $'),1,1,'C');

 $totalBs = 0;
 $totalDolar = 0;
 $avgBsTotal = 0;
 $avgDollaTotal = 0;

 foreach($ingresoFromTasa as $date => $total){
 $pdf->SetFont('arial','',10,5);
 //$pdf->Cell(30,8,utf8_decode(''),1,1,'C');
 $fecha = explode(' ',$date)[0];
 $pdf->Cell(20,5,utf8_decode(
 __('dates.'.$fecha,['day' => explode(' ',$date)[1]])
 ),1,0,'C');
 //dd($totalVentasDia);
 foreach($total as $tasa => $monto){
 /* SI EXISTEN 2 TASAS HAY QUE HACER LA VARIACION */
 $montoTasa = Fatasacamb::where('codigoid',$tasa)->first()->valor;
 $dolares = $ingresoFromTasa[$date][$tasa] / $montoTasa;
 $bolivares = $ingresoFromTasa[$date][$tasa];
 $avgBs = $ingresoFromTasa[$date][$tasa] / $totalVentasDia[$date];
 $avgDolla = $dolares / $totalVentasDia[$date];

 $pdf->Cell(30,5, utf8_decode($totalVentasDia[$date]),1,0,'C');
 $pdf->Cell(30,5, number_format($montoTasa,2,',','.'),1,0,'C');
 $pdf->Cell(40,5, number_format( $ingresoFromTasa[$date][$tasa] ,2,',','.') ,1,0,'C');
 $pdf->Cell(40,5, number_format( $dolares ,2,',','.') ,1,0,'C');
 $pdf->Cell(40,5, number_format( $avgBs ,2,',','.') ,1,0,'C');
 $pdf->Cell(30,5, number_format( $avgDolla ,2,',','.') ,1,1,'C');

 $totalBs += $bolivares;
 $totalDolar += $dolares;
 $avgBsTotal += $avgBs;
 $avgDollaTotal += $avgDolla;
 }
 
 }
 $pdf->Cell(30,5,"",0,1,'C');
 
 $pdf->Cell(120,5, " RESUMEN " ,1,1,'C');
 $pdf->Cell(60,6,'TOTAL BOLIVARES ',1,0,'C');
 $pdf->Cell(60,6,round($totalBs,2),1,1,'C');
 $pdf->Cell(60,6,'TOTAL DOLARES ',1,0,'C');
 $pdf->Cell(60,6,round($totalDolar,2),1,1,'C');
 $pdf->Cell(60,6,'TOTAL PROMEDIO ',1,0,'C');
 $pdf->Cell(60,6,round($avgBsTotal / count($ingresoFromTasa),2),1,1,'C');
 $pdf->Cell(60,6,'TOTAL PROMEDIO DOLARES ',1,0,'C');
 $pdf->Cell(60,6,round($avgDollaTotal / count($ingresoFromTasa),2),1,1,'C');

 //$pdf->Cell(0,0,utf8_decode('MONTO TOTAL: '.number_format($MontoTotal,3,',','.').' '.$monedacod->nomenclatura),0,0,'R');
 $pdf->Output('I','LIBRO DE VENTAS-'.$fecha.'pdf');
 
 exit;

 }

 public function buscarFecha(Request $request){
 $fecha = $request['fecha'];

 if($fecha == 'diario'){
 $desde = Carbon::today()->toDateString();
 $hasta = Carbon::today()->toDateString();
 }

 elseif($fecha == 'ayer'){
 $desde = Carbon::yesterday()->toDateString();
 $hasta = Carbon::yesterday()->toDateString();
 }

 elseif($fecha == 'semanal'){
 $desde = Carbon::now()->startOfWeek()->toDateString();
 $hasta = Carbon::now()->endOfWeek()->toDateString();
 }

 elseif($fecha == 'semant'){
 $previous_week = strtotime("-1 week +1 day");
 $start_week = strtotime("last sunday midnight",$previous_week);
 $end_week = strtotime("next saturday",$start_week);
 $desde = date("Y-m-d",$start_week);
 $hasta = date("Y-m-d",$end_week);
 }

 elseif($fecha == 'mensual'){
 $desde = Carbon::today()->startOfMonth();
 $hasta= $desde->copy()->endOfMonth()->toDateString();
 $desde = $desde->toDateString();
 }

 elseif($fecha == 'mesant'){
 $desde = Carbon::now()->startOfMonth()->subMonth()->toDateString();
 $hasta = Carbon::now()->subMonth()->endOfMonth()->toDateString();
 }

 elseif($fecha == 'anual'){
 $date = Carbon::now();

 $desde = $date->copy()->startOfYear()->toDateString();
 $hasta = $date->copy()->endOfYear()->subDay()->addDay()->toDateString();
 }
 else{
 $date = Carbon::now();
 $desde = $date->copy()->subYears(1)->startOfYear()->toDateString();
 $hasta = $date->copy()->subYears(1)->endOfYear()->subDay()->addDay()->toDateString();
 }
 
 $fechas = [];
 array_push($fechas, $desde);
 array_push($fechas, $hasta);
 return response()->json($fechas);
 }

 public function buscarCajas(Request $request){
 
 $sucursal = $request['sucursal'];
 if($sucursal != 'TODAS' ){
 $cajas = Caja::where('codsuc','=',$sucursal)->get();
 return response()->json($cajas);
 }
 
 }


 
}