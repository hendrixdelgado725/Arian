<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\Farecarg;
use App\Fargoart;
use App\User;
use Helper;
use Exception;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
use App\permission_user;
use App\Segususuc;

class RegistroRecargoController extends Controller
{
    //
    private $guard;
    public function __construct(Guard $guard)
    {
        $this->middleware('auth');
        $this->guard = $guard;
    }

    /*Función que ejecuta la inserción de los archivos en la base de datos*/
    public  function create(Request $request)
    {
        $valor=request()->all();
        $sucuse_reg=Auth::user()->getsucUse()->codsuc;
        $sep=explode('-', $sucuse_reg);

        $request->validate([
            'descripcion'=>'required|string|min:5|max:100',
            'valor'=> 'required|numeric|unique:pgsql2.farecarg,monrgo'
        ]);

        try{
            $rgo=Farecarg::withTrashed()->get()->count();
            $codigoid=($rgo+1).'-'.$sep[1];

            $ultCodrgo=Farecarg::withTrashed()->Orderbydesc('codrgo')->first();
            $codrgo= ($ultCodrgo ? str_pad($ultCodrgo->codrgo+1, 4, '0', STR_PAD_LEFT) : '0001');
            
            $verificar=Farecarg::where([['defecto','S'],['deleted_at',NULL]])->first();

            if($verificar)
            {
                if($valor['xdefect']=='S')
                {
                    $verificar->defecto='N';
                    $verificar->save();
                }
            }
            else
            {
                if($valor['xdefect']=='N')
                    $valor['xdefect']='S';
            }

            Farecarg::create(
                [
                    'codrgo' => $codrgo,
                    'nomrgo' => $valor['descripcion'],
                    'monrgo' => $valor['valor'],
                    'status' => $valor['defecto'],
                    'defecto' => $valor['xdefect'],
                    'tiprgo' => 'P',
                    'codigoid' => $codigoid,
                    'codsuc_reg' => $sucuse_reg,
                    'codsuc' => $sucuse_reg
                ]
            );
            Session::flash('success', 'El recargo '.$valor['descripcion'].', se registro Exitosamente');
            return redirect()->route('listaRecargo');

        }catch(Exception $e){
            Session::flash('error', 'Se presentó un problema en el registro, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
            return redirect()->route('listaRecargo');
        }
        
    }

    /*Función para  ingresar a la edición del archivo*/
    public function edit($id)
    {
        $menu = new HomeController();
        $desc=Helper::desencriptar($id);
        $sep=explode('-', $desc);
        $ID=(int)$sep[1];
        
        $recargo= Farecarg::findOrFail($ID);
        // $recargo = Farecarg::where('id',$sep[0])->where('codigo_id',$sep[1])->first();
        $accion='edicion';

        return view('/configuracionGenerales/registroEdicionRecargo')->with('menus',$menu->retornarMenu())->with('recargo',$recargo)->with('accion',$accion);
    }

    /*Funcion que realiza la edicion de los campos*/
    public function update(Request $request, $id)
    {
        $valor=request()->all();
        $sep=explode('|', $id);
        // dd($sep);
        $request->validate([
            'descripcion'=>'required|string|min:5|max:100',
            'valor'=> 'required|numeric|unique:pgsql2.farecarg,monrgo'
        ]); 
        try{
            $recargo=Farecarg::findOrFail($sep[0]);

            if($recargo->defecto!=$valor['xdefect'])
            {
                if($valor['xdefect']=='S')
                {
                    $verificar=Farecarg::VerificarDefecto();
                    if($verificar)
                    {
                        $verificar->defecto='N';
                        $verificar->save();
                    }
                }
            }        
            // $actualizar=Farecarg::findOrFail($id)->update([
            $actualizar=Farecarg::where([['id',$sep[0]]])->update([
                'nomrgo'=>$valor['descripcion'],
                'monrgo'=>$valor['valor'],
                'status'=>$valor['defecto'],
                'defecto'=>$valor['xdefect']
            ]);

            Session::flash('success', 'El recargo '.$valor['descripcion'].', se actualizó Exitosamente');
            return redirect()->route('listaRecargo');

        }catch(Exception $e){
            Session::flash('error', 'Se presentó un problema en la Edición, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
            return redirect()->route('listaRecargo');
        }
        
    }

    /*funcion de eliminar el registro*/
    public function delete($id)
    {
        
        $desc=Helper::desencriptar($id);
        $sep=explode('|', $desc);
        $ID=(int)$sep[1];
        $codig=(string)$sep[2];
        $verifica=Farecarg::findOrFail($ID);

        $comprobar=Fargoart::where('codrgo',$verifica->codrgo)->first();

        if($comprobar)
        { 
            return response()->json([
                'titulo' => 'error el recargo no puede ser eliminado'
            ]);
        }
        else
        {
            if($verifica->defecto=='S')
            {
                $enct=Farecarg::ultimoRegistro();
                if($enct)
                {
                    $enct->status='S';
                    $enct->save();
                }

            }
            $verifica->delete();
            // $recargo=Farecarg::findOrFail($ID)->update(['deleted_at'=>date('Y-m-d h:m:i')]);
            return response()->json([
                    'titulo' => 'El recargo se elimino'
                ]);
            }
    }

    /*Vista que genera el listado inicial para la carga de la informaci{on*/
    public function listado()
    {

        $menu = new HomeController();
        
        $recargo=Farecarg::orderBy('id','DESC')->where('deleted_at',NULL)->paginate(8);
        return view('/configuracionGenerales/recargo')->with('menus',$menu->retornarMenu())->with('recargo',$recargo);

    }

    /*Boton registrar que aparece en el listado*/
    public function nuevoRecargo()
    {
        $menu = new HomeController();
        $accion='registro';
        return view('/configuracionGenerales/registroEdicionRecargo')->with('menus',$menu->retornarMenu())->with('accion',$accion);
    }

    /*busqueda de filtro*/
    public function filtro(Request $request)
    {
        $menu = new HomeController();

        $recargo= Farecarg::where('deleted_at',NULL)->where('nomrgo','ilike','%'.$request->filtro.'%')
        ->OrWhere('nomrgo','ilike','%'.$request->filtro.'%')->OrWhere('id','ilike','%'.$request->filtro.'%')->paginate(8);


        return view('/configuracionGenerales/recargo')->with('menus',$menu->retornarMenu())->with('recargo',$recargo);
    }

}
