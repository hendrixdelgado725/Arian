<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Mail\RequisicionMail; 
use App\Mail\ReqPresupuestoMail;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Session;
use Helper;
use App\User;
use App\Nphojint;
use App\Models\Casolart;
use App\Models\Caartsol;
use App\Models\Npcatpre;
use App\Models\Nppartidas;
use App\Models\Cpprecom;
use App\Models\Cpimpprc;
use App\Models\Cpasiini;
use App\Cpdeftit;

class PrecomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function retornarMenu()
    {
        $yaml = new Parser();
        $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
        return $menu['menu'];
    }

    public function index()
    {
        //
        $precoms = Cpprecom::with('articles.cpdeftit')->orderBy('id', 'DESC')->paginate(8);
        #dd($precoms); where('staprc', 'E')->
        $menu = $this->retornarMenu(); 

        return view('compras.precompromisos.precom')->with([
            'precoms' => $precoms, 
            'menus' => $menu
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function genPrecom($reqart)
    {
        #dd($reqart);
        $date = Carbon::now()->format('Y-m-d');
        $anio = Carbon::now()->format('Y');
        $req = Casolart::where('reqart', $reqart)->with('articulos.partida', 'unidad', 'solicitante', 'ccosto', 'fin', 'moneda')->first();
        
        try {
            //code...
            DB::beginTransaction();

            $precompromiso = Cpprecom::create([
                'refprc' => $req->reqart,
                'tipprc' => 'SAE',
                'fecprc' => $date,
                'anoprc' => $anio,
                'desprc' => $req->desreq,
                'monprc' => $req->monreq,
                'salcom' => 0.00,
                'salcau' => 0.00,
                'salpag' => 0.00,
                'salaju' => 0.00,
                'staprc' => 'E',
                'cedrif' => 'J-00000000-0',
                #'fecreg'
            ]);

            if($req->articulos && count($req->articulos) > 0){
                foreach($req->articulos as $key => $value){
                    $articulos[] = [
                        'refprc' => $req->reqart,
                        'codpre' => $value['codcat'].'-'.$value['codpar'],
                        'monimp' => $value['montot'],
                        'moncom' => 0.00,
                        'moncau' => 0.00,
                        'monpag' => 0.00,
                        'monaju' => 0.00,
                        'staimp' => 'E',
                    ];
                }
            }

            Cpimpprc::insert($articulos);


        DB::commit();

        #Llamar a función de correo para gerente presupuesto
        /*
        ***
        * CORREO DE GERENTE DE PRESUPUESTO
        * Prieto Rojas, Carolina = cprieto@vit.gob.ve
        ***
        */

            
        Session::flash('success', 'Precompromiso de la Requisición '.$req->reqart.' enviado con éxito con éxito.');
        return redirect()->route('listaReq');

        } catch (\Throwable $th) {
            //throw $th;
            throw $th;
            DB::rollback();
            
            Session::flash('error', 'El precompromiso para la requisición '.$req->reqart.' no pudo ser creado, consulte con los administradores del sistema');
            return redirect()->route('listaReq');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($refprc)
    {
        //
        $precom = Cpprecom::where('refprc', $refprc)->with('articles.cpdeftit', 'articles.asignado')->first();
        $menu = $this->retornarMenu(); 

        return view('compras.precompromisos.precomDetalles')->with([
            'precom' => $precom, 
            'menus' => $menu
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
