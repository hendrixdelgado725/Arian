<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use App\rolesusuarios;
use Helper;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Segususuc;

class RolesController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware('auth');
    }

	public function index()
	{
		$menu = new HomeController();
		$roles = rolesusuarios::paginate(8);
		return view('configuracionGenerales.roles.listasRoles')->with('menus',$menu->retornarMenu())->with('roles',$roles)->with('sec',Segususuc::where('codusuario',Auth::user()->codigoid)->where('activo','TRUE')->first()->getSucursal->nomsucu);
	}

	public function deleteRol($id)
	{
		$desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID = $sep[1].'-'.$sep[2];
		#dd($sep);
		
		$delete = \App\rolesusuarios::where('codigoid',$ID)->delete();
		return response()->json([
			'titulo' => 'SE HA ELIMINADO CON EXITO'

		]);
	}

	public function createRol()
	{
		 $menu = new HomeController();

		 return view('configuracionGenerales.roles.crearRoles')->with('menus',$menu->retornarMenu());
	}

	public function createRoles(Request $request)
	{	
		$request->validate([

			'codigos' => 'required',
			'nomCargo'=> 'required'
		]);
		$menu = new HomeController();
		$roles = rolesusuarios::paginate(8);
		
		
		$search = rolesusuarios::where('codroles',$request->codigos)->first();
		
		if (is_null($search) === false) {
		Session::flash('error', 'Este rol se encuentra registrado');
		
		return redirect()->route('rolesIndex')->with('menus',$menu->retornarMenu())->with('roles',$roles);
			
		}else{


			$sucuse_reg=Auth::user()->getsucUse()->codsuc;
            $guion='-';
            $cont = rolesusuarios::withTrashed()->get()->count();
            $cont = $cont+1;
            $suc = Auth::user()->codsuc_reg;
            $codsuc_reg = explode('-',$suc);
            $cod = $cont.$guion.$codsuc_reg[1];

			rolesusuarios::create([
				'codroles' => $request->codigos,
				'nombre' => $request->nomCargo,
				'codsuc_reg' => $suc,
				'codigoid' => $cod
			]);

			Session::flash('success', 'Se ha registrado con exito');
		
			return redirect()->route('rolesIndex')->with('menus',$menu->retornarMenu())->with('roles',$roles);

		}


		
	}

}
