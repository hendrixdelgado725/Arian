<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Exception;
use Session;
use Helper;
use App\User;
use App\Bancos;
use App\Almacen;
use App\cajaUser;
use App\Caregart;
use App\Famoneda;
use App\Fatippag;
use App\Farecarg;
use App\Fadescto;
use App\Facliente;
use App\Fatasacamb;
use App\Caartalmubi;
use App\Models\Caja;
use App\Models\Serial;
use Illuminate\Contracts\Auth\Guard;
use App\Models\serialDisponible;
use App\Models\Fanotaentrega;
use App\Models\Fanotaentregaart;
use App\Http\Requests\ClienteRequest;
use App\Http\Controllers\generadorPDF;
use App\Http\Requests\FanotaentregaRequest;

class FaNotaEntregaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Guard $auth){

        $this->auth = $auth;
        $this->middleware('auth');

}
    public function sucursal()
    {
        $suc = Auth::user()->userSegSuc->codsuc;
        return $suc;
    }

    public function retornarMenu()
    {
        $yaml = new Parser();
        $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
        return $menu['menu'];
    }

    public function index()
    {
        //
        $notas = Fanotaentrega::with('cliente')->orderBy('fecnota', 'desc')->paginate(8);
        $menu = $this->retornarMenu();

        return view('facturacion.FaNotaEntrega.FNEindex')->with([
            'menus' => $menu,
            'notas' => $notas,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        try {
            $userCaja = cajaUser::where('loguse', $this->auth->user()->loguse)->get();
            if(count($userCaja) == 0){
                Session::flash('error','No tiene caja asociada');
                return redirect()->route('FNEIndex');
            }
            $ip = request()->ip() === '::1' ? '127.0.0.1' : request()->ip();
            $caja1 = Caja::where('impfishost',$ip)->where('codsuc', session('codsuc'))->first();
            if($caja1 === null) throw new Exception("No tienes una caja asignada asociala a la ip de la maquina que va a facturar", 1);
            $menu = $this->retornarMenu();
            $user = Auth::user();
            $sucursal = Auth::user()->userSegSuc->codsuc;
            $tasa = Fatasacamb::with('moneda', 'moneda2')->whereHas('moneda', function ($q) {
                $q->where('nombre', '=', 'DOLAR')->orWhere('nombre', '=', 'DOLLAR');
            })->where('activo', '=', true)->first();
            $alm = Almacen::where('codsuc_asoc', '=', $sucursal)->select('codalm')->where('pfactur', true)->first();
            $articulos = Caregart::with('inventario', 'faartpvp')->has('costos')
            ->where('tipreg','F')
            //->where('staart','A')
            ->whereHas('almacenubi',function($q) use ($alm){
            //  $q->where('exiact','>',0)->whereIn('codalm',$alm);
              $q->where('exiact','>',0)->where('codalm',$alm->codalm);
            })
            ->get();
            $recargos = Farecarg::where('status', '=', 'S')
                ->where('defecto', '=', 'S')
                ->where('deleted_at', '=', null)
                ->whereHas('sucursalRec', function ($q) use ($sucursal) {
                    $q->where('codsuc', $sucursal)->where('deleted_at', null);
                })
                ->get();
            $descuentos = Fadescto::get();
            $monedas = Famoneda::where('activo', true)->get();
            $bancos = Bancos::get();
            $cajaU = cajaUser::whereNull('deleted_at')->with('caja')->where('loguse',Auth::user()->loguse)->get();
    
            return view('facturacion.FaNotaEntrega.FNEcreate')->with([
                'menus' => $menu,
                'user' => $user,
                'tasa' => $tasa,
                'articulos' => $articulos,
                'recargos' => $recargos,
                'descuentos' => $descuentos,
                'monedas' => $monedas,
                'bancos' => $bancos,
                'cajas' => $cajaU
            ]);    
        } catch (Exception $e) {
            //throw $th;
            Session::flash('error',$e->getMessage());
            return redirect('/modulo/Facturacion/NotaEntrega');
        }
        
    }

    public function getData()
    {
        $clientes = Facliente::get();
        $fpagos = Fatippag::get();

        return response()->json([
            'clientes' => $clientes,
            'fpagos' => $fpagos,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FanotaentregaRequest $request)
    {
        DB::beginTransaction();
        try {
            $codnota = $this->getNumero();
            $date = Carbon::now()->format('Y-m-d');
            $suc = Auth::user()->userSegSuc->codsuc;
            $fanotaentrega = Fanotaentrega::create([
                'codnota' => $codnota,
                'fecnota' => $date,
                'status' => 'A',
                'codcli' => $request->codcli,
                'monto' => floatval($request->totalNota),
                'montasa' => floatval($request->tasaNota),
                'tippag' => $request->forpag,
                'tipmon' => $request->tipmon, 
                'refer' => $request->refer,
                'banco_id' => $request->banco,
                'subtotal' => floatval($request->subtotalNota),
                'monrecargo' => floatval($request->ivaNota),
                'mondes' => floatval($request->descuentoNota),
                'usunot' => Auth::user()->loguse,
                'observ' => '',
                'codcaj' => $request->caja,
                'codsuc' => Auth::user()->userSegSuc->codsuc,
                'tasa' => $request->tasa,
            ]);
        foreach($request->arts as $key => $value){
            $articulos[] = [
                'codnota' => $codnota,
                'codart' => $value["codart"],
                'cantart' => $value["cantart"],
                'subtotal' => $value["subtotal"],
                'mondes' => $value["mondes"],
                'totrgo' => $value["totrgo"],
                'totart' => $value["totart"],
                'descto' => $value["descto"],
                'monrgo' => $value["monrgo"],
                'montasa' => $value["montasa"],
                'codalm' => $value["codalm"],
                'isserial' => $value["isserial"] ?? false,
            ];
            if(!empty($value["seriales"])){
                if(count($value["seriales"]) !== 0){
                  
                    foreach($value["seriales"] as $serial){
                      $toSerial[] = [
                        'serial' => $serial,
                        'factura' => $codnota,
                        'codart' => $value['codart'],
                        'facturar_serial' => false
                       ];
                       $updateSerial[] = $serial;
                    }
  
                }
        }
        
        }

        if(isset($toSerial)){
          
            Serial::insert($toSerial);
            serialDisponible::whereIn('seriales',$updateSerial)->update(['disponible' => false]);
        }
        Fanotaentregaart::insert($articulos);


          $articulos = Fanotaentregaart::where('codnota','=',$codnota)->get();
          $almacenes = Almacen::where('codsuc_asoc','=',$suc)->where('pfactur',true)->get(); 
          $existencia = '';
          foreach($articulos as $art){

          foreach($almacenes as $almacen){
            if($art->talla){
               $existencia = Caartalmubi::where('codalm','=',$almacen->codalm)
               ->where('codart','=',$art->codart)
               ->where('codtalla','=',$art->talla)
               ->where('exiact','>',0)
               ->first();
              if($existencia){
                $codalm = $almacen->codalm;
              break;
              }
            }//if talla
             else{
             $existencia = Caartalmubi::where('codalm','=',$almacen->codalm)
             ->where('codart','=',$art->codart)
             ->where('exiact','>',0)
             ->first();
              

              if($existencia){
                $codalm = $almacen->codalm;
              break;
              }
            }//else
          }//foreach
            if(isset($existencia)){
              $cantEnInventario = $existencia->exiact;
              $cantSustraer = $art->cantart;
              $cantEnInventario = $cantEnInventario-$cantSustraer;
              $existencia->exiact = $cantEnInventario;
              $existencia->save();
              $aral = Fanotaentregaart::where('codnota','=',$art->codnota)
                      ->where('codart','=',$art->codart)->first();
              $aral->codalm = $codalm;
              $aral->save();
            }  

           } //foreach articulos


        DB::commit();
        
        return response()->json([
            'success' => true,
            'message' => 'Nota de entrega guardada con éxito',
            'redirect' => route('FNEIndex'),
        ]);
    } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            throw $th;
            return response()->json([
                'error' => true,
                'Error al guardar Nota de entrega'
            ], 500);
        }
        
    }

    public function getNumero()
    {
        $prefijo = 'PRO';
        $año = date('Y');
        $mes = date('m');
        $dia = date('d');
        $num = Fanotaentrega::where('codsuc', $this->sucursal())->withTrashed()->count() + 1;

        $string = Auth::user()->usersuc->nomsucu;
        $words = str_word_count($string, 1, 'UTF-8');
        $initials = array();
        foreach ($words as $word) {
            $initials[] = mb_substr($word, 0, 1, 'UTF-8');
        }
        $suc = implode('', $initials);

        $contador = str_pad($num, 5, 0, STR_PAD_LEFT);

        $numero = $prefijo . $año . $mes . $dia . '-' . $suc . $contador;

        return $numero;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $nota = Fanotaentrega::where('id', $id)->with('cliente', 'articulos.articulo', 'articulos.costos', 'cajero', 'moneda', 'tasa', 'banco', 'caja')->first();
        $menu = $this->retornarMenu();

        return view('facturacion.FaNotaEntrega.FNEshow')->with([
            'menus' => $menu,
            'nota' => $nota
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function devolucion(Request $request)
    {
        $request->validate([
            'motanu' => 'required'
        ]);
        try{

            $nota = Fanotaentrega::where('codnota', $request->codnota)->first();
            $date = Carbon::now()->format('Y-m-d');
            
            $nota->status = 'N';
            $nota->usuanu = Auth::user()->nomuse;
            $nota->fecanu = $date;
            $nota->motanu = strtoupper($request->motanu);
            $nota->save();

            $cajaId = Caja::where('id', $request->codcaja)->first();
            $suc = Auth::user()->userSegSuc->codsuc;
            
            if(!$cajaId) throw new Exception ("No existe caja para devolver", 500);
            
    
              $articulos = Fanotaentregaart::where('codnota','=',$request->codnota)->get();
              $almacenes = Almacen::where('codsuc_asoc','=',$suc)->where('pfactur',true)->get(); 
              
              foreach($articulos as $art){
    
                foreach($almacenes as $almacen){
                  
                 $existencia = Caartalmubi::where('codalm','=',$almacen->codalm)
                 ->where('codart','=',$art->codart)
                 ->where('exiact','>=',0)
                 ->first();
                  if($existencia){
                    $codalm = $almacen->codalm;
                  break;
                  }
              }
    
                $serialD = Serial::where('factura',$request->codnota)->select('serial')->get(); 
                
                if($serialD->isNotEmpty()){
                  foreach ($serialD as $key => $value) {
                    
                    $seriales[] = $value->serial;
                  }
    
                  Serial::where('factura',$request->codnota)
                  ->update([
                    'deleted_at' => Carbon::now()
                  ]);
                    
                    serialDisponible::whereIn('seriales',$seriales)
                                      ->update([
                                                'disponible' => true
                                              ]);
                  }
                
                if(isset($existencia)){
                  $cantEnInventario = $existencia->exiact;
                  $cantSustraer = $art->cantart;
                  $cantEnInventario = $cantEnInventario+$cantSustraer;
                  $existencia->exiact = $cantEnInventario;
                  $existencia->save();
                  $aral = Fanotaentregaart::where('codnota','=',$art->codnota)
                          ->where('codart','=',$art->codart)->first();
                  $aral->codalm = isset($codalm);
                  $aral->save();
                }
    
               } 
               return response()->json([
                'success' => true,
                'message' => 'Nota de entrega devuelta con éxito',
                'redirect' => route('FNEIndex'),
            ]);
    
          }catch(\Exception $e){
            
            return response()->json([
              'error' => true,
              'msg'   =>  $e->getMessage()
            ],500);
          }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function reportePdf($nota)
    {

        $notaPdf = Fanotaentrega::where('codnota', $nota)->with('cliente', 'articulos.articulo', 'articulos.costos', 'cajero', 'moneda', 'tasa', 'banco', 'caja')->first();
        $articulos = Fanotaentregaart::where('codnota', $nota)->with('articulo', 'costos')->get();
        $titulo = 'Nota de Entrega';
        $date = date('Y-m-d');

        $pdf = new generadorPDF();

        $pdf->AddPage('P');
        $pdf->SetTitle($notaPdf['codnota']);
        $pdf->SetFont('arial','B',16);
        $pdf->SetWidths(array(90,90));
        $pdf->Ln();
        $pdf->Cell(0,22,utf8_decode($titulo.' N° '.$notaPdf->codnota),0,0,'C');
        $pdf->Ln();
        $pdf->SetFont('arial','B',10);
        $pdf->SetFillColor(0,0,0);
        $pdf->Cell(37,7,utf8_decode('Fecha de emisión'),1,0,'C');
        $pdf->Cell(37,7,utf8_decode('Código'),1,0,'C');
        $pdf->Cell(39,7,utf8_decode('Cajero'),1,0,'C');
        $pdf->Cell(38,7,utf8_decode('Caja'),1,0,'C');
        $pdf->Cell(38,7,utf8_decode('Cliente'),1,0,'C');
        $pdf->Ln();
        $pdf->SetWidths(array(37,37,39,38,38));
        $pdf->SetAligns(['C','C','C','C','C']);
        $pdf->SetFont('arial','',10,3);
        $pdf->Row(array(
          ($notaPdf->fecnota),
          ($notaPdf->codnota),
          ($notaPdf->cajero->nomuse), 
          ($notaPdf->caja->descaj), 
          ($notaPdf->cliente->nompro), 
        ));
        $pdf->SetFillColor(0,0,0);
        $pdf->SetFont('arial','B',10,5);

        if ($notaPdf->tippag === 'TRANSFERENCIA' || $notaPdf->tippag === 'DEPOSITO') {
            
            $pdf->Cell(37,7,utf8_decode('Estado'),1,0,'C');
            $pdf->Cell(37,7,utf8_decode('Moneda'),1,0,'C');
            $pdf->Cell(39,7,utf8_decode('Forma de pago'),1,0,'C');
            $pdf->Cell(38,7,utf8_decode('Banco'),1,0,'C');
            $pdf->Cell(38,7,utf8_decode('Referencia'),1,0,'C');
            $pdf->Ln();
            $pdf->SetWidths(array(37,37,39,38,38));
            $pdf->SetAligns(['C','C','C','C', 'C']);
            $pdf->SetFont('arial','',10,3);
            $pdf->Row(array(
            ($notaPdf->status),
            ($notaPdf->moneda->nombre),
            ($notaPdf->tippag),
            ($notaPdf->banco->nomban), 
            ($notaPdf->refer), 
            
            ));
        }

        else {
            $pdf->Cell(63,7,utf8_decode('Moneda'),1,0,'C');
            $pdf->Cell(63,7,utf8_decode('Moneda'),1,0,'C');
            $pdf->Cell(63,7,utf8_decode('Forma de pago'),1,0,'C');
            $pdf->Ln();
            $pdf->SetWidths(array(63,63,63));
            $pdf->SetAligns(['C','C']);
            $pdf->SetFont('arial','',10,3);
            $pdf->Row(array(
            ($notaPdf->status),
            ($notaPdf->moneda->nombre),
            ($notaPdf->tippag),
            ));
        }

        $pdf->Ln(2);
        $pdf->SetFillColor(0,0,0);
        $pdf->SetFont('arial','B',10,5);
        $pdf->Cell(26,7,utf8_decode('Cantidad'),1,0,'C');
        $pdf->Cell(88,7,utf8_decode('Descripción'),1,0,'C');
        $pdf->Cell(37,7,utf8_decode('Precio'),1,0,'C');
        $pdf->Cell(38,7,utf8_decode('Subtotal'),1,0,'C');

        $pdf->Ln();
        $pdf->SetWidths(array(26,88,37,38));
        $pdf->SetAligns(['C','J','C','C']);
        $pdf->SetFont('arial','',10,0);
        foreach ($articulos as $art) {
            $string = implode(PHP_EOL, $art->seriales());

            $pdf->Row(array
            (PHP_EOL.$art->cantart, 
            PHP_EOL.$art->codart.' - '.$art->articulo->desart.PHP_EOL.PHP_EOL.'S/N: '.$string.PHP_EOL,
            PHP_EOL.$art->costos->pvpart, 
            PHP_EOL.$art->subtotal.PHP_EOL));
                $string = '';
            }

        $pdf->Ln(6);
        $pdf->SetFont('arial','B',10);
        $pdf->Cell(0,0,utf8_decode('Costos'),0,0,'C'); 
        $pdf->Ln(4);
        $pdf->SetFont('arial','B',10,5);  
        $pdf->SetFillColor(255,157,116);

        $pdf->Cell(50,7,utf8_decode('Subtotal'),1,0,'C');
        $pdf->Cell(45,7,utf8_decode('Recargo'),1,0,'C');
        $pdf->Cell(45,7,utf8_decode('Monto descuento'),1,0,'C');
        $pdf->Cell(49,7,utf8_decode('Monto Total'),1,0,'C');
        $pdf->Ln();
        $pdf->SetWidths(array(50,45,45,49));
        $pdf->SetAligns(['C','C','C','C']);
        $pdf->SetFont('arial','',10,5);
        $pdf->Cell(50,7,utf8_decode($notaPdf->subtotal),1,0,'C');
        $pdf->Cell(45,7,utf8_decode($notaPdf->monrecargo),1,0,'C');
        $pdf->Cell(45,7,utf8_decode($notaPdf->mondes),1,0,'C');
        $pdf->Cell(49,7,utf8_decode($notaPdf->monto),1,0,'C');

        $pdf->Output('I',$notaPdf->codnota.'.pdf');
        exit;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function newCliente(ClienteRequest $request)
    {
        $guion = '-';
        $documento = $request->tipodocumento . $guion . $request->cedularif;
        $documento2 = $request->tipodocumento . $request->cedularif;

        $comprobar = Facliente::where('codpro', $documento2)->withTrashed()->first();
        if ($comprobar) {
            if ($comprobar['deleted_at'] == NULL) {
                return response()->json([
                    'error' => true,
                    'message' => 'Cliente ya registrado',
                ]);
            } else {
                $comprobar->update(
                    [
                        'deleted_at' => NULL,
                        'codpro' => $documento2,
                        'nompro' => $request['nombre'],
                        'rifpro' => $documento,
                        'dirpro' => $request['direccion'],
                        'telpro' => $request['telefono'],
                        'email' => $request['email']
                    ]
                );
                $clientes = Facliente::get();

                return response()->json([
                    'success' => true,
                    'message' => 'Cliente registrado con éxito',
                    'clientes' => $clientes
                ]);
            }
        }


        $cont = Facliente::withTrashed()->get()->count();

        $cont = $cont + 1;

        $suc = Auth::user()->getCodigoActiveS();
        $codsuc_reg = explode('-', $suc);
        $cod = $cont . $guion . $codsuc_reg[1];

        if ($request['email'] == null) {
            $request['email'] = '';
        }


        Facliente::create([
            'codpro' => $documento2,
            'nompro' => $request['nombre'],
            'rifpro' => $documento,
            'dirpro' => $request['direccion'],
            'telpro' => $request['telefono'],
            'email' => $request['email'],
            'codigoid' => $cod,
            'codsuc' => $suc,
        ]);

        $clientes = Facliente::get();

        return response()->json([
            'success' => true,
            'message' => 'Cliente registrado con éxito',
            'clientes' => $clientes,
        ]);
    }

    public function closeCaja(Request $request)
    {
        
            $inactivo = cajaUser::where('loguse',Auth::user()->loguse)->where('activo','TRUE')->first();

            if(is_null($inactivo)){
                return response()->json([
                    'titulo' => 'No hay caja activa',
                    'redirect' => route('FNEIndex'),
                ]);
            }
            $inactivo->activo = 'FALSE';
            $inactivo->save();
    
            return response()->json([
              'titulo' => 'CAJA CERRADA',
              'redirect' => route('FNEIndex'),
            ]);
    }

    public function checkCaja(Request $request)
    {
        $activo = cajaUser::where('loguse', Auth::user()->loguse)->where('activo','TRUE')->first();

        return response()->json([
            'caja' => $activo
        ]);
    }
}
