<?php

namespace App\Http\Controllers\auditoria;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use Illuminate\Contracts\Auth\Guard;
use App\User;

//use Spatie\Activitylog\Models\Activity;
//use OwenIt\Auditing\Models\Audit;
use App\Activity;
use App\Audit;
use Helper;
use App\Segususuc;
use Illuminate\Support\Facades\Auth;

class AuditoriaController extends Controller
{
   

		private $guard;
		public function __construct(Guard $guard){
			$this->middleware('auth');
			$this->guard = $guard;
		}


		public function index()
		{
			$menu = new HomeController();
	       	$audit = Audit::orderBy('id','DESC')->with(['user'])->paginate(10);
			
			
			return view('auditoria.listadoAuditoria')->with('menus',$menu->retornarMenu())->with('auditor',$audit);
		}

		public function getActividas($id)
		{
	      
	      $desc=Helper::desencriptar($id);
    	  $sep  = explode('-',$desc);
          $ID= (int)$sep[1];

          $audit = Audit::where('id',$ID)->with(['user'])->first();
          
          return response()->json([

          	 'nombreUser'  => $audit->user->nomuse,
          	 'Ip'          => $audit->ip_address,
          	 'navegador'   => $audit->user_agent,
          	 'old_values'  => $audit->old_values,
          	 'new_values'  => $audit->new_values,
          	 'url' 		   => $audit->url,
          	 'Tabla'       => $audit->auditable_type

          ]);


		}

		public function filtro(Request $request)
		{

			$filter = str_replace('\\', '\\\\', $request->filtro);
			$user = Audit::with('user')
			->where('event','ilike','%'.$filter.'%')
			->orWhere('auditable_type', 'ilike', '%'.$filter.'%')
			->orWhere('ip_address', 'ilike', '%'.$filter.'%')
			->orWhere('created_at', 'ilike', '%'.$filter.'%')
			->orWhereHas('user', function ($q) use ($filter) {
				$q->where('nomuse', 'ilike', '%'.$filter.'%');
			})
			->orderBy('id','DESC')->paginate(10);	
			$menu = new HomeController();
			

			return view('auditoria.listadoAuditoria')->with('menus',$menu->retornarMenu())->with('auditor',$user);

		   
		}
}
