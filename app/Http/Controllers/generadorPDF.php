<?php

namespace App\Http\Controllers;

use Fpdf;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Parser;
use Illuminate\Support\Facades\Auth;
use App\Sucursal;
use App\Empresa;
use Helper;

class generadorPDF extends \Codedge\Fpdf\Fpdf\Fpdf
{
var $widths;
var $aligns;

private $hasTitle;
private $title;

public function __construct($hasTitle = false, $title = '')
{
    parent::__construct();
    $this->hasTitle = $hasTitle;
    $this->title = $title;
}

function Sucursaldir(){

}

function AddPage($orientation='', $size='', $rotation=0)
{
    // Start a new page
    if($this->state==3)
        $this->Error('The document is closed');
    $family = $this->FontFamily;
    $style = $this->FontStyle.($this->underline ? 'U' : '');
    $fontsize = $this->FontSizePt;
    $lw = $this->LineWidth;
    $dc = $this->DrawColor;
    $fc = $this->FillColor;
    $tc = $this->TextColor;
    $cf = $this->ColorFlag;
    if($this->page>0)
    {
        // Page footer
        $this->InFooter = true;
        $this->Footer($orientation);
        $this->InFooter = false;
        // Close page
        $this->_endpage();
    }
    // Start new page
    $this->_beginpage($orientation,$size,$rotation);
    // Set line cap style to square
    $this->_out('2 J');
    // Set line width
    $this->LineWidth = $lw;
    $this->_out(sprintf('%.2F w',$lw*$this->k));
    // Set font
    if($family)
        $this->SetFont($family,$style,$fontsize);
    // Set colors
    $this->DrawColor = $dc;
    if($dc!='0 G')
        $this->_out($dc);
    $this->FillColor = $fc;
    if($fc!='0 g')
        $this->_out($fc);
    $this->TextColor = $tc;
    $this->ColorFlag = $cf;
    // Page header
    $this->InHeader = true;
    $this->Header($orientation);
    $this->InHeader = false;
    // Restore line width
    if($this->LineWidth!=$lw)
    {
        $this->LineWidth = $lw;
        $this->_out(sprintf('%.2F w',$lw*$this->k));
    }
    // Restore font
    if($family)
        $this->SetFont($family,$style,$fontsize);
    // Restore colors
    if($this->DrawColor!=$dc)
    {
        $this->DrawColor = $dc;
        $this->_out($dc);
    }
    if($this->FillColor!=$fc)
    {
        $this->FillColor = $fc;
        $this->_out($fc);
    }
    $this->TextColor = $tc;
    $this->ColorFlag = $cf;
}

function Footer($orientation='')
{
    // Position at 1.5 cm from bottom
/*    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,$this->PageNo(),0,0,'R');*/
 

//     if ($orientation == 'L')
//    {$this->Image('images/pdf/A1WIN2.jpg',10,190,277,15);
//     $this->Ln(20);} 
//     else{
//     $this->Image('images/pdf/A1WIN2.jpg',10,276,190,15);
//     $this->Ln(20);
//     }
     $this->SetTextColor(0,0,0);
     $this->SetFont('arial','',8,5);
     $this->Cell(0,10,'Pag '.$this->PageNo(),0,0,'R');
    
}
function Header($orientation='')
{
    $suc= session('codsuc');
    $dirfis = Sucursal::where('codsuc','=',$suc)->first()->dirfis;
    $codemp=Sucursal::where('codsuc','=',$suc)->first()->codempre;
    $nomemp=Empresa::where('codrif','=',$codemp)->first()->nomrazon;
    $dirinst= Empresa::Orderby('id','DESC')->first()->dirfis;
    $logo = Empresa::where('codrif','=',$codemp)->first()->logo;
    
    //dd($dirfis);
    // Logo
    // 
    if($orientation == 'L')
    {
        $this->Image($logo,10,4,40,30);
        //$this->Ln(23);
        $this->SetTextColor(0,0,0);
        $this->SetFont('arial','B',10);
        $length = strlen($dirfis);
        //$this->setXY(50,15);
        //$this->Cell(150,0,utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'),0,0,'L');
        //dd($this->getY());
        $this->ln(5);
        $this->setXY(50,$this->getY());
        $this->Cell(150,0,utf8_decode($nomemp),0,0,'L');
        $this->ln(5);
        $this->setXY(50,$this->getY());
        $this->Cell(150,0,utf8_decode($codemp),0,0,'L');
        $this->ln(5);
        $this->setXY(50,$this->getY());
        // $this->cell(20,date('d-m-YYYY'),0,0,'R');
        // dd($dirfis); 
        if($length > 100){
            $parts = str_split($dirfis, 68);
            $length2 = strlen($length);
 

            for($i=0;$i<$length2-1;$i++){
                $this->setX(50);
                $this->Cell(150,0,utf8_decode(strtoupper($parts[$i])),0,0,'L');
                $this->Ln(5);
            }
        }
        else{
            $this->setX(50);

            $this->Cell(150,0,utf8_decode($dirfis),0,0,'L');
        }
        $this->Ln(10);
        
    }else{
        if($this->hasTitle){
            $this->Image('images/pdf/logo.jpg',10,4,40,30);
            $this->Ln(15);
            $this->SetTextColor(0,0,0);
            $this->SetFont('arial','B',16);
            /* $this->Cell(0,0,$dirfis,0,0,'C');*/
            $length = strlen($dirfis);
            if($length > 55){
                $parts = str_split($dirfis, 55);
                $length2 = strlen($length);
                for($i=0;$i<$length2;$i++){
                    $this->Cell(0,0,$parts[$i],0,0,'C');
                    $this->Ln(5);
                }
            }
            else{
                $this->Cell(0,0,$dirfis,0,0,'C');
            }
            $this->Ln(2);
        }else{
            $this->Image($logo,10,4,40,30);
            $this->Ln(5);
            $this->SetTextColor(0,0,0);
            $this->SetFont('arial','B',11);
            $length = strlen($dirfis);

            //$this->setXY(70,15);
            //$this->Cell(150,0,utf8_decode('REPÚBLICA BOLIVARIANA DE VENEZUELA'),0,0,'L');
            //$this->ln(5);
            $this->setXY(50,$this->getY());
            $this->Cell(150,0,utf8_decode($nomemp),0,0,'L');
            $this->ln(5);
            $this->setXY(50,$this->getY());
            $this->Cell(150,0,utf8_decode($codemp),0,0,'L');
            $this->ln(5);
            $this->setXY(50,$this->getY());
            if($length > 100){
                $parts = str_split($dirfis, 68);
                $length2 = strlen($length);
   
    
                for($i=0;$i<$length2-1;$i++){  
                    $this->setX(50);
                    $this->Cell(150,0,utf8_decode(strtoupper($parts[$i])),0,0,'L');
                    $this->Ln(5);
                }
            }
            else{
                $this->setX(50);

                $this->Cell(150,0,utf8_decode($dirfis),0,0,'L');
            }
            $this->Ln(10);
            $this->SetXY(50,8);
            //$this->Cell(0,0,$this->title,0,0,'C');
            $this->MultiCell(150,13, utf8_decode($this->title), 0,'C');
            $this->Ln(2);

        }
    }
    // Arial bold 15
/*    $this->SetFont('Arial','B',15);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(30,10,'Title',1,0,'C');*/
    // Line break
}
function SetWidths($w)
{
    //Set the array of column widths
    $this->widths=$w;
}

function SetAligns($a)
{
    //Set the array of column alignments
    $this->aligns=$a;
}

function Row($data)
{
    $ln = 3;
    //Calculate the height of the row
    $nb=0;
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=$ln*$nb;
    //Issue a page break first if needed
    $this->CheckPageBreak($h);
    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
        //Save the current position
        $x=$this->GetX();
        $y=$this->GetY();
        //Draw the border
        $this->Rect($x,$y,$w,$h);
        //Print the text
        $this->MultiCell($w,$ln,utf8_decode($data[$i]),0,$a);
        //Put the position to the right of the cell
        $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
}

function Parrafo($txt)
{
    $this->MultiCell(189,3, utf8_decode($txt), 0, 'J', false);
}

function CheckPageBreak($h)
{
    //If the height h would cause an overflow, add a new page immediately
    if($this->GetY()+$h>$this->PageBreakTrigger)
        $this->AddPage($this->CurOrientation);
}

function NbLines($w,$txt)
{
    //Computes the number of lines a MultiCell of width w will take
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 and $s[$nb-1]=="\n")
        $nb--;
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;
    while($i<$nb)
    {
        $c=$s[$i];
        if($c=="\n")
        {
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
            continue;
        }
        if($c==' ')
            $sep=$i;
        $l+=$cw[$c];
        if($l>$wmax)
        {
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
            }
            else
                $i=$sep+1;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
        }
        else
            $i++;
    }
    return $nl;
}
function Close($orientation='')
{
    // Terminate document
    if($this->state==3)
        return;
    if($this->page==0)
        $this->AddPage($orientation);
    // Page footer
    $this->InFooter = true;
    $this->Footer($orientation);
    $this->InFooter = false;
    // Close page
    $this->_endpage();
    // Close document
    $this->_enddoc();
}


function Output($dest='', $name='', $isUTF8=false, $orientation='')
{
    // Output PDF to some destination
    $this->Close($orientation);
    if(strlen($name)==1 && strlen($dest)!=1)
    {
        // Fix parameter order
        $tmp = $dest;
        $dest = $name;
        $name = $tmp;
    }
    if($dest=='')
        $dest = 'I';
    if($name=='')
        $name = 'doc.pdf';
    switch(strtoupper($dest))
    {
        case 'I':
            // Send to standard output
            $this->_checkoutput();
            if(PHP_SAPI!='cli')
            {
                // We send to a browser
                header('Content-Type: application/pdf');
                header('Content-Disposition: inline; '.$this->_httpencode('filename',$name,$isUTF8));
                header('Cache-Control: private, max-age=0, must-revalidate');
                header('Pragma: public');
            }
            echo $this->buffer;
            break;
        case 'D':
            // Download file
            $this->_checkoutput();
            header('Content-Type: application/x-download');
            header('Content-Disposition: attachment; '.$this->_httpencode('filename',$name,$isUTF8));
            header('Cache-Control: private, max-age=0, must-revalidate');
            header('Pragma: public');
            echo $this->buffer;
            break;
        case 'F':
            // Save to local file
            if(!file_put_contents($name,$this->buffer))
                $this->Error('Unable to create output file: '.$name);
            break;
        case 'S':
            // Return as a string
            return $this->buffer;
        default:
            $this->Error('Incorrect output destination: '.$dest);
    }
    return '';
}

}