<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PrecomController;
use App\Mail\RequisicionMail; 
use App\Mail\OrdenesMail; 
use App\Mail\ReqPresupuestoMail;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Session;
use Helper;
use Exception;
use App\User;
use App\Famoneda;
use App\Almacen;
use App\Farecarg;
use App\Cpdeftit;
use App\Caregart;
use App\Nphojint;
use App\Tsdefmon;
use App\Caprovee;
use App\Fatasacamb;
use App\NotaEntrega;
use App\NotaEntregaArt;
use App\Models\Casolart;
use App\Models\Caartord;
use App\Models\Segcenusu;
use App\Models\Costos;
use App\Models\Caconpag;
use App\Models\Caordcom;
use App\Models\Caforent;
use App\Models\Fortipfin;
use App\Npasicaremp;
use App\Models\Caartsol;
use App\Models\Npcatpre;
use App\Models\Nppartidas;
use App\Models\Cpprecom;
use App\Models\Cpimpprc;
use App\Http\Controllers\generadorPDF;

class OrdenesController extends Controller
{

    public function retornarMenu()
    {
        $yaml = new Parser();
        $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
        return $menu['menu'];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filtro = trim($request->get('filtro'));

        if($filtro == ""){
            
            $compras = Caordcom::where('tipord', 'C')->orderBy('id','DESC')->paginate(8);

            $servicios = Caordcom::where('tipord', 'S')->orderBy('id','DESC')->paginate(8);

        } else{

            $compras = Caordcom::where('tipord', 'C')
            ->where('ordcom', 'LIKE', '%'.$filtro.'%')
            ->orwhere('desord', 'LIKE', '%'.$filtro.'%')
            ->orwhere('staord', 'LIKE', '%'.$filtro.'%')
            ->orwhere('fecord', 'LIKE', '%'.$filtro.'%')
            ->orderBy('id','DESC')->paginate(8);

            $servicios = Caordcom::where('tipord', 'S')
            ->where('ordcom', 'LIKE', '%'.$filtro.'%')
            ->orwhere('desord', 'LIKE', '%'.$filtro.'%')
            ->orwhere('staord', 'LIKE', '%'.$filtro.'%')
            ->orwhere('fecord', 'LIKE', '%'.$filtro.'%')
            ->orderBy('id','DESC')->paginate(8);

        }
        
        $menu = $this->retornarMenu();
        
        return view('compras.ordenes.ordenes')->with([
            'menus' => $menu,
            'compras' => $compras,
            'servicios' => $servicios,
            'filtro' => $filtro,
        ]);
    }

    public function create(){

        $menu = $this->retornarMenu();
        $costos = Costos::get();
        $solicitante = Auth::user();
        $provs = Caprovee::get();
        $condiciones = Caconpag::get();
        $formasEnt = Caforent::get();
        $tipoFinan = Fortipfin::get();
        $moneda = Tsdefmon::get();
        $recargos = Farecarg::get();
           
        
        return view('compras.ordenes.createOrdenes')->with([
            'menus' => $menu,
            'costos' => $costos,
            'proveedores' => $provs,
            'solicitante' => $solicitante,
            'condiciones' => $condiciones,
            'formasEnt' => $formasEnt,
            'tipoFinan' => $tipoFinan,
            'moneda' => $moneda,
            'recargos' => $recargos,
        ]);

    }

    public function ListadoView(){

        $menu = $this->retornarMenu();

        $provees = Caprovee::orderBy('fecreg', 'asc')->whereNotNull('fecreg')->get();
        $ordenCompra = Caordcom::where('doccom', 'OC')->orderBy('fecord','asc')->get();
        $ordenServicio = Caordcom::where('doccom', 'OS')->orderBy('fecord','asc')->get();
        
        return view('reportes.compras.listadoResumenOrdenesCompras')->with([
            'menus' => $menu,
            'provees' => $provees,
            'ordenCompra' => $ordenCompra,
            'ordenServicio' => $ordenServicio,
        ]);

    }

    public function generarReporteOrdenes(Request $request){
        $orden = Caordcom::where('ordcom', $request->ordcom)->with('articulos', 'solicitante', 'ccosto', 'proveedor', 'pedido', 'condicionPago', 'formaEntrega', 'tipoFinancia', 'tipoMoneda', 'tasa')->first();
        $pdf = new generadorPDF();

        $titulo = utf8_decode("REPORTE DE ORDEN");
        $fecha = date('d-m-Y');

        $pdf->AddPage('P');
        $pdf->SetTitle($titulo);

        $pdf->SetFont('arial', 'B', 16);
        $pdf->SetWidths(array(90, 90));
        $pdf->Ln();
        $pdf->Cell(0, 16, utf8_decode($titulo), 0, 0, 'C');
        $pdf->Ln();
        $pdf->SetFont('arial', 'B', 10);
        $pdf->Ln(5);
        $pdf->Cell(0, 0, utf8_decode('FECHA EMISIÓN: ' . $fecha), 0, 0, 'L');
        $pdf->Ln(10);
        $pdf->SetFont('arial', 'B', 8, 5);
        $pdf->SetFillColor(2, 157, 116);
        $pdf->Cell(47, 8, utf8_decode('SOLICITANTE'), 1, 0, 'C');
        $pdf->Cell(63, 8, utf8_decode('DEPARTAMENTO'), 1, 0, 'C');
        $pdf->Cell(40, 8, utf8_decode('NÚMERO'), 1, 0, 'C');
        $pdf->Cell(40, 8, utf8_decode('FECHA'), 1, 0, 'C');
        $pdf->Ln();
        $pdf->SetWidths(array(47, 47, 47, 47,));
        $pdf->SetAligns(['C', 'C', 'C', 'C', 'C', 'C', 'C', 'C']);
        $pdf->SetFont('arial', '', 8, 5);

        $pdf->SetWidths(array(47, 63, 40, 40));
        $pdf->SetAligns(['C', 'C', 'C', 'C', 'C', 'C', 'C', 'C']);
        $pdf->SetFont('arial', '', 8, 5);


        $pdf->Row(
            array(
                utf8_decode($orden->solicitante->nomemp),
                utf8_decode($orden->ccosto->descen),
                $orden->ordcom,
                $orden->fecord,
            )
        );

        $pdf->SetFont('arial', 'B', 8, 5);
        $pdf->SetFillColor(2, 157, 116);
        $pdf->Cell(63, 8, utf8_decode('TIPO DE ORDEN'), 1, 0, 'C');
        $pdf->Cell(47, 8, utf8_decode('N° PEDIDO'), 1, 0, 'C');
        $pdf->Cell(80, 8, utf8_decode('PROVEEDOR'), 1, 0, 'C');
        $pdf->Ln();

        $pdf->SetWidths(array(63, 47, 80));
        $pdf->SetAligns(['C', 'C', 'C']);
        $pdf->SetFont('arial', '', 8, 5);
        $pdf->Row(
            array(
                utf8_decode($orden->tipord),
                $orden->ordcom,
                utf8_decode($orden->proveedor->nompro),
            )
        );


        $pdf->Output('I', 'ORDEN');
        exit;

    }

    public function reporteCompra(Request $request){

        $status = $request['status'];
        $desde = $request['desde'];
        $hasta = $request['hasta'];
        $desdeProveedor = $request['desdeProveedorCompra'];
        $hastaProveedor = $request['hastaProveedorCompra'];
        $desdeOrden = $request['desdeOrdenCompra'];
        $hastaOrden = $request['hastaOrdenCompra'];

        try{
            if(!$desde)throw new Exception("Debe Seleccionar el Parametros de Fecha a Consultar");
            else if(!$hasta)throw new Exception("Debe Seleccionar el Parametros de Fecha a Consultar");
            else if(!$desdeProveedor)throw new Exception("Debe Seleccionar un Proveedor");
            else if(!$hastaProveedor)throw new Exception("Debe Seleccionar un Proveedor");
            else if(!$desdeOrden)throw new Exception("Debe Seleccionar una Orden de Compra");
            else if(!$hastaOrden)throw new Exception("Debe Seleccionar una Orden de Compra");

        }
        catch(Exception $e){
            return redirect()->route('ListadoResumenOrdenesCompras')->with('error',$e->getMessage());
        } 

        $provdesde = Caprovee::whereId($desdeProveedor)->select('fecreg')->first();
        $provhasta = Caprovee::whereId($hastaProveedor)->select('fecreg')->first();
       
        $proveedores = Caprovee::where('fecreg', '>=', $provdesde->fecreg)
        ->where('fecreg', '<=', $provhasta->fecreg)
        ->pluck('codpro');

        $orden = Caordcom::with('articulos','proveedor', 'ccosto')->where('fecord', '>=', $desdeOrden)->where('fecord', '<=', $hastaOrden)
        ->where('created_at', '>=', $desde)->where('created_at', '<=', $hasta)->get();  

        $pdf = new generadorPDF();
        
        $titulo = 'REPORTE DE ORDENES DE COMPRAS';
        $total = $proveedores->count();
        $fecha = date('Y-m-d');

        $pdf->AddPage('L');
        $pdf->SetTitle('REPORTE DE ORDENES DE COMPRAS');
        $pdf->SetFont('arial','B',16);
        $pdf->SetWidths(array(90,90));
        $pdf->Ln();
        $pdf->Cell(0,10,utf8_decode($titulo),0,0,'C');
        $pdf->Ln();
        $pdf->SetFont('arial','B',10);
        $pdf->Ln(10);
        
        $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: DESDE '.date("d-m-Y", strtotime($desde)).' HASTA '.date("d-m-Y", strtotime($hasta))),0,0,'L');
        $pdf->Ln(5);
        $pdf->Cell(0,0,utf8_decode('REPORTE EMITIDO: '.date("d-m-Y", strtotime($fecha))),0,0,'L');

        $pdf->Ln(10);
        $pdf->SetFont('arial','B',8,5);
        $pdf->SetFillColor(2,157,116);

        $pdf->Cell(20,8,utf8_decode('N° ORDEN'),1,0,'C');
        $pdf->Cell(35,8,utf8_decode('DESCRIPCIÓN ORDEN'),1,0,'C');
        $pdf->Cell(20,8,utf8_decode('FECHA'),1,0,'C');
        $pdf->Cell(20,8,utf8_decode('ESTATUS'),1,0,'C');
        $pdf->Cell(30,8,utf8_decode('PROVEEDOR'),1,0,'C');
        $pdf->Cell(35,8,utf8_decode('UNI. SOLICITANTE'),1,0,'C');
        $pdf->Cell(20,8,utf8_decode('SOLICITUD'),1,0,'C');
        $pdf->Cell(20,8,utf8_decode('ORD. PAGO'),1,0,'C');
        $pdf->Cell(18,8,utf8_decode('MONTO ($)'),1,0,'C');
        $pdf->Cell(20,8,utf8_decode('MONTO'),1,0,'C');
        $pdf->Cell(20,8,utf8_decode('RECARGO'),1,0,'C');
        $pdf->Cell(20,8,utf8_decode('TOTAL'),1,0,'C');

        $pdf->Ln();
        $pdf->SetWidths(array(20,35,20,20,30,35,20,20,18,20,20,20));
        $pdf->SetAligns(['C','C','C','C','C','C','C']);
        $pdf->SetFont('arial','',8,5);
        
        foreach($orden as $ordenes){

            if($status == 'A'){

                $pdf->Row(
                
                    array(
                        utf8_decode($ordenes->ordcom),
                        utf8_decode($ordenes->desord),
                        utf8_decode($ordenes->created_at),
                        utf8_decode($ordenes->afepre == 'A' ? 'APROBADA' : '' ),
                        utf8_decode($ordenes->proveedor->nompro),
                        utf8_decode($ordenes->ccosto->descen),
                        utf8_decode($ordenes->refsol),
                        utf8_decode(''),
                        utf8_decode($ordenes->montasa),
                        utf8_decode($ordenes->subtotal),
                        utf8_decode($ordenes->moniva),
                        utf8_decode($ordenes->monord),
                    )
    
                );

            } elseif($status == 'N'){

                $pdf->Row(
                
                    array(
                        utf8_decode($ordenes->ordcom),
                        utf8_decode($ordenes->desord),
                        utf8_decode($ordenes->created_at),
                        utf8_decode($ordenes->afepre == 'N' ? 'ANULADADA' : '' ),
                        utf8_decode($ordenes->proveedor->nompro),
                        utf8_decode($ordenes->ccosto->descen),
                        utf8_decode($ordenes->refsol),
                        utf8_decode(''),
                        utf8_decode($ordenes->montasa),
                        utf8_decode($ordenes->subtotal),
                        utf8_decode($ordenes->moniva),
                        utf8_decode($ordenes->monord),
                    )
    
                );

            } elseif($status == 'TODAS'){

                if($ordenes->afepre == 'A' || $ordenes->afepre == 'N'){
                   
                    $pdf->Row(
                
                        array(
                            utf8_decode($ordenes->ordcom),
                            utf8_decode($ordenes->desord),
                            utf8_decode($ordenes->created_at),
                            utf8_decode($ordenes->afepre == 'A' ? 'APROBADA' : 'ANULADA' ),
                            utf8_decode($ordenes->proveedor->nompro),
                            utf8_decode($ordenes->ccosto->descen),
                            utf8_decode($ordenes->refsol),
                            utf8_decode(''),
                            utf8_decode($ordenes->montasa),
                            utf8_decode($ordenes->subtotal),
                            utf8_decode($ordenes->moniva),
                            utf8_decode($ordenes->monord),
                        )
        
                    );

                } 

            }
                
                
        }

        $pdf->Output('I','Reporte de Ordenes de Compra-'.$fecha.'pdf');

    }

    public function reporteServicio(Request $request){

        $status = $request['status'];
        $desde = $request['desde'];
        $hasta = $request['hasta'];
        $desdeProveedorServicio = $request['desdeProveedorServicio'];
        $hastaProveedorServicio = $request['hastaProveedorServicio'];
        $desdeOrdenServicio = $request['desdeOrdenServicio'];
        $hastaOrdenServicio = $request['hastaOrdenServicio'];
        $provdesde = Caprovee::whereId($desdeProveedorServicio)->select('fecreg')->first();
        $provhasta = Caprovee::whereId($hastaProveedorServicio)->select('fecreg')->first();

        try{
            if(!$desde)throw new Exception("Debe Seleccionar el Parametros de Fecha a Consultar");
            else if(!$hasta)throw new Exception("Debe Seleccionar el Parametros de Fecha a Consultar");
            else if(!$desdeProveedorServicio)throw new Exception("Debe Seleccionar un Proveedor");
            else if(!$hastaProveedorServicio)throw new Exception("Debe Seleccionar un Proveedor");
            else if(!$desdeOrdenServicio)throw new Exception("Debe Seleccionar una Orden de Servicio");
            else if(!$hastaOrdenServicio)throw new Exception("Debe Seleccionar una Orden de Servicio");

        }
        catch(Exception $e){
            return redirect()->route('ListadoResumenOrdenesCompras')->with('error',$e->getMessage());
        }
       
        $proveedores = Caprovee::where('fecreg', '>=', $provdesde->fecreg)
        ->where('fecreg', '<=', $provhasta->fecreg)
        ->pluck('codpro');

        $servicio = Caordcom::with('articulos','proveedor', 'ccosto')->where('fecord', '>=', $desdeOrdenServicio)->where('fecord', '<=', $hastaOrdenServicio)
        ->where('created_at', '>=', $desde)->where('created_at', '<=', $hasta)->get();  

        $pdf = new generadorPDF();
        
        $titulo = 'REPORTE DE ORDENES DE COMPRAS';
        $total = $proveedores->count();
        $fecha = date('Y-m-d');

        $pdf->AddPage('L');
        $pdf->SetTitle('REPORTE DE ORDENES DE SERVICIO');
        $pdf->SetFont('arial','B',16);
        $pdf->SetWidths(array(90,90));
        $pdf->Ln();
        $pdf->Cell(0,10,utf8_decode($titulo),0,0,'C');
        $pdf->Ln();
        $pdf->SetFont('arial','B',10);
        $pdf->Ln(10);
        
        $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: DESDE '.date("d-m-Y", strtotime($desde)).' HASTA '.date("d-m-Y", strtotime($hasta))),0,0,'L');
        $pdf->Ln(5);
        $pdf->Cell(0,0,utf8_decode('REPORTE EMITIDO: '.date("d-m-Y", strtotime($fecha))),0,0,'L');

        $pdf->Ln(10);
        $pdf->SetFont('arial','B',8,5);
        $pdf->SetFillColor(2,157,116);

        $pdf->Cell(20,8,utf8_decode('N° ORDEN'),1,0,'C');
        $pdf->Cell(35,8,utf8_decode('DESCRIPCIÓN ORDEN'),1,0,'C');
        $pdf->Cell(20,8,utf8_decode('FECHA'),1,0,'C');
        $pdf->Cell(20,8,utf8_decode('ESTATUS'),1,0,'C');
        $pdf->Cell(30,8,utf8_decode('PROVEEDOR'),1,0,'C');
        $pdf->Cell(35,8,utf8_decode('UNI. SOLICITANTE'),1,0,'C');
        $pdf->Cell(20,8,utf8_decode('SOLICITUD'),1,0,'C');
        $pdf->Cell(20,8,utf8_decode('ORD. PAGO'),1,0,'C');
        $pdf->Cell(18,8,utf8_decode('MONTO ($)'),1,0,'C');
        $pdf->Cell(20,8,utf8_decode('MONTO'),1,0,'C');
        $pdf->Cell(20,8,utf8_decode('RECARGO'),1,0,'C');
        $pdf->Cell(20,8,utf8_decode('TOTAL'),1,0,'C');

        $pdf->Ln();
        $pdf->SetWidths(array(20,35,20,20,30,35,20,20,18,20,20,20));
        $pdf->SetAligns(['C','C','C','C','C','C','C']);
        $pdf->SetFont('arial','',8,5);
        
        foreach($servicio as $servicios){

            if($status == 'A'){

                $pdf->Row(
                
                    array(
                        utf8_decode($servicios->ordcom),
                        utf8_decode($servicios->desord),
                        utf8_decode($servicios->created_at),
                        utf8_decode($servicios->afepre == 'A' ? 'APROBADA' : '' ),
                        utf8_decode($servicios->proveedor->nompro),
                        utf8_decode($servicios->ccosto->descen),
                        utf8_decode($servicios->refsol),
                        utf8_decode(''),
                        utf8_decode($servicios->montasa),
                        utf8_decode($servicios->subtotal),
                        utf8_decode($servicios->moniva),
                        utf8_decode($servicios->monord),
                    )
    
                );

            } elseif($status == 'N'){

                $pdf->Row(
                
                    array(
                        utf8_decode($servicios->ordcom),
                        utf8_decode($servicios->desord),
                        utf8_decode($servicios->created_at),
                        utf8_decode($servicios->afepre == 'N' ? 'ANULADADA' : '' ),
                        utf8_decode($servicios->proveedor->nompro),
                        utf8_decode($servicios->ccosto->descen),
                        utf8_decode($servicios->refsol),
                        utf8_decode(''),
                        utf8_decode($servicios->montasa),
                        utf8_decode($servicios->subtotal),
                        utf8_decode($servicios->moniva),
                        utf8_decode($servicios->monord),
                    )
    
                );

            } elseif($status == 'TODAS'){

                if($servicios->afepre == 'A' || $servicios->afepre == 'N'){
                   
                    $pdf->Row(
                
                        array(
                            utf8_decode($servicios->ordcom),
                            utf8_decode($servicios->desord),
                            utf8_decode($servicios->created_at),
                            utf8_decode($servicios->afepre == 'A' ? 'APROBADA' : 'ANULADA' ),
                            utf8_decode($servicios->proveedor->nompro),
                            utf8_decode($servicios->ccosto->descen),
                            utf8_decode($servicios->refsol),
                            utf8_decode(''),
                            utf8_decode($servicios->montasa),
                            utf8_decode($servicios->subtotal),
                            utf8_decode($servicios->moniva),
                            utf8_decode($servicios->monord),
                        )
        
                    );

                } 

            }
                
        }

        $pdf->Output('I','Reporte de Ordenes de Servicio-'.$fecha.'pdf');

    }

    /**
     *  a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $numero = $this->getNumero($request->tipord);
        //dd($numero);
            $req = Caordcom::find($request->id);
            if(!$req){
                try {
                    DB::beginTransaction();

                    $ordenes = Caordcom::create([
                        'ordcom' => $numero,
                        'fecord' => $request->date,
                        'codpro' => $request->codpro,
                        'desord' => $request->desord,
                        'monord' => $request->monord, 
                        'codcen' => $request->codcen,
                        'tipord' => $request->tipord,
                        'staord' => 'U',
                        'afepre' => 'N',
                        'conpag' => $request->conpag,
                        'forent' => $request->forent,
                        'tipmon' => $request->tipmon,
                        'valmon' => $request->valmon,
                        'codemp' => $request->codemp,
                        'doccom' => $request->doccom,
                        'tipfin' => $request->tipfin,
                        'motanu' => '',
                        'usuroc' => $request->usuroc,
                        'refsol' => $request->numped ?? '',
                        'tasa'   => $request->tasa,
                        'montasa'=> $request->montasa,
                        'tasaid' => $request->tasaid,
                        'subtotal' => $request->subtotal,
                        'moniva' => $request->moniva,
                    ]);

                    if($request->selArts && count($request->selArts) > 0){
                        foreach($request->selArts as $key => $value) {
                            $articulos[] = [
                                'ordcom' => $numero,
                                'codart' => $value["codart"],
                                'canrec' => 0.00,
                                'unimed' => $value["unimed"],
                                'desart' => $value["desart"],
                                'codcat' => '',
                                'canord' => floatval($value["canord"]),
                                'totart' => floatval($value["montot"]), 
                                'preart' => floatval($value["cosult"]),
                                'dtoart' => floatval($value["dtoart"]),
                                'rgoart' => floatval($value["rgoart"]),
                                'reqart' => $request->numped ?? '',
                                'montasa' => floatval($value["montasa"]),
                            ];
                        }
                    }

                    if($request->numped !== null){
                        $casolart = Casolart::where('reqart', $request->numped)->first();

                        $casolart->aprreq = 'O';

                        $casolart->save();
                    }

                    Caartord::insert($articulos); 

                    DB::commit();

                    $data = Caordcom::where('ordcom', $numero)->with('ccosto', 'articulos', 'solicitante')->first();
                    $correo = $request->correo;
                    $message = 'Información suministrada correctamente. ';

                    try {
                        $this->gerMail('jpinto@vit.gob.ve', $data);
                        $correoEnviado = true;
                        $message = $message."Correo enviado con éxito";
                    } catch (\Throwable $th) {
                        $correoEnviado = false;
                        $message = $message."No se pudo enviar el correo";
                    }

                    return response()->json([
                        'success' => true,
                        'message'=> $message,
                        'redirect' => route('Ordenes'),
                    ]);

                } catch (\Throwable $th) {
                    throw $th;
                    DB::rollback();
                    return response()->json(['error' => true,
                'Error al registrar orden'], 200);
                }
            } else {
                try {
                    
                        $req->ordcom = $request['numero'];
                        $req->fecord = $request['date'];
                        $req->codpro = $request['codpro'];
                        $req->desord = $request['desord'];
                        $req->monord = $request['monord'];
                        $req->codcen = $request['codcen'];
                        $req->tipord = $request['tipord'];
                        $req->staord = 'U';
                        $req->afepre = 'N';
                        $req->conpag = $request['conpag'];
                        $req->forent = $request['forent'];
                        $req->tipmon = $request['tipmon'];
                        $req->valmon = $request['valmon'];
                        $req->codemp = $request['codemp'];
                        $req->doccom = $request['doccom'];
                        $req->tipfin = $request['tipfin'];
                        $req->motanu = '';
                        $req->usuroc = $request['usuroc'];
                        $req->refsol = $request["numped"] ?? '';
                        $req->tasa   = $request['tasa'];
                        $req->montasa= $request['montasa'];
                        $req->tasaid = $request['tasaid'];
                        $req->subtotal = $request['subtotal'];
                        $req->moniva = $request['moniva'];

                        $req->save();

                    if($request->selArts && count($request->selArts) > 0){
                        foreach($request->selArts as $key => $value) {
                            $articulos[] = [
                                'ordcom' => $request->numero,
                                'codart' => $value["codart"],
                                'canrec' => 0.00,
                                'unimed' => $value["unimed"],
                                'desart' => $value["desart"],
                                'canord' => floatval($value["canord"]),
                                'totart' => floatval($value["montot"]), 
                                'preart' => floatval($value["cosult"]),
                                'dtoart' => floatval($value["dtoart"]),
                                'rgoart' => floatval($value["rgoart"]),
                                'reqart' => $request->numped ?? '',
                                'montasa' => floatval($value["montasa"]),
                            ];
                        }
                    }

                    if(!empty($request->numped)){
                        $casolart = Casolart::where('reqart', $request->numped)->first();
                        $casolart->aprreq = 'O';

                        $casolart->save();
                    }

                        Caartord::where('ordcom', $request->numero)->delete();
                        Caartord::insert($articulos);

                        return response()->json([
                            'success' => true,
                            'message'=> 'Orden actualizada con éxito',
                            'redirect' => route('Ordenes'),
                        ]);

                } catch (\Throwable $th) {
                    throw $th;
                    DB::rollback();
                    return response()->json(['error' => true,
                    'Error al guardar orden'], 200);
                }
            }

    }


    public function getPendientes()
    {
        $items = Caordcom::where('staord', 'P')->get(); //Cambiar a P para el estado de Pendiente

        return response()->json($items, 200);
    }

    public function save(Request $request)
    {

        #dd($request);
        $req = Caordcom::find($request->id);
        if($req){
            
            try {
                $req->ordcom = $request['numero'];
                $req->fecord = $request['date'];
                $req->codpro = $request['codpro'] ?? '';
                $req->desord = $request['desord'] ?? '';
                $req->monord = $request['monord'] ?? 0;
                $req->codcen = $request['codcen'] ?? '';
                $req->tipord = $request['tipord'] ?? '';
                $req->staord = 'P';
                $req->afepre = 'P';
                $req->conpag = $request['conpag'] ?? '';
                $req->forent = $request['forent'] ?? '';
                $req->tipmon = $request['tipmon'] ?? '';
                $req->valmon = $request['valmon'] ?? 0;
                $req->codemp = $request['codemp'];
                $req->doccom = $request['doccom'] ?? '';
                $req->tipfin = $request['tipfin'] ?? '';
                $req->motanu = '';
                $req->usuroc = $request['usuroc'];
                $req->refsol = $request['numped'] ?? '';
                $req->tasa   = $request['tasa'] ?? 0;
                $req->montasa= $request['montasa'] ?? 0;
                $req->tasaid = $request['tasaid'] ?? '';
                $req->subtotal= $request['subtotal'] ?? 0;
                $req->moniva = $request['moniva'] ?? 0;

                $req->save();

            if($request->selArts && count($request->selArts) > 0){
                foreach($request->selArts as $key => $value) {
                    $articulos[] = [
                         'ordcom' => $request->numero ?? '',
                         'codart' => $value["codart"] ?? '',
                         'canrec' => 0.00,
                         'unimed' => $value["unimed"] ?? '',
                         'desart' => $value["desart"] ?? '',
                         'canord' => floatval($value["canord"]) ?? 0,
                         'totart' => floatval($value["montot"]) ?? 0, 
                         'preart' => floatval($value["cosult"]) ?? 0,
                         'dtoart' => floatval($value["dtoart"]) ?? 0,
                         'rgoart' => floatval($value["rgoart"]) ?? 0,
                         'reqart' => $request->numped ?? '',
                         'montasa' => floatval($value["montasa"]) ?? 0,
                    ];
                }
            }

                Caartord::where('ordcom', $request->numero)->delete();
                Caartord::insert($articulos);

                return response()->json([
                    'success' => true,
                    'message'=> 'Orden actualizada con éxito',
                    'redirect' => route('Ordenes'),
                ]);

            } catch (\Throwable $th) {
                //throw $th;
                DB::rollback();
                throw $th;
                return response()->json(['error' => true,
                'Error al guardar orden'], 200);
            }
            
            

        } else{

        try {
            DB::beginTransaction();

            $ordenes = Caordcom::create([
                'ordcom' => $request->numero,
                'fecord' => $request->date,
                'codpro' => $request->codpro ?? '',
                'desord' => $request->desord ?? '',
                'monord' => $request->monord ?? 0, 
                'codcen' => $request->codcen ?? '',
                'tipord' => $request->tipord ?? '',
                'staord' => 'P',
                'afepre' => 'P',
                'ordcom' => '',
                'conpag' => $request->conpag ?? '',
                'forent' => $request->forent ?? '',
                'tipmon' => $request->tipmon ?? '',
                'valmon' => $request->valmon ?? 0,
                'codemp' => $request->codemp,
                'doccom' => $request->doccom ?? '',
                'tipfin' => $request->tipfin ?? '',
                'motanu' => '',
                'usuroc' => $request->usuroc,
                'refsol' => $request->numped ?? '',
                'tasa'   => $request->tasa ?? 0,
                'montasa' => $request->montasa ?? 0,
                'tasaid' => $request->tasaid ?? '',
                'subtotal' => $request->subtotal ?? 0,
                'moniva' => $request->moniva ?? 0,
            ]);

            if($request->selArts && count($request->selArts) > 0){
                foreach($request->selArts as $key => $value) {
                    $articulos[] = [
                        'ordcom' => $request->numero ?? '',
                        'codart' => $value["codart"] ?? '',
                        'canrec' => 0.00,
                        'codcat' => '',
                        'unimed' => $value["unimed"] ?? '',
                        'desart' => $value["desart"] ?? '',
                        'canord' => floatval($value["canord"]) ?? 0,
                        'totart' => floatval($value["montot"]) ?? 0, 
                        'preart' => floatval($value["cosult"]) ?? 0,
                        'dtoart' => floatval($value["dtoart"]) ?? 0,
                        'rgoart' => floatval($value["rgoart"]) ?? 0,
                        'reqart' => $request->numped ?? '',
                        'montasa' => floatval($value["montasa"]) ?? 0,
                    ];
                }
            }

            Caartord::insert($articulos); 

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => 'Orden guardada con éxito',
                'redirect' => route('Ordenes'),
            ]);

            
        } catch (\Throwable $th) {
            throw $th;
            DB::rollback();
            return response()->json(['error' => true,
            'Error al guardar Orden'], 200);
        }
        }
        

    }

    public function gerMail($email, $data){ //Función de envío de correo a gerente
        
        Mail::to($email)->send(new OrdenesMail($data));
        return response()->json('Correo enviado', 200);

    }

    public function getData() //Envío de datos
    {

        $provs = Caprovee::get();
        $tasa = Fatasacamb::with('moneda','moneda2')->whereHas('moneda', function($q){
            $q->where('nombre','=','DOLLAR')->orWhere('nombre','=','DOLAR');
        })->where('activo','=',true)->first();  
        return response()->json([
            'proveedores' => $provs,
            'tasa' => $tasa,
        ], 200);
    }

    public function getArticulosPedido(Request $request)
    {
        
        $items = Caartsol::where('reqart', $request->reqart)->get();

        return response()->json($items, 200);
    }

    public function getPedidos()
    {
        $items = Casolart::where('stareq', 'A')->where('aprreq', 'G')->get(); //Cambiar a P para el estado de Pendiente

        return response()->json($items, 200);
    }

    
    public function getArticulos(Request $request){

        if($request->consulta === 'S'){
            $querys = Caregart::where('tipreg', null)->where('tipo', 'S')->select('codart', 'desart', 'cosult', 'unimed', 'exitot')->whereNull('deleted_at')
            ->with('color', 'facategoria', 'subcategoria', 'demoProducto', 'moneda')->get();
        } else {
            $querys = Caregart::where('tipreg', null)->where('tipo', 'A')->select('codart', 'desart', 'cosult', 'unimed', 'exitot')->whereNull('deleted_at')
            ->with('color', 'facategoria', 'subcategoria', 'demoProducto', 'moneda')->get();
        }
        
        return response()->json([
            'articulos' => $querys,
        ], 200);

    }

    public function getNumero($tipo){
        if($tipo === 'S'){
            $doccom = 'O'.$tipo;
            $numero = $doccom.str_pad(Caordcom::where('tipord', 'S')->withTrashed()->count() + 1 ,6, 0, STR_PAD_LEFT);
        } else {
            $doccom = 'O'.$tipo;
            $numero = $doccom.str_pad(Caordcom::where('tipord', 'C')->withTrashed()->count() + 1,6, 0, STR_PAD_LEFT);
        }
        return $numero;
    }

    public function action(Request $request)
    {
        
        $date = Carbon::now()->format('Y-m-d');

        $orden = Caordcom::where('ordcom', $request->ordcom)->first();

        $user = Nphojint::where('codemp', $request->user)->first();

        if($request->param === 'S'){

            $orden->staord = 'A';
            $orden->afepre = 'P';
            $orden->usuapr = $user->cedemp;
            $orden->fecapr = $date;

            $orden->update();

        } else if($request->param === 'N'){
            $orden->staord = $request->param;
            $orden->afepre = 'N';
            $orden->motanu = $request->motanu;
            $orden->usuanu = $user->cedemp;
            $orden->fecanu = $date;
            $orden->fecapr = null;

            $orden->update();
        }

        return response()->json(['success' => true, 'Procesado con éxito']);
    }

    public function sendnota(Request $request)
    {
        $orden = Caordcom::where('ordcom', $request->ordcom)->first();

            $orden->afepre = 'G';
            $orden->save();
        
            $user = Auth::user();
            $created_by = $user->codigoid;
            $sucursal = $user->userSegSuc->codsuc;
            $codsuc_reg = explode('-',$sucursal);
            $codsuc = $codsuc_reg[1];

            if($request->tipord == 'C'){

                $notas = NotaEntrega::where('tipo', 'C')->withTrashed()->count();
                $notas = $notas+1;
                $numero = 'NC'.$codsuc.'-'.$notas;
                $codigoid = $notas.'-'.$codsuc;

            } else {

                $notas = NotaEntrega::where('tipo', 'S')->withTrashed()->count();
                $notas = $notas+1;
                $numero = 'NS'.$codsuc.'-'.$notas;
                $codigoid = $notas.'-'.$codsuc;

            }

        try {
            DB::beginTransaction();

            $notaEntrega = NotaEntrega::create([    
                'numero' => $numero,
                'almdes' => $request->nomalm,
                'created_by' => $created_by,
                'cod_cliente' => $request->cod_cliente, //trae proveedor de orden de compra
                'fatasacamb' => '', // Tasa de cambio de la orden de compra
                'total' => $request->monord, // total de orden
                'observacion' => '',
                'facturado' => false,
                'estatus' => 'P',
                'vencimiento' => $request->vencimiento ?? null,
                'tipo' => $request->tipord, // si es orden de compra o servicio
                'documento' => $request->doccom, // traido de orden de compra doccom
                'descuento' => null, // trae total descuento de articulos orden de compra
            ]);

            if($request->articulos && count($request->articulos) > 0){
                foreach($request->articulos as $key => $value) {

                    $articulos[] = [
                        'codtalla' => '',
                        'coin_name' => '',
                        'descuento_id' => '',
                        'cambio' => 0.0,
                        'codart' => $value["codart"],
                        'desart' => $value["desart"],
                        'preunit' => $value["preart"],
                        'cantidad'  => $value["canord"],
                        'canrec' => $value["canrec"],
                        'costo' => $value["totart"],
                        'numero_nota' => $numero,
                        'moneda' => $request->tipmon,
                        'monto_descuento' => $value["dtoart"],
                        'recibido' => $value["recibido"] == "true" ? true : false,
                    ];
                }
            }

            NotaEntregaArt::insert($articulos); 

            DB::commit();

            //Envío de correo
            #where('reqart', $request->reqart)

            // $data = Casolart::where('reqart', $request->numero)->with('ccosto', 'articulos', 'solicitante')->first();
            // $correo = $request->correo;
            // try {
            //     //code...
            //     $this->gerMail('jpinto@vit.gob.ve', $data); //Enviar correo gerente
            // } catch (\Throwable $th) {
            //     //throw $th;
            // return response()->json(['error' => true, 'message' => 'Registrado con éxito, no se pudo enviar el correo']);
                
            // }

            return response()->json([
                'success' => true,
                'message'=> 'Información suministrada correctamente',
                'redirect' => route('Ordenes'),
            ]);

            
        } catch (\Throwable $th) {
            throw $th;
            DB::rollback();
            return response()->json(['error' => true,
        'Error al registrar Nota'], 200);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $ordenes = Caordcom::where('id', $id)->with('ccosto', 'articulos', 'solicitante', 'proveedor', 'condicionPago', 'formaEntrega', 'tipoFinancia', 'tipoMoneda', 'pedido', 'tasa')
        ->first();

        $menus = $this->retornarMenu(); 

        return view('compras.ordenes.ordenShow')->with(
            [
                'ordenes'  => $ordenes,
                'menus' => $menus
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function getAlmacen() //Envío de datos
    {
      $almacenes = Almacen::get();
      
        return response()->json([
            'almacenes' => $almacenes,
        ], 200);
    }

    public function NotaEntrega($id){

        $ordenes = $id;

        //Caordcom::where('id', $id)->with('ccosto', 'articulos', 'solicitante', 'proveedor', 'condicionPago', 'formaEntrega', 'tipoFinancia', 'tipoMoneda')
        //->first();

        $menus = $this->retornarMenu(); 

        return view('compras.ordenes.generarNotaEntrega')->with(
            [
                'ordenes'  => $ordenes,
                'menus' => $menus
            ]);

    }

    public function getOrden(Request $request){

        $ordenFull = Caordcom::where('id', $request->ordenId)->with('ccosto', 'articulos', 'solicitante', 'proveedor', 'condicionPago', 'formaEntrega', 'tipoFinancia', 'tipoMoneda', 'tasa')
        ->first();
        
        return response()->json([
            'ordenFull' => $ordenFull,
        ], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ordenes = Caordcom::findOrFail($id);
        
        $ordenes->delete();

        if($ordenes === null){
            return response()->json([
                'titulo' => 'Hubo Error eliminando'
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Eliminado con éxito',
            'redirect' => route('Ordenes'),
        ]);
    }
}
