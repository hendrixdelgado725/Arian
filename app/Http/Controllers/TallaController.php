<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\User;
use App\Tallas;
use App\Caregart;
use App\Facategoria;
use Helper;
use Exception;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Segususuc;

class TallaController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*Función que ejecuta la inserción de los archivos en la base de datos*/
    public function create(Request $request)
    {   
        $conteotallas = Tallas::withTrashed()->count();
        $conteotallas = $conteotallas+1;
        $conteo = str_pad($conteotallas, 4, 0, STR_PAD_LEFT);
    	$valor=request()->all();
    	$sucuse_reg=Auth::user()->getsucUse()->codsuc;
        $sep=explode('-', $sucuse_reg);
        $request->validate([
            'descripcion'=>'required|string|min:1|max:20',
        ]);
        // dd($request);
        try{

        	$verificar=Tallas::where([['tallas',$valor['descripcion']],['deleted_at',NULL]])->where('codcategoria',$valor['categoria'])->first();
        	if($verificar)
        	{
				Session::flash('error', 'La Talla '.$valor['descripcion'].', se encuentra registrada. Verifique Información');        		
				return redirect()->route('nuevaTalla');	
        	}
        	else{
        		$tal=Tallas::withTrashed()->get()->count();
	        	$codigoid=($tal+1).'-'.$sep[1];

	        	Tallas::create(
	        		[
	        			'tallas'    =>$valor['descripcion'],
	        			'codtallas' => 'A-'.$conteo,
                        'codcategoria' => $valor['categoria'],
	        			'codsuc_reg'=> $sucuse_reg,
	        			'codigoid'  => $codigoid
	        		]
	        	);
	        	Session::flash('success', 'La Talla '.$valor['descripcion'].', se a registrado Exitosamente');
	            return redirect()->route('listaTalla');
        	}
        }catch(Exception $e){
            Session::flash('error', 'Se presentó un problema en el registro, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
            return redirect()->route('listaTalla');
        }
    }

    /*Vista que genera el listado inicial para la carga de la informaci{on*/
    public function listado()
    {
        $menu = new HomeController();
        $talla=Tallas::orderBy('id','DESC')->paginate(8);
        return view('/configuracionGenerales/talla')->with('menus',$menu->retornarMenu())->with('talla',$talla);

    }

    public function edit($id)
    {
        $menu = new HomeController();
        $desc=Helper::desencriptar($id);
        $sep=explode('|', $desc);
        $ID=(int)$sep[1];
        $codig=(string)$sep[2];
        $talla=Tallas::findOrFail($ID);
        $categoria =  Facategoria::all(); 
        $accion = 'edicion';
        return view('/configuracionGenerales/registroEditarTallas')->with('menus',$menu->retornarMenu())->with('talla',$talla)->with( 'categoria', $categoria)->with('accion', $accion);
        
    }

    public function editarTalla(Request $request, $id){

        // $request->validate([
        //     'categoria' => 'required:pgsql2,Tallas',
        //     'codtallas' => 'required:pgsql2,Tallas',
        // ]);
        try {
            
            $verificar=Tallas::where('tallas',$request->all()['codtallas'])->where('codcategoria',$request->all()['categoria'])->first();   
            if($verificar !== null) throw new \Exception("los datos a cambiar ya estan registrado pruebe con otro", 1);
            
            $talla = Tallas::findOrFail($id);
        
        $talla->codcategoria = $request->categoria;
        $talla->tallas = $request->codtallas;
        $talla->update();


        } catch (\Exception $th) {
            Session::flash('error', $th->getMessage());
            return redirect()->route('listaTalla');
        }

        
        
        Session::flash('success', 'Registro Editado con exito.');
        return redirect()->route('listaTalla');

    }

    /*Boton registrar que aparece en el listado*/
    public function nuevaTalla()
    {
    	$menu = new HomeController();
        $accion='registro';
        // $categoria
        return view('/configuracionGenerales/registroEditarTallas')->with([
            'menus'=>$menu->retornarMenu(),
            'accion'=>$accion,
            'categoria'=>Facategoria::all()        ]);
    }

     /*funcion de eliminar el registro*/
    public function delete($id)
    {
    	$desc=Helper::desencriptar($id);
      $sep=explode('|', $desc);
      $ID=(int)$sep[1];
      $codig=(string)$sep[2];
      $talla=Tallas::findOrFail($ID);

      $comprobar= Caregart::where('codtallas','ilike','%'.$talla->codtallas.'%')->first();

      if($comprobar)
      {
      	return response()->json([
                'titulo' => 'Error La talla no puede ser eliminado'
            ]);
      }else
     	{
     		$talla->update(['deleted_at'=>date('Y-m-d h:m:i')]);
     		return response()->json([
                'titulo' => 'La talla se elimino'
            ]);
     	}
      
    }
    /*busqueda de filtro*/
    public function filtro(Request $request)
    {
        $menu = new HomeController();
        $talla= Tallas::where('deleted_at',NULL)->where('tallas','ilike','%'.$request->filtro.'%')->OrWhere('id','ilike','%'.$request->filtro.'%')->paginate(8);
        
        return view('/configuracionGenerales/talla')->with('menus',$menu->retornarMenu())->with('talla',$talla);
    }
}
