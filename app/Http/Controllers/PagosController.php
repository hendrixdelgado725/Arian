<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Yaml\Parser;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\PagoRequest;
use Carbon\Carbon;
use Session;
use App\Empresa;
use App\Bancos;
use App\Fatippag;
use App\Tsdefmon;
use App\Caprovee;
use App\Fatasacamb;
use App\Models\Caordcom;
use App\Models\Cafacpag;
use App\Models\Capagord;
use App\Models\Retenciones;

class PagosController extends Controller
{

    public function retornarMenu()
    {
        $yaml = new Parser();
        $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
        return $menu['menu'];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
        $pagos = Capagord::orderBy('id', 'DESC')->paginate(8);
        $menu = $this->retornarMenu();
        return view('compras.pago.pagos')->with(['pagos' => $pagos, 'menus' => $menu]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $year = Carbon::now()->format('Y');
        $conteo = Capagord::withTrashed()->count() + 1;
        $numero = 'PO' . str_pad($conteo, 6, 0, STR_PAD_LEFT);
        $tipos = Fatippag::get();
        $monedas = Tsdefmon::get();
        $menu = $this->retornarMenu();
        $ivas = Retenciones::where('destip', 'ilike', '%' . 'IVA' . '%')->whereRaw('LENGTH(destip) < 9')->get();
        $bancos = Bancos::get();
        $tasa = Fatasacamb::with('moneda', 'moneda2')->whereHas('moneda', function ($q) {
            $q->where('nombre', '=', 'DOLLAR')->orWhere('nombre', '=', 'DOLAR');
        })->where('activo', '=', true)->first();
        #dd($bancos);
        return view('compras.pago.pagoCreate')
            ->with([
                'menus' => $menu,
                'numero' => $numero,
                'tipos' => $tipos,
                'monedas' => $monedas,
                'ivas' => $ivas,
                'bancos' => $bancos,
                'tasa' => $tasa,
            ]);
    }

    public function getData() //Envío de datos
    {
        $provs = Caprovee::get();
        return response()->json([
            'proveedores' => $provs,
        ], 200);
    }

    public function loadOrdenes()
    {
        $ordenes = Caordcom::where('staord', 'A')->where('afepre', 'P')->with('articulos', 'solicitante', 'ccosto', 'pedido', 'tasa', 'proveedor')->get();

        return response()->json($ordenes, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PagoRequest $request)
    {
        //
        $orden = Caordcom::where('ordcom', $request->numord)->with('proveedor')->first();

        $date = Carbon::now()->format('Y-m-d');

        DB::beginTransaction();
        try {

            $pagos = Capagord::insert([
                'numpag' => $request->numpag,
                'numord' => $orden->ordcom,
                'despag' => $request->despag,
                'fecemi' => $date,
                'cedrif' => $orden->proveedor->codpro,
                'nomben' => $orden->proveedor->nompro,
                'desord' => $orden->desord,
                'numref' => $request->refpag ?? '',
                'ctaban' => $request->banco ?? '',
                'status' => 'E',
                'monord' => $orden->monord,
                'montot' => $request->montot,
                'mondes' => $request->mondes,
                'monexe' => $request->monexe,
                'monbas' => $request->monbas,
                'moniva' => $request->moniva,
                'monret' => $request->monret,
                'monpag' => $request->monpag,
                'monipb' => $request->monipb ?? null,
                'tasa' => floatval($request->tasa),
                'montasa' => $request->totaltas,
                'tasaid' => $request->tasaid,
                'fecpag' => $date,
                'desanu' => '',
                'loguse' => Auth::user()->loguse,
                'usuarioreg' => Auth::user()->cedemp,
                'codmon' => '',
                'valmon' => 0,
                'obspag' => $request->numped ?? '',
            ]);

            if ($request->facturas && count($request->facturas) > 0) {
                foreach ($request->facturas as $key => $value) {
                    $facturas[] = [
                        'numop' => $key + 1,
                        'numpag' => $request->numpag,
                        'numord' => $orden->ordcom,
                        'numfac' => $value["numfac"],
                        'numctr' => $value["numctr"],
                        'fecfac' => $value["fecfac"],
                        'fecrecfac' => $value["fecrecfac"],
                        'rifalt' => $value["rifalt"],
                        'tiptra' => $value["tiptra"] ?? 01,
                        'porret' => $value["porret"],
                        'totfac' => floatval($value["totfac"]),
                        'exeiva' => floatval($value["exeiva"]),
                        'mondes' => floatval($value["desfac"]),
                        'basimp' => floatval($value["basimp"]),
                        'poriva' => floatval($value["moniva"]),
                        'moniva' => floatval($value["impiva"]),
                        'monret' => floatval($value["monret"]),
                        'monpag' => floatval($value["monpag"]),
                        'monipb' => floatval($value["monipb"]) ?? null,
                        'tasa'   => $request->tasa,
                        'montasa'   => $value["montas"],
                        'tasaid' => $request->tasaid,
                    ];
                }
            }

            Cafacpag::insert($facturas);

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => 'Pago guardado con éxito',
                'redirect' => route('pagos'),
            ]);
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
            return response()->json([
                'error' => true,
                'Error al guardar Pago'
            ], 200);
        }
    }

    public function generarReportePagos($id){

        $pagosPDF =  Capagord::with('facturas.proveedor', 'orden', 'banco', 'beneficiario', 'register', 'tasa')->find($id);
        $empresa = Empresa::first();
             
        if(!$pagosPDF){
                Session::flash('error', 'No existen registros con los parametros de busqueda ingresados');
                return redirect()->route('pagos');
        }

        $pdf = new generadorPDF();

        $titulo = utf8_decode("Datos del Agente de Retención");
        $fecha = date('d-m-Y');
        $month = date('m');
        $year = date('Y');

        $pdf->AddPage('L');
        $pdf->setFont("Arial","",7);
        $pdf->cell(70,3,"");

        $pdf->SetFillColor(190,190,190);
        $pdf->Rect(170,45,45,12,"DF");
        $pdf->Rect(225,45,45,12);
        $pdf->setFont("Arial","",7);
        $pdf->ln(-1);
        $pdf->cell(168,7,"");
        $pdf->cell(58,0,"Numero Comprobante:");
        $pdf->cell(50,0,utf8_decode("Fecha de Emisión: "));
        $pdf->setFont("Arial","B",8);
        $pdf->ln(0);
        $pdf->cell(229,8,"");
        $pdf->cell(50,8,$fecha);
        // $pdf->setFont("Arial","B",10);
        // $pdf->setxy(236,53);
        // $pdf->cell(30,0,$fecha);
        $pdf->ln(11);

        $pdf->setFont("Arial","B",8);
        $pdf->setX(105);
        $pdf->cell(80,5, $titulo);
        $pdf->ln();
        $pdf->Rect(10,$pdf->GetY(),100,8);
        $pdf->Rect(120,$pdf->GetY(),85,8);
        $pdf->Rect(210,$pdf->GetY(),60,13);
        $pdf->Rect(10,$pdf->GetY()+10,195,11);
        $pdf->ln(1);
        $pdf->cell(1,5,"");
        $pdf->cell(110,5,"Nombre: ". $empresa->nomrazon);
        $pdf->cell(150,5,"No. R.I.F.: ". $empresa->codrif);
        $pdf->ln(-1);
        $pdf->cell(218,5,"");
        $pdf->cell(20,5,"Periodo Fiscal");
        $pdf->ln(6);
        $pdf->cell(203,5,"");
        $pdf->cell(33,5,utf8_decode("Año: ". $year));
        $pdf->cell(20,5,utf8_decode("Mes: ". $month));
        $pdf->ln(6);
        $pdf->cell(1,5,"");
        $pdf->cell(110,4,"Direccion: ". $empresa->dirfis);
        $pdf->ln(10);
        $pdf->cell(99,5,"");
        $pdf->cell(50,5,"Datos del Contribuyente");
        $pdf->Rect(10,$pdf->GetY()+5,100,15);
        $pdf->Rect(120,$pdf->GetY()+5,85,15);

        $pdf->ln();
        $pdf->cell(2,5,"");
        $pdf->cell(110,5,"Nombre o Razon Social: ");
        $pdf->cell(50,5,"RIF. del Sujeto Retenido: ");
        $pdf->ln(0);
        $pdf->cell(28,8,"");
        $pdf->cell(115,16,$pagosPDF->beneficiario->nompro);
        $pdf->cell(50,16,$pagosPDF->beneficiario->rifpro);
        $pdf->ln(17);
        $pdf->Rect(189,$pdf->GetY(),65,5,"DF");
        $pdf->Rect(10,$pdf->GetY()+5,265,9,"DF");

        $pdf->cell(175,5,"");
        $pdf->cell(70,5,"Compras Internas o Importaciones",0,0,'C');
        $pdf->ln();
        $pdf->setFont("Arial","",8);
        $pdf->cell(15,9,"Nro OP",1);
        $pdf->cell(26,9,"Fecha de Factura",1);
        $pdf->cell(28,9,"Numero de Factura",1);
        $pdf->cell(35,9,"Nro. Control de Factura",1);
        $pdf->cell(25,9,"Total Facturado",1);
        $pdf->cell(50,9,"Compras sin derecho a Credito IVA",1);
        $pdf->cell(25,9,"Base Imponible",1);
        $pdf->cell(18,9,"% Alicuota",1);
        $pdf->cell(22,9,"Impuesto IVA",1);
        $pdf->cell(21,9,"IVA Retenido",1);

        $pdf->Ln(10);
        $pdf->SetWidths(array(15,26,28,35,25,50,25,18,22,21));
        $pdf->SetAligns(['C','C','C','C','C','C','C','C','C','C']);
        $pdf->SetFont('arial','',8,5);

        foreach($pagosPDF->facturas as $facturaItems){

            $pdf->Row(
                    
                array(
                    utf8_decode($facturaItems->numop),
                    utf8_decode($facturaItems->fecfac),
                    utf8_decode($facturaItems->numfac),
                    utf8_decode($facturaItems->numctr),
                    utf8_decode($facturaItems->totfac),
                    utf8_decode($facturaItems->exeiva),
                    utf8_decode($facturaItems->basimp),
                    utf8_decode($facturaItems->poriva),
                    utf8_decode($facturaItems->moniva),
                    utf8_decode($facturaItems->monret),
                )

            );

        }

        $pdf->Ln(1);
        $pdf->setFont("Arial","B",8);
        $pdf->cell(104,5,"Totales",0,0,'R');
        $pdf->SetFont('arial','',8,5);
        $pdf->cell(25,4.5,$pagosPDF->montot,1,0,'C');
        $pdf->cell(50,4.5,$pagosPDF->monexe,1,0,'C');
        $pdf->cell(25,4.5,$pagosPDF->monbas,1,0,'C');
        $pdf->cell(18,4.5,"",0,0,'C');
        $pdf->cell(22,4.5,$pagosPDF->moniva,1,0,'C');
        $pdf->cell(21,4.5,$pagosPDF->monret,1,0,'C');   

        $pdf->setFont("Arial","B",8);
        $pdf->line(15,175,81,175);
        $pdf->setxy(21,180);
        $pdf->cell(30,0,utf8_decode("Firma y Sello del Agente de Retención"));
        $pdf->line(110,175,180,175);
        $pdf->setxy(125,180);
        $pdf->cell(30,0,utf8_decode("Firma y Sello del Proveedor"));

        $pdf->line(210,175,265,175);
        $pdf->setxy(225,180);
        $pdf->cell(30,0,utf8_decode("Fecha de Entrega"));
        
        // $pdf->setxy(85,187);
        // $pdf->setxy(125,190);
        // $pdf->cell(25,5,"Firma del Contribuyente");
        // $pdf->line(120,190,165,190);
        // $pdf->setxy(220,190);
        // $pdf->cell(25,5,"Fecha de Entrega: ");
        // $pdf->setxy(210,187);







        

       // $pdf->Ln(5);
        //$pdf->Cell(0,0,utf8_decode('TOTAL: '.$total),0,0,'L');
        // $pdf->Ln(10);
        // $pdf->SetFont('arial','B',8,5);
        // $pdf->SetFillColor(2,157,116);
        // $pdf->Cell(47,8,utf8_decode('SOLICITANTE'),1,0,'C');
        // $pdf->Cell(63,8,utf8_decode('DEPARTAMENTO'),1,0,'C');
        // $pdf->Cell(40,8,utf8_decode('NÚMERO'),1,0,'C');
        // $pdf->Cell(40,8,utf8_decode('FECHA'),1,0,'C');
        // $pdf->Ln();
        // $pdf->SetWidths(array(47,47,47,47,));
        // $pdf->SetAligns(['C','C','C','C','C','C','C','C']);
        // $pdf->SetFont('arial','',8,5);

        // $last=1;
        // $limite=1;
        // $index = 0;
        

        // $pdf->SetWidths(array(47,63,40,40));
        // $pdf->SetAligns(['C','C','C','C','C','C','C','C']);
        // $pdf->SetFont('arial','',8,5);


        // // $pdf->Row(
        // //     array(
        // //         utf8_decode($requisicion->solicitante->nomemp),
        // //         utf8_decode($requisicion->ccosto->descen),
        // //         $requisicion->reqart,
        // //         $requisicion->fecreq,
        // //     ));



        // // $pdf->Ln();
        // // $pdf->SetFont('arial','B',8,5);
        // // $pdf->SetFillColor(2,157,116);
        // // $pdf->Cell(190,6,utf8_decode('JUSTIFICACIÓN'),1,0,'C');
        // // $pdf->Ln();
        // // $pdf->SetWidths(array(190));
        // // $pdf->SetAligns(['C']);
        // // $pdf->SetFont('arial','',8,5);
        // // $pdf->Row(
        // //         array(
        // //             utf8_decode($requisicion->desreq),
        // //     ));



        // $pdf->Ln();
        // $pdf->SetFont('arial','B',9);
        // // $pdf->SetFillColor(2,157,116);
        // $pdf->Cell(190,6,utf8_decode('ARTÍCULOS'),1,1,'C');
        // $limit = (int)280 / 3;
        // $pdf->Cell(110,6,utf8_decode('DESCRIPCIÓN'),1,0,'C');
        // $pdf->Cell(40,6,utf8_decode('UNIDAD DE MEDIDA'),1,0,'C');
        // $pdf->Cell(40,6,utf8_decode('CANT. REQUERIDA'),1,1,'C');
        // /* $pdf->Cell(40,6,utf8_decode('COSTO'),1,0,'C');
        // $pdf->Cell(40,6,utf8_decode('RECARGO'),1,0,'C');
        // $pdf->Cell(40,6,utf8_decode('DESCUENTO'),1,0,'C');
        // $pdf->Cell(40,6,utf8_decode('TOTAL'),1,1,'C');  */
        // $pdf->SetWidths(array(110,40,40));
        // $pdf->SetAligns(['C','C','C']);
        // $pdf->SetFont('arial','',9);
       
    //     foreach($requisicion->articulos as $key => $articulo){


    //         $pdf->Row(
    //             array(
    //                 utf8_decode($articulo->desart),
    //                 utf8_decode($articulo->unimed),
    //                 $articulo->canreq, 
    //                /*  $articulo->costo,
    //                 $articulo->monrgo,
    //                 $articulo->mondes,
    //                 $articulo->montot */
    //             ));
                
    //         }
    //         $pdf->Ln(20);
    //         $pdf->SetFont('arial','B',10);
    //         $pdf->Cell(0,0,utf8_decode('RESPONSABLES'),0,0,'C'); 
    //         $pdf->SetFont('arial','B',8,5);
    //         $pdf->SetFillColor(2,157,116);
    //         $pdf->Ln(10);
    //         $pdf->Cell(0,0,utf8_decode('Solicitado Por:'),0,1,'L'); 
    //         $pdf->Cell(0,0,utf8_decode('Aprobado Por:'),0,1,'R'); 
    //         $pdf->Ln(10);
    //         $pdf->Cell(40,8,utf8_decode('SOLICITANTE'),1,0,'L');
    //         $pdf->cell(110,0, '',0,0,'C');
    //         $pdf->Cell(40,8,utf8_decode('APROBADO'),1,0,'R');
    //         $pdf->Ln(8);
    //         $pdf->cell(40,24,'',1,0,'L'); //firma
    //         $pdf->cell(110, 0, '', 0,0, 'C');
    //         $pdf->cell(40,24,'',1,0,'R'); //firma
    //         $pdf->Ln(20);
    //         $pdf->SetTextColor(0,0,0);
    //         $pdf->Ln(5);
             $pdf->Output('I','PEDIDO-'.$fecha.'pdf');
        
        // exit;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $pago = Capagord::with('facturas.proveedor', 'orden', 'banco', 'beneficiario', 'register', 'tasa')->find($id);
        $menus = $this->retornarMenu();
        return view('compras.pago.pagoEdit')->with(['pago' => $pago, 'menus' => $menus]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
