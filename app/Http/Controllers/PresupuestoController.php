<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PresupuestoRequest;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\Caregart;
use App\Facliente;
use App\Fatasacamb;
use App\Fatippag;
use App\Farecarg;
use App\Fadescto;
use App\Famoneda;
use App\Fadurpre;
use App\Fapresup;
use App\Almacen;
use App\Fapreserart;
use App\Sucursal;
use App\Fasuc_rgo;
use App\Tallas;
use Illuminate\Support\Facades\Auth;
use Helper;
use User;
use Exception;
use Session;
use Carbon\Carbon;
use \Fpdf;
use App\Http\Controllers\generadorPDF;
use App\Segususuc;

class PresupuestoController extends Controller
{

    protected $id = null;
    public function __construct()
    {
        $this->middleware('auth');
        
        
    }

    public function index(){

      
        $menu = new HomeController(); 
        $presupuestos = Fapresup::with('moneda','cliente','valido','articulos','detarticulos','fpago'
        ,'recargo','descuento')->where('codsuc', session('codsuc'))->orderBy('id','DESC')->paginate(8);
        $date = date('Y-m-d');

        return view('/configuracionGenerales/presupuesto')->with([
            'menus'=> $menu->retornarMenu(),
            'presupuestos'=>$presupuestos,
            'date'=>$date        ]);
    }

    public function nuevoPresupuesto(){
        $menu = new HomeController();
       // $articulos = caregart::has('costoUnit')->limit(100)->get();
      // return $articulos = caregart::has('costos')->get();
        $articulos = Caregart::has('costos')->whereHas('moneda')->get();
        
        $fpagos = Fatippag::get();
        $tasas = Fatasacamb::with('moneda','moneda2')->where('activo','=',true)->get();

        $recargos = Farecarg::where('status','=','S')->where('deleted_at','=',null)->get();

        // $codsuc = Auth::user()->codsuc_reg;
        // $recargo =  Fasuc_rgo::with('recargo')->whereHas('recargo', function($q) use ($codsuc){
        //     $q->where('codsuc','=',$codsuc)->where('status','=','S');
        //  })->first();
        $descuentos = Fadescto::orderBy('id', 'desc')->whereNotNull('codigoid')->get();

        //$descuentos = Fadescto::get();
        $duraciones = Fadurpre::get();

        $monedas = Famoneda::has('precios')->get();
        
        $tallas = \App\Tallas::get();
        
        return view('/configuracionGenerales/registroPresupuesto')->with([
            'menus'=> $menu->retornarMenu(),
            'articulos'=>$articulos,
            'tasas'=> $tasas,
            'fpagos'=>$fpagos,
            'recargos'=>$recargos,
            'descuentos'=>$descuentos,
            'duraciones'=>$duraciones,
            'monedas'=>$monedas,
            'tallas' => $tallas]);
    }

    public function getArticulos(Request $request)
    {
      $alm = Almacen::where('codsuc_asoc','=',session('codsuc'))->select('codalm')->where('pfactur',true)->first();

        $articulos = Caregart::with('costoPro')->whereHas('costoPro', function($q) use ($request){
          $q->where('status','=', 'A')->where('codmone',$request->codigo);
        })->where('tipreg','F')
        ->whereHas('almacenubi',function($q) use ($alm){
          $q->where('exiact','>',0)->where('codalm',$alm->codalm);
        })->withSum('almacenubi','exiact')->get();
        if(empty($articulos->toArray())) return response()->json(['status' => 'error', 'message' => 'No se encontraron artículos con precio en esta moneda'], 500);
      return response()->json($articulos);
    }

    public function getClientes()
    {
      $clientes = Facliente::where('codsuc',session('codsuc'))->orderBy('nompro')->get();

      return response()->json($clientes);
    }

    
    public function create (PresupuestoRequest $request){
   try{
        $cont = Fapresup::withTrashed()->get()->count();
        $cont = $cont +1;

        $prefijo = 'PRE';
        $guion = '-';
        $suc = Auth::user()->codsuc_reg;
        $codsuc_reg = explode('-',$suc); 
        $cod = $cont.$guion.$codsuc_reg[1];
        $año = date('Y');
        $mes = date('m');
        $dia = date('d');
        $terminacion = '00';
        $sepduracion  = explode(' ',$request->duracion);
        $days = $sepduracion[0];
        $duracion_id = Fadurpre::where('duracion','=',$request->duracion)->first()->codigoid;
        $ldate = date('Y-m-d H:i:s');
        $vence = date('Y-m-d H:i:s', strtotime($ldate.' + '.$days.' days'));
        $num = 1;

        $recarga = Farecarg::where('codrgo','=',$request->recargo)->first();
        $recarga_id = $recarga->id;

   }
   catch(Exception $e){
    dd($e);
            \Log::info($e->getMessage());
            Session::flash('error', $e->getMessage());
        return redirect()->route('nuevoPresupuesto');


  }
      try {
        
        
        $autor = Auth::user()->nomuse;
        $cedaut = Auth::user()->cedemp;
        $sucursal = Sucursal::where('codsuc', Auth::user()->userSegSuc->codsuc)->first();

        $string = $sucursal->nomsucu;
        $words = str_word_count($string, 1, 'UTF-8');
        $initials = array();
        foreach ($words as $word) {
            $initials[] = mb_substr($word, 0, 1, 'UTF-8');
        }
        $result = implode('', $initials);
        $existepre = Fapresup::where('codsuc', $sucursal->codsuc)->withTrashed()->get();
        if(!$existepre->isEmpty()){
           $cont2 = $existepre->count();
           $num = $cont2 + 1;
        }

        $conteo = str_pad($num, 4, 0, STR_PAD_LEFT);

        $nropre = $prefijo.$año.$mes.$dia.$guion.$result.$conteo;
        $presupuesto = new  Fapresup();
        if($request->codigoTasa){
          $tasa = Fatasacamb::with('moneda','moneda2')->find($request->codigoTasa);
          if($tasa->moneda2->codigoid == $request->codmon){
             $CambioDtotal = $request->totalPre/$tasa->valor;
             $cambio = $CambioDtotal;
             $presupuesto->fatasacamb_id = $tasa->codigoid;
             $presupuesto->monto_tasa = $cambio;
          }
          else{
             $CambioDtotal = $request->totalPre*$tasa->valor;
             $cambio = $CambioDtotal;
             $presupuesto->fatasacamb_id = $tasa->codigoid;
             $presupuesto->monto_tasa = $cambio;
          }
         
        }
        else {
          $presupuesto->fatasacamb_id = '';
          $presupuesto->monto_tasa = 0;
        }

          $conteo = Fapresup::withTrashed()->count() + 1;
          $refpre = $prefijo . str_pad($conteo, 5, 0, STR_PAD_LEFT);
          $date = Carbon::now()->format('Y-m-d');

            $presupuesto->refpre = $refpre;
            $presupuesto->fecpre = $date;
            $presupuesto->codpre = $nropre;
            $presupuesto->codcli = $request->codpro;
            $presupuesto->moneda_id = $request->codmon;
            $presupuesto->mondesc = floatval($request->descuentoPre);
            $presupuesto->farecarg_id = $recarga_id;
            $presupuesto->monrgo = floatval($request->ivaPre);
            $presupuesto->monpre = floatval($request->totalPre);
            $presupuesto->subtotalpre = floatval($request->subtotalPre);
            $presupuesto->duracion_id = $duracion_id;
            $presupuesto->vencimiento = $vence;
            $presupuesto->autor = $autor;
            $presupuesto->codsuc= $sucursal->codsuc;
            $presupuesto->estatus = "PENDIENTE";
            $presupuesto->codigoid = $cod;
            $presupuesto->facturado = false;
            $presupuesto->cedaut = $cedaut;
            $presupuesto->save();

            if ($request->arts && count($request->arts) > 0) {
              foreach ($request->arts as $key => $value) {
                  $arts[] = [
                    'refpre' => $refpre,
                    'codpre' => $nropre,
                    'codart' => $value["codart"],
                    'desart' => '',
                    'cantart' => $value["cantart"],
                    'talla' => 'N/A',
                    'codtalla' => 'N/A',
                    'preunit' => floatval($value["precio"]),
                    'codsuc' => $suc,
                    'monto_descuento' => floatval($value["mondes"]),
                    'codigoid' => '',
                    'descuento_id' => '',
                    'descuento' => '',
                    'totart' => floatval($value["totart"]),
                  ];
              }
          }

          Fapreserart::insert($arts);


         /* while ($i < $lenght) {
        
          $art = Caregart::where('codart','=',$request->arts[$i]["codart"])->with('costoUnit')->first();
          $precio = $art->costoUnit->pvpart*$request->arts[$i]["cantart"];
          $preunit = $art->costoUnit->pvpart;
          $conter = Fapreserart::withTrashed()->get()->count();
          $conter = $conter+1;
          $codsuc_reg = explode('-',$suc);
          $codi = $conter.$guion.$codsuc_reg[0];
          $codart = $art->codart;
          $desart = $art->desart;
          $detalleArticulo = new Fapreserart;
          $des = $request->arts[$i]["descto"];
          $descuento = Fadescto::where('desdesc','=',$des)->first();
                if($descuento->mondesc!="0"){
                    $descuentoPreArt = $precio * $descuento->mondesc/100;
                    $descuentoId= $descuento->codigoid;
                    $descuentoNom = $descuento->desdesc;
                    $detalleArticulo->monto_descuento = $descuentoPreArt ?? '0';
                    $detalleArticulo->descuento_id = $descuentoId;
                    $detalleArticulo->descuento = $descuentoNom;
                } else{
                  $detalleArticulo->monto_descuento = 0;
                  $detalleArticulo->descuento_id = '';
                  $detalleArticulo->descuento = 'SIN DESCUENTO';
                }
                
                $detalleArticulo->refpre = $refpre;
                $detalleArticulo->codpre = $nropre;
                $detalleArticulo->codart = $codart;
                $detalleArticulo->desart = $desart;
                $detalleArticulo->cantart = $request->arts[$i]["cantart"];
                $detalleArticulo->talla = 'N/A';
                $detalleArticulo->codtalla = 'N/A';
                $detalleArticulo->preunit = $preunit;
                $detalleArticulo->codigoid = $codi;
                $detalleArticulo->codsuc = $suc;
                $detalleArticulo->save();
                
                
          $i++;
         };
 */
      } catch (Exception $e) {
        dd($e);
        \Log::info($e->getMessage());
        Session::flash('error', $e->getMessage());
        return redirect()->route('nuevoPresupuesto');
      }

      Session::flash('success', 'Registro creado con exito.');
      return response()->json([
          'success' => true,
          'message' => 'Información suministrada correctamente',
          'redirect' => route('listaPresupuesto'),
      ]);
        
        
    }
    
    public function edit($id){
        
        $menu = new HomeController();
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        
        
        $articulos = Caregart::orderBy('id','DESC')->has('costoUnit')->whereHas('moneda')->get();
        $descuentos = Fadescto::get();
        $presupuesto = Fapresup::where('id','=',$ID)->with('moneda','cliente','valido','articulos.discount','detarticulos','fpago','recargo','descuento','tCambio','cambioMoneda','cambioMoneda2','descuentoArt')->first();
        #dd($presupuesto);
        $tallas = \App\Tallas::get();
        #dd($presupuesto);
        
        return view('/configuracionGenerales/editarPresupuesto')->with([
            'menus'=> $menu->retornarMenu(),
            'presupuesto'=>$presupuesto,
            'articulos'=> $articulos,
            'descuentos'=>$descuentos,
            'arttojson'=> json_encode($articulos),
            'descutojson'=>json_encode($descuentos),
            'tallas' => $tallas,
            'id' => $id
            ]);
       }
/*Falta incremental los calculos */
    public function update(Request $request, $id,$id2){
      
     
      try{
        $arrayTt = array();
        foreach($request->articulos as $key){
          if($key != null){
            array_push($arrayTt,$key);
          }
        }
        $arrayTt = array_count_values($arrayTt);
       
      
       foreach($arrayTt as $key){
         if($key >= 2) throw new Exception("Hay Articulo iguales ingrese uno diferente");
       }

          $i =0;
          $i2 =0;
          $i3 = 0;
          /*foreach($request->tallas as $key => $value) {
          
                if($value === null) $i++;
          }*/
          foreach ($request->cantidad as $key => $value) {
                if($value === null) $i2++;
          }

          // foreach ($request->descuentos as $key => $value) {
                
          //       if($value === "0") $i3++;
          // }
          
          //if($i === count($request->tallas)) throw new Exception("Tiene que agregar una talla", 1);
          
          if($i2 === count($request->cantidad)) throw new Exception("Tiene que agregar una cantidad", 1);

          //if($i3 === count($request->descuentos)) throw new Exception("tienes que agregar Descuentos");

      }catch(\Exception $e){        
      
        Session::flash('error', $e->getMessage());
        return redirect()->route('editarPresupuesto',['id' => $id2]);    
      }


      $in = null;
      
      if($request->cantidad[0] == null) {
        $in = 0;
      }
        $guion = '-';
        $suc = Auth::user()->codsuc_reg;

        $cantidades = array_filter($request->cantidad,array($this,"filtrar"));

        $articulos = array_filter($request->articulos,array($this,"filtrar2"));
        //$tallas = array_filter($request->tallas);
        /*$descuentos = array_filter($request->descuentos);*/
        $descuentos = $request->descuentos;
        #dd($descuentos);
        if($in == 0){
        unset($descuentos[$in]);
        }

        $cantidades = array_values($cantidades);
        $articulos = array_values($articulos);
        $descuentos = array_values($descuentos);
        $presupuestoss = Fapresup::where('id','=',$id)->first();

        if($request->borrados!=null){


            $borrados = explode(',',$request->borrados);
//aqui quede yop
            foreach($borrados as $borrar){
                           
                $artborrar = Fapreserart::find($borrar);
                #dd($artborrar);
                
                $artborrar->delete();
                $presupuestoss->save();
            }
        }

  
        $presupuesto = Fapresup::with('articulos')->find($id);
        $arts = Fapreserart::where('codpre','=',$presupuesto->codpre)->get();
        $recargo = Farecarg::where('nomrgo',$request->recargo)->first();
        
        #dd($presupuesto);      
        $tienedes = false;
        $descuentoTotal = 0;        
        $subtotal = 0;

        $moneda = Famoneda::where('nombre','=',$request->moneda)->first();
        $moneda_id = $moneda->codigoid;

        try{
            foreach(array_combine($articulos, $cantidades) as $articulo => $cantidad){
                $consultaP = Caregart::where('codart','=',$articulo)->with('costos')
                ->whereHas('costos', function($q) use ($moneda_id){
                   $q->where('codmone','=',$moneda_id);
                })->get();
                
                if($consultaP->isEmpty() || $consultaP == null) throw new Exception("Los articulos ingresados no cuentan con precios registrados con la moneda seleccionada");
            } //foreach
           }
           catch(Exception $e){
            #dd($e);
             /* $menu = new HomeController();
             $articulos = Caregart::orderBy('id','DESC')->has('costoUnit')->limit(50)->get();
             $descuentos = Fadescto::get();
             $presupuesto = Fapresup::where('id','=',$id)
             ->with('moneda','cliente','valido','articulos','detarticulos','fpago'
             ,'recargo','descuento','tCambio')->first();
             
             return redirect()->back()->with([
                'error'=> $e->getMessage(),
                'menus'=> $menu->retornarMenu(),
                'articulos'=>$articulos,
                'descuentos'=>$descuentos,
                'presupuesto'=>$presupuesto,
                'arttojson'=> json_encode($articulos),
                'descutojson'=>json_encode($descuentos)                ]); */
                Session::flash('error', $e->getMessage());
                return redirect()->route('editarPresupuesto',['id' => $id2]);    
          }//endcatch
          $a = 0;
          $lenght = count($articulos); 
         # dd($lenght);
          $i = 0;
          $i2 = 0;
          try {
            $existe = null;
            $precio = 0;
            $cont = 0;
            $codsuc_reg = explode('-',$suc);
            while($i < $lenght){
              foreach($arts as $art) {
                
                
                $consulta = Caregart::where('codart','=',$articulos[$i])->with('costoUnit')->first();

                
  
                $precio = $consulta->costoUnit->pvpart*$cantidades[$i];
                #dd($articulos[$i]);
  
                if($art->codart == $consulta->codart  || 
                   $art->codart == $consulta->codart){
                     $cont = 1;
                     $artViejo = Fapreserart::where('codpre','=',$presupuesto->codpre)
                     ->where('codart','=',$art->codart)->first();
                     
                    #dd($artViejo);
                                      
                    /*  if($art->cantart == $cantidades[$i]){
                          $artViejo->cantart = 
                         }
                        else{
                          $artViejo->cantart = $cantidades[$i];
                                          }*/
  
                    if(!$artViejo){
                        $artViejo = Fapreserart::where('codpre','=',$presupuesto->codpre)->where('codart','=',$art->codart)->first();
                        }
                        #dd([$artViejo->cantart, $cantidades[$i]]);
                      $artViejo->cantart = $cantidades[$i];
                     
                      #dd($cantidades);
                      #dd($descuentos[$i]);
                      if($descuentos[$i]==="0" || $descuentos[$i]==='SIN DESCUENTO'){
                          // $i = $i+1
                          $subtotal = $subtotal + $precio;
                          $artViejo->descuento = 'SIN DESCUENTO';
                          $artViejo->monto_descuento = 0;
                          $descuentoTotal = $descuentoTotal + 0;
                          $presupuesto->subtotalpre = $subtotal;
                          $presupuesto->mondesc = $descuentoTotal;
  
                          
                      }else{
  
                          $tienedes = true;
                           
                           if($descuentos[$i]==$artViejo->descuento){
                            
                              $des = $descuentos[$i];
                              $descuento = Fadescto::where('desdesc','=',$des)->first()->mondesc;
                              $descuentoId = Fadescto::where('desdesc','=',$des)->first()->codigoid;
                              $descuentoArt = $precio * $descuento/100;
                              $descuentoTotal = $descuentoTotal+$descuentoArt;
                              $subtotal = $subtotal + $precio;
                              $sacarIva = (($subtotal - $descuentoTotal) * $recargo->monrgo) / 100;
                              $total = ($subtotal - $descuentoTotal) + $sacarIva;
                              $presupuesto->monrgo = $sacarIva;
                              $presupuesto->subtotalpre = $subtotal;
                              $presupuesto->mondesc = $descuentoTotal;
                              $presupuesto->monpre = $total;
                              $presupuesto->monto_tasa = $total;
                              #dd($descuento);
                              
                              $artViejo->descuento = $des;
                              $artViejo->monto_descuento = $descuentoArt;
                              $artViejo->descuento_id = $descuentoId;
                              
                           }
                           else{                   
                              $des = $descuentos[$i];
                              $descuento = Fadescto::where('desdesc','=',$des)->first()->mondesc;
                              $descuentoId = Fadescto::where('desdesc','=',$des)->first()->codigoid;
                              $descuentoArt = $precio * $descuento/100;
                              $descuentoTotal = $descuentoTotal+$descuentoArt;
                              $subtotal = $subtotal + $precio;
                              $sacarIva = (($subtotal - $descuentoTotal) * $recargo->monrgo)/100;
                              $presupuesto->monrgo = $sacarIva;
                              $presupuesto->subtotalpre = $subtotal;
                              $presupuesto->mondesc = $descuentoTotal;
                              $total = ($subtotal - $descuentoTotal) + $sacarIva;
                              $presupuesto->monto_tasa = $total;
                              $presupuesto->monpre = $total;
                              $artViejo->descuento = $des;
                              $artViejo->monto_descuento = $descuentoArt;
                              $artViejo->descuento_id = $descuentoId;
                             
                              
                           }
                          
                      } //else
                      
                           $artViejo->save();   
                          $presupuesto->save();
                         
                    #llegue y modifica la cantidad
                }//if
                
                 
               }
               $existe = Fapreserart::where('codart','=',$articulos[$i])
                ->where('codpre','=',$presupuesto->codpre)->whereNull('deleted_at')->first();
                
                //si no obtiene contador y ademas no tiene registro bajo el presupuesto
                #var_dump($i);
                
                if(is_null($existe)){

                 $artReferencia = Caregart::where('codart','=',$articulos[$i])
                                  ->with('costoUnit')->first();
                                                
                                  
                 $preunit = $artReferencia->costoUnit->pvpart;
                 $codart = $artReferencia->codart;
                 $desart = $artReferencia->desart;
                 $detalleArticulo = new Fapreserart;
                  
                   if($descuentos[$i]=="0"){
                       $subtotal = $subtotal + $precio;
                       $des = 'SIN DESCUENTO';
                       $descuentoArt = 0;
                       $detalleArticulo->descuento = 'SIN DESCUENTO';
                       $detalleArticulo->monto_descuento = 0;
                       $descuentoTotal = $descuentoTotal + 0;
                       $sacarIva = (($subtotal - $descuentoTotal) * $recargo->monrgo)/100;
                       $presupuesto->monrgo = $sacarIva;
                       // $i = $i+1;
                      }
                   else{
                     $tienedes = true;
                     $des = $descuentos[$i2];
                     $descuento = Fadescto::where('desdesc','=',$des)->first()->mondesc;
                     $descuentoId = Fadescto::where('desdesc','=',$des)->first()->codigoid;
                     $descuentoArt = $precio * $descuento/100;
                     $descuentoTotal = $descuentoTotal+$descuentoArt;
                     $subtotal = $subtotal + $precio;
                     $sacarIva = (($subtotal - $descuentoTotal) * $recargo->monrgo)/100;
                     $presupuesto->monrgo = $sacarIva;
                     $presupuesto->subtotalpre = $subtotal;
                     $presupuesto->mondesc = $descuentoTotal;
                    /*  $detalleArticulo->descuento = $des;
                     $detalleArticulo->monto_descuento = $descuentoArt;
                     $detalleArticulo->descuento_id = $descuentoId; */
                     // dd($descuentoTotal);
                     }
                 $conter = Fapreserart::withTrashed()->get()->count();
                 $conter = $conter+1;
                 $codi = $conter.$guion.$codsuc_reg[1];
                 //$tall = Tallas::where('codtallas','=',$tallas[$i])->first();

                 /* $detalleArticulo->refpre = $presupuesto->refpre;
                 $detalleArticulo->codpre = $presupuesto->codpre;
                 $detalleArticulo->codart = $codart;
                 $detalleArticulo->desart = $desart;
                 $detalleArticulo->cantart = $cantidades[$i];
                 $detalleArticulo->preunit = $preunit;
                 $detalleArticulo->codigoid = $codi;
                 $detalleArticulo->codsuc = $suc;
                if($tallas[$i] != 'NADA'){
                    $detalleArticulo->talla = $tall->tallas ?? 'N/A';
                    $detalleArticulo->codtalla = $tall->codtallas ?? 'N/A';
                 } */

                  $talla =  'N/A';
                  $codtalla = 'N/A';
               
                 #dd($detalleArticulo);
                 $fapresart = Fapreserart::create([
                  'descuento' => $des,
                  'monto_descuento' => $descuentoArt,
                  'descuento_id' => $descuentoId,
                  'refpre' => $presupuesto->refpre,
                  'codpre' => $presupuesto->codpre,
                  'codart' => $codart,
                  'desart' => $desart,
                  'cantart' => $cantidades[$i],
                  'preunit' => $preunit,
                  'codigoid' => $codi,
                  'codsuc' => $suc,
                  'talla' => $talla,
                  'codtalla' => $codtalla,

                 ]);
                 #dd($fapresart);
                 #$detalleArticulo->create();
                 $presupuesto->save();
                  } //if
               $i++;
            }

            
             # die;
          } catch (Exception $e) {
            Session::flash('error', $e->getMessage());
            return redirect()->route('editarPresupuesto',['id' => $id2]);    
          }
        
          Session::flash('success', 'Se ha Editado el presupuesto');
        return redirect()->route('listaPresupuesto');

    } //update

    public function cargarTasa(Request $request){
    
       $tasa = Fatasacamb::with('moneda','moneda2')->whereHas('moneda2',function($q) use ($request){
        $q->where('codigoid','=',$request->codigo);
        })->where('activo',true)->first();

        return response()->json($tasa);
    }

    public function filtro(Request $request){
        $menu = new HomeController();
        $filtro = $request->filtro;
        $date = date('Y-m-d');
        $presupuestos = Fapresup::with('moneda','cliente','valido','articulos','detarticulos','fpago'
        ,'recargo','descuento', 'register')->where('codpre','ilike','%'.$filtro.'%')
        ->orWhereHas('cliente', function($q) use ($filtro){
              $q->where('nompro','ilike','%'.$filtro.'%');
        })->orWhereHas('register', function($q) use ($filtro){
          $q->where('nomemp', 'ilike', '%'.$filtro.'%');
        })->orWhere('estatus', 'ilike', '%'.$filtro.'%')->paginate(8);

    
        return view('/configuracionGenerales/presupuesto')->with('menus',$menu->retornarMenu())->with('presupuestos',$presupuestos)->with('date',$date)->with('sec' ,Segususuc::where('codusuario',Auth::user()->codigoid)->where('activo','TRUE')->first()->getSucursal->nomsucu);
    }

    public function cargarArts(Request $request){
       $moneda = $request->moneda;
       $moneda_id = Famoneda::where('nombre','=',$moneda)->first()->codigoid;  
       $articulos = Caregart::has('costos')->WhereHas('costos', function($q) use ($moneda_id){
              $q->where('codmone','=',$moneda_id);
        })->get();

        

         return response()->json($articulos);
        // return view('/configuracionGenerales/registroPresupuesto.list-articulos', compact('articulos'));
    }

    public function vistaReporte(){
        $menu = new HomeController();
        /*$sucursales = Sucursal::get();*/
        $sucursales = Sucursal::where('codsuc',Auth::user()->userSegSuc->codsuc)->get();
        return view('/reportes/presupuestosReportes')->with('menus',$menu->retornarMenu())->with('sucursales',$sucursales)->with('sec' ,Segususuc::where('codusuario',Auth::user()->codigoid)->where('activo','TRUE')->first()->getSucursal->nomsucu);
    }

    public function aprobar(Request $request){
        $menu = new HomeController();
        $desc=Helper::desencriptar($request->pre);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $presupuesto = Fapresup::find($ID);
        $presupuesto->estatus = "APROBADO";
        $presupuesto->save();
      /*  Session::flash('success', 'Se ha aprobado el presupuesto N° '.$presupuesto->codpre.', Exitosamente');*/
        /*return redirect()->route('listaPresupuesto');*/
        return response()->json(['exito' => $presupuesto->codpre]);
    }

    public function anular(Request $request){
     /*   return $request;*/
        $menu = new HomeController();
        $desc=Helper::desencriptar($request->pre);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $presupuesto = Fapresup::find($ID);
        $presupuesto->estatus = "ANULADO";
        $presupuesto->save();
      /*  Session::flash('success', 'Se ha cancelado el presupuesto N° '.$presupuesto->codpre.', Exitosamente');
        return redirect()->route('listaPresupuesto');*/
        return response()->json(['exito' => $presupuesto->codpre]);
    }

    public function detallar($id){
        $menu = new HomeController();
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $presupuesto = Fapresup::with('moneda','cliente','valido','articulos','detarticulos','fpago'
        ,'recargo','descuento','cambioMoneda','cambioMoneda2','tCambio')->find($ID);
        $date = date('Y-m-d');
        $articulos = Fapreserart::with('demografia','categoria','subcategoria', 'articulo')->where('codpre','=',$presupuesto->codpre)->get();
        #dd($articulos);
        return view('/configuracionGenerales/presupuestoDetalle')->with('menus',$menu->retornarMenu())
        ->with('presupuesto',$presupuesto)->with('articulos',$articulos)->with('date',$date);
    }
    
     /*********REPORTES************* */
    public function pdfPresupuesto($presupuesto){
     
        $desc=Helper::desencriptar($presupuesto);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $presupuestoIn = Fapresup::with('moneda','cliente','valido','articulos','detarticulos','fpago'
        ,'recargo','descuento','sucursales', 'register', 'register.npasicaremp')->find($ID);
        #dd($presupuestoIn);
        $articulos = Fapreserart::with('demografia','categoria','subcategoria', 'articulo')->where('codpre','=',$presupuestoIn->codpre)->get();
        $titulo = 'PRESUPUESTO';
        $date = date('Y-m-d');
        
        $pdf = new generadorPDF();

    $pdf->AddPage('P');
    $pdf->SetTitle($presupuestoIn['codpre']);
    $pdf->SetFont('arial','B',16);
    $pdf->SetWidths(array(90,90));
    $pdf->Ln();
    $pdf->Cell(0,22,utf8_decode($titulo.' N° '.$presupuestoIn->codpre),0,0,'C');
    $pdf->Ln();
    $pdf->SetFont('arial','B',10);
        // $pdf->Cell(0,0,utf8_decode('Sucursal: '.$presupuestoIn->codsuc),0,0,'L');
        $pdf->SetFillColor(0,0,0);
        $pdf->SetFont('arial','B',8,5);
        $pdf->Cell(31,4,utf8_decode('Fecha de emisión'),1,0,'C');
        $pdf->Cell(31,4,utf8_decode('Fecha de vencimiento'),1,0,'C');
        $pdf->Cell(52,4,utf8_decode('Elaborado por'),1,0,'C');
        $pdf->Cell(32,4,utf8_decode('Cargo'),1,0,'C');
        $pdf->Cell(43,4,utf8_decode('Correo'),1,0,'C');
        $pdf->Ln();
        $pdf->SetWidths(array(31,31,52,32,43));
        $pdf->SetAligns(['C','C','C','C','C']);
        $pdf->SetFont('arial','',8,3);
        $pdf->Row(array(
          date_format($presupuestoIn->created_at, 'Y-m-d'),
          ($presupuestoIn->vencimiento),
          ($presupuestoIn->register->nomuse), 
          ($presupuestoIn->register->npasicaremp->nomcar ?? 'No asignado'), 
          ($presupuestoIn->register->emaemp ?? 'No asignado'), 
        ));

       /*  $pdf->Cell(0,0,utf8_decode('AUTOR: '.$presupuestoIn->autor),0,0,'L');        
        $pdf->Ln(5);
        $pdf->Cell(0,0,utf8_decode('SUCURSAL: '.$presupuestoIn->sucursales->nomsucu),0,0,'L');        
        $pdf->Ln(5);
        $pdf->Cell(0,0,utf8_decode('CREADO EL: '.date_format($presupuestoIn->created_at, 'Y-m-d')),0,0,'L');
        $pdf->Ln(5);
        $pdf->Cell(0,0,utf8_decode('VENCE EL: '.($presupuestoIn->vencimiento)),0,0,'L');
        $pdf->Ln(5);
        $pdf->Cell(0,0,utf8_decode('VALIDO POR: '.$presupuestoIn->valido->duracion.' Días'),0,0,'L');  
        $pdf->Ln(5);      
        if($presupuestoIn->vencimiento <= $date){
            $pdf->Cell(0,0,utf8_decode('ESTATUS: VENCIDO'),0,0,'L');
            $pdf->Ln(5);     
        }
        else{
            $pdf->Cell(0,0,utf8_decode('ESTATUS: '.$presupuestoIn->estatus),0,0,'L');
            $pdf->Ln(5);     
        } */
        ///INFORMACION DEL CLIENTE
/* 
        $pdf->Cell(0,0,utf8_decode('Información del cliente'),0,0,'C'); 
        $pdf->Ln(4);    */    
        $pdf->Ln(1);
        $pdf->SetFillColor(204,204,204);
        $pdf->SetFont('arial','B',8,5);
        $pdf->Cell(62,4,utf8_decode('Doc. Indentidad / RIF'),1,0,'C');
        $pdf->Cell(64,4,utf8_decode('Cliente'),1,0,'C');
        $pdf->Cell(63,4,utf8_decode('Numero de Teléfono'),1,0,'C');

       $pdf->Ln();
       $pdf->SetFillColor(255,0,0);
       $pdf->SetWidths(array(62,64,63));
       $pdf->SetAligns(['C','C','C']);
       $pdf->SetFont('arial','',8,5);
       $pdf->Row(array($presupuestoIn->codcli,$presupuestoIn->cliente->nompro,$presupuestoIn->cliente->telpro));
       $pdf->Ln(1);
       $pdf->SetFont('arial','B',8,5);

       $pdf->SetFillColor(255,255,255);
       $pdf->Cell(189,4,utf8_decode('Dirección'),1,0,'C');
       $pdf->Ln();
       $pdf->SetFont('arial','',8,5);
       $pdf->Multicell(189,4,strtoupper($presupuestoIn->cliente->dirpro),1,'C');
        $pdf->Ln();
        $pdf->SetFont('arial','B',8,5);
        $pdf->SetFillColor(204,204,204);
        $pdf->Cell(48,4,utf8_decode('Validez de la oferta'),1,0,'C');
        $pdf->Cell(47,4,utf8_decode('Estatus'),1,0,'C');
        $pdf->Cell(46,4,utf8_decode('Tipo de moneda'),1,0,'C');
        $pdf->Cell(48,4,utf8_decode('Tasa de Cambio'),1,0,'C');

        $pdf->Ln();
        $pdf->SetWidths(array(48,47,46,48));
        $pdf->SetAligns(['C','C','C','C','C','C']);
        $pdf->SetFont('arial','',8,3);
        $pdf->Row(array(
          ($presupuestoIn->valido->duracion.' Días'),
          ($presupuestoIn->estatus), 
          ($presupuestoIn->moneda->nombre),
          ($presupuestoIn?->tcambio?->valor.' '.$presupuestoIn?->cambioMoneda?->nomenclatura), 

        ));



       ////INFORMACION DE ARTICULOS
       $pdf->Ln(6);
       $pdf->SetFont('arial','B',10);
       $pdf->Cell(0,0,utf8_decode('Información de los artículos'),0,0,'C'); 
       $pdf->Ln(4);
       $pdf->SetFont('arial','B',8,5);
       $pdf->SetFillColor(2,157,116);
       $pdf->Cell(29,8,utf8_decode('Cod. Artículo'),1,0,'C');
       $pdf->Cell(92,8,utf8_decode('Descripción'),1,0,'C');
       $pdf->Cell(18,8,utf8_decode('Cantidad'),1,0,'C');
       $pdf->Cell(25,8,utf8_decode('Costo Unit.'),1,0,'C');
       $pdf->Cell(25,8,utf8_decode('Total'),1,0,'C');

       $pdf->Ln();
       $pdf->SetWidths(array(29,92,18,25,25));
       $pdf->SetAligns(['C','C','C','C','C']);
       $pdf->SetFont('arial','',8,0);
       

       foreach($articulos as $articulo){
 #dd(floatval($articulo->preunit));
        $total = floatval($articulo->cantart) * floatval($articulo->preunit);
          
            if($presupuestoIn->moneda->nombre == "PETRO"){
            $pdf->Row(array($articulo->codart,$articulo->articulo->desart/* .'/'.$articulo->demografia->nombredemo.'/'.$articulo->categoria->nombre */,$articulo->cantart,number_format($articulo->preunit,2,',','.'), number_format($articulo->totart,2,',','.')));
            }
            else{
              #dd($articulo->demografia);
              
                $pdf->Row(array($articulo->codart,$articulo->articulo->desart,/*'/'.$articulo->demografia->nombredemo.*//*.$articulo->categoria->nombre.'/'*/$articulo->cantart,number_format($articulo->preunit,2),number_format($articulo->totart,2,',','.')));
              
                
            }
       
       }

    //   INFORMACION DE FINANZA
       $pdf->Ln(6);
       $pdf->SetFont('arial','B',10);
       $pdf->Cell(0,0,utf8_decode('Costos'),0,0,'C'); 
       $pdf->Ln(4);
       $pdf->SetFont('arial','B',8,5);  
       $pdf->SetFillColor(255,157,116);
       if ($presupuestoIn->moneda->nombre == "BOLIVAR") {
        # code...
        $pdf->Cell(38,8,utf8_decode('Subtotal'),1,0,'C');
        $pdf->Cell(38,8,utf8_decode('Monto descuento'),1,0,'C');
        $pdf->Cell(38,8,utf8_decode('Recargo'),1,0,'C');
        $pdf->Cell(38,8,utf8_decode('Monto Total'),1,0,'C');
        $pdf->Cell(37,8,utf8_decode('Monto Total ($)'),1,0,'C');
 
       $pdf->Ln();
       $pdf->SetAligns(['C','C','C','C','C']);    
       $pdf->SetFont('arial','',8,5);
 
       
       $pdf->Cell(38,8,utf8_decode(number_format($presupuestoIn->subtotalpre,2,',','.').' '.$presupuestoIn->moneda->nomenclatura),1,0,'C');
     
       if($presupuestoIn->mondesc){
            $pdf->Cell(38,8,utf8_decode(number_format($presupuestoIn->mondesc,2,',','.').' '.$presupuestoIn->moneda->nomenclatura),1,0,'C');
          }
       else{
            $pdf->Cell(38,8,utf8_decode("0,00"),1,0,'C');
           } 
       if($presupuestoIn->recargo){
            $pdf->Cell(38,8,utf8_decode(number_format($presupuestoIn->monrgo,2,',','.').' '.$presupuestoIn->moneda->nomenclatura.' ('.$presupuestoIn->recargo->nomrgo.')'),1,0,'C');
       }
       else {
            $pdf->Cell(38,8,utf8_decode('SIN RECARGO'),1,0,'C');
            }
            $pdf->Cell(38,8,utf8_decode(number_format($presupuestoIn->monpre,2,',','.').' '.$presupuestoIn->moneda->nomenclatura),1,0,'C');
            $pdf->Cell(37,8,utf8_decode(number_format($presupuestoIn->monpre/$presupuestoIn->tcambio->valor,2,',','.').' $'),1,0,'C');
       } else{
        $pdf->Cell(50,8,utf8_decode('Subtotal'),1,0,'C');
       $pdf->Cell(45,8,utf8_decode('Monto descuento'),1,0,'C');
       $pdf->Cell(45,8,utf8_decode('Recargo'),1,0,'C');
       $pdf->Cell(49,8,utf8_decode('Monto Total'),1,0,'C');

      $pdf->Ln();
    $pdf->SetWidths(array(50,45,45,49));
      $pdf->SetFont('arial','',8,5);

      $pdf->Cell(50,8,utf8_decode(number_format($presupuestoIn->subtotalpre,2,',','.').' '.$presupuestoIn->moneda->nomenclatura),1,0,'C');
    
      if($presupuestoIn->mondesc){
            $pdf->Cell(45,8,utf8_decode(number_format($presupuestoIn->mondesc,2,',','.').' '.$presupuestoIn->moneda->nomenclatura),1,0,'C');
         }
      else{
            $pdf->Cell(45,8,utf8_decode("0,00"),1,0,'C');
          } 
      if($presupuestoIn->recargo){
           $pdf->Cell(45,8,utf8_decode(number_format($presupuestoIn->monrgo,2,',','.').' '.$presupuestoIn->moneda->nomenclatura.' ('.$presupuestoIn->recargo->nomrgo.')'),1,0,'C');
      }
      else {
            $pdf->Cell(45,8,utf8_decode('SIN RECARGO'),1,0,'C');
           }
           
               $pdf->Cell(49,8,utf8_decode(number_format($presupuestoIn->monpre,2,',','.').' '.$presupuestoIn->moneda->nomenclatura),1,0,'C');
       }

      
         
             $pdf->ln(); 

             $pdf->AddPage('P');
             $pdf->ln(15);
             $pdf->SetFont('arial','B',10);
             $pdf->Cell(189,5, utf8_decode('ANEXOS'), 1, 0, 'C');
             $pdf->Ln(8);
             //$pdf->Cell(189, 100, '', 1,0,'J');
             $pdf->SetFont('arial','B',8);
             $pdf->Cell(189, 5, utf8_decode('Marca:'), 0, 0, 'J');
             $pdf->Ln(3);
             $pdf->SetFont('arial', '',8);
             $pdf->Cell(189, 5, utf8_decode('Equipos VIT.'), 0, 0, 'J');
             $pdf->Ln(3);
             $pdf->SetFont('arial','B',8);
             $pdf->Cell(189, 5, utf8_decode('Lugar de entrega:'), 0, 0, 'J');
             $pdf->Ln(3);
             $pdf->SetFont('arial', '',8);
             $pdf->Cell(189, 5, utf8_decode('A convenir.'), 0, 0, 'J');
             $pdf->Ln(3);
             $pdf->SetFont('arial','B',8);
             $pdf->Cell(189, 5, utf8_decode('Garantía:'), 0, 0, 'J');
             $pdf->Ln(4);
             $pdf->SetFont('arial', '',8);
             $pdf->SetFillColor(255,255,255);
             $pdf->Parrafo('Para los Equipos de Escritorio, Equipos Portátiles, Servidores de Rack, Torre, monitores es de 12 meses, perifericos 6 meses a partir de la fecha de entrega, y se regirán según el certificado de garantía. Garantía sujeta a los términos y condiciones de la garantía');
             $pdf->SetFont('arial','B',8);
             $pdf->Cell(12, 5, utf8_decode('Anexo 1:'), 0, 0, 'J');
             $pdf->SetFont('arial', '',8);
             $pdf->Cell(189, 5, utf8_decode('Términos y Condiciones de la Garantía.'), 0, 0, 'J');
             $pdf->Ln(4);
             $pdf->Parrafo('No será válida la Garantía en los siguientes casos');
             $pdf->Parrafo('La garantía cubre únicamente aquellos defectos que surgiesen como resultado del uso normal del producto y no por aquellos que resultasen de:');
             $pdf->Parrafo('1°. Si ha sido violentada la etiqueta de sello de garantía.');
             $pdf->Parrafo('2°. Que el producto no haya sido operado conforme al instructivo de uso y operación que se acompaña y no se hayan observado las recomendaciones y advertencias que se indican.');
             $pdf->Parrafo('3°. Que el producto haya sido modificado o desarmado parcial o totalmente; o haya sido manipulado negligentemente y como consecuencia haya sufrido daños atribuibles al cliente, persona o talleres no autorizados por VIT, así como por daños causados por el uso o conservación fuera de los parámetros normales del producto y por la modificación e incorporación de otros productos');
             $pdf->Parrafo('4°. Que los datos del certificado y del producto no coincidan o hayan sido alterados o removidos de su lugar.');
             $pdf->Parrafo('5°. Que el sello de garantía del equipo sea removido por personas no autorizadas por VIT.');
             $pdf->SetFont('arial', 'B',8);
             $pdf->Cell(12, 3, utf8_decode('Anexo 2:'), 0, 0, 'J');
             $pdf->SetFont('arial', '',8);
             $pdf->Cell(189, 3, utf8_decode('Servicio y Soporte Técnico.'), 0, 0, 'J');
             $pdf->Ln(3);
             $pdf->Parrafo('A) Servicio de Soporte Técnico al Cliente por Helpdesk:');
             $pdf->Parrafo('El objetivo fundamental es atender los requerimientos solicitados por los clientes brindando soporte técnico preliminar, a través de la línea 0800-infoVIT (0800-4636848), chat o correo electrónico, a través de nuestra página web, www.vit.gob.ve, brindando atención personalizada por parte de especialistas en aras de solucionar las fallas o problemas presentados en un primer nivel, las cuales de no ser resueltas por esta vía, se realizará el servicio en sitio. Escalamiento de la Llamada (niveles).');
             $pdf->Parrafo('1) Operador - 1° Nivel: Se deberá llamar al (0800-4636848), para solicitar información del servicio en cuestión.');
             $pdf->Parrafo('2)Analista de Soporte Técnico (A.S.T.) - 2° Nivel: Analista de Soporte Técnico (coordinador de los servicios) a través de los teléfonos 0212-3732853, (0800-4636848), Email: soporte@vit.gob.ve');
             $pdf->Parrafo('3) Analista de Postventa - 3° Nivel: Analista de Postventa a través de los teléfonos 0212-3732853, (0800-4636848)');
             $pdf->Parrafo('4) Gerente de Postventa - 4° Nivel: Gerente de Postventa a través de los teléfonos 0212-3732853, 0416-6134571, (0800-4636848) Email: lrada@vit.gob.ve');
             $pdf->Parrafo('B) Servicio de Soporte Técnico En Sitio:');
             $pdf->Parrafo('El objetivo de Soporte En Sitio es mantener en el tiempo la continuidad operativa de la plataforma tecnológica de los clientes y usuarios de VIT, logrando con esto optimizar la productividad de los equipos para nuestros clientes, todo esto dentro de tiempos de solución definidos.');
             $pdf->Parrafo('El servicio de Soporte En Sitio se brindará desde la oficina de VIT en Caracas y, en conjunto con los distintos canales que prestan el servicio de postventa, alrededor de todo el país.');
             $pdf->SetFont('arial', 'B',8);
             $pdf->Cell(12, 3, utf8_decode('Anexo 3:'), 0, 0, 'J');
             $pdf->SetFont('arial', '',8);
             $pdf->Cell(189, 3, utf8_decode('Condiciones de Negociación'), 0, 0, 'J');
             $pdf->Parrafo('1ro. Pago del 100% por el monto total de la oferta');
             $pdf->Parrafo('2do. VIT se reserva el derecho de modificación de modelos según inventario existente');
             $pdf->Parrafo('3ro. Los cálculos para el desglose en bolívares son efectuados a la tasa del día, por lo que el pago debe ser abonado según la tasa del BCV vigente a la fecha de la emisión de la factura.');
             $pdf->SetFont('arial', 'B',8);
             $pdf->Cell(12, 3, utf8_decode('Anexo 4:'), 0, 0, 'J');
             $pdf->SetFont('arial', '',8);
             $pdf->Cell(189, 3, utf8_decode('Condiciones de Pago'), 0, 0, 'J');
             $pdf->Parrafo('Transferencias bancarias a los numeros de cuenta indicados en certificacion bancaria anexa a esta oferta. ');
             $pdf->Parrafo('VIT se encuentra exenta en la aplicación de retención de compromiso de Responsabilidad Social, según lo expuesto en el Art. 3, del Reglamento de la Ley de Contrataciones Públicas');


        $pdf->Output('I',$presupuestoIn->codpre.'.pdf');
      exit;
    }
    public function reportePresupuesto(Request $request){
   try{
     if(!$request['desde']||!$request['hasta'])throw new Exception("Debe seleccionar una fecha");

       $desde = $request['desde'];
       $hasta = $request['hasta'];
       $diames = $request['diames'];
         switch ($diames) {
            case 'diario':
               $tipo = 'DIARIO';
            break;
            case 'ayer':
                $tipo = 'DÍA ANTERIOR';
            break;
            case 'semanal':
               $tipo = "SEMANAL";
            break;
            case 'semant':
               $tipo = "SEMANA ANTERIOR";
            break;
            case 'mensual':
                $tipo = 'MENSUAL';
            break;
            case 'mesant':
              $tipo = 'MES ANTERIOR';
            break;
            case 'anual':
              $tipo = 'ANUAL';
            break;
            case 'antyear':
              $tipo = 'AÑO ANTERIOR';
            break;
            default:
              $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
            break;

        }
       }catch(Exception $e){
         return redirect()->route('reportesPresupuestos')->with('error',$e->getMessage());
       }

        try{
            if(!$request->sucursal)throw new Exception("Debe seleccionar una sucursal");
            if($request->sucursal =="TODAS"){
                $general = true;
            }
            else{
                $general = false;
                $codsuc = $request->sucursal;
            }

        }
        catch(Exception $e){
            return redirect()->route('reportesPresupuestos')->with('error',$e->getMessage());
        }
        
      ///EVALUA LOS PERIODOS DE TIEMPO SELECCIONADOS Y SI SERA UNA CONSULTA GENERALIZADA O POR SUCURSAL

        $estatus = $request['status'];

      try{

               
               if($general){
                switch ($estatus) {
                  case 'todas':
                    $presupuestos = Fapresup::with('moneda','cliente','valido','articulos','detarticulos','fpago'
                    ,'recargo','descuento')->whereBetween('created_at',[$desde,$hasta])->get();
                    break;
                  case 'facturado':
                    $presupuestos = Fapresup::with('moneda','cliente','valido','articulos','detarticulos','fpago'
                    ,'recargo','descuento')->where('facturado',true)->whereBetween('created_at',[$desde,$hasta])->get();
                  break;
                  case 'aprobado':
                    $presupuestos = Fapresup::with('moneda','cliente','valido','articulos','detarticulos','fpago'
                    ,'recargo','descuento')->where('estatus','=','APROBADO')->whereBetween('created_at',[$desde,$hasta])->get();
                  break;     
                  default:
                    $presupuestos = Fapresup::with('moneda','cliente','valido','articulos','detarticulos','fpago'
                    ,'recargo','descuento')->where('estatus','=','ANULADO')->whereBetween('created_at',[$desde,$hasta])->get();
                    break;
                 }
                   
               }
               else{
                 switch ($estatus) {
                  case 'todas':
                      $presupuestos = Fapresup::with('moneda','cliente','valido','articulos','detarticulos','fpago'
                    ,'recargo','descuento')->whereBetween('created_at',[$desde,$hasta])->where('codsuc','=',$codsuc)->get();
                    break;
                  case 'facturado':
                    $presupuestos = Fapresup::with('moneda','cliente','valido','articulos','detarticulos','fpago'
                    ,'recargo','descuento')->whereBetween('created_at',[$desde,$hasta])->where('codsuc','=',$codsuc)->where('facturado',true)->whereBetween('created_at',[$desde,$hasta])->get();
                  break;
                  case 'aprobado':
                    $presupuestos = Fapresup::with('moneda','cliente','valido','articulos','detarticulos','fpago'
                    ,'recargo','descuento')->whereBetween('created_at',[$desde,$hasta])->where('codsuc','=',$codsuc)->where('estatus','=','APROBADO')->whereBetween('created_at',[$desde,$hasta])->get();
                  break;     
                  default:
                      $presupuestos = Fapresup::with('moneda','cliente','valido','articulos','detarticulos','fpago'
                    ,'recargo','descuento')->whereBetween('created_at',[$desde,$hasta])->where('codsuc','=',$codsuc)->where('estatus','=','ANULADO')->whereBetween('created_at',[$desde,$hasta])->get();
                    break;
                 }
               }
            
            if($presupuestos->isEmpty()){
                Session::flash('error', 'No existen registros con los parametros de busqueda ingresados');
                return redirect()->route('reportesPresupuestos');}
        }//try
        catch(Exception $e){
            Session::flash('error', 'Se presentó un problema en la generación del reporte, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
            return redirect()->route('reportesPresupuestos');
        }
            $pdf = new generadorPDF();

            $titulo = 'REPORTE DE PRESUPUESTOS';
            $total = $presupuestos->count();
            $fecha = date('Y-m-d');
    
            $pdf->AddPage('L');
            $pdf->SetTitle('REPORTE DE PRESUPUESTOS');
            $pdf->SetFont('arial','B',16);
            $pdf->SetWidths(array(90,90));
            $pdf->Ln();
            $pdf->Cell(0,22,utf8_decode($titulo),0,0,'C');
            $pdf->Ln();
            $pdf->SetFont('arial','B',10);
            if($tipo == 'Intervalo'){
                $pdf->Cell(0,0,utf8_decode('Tipo: '.$tipo.' '.$desde.' - '.$hasta),0,0,'L');
            }
            else{
                $pdf->Cell(0,0,utf8_decode('Tipo: '.$tipo),0,0,'L');
            }
            $pdf->Ln(5);
            $pdf->Cell(0,0,utf8_decode('Fecha actual: '.$fecha),0,0,'L');
            $pdf->Ln(5);
            $pdf->Cell(0,0,utf8_decode('Total: '.$total),0,0,'L');
            $pdf->Ln(10);
            $pdf->SetFont('arial','B',8,5);
            $pdf->SetFillColor(2,157,116);
            $pdf->Cell(50,8,utf8_decode('N° PRESUPUESTO'),1,0,'C');
            $pdf->Cell(110,8,utf8_decode('CLIENTE'),1,0,'C');
           /* $pdf->Cell(30,8,utf8_decode('FORMA DE PAGO'),1,0,'C');*/
            $pdf->Cell(30,8,utf8_decode('VALIDEZ'),1,0,'C');
            $pdf->Cell(30,8,utf8_decode('ESTATUS'),1,0,'C');
            $pdf->Cell(60,8,utf8_decode('AUTOR'),1,0,'C');
            $pdf->Ln();
            $pdf->SetWidths(array(50,110,30,30,60));
            $pdf->SetAligns(['C','C','C','C','C','C']);
            $pdf->SetFont('arial','',8,5);
            foreach($presupuestos as $presupuesto){

                if($presupuesto->facturado == true){
                    $pdf->SetTextColor(0,0,255);
                }
                elseif($presupuesto->estatus == 'ANULADO'){
                    $pdf->SetTextColor(255,0,0);
                }

                $pdf->Row(array($presupuesto->codpre,$presupuesto->cliente->nompro,
                $presupuesto->valido->duracion.' Dias',$presupuesto->estatus,$presupuesto->autor));

                 $pdf->SetTextColor(0,0,0);
            }
            $pdf->Ln(5);
            $pdf->Output('I','Reporte de Presupuestos-'.$fecha.'pdf');
             $pdf->SetTextColor(0,0,0);

            
           exit;
    }

    public function cargarTalla(Request $request){

     $art = Caregart::where('codart','=',$request['articulo'])->first();
/*     $codtallas = explode(' ',$art->codtallas);
     $tallas = [];
     foreach($codtallas as $talla){
       $t = Tallas::where('codtallas','=',$talla)->first();
       array_push($tallas, $t);
     }*/
     #dd($art->codtallas);
      $tallas = [];
       $codtallas = explode(' ',$art->codtallas);
       array_push($codtallas,'A-N/A');
        foreach($codtallas as $codtalla){
          $talla = Tallas::where('codtallas','=',$codtalla)->get();
          array_push($tallas, $talla);
      }
/*      return response()->json($tallas);*/

        if($tallas[0]->isEmpty()){
          return response()->json('n/a');
        }
        else{
          return response()->json(['tallas'=>$tallas]);
        }
    }
   public function filtrar($value)
    {
        
        if(strcmp($value, "0") === 0 || strcmp($value, null) !== 0) return true;
    
    }
    
   public function filtrar2($value)
    {
          if($value !== null) return true;
    }   
}
?>