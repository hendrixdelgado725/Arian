<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Carbon\Carbon;
use DataTables;
use Exception;
use Session;
use Helper;
use App\User;
use App\Models\Cpdetptocta;
use App\Models\Cpptocta;
use App\Models\Cpasiini;
use App\Models\Nphiscon;
use App\Models\Bnubica;


class EmipuntoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function retornarMenu()
    {
        $yaml = new Parser();
        $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
        return $menu['menu'];
    }

    public function index()
    {
        //
        $puntos = Cpptocta::paginate(8);
        $menu = $this->retornarMenu();

        return view('compras.emisionPunto.emiPunto')
        ->with([
            'menus' => $menu, 
            'puntos' => $puntos
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $menu = $this->retornarMenu();
        $partidas = Cpasiini::select(['codpre', 'nompre'])->get();
        $ubicaciones = Bnubica::get();
        $date = Carbon::now()->format('Y-m-d');

        return view('compras.emisionPunto.emiPtocreate')
        ->with([
            'menus' => $menu,
            'partidas' => $partidas,
            'ubicaciones' => $ubicaciones,
            'date' => $date
        ]);
    }

    public function datosCreate() 
    {
        
        $partidas = Cpasiini::get();
        
        return DataTables::of($partidas)->addColumn('action',function($partidas){

            $button = "<a href='#' id='partidas' class='check1 ponercta' 
                       onclick='getPartidas($partidas)' >
                       <i class='fas fa-check'></i></a>";
            
             return $button;
           })->rawColumns(['action'])->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        dd($request);
        $request->validate([
            'numpta' => 'required',
            'uniori' => 'required',
            'unides' => 'required',
        ]);

        foreach($request->partidas as $partida){
            $exam[] = Cpasiini::where('id', $partida)->first();
        }

        $puntopar = [];
        $partidas = array_filter($exam);
        $montos = array_filter($request->monto);
        $num = 0;
        #dd($montos);


        try {
            //code...
            foreach($partidas as $item => $value) {
                if(($partidas[$item] !== null) && ($montos[$item] !== null)){
                $monto = floatval($montos[$item]);
                    $puntopar[] = [
                        'numpta' => $request->numpta,
                        'codpre' => $value->codpre,
                        'moncod' => $monto,
                        'moncoddis' => 0.00,
                    ];
                    $num++;
                }
                
            }
            if($num === 0) {
                throw new \Exception("Debe especificarse al menos un registro de partida con su respectivo monto");
            }

            Cpptocta::create([
                'numpta' => $request->numpta,
                'fecpta' => $request->fecpta,
                'codubiori' => '',
                'codubides' => '',
                'asunto' => $request->asunto ?? '',
                'motivo' => $request->motivo ?? '',
                'reccon' => '',
            ]);
            if(isset($puntopar)){
                Cpdetptocta::insert($puntopar);
            }  
            $request->session()->flash('success', 'Se ha registrado el punto de cuenta N° '.$request->numpta.' se ha registrado correctamente.');
            $menu = $this->retornarMenu();
            $puntos = Cpptocta::get();
            return redirect()->route('PtoctaIndex')->with([
                'menus' => $menu, 
                'puntos' => $puntos]);

        } catch (\Exception $e) {
            //throw $th;
            return redirect()->route('PtoctaCreate')->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
