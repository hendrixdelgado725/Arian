<?php

namespace App\Http\Controllers;

use Session;
use App\User;
use App\cajaUser;
use App\Models\Caja;
use App\Models\Turno;
use Illuminate\Http\Request;
use Symfony\Component\Yaml\Parser;

class TurnosController extends Controller
{
    public function retornarMenu()
    {
        $yaml = new Parser();
        $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
        return $menu['menu'];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $turno = Turno::with('Cajero.usuarios', 'Supervisor', 'Caja')->orderBy('id','DESC')->paginate(8);
        $menus = $this->retornarMenu();
        return view('facturacion.turnos', compact('menus', 'turno'));
    }

    public function create(){

        $menus = $this->retornarMenu();
        return view('facturacion.turnosCreate', compact('menus'));
    }

    public function getData() //Envío de datos a la ruta de creación de turnos
    {
        $cajas = Caja::get();
        $supervisores = User::where('codroles', '!=', 'R-003')->get();
        
        return response()->json([
            'cajas' => $cajas,
            'supervisores' => $supervisores,
        ], 200);
    }

    public function getCajeros(Request $request) //Envío de datos a la ruta de creación de turnos
    {
        $cajeros = cajaUser::where('codcaja', $request->caja)->get();
        
        return response()->json([
            'cajeros' => $cajeros,
        ], 200);
    }

    public function filtroTurnos(Request $request) {
        
        
        $turno = Turno::whereHas('Cajero',function ($q) use ($request){
            $q->where('loguse','ilike','%'.$request->all()['filtro'].'%');
        })->orderBy('id','DESC')->paginate(10);
        $menus = $this->retornarMenu();
        return view('facturacion.turnos', compact('menus', 'turno'));

    }


    public function getTurno(Request $request){

        $turno = Turno::where('id',$request->id)->first();
        
        return response()->json([
            'turno' => $turno,
        ], 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        $numero = str_pad(Turno::withTrashed()->count() + 1, 6, 0, STR_PAD_LEFT);
        
        $request->validate([
            'caja' => 'required:pgsql2,Turno',
            'cajero' => 'required:pgsql2,Turno',
            'tipoturno' => 'required:pgsql2,Turno',
            'supervisor' => 'required:pgsql2,Turno',
        ]);
        $turno = Turno::create([
            'caja' => $request->caja,
            'cajero' => $request->cajero,
            'tipoturno' => $request->tipoturno,
            'supervisor' => $request->supervisor,
            'inicio' => date("d-m-Y"),
            'estatus' => true,
            'numtur' => $numero,
        ]);

        return response()->json([
            'success' => true,
            'msg' => 'Información suministrada correctamente',
            'redirect' => route('turnos'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menus = $this->retornarMenu();
        $type = 'editing';
        $turno = Turno::findOrFail($id);

        if(!$turno){
            return redirect()->back();
        }

        return view('facturacion.turnosEdit', compact('turno', 'menus', 'type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());

        $request->validate([
            'caja' => 'required:pgsql2,Turno',
            'cajero' => 'required:pgsql2,Turno',
            'tipoturno' => 'required:pgsql2,Turno',
            'supervisor' => 'required:pgsql2,Turno',
        ]);

        $turno = Turno::findOrFail($id);

        $turno->caja = $request->caja;
        $turno->cajero = $request->cajero;
        $turno->tipoturno = $request->tipoturno;
        $turno->supervisor = $request->supervisor;
        $turno->inicio = date("d-m-Y");
        $turno->save();

        return response()->json([
            'success' => true,
            'Información suministrada correctamente',
            'redirect' => route('turnos'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $turno = Turno::where('id',$id)->with('Caja.ventas')->doesnthave('Caja.ventas')->delete();
        
        if($turno !== 0){
            return response()->json([
                
                'exito' => true
            ]);
        }else{
            return response()->json([
                'error' => true
            ]);
        }

//        Session::flash('success', 'Registro eliminado con exito.');
//        
    }
}
