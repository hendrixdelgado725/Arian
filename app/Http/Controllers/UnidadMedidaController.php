<?php

namespace App\Http\Controllers;

use Session;
use App\Models\Caunimed;
use Illuminate\Http\Request;
use Symfony\Component\Yaml\Parser;


class UnidadMedidaController extends Controller
{
    public function retornarMenu()
    {
        $yaml = new Parser();
        $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
        return $menu['menu'];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $menus = $this->retornarMenu();

        if($request->filtro){
            $unidadmedida = Caunimed::where('nombre','ilike','%'.$request->filtro.'%')->
            orWhere('nomenclatura','ilike','%'.$request->filtro.'%')->paginate(10);
        } else { 

             $unidadmedida = Caunimed::paginate(10);

        }

        return view('configuracionGenerales.unidadMedida', compact('menus', 'unidadmedida'));

    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $menus = $this->retornarMenu();
        return view('configuracionGenerales.unidadMedidaCreate', compact('menus'));
    }

    public function getUni(Request $request){

        $unidadmedida = Caunimed::findOrFail($request->id);

        return response()->json([
            'unidadmedida' => $unidadmedida,
        ], 200);

    }


    public function store(Request $request)
    {

        $unidadmedidaconteo = Caunimed::withTrashed()->count();
        $unidadmedidaconteo = $unidadmedidaconteo+1;

        $codigo = 'UM-'.$unidadmedidaconteo;

        $request->validate([
            'nomenclatura' => 'required:pgsql2,Caunimed|unique:pgsql2.caunimed,nomenclatura',
            'nombre' => 'required:pgsql2,Caunimed|unique:pgsql2.caunimed,nombre',
        ]);

        $unidadmedida = Caunimed::create([
            'nomenclatura' => $request->nomenclatura,
            'nombre' => $request->nombre,
            'codigo' => $codigo,
        ]);

        Session::flash('success', 'Registro creado con exito.');
        return response()->json([
            'success' => true,
            'Información suministrada correctamente',
            'redirect' => route('Unidadmedida.index'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menus = $this->retornarMenu();
        $type = 'editing';
        $unidadmedida = Caunimed::findOrFail($id);

        if(!$unidadmedida){
            return redirect()->back();
        }

        return view('configuracionGenerales.unidadMedidaEdit', compact('unidadmedida', 'menus', 'type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nomenclatura' => 'required:pgsql2,Caunimed|unique:pgsql2.caunimed,nomenclatura',
            'nombre' => 'required:pgsql2,Caunimed|unique:pgsql2.caunimed,nombre',
        ]);

        $unidadmedida = Caunimed::findOrFail($id);

        $unidadmedida->nomenclatura = $request->nomenclatura;
        $unidadmedida->nombre = $request->nombre;

        $unidadmedida->update();
        
        Session::flash('success', 'Registro editado con exito.');
        return response()->json([
            'success' => true,
            'Información suministrada correctamente',
            'redirect' => route('Unidadmedida.index'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $unidadmedida = Caunimed::findOrFail($id);
        
            if($unidadmedida === null){
                return response()->json([
                    'titulo' => 'Hubo Error eliminando'
                ]);
            }
        $unidadmedida->delete();


        Session::flash('success', 'Registro eliminado con exito.');
        return response()->json([
            'success' => true,
            'message' => 'Información suministrada correctamente',
            'redirect' => route('Unidadmedida.index'),
        ]);
            
        } catch (\Exception $e) {
            //throw $th;
            dd($e);
        }
        
    }
}
