<?php

namespace App\Http\Controllers\UbicacionesAlm;

use App\Http\Controllers\Controller;


use Illuminate\Http\Request;


use App\Models\Menu;
use App\Caartalmubimov;
use App\Caartalmubi;
use App\Cadefubi;
use App\Almacen;
use App\Alma_ubi;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Helper;
use Session;
use Exception;
use Illuminate\Validation\Rule;
use App\Segususuc;

class UbiAlmController extends Controller
{
    public function __contruct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        return view('almacen.ubicacionesList')->with(['menus' => $this->getMenu(),'ubicaciones' => Cadefubi::GetList()]);
    }

    public function create(){
        return view('almacen.ubicacionCreate')->with([
            'menus' => $this->getMenu(),
            'ubicaciones' => Cadefubi::GetList(),
            'almacenes' => Almacen::getAlmCreateUbi(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $request->validate([
            'codalm' => 'required',
            'nomubi' => 'required'
        ]);

        try{
            $codubis = [];
            $ubicacion = Alma_ubi::where('codalm',$request->codalm)->get();
            foreach ($ubicacion as $ubi) {
              $codubis[] = $ubi->codubi;
            }
            $Cad = Cadefubi::where('nomubi',$request->nomubi)->whereIn('codubi',$codubis)->get();
            if(!$Cad->isEmpty()) throw new Exception("Existe una ubicacion con el mismo nombre y almacen");
            $lastUbi=  Alma_ubi::withTrashed()->get()->count();
            $ubi = new Cadefubi;
            $ubi->nomubi = $request->nomubi;
            $codubi= $request->codalm.'-'.str_pad($lastUbi+1, 3, '0', STR_PAD_LEFT);
            $ubi->codubi = $codubi;
            $ubi->save();
            
            Alma_ubi::create([
                'codubi' => $codubi,
                'codalm' => $request->codalm,
                'codsuc' => session('codsuc')
            ]);
            
            

            Session::flash('success', 'La Ubicación '.$request->nomubi.', se registro Exitosamente');
            //Session::flash('error', 'Error de Aplicacion');
        }catch(Exception $e){
            return redirect()->route('ubiCreate')->with(['error' => $e->getMessage()]);
        }
        return redirect()->route('ubicacionesList');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        // $id = Cadefubi::findOrFail(Helper::decrypt($id));
        
        // $temp = Alma_ubi::where('codubi',$id->codubi)->get();

        // dd(Almacen::getAlmCreateUbi(),$temp,Cadefubi::GetList());

        return view('almacen.ubicacionEdit')->with([
            'menus' => $this->getMenu(),
            'ubicacion' => $id = Cadefubi::findOrFail(Helper::decrypt($id)),
            'almubi' => Alma_ubi::where('codubi',$id->codubi)->first(),
            'almubi2' => Alma_ubi::where('codubi',$id->codubi)->get(),
            'almacenes' => Almacen::getAlmCreateUbi()
        ]);

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idubi,$almubi)
    {

        
        
        
        
        // $request->validate([
        //     'nomubi' => 
        //     [
        //         "required",
        //     ],
        //     'codalm' => 
        //     [
        //         "required"
        //     ],
        // ]);

        
        
        try{
          
        
            $codubis = [];
            $ubicacion = Alma_ubi::where('codalm',$request->codalm)->get();
            foreach ($ubicacion as $ubi) {
                $codubis[] = $ubi->codubi;
            }
            $Cad = Cadefubi::where('nomubi',$request->nomubi)->whereIn('codubi',$codubis)->where('id','<>',
            $idubi)->get();
        
            if(!$Cad->isEmpty()) throw new Exception("Existe una ubicacion con el mismo nombre y almacén");
        
            $codubi = Cadefubi::findOrFail($idubi);
        
          //  $exist = Caartalmubi::findQtyUbi($codubi->codubi);
        
            /*if($exist){
                if($exist->codalm !== $request->codalm){
                    throw new Exception("No se puede cambiar almacén debido a que en esta ubicación existen articulos");
                }
            }*/
            
            $temp = collect($request->codalm);
            
            if(isset($request->all()['datosEliminado'][0])){
                $collection = collect(explode(",",$request->all()['datosEliminado'][0]));
                

                $collection->map(function($val) use ($codubi,&$r) {
                   
                   $r = Alma_ubi::where([['codalm',$val],['codubi' ,$codubi->codubi]])->delete();
                   
                });
            }elseif($temp->isNotEmpty()){
                    
                    collect($temp)->map(function ($codAlm) use($codubi,&$r){
                        
                     $r = Alma_ubi::updateOrCreate([
                             'codalm' => $codAlm,
                             'codubi' => $codubi->codubi
                         ],[
                            'codalm' => $codAlm,
                            'codubi' => $codubi->codubi,
                            
                        ]);
         
         
                     });
                }
            

            if($r)Session::flash('success', 'La Ubicación '.$request->nomubi.', se actualizó Exitosamente');
            else Session::flash('error', 'Error de Aplicacion');
        }catch(Exception $e){
            return redirect()->route('ubiShow',Helper::crypt($idubi))->with(['error' => $e->getMessage()]);
        }
        return redirect()->route('ubicacionesList');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $id = Helper::decrypt($id);
        $ubicacion = Cadefubi::findOrFail($id);
        try{
            $exist = Caartalmubi::findQtyUbi($ubicacion->codubi);
            if($exist) throw new Exception("Existen articulos en esta ubicacion");
            $almaubi = Alma_ubi::where('codubi',$ubicacion->codubi)->first();
            $r = $almaubi->delete();
            $x = $ubicacion->delete();
            if(!$r || !$x) throw new Exception("Error al eliminar Ubicacion");
        }catch(Exception $e){
            return response()->json(["error" => $e->getMessage()]);
        }
        return response()->json(["exito" => true]);
    }

    public function getMenu(){
        return $this->menu = Menu::getmenu();
    }

    public function filtro(Request $request){
        
        return view('almacen.ubicacionesList')
        ->with([
                'menus'       => $this->getMenu(),
                'ubicaciones' => Cadefubi::where('nomubi','ilike',"%".$request->filtro."%")->with('Almacenes')->whereHas('Almacenes',function($q) use ($request){
                    $q->where('nomalm','ilike',"%{$request->filtro}%");
                })->paginate(10)
            ]);
                                                      
    }

}
