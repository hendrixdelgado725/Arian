<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use Illuminate\Contracts\Auth\Guard;
use App\Segususuc;
use App\Models\Caja;
use App\cajaUser;
use Carbon\Carbon;
use Helper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;
use App\User;
use \Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class reiniciarCajaController extends Controller 
{
    use AuthenticatesUsers;
    protected $auth;
    protected $loginView = '/facturacionv2';
    protected $guard = 'caja';
    
    public function __construct(Guard $auth){

            $this->auth = $auth;
            $this->middleware('auth');

    }

      public function authenticated(Request $request)
      {
          
          $cajaUser = cajaUser::where('loguse',$this->auth->user()->loguse)
          ->where('password','md5'.md5(strtoupper($this->auth->user()->loguse).$request->id2))
          ->whereNull('deleted_at')->with('caja')->whereHas('caja',function ($q) use ($request)
          {
            
            $q->where('id',$request->id);
            
          })->first();
          #dd($cajaUser);
          if ($cajaUser !== null) {
            
            $activo = cajaUser::where('loguse',$this->auth->user()->loguse)->where('codcaja',$cajaUser->codcaja)->first();
            $activo->activo = 'TRUE';
            $activo->save();  
              return response()->json([

                'logueado'   => 'LA CAJA ESTA ABIERTA',
                'nombreCaja' => $activo->caja->descaj,
                'status' => true

              ]);
          }else{
              
              return response()->json([

                'logueado' => 'CONTRASEÑA INCORRECTA',
                'status' => null
              ]);

          }
        }


  //hacer para que no se pueda registrar el mismo usuario 2 veces OJO
    public function index()
    {
         $menu = new HomeController();
      //$cajaUSer = cajaUser::whereNull('deleted_at')->get();
        $cja = cajaUser::whereNull('deleted_at')->paginate(8);
      
      return view('reiniciarCaja.reiniciarContrasenaCaja')->with('menus',$menu->retornarMenu())->with('cajaUser',$cja)->with('cajas',Caja::whereNull('deleted_at')->get());

    }

        public function busqueda(Request $request)
    {
        
        $menu = new HomeController();

        $cja = cajaUser::where('loguse','ilike','%'.$request->filtro.'%')
               ->whereNull('deleted_at')->paginate(8);
       
        return view('reiniciarCaja.reiniciarContrasenaCaja')->with('menus',$menu->retornarMenu())->with('cajaUser',$cja)->with('cajas',Caja::whereNull('deleted_at')->get());

    }

    public function create()
    {
      
      $menu = new HomeController();
      
      return view('reiniciarCaja.registroContrasenaCaja')->with('menus',$menu->retornarMenu())->with('user',Segususuc::whereNull('deleted_at')->get())->with('caja',Caja::whereNull('deleted_at')->get());
    
    }


    public function search($id)
    {   
        $particional = explode('-',$id);
       

        $user = \App\User::where('loguse',$particional[0])->with(['userSegSuc'=> function ($q) use ($particional)
        {
           $q->where('loguse',$particional[0])->where('codsuc',$particional[1].'-'.$particional[2]);
        }])->first();
        //dd($user->userSegSuc->getSucursal->nomsucu);
        return response()->json([
          'cedula'   => $user->cedemp,
          'sucursal' => $user->userSegSuc->getSucursal->nomsucu
        ]);
      }

    //pasa por aqui para validar si contraseña se ṕide es correcta
    private function validar($contra)
    {
      return true;
      if (preg_match("/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/",$contra)){
          
      }
     

      return false;
    }

    public function registro(Request $request)
    {

      //dd($request->all());
      $request->validate([

        'contra'      => 'required|min:8|max:16',
        'Confircontra'=> 'required|min:8|max:16',
        'caja'        => 'required',
        'nombre'      => 'required',
        'cedularif'   => 'required',
        'user'        => 'required'

      ]);
      
      
      try {
          DB::beginTransaction();
          $sucuse_reg=Auth::user()->getsucUse()->codsuc;
          $guion='-';
          $cont = cajaUser::withTrashed()->get()->count();
          $cont = $cont+1;
          $suc=Auth::user()->getCodigoActiveS();
          // $suc = Auth::user()->codsuc_reg;        
          $codsuc_reg = explode('-',$suc);
          $cod = $cont.$guion.$codsuc_reg[1];  
          #dd($cod);
          $capture = explode('-',$request->user)[0];   

          $busqueda = cajaUser::where('loguse',$capture)->where('codcaja',$request->caja)->where('deleted_at',null)->first();          
          
          if (is_null($busqueda) === false)      
                throw new Exception('este usuario tiene registrado la caja ' . $busqueda->caja->descaj);
         elseif (strcmp($request->contra, $request->Confircontra) !== 0) 
                throw new Exception('la contraseña que ingreso no son iguales');
  

        $searchCaja = Caja::find($request->caja);
        $menu = new HomeController();
        
        cajaUser::insert([
          'loguse'     => $capture,
          'codcaja'    => $searchCaja->id,
          'codsucu'    => $request->nombre,
          'cedula'     => $request->cedularif,
          'created_at' => Carbon::now(),
          'password'   => 'md5'.md5(strtoupper($capture).$request->contra),
          'confirmar_password' => 'md5'.md5(strtoupper($capture).$request->Confircontra),
          'remember_token' => $request->_token,
          'codsuc_reg' => $suc,
          'codigoid' => $cod,
          'activo'    => true
        ]);
        
        Session::flash('success','se ha registrado');
        return redirect()->route('reiniCaja.lista');
      

          DB::commit();
      } catch (Exception $e) {
        
          DB::rollback();
        Session::flash('error','no se ha registrado o no cumple la clave especifica, '.$e->getMessage());
        return redirect()->route('reiniCaja.lista');

      }

    }

    public function delete($id)
    {

      $search = cajaUser::find($id)->delete();

      return response()->json([

        'titulo' => 'SE HA ELIMINADO CON EXITO'

      ]);
    }

    public function update($id)
    {
      
      $desc=Helper::desencriptar($id);
      $sep=explode('-', $desc);
      $ID= $sep[1].'-'.$sep[2];
      $cajas = Caja::whereNull('deleted_at')->get();
      $search = cajaUser::where('codigoid',$ID)->first();
      $string = '<option value="0" disabled >Elige Una Caja</option>';
      
    
     foreach ($cajas as $key ) {
          $string .= '<option value="'.$key->id.'">'.$key->descaj.'</option>';

     }     

      if ($search) {
          $string .= '<option selected value="'.$search->caja->id.'">'.$search->caja->descaj.'</option>';
          }                
    
     return response()->json([

        'password' => 'md5'.md5(strtoupper($this->auth->user()->loguse).$search->password),
        'loguse' => $search->loguse,
        'codcaja' => $string,
        'codigoid' => $search->codigoid,
        'nombre' => $search->usuarios->nomuse,


      ]);   
    }

    public function updateCaja(Request $request,$id)
    {
            //dd('md5'.md5(strtoupper($request->id2).$request->id3));
            $request->validate([
              'id3' => 'required|min:8|max:16',//valida las contraseña su longitud
              'id5' => 'required|min:8|max:16'
            ]);
            try{
              DB::beginTransaction();
              
              if ($request->ajax()) {
              
             if (strcmp($request->id3, $request->id5) !== 0) {
                  throw new \Exception('LA CONTRASEÑA NO SON IGUALES');
                  
              }elseif (strcmp($request->id3, $request->id5) === 0) {    
                
               $search = cajaUser::where('codigoid',$id)->where('loguse',$request->id2)->first();
               //$this->auth->user()->loguse
               $search->loguse = $request->id2;
               $search->password = 'md5'.md5(strtoupper($request->id2).$request->id3);
               $search->confirmar_password = 'md5'.md5(strtoupper($request->id2).$request->id5);
               $search->codcaja = $request->id4;
               $search->activo = 'FALSE';
               $search->save();
                DB::commit();
               return response()->json([

                  'titulo' =>Session::flash('success', 'SE HA ACTUALIZADO CON EXITO') 
            
              ]);
               

            }

           }

            }catch(\Exception $e){
              DB::rollback();
              return response()->json([
               'titulo' => Session::flash('error',strtoupper($e->getMessage()))
            ]);
      }
    
    }

    public function abrirCaja(Request $request,$id)
    {
      if ($request->ajax()) {

        $user = decrypt($id);   
        $cajaUser = cajaUser::where('loguse',$user)->whereNull('deleted_at')->first();

        if ($cajaUser != null) {
      
          return response()->json([

            'cajaNombre' => $cajaUser->caja->descaj,

          ]);
      
        }else{

          return response()->json([

            'titulo' => 'NO TIENE CAJA ASOCIADA'
          ]);
      
        }

      }

    }

    public function updatedWindows($id,Request $request)
    {

     
      $cajaU = cajaUser::whereNull('deleted_at')/*->whereHas('caja',function ($q) use ($request)
      {
          $q->where('impfishost',$request->ip());
      })*/->with('caja')->where('loguse',Auth::user()->loguse)->get();
        
          $string = '<option disabled selected="selected" value="0">Eliges Una caja asociadas</option>';
          foreach ($cajaU as $key) {
            $string .= '<option value="'.$key->caja->id.'">'.$key->caja->descaj.'</option>';
          }

      if($request->ajax()){
        
        $decrypt = decrypt($id);

        $activo = cajaUser::where('loguse',$decrypt)->where('activo','TRUE')->first();

        return $activo ? response()->json([
          'status'     => $activo->activo,
          'nombreCaja' => $activo->caja->descaj,
          'cajas' => !$cajaU->isEmpty() ? $string : null
        ]) : response()->json([
          'status' => null,
          'cajas' => !$cajaU->isEmpty() ? $string : null
        ]);

      }
    }


    public function closeBox(Request $request,$id){
      
      if ($request->ajax()) {
        $decry = decrypt($id);
        $inactivo = cajaUser::where('loguse',$decry)->where('activo','TRUE')->first();
        $inactivo->activo = 'FALSE';
        $inactivo->save();

        return response()->json([

          'titulo' => 'CAJA CERRADA'

        ]);
      }

    }



}