<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Caartalmubi;
use App\Almacen;

use Illuminate\Support\Facades\Auth;
use Helper;
use Session;    
use Exception;
use Carbon\Carbon;
use \Fpdf;
use App\Http\Controllers\generadorPDF;
use App\Segususuc;
use App\Facategoria;
use App\Caregart;
use App\Repositories\{SucursalRepository};



class EstatusInventarioController extends Controller
{
    private $sucursalAu;

    public function __construct(SucursalRepository $sucursalAu)
    {
      $this->sucursalAu = $sucursalAu; 
      $this->middleware('auth');
    }

    public function index(){
       
          if(Auth::user()->role->nombre === 'ADMIN')
               $almacen = Almacen::getWithExist();
          else $almacen = Almacen::getWithExist2();//muestra el almacen asociada a la sucursal del usuario del que se autenticó

          
          return view('almacen.estatusInventario')
                    ->with([
                        'menus'=> Helper::getmenu(),
                        'almacenes' => isset($almacen) ? $almacen : ''
                    ]);
    }

    public function view(Request $request){

      try {
        if(!$request->almacen) throw new Exception("Debe seleccionar un almacén", 1);
        $invent = Caregart::whereHas('costoPro')->where('tipreg','F')->where('desart', 'ilike', '%'.$request->filtro.'%')
        ->whereHas('almacenubi',function($q) use ($request){
          $q->where('exiact','>',0)->where('codalm',$request->almacen);
        })->withSum('almacenubi','exiact')->get();
        if(count($invent) === 0) throw new Exception("No se encontraron artículos", 1);

          if(Auth::user()->role->nombre === 'ADMIN')
               $almacen = Almacen::getWithExist();
          else $almacen = Almacen::getWithExist2();
  
          return response()->json(['articulos' => $invent], 200);
          
        /* return view('almacen.estatusInventario')
                    ->with(['menus'=> Helper::getmenu(),
                            'almacenes' => isset($almacen) ? $almacen : '',
                            'selectedAlm' => $request->almacen,
                            'inventario' => $invent,
                            'nomalm '=> Almacen::where('codalm',$request->almacen)->first()->nomalm,
                            'filtro' => $request->filtro ?? ''
                            ]); */
      } catch (Exception $e) {
        return response()->json(['error' => $e->getMessage(), 'message' => $e->getMessage()], 500);
      }
       
    }

    // public function filtro(Request $request){

    // }

    // php

    public function ajax(){

    }

    public function vistaReporte(){
        $almacenes = Almacen::where('codsuc',session('codsuc'))->get();
        $articulos = Caartalmubi::where('codsuc',session('codsuc'))->with('articulos')->whereHas('articulos')
        ->get()->groupBy(function ($q) {
         
          return $q['articulos']['codart'];
        
        })->toArray();
        $categorias = Caartalmubi::where('codsuc',session('codsuc'))->with('articulos.facategoria')->whereHas('articulos.facategoria')->get()->groupBy([
          function($q){
            return $q['articulos']['facategoria']["codigoid"];
          }
        ])->toArray();
        $articulos = array_keys($articulos);
        $categorias = array_keys($categorias);
        $articulos = Caregart::whereIn('codart',$articulos)->select(['codart','desart'])->get();
        $categorias = Facategoria::whereIn('codigoid',$categorias)->select(['codigoid','nombre'])->get();

        return view('/reportes/inventarioReporte')
        ->with([
          'menus'=> Helper::getmenu(),
          'almacenes' => $almacenes,
          'categorias' => $categorias,
          'productos' => $articulos
        ]);
    }

    public function generarReporte(Request $request){

         // $desde = $request['desde'];
         // $hasta = $request['hasta'];
         // $diames = $request['diames'];
        //  switch ($diames) {
        //     case 'diario':
        //        $tipo = 'DIARIO';
        //     break;
        //     case 'ayer':
        //         $tipo = 'DÍA ANTERIOR';
        //     break;
        //     case 'semanal':
        //        $tipo = "SEMANAL";
        //     break;
        //     case 'semant':
        //        $tipo = "SEMANA ANTERIOR";
        //     break;
        //     case 'mensual':
        //         $tipo = 'MENSUAL';
        //     break;
        //     case 'mesant':
        //       $tipo = 'MES ANTERIOR';
        //     break;
        //     case 'anual':
        //       $tipo = 'ANUAL';
        //     break;
        //     case 'antyear':
        //       $tipo = 'AÑO ANTERIOR';
        //     break;
        //     default:
        //       $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
        //     break;

        // }

      try{
            if(!$request->almacen)throw new Exception("Debe seleccionar un almacen");
            if($request->almacen =="TODAS"){
                $general = true;
            }
            else{
                $general = false;
                $codalm = $request->almacen;
            }

        }
        catch(Exception $e){
            return redirect()->route('reportesERAlmacen')->with('error',$e->getMessage());
        }
        
      ///EVALUA LOS PERIODOS DE TIEMPO SELECCIONADOS Y SI SERA UNA CONSULTA GENERALIZADA O POR SUCURSAL
    try{
            //termina el if mayor por dias

            $nomalm=Almacen::where('codalm',$request->almacen)->first()->nomalm;
            
            
            if(isset($request->almacen) && (isset($request->codart) === false)){
              $existencias = Caartalmubi::where('codalm',$request->almacen)
              ->where('exiact','>=',0)
              ->with('ubicaciones')
              ->whereHas('ubicaciones')
              ->with('articulos')
             /* ->whereHas('articulos', function ($q) use($request){
                  $q//->whereNotNull('codsubcat')
                  // ->whereNotNull('codcolor')
                  ->whereNotNull('codcat');
              })*/->with('precioArt')->whereHas('precioArt',function($q){
                  $q->where('faartpvp.status','A');
              })
              ->with('almacen')
              ->with('tallaArt')
              ->with('color')
              ->with('articulos.costoPro')
              ->with('articulos.moneda')
              ->category()
              ->orderby('codart')
              ->get();
              
            }elseif(isset($request->almacen) && isset($request->codcat)){

              $existencias = Caartalmubi::where('codalm',$request->almacen)
              ->where('exiact','>=',0)
              ->with('ubicaciones')
              ->whereHas('ubicaciones')
              ->with('articulos')
              ->whereHas('articulos', function ($q) use($request){
                  $q->where('codcat',$request->codcat);
              })->with('precioArt')->whereHas('precioArt',function($q){
                  $q->where('faartpvp.status','A');
              })
              ->with('almacen')
              ->with('tallaArt')
              ->with('color')
              ->with('articulos.costoPro')
              ->with('articulos.moneda')
              ->category()
              ->orderby('codart')
              ->get();



            }elseif(isset($request->almacen) && isset($request->codart)){

              $existencias = Caartalmubi::where('codalm',$request->almacen)
              ->where('codart',$request->codart)
              ->where('exiact','>=',0)
              ->with('ubicaciones')
              ->whereHas('ubicaciones')
              ->with('articulos')
              ->with('precioArt')->whereHas('precioArt',function($q){
                  $q->where('faartpvp.status','A');
              })
              ->with('almacen')
              ->with('tallaArt')
              ->with('color')
              ->with('articulos.costoPro')
              ->with('articulos.moneda')
              ->category()
              ->orderby('codart')
              ->get();
            }


            
            if($existencias->isEmpty()){
                Session::flash('error', 'No existen registros con los parametros de busqueda ingresados');
                return redirect()->route('reportesERAlmacen');
            }

            $group = $existencias->groupBy(function($q){
                return Facategoria::where('codigoid',$q["articulos"]["codcat"])->first()->nombre;
            });
            

            $totalExistencia = $existencias->sum('exiact');
            //dd($totalExistencia);
            $sumCategory = $group->map(function($q){
                return $q->sum('exiact');
            });

    }//try
    catch(Exception $e){
        Session::flash('error', 'Se presentó un problema en la generación del reporte, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
        return redirect()->route('reportesERAlmacen');
    }
            $pdf = new generadorPDF();
/*
            return $existencias;*/

            $titulo = 'REPORTE DE EXISTENCIA ALMACEN';
            $total = $existencias->count();
            $totalExist = 0;
            $fecha = date('Y-m-d');
    
            $pdf->AddPage('L');
            $pdf->SetTitle('REPORTE DE EXISTENCIA ALMACEN');
            $pdf->SetFont('arial','B',16);
            $pdf->SetWidths(array(90,90));
            $pdf->Ln();
            $pdf->Cell(0,22,utf8_decode($titulo),0,0,'C');
            $pdf->Ln();
            $pdf->SetFont('arial','B',10);
            // if($tipo == 'Intervalo'){
            //     $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo.' '.$desde.' - '.$hasta),0,0,'L');
            // }
            // else{
            //     $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo),0,0,'L');
            // }
            $pdf->Ln(5);
            $pdf->Cell(0,0,utf8_decode('ALMACÉN: '.$nomalm),0,0,'L');
            $pdf->Ln(5);
            $pdf->Cell(0,0,utf8_decode('FECHA: '.date("d-m-Y", strtotime($fecha))),0,0,'L');
            $pdf->Ln(5);
            $pdf->Cell(0,0,utf8_decode('TOTAL ARTICULOS: '.$total),0,0,'L');
            $pdf->Ln(5);
            foreach($sumCategory as $category => $value){
                $pdf->Cell(0,0,utf8_decode($category.' : '.$value),0,1,'L');
                $pdf->Ln(5);
            }
            $pdf->SetFont('arial','B',9,5);
            $pdf->SetFillColor(2,157,116);
            $pdf->Cell(25,8,utf8_decode('COD.ARTICULO'),1,0,'C');
            $pdf->Cell(50,8,utf8_decode('UBICACION'),1,0,'C');
            $pdf->Cell(110,8,utf8_decode('ARTICULO'),1,0,'C');
            $pdf->Cell(30,8,utf8_decode('TALLA ART.'),1,0,'C');
            $pdf->Cell(35,8,utf8_decode('EXISTENCIA'),1,0,'C');
            $pdf->Cell(26,8,utf8_decode('NP'),1,0,'C');
            $pdf->Ln();
            $pdf->SetWidths(array(25,50,110,30,35,26));
            $pdf->SetAligns(['C','C','C','C','C','C']);
            $pdf->SetFont('arial','',9,5);


            foreach($existencias as $existencia){

            if(!$existencia->tallas){
                        $talla = 'N/A';
                  $pdf->Row(array(
                  $existencia->articulos->codart,
                  $existencia->ubicaciones->nomubi,
                  $existencia->articulos->desart,
                  $talla,
                  $existencia->exiact,
                  $existencia->codsgp === null ? "N/P" : $existencia->codsgp
                ));
            } 
            else{
                  $pdf->Row(array(
                    $existencia->articulos->codart,
                    $existencia->ubicaciones->nomubi,
                    $existencia->articulos->desart,
                    $existencia->tallas->tallas,
                    $existencia->exiact,
                    $existencia->codsgp === null ? "N/P" : $existencia->codsgp
                ));
            }
            $totalExist+=$existencia->exiact; 
            }
            $pdf->Ln(5);
            $pdf->SetFont('arial','B',10,5);
	     	    $pdf->SetFillColor(2,157,116);
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(0,0,utf8_decode('TOTAL DE ARTICULOS EN INVENTARIO : '.number_format($totalExist,0,',','.')),0,0,'R');
            $pdf->SetTextColor(0,0,0);
            $pdf->Ln(5);
            $pdf->Output('I','EXISTENCIA ALMACEN-'.$fecha.'pdf');
            
           exit;
    }
}
