<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\Caregart;
use App\Famoneda;
use App\Facategoria;
use App\Faartfac;
use App\Fafactur;
use App\Tallas;
use Illuminate\Support\Facades\Auth;
use App\Sucursal;
use Helper;
use Exception;
use Session;
use Carbon\Carbon;
use \Fpdf;
use App\Http\Controllers\generadorPDF;
use App\Fanotcre;
use App\Models\Caja;

class ProductoVendidoController extends Controller
{
    function __construct()
    {
      $this->middleware('auth');
    }
    public function vistaReporte(){
        $menu = new HomeController();
        if(Auth::user()->role->nombre == 'ADMIN')$sucursales = Sucursal::get();
        else $sucursales = Sucursal::where('codsuc',Auth::user()->userSegSuc->codsuc)->get();
        return view('/reportes/productosVendidosReportes')->with('menus',$menu->retornarMenu())->with('sucursales',$sucursales);
    }

    public function vistaReporteP(){
      $menu = new HomeController();
      if(Auth::user()->role->nombre == 'ADMIN')$sucursales = Sucursal::get();
      else $sucursales = Sucursal::where('codsuc',Auth::user()->userSegSuc->codsuc)->get();
      $moneda=Famoneda::get();
      $productos= Caregart::where('tipreg','F')->get();
      return view('/reportes/resumenProductosVendidos')->with('menus',$menu->retornarMenu())->with('sucursales',$sucursales)->with('moneda',$moneda)->with('productos',$productos);
  }

  public function reporteRProductoV(Request $request)
  {
    $desde = $request['desde'];
    $hasta = $request['hasta'];
    $caja = $request['caja'];
    $diames = $request['diames'];
    $codsuc = null;// agregue por que no estaba definida
    $desde1 = '';
    $hasta1 = '';
    $tipo2 = '';

    switch ($diames) {
      case 'diario':
         $tipo = 'DIARIO '.$desde;
      break;
      case 'ayer':
          $tipo = 'DÍA ANTERIOR '.$desde;
      break;
      case 'semanal':
         $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
      break;
      case 'semant':
         $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
      break;
      case 'mensual':
          $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
      break;
      case 'mesant':
        $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
      break;
      case 'anual':
        $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
      break;
      case 'antyear':
        $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
      break;
      default:
      if($desde==null || $hasta==null)
      {
        Session::flash('error', 'Debe Seleccionar el Parametros de Fecha a Consultar');
          return redirect()->route('reportesRPvendidos');
      }
        $fc=explode(' - ', $desde);
        list($y,$m,$d) = explode("-",$fc[0]);
        $desde1=$d.'-'.$m.'-'.$y;
        $fch=explode(' - ', $hasta);
        list($y,$m,$d) = explode("-",$fch[0]);
        $hasta1=$d.'-'.$m.'-'.$y;
        $tipo = 'INTERVALO ENTRE '.$desde1.' HASTA '.$hasta1;
        $tipo2 = 'INTERVALO';
      break;

    }//switch

    try{
      if(!$request->sucursal)throw new Exception("Debe seleccionar una sucursal");
        if($request->caja==NULL)
        {   
            $general = true;
            $codsuc = $request->sucursal;//aqui agregue yo esta variable para que tome el valor para la consulta
        }
        else{
            $general = false;
            $codsuc = $request->sucursal;
        }
    }
    catch(Exception $e){
        return redirect()->route('reportesRPvendidos')->with('error',$e->getMessage());
    }//catch

    try{
      if(!$request->tipo)throw new Exception("Debe seleccionar un tipo");
        if($request->tipo=='T')
        {   
            $tipo = false;
        }
        else{
          if(!$request->producto)throw new Exception("Debe seleccionar un producto");
            $tipo = true;
            $prod = $request->producto;
        }
    }
    catch(Exception $e){
        return redirect()->route('reportesRPvendidos')->with('error',$e->getMessage());
    }//catch

    try{
      $cajas = Caja::wheredescaj($caja)->first();  
      
      $turno = null;
      if(isset($request->all()['turno']))
          $turno = $request->all()['turno'];

       if($general){
          if($request->tipo=='T')  
            $productosV = Faartfac::distinct()->select('codart','desart')->with('sucursal')
            ->whereHas('factura',function($q) use($codsuc,$desde,$hasta,$cajas){
              $q->where('codcaj','=',$cajas->id)
                  ->where('fecfac','>=',$desde)
                  ->where('fecfac','<=',$hasta);
            })            
            ->where('codsuc','=',$codsuc)
            ->orderby('codart')
            ->get();
          else
          $productosV = Faartfac::distinct()->select('codart','desart')->with('sucursal')
          ->whereHas('factura',function($q) use($codsuc,$desde,$hasta,$cajas){
            $q->where('codcaj','=',$cajas->id)
                ->where('fecfac','>=',$desde)
                ->where('fecfac','<=',$hasta);
          })      
          ->where('codart',$prod)      
          ->where('codsuc','=',$codsuc)
          ->orderby('codart')
          ->get();

            $refs 		  =  $productosV->toArray();
       }
       else{ 
        
        if($request->tipo=='T')  
            $productosV = Faartfac::distinct()->select('codart','desart')->whereHas('factura',function($q) use($codsuc,$desde,$hasta,$cajas,$request,$turno){
                 $q->where('codcaj','=',$cajas->id)
                  ->where('is_fiscal',$cajas->is_fiscal)
                  ->where('fecfac','>=',$desde)
                  ->where('fecfac','<=',$hasta)
                  ->where('codsuc','=',$codsuc)
                  ->when(isset($request->all()['turno']) === true,function ($q) use ($turno){
                    switch ($turno) {
                      case 'vespertino':
                        $q->where('turnos',$turno);
                        break;
                      case 'matutino':
                        $q->where('turnos',$turno);
                        break;
                      case 'completo':
                       $q->where('turnos',$turno);
                       break;
                       
                       case 'general':     
                          
                        return ;
                       break;
                    }
                  })->orWhere('fecanu','>=',$desde)
                    ->where('fecanu','<=',$hasta);  

               })->with('sucursal','factura')
                 ->orderby('codart','DESC')
                 ->where('codsuc','=',$codsuc)->get();
          else
            $productosV = Faartfac::distinct()->select('codart','desart')->whereHas('factura',function($q) use($codsuc,$desde,$hasta,$cajas){
              $q->where('codcaj','=',$cajas->id)
              ->where('is_fiscal',$cajas->is_fiscal)
              ->where('fecfac','>=',$desde)
              ->where('fecfac','<=',$hasta) 
              ->when(isset($request->all()['turno']) === true,function ($q) use ($turno){
                switch ($turno) {
                  case 'vespertino':
                    $q->where('turnos',$turno);
                    break;
                  case 'matutino':
                    $q->where('turnos',$turno);
                    break;
                  case 'completo':
                   $q->where('turnos',$turno);
                   break;
                   
                   case 'general':     
                      
                    return ;
                   break;
                }
              })->orWhere('fecanu','>=',$desde)
                    ->where('fecanu','<=',$hasta);       
            })->with('sucursal','factura')
              ->where('codart',$prod)
              ->orderby('codart')
              ->where('codsuc','=',$codsuc)->get();



              $refs 		  =  $productosV->toArray();
                 
        }//else
        
        
        if($cajas->is_fiscal === "true"){

          //Factura Anuladas en el rango de fecha seleccionada
          $FacturasAnul = Fanotcre::where('fecnot','>=',$desde)
                  ->where('fecnot','<=',$hasta)
                  ->with('perteneceFactura.articulos')
                  ->get();
          
          $FacturasAnul = $FacturasAnul->map(function ($var)
          {
            return $var->perteneceFactura->articulos;
          });
          $productosV = $productosV->merge($FacturasAnul->flatten())->sortBy('reffac');
        }
        

    
    if($productosV->isEmpty()){
        Session::flash('error', 'No existen registros con los parametros de busqueda ingresados');
        return redirect()->route('reportesRPvendidos');}
  }//try
  catch(Exception $e){
      Session::flash('error', 'Se presentó un problema en la generación del reporte, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
      return redirect()->route('reportesRPvendidos');
  }//catch

  $nomsucu = Sucursal::where('codsuc','=',$codsuc)->first()->nomsucu;
  $dirfis = Sucursal::where('codsuc','=',$codsuc)->first()->dirfis;

  $pdf = new generadorPDF();
   
  $facturas = Fafactur::with('cliente','sucursales','articulos','pagos','cajero',
  'notaCredito2')

    ->date2($desde,$hasta)

    ->where('codsuc','=',$codsuc)->orderby('reffac')
    ->where('codcaj','=',$cajas->id)
    ->where('is_fiscal',$cajas->is_fiscal)
    ->when(isset($request->all()['turno']) === true,function ($q) use ($turno){
    switch ($turno) {
      case 'vespertino':
        $q->where('turnos',$turno);
        break;
      case 'matutino':
        $q->where('turnos',$turno);
        break;

      case 'completo':
      $q->where('turnos',$turno);
      break;
      
      case 'general':     
        
      return ;
      break;
    }
    })
    ->get();


  $titulo = 'RESUMEN DE PRODUCTOS VENDIDOS';
  $total = $productosV->count();
  $fecha = date('Y-m-d');

  $pdf->AddPage('P');
  $pdf->SetTitle('RESUMEN DE PRODUCTOS VENDIDOS');
  $pdf->SetFont('arial','B',16);
	$pdf->SetWidths(array(90,90));
	$pdf->Ln();
	$pdf->Cell(0,22,utf8_decode($titulo),0,0,'C');
	$pdf->Ln();
	$pdf->SetFont('arial','B',10);
  $pdf->SetFillColor(255,255,255);

  $pdf->Cell(180,5,utf8_decode('TIENDA: '.$nomsucu),0,0,'L');
  $pdf->Ln();
  $pdf->Cell(180,5,utf8_decode('CAJA: '.$cajas->descaj),0,0,'L');
  $pdf->Ln();
  $pdf->Multicell(180,5,utf8_decode('SUCURSAL: '.$dirfis),0,1,'L');
  
  if($tipo == 'Intervalo' || $request->tipo=='T'){
    $pdf->Cell(180,5,utf8_decode('TIPO DE REPORTE: '.$tipo.' '.$desde.' - '.$hasta),0,0,'L');
    $pdf->Ln();
  }
  else{
    $pdf->Cell(180,5,utf8_decode('TIPO DE REPORTE: '.$tipo),0,0,'L');
    $pdf->Ln();
  }
  if(isset($turno))
      $pdf->Cell(180,5,utf8_decode('TURNO: '. strtoupper($turno)),0,0,'L');  

      
      $pdf->Ln();
      $first = $facturas->first()->reffac;
      
      $last  = $facturas->last()->reffac;
      

    $pdf->Cell(120,5,utf8_decode('PRIMERA FACTURA: '.$first),0,1,'L');
    $pdf->Cell(120,5,utf8_decode('ULTIMA FACTURA:  '.$last),0,1,'L');
    $pdf->Ln(10);
    $pdf->SetFont('arial','B',8,5);
    $pdf->SetFillColor(2,157,116);
    $pdf->Cell(25,8,utf8_decode('Código'),1,0,'C');
    $pdf->Cell(47,8,utf8_decode('Producto'),1,0,'C');
    $pdf->Cell(23,8,utf8_decode('Und. Vendidas'),1,0,'C');
    $pdf->Cell(23,8,utf8_decode('Und. Devueltas'),1,0,'C');
    $pdf->Cell(23,8,utf8_decode('Neto Devolución'),1,0,'C');
    $pdf->Cell(23,8,utf8_decode('Neto Factura'),1,0,'C');
    $pdf->Cell(23,8,utf8_decode('Monto Total Neto'),1,0,'C');
    $pdf->Ln();
    $pdf->SetWidths(array(25,47,23,23,23,23,23));
    $pdf->SetAligns(['C','C','R','R','R','R','R']);
    $pdf->SetFont('arial','',8,5);
    $totcantvend=$totcantdev=$totnetdev=$totnetvend=0;
    
    foreach ($refs as $producto) {
        
        
        $undvend=Faartfac::whereHas('factura',function($q) use($codsuc,$desde,$hasta,$cajas,$request,$turno){
          $q->where('codcaj','=',$cajas->id)
           ->where('is_fiscal',$cajas->is_fiscal)
           ->where('fecfac','>=',$desde)
           ->where('fecfac','<=',$hasta)
           ->where('codsuc','=',$codsuc)

           ->when(isset($request->all()['turno']) === true,function ($q) use ($request,$turno){
            switch ($turno) {
              case 'vespertino':
                $q->where('turnos',$turno);
                break;
              case 'matutino':
                $q->where('turnos',$turno);
                break;
              case 'completo':
               $q->where('turnos',$turno);
               break;
               
               case 'general':     
                      
                return ;
               break;
            }
          })/*->orWhere('fecanu','>=',$desde)
          ->where('fecanu','<=',$hasta)*/;   

        })->where('codsuc','=',$codsuc)->where('codart','=',$producto['codart'])->sum('cantot');

        $netvend=Faartfac::whereHas('factura',function($q) use($codsuc,$desde,$hasta,$cajas,$request,$turno){
          $q->where('codcaj','=',$cajas->id)
            ->where('is_fiscal',$cajas->is_fiscal)
           ->where('fecfac','>=',$desde)
           ->where('fecfac','<=',$hasta)
           ->where('codsuc','=',$codsuc)

           ->when(isset($request->all()['turno']) === true,function ($q) use ($request,$turno){
            switch ($turno) {
              case 'vespertino':
                $q->where('turnos',$turno);
                break;
              case 'matutino':
                $q->where('turnos',$turno);
                break;
              case 'completo':
               $q->where('turnos',$turno);
               break;
               
               case 'general':     
                  
                return ;
               break;
            }
          })/*->orWhere('fecanu','>=',$desde)
          ->where('fecanu','<=',$hasta)*/;    
        })->where('codsuc','=',$codsuc)->where('codart','=',$producto['codart'])->sum('costo');

        $unddev=Faartfac::whereHas('factura',function($q) use($codsuc,$desde,$hasta,$cajas,$request,$turno){
          $q->where('codcaj','=',$cajas->id)
            ->where('is_fiscal',$cajas->is_fiscal)
           ->where('fecanu','>=',$desde)
           ->where('fecanu','<=',$hasta)
           ->where('status','=','N')
           ->where('codsuc','=',$codsuc)

           ->when(isset($request->all()['turno']) === true,function ($q) use ($request,$turno){
            switch ($turno) {
              case 'vespertino':
                $q->where('turnos',$turno);
                break;
              case 'matutino':
                $q->where('turnos',$turno);
                break;
              case 'completo':
               $q->where('turnos',$turno);
               break;
               
               case 'general':     
                  
                return ;
               break;
            }
          })/*->orWhere('fecanu','>=',$desde)
            ->where('fecanu','<=',$hasta)*/;    


        })->where('codsuc','=',$codsuc)->where('codart','=',$producto['codart'])->sum('cantot');

        $netdev=Faartfac::whereHas('factura',function($q) use($codsuc,$desde,$hasta,$cajas,$request,$turno){
          $q->where('codcaj','=',$cajas->id)
          ->where('is_fiscal',$cajas->is_fiscal) 
          ->where('fecanu','>=',$desde)
          ->where('fecanu','<=',$hasta)
          ->where('status','=','N')
          ->where('codsuc','=',$codsuc)

          ->when(isset($request->all()['turno']) === true,function ($q) use ($request,$turno){
            switch ($turno) {
              case 'vespertino':
                $q->where('turnos',$turno);
                break;
              case 'matutino':
                $q->where('turnos',$turno);
                break;
              case 'completo':
               $q->where('turnos',$turno);
               break;
               
               case 'general':     
                  
                return ;
               break;
            }
          })/*->orWhere('fecanu','>=',$desde)
          ->where('fecanu','<=',$hasta)*/;    


        })->where('codsuc','=',$codsuc)->where('codart','=',$producto['codart'])->sum('costo');
        

        $pdf->Row(array($producto['codart'] ,substr($producto['desart'],0,80),number_format($undvend,2,',','.'),
        number_format($unddev?$unddev:0,2,',','.'),number_format($netdev?$netdev:0,2,',','.'),
        number_format($netvend?$netvend:0,2,',','.'),number_format(($netvend-$netdev),2,',','.'))); 

        if($pdf->getY()>250)
         {
          $pdf->AddPage('P');
          $pdf->ln(10);
         } 
        //dd($pdf->getY());
        $totcantdev+=$unddev;
        $totcantvend+=$undvend;
        $totnetdev+=$netdev;
        $totnetvend+=$netvend;
    }//foreach
#dd($totnetdev);

    $pdf->SetFont('arial','B',8,5);
    $pdf->SetFillColor(2,157,116);
    $pdf->Cell(25,8,utf8_decode(''),0,0,'C');
    $pdf->Cell(47,8,utf8_decode('Total General'),0,0,'C');
    $pdf->Cell(23,8,number_format($totcantvend,2,',','.'),0,0,'R');
    $pdf->Cell(23,8,number_format($totcantdev,2,',','.'),0,0,'R');
    $pdf->Cell(23,8,number_format($totnetdev,2,',','.'),0,0,'R');
    $pdf->Cell(23,8,number_format($totnetvend,2,',','.'),0,0,'R');
    $pdf->Cell(23,8,number_format($totnetvend-$totnetdev,2,',','.'),0,0,'R');
    $pdf->ln(5);
    $pdf->SetFont('arial','B',10,5);
	  $pdf->SetFillColor(2,157,116);
    $pdf->Output('I','REPORTE DE VENTAS-'.$fecha.'pdf');
            
  exit;
  }//resumenproductosvendidos

  public function reporteProductoV22(Request $request){
    
    $desde = $request['desde'];
    $hasta = $request['hasta'];
    $caja = $request['caja'];
    $diames = $request['diames'];
    $codsuc = null;// agregue por que no estaba definida
    $desde1 = '';
    $hasta1 = '';
    $tipo2 = '';
    switch ($diames) {
       case 'diario':
          $tipo = 'DIARIO '.$desde;
       break;
       case 'ayer':
           $tipo = 'DÍA ANTERIOR '.$desde;
       break;
       case 'semanal':
          $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
       break;
       case 'semant':
          $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
       break;
       case 'mensual':
           $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
       break;
       case 'mesant':
         $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
       break;
       case 'anual':
         $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
       break;
       case 'antyear':
         $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
       break;
       default:
       if($desde==null || $hasta==null)
       {
         Session::flash('error', 'Debe Seleccionar el Parametros de Fecha a Consultar');
           return redirect()->route('reportesProductosV');
       }
         $fc=explode(' - ', $desde);
         list($y,$m,$d) = explode("-",$fc[0]);
         $desde1=$d.'-'.$m.'-'.$y;
         $fch=explode(' - ', $hasta);
         list($y,$m,$d) = explode("-",$fch[0]);
         $hasta1=$d.'-'.$m.'-'.$y;
         $tipo = 'INTERVALO ENTRE '.$desde1.' HASTA '.$hasta1;
         $tipo2 = 'INTERVALO';
       break;

   }



   try{
       if(!$request->sucursal)throw new Exception("Debe seleccionar una sucursal");
       //dd($request->caja);
       // if($request->sucursal =="TODAS"){
       if($request->caja==NULL)
       {   #dd("llego");
           $general = true;
           $codsuc = $request->sucursal;//aqui agregue yo esta variable para que tome el valor para la consulta
           //dd($codsuc);
       }
       else{
           $general = false;
           $codsuc = $request->sucursal;
       }
   }
   catch(Exception $e){
       return redirect()->route('reportesProductosV')->with('error',$e->getMessage());
   }
   
 ///EVALUA la SELECCION Y SI SERA UNA CONSULTA TODAS LAS CAJAS  O POR INDIVIDUAL
 #$codcaja = Caja::where()
  # dd(abs($codcaj));
 try{
    

     $turno = null;
     if(isset($request->all()['turno']))
     $turno = $request->all()['turno'];

         $cajas = Caja::wheredescaj($caja)->first();   
          if($general){
           
               $productosV = Faartfac::with('sucursal')
               ->whereHas('factura',function($q) use($codsuc,$desde,$hasta,$cajas){
                 $q->where('codcaj','=',$cajas->id)
                   #  ->where('fecanu','>=',$desde)
                   #  ->where('fecanu','<=',$hasta)
                     ->where('fecfac','>=',$desde)
                     ->where('fecfac','<=',$hasta);
               })
               ->where('codsuc','=',$codsuc)
               ->orderby('reffac')
               ->get();
           
          }
          else{
         
               
               $productosV = Faartfac::whereHas('factura',function($q) use($codsuc,$desde,$hasta,$cajas,$request,$turno){
                  # dd($caja);
                    $q->where('codcaj','=',$cajas->id)
                     ->where('fecfac','>=',$desde)
                     ->where('fecfac','<=',$hasta)
                     ->when(isset($request->all()['turno']) === true,function ($q) use ($turno){
                      switch ($turno) {
                        case 'vespertino':
                          $q->where('turnos',$turno);
                          break;
                        case 'matutino':
                          $q->where('turnos',$turno);
                          break;
                        case 'completo':
                         $q->where('turnos',$turno);
                         break;
                         
                         case 'general':     
                          
                          return ;
                         break;
                      }
                    })->orWhere('fecanu','>=',$desde)
                      ->where('fecanu','<=',$hasta);      

                  })->with('sucursal','factura','descuento')
                    ->orderby('reffac')
                    ->where('codsuc','=',$codsuc)->get();
                
                    
                }
                   

                   $FacturasAnul = Fanotcre::where('fecnot','>=',$desde)
                   ->where('fecnot','<=',$hasta)
                   ->with('perteneceFactura.articulos')
                   ->get();
                  
                   $FacturasAnul = $FacturasAnul->map(function ($var)
                  {
                    return $var->perteneceFactura->articulos;
                  });
                  
                  $productosV = $productosV->merge($FacturasAnul->flatten())->sortBy('reffac');
                 
       //dd($productosV);
       if($productosV->isEmpty()){
           Session::flash('error', 'No existen registros con los parametros de busqueda ingresados');
           return redirect()->route('reportesProductosV');}
   }//try
   catch(Exception $e){
       Session::flash('error', 'Se presentó un problema en la generación del reporte, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
       return redirect()->route('reportesProductosV');
   }
   #dd($codsuc);
       $nomsucu = Sucursal::where('codsuc','=',$codsuc)->first()->nomsucu;
       $dirfis = Sucursal::where('codsuc','=',$codsuc)->first()->dirfis;

       $pdf = new generadorPDF();

       $titulo = 'PRODUCTOS VENDIDOS';
       $total = $productosV->count();
       $fecha = date('Y-m-d');
       
       $pdf->AddPage('L');
       $pdf->SetTitle('PRODUCTOS VENDIDOS');
       $pdf->SetFont('arial','B',16);
       $pdf->SetWidths(array(90,90));
       $pdf->Ln();
       $pdf->Cell(0,22,utf8_decode($titulo),0,0,'C');
       $pdf->Ln();
       $pdf->SetFont('arial','B',10);

       $pdf->Cell(280,5,utf8_decode('TIENDA: '.$nomsucu),0,0,'C');
       $pdf->Ln();
       $pdf->Cell(280,5,utf8_decode('CAJA: '.$cajas->descaj),0,0,'C');
       $pdf->Ln();
       $pdf->Cell(280,5,utf8_decode('SUCURSAL: '.$dirfis),0,0,'C');
       $pdf->Ln(8);

       if($tipo == 'Intervalo'){
           $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo.' '.$desde.' - '.$hasta),0,0,'C');
       }
       else{
           $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo),0,0,'C');
       }
       $pdf->Ln(5);
       $pdf->Cell(0,0,utf8_decode('FECHA: '.$fecha),0,0,'C');
       $pdf->Ln(5);
       if(isset($turno))
          $pdf->Cell(0,0,utf8_decode('TURNO: '. strtoupper($turno)),0,0,'C');
       $pdf->Ln(5);
       $pdf->Cell(0,0,utf8_decode('VENTAS EFECTUADAS: '.$total),0,0,'C');
       $pdf->Ln(10);
       $pdf->SetFont('arial','B',8,5);
        $pdf->SetFillColor(2,157,116);
        $pdf->Cell(20,8,utf8_decode('Documento'),1,0,'C');
        $pdf->Cell(25,8,utf8_decode('Cod.Artículo'),1,0,'C');
       $pdf->Cell(47,8,utf8_decode('Descripción'),1,0,'C');
       //$pdf->Cell(11,8,utf8_decode('Talla'),1,0,'C');
       $pdf->Cell(17,8,utf8_decode('NP'),1,0,'C');
       $pdf->Cell(15,8,utf8_decode('Cantidad'),1,0,'C');
       $pdf->Cell(19,8,utf8_decode('Monto Bruto'),1,0,'C');
       $pdf->Cell(10,8,utf8_decode('%'),1,0,'C');
       $pdf->Cell(16,8,utf8_decode('Recargo'),1,0,'C');
       $pdf->Cell(17,8,utf8_decode('Descuento'),1,0,'C');
       $pdf->Cell(18,8,utf8_decode('Descuento %'),1,0,'C');
       $pdf->Cell(18,8,utf8_decode('Costo'),1,0,'C');
       $pdf->Cell(20,8,utf8_decode('Utilidad'),1,0,'C');
       $pdf->Cell(15,8,utf8_decode('%Utilidad'),1,0,'C');
       $pdf->Cell(20,8,utf8_decode('Seriales'),1,0,'C');
       $pdf->Ln();
       $pdf->SetWidths(array(20,25,47,17,15,19,10,16,17,18,18,20,15,20));
       $pdf->SetAligns(['C','C','C','C','C','C','C','C','C','C','C','C','C','C']);
       $pdf->SetFont('arial','',8,5);
       $cantidad = 0;
       $montoBruto = 0;
       $recargo = 0;
       $costo = 0;
       $cantidadD = 0;
       $totalD = 0;
       $cantidadM = 0;
       
       foreach ($productosV as $producto) {
         
         $serial1 = \App\Models\Serial::withTrashed()->where('factura',$producto->reffac)->where('codart',$producto->codart)->get();
         $seriales = '';
         foreach ($serial1 as $key) {
           $seriales .= $key->serial.', ';
         }
         
         if($producto->factura->status === 'N'){//facturas NC Y ANULADAS
           

           if($producto->factura->fecfac === $producto->factura->fecanu){
             $pdf->SetTextColor(0,0,0);        
             $pdf->Row(array($producto->reffac,$producto->codart,$producto->desart,'',$producto->cantot,
                 number_format($producto->totart,2,',','.'),number_format($producto->recargo,2,',','.'),number_format($producto->monrgo,
                 2,',','.'),number_format($producto->mondes ?? 0,2,',','.'),number_format(optional($producto->Descuento)->mondesc ?? 0,2,',','.'),number_format($producto->precio,2,',','.'),
                 number_format(0,2,',','.'),'100%',$seriales)); 
                 $cantidad += $producto->cantot;
                 $montoBruto += $producto->totart;
                 $recargo += $producto->recargo;
                 $costo += $producto->precio;
                 $pdf->SetTextColor(255,0,0);        
                 $pdf->Row(array($producto->reffac,$producto->codart,$producto->desart,'',$producto->cantot*-1,
                 number_format($producto->totart*-1,2,',','.'),number_format($producto->recargo*-1,2,',','.'),number_format($producto->monrgo*-1,
                 2,',','.'),number_format($producto->mondes*-1 ?? 0,2,',','.'),number_format(optional($producto->Descuento)->mondesc*-1 ?? 0,2,',','.'),number_format($producto->precio*-1,2,',','.'),
                 number_format(0,2,',','.'),'100%',$seriales)); 
                 $cantidad += $producto->cantot;
                 $montoBruto += $producto->totart*-1;
                 $recargo += $producto->recargo*-1;
                 $costo += $producto->precio*-1;
                 $cantidadD += $producto->cantot;
                 $totalD += $producto->totart;
            }else{
             if($producto->factura->fecfac !== $producto->factura->fecanu && $desde === $hasta){//en caso de se halla devuelto la factura y consulta un dia especifico
               if ($desde >= $producto->factura->fecanu && $desde > $producto->factura->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                 $pdf->SetTextColor(255,0,0);        

                 $pdf->Row(array($producto->reffac,$producto->codart,$producto->desart,'',$producto->cantot*-1,
                 number_format($producto->totart*-1,2,',','.'),number_format($producto->recargo*-1,2,',','.'),number_format($producto->monrgo*-1,
                 2,',','.'),number_format($producto->mondes*-1 ?? 0,2,',','.'),number_format(optional($producto->Descuento)->mondesc*-1 ?? 0,2,',','.'),number_format($producto->precio*-1,2,',','.'),
                 number_format(0,2,',','.'),'100%',$seriales)); 
                 $cantidad += $producto->cantot;
                 $montoBruto += $producto->totart*-1;
                 $recargo += $producto->recargo*-1;
                 $costo += $producto->precio*-1;
                 $cantidadD += $producto->cantot;
                 $totalD += $producto->totart;
                 
               }else if($desde <= $producto->factura->fecfac){
                 $pdf->SetTextColor(0,0,0);        
     
                 $pdf->Row(array($producto->reffac,$producto->codart,$producto->desart,'',$producto->cantot,
                 number_format($producto->totart,2,',','.'),number_format($producto->recargo,2,',','.'),number_format($producto->monrgo,
                 2,',','.'),number_format($producto->mondes ?? 0,2,',','.'),number_format(optional($producto->Descuento)->mondesc ?? 0,2,',','.'),number_format($producto->precio,2,',','.'),
                 number_format(0,2,',','.'),'100%',$seriales)); 
                 $cantidad += $producto->cantot;
                 $montoBruto += $producto->totart;
                 $recargo += $producto->recargo;
                 $costo += $producto->precio;
               }
               

            
                
            }else if($producto->factura->fecfac !== $producto->factura->fecanu && $desde !== $hasta){//en caso de se halla devuelto la factura y consulta en dias diferentes
             
             if ($desde <= $producto->factura->fecfac ) {//cuando se consulta la factura que sea mayor o igual del dia
               $pdf->SetTextColor(0,0,0);        
               $pdf->Row(array($producto->reffac,$producto->codart,$producto->desart,'',$producto->cantot,
                 number_format($producto->totart,2,',','.'),number_format($producto->recargo,2,',','.'),number_format($producto->monrgo,
                 2,',','.'),number_format($producto->mondes ?? 0,2,',','.'),number_format(optional($producto->Descuento)->mondesc ?? 0,2,',','.'),number_format($producto->precio,2,',','.'),
                 number_format(0,2,',','.'),'100%',$seriales)); 
                 $cantidad += $producto->cantot;
                 $montoBruto += $producto->totart;
                 $recargo += $producto->recargo;
                 $costo += $producto->precio;
                 
                 $pdf->SetTextColor(255,0,0);        
                 
                 $pdf->Row(array($producto->reffac,$producto->codart,$producto->desart,'',$producto->cantot*-1,
                 number_format($producto->totart*-1,2,',','.'),number_format($producto->recargo*-1,2,',','.'),number_format($producto->monrgo*-1,
                 2,',','.'),number_format($producto->mondes*-1 ?? 0,2,',','.'),number_format(optional($producto->Descuento)->mondesc*-1 ?? 0,2,',','.'),number_format($producto->precio*-1,2,',','.'),
                 number_format(0,2,',','.'),'100%',$seriales)); 
                 $cantidad += $producto->cantot;
                 $montoBruto += $producto->totart*-1;
                 $recargo += $producto->recargo*-1;
                 $costo += $producto->precio*-1;
                 $cantidadD += $producto->cantot;
                 $totalD += $producto->totart;
                }elseif($producto->fecfac <= $producto->fecanu){
                 $pdf->SetTextColor(255,0,0);        
                 
                 $pdf->Row(array($producto->reffac,$producto->codart,$producto->desart,'',$producto->cantot*-1,
                 number_format($producto->totart*-1,2,',','.'),number_format($producto->recargo*-1,2,',','.'),number_format($producto->monrgo*-1,
                 2,',','.'),number_format($producto->mondes*-1 ?? 0,2,',','.'),number_format(optional($producto->Descuento)->mondesc*-1 ?? 0,2,',','.'),number_format($producto->precio*-1,2,',','.'),
                 number_format(0,2,',','.'),'100%',$seriales)); 
                 $cantidad += $producto->cantot;
                 $montoBruto += $producto->totart*-1;
                 $recargo += $producto->recargo*-1;
                 $costo += $producto->precio*-1;
                 $cantidadD += $producto->cantot;
                 $totalD += $producto->totart;
                }
             }
            }
         }else{//FACTURA ACTIVAS
            
           $pdf->SetTextColor(0,0,0);        
           $pdf->Row(array($producto->reffac,$producto->codart,$producto->desart,'',$producto->cantot,
                 number_format($producto->totart,2,',','.'),number_format($producto->recargo,2,',','.'),number_format($producto->monrgo,
                 2,',','.'),number_format($producto->mondes ?? 0,2,',','.'),number_format(optional($producto->Descuento)->mondesc ?? 0,2,',','.'),number_format($producto->precio,2,',','.'),
                 number_format(0,2,',','.'),'100%',$seriales)); 
                 $cantidad += $producto->cantot;
                 $montoBruto += $producto->totart;
                 $recargo += $producto->recargo;
                 $costo += $producto->precio;
                 $cantidadM += $producto->cantot;
                 
         }

         
       }
      
       
      # $tmontobruto = $tmontobruto-$MontoTotalB->sum();
      # $totalCant = $totalCant-$totalCantAn;
       $pdf->SetFont('arial','B',9);
      
       // $pdf->Cell(25,8,number_format($recargo,2,',','.'),0,0,'R');
       // $pdf->Cell(40,8,number_format($costo,2,',','.'),0,0,'R');
       $pdf->AddPage('L');

       $pdf->Ln(5);
       $pdf->SetTextColor(0,0,0);
       $pdf->Cell(500,8,"Cantidad Ventas del Mes :".number_format($cantidad,2,',','.'),0,0,'L');
       $pdf->Ln(5);
       $pdf->Cell(500,8,"Total Ventas del Mes :".number_format($montoBruto,2,',','.'),0,0,'L');
       $pdf->Ln(5);
       $pdf->Cell(500,8,"Cantidad Devoluciones del Mes :".number_format($cantidadD,2,',','.'),0,0,'L');
       $pdf->Ln(5);
       $pdf->Cell(500,8,"Total Devoluciones del Mes :".number_format($totalD,2,',','.'),0,0,'L');
       $pdf->Ln(5);
       $pdf->Cell(500,8,"Cantidad del Mes :".number_format($cantidad-$cantidadD ,2,',','.'),0,0,'L');
       $pdf->Ln(5);
       $pdf->Cell(500,8,"Total del Mes :".number_format((($montoBruto - $totalD) > 0) ? $montoBruto - $totalD : 0,2,',','.'),0,0,'L');

       $pdf->SetFont('arial','B',10,5);
        $pdf->SetFillColor(2,157,116);
       // $pdf->Cell(0,0,utf8_decode('MONTO TOTAL EN VENTAS: '.number_format($MontoTotal,3,',','.')),0,0,'R');
       $pdf->Output('I','REPORTE DE VENTAS-'.$fecha.'pdf');
       
      exit;
   }  


    public function reporteProductoV(Request $request){

         $desde = $request['desde'];
         $hasta = $request['hasta'];
         $caja = $request['caja'];
         $diames = $request['diames'];
         $codsuc = null;// agregue por que no estaba definida
         $desde1 = '';
         $hasta1 = '';
         $tipo2 = '';
         switch ($diames) {
            case 'diario':
               $tipo = 'DIARIO '.$desde;
            break;
            case 'ayer':
                $tipo = 'DÍA ANTERIOR '.$desde;
            break;
            case 'semanal':
               $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
            break;
            case 'semant':
               $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
            break;
            case 'mensual':
                $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
            break;
            case 'mesant':
              $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
            break;
            case 'anual':
              $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
            break;
            case 'antyear':
              $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
            break;
            default:
            if($desde==null || $hasta==null)
            {
              Session::flash('error', 'Debe Seleccionar el Parametros de Fecha a Consultar');
                return redirect()->route('reportesProductosV');
            }
              $fc=explode(' - ', $desde);
              list($y,$m,$d) = explode("-",$fc[0]);
              $desde1=$d.'-'.$m.'-'.$y;
              $fch=explode(' - ', $hasta);
              list($y,$m,$d) = explode("-",$fch[0]);
              $hasta1=$d.'-'.$m.'-'.$y;
              $tipo = 'INTERVALO ENTRE '.$desde1.' HASTA '.$hasta1;
              $tipo2 = 'INTERVALO';
            break;

        }



    	  try{
            if(!$request->sucursal)throw new Exception("Debe seleccionar una sucursal");
            //dd($request->caja);
            // if($request->sucursal =="TODAS"){
            if($request->caja==NULL)
            {   #dd("llego");
                $general = true;
                $codsuc = $request->sucursal;//aqui agregue yo esta variable para que tome el valor para la consulta
                //dd($codsuc);
            }
            else{
                $general = false;
                $codsuc = $request->sucursal;
            }
        }
        catch(Exception $e){
          dd($e);
            return redirect()->route('reportesProductosV')->with('error',$e->getMessage());
        }

      ///EVALUA la SELECCION Y SI SERA UNA CONSULTA TODAS LAS CAJAS  O POR INDIVIDUAL
      #$codcaja = Caja::where()
       # dd(abs($codcaj));
      try{
              $cajas = Caja::wheredescaj($caja)->first();
               if($general){

                    $productosV = Faartfac::with('sucursal')
                    ->whereHas('factura',function($q) use($codsuc,$desde,$hasta,$cajas){
                      $q->where('codcaj','=',$cajas->id)
                        #  ->where('fecanu','>=',$desde)
                        #  ->where('fecanu','<=',$hasta)
                          ->where('fecfac','>=',$desde)
                          ->where('fecfac','<=',$hasta);
                    })
                    ->where('codsuc','=',$codsuc)
                    ->orderby('reffac')
                    ->get();

               }
               else{

                    
                    $productosV = Faartfac::whereHas('factura',function($q) use($codsuc,$desde,$hasta,$cajas){
                       # dd($caja);
                       	$q->where('codcaj','=',$cajas->id)
                          ->where('fecfac','>=',$desde)
                          ->where('fecfac','<=',$hasta)
                          ->orWhere('fecanu','>=',$desde)
                          ->where('fecanu','<=',$hasta);

                       })->with('sucursal','factura','articuloRgo')
                         ->orderby('reffac')
                         ->where('codsuc','=',$codsuc)->get();

                        }
                        if($cajas->is_fiscal === "true"){
                          
                          $FacturasAnul = Fanotcre::where('fecnot','>=',$desde)
                          ->where('fecnot','<=',$hasta)
                          ->with('perteneceFactura.articulos')
                          ->get();
                          $FacturasAnul = $FacturasAnul->map(function ($var)
                          {
                            return $var->perteneceFactura->articulos;
                         });
                         $productosV = $productosV->merge($FacturasAnul->flatten())->sortBy('reffac');
                        }
                        


            if($productosV->isEmpty()){
                Session::flash('error', 'No existen registros con los parametros de busqueda ingresados');
                return redirect()->route('reportesProductosV');}
        }//try
        catch(Exception $e){
          
            Session::flash('error', 'Se presentó un problema en la generación del reporte, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
            return redirect()->route('reportesProductosV');
        }

            $nomsucu = Sucursal::where('codsuc','=',$codsuc)->first()->nomsucu;
            $dirfis = Sucursal::where('codsuc','=',$codsuc)->first()->dirfis;

            $pdf = new generadorPDF();

            $titulo = 'PRODUCTOS VENDIDOS';
            $total = $productosV->count();
            $fecha = date('Y-m-d');

		        $pdf->AddPage('L');
            $pdf->SetTitle('PRODUCTOS VENDIDOS');
            $pdf->SetFont('arial','B',16);
		        $pdf->SetWidths(array(90,90));
		        $pdf->Ln();
		        $pdf->Cell(0,22,utf8_decode($titulo),0,0,'C');
		        $pdf->Ln();
		        $pdf->SetFont('arial','B',10);

            $pdf->Cell(280,5,utf8_decode('TIENDA: '.$nomsucu),0,0,'C');
            $pdf->Ln();
            $pdf->Cell(280,5,utf8_decode('CAJA: '.$cajas->descaj),0,0,'C');
            $pdf->Ln();
            $pdf->Cell(280,5,utf8_decode('SUCURSAL: '.$dirfis),0,0,'C');
            $pdf->Ln(8);

            if($tipo == 'Intervalo'){
                $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo.' '.$desde.' - '.$hasta),0,0,'C');
            }
            else{
                $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo),0,0,'C');
            }
            $pdf->Ln(5);
            $pdf->Cell(0,0,utf8_decode('FECHA: '.$fecha),0,0,'L');
            $pdf->Ln(5);
            $pdf->Cell(0,0,utf8_decode('VENTAS EFECTUADAS: '.$total),0,0,'C');
            $pdf->Ln(10);
            $pdf->SetFont('arial','B',8,5);
	     	    $pdf->SetFillColor(2,157,116);
	     	    $pdf->Cell(20,8,utf8_decode('Documento'),1,0,'C');
	     	    $pdf->Cell(25,8,utf8_decode('Cod.Artículo'),1,0,'C');
            $pdf->Cell(47,8,utf8_decode('Descripción'),1,0,'C');
            $pdf->Cell(11,8,utf8_decode('Talla'),1,0,'C');
            $pdf->Cell(20,8,utf8_decode('NP'),1,0,'C');
            $pdf->Cell(15,8,utf8_decode('Cantidad'),1,0,'C');
            $pdf->Cell(19,8,utf8_decode('Monto Bruto'),1,0,'C');
            $pdf->Cell(10,8,utf8_decode('%'),1,0,'C');
            $pdf->Cell(16,8,utf8_decode('Recargo'),1,0,'C');
            $pdf->Cell(20,8,utf8_decode('Descuento'),1,0,'C');
            $pdf->Cell(18,8,utf8_decode('Costo'),1,0,'C');
            $pdf->Cell(20,8,utf8_decode('Utilidad'),1,0,'C');
            $pdf->Cell(15,8,utf8_decode('%Utilidad'),1,0,'C');
            $pdf->Cell(20,8,utf8_decode('Seriales'),1,0,'C');
            $pdf->Ln();
            $pdf->SetWidths(array(20,25,47,11,20,15,19,10,16,20,18,20,15,20));
            $pdf->SetAligns(['C','C','C','C','C','C','C','C','C','C','C','C','C','C']);
            $pdf->SetFont('arial','',8,5);
            $cantidad = 0;
            $montoBruto = 0;
            $recargo = 0;
            $costo = 0;
            $cantidadD = 0;
            $totalD = 0;
            $cantidadM = 0;
            
            foreach ($productosV as $producto) {

              $serial1 = \App\Models\Serial::withTrashed()->where('factura',$producto->reffac)
                         ->where('codart',$producto->codart)->get();
              $seriales = '';
              foreach ($serial1 as $key) {
                $seriales .= $key->serial.', ';
              }

              if($producto->factura->status === 'N'){//facturas NC Y ANULADAS


                if($producto->factura->fecfac === $producto->factura->fecanu){

                  $pdf->SetTextColor(0,0,0);
                  $pdf->Row(array($producto->reffac,$producto->codart,$producto->desart,'','',$producto->cantot,
                  number_format(($producto->articuloRgo !== null) ? $producto->totart : $producto->precio * $producto->cantot,2,',','.'),
                  number_format(($producto->articuloRgo !== null) ? $producto->recargo : 0,2,',','.'),
                  number_format(($producto->articuloRgo !== null) ? $producto->monrgo : 0,2,',','.'),
                      number_format($producto->mondes,2,',','.'),number_format($producto->precio,2,',','.'),
                      number_format(0,2,',','.'),'100%',$seriales));
                      $cantidad += $producto->cantot;
                      /*OJO PENDIENTE CON ESTA CONDICION AGREGADA */
                      if($producto->articuloRgo  !== null) $montoBruto += $producto->totart;
                      else $montoBruto += ($producto->precio * $producto->cantot);

                      if($producto->articuloRgo  !== null) $recargo += $producto->recargo;
                      else $recargo +=0;

                      $costo += $producto->precio;
                      $pdf->SetTextColor(255,0,0);

                      $pdf->Row(array($producto->reffac,$producto->codart,$producto->desart,'','',number_format($producto->cantot*-1,2,'.',','),
                      number_format(($producto->articuloRgo !== null) ? $producto->totart*-1 : ($producto->precio * $producto->cantot)*-1,2,',','.'),
                      number_format(($producto->articuloRgo !== null) ? $producto->recargo*-1 : 0,2,',','.'),
                      number_format(($producto->articuloRgo !== null) ? $producto->monrgo*-1 : 0,2,',','.'),
                      number_format($producto->mondes*-1,2,',','.'),number_format($producto->precio*-1,2,',','.'),
                      number_format(0,2,',','.'),'100%',$seriales));
                      $cantidad += $producto->cantot;

                      if($producto->articuloRgo  !== null) $montoBruto += $producto->totart*-1;
                      else $montoBruto += ($producto->precio * $producto->cantot)*-1;

                      if($producto->articuloRgo  !== null) $recargo += $producto->recargo*-1;
                      else $recargo +=0;

                      $costo += $producto->precio*-1;
                      $cantidadD += $producto->cantot;
                      $totalD += $producto->totart;
                 }else{
                  if($producto->factura->fecfac !== $producto->factura->fecanu && $desde === $hasta){//en caso de se halla devuelto la factura y consulta un dia especifico
                    if ($desde >= $producto->factura->fecanu && $desde > $producto->factura->fecfac) {//cuando se consulta la factura que sea mayor o igual del dia
                      $pdf->SetTextColor(255,0,0);

                      $pdf->Row(array($producto->reffac,$producto->codart,$producto->desart,'','',$producto->cantot*-1,
                      number_format(($producto->articuloRgo !== null) ? $producto->totart*-1 : ($producto->precio * $producto->cantot)*-1,2,',','.'),
                      number_format(($producto->articuloRgo !== null) ? $producto->recargo*-1 : 0,2,',','.'),
                      number_format(($producto->articuloRgo !== null) ? $producto->monrgo*-1 : 0,2,',','.'),
                      number_format($producto->mondes*-1,2,',','.'),number_format($producto->precio*-1,2,',','.'),
                      number_format(0,2,',','.'),'100%',$seriales));
                      $cantidad += $producto->cantot;

                      if(($producto->articuloRgo !== null)) $montoBruto += $producto->totart*-1;
                      else $montoBruto += ($producto->precio * $producto->cantot)*-1;

                      if(($producto->articuloRgo !== null)) $recargo += $producto->recargo*-1;
                      else $recargo +=0;

                      $costo += $producto->precio*-1;
                      $cantidadD += $producto->cantot;
                      $totalD += $producto->totart;

                    }else if($desde <= $producto->factura->fecfac){
                      $pdf->SetTextColor(0,0,0);

                      $pdf->Row(array($producto->reffac,$producto->codart,$producto->desart,'','',$producto->cantot,
                      number_format(($producto->articuloRgo !== null) ? $producto->totart : ($producto->precio * $producto->cantot),2,',','.'),
                      number_format(($producto->articuloRgo !== null) ? $producto->recargo : 0,2,',','.'),
                      number_format(($producto->articuloRgo !== null) ? $producto->monrgo : 0,2,',','.'),
                      number_format($producto->mondes,2,',','.'),number_format($producto->precio,2,',','.'),
                      number_format(0,2,',','.'),'100%',$seriales));
                      $cantidad += $producto->cantot;
                      $montoBruto += $producto->totart;
                      $recargo += $producto->recargo;
                      $costo += $producto->precio;



                      if($desde <= $producto->factura->fecanu && $hasta >= $producto->factura->fecanu){

                        $pdf->SetTextColor(255,0,0);

                      $pdf->Row(array($producto->reffac,$producto->codart,$producto->desart,'','',$producto->cantot*-1,
                      number_format(($producto->articuloRgo !== null) ? $producto->totart*-1 : ($producto->precio * $producto->cantot)*-1,2,',','.'),
                      number_format(($producto->articuloRgo !== null) ? $producto->recargo*-1 : 0,2,',','.'),
                      number_format(($producto->articuloRgo !== null) ? $producto->monrgo*-1 : 0,2,',','.'),
                      number_format($producto->mondes*-1,2,',','.'),number_format($producto->precio*-1,2,',','.'),
                      number_format(0,2,',','.'),'100%',$seriales));
                      $cantidad += $producto->cantot;
                      $montoBruto += $producto->totart*-1;
                      $recargo += $producto->recargo*-1;
                      $costo += $producto->precio*-1;
                      $cantidadD += $producto->cantot;
                      $totalD += $producto->totart;

                      }
                    }




                 }else if($producto->factura->fecfac !== $producto->factura->fecanu && $desde !== $hasta){//en caso de se halla devuelto la factura y consulta en dias diferentes

                  if ($desde <= $producto->factura->fecfac ) {//cuando se consulta la factura que sea mayor o igual del dia
                    $pdf->SetTextColor(0,0,0);
                    $pdf->Row(array($producto->reffac,$producto->codart,$producto->desart,'','',$producto->cantot,
                    number_format(($producto->articuloRgo !== null) ? $producto->totart : ($producto->precio * $producto->cantot),2,',','.'),
                    number_format(($producto->articuloRgo !== null) ? $producto->recargo : 0,2,',','.'),
                    number_format(($producto->articuloRgo !== null) ? $producto->monrgo : 0,2,',','.'),
                    number_format($producto->mondes,2,',','.'),number_format($producto->precio,2,',','.'),
                    number_format(0,2,',','.'),'100%',$seriales));

                    $cantidad += $producto->cantot;

                      if($producto->articuloRgo !== null) $montoBruto += $producto->totart;
                      else $montoBruto += ($producto->precio * $producto->cantot);
                      if($producto->articuloRgo !== null) $recargo += $producto->recargo;
                      else $recargo +=0;

                      $costo += $producto->precio;

                      $pdf->SetTextColor(255,0,0);

                      $pdf->Row(array($producto->reffac,$producto->codart,$producto->desart,'','',$producto->cantot*-1,
                      number_format(($producto->articuloRgo !== null) ? $producto->totart*-1 : ($producto->precio * $producto->cantot)*-1,2,',','.'),
                      number_format(($producto->articuloRgo !== null) ? $producto->recargo*-1 : 0,2,',','.'),
                      number_format(($producto->articuloRgo !== null) ? $producto->monrgo*-1 : 0,2,',','.'),
                      number_format($producto->mondes*-1,2,',','.'),number_format($producto->precio*-1,2,',','.'),
                      number_format(0,2,',','.'),'100%',$seriales));
                      $cantidad += $producto->cantot;

                      if($producto->articuloRgo !== null) $montoBruto += $producto->totart*-1;
                      else $montoBruto += ($producto->precio * $producto->cantot)*-1;
                      if($producto->articuloRgo !== null) $recargo += $producto->recargo*-1;
                      else $recargo +=0;

                      $costo += $producto->precio*-1;
                      $cantidadD += $producto->cantot;
                      $totalD += $producto->totart;

                     }elseif($producto->fecfac <= $producto->fecanu){
                      $pdf->SetTextColor(255,0,0);

                      $pdf->Row(array($producto->reffac,$producto->codart,$producto->desart,'','',$producto->cantot*-1,
                      number_format(($producto->articuloRgo  !== null) ? $producto->totart*-1 : ($producto->precio * $producto->cantot)*-1,2,',','.'),
                      number_format(($producto->articuloRgo  !== null) ? $producto->recargo*-1 : 0,2,',','.'),
                      number_format(($producto->articuloRgo  !== null) ? $producto->monrgo*-1 : 0,2,',','.'),
                      number_format($producto->mondes*-1,2,',','.'),number_format($producto->precio*-1,2,',','.'),
                      number_format(0,2,',','.'),'100%',$seriales));
                      $cantidad += $producto->cantot;
                      if($producto->articuloRgo !== null) $montoBruto += $producto->totart*-1;
                      else $montoBruto += ($producto->precio * $producto->cantot)*-1;
                      if($producto->articuloRgo !== null) $recargo += $producto->recargo*-1;
                      else $recargo +=0;

                      $costo += $producto->precio*-1;
                      $cantidadD += $producto->cantot;
                      $totalD += $producto->totart;
                     }
                  }
                 }
              }else{//FACTURA ACTIVAS

                $pdf->SetTextColor(0,0,0);
                $pdf->Row(array($producto->reffac,$producto->codart,$producto->desart,'','',$producto->cantot,
                      number_format(($producto->articuloRgo !== null) ? $producto->totart : ($producto->precio * $producto->cantot),2,',','.'),
                      number_format(($producto->articuloRgo !== null) ? $producto->recargo: 0,2,',','.'),
                      number_format(($producto->articuloRgo !== null) ? $producto->monrgo : 0,2,',','.'),
                      number_format($producto->mondes,2,',','.'),number_format($producto->precio,2,',','.'),
                      number_format(0,2,',','.'),'100%',$seriales));
                      $cantidad += $producto->cantot;
                      if($producto->articuloRgo  !== null) $montoBruto += $producto->totart;
                      else $montoBruto += ($producto->precio * $producto->cantot);
                      if($producto->articuloRgo  !== null) $recargo += $producto->recargo;
                      else  $recargo += 0;

                      $costo += $producto->precio;
                      $cantidadM += $producto->cantot;
                      //($producto->articulo->ivastatus === "true")
              }


            }


           # $tmontobruto = $tmontobruto-$MontoTotalB->sum();
           # $totalCant = $totalCant-$totalCantAn;
            $pdf->SetFont('arial','B',9);

            $pdf->AddPage('L');

            $pdf->Ln(5);
            $pdf->Ln(5);
            $pdf->SetTextColor(0,0,0);
            $pdf->Cell(500,8,"Cantidad Ventas del Mes :".number_format($cantidad,2,',','.'),0,0,'L');
            $pdf->Cell(500,8,"Total Ventas del Mes :".number_format($montoBruto,2,',','.'),0,0,'L');
            $pdf->Ln(5);
            $pdf->Cell(500,8,"Cantidad Devoluciones del Mes :".number_format($cantidadD,2,',','.'),0,0,'L');
            $pdf->Ln(5);
            $pdf->Cell(500,8,"Total Devoluciones del Mes :".number_format($totalD,2,',','.'),0,0,'L');
            $pdf->Ln(5);
            $pdf->Cell(500,8,"Cantidad del Mes :".number_format($cantidad-$cantidadD ,2,',','.'),0,0,'L');
            $pdf->Ln(5);
            $pdf->Cell(500,8,"Total del Mes :".number_format((($montoBruto - $totalD) > 0) ? $montoBruto - $totalD : 0,2,',','.'),0,0,'L');

            $pdf->SetFont('arial','B',10,5);
	     	    $pdf->SetFillColor(2,157,116);
            // $pdf->Cell(0,0,utf8_decode('MONTO TOTAL EN VENTAS: '.number_format($MontoTotal,3,',','.')),0,0,'R');
              $pdf->Output('I','REPORTE DE VENTAS-'.$fecha.'pdf');

           exit;
        }

        public function vistaReporteVS(){
          $menu = new HomeController();
          if(Auth::user()->role->nombre == 'ADMIN')$sucursales = Sucursal::get();
          else $sucursales = Sucursal::where('codsuc',Auth::user()->userSegSuc->codsuc)->get();
          /*
          $articulos = Faartfac::with('articulo')->whereHas('articulo')->get()->groupBy([
            function($q){
              return $q['articulo']["codart"];
            }
          ])->toArray();
          $categorias = Faartfac::with('articulo.facategoria')->whereHas('articulo.facategoria')->get()->groupBy([
            function($q){
              return $q['articulo']['facategoria']["codigoid"];
            }
          ])->toArray();

          $articulos = array_keys($articulos);
          $categorias = array_keys($categorias);
          */
          $articulos = Caregart::select(['codart','desart'])->get();
          $categorias = Facategoria::select(['codigoid','nombre'])->get();
          return view('/reportes/productosVendidosReportesV2',
          [
          'menus'=> $menu->retornarMenu(),
          'sucursales'=> $sucursales,
          'articulos' => $articulos,
          'categorias' => $categorias,
          'marcas' => \App\famodelo::get()

          ]);
        }



  public function reporteProductoVS(Request $request){

    $desde = $request['desde'];
    $hasta = $request['hasta'];
    $diames = $request['diames'];
    $codsuc = $request->sucursal;
    /* try{ */
      $productosV = Faartfac::with('sucursal')->
          whereHas('factura',function($q) use($codsuc){
          $q->where('codsuc','=',$codsuc)->where('status','A')->where('impfissta','I');
          })
          ->with('articulo.facategoria')
         /* ->whereHas('articulo.facategoria',function($q)use($request){
            $q->categorias($request->codcat);
          })*/
          ->with('articulo.getMarca')
          /*->whereHas('articulo.getMarca',function($q)use($request){
            $q->marcas($request->marca);
          })*/
          ->date($request["desde"],$request["hasta"])
          ->producto($request->codart)
          //->descrip($request->descrip)
          ->get();
          dd($productosV);
      if($productosV->isEmpty()){
        Session::flash('error', 'No existen registros con los parametros de busqueda ingresados');
        return redirect()->route('reportesProductosV2');}
/*       }//try
      catch(Exception $e){
        Session::flash('error', 'Se presentó un problema en la generación del reporte, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
        return redirect()->route('reportesProductosV2');
      } */
    $pdf = new generadorPDF();

    $titulo = 'REPORTE DE PRODUCTOS VENDIDOS';
    $total = $productosV->count();
    if($desde && $hasta){
      $fecha = Carbon::parse($desde)->format('d/m/yy')." HASTA ".Carbon::parse($hasta)->format('d/m/yy');
    }else{
      $fecha = "GENERAL";
    }

    $groupBy = $productosV->groupBy(function($q){
      return __(Carbon::parse($q->created_at)->format('D d m y'),
                ['date' => Carbon::parse($q->created_at)->format('d')]);
    });

    $numventas = $groupBy->map(function($q){
      //dd($q);
      return [
        'numVentasDia' => $q->flatten()->groupBy('reffac')->count(),
        'numArticulosDia' => ( int ) $q->sum('cantot')
      ];
    });
    /*
      PARA QUE NO SE REPITA LA POSIBILIDAD QUE SE REPITAN LOS CAMPOS AÑADO EL MES Y EL AÑO
    */
  /*  $groupedByart = $groupBy->map(function($q){
    return $q->groupBy(
        [function($q){
          return [
            Facategoria::where('codigoid',$q["articulo"]["codcat"])->first()->nombre,
          ];

        function($q){
          return [
            famodelo::where('codigoid',$q["articulo"]["codigomarca"])->first()->defmodelo ?? '',
          ];
        }
        ];
      );
    });/* Agrupo por categorias */
    //dd($groupedByart);
    //$groupedByMarca = $groupBy->map(function($))
    //dd($groupedByart);
    $lastArray = $groupedByart->map(function($q){
      return $q->map(function($q){
        return $q->map(function($q){
          return $q->groupBy(
            [
              function($q){
                $string  = $q["talla"] === 'A-N/A' || $q["talla"] === null ? '': ' Talla: '.Tallas::where('codtallas',$q["talla"])->first()->tallas;

                return $q["codart"].$string;
              }
            ]
          );
        });
      });
    });/* Agrupo por articulos */

    //dd($lastArray);
    $totalesVenta = $lastArray->map(function($categoria){
      return $categoria->map(function($marca){
        return $marca->map(function($faartfac){//RECORRO LOS ARTICULOS DENTRO DE ESAS CATEGORIAS
          return [
            'totalCantidad' =>
              $faartfac->map(function($articulo){
                return [
                  'vendidos' => ( int ) $articulo->sum('cantot'),
                ];
              })->sum('vendidos'),

            'totalVentaDiaria' =>
              $faartfac->flatten()->groupBy('reffac')->count(),

            'totalVenta' =>
              $faartfac->map(function($articulo){
                return [
                  'money'  => ( float ) $articulo->sum('costo')
                ];
              })->sum('money'),

            'productosVendidos' =>
            $faartfac->map(function($articulo){
              return [
                'vendidos' => ( int ) $articulo->sum('cantot'),
                'money'    => ( float ) $articulo->sum('costo'),
                //''/* SE PUEDE COLOCAR LA TASA */
              ];
            }),
            'ventaDivisa' =>
              $faartfac->map(function($articulo){
                //dd($articulo);
                return [
                  'divisa' => (float) $articulo->sum(function ($values){
                    return count(explode(' ',$values->predivisa)) > 0 ? (float) explode(' ',$values->predivisa)[0] : 0 ;
                  })/$articulo->count()
                ];
              })
          ];
        });
      });
    });


    $pdf->AddPage('L');
    $pdf->SetTitle('REPORTE DE PRODUCTOS VENDIDOS');
    $pdf->SetFont('arial','B',16);
    $pdf->SetWidths(array(90,90));
    $pdf->Ln();

    $pdf->Cell(0,22,utf8_decode($titulo),0,0,'C');
    $pdf->Ln();
    $pdf->SetFont('arial','B',10);
    $pdf->Ln(1);
    $pdf->Cell(0,0,utf8_decode('FECHA CONSULTA REPORTE : '.$fecha),0,0,'L');
    $pdf->Ln(2);
    $totalArtGeneral = 0;
    $totalVenGeneral = 0;
    foreach($totalesVenta as $date => $categoria){
      /* INICIO DE ACUMULADORES */
      $totalVendidos = 0;
      $times = 0;
      $timesProd = 0;
      $column = 0;
      if($pdf->GetY() >= 165 && $pdf->GetY() <= 190){
        $pdf->AddPage('L');
      }
      $pdf->SetFont('arial','B',8,5);
      $pdf->SetFillColor(2,157,116);
      $pdf->Cell(20,8,utf8_decode('FECHA'),1,0,'C');
      $pdf->Cell(190,8,utf8_decode('CATEGORÍA'),1,0,'C');
      $pdf->Cell(30,8,utf8_decode('TOTAL VENDIDOS'),1,0,'C');
      $pdf->Cell(30,8,utf8_decode('PORCENTAJE'),1,1,'C');
      $fecha =  explode(' ',$date)[0];
      $pdf->Cell(20,5,utf8_decode(
        __('dates.'.$fecha,['day' => explode(' ',$date)[1]]).' '.
        __('dates.'.explode(' ',$date)[2])
      ),1,0,'C');
      /* Iteramos primero los valores principales  es la primera agrupacion por fecha
        $numventas
        numventas
        numVentasDia
        numArticulosDia
      */
      foreach ($categoria as $nameCategoria => $marcas) {

        foreach($marcas as $nameMarca => $detalles){
          $timesProd  = 0;
          $realTime = 0;
          $times !== 0 ? $pdf->Cell(20,5,'',0,0,'C') : '';
          $pdf->Cell(190,5,utf8_decode($nameCategoria." [ ".$nameMarca." ]"),1,0,'C');
          $pdf->Cell(30,5,utf8_decode($detalles['totalCantidad']),1,0,'C');
          $pdf->Cell(30,5,round(($detalles["totalCantidad"]/$numventas[$date]["numArticulosDia"])*100,2)."%",1,1,'C');
          /* CAPTURO LA CATEGORIA PARA HACER UN RESUMEN GLOBAL */
          $totales[] = [
            'marcas'     => $nameMarca,
            'category'   => $nameCategoria,
            'cant'       => $detalles["totalCantidad"],
            'totalVenta' => $detalles["totalVenta"],
          ];
          $times++;
          //$pdf->Cell(270,5,utf8_decode("DETALLES"),1,1,'C');
          foreach ($detalles["productosVendidos"] as $key => $value) {
            if($timesProd === 0 || $timesProd === 2){
              /* RECORRIDO PARA HACER UNA SEPARACION PARA QUE LOS ARTICULOS NO CHOQUEN CON LA FECHA */
              $pdf->Cell($timesProd == 2 ? 1 : 20 ,5,'',0,0,'C');
            }
            $pdf->Cell(55,4,utf8_decode($key),0,0,'L');
            $pdf->Cell(10,4,utf8_decode($value["vendidos"]),0,$timesProd == 2 ? 1 : 0,'L');
            $totalesDetail[] = [
              'marcas'    => $nameMarca,
              'category'  => $nameCategoria,
              'codart'    => $key,
              'cant'      => $value["vendidos"],
              'divisa'    => $detalles['ventaDivisa'][$key]["divisa"] * $value["vendidos"],
              'unit'      => $detalles['ventaDivisa'][$key]["divisa"]
            ];

            $totalVendidos+=$value["vendidos"];/* ACUMULADOR */
            $timesProd++;
            //$column = $timesProd >= 0 && $timesProd <= 1 ? 0 : 1;
            $timesProd = $timesProd === 3 ? 0 : $timesProd;
          }
          $pdf->Cell(1,4,'',0,1,'C');
        }

      }

      $pdf->SetFont('arial','B',9);
      $pdf->Ln();
      $pdf->Cell(80,5,'TOTAL ARTICULOS VENDIDOS : '.$totalVendidos,0,0,'C');
      $pdf->Cell(80,5,'TOTAL VENTAS DEL DIA  '.__('dates.'.$fecha,['day' => explode(' ',$date)[1]]).' '.__('dates.'.explode(' ',$date)[2]).' : '.$numventas[$date]["numVentasDia"],0,1,'C');
      $totalVenGeneral+=$totalVendidos;
      $totalArtGeneral+=$numventas[$date]["numVentasDia"];

        }
      $pdf->Ln(5);
      $pdf->SetFont('arial','B',10,5);
      if($pdf->GetY() >= 155 && $pdf->GetY() <= 190){
        $pdf->AddPage('L');
      }
      $pdf->Cell(80,5,'RESUMEN',0,1,'I');
      $pdf->Cell(80,5,'TOTAL ARTICULOS VENDIDOS : '.$totalVenGeneral,0,0,'I');
      $pdf->Cell(80,5,'VENTAS TOTALES : '.$totalArtGeneral,0,1,'I');

      $totales = collect($totales);
      /* CONVIERTO EN COLECCION EL ARRAY CONSTRUIDO PARA AGRUPAR */
      $totalesBellote = $totales->groupBy(['category','marcas'])->map(function($categoria){
        return $categoria->map(function($marca){
          return [
            'cant' => $marca->sum('cant') ,
            'totalVenta' => $marca->sum('totalVenta')
          ];
        });
      });

      $totalesDetail = collect($totalesDetail);
      $totalesDetailBellote = $totalesDetail
      ->groupBy(['category','marcas'])->map(function($marca){
        return $marca->map(function($articulo){
          return $articulo->groupBy('codart')->map(function($q){
            return $q->sum('cant');
          });
        });
      });

      $pdf->Cell(1,4,'',0,1,'C');
      $pdf->SetFont('arial','B',10);
      $pdf->Cell(270,4,'RESUMEN POR REFENCIAS',0,1,'C');
      $pdf->Cell(1,4,'',0,1,'C');

      foreach ($totalesDetailBellote as $key => $value) {
        // if($timesProd === 0 || $timesProd === 2){
        //   /* RECORRIDO PARA HACER UNA SEPARACION PARA QUE LOS ARTICULOS NO CHOQUEN CON LA FECHA */
        //   $pdf->Cell($timesProd == 2 ? 1 : 20 ,5,'',0,0,'C');
        // }
        $pdf->SetFont('arial','B',10);
        $pdf->Cell(270,4,utf8_decode($key),0,1,'C');

        foreach($value as $marca => $producto){
          $pdf->SetFont('arial','B',10);
          $pdf->Ln();
          $pdf->Cell(270,4,utf8_decode("[ MARCA : ".$marca." ]"),0,1,'C');
          $pdf->SetFont('arial','',9);
          foreach($producto as $ref => $cant){
            $pdf->Cell(55,4,utf8_decode($ref),0,0,'L');
            $pdf->Cell(10,4,utf8_decode($cant),0,$timesProd == 2 ? 1 : 0,'L');
            $timesProd++;
            $timesProd = $timesProd === 4 ? 0 : $timesProd;
          }
        }
      $pdf->Cell(1,6,'',0,1,'C');
      }
      $detalladoDivisa = $totalesDetail->groupBy(['category','marcas'])->map(function($category){
        return $category->map(function($marcas){
            return [
              'cant'  => $marcas->sum('cant'),
              'divisa'=> $marcas->sum('divisa')
            ];
          });
      });
      $globalMarcas = $totalesDetail->groupBy('marcas')->map(function($marca){
        return [
          'cant'  => $marca->sum('cant'),
          'divisa'=> $marca->sum('divisa')
        ];
      });


      $pdf->Cell(1,10,'',0,1,'C');
      if($pdf->getY()>= 150 && $pdf->getY()<=180){
        $pdf->AddPage('L');
      }
        $pdf->SetFont('arial','B',10);
        $pdf->Cell(270,4,'RESUMEN FACTURADO GLOBAL',0,1,'C');
        $pdf->Cell(1,4,'',0,1,'C');
        $pdf->SetFont('arial','B',9);
        $pdf->Cell(30,5,'CATEGORIA',0,0,'I');
        $pdf->Cell(30,5,'MARCA',0,0,'I');
        $pdf->Cell(30,5,'CANTIDAD',0,0,'I');
        $pdf->Cell(30,5,'PORCENTAJE',0,0,'I');
        $pdf->Cell(40,5,'FACTURADO',0,0,'I');
        $pdf->Cell(30,5,'MONTO DIVISA',0,1,'I');
        $pdf->SetFont('arial','',9);

        //dd($globalMarcas);
        $total       = 0;
        $totalDivisa = 0;
        $totalBs     = 0;
        foreach($totalesBellote as $key => $value)
        {
          foreach($value as $keyMarca => $valueMarca){
            $mult = round(($valueMarca["cant"]/$totalVenGeneral)*100,2);
            $pdf->Cell(30,5,$key,0,0,'I');
            $pdf->Cell(30,5,$keyMarca,0,0,'I');
            $pdf->Cell(30,5,$valueMarca["cant"],0,0,'I');
            $pdf->Cell(30,5,$mult.'%',0,0,'I');
            $pdf->Cell(40,5,number_format($valueMarca["totalVenta"],2,',','.').' Bs.S',0,0,'I');
            $pdf->Cell(30,5,number_format($detalladoDivisa[$key][$keyMarca]["divisa"],2,',','.').' $',0,1,'I');
            $totalDivisa += round($detalladoDivisa[$key][$keyMarca]["divisa"],2);
            $totalBs     += round($valueMarca["totalVenta"],2);
            $total       = $total + $mult;
          }
        }
        $pdf->SetFont('arial','B',10);
        $pdf->Cell(120,5,'TOTALES',0,0,'I');
        $pdf->Cell(40,5,number_format($totalBs,2,',','.')." Bs.S",0,0,'I');
        $pdf->Cell(30,5,number_format($totalDivisa,2,',','.')." $",0,0,'I');
    //$pdf->Cell(0,0,utf8_decode('MONTO TOTAL EN VENTAS: '.number_format($MontoTotal,3,',','.').' Bs'),0,0,'R');
    $pdf->Output('I','REPORTE DE VENTAS-'.$fecha.'pdf');

    exit;
  }


}

