<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Yaml\Parser;
use App\Tsdefmon;
use App\Caprovee;
use App\Fatasacamb;
use App\Models\Cacotiza;
use App\Models\Casolart;
use App\Models\Caconpag;
use App\Models\Caforent;

class CotizaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function retornarMenu()
    {
        $yaml = new Parser();
        $menu = $yaml->parse(file_get_contents('menu/menu.yml'));
        return $menu['menu'];
    }

    public function index()
    {
        //
        $cots = Cacotiza::orderBy('id')->paginate(8);
        $menus = $this->retornarMenu();

        return view('compras.cotizaciones.cotizaciones', compact('cots', 'menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $menus = $this->retornarMenu();
        $monedas = Tsdefmon::get();
        $conteo = Cacotiza::withTrashed()->count() + 1;
        $numero = 'C' . str_pad($conteo, 7, 0, STR_PAD_LEFT);
        $tasa = Fatasacamb::with('moneda', 'moneda2')->whereHas('moneda', function ($q) {
            $q->where('nombre', '=', 'DOLLAR');
        })->where('activo', '=', true)->first();
        $condiciones = Caconpag::get();
        $formas = Caforent::get();
        $proveedores = Caprovee::where('estpro', 'A')->get();
        
        return view('compras.cotizaciones.cotizacionCreate', compact('menus', 'numero', 'monedas', 'tasa', 'proveedores', 'formas', 'condiciones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function loadPedidos()
    {
        $pedidos = Casolart::where('stareq', 'A')->where('aprreq', 'G')->get();

        return response()->json($pedidos, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
