<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DemoProductoRequest;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\DemoProducto;
use Illuminate\Support\Facades\Auth;
use User;
use Helper;
use App\Caregart;
use Session;
use App\Segususuc;

class DemoProductoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $menu = new HomeController();
        $demografias = DemoProducto::orderBy('id','DESC')->paginate(8);
        return view('/configuracionGenerales/demografiaProducto')->with('menus',$menu->retornarMenu())
        ->with('demografias',$demografias);
    }

    public function nuevaDemoP(){
        $menu = new HomeController();
        return view('/configuracionGenerales/registroDemoProducto')->with('menus',$menu->retornarMenu());
    }

    public function create(DemoProductoRequest $request){

        $guion ='-';
        $cont = DemoProducto::withTrashed()->get()->count();
        $cont = $cont+1;

        // $suc = Auth::user()->codsuc_reg;
        $suc=Auth::user()->getCodigoActiveS();
        $codsuc_reg = explode('-',$suc);
        $cod = $cont.$guion.$codsuc_reg[1];

        $comprobar=DemoProducto::withTrashed()->where('nomendemo',$request['nomenclatura'])->whereOr('nombreDemo',$request['nombre'])->first();

        if($comprobar)
        {
            if($comprobar['deleted_at']==NULL)
            {
                if ($comprobar['nomendemo']==$request['nomenclatura'])
                {
                    Session::flash('error', ' La Nomeclatura  '.$request['nomenclatura'].' posee un registro Activo en el sistema, verificar información.');
                }
                else
                {
                    Session::flash('error', ' La Demografia  '.$request['nombre'].' posee un registro Activo en el sistema, verificar información.');   
                }
                return redirect()->route('listaDemoProducto');
            } 
            else
            {
                $comprobar->restore();
                $comprobar->update([
                  'nombreDemo'=>$request['nombre'],
                  'codsuc'=>$suc
                ]);

                Session::flash('success', 'La demografia '.$request['nombre'].', se registro Exitosamente');
                return redirect()->route('listaDemoProducto');
            }   
        }
        

        DemoProducto::create([
            'nombredemo'=>$request['nombre'],
            'nomendemo'=>$request['nomenclatura'],
            'codigoid'=>$cod,
            'codsuc'=>$suc,
        ]);

        Session::flash('success', 'La demografia '.$request['nombre'].', se registro Exitosamente');
        return redirect()->route('listaDemoProducto');
    }

    public function edit($id){
        $menu = new HomeController();
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $demografia = DemoProducto::find($ID);

        return view('/configuracionGenerales/editarDemoProducto')->with('menus',$menu->retornarMenu())->with('demografia',$demografia);
    }

    public function update(DemoProductoRequest $request, $id){

        $demografia = DemoProducto::find($id);
        $demografia->nombredemo = $request['nombre'];
        $demografia->nomendemo = $request['nomenclatura'];
        $demografia->save();

        Session::flash('success', 'La demografia '.$request['nombre'].', se actualizó Exitosamente');
        return redirect()->route('listaDemoProducto');
    }

    public function delete($id){
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $demografia = DemoProducto::find($ID);


        // $codigoid = $demografia->codigoid;

        // $product = caregart::with('demografia')->whereHas('demografia',function($q) use ($codigoid){
        //     $q->where('codigoid','=',$codigoid);
        // })->get();
        
        $articulo = DemoProducto::where('id','=',$ID)->has('articulos')->get();
        if($articulo->isEmpty()){

            $demografia->delete();
            return response()->json([
                'titulo' => 'Exito se elimino'
            ]);
        }
        else{
            return response()->json([
                'titulo' => 'error Esta demografia no puede ser eliminada'
            ]);

           }

       }

    public function filtro(Request $request){
        $menu = new HomeController();
        $demografias = DemoProducto::where('nombredemo','ilike','%'.$request->filtro.'%')
        ->orderBy('nombredemo','ASC')->paginate(8);

        return view('/configuracionGenerales/demografiaProducto')->with('menus',$menu->retornarMenu())
        ->with('demografias',$demografias);
    }
}
