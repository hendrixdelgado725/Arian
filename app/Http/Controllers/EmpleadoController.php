<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EmpleadoRequest;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\Npasicaremp;
use App\Nphojint;
use App\Npcargos;
use Helper;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Contracts\Auth\Guard;
use App\permission_user;
use App\Segususuc;
use Carbon\Carbon;

class EmpleadoController extends Controller
{

    private $guard;
    public function __construct(Guard $guard)
    {
        $this->middleware('auth');
        $this->guard = $guard;
        
    }

    public function index(Request $request){



        $menu = new HomeController();

        $empleados = Nphojint::with('npasicaremp')->orderBy('id','DESC')->paginate(8);
        

        return view('/configuracionGenerales/empleado')->with('menus',$menu->retornarMenu())->with('empleados',$empleados);
    }
    
    public function create(EmpleadoRequest $request){


        $maxCaracter = mb_strlen(trim($request->cedularif));
        if($maxCaracter >= 16){
            Session::flash('error', 'La cedula es muy larga, Verifique la Información.');
            return redirect()->route('nuevoEmpleado');
        } else {

        try{
            $cargo = Npcargos::where('codcar','=',$request->cargo)->first();
            $guion = '-';
            $rifemp = $request->tipodocumento.$guion.$request->cedularif;

            if($request->estatus==='Activo'){
                $estatus = 'A';
            }
            else{
                $estatus = 'R';
            }
           
            $cont = Nphojint::withTrashed()->get()->count();
            $cont = $cont+1;
            $cont2 = Npasicaremp::withTrashed()->get()->count();
            $cont2 = $cont2 +1;
            // $suc = Auth::user()->codsuc_reg;
            $suc=Auth::user()->getCodigoActiveS();
            $codsuc_reg = explode('-',$suc);
            $cod = $cont.$guion.$codsuc_reg[1];
            $cod2 = $cont2.$guion.$codsuc_reg[1];

            /*dd($rifemp);*/

            $comprobar=Nphojint::where('codemp','=',$request['cedularif'])->withTrashed()->first();
              /* dd($comprobar);*/
              if($comprobar){
                 if($comprobar['deleted_at']==NULL||$comprobar['deleted_at']=='')
                {      
                Session::flash('error', ' El número de Documento '.$rifemp.' posee un registro Activo en el sistema, verificar información.');
                return redirect()->route('nuevoEmpleado');
                }
                else
                {
                $comprobar->deleted_at = NULL;
                $comprobar->nomemp = $request['nombre'];
                $comprobar->codemp = $request['cedularif'];
                $comprobar->cedemp = $request['cedularif'];
                $comprobar->dirhab = $request['direccion'];
                $comprobar->celemp = $request['telefono'];
                $comprobar->telhab = $request['telefono2'];
                $comprobar->emaemp = $request['email'];
                $comprobar->fecing = $request['fechaing'];
                $comprobar->staemp = $estatus;
                $comprobar->rifemp = $rifemp;
                $comprobar->codigoid = $cod;
                $comprobar->codsuc = $suc;
                $comprobar->save();
                   
                $comprobar2 = Npasicaremp::where('codemp','=',$request['cedularif'])->where('status','=','V')->first();
                if($comprobar2->codcar == $cargo->codcar){

                }
                else{
                    $comprobar2->status = 'E';   
                    $comprobar2->save();

                    $nuevoCargo = new Npasicaremp;
                    $nuevoCargo->codemp = $request['cedularif'];
                    $nuevoCargo->codcar = $cargo->codcar;
                    $nuevoCargo->nomcar = $cargo->nomcar;
                    $nuevoCargo->nomemp = $request['nombre'];
                    $nuevoCargo->status = 'V';
                    $nuevoCargo->codigoid = $cod2;
                    $nuevoCargo->codsuc = $suc;
                    $nuevoCargo->save();
                }
                Session::flash('success', 'El empleado '.$request['nombre'].', se registro Exitosamente');
                return redirect('/modulo/ConfiguracionGenerales/Empleado');
             }
            
            }

             $empleado = new  Nphojint;
             $empleado->nomemp = $request['nombre'];
             $empleado->codemp = $request['cedularif'];
             $empleado->cedemp = $request['cedularif'];
             $empleado->dirhab = $request['direccion'];
             $empleado->celemp = $request['telefono'];
             $empleado->telhab = $request['telefono2'];
             $empleado->emaemp = $request['email'];
             $empleado->fecing = $request['fechaing'];
             $empleado->staemp = $estatus;
             $empleado->rifemp = $rifemp;
             $empleado->codigoid = $cod;
             $empleado->codsuc = $suc;
             $empleado->save();

             $cargoDempleado = new Npasicaremp;
             $cargoDempleado->codemp = $request['cedularif'];
             $cargoDempleado->codcar = $cargo->codcar;
             $cargoDempleado->nomcar = $cargo->nomcar;
             $cargoDempleado->nomemp = $request['nombre'];
             $cargoDempleado->fecasi = Carbon::now();
             $cargoDempleado->status = 'V';
             $cargoDempleado->codigoid = $cod2;
             $cargoDempleado->codsuc = $suc;
             $cargoDempleado->save();

           

        Session::flash('success', 'El empleado '.$request['nombre'].', se registro Exitosamente');
        return redirect('modulo/ConfiguracionGenerales/Empleado');
    }//try
    catch(Exception $e){
            return redirect()->route('nuevoEmpleado')->with('error',$e->getMessage());
        }

    }
    }

    public function edit($id){
        
       $menu = new HomeController();
       $desc=Helper::desencriptar($id);
       $sep  = explode('-',$desc);
       $ID= (int)$sep[1];
        
       $empleado = Nphojint::with(['npasicaremp' => function($q){
        $q->where('status','V');
        }])->find($ID);
       #dd($empleado);
       $cargos = Npcargos::get();
       return view('/configuracionGenerales/editarEmpleado')->with('menus',$menu->retornarMenu())->with('empleado',$empleado)->with('cargos',$cargos);


    }

    public function update(EmpleadoRequest $request, $id){

        if($request->estatus==='Activo'){
            $estatus = 'A';
        }
        else{
            $estatus = 'R';
        }

        $cargo = Npcargos::where('codcar','=',$request->cargo)->first();
        $guion = '-';
        $rifemp = $request->tipodocumento.$guion.$request->cedularif;

        $empleado = Nphojint::find($id);
        $empleado->codemp = $request->cedularif;
        $empleado->cedemp = $request->cedularif;
        $empleado->rifemp = $rifemp;
        $empleado->nomemp = $request->nombre;
        $empleado->dirhab = $request->direccion;
        $empleado->emaemp = $request->email;
        $empleado->celemp = $request->telefono;
        $empleado->telhab = $request->telefono2;
        $empleado->fecing = $request->fechaing;
        if($request['fechaegre']){
        $empleado->fecret = $request->fechaegre;            
        }
        $empleado->staemp = $estatus;
        $empleado->save();

        $ultimoCargo = Npasicaremp::where('codemp','=',$request->cedularif)
        ->orderBy('id','DESC')->first();

       
        
         $cont = Npasicaremp::withTrashed()->get()->count();
         $cont = $cont +1;
         $suc = Auth::user()->codsuc_reg;
         $codsuc_reg = explode('-',$suc);
         $cod = $cont.$guion.$codsuc_reg[1];

         if($ultimoCargo !== null){
            if($ultimoCargo->codcar == $cargo->codcar){
                $ultimoCargo->status = 'E';   
                $ultimoCargo->save();
            }
         }
         

         $nuevoCargo = new Npasicaremp;
         $nuevoCargo->codemp = $request['cedularif'];
         $nuevoCargo->codcar = $cargo->codcar;
         $nuevoCargo->nomcar = $cargo->nomcar;
         $nuevoCargo->nomemp = $request['nombre'];
         $nuevoCargo->status = 'V';
         $nuevoCargo->fecasi = Carbon::now();
         $nuevoCargo->codigoid = $cod;
         $nuevoCargo->codsuc = $suc;
         $nuevoCargo->save();
        

        Session::flash('success', 'El empleado '.$request['nombre'].', se actualizó Exitosamente');
        return redirect('modulo/ConfiguracionGenerales/Empleado');
    }

    public function delete($id){

        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $empleado=Nphojint::find($ID);
        $empleado->delete();
        return response()->json([
            'titulo' => 'Exito se elimino'
        ]);

    }

    public function filtro (Request $request){
        $menu = new HomeController();

        $empleados = Nphojint::with('npasicaremp')->where('codemp','ilike','%'.$request->filtro.'%')
        ->orWhere('nomemp','ilike','%'.$request->filtro.'%')->orWhere('dirhab','ilike','%'.$request->filtro.'%')
        ->orWhereHas('npasicaremp',function($q) use ($request){
            $q->where('nomcar','ilike','%'.$request->filtro.'%');
        })->orderBy('id','DESC')->paginate(8);
        

        return view('/configuracionGenerales/empleado')->with('menus',$menu->retornarMenu())->with('empleados',$empleados);
    }
    
    public function nuevoempleado(){


        $menu = new HomeController();
        $cargos = Npcargos::orderBy('nomcar','ASC')->get();

        if($cargos->isEmpty())
        {
            return redirect('/modulo/ConfiguracionGenerales/Empleado')->with(['error' => 'No existen Cargos activos, Verifique Información']);          
        }
        return view('/configuracionGenerales/registroEmpleado')->with('menus',$menu->retornarMenu())->with('cargos',$cargos);
    }

    
}
