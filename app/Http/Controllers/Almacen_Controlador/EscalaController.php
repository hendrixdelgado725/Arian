<?php

namespace App\Http\Controllers\Almacen_Controlador;

use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Arr;
use App\User;
use App\Fadefesc;
use App\Fadetesc;
use App\Caregart;
use App\Tallas;
use Helper;
use Exception;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Segususuc;

class EscalaController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*Función que ejecuta la inserción de los archivos en la base de datos*/
    public function create(Request $request)
    {
        $valor=request()->all();
        $sucuse_reg=Auth::user()->getsucUse()->codsuc;
        $sep= explode('-', $sucuse_reg);
        $tall=[];
        $ver=false;
        // dd($valor);
        $request->validate([
            'referencia'=>'required|string|min:5|max:20',
            'nombre'=>'required|string|min:5|max:150'
        ]);
        try{
            for ($i=0; $i<12;$i++){
                if(($valor['div_tallas_hd_'.$i])!=null){
                    if($valor['div_tallas_'.$i]!=null ){
                        $tall[]=[$valor['div_tallas_'.$i],$valor['div_tallas_hd_'.$i]];
                        $ver=true;
                    }    
                }
            }
            
            
            // dd($tall);
            if($ver==false)
            {
                Session::flash('error', 'La Escala no Puede ser registrada si no posee alguna cantidad asociada');
                return redirect()->route('listaEscala');
            }
            else
            {
                $esc=Fadefesc::get()->count();
                $codigoid= ($esc+1).'-'.$sep[1];

                Fadefesc::create([
                    'codesc'=>$valor['referencia'],
                    'nomesc'=> $valor['nombre'],
                    'codsuc_reg'=> $sucuse_reg,
                    'codart'=> $valor['articulo'],
                    'codigoid' => $codigoid
                ]);

                foreach ($tall as $tl ) {
                // dd($tl[0]);
                    $det=Fadetesc::get()->count();
                    $codigoidDet= ($det+1).'-'.$sep[1];
                    Fadetesc::create([
                        'codesc'=>$valor['referencia'],
                        'codtallas'=>$tl[1],
                        'cantidad'=>$tl[0],
                        'coddefesc'=>$codigoid,
                        'codsuc_reg'=> $sucuse_reg,
                        'codigoid'=> $codigoidDet
                    ]);
                }
                Session::flash('success', 'La Escala '.$valor['nombre'].', se registro Exitosamente');
                return redirect()->route('listaEscala');
            }
        }catch(Exception $e){
                Session::flash('error', 'Se presentó un problema en el Registrp, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
                return redirect()->route('listaEscala');    
            }
    }

     /*Funcion que realiza la edicion de los campos*/
    public function update(Request $request, $id)
    {
        $valor=request()->all();
        $sep=explode('|', $id);
        $escala=Fadefesc::findOrFail($sep[0]);
        $val=explode(',', $escala->getArticulo()->codtallas);
        $request->validate([
            'nombre'=>'required|string|min:5|max:150'
        ]);
        try{ 
            /*************AGREGAR O ACTUALIZAR TALLA DE ESCALA************************/
            $tall=[];
            $ver=false;
            for ($i=0; $i<count($val);$i++){
                if(($valor['div_tallas_ct_'.$i])!=null){
                    if($valor['div_tallas_oc_'.$i]!=null ){
                        $tall[]=[$valor['div_tallas_oc_'.$i],$valor['div_tallas_ct_'.$i]];
                        $ver=true;
                    }    
                }
            }
           
            if($ver==false){
                Session::flash('error', 'La Escala no puede actualizar, si no posee alguna cantidad asociada');
                return redirect()->route('listaEscala');
            }
            else{
                /**************ACTUALIZAR DEFINICION DE ESCALA ESCALA***********************/
                $actualizar=Fadefesc::where([['id',$sep[0]],['codigoid',$sep[1]]])->update(['nomesc'=>$valor['nombre']]);
                /************ELIMINA LOGICAMENTE LA TALLA DE LA ESCALA*******************/
                if($request->borrados!=null){
                    $borrados=explode(',', $request->borrados);
                    foreach ($borrados as $borrar) {
                        $borrado=Fadetesc::where([['codtallas',$borrar],['deleted_at',null],['coddefesc',$sep[1]]])->update(['deleted_at'=>date('Y-m-d h:m:i')]);
                    }
                }

                foreach ($tall as $det) {
                    $verificar=Fadetesc::where([['deleted_at',null],['coddefesc',$sep[1]],['codtallas',$det[0]]])->first();
                    if($verificar==null)
                    {
                        $sucuse_reg=Auth::user()->getsucUse()->codsuc;
                        $sep1= explode('-', $sucuse_reg);
                        $ultid2 = Fadetesc::get()->count();
                        $codigoidDet = ($ultid2+1).'-'.$sep1[1];
                        Fadetesc::create([
                        'codesc'=>$escala['codesc'],
                        'codtallas'=>$det[0],
                        'cantidad'=>$det[1],
                        'coddefesc'=>$sep[1],
                        'codsuc_reg'=> $sucuse_reg,
                        'codigoid'=> $codigoidDet
                        ]);
                    }
                    else
                    {
                        $verificar->update(['cantidad'=>$det[1]]);
                    }
                }
            }
            Session::flash('success', 'La Escala '.$valor['nombre'].', se actualizó Exitosamente');
            return redirect()->route('listaEscala'); 

        }catch(Exception $e){
            Session::flash('error', 'Se presentó un problema en la Edición, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
            return redirect()->route('listaEscala');    
        }
    }

    /*Vista que genera el listado inicial para la carga de la informaci{on*/
    public function listado()
    {
    	$menu = new HomeController();
    	$escala= Fadefesc::orderBy('id','DESC')->where('deleted_at',NULL)->paginate(8);

        // dd($usuario[0]->getNombreSucursal());
    	return view('almacen.escala')->with('menus',$menu->retornarMenu())->with('escala',$escala);
    }

    /*Boton registrar que aparece en el listado*/
    public function nuevaEscala()
    {
        $menu = new HomeController();
        $articulo= Caregart::where([['deleted_at',NULL],['tipreg','F']])->wherenotnull('codtallas')->orderBy('desart')->get();

        $accion='registro';

        return view('/almacen/registroEdicionEscala')->with('menus',$menu->retornarMenu())->with('accion',$accion)->with('articulo',$articulo);
    }

    /*Función para  ingresar a la edición del archivo*/
    public function edit($id)
    {
        $menu = new HomeController();
        $desc=Helper::desencriptar($id);
        $sep=explode('-', $desc);
        $ID= (int) $sep[1];
        $escala=Fadefesc::findOrFail($ID);
        $detescala=$escala->getTallasAsociadas();
        $valor=explode(',', $escala->getArticulo()->codtallas);
        // 
        $arr=[];
        $tallnocant=[];
        for ($i=0; $i<count($detescala);$i++)
        {
            $arr[]=$detescala[$i]['codtallas'];
        }
    
        foreach ($valor as $codt) {
            if(!in_array($codt, $arr)){
                $tallnocant[]=$codt;
            }
        }
        $tallsinasociar=$escala->getArticulo()->tallas($tallnocant);
        // dd($tallsinasociar);
        $accion='edicion';

        return view('/almacen/registroEdicionEscala')->with([
            'menus'=> $menu->retornarMenu(),
            'escala' => $escala,
            'accion' => $accion,
            'detescala' => $detescala,
            'tallsinasociar' => $tallsinasociar,
            // 'sucujson'=> json_encode($sucursal),
        ]);
    }

    /*funcion de eliminar el registro*/
    public function delete($id)
    {
        $desc=Helper::desencriptar($id);
        $sep=explode('|', $desc);
        $ID=(int)$sep[1];
        $codig=(string)$sep[2];

        /****Falta agregar cuando tiene factura asociada****/

        $comprobar= Fadefesc::findOrFail($ID)->update(['deleted_at'=>date('Y-m-d h:m:i')]);
        return response()->json(['titulo' => 'La escala se elimino']);
    }
    /*busqueda de filtro*/
    public function filtro(Request $request)
    {
        $menu = new HomeController();

        $escala=Fadefesc::where('nomesc','ilike','%'.$request->filtro.'%')->OrWhere('id','ilike','%'.$request->filtro.'%')->OrWhere('codesc','ilike','%'.$request->filtro.'%')->paginate(8);

        return view('/almacen/escala')->with('menus',$menu->retornarMenu())->with('escala',$escala);
    }

    /**/
    public function ajax()
    {
    	$data=Request();
        
    	if($data['ajax']==1)
    	{
    		$articulo=Caregart::where('codart',$data['id'])->first();
            $sep=explode(',', $articulo->codtallas);

            foreach ($sep as $cod) {
                # code...
                $tallas=Tallas::where('codtallas',$cod)->get();
                $tal[]=$tallas;
            }

    		return response()->json(['options'=>($tal ? $tal :'0')]);
            
    	}
    }
}
