<?php

namespace App\Http\Controllers\Almacen_Controlador;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Auth\Guard;
use Helper;
use Session;
use App\permission_user;
use App\Fasubcategoria;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\faartpvp;
use App\Caregart;
use Carbon\Carbon;
use App\ColorPro;
use App\Facategoria;
use App\Events\Auditoria;
use App\Segususuc;
use App\caartalm;
use App\Models\Caarttal;
use App\famodelo;
use Illuminate\Support\Facades\Validator;
use DataTables;

class RegistroArticulosController extends Controller
{


  private $año = "";
  private $guard;
  public function __construct(Guard $guard)
  {
    $this->middleware('auth');
    $this->guard = $guard;
  }

  public function detalleArticulo($id){
      $querys = \App\Caregart::where('codart', $id)
      ->whereNull('deleted_at')->with(['faartpvp' => function ($q) use ($id) {
        $q->where('status', 'A')->where('codart', $id);
    }, 'color', 'facategoria', 'subcategoria', 'moneda', 'almacen'])->first();
      return response()->json([
        'articulo' => $querys
      ]);
  }

  public function multiple_update(Request $request){
    
    if($request->accion == "activar"){
      foreach ($request->articulos as $articulo) {
          $art = caregart::where('codart',$articulo)->where('staart' , 'I')->update(['staart' => "A"]);
          //echo($art);
      }
    }elseif($request->accion == "desactivar"){
      foreach ($request->articulos as $articulo) {
        $art = caregart::where('codart',$articulo)->where('staart' , 'A')->update(['staart' => "I"]);
        //echo($art);
    }
  
    }elseif($request->accion == "eliminar"){

      foreach ($request->articulos as $articulo) {
        
            $search = \App\Caregart::where('codart', $articulo)->first();
            if (Caregart::notDeleteBill($search->codart)){
              return response()->json([
                'titulo' => 'NO SE PUEDE ELIMINAR EL ARTICULO POR TIENE FACTURA'
              ]);
            } else {
              $precio = \App\faartpvp::where('codart', $search->codart)->where('status', 'A')->where('deleted_at', null)->delete();
              $search->delete();
            }
      }
    }elseif($request->accion == "facturable"){
      foreach ($request->articulos as $articulo) {
        $art = caregart::where('codart',$articulo)->whereNull('tipreg')->update(['tipreg' => "F"]);
      }
      //dd($request->accion ," factivar");
    }elseif($request->accion == "No facturable"){
      echo("hola");
      foreach ($request->articulos as $articulo) {
        $art = caregart::where('codart',$articulo)->where('tipreg', 'F')->update(['tipreg' => ""]);
        //echo($art);
      }
    }
    
  }

  public function filtro(Request $request)
  {
      $menu = new HomeController();
      // Variables de filtro
      $filtro = $request->filtro;
      $categoria = $request->categoria;
      $estatus = $request->estatus;
      $facturable = $request->facturable;
  
      // Consulta base
      $querys = Caregart::whereNull('deleted_at');
  
      // Aplicación de filtros
      if ($filtro) {
          $querys->where(function ($q) use ($filtro) {
              $q->where('codart', 'ilike', '%' . $filtro . '%')
                  ->orWhere('desart', 'ilike', '%' . $filtro . '%')
                  ->orWhere('nomart', 'ilike', '%' . $filtro . '%');
          });
      }
  
      if ($categoria) {
          $querys->where('codcat', 'ilike', '%' . $categoria . '%');
      }
  
      if ($estatus) {
          $querys->where('staart', 'ilike', '%' . $estatus . '%');
      }
  
      if ($facturable) {
        if ($facturable === "null") {
            $querys->whereNull('tipreg');
        } else {
            $querys->where('tipreg', 'ilike', '%' . $facturable . '%');
        }
        $querys->orderBy('id', 'DESC');
      }
  
      // Si todas las variables están vacías, mostrar todos los registros
      if (!$filtro && !$categoria && !$estatus && !$facturable) {
          $querys = Caregart::whereNull('deleted_at');
      }
  
      // Carga anticipada de relaciones
      $querys = $querys->with(['faartpvp' => function ($q) use ($request) {
          $q->where('status', 'A')->where('codart', 'ilike', '%' . $request->filtro . '%');
      }, 'color', 'facategoria', 'subcategoria', 'moneda', 'almacen', 'costoPro']);

      $articulos = $querys->orderBy('id', 'DESC')->paginate(6);

      return response()->json(['menus'=>$menu->retornarMenu() ,'articulos'=>$articulos], 200);

  }


//   private function query($request)
//   {
    
//     try {
//       if(isset($request->all()['filtro']) === false || $request->all()['filtro'] === " ") throw new \Exception("0", 1);//controla la excepcion para filtrar en caso que este vacio
      
//         $querys = \App\Caregart::where('tipreg', 'F')->where('codart', 'ilike', '%' . $request->filtro . '%')
//         ->orWhere('desart','ilike','%'.$request->filtro.'%')
//         ->whereNull('deleted_at')
//         ->with(['faartpvp' => function ($q) use ($request) {
//           $q->where('status', 'A')->where('codart', 'ilike', '%' . $request->filtro . '%');
//         }, 'color', 'facategoria', 'subcategoria', 'demoProducto', 'moneda'])->paginate(8);      


//     } catch (\Exception $th) {
      
//       $querys = \App\Caregart::where('tipreg', 'F')->whereNull('deleted_at')
//       ->with(['faartpvp' => function ($q) {
//         $q->where('status', 'A');
//       }, 'color', 'facategoria', 'subcategoria', 'demoProducto', 'moneda'])->paginate(8);
//     }

// }

  private function query(){

    $querys = null;
    
     $querys = \App\Caregart::whereNull('deleted_at')->with([ 'costoPro', 'color', 'facategoria', 'subcategoria', 'moneda' , 'almacen', 'almacenes.almacenes'])
     ->orderBy('id', 'DESC')->paginate(6);
    
    //with('almacen')->where('tipreg', 'F')->whereNull('deleted_at')
    //     ->with(['faartpvp' => function ($q) {
    //       $q->where('status', 'A');
    //     }, 'color', 'facategoria', 'subcategoria', 'demoProducto', 'moneda', 'getMarca'])->orderBy('id','ASC')->paginate(4);
    // // dd($querys);
    return $querys; 
  }


  public function index(Request $request)
  {
    $menu = new HomeController();
    return view('almacen.listadoArticulo')->with([
      'menus' => $menu->retornarMenu(),
      'articulos' => $this->query($request),
    ]);
  }

  public function getpermiso(){
    $usuarioLogeado = Auth()->user()->loguse;
    $permisoUsuario = permission_user::where('codusuarios', $usuarioLogeado)->get();
    $permiso = $permisoUsuario[0]->codpermission;
    return response()->json(['permiso'=>$permiso]);
  }

  public function informationArticulos(Request $request){

      // Variables de filtro
      $filtro = $request->filtro;
      $categoria = $request->categoria;
      $estatus = $request->estatus;
      $facturable = $request->facturable;

    if($filtro || $categoria || $estatus || $facturable){
        
          // Consulta base
          $querys = Caregart::whereNull('deleted_at');
      
          // Aplicación de filtros
          if ($filtro) {
              $querys->where(function ($q) use ($filtro) {
                  $q->where('codart', 'ilike', '%' . $filtro . '%')
                      ->orWhere('desart', 'ilike', '%' . $filtro . '%');
              });
          }
      
          if ($categoria) {
              $querys->where('codcat', 'ilike', '%' . $categoria . '%');
          }
      
          if ($estatus) {
              $querys->where('staart', 'ilike', '%' . $estatus . '%');
          }
      
          if ($facturable) {
            if ($facturable === "null") {
                $querys->whereNull('tipreg');
            } else {
                $querys->where('tipreg', 'ilike', '%' . $facturable . '%');
            }
            $querys->orderBy('id', 'ASC');
          }
      
          // Si todas las variables están vacías, mostrar todos los registros
          if (!$filtro && !$categoria && !$estatus && !$facturable) {
              $querys = Caregart::whereNull('deleted_at');
          }
      
          // Carga anticipada de relaciones
          $querys = $querys->with(['faartpvp' => function ($q) use ($request) {
              $q->where('status', 'A')->where('codart', 'ilike', '%' . $request->filtro . '%');
          }, 'color', 'facategoria', 'subcategoria', 'moneda', 'almacen']);
    
          $articulos = $querys->orderBy('id', 'ASC')->paginate(6);
    } else {
          $articulos = $this->query();
    }
    
    $categoria = facategoria::select('nombre','codigoid')->get(); 
    return response()->json(['articulos'=>$articulos, 'categorias'=>$categoria]);

  }


  public function registrarArticulo()
  {
    
    $menu = new HomeController();
    $almacenes = \App\Almacen::all();
    $moneda = \App\Famoneda::all();
    $categoria = \App\Facategoria::all();
    $color = \App\ColorPro::all();
    $modelo = \App\famodelo::all();
    $subcategoria = \App\Fasubcategoria::all();
    $tallas = \App\Tallas::all();
    
    //dd($categoria);
    return view('almacen.registrarArticulos')->with([

      'menus' => $menu->retornarMenu(),
      'almacenes'  => $almacenes,
      'monedas' => $moneda,
      'categoria' => $categoria,
      'color' => $color,
      'modelo' => $modelo,
      'sub' => $subcategoria,
      'tallas' => $tallas,
      'marcas' => famodelo::all()
    ]);
  }

  public function updateArticulo($id, Request $request)
  {
    
    $ID = $id;
    
    $menu = new HomeController();
    $articulos = \App\Caregart::where('codart', $ID)->with(['faartpvp' => function ($q) {
      $q->where('status', 'A');
    }, 'color', 'facategoria', 'subcategoria', 'moneda','getMarca', 'almacenes.almacenes', 'allTallas.tallas'])->first();
    $almacenes = \App\Almacen::all();
    $categoria = \App\Facategoria::all();
    $moneda = \App\Famoneda::all();
    $tallas = \App\Tallas::all();
    $subcategoria = \App\Fasubcategoria::all();
    $color = \App\ColorPro::all();
    $temp = explode(',', $articulos->codtallas);
    $cadena = '';
    // $alam = \App\Almacen::where('codalm',$almacen->codalm)->first();
    $recar = \App\Farecarg::where('status','S')->first()->monrgo;
    


    return view('almacen.actualizarRegistroArticulos')->with([
      'menus' => $menu->retornarMenu(),
      'articulos' => $articulos,
      'almacenes' => $almacenes,
      'categoria' => $categoria,
      'sub' => $subcategoria,
      'color' => $color,
      'tallas' => $tallas,
      'marcas' => famodelo::all(),
      'recargos' => $recar
      // 'moneda' => $moneda
    ]);
  }


  public function actualizarArticulo(Request $request, $id)
  {
    $rules = [
      'codigos'       => 'required|string|max:17',
      'nomart'        => 'required|string',
      'Descripcion'   => 'required|string',
      #'almacenes'     => 'required|array|min:1',
      //'precio'        => 'required',
      //'monedas'       => 'required',
      //'alm'           => 'required',
      'tipo'          => 'required', 
      'nomcolor'      => 'sometimes|nullable',
      #'marca' => 'required'
    ];
    
    #dd($request->nombre);
    $validate = \Validator::make($request->only('codigos', 'nomart', 'Descripcion', 'almacenes', 'nomcolor', 'tipo'), $rules);
    if ($validate->fails()) return redirect()->back()->withErrors($validate)->withInput();

     
    try {

      $cadena = '';
      if (!is_null($request->talla)) {

        for ($i = 0; $i < count($request->talla); $i++) {
          $cadena .= $request->talla[$i] . ' ';
        }
      }
      $search = ['/\./', '/,/'];
      $replace = ['', '.'];
      $monto = preg_replace($search, $replace, $request->precio);

      $sucuse_reg = Auth::user()->getsucUse()->codsuc;
      $guion1 = '-';
      $cont1 = faartpvp::withTrashed()->get()->count();
      $cont1 = $cont1 + 1;
      // $suc1 = Auth::user()->codsuc_reg;
      $suc1=Auth::user()->getCodigoActiveS();
      $codsuc_reg1 = explode('-', $suc1);
      $cod1 = $cont1 . $guion1 . $codsuc_reg1[1];

      $isSerial = null;
      if(isset($request->pSerial)){
        if($request->pSerial === '1' || $request->pSerial === true || $request->pSerial === "true"){
            $isSerial = true;
        }
      }else{
            $isSerial = false;
      }

      $ivaStatus = null;
      
      if(isset($request->iva)){
        if($request->iva === true || $request->iva === "true"){
          $ivaStatus = "true";
        }
      }else{
          $ivaStatus = "false";
      }
      

      Caregart::updateOrCreate(
        ['codart' => $request->codigos],
        [
          'desart' => $request->Descripcion,
          'nomart' => $request->nomart,
          'codcat' => $request->nomcategoria,
          'codsubcat' => $request->nomsubcategorias,
          'codcolor' =>  $request->nomcolor,
          'codtallas' =>  $cadena,
          'ctadef' => $request->cGasto,
          'codcta' => $request->cCosto,
          'ctatra' => $request->cTransitoria,
          'tipreg' => $request->pfactur,
          'codpar' => $request->partidas,
          'codsgp' => $request->np,
          'codigomarca' => $request->marca,
          'tipo' => $request->tipo,
          'isserial' => $isSerial,
          'ivastatus' => $ivaStatus,
          'codsuc' => $sucuse_reg,
          'codsuc_reg' => $sucuse_reg

        ]
      );
      $almacenesDelete = caartalm::where('codart', $request->codigos)->delete();
      $tallasDelete = Caarttal::where('codart', $request->codigos)->delete();
 
      /* if ($request->almacenes && count($request->almacenes) > 0) {
        foreach ($request->almacenes as $key => $value) {
            $almacenes[] = [
                'codart' => $request->codigos,
                'codalm' => $value,
                'codsuc' => $sucuse_reg
            ];
        }
      caartalm::insert($almacenes);
    }
 */
    if ($request->tallas && count($request->tallas) > 0) {
      foreach ($request->tallas as $key => $value) {
          $tallas[] = [
              'codart' => $request->codigos,
              'codtalla' => $value,
          ];
        }
      Caarttal::insert($tallas);
  }

    } catch (\Exception $e) {
  
      Session::flash('error', 'Error Contacte el administrador de sistemas ' . $e->getMessage());
      return redirect()->back();
    }

    Session::flash('success', 'Se actualizo el Articulo');
    return redirect()->back();

  }


  public function deleteArticulo($id)
  {

    #dd($id);
    // $desc = Helper::desencriptar($id);
    // $sep  = explode('-', $desc);
    // $ID = $sep[1] . '-' . $sep[2];
    // #dd($ID);
    $search = \App\Caregart::where('codart', $id)->first();
    if (Caregart::notDeleteBill($search->codart)) {
      return response()->json([

        'titulo' => 'NO SE PUEDE ELIMINAR EL ARTICULO POR TIENE FACTURA'
      ]);
    } else {

      $precio = \App\faartpvp::where('codart', $search->codart)->where('status', 'A')->where('deleted_at', null)->delete();
      $search->delete();
    }
    // dd($search);
    if ($search != null) {

      return response()->json([
        'titulo' => 'SE HA ELIMINADO CON EXITO'
      ]);
    }
  }

  public function preciosArticulo(Request $request)
  {

    $menu = new HomeController();
    
    $alma = \App\Almacen::whereNULL('deleted_at')->wherenotNULL('codsuc_asoc')->get();
    $categoria = \App\Facategoria::all();
    $moneda = \App\Famoneda::all();
    $articulos = \App\Caregart::orderBy('codart', 'asc')->where('tipreg', 'F')->whereNull('deleted_at')->where('artprecio', 'INACTIVO')->get();

    $sec = Segususuc::where('codusuario', Auth::user()->codigoid)->where('activo', 'TRUE')->first();
    return view('almacen.registrarPrecios')->with('menus', $menu->retornarMenu())->with('alma', $alma)->with('categoria', $categoria)->with('monedas', $moneda)->with('articulos', $articulos)->with('precio', $this->query());
  }

  public function IngresarpreciosArticulos(Request $request)
  {


    $request->validate([
      'codigos'   => 'required|string',
      'monedass'  => 'required|string',
      #'precio'    => 'required'
    ]);

    $precio = faartpvp::where('codart', $request->codigos)->where('status', 'A')->whereNull('deleted_at')->first();

    $articulos = \App\Caregart::where('codart', $request->codigos)->whereNull('deleted_at')->where('artprecio', 'ACTIVO')->where('artprecio', 'INACTIVO')->first();


    /* if (!is_null($precio) || !is_null($articulos)) {


                Session::flash('error', 'El codigo del articulo '.$request->codigos.', se encuentra registrado');
                return redirect('/modulo/Almacen/Artculos');

             }else{*/


    $sucuse_reg = Auth::user()->getsucUse()->codsuc;
    $guion1 = '-';
    $cont1 = faartpvp::withTrashed()->get()->count();
    $cont1 = $cont1 + 1;
    // $suc1 = Auth::user()->codsuc_reg;
    $suc1=Auth::user()->getCodigoActiveS();
    $codsuc_reg1 = explode('-', $suc1);
    $cod1 = $cont1 . $guion1 . $codsuc_reg1[1];

    $search = ['/\./', '/,/'];
    $replace = ['', '.'];
    $monto = preg_replace($search, $replace, $request->precio);
    $precio = new faartpvp();
    $precio->codart = $request->codigos;
    $precio->pvpart = $monto;
    // $precio->codmone = $request->monedass;
    // $precio->codalm = $request->nomalm;
    $precio->codsuc_reg = $sucuse_reg;
    $precio->codigoid = $cod1;
    $precio->status = 'A';
    #event(new Auditoria($precio,'created'));
    $precio->save();

    Caregart::where('artprecio', 'INACTIVO')->where('codart', $request->codigos)->update(['artprecio' => 'ACTIVO']);
    #event(new ($precio));
    Session::flash('success', 'El Precio con El Artículo que tiene el codigo ' . $request->codigos . ', se registro Exitosamente');

    $menu = new HomeController();
    return redirect('/modulo/Almacen/Artculos')->with('menus', $menu->retornarMenu());

    //}
  }

  public function RegistroArticulos(Request $request)
  {
    $rules = [
      'codigos'       => 'required|string|max:17',
      'nomart'        => 'required|string',
      'Descripcion'   => 'required|string',
      #'almacenes'     => 'required|array|min:1',
      //'precio'        => 'required',
      //'monedas'       => 'required',
      //'alm'           => 'required',+
      'nomart' => 'required|string',
      'nomcolor'      => 'sometimes|nullable',
      'tipo' => 'required'
    ];
    
    #dd($request->nombre);
    $validate = \Validator::make($request->only('codigos', 'nomart', 'Descripcion', 'almacenes', 'nomcolor', 'tipo'), $rules);
    if ($validate->fails()) return redirect()->back()->withErrors($validate)->withInput();

    try {
      DB::beginTransaction();

      if (is_null($request->talla) && ($request->nomcategoria === '2-0002' || $request->nomcategoria === '1-0002')) {
        Session::flash('error', 'Tiene que agregar la talla');
        return redirect()->back();
      }

      $search = Caregart::where('codart', $request->codigos)->first();

      if ($search != null) {
        Session::flash('error', 'Este Codigo Esta Asociado A Un Articulo ' . $search->desart);
        return redirect()->back();
      }

      $sucuse_reg = Auth::user()->getsucUse()->codsuc;

      $guion1 = '-';
      $cont1 = Caregart::withTrashed()->get()->count();
      $cont1 = $cont1 + 1;
      // $suc1 = Auth::user()->codsuc_reg;
      $suc1=Auth::user()->getCodigoActiveS();
      $codsuc_reg1 = explode('-', $suc1);
      $cod1 = $cont1 . $guion1 . $codsuc_reg1[1];
      $cadena = null;
      #   dd($request->all());
      if (is_array($request->talla)) {

        for ($i = 0; $i < count($request->talla); $i++) {
          if ($request->talla[$i] !== null) {

            $cadena .= $request->talla[$i] . ' ';
          }
        }
      }
      $serial = false;
      if(isset($request->pSerial)){
       $serial = ($request->pSerial === 'T') ? true : false;
      }

      Caregart::create([
        'codart' => $request->codigos,
        'nomart' => $request->nomart ?? '',
        'desart' => $request->Descripcion,
        'codcolor' => $request->nomcolor,
        'codcat' => $request->nomcategoria,
        'codsubcat' => $request->nomsubcategorias,
        'tipreg' => 'F',
        'codsuc_reg' => $sucuse_reg,
        'codigoid' => $cod1,
        'artprecio' => 'ACTIVO',
        'desfac' => $request->codigos,
        'tipo' => $request->tipo,
        // 'desfac' => $request->codigos,
        'codigomarca' => $request->marca,
        'codtallas' => $cadena,
        'codcta' => $request->cCosto,
        'ctatra' => $request->cTransitoria,
        'ctadef' => $request->cGasto,
        'codpar' => $request->partidas,
        'codsgp' => $request->np,
        'isserial' => $serial,
        'codsuc' => $sucuse_reg,
        'codsuc_reg' => $sucuse_reg
        ]);

        /* if ($request->almacenes && count($request->almacenes) > 0) {
          foreach ($request->almacenes as $key => $value) {
              $almacenes[] = [
                  'codart' => $request->codigos,
                  'codalm' => $value,
                  'codsuc' => $sucuse_reg
              ];
          }
      caartalm::insert($almacenes);
      } */

      if ($request->tallas && count($request->tallas) > 0) {
        foreach ($request->tallas as $key => $value) {
            $tallas[] = [
                'codart' => $request->codigos,
                'codtalla' => $value,
            ];
        }
      Caarttal::insert($tallas);

    }
      DB::commit();

    } catch (\Exception $e) {
      DB::rollback();

      Session::flash('error', $e->getMessage());
      return redirect()->back();
    }


    Session::flash('success', 'El Artículo ' . $request->Descripcion . ', se registro Exitosamente');
    return redirect()->back();
  }

  public function filtrosprecios($id)
  {

    $articulos = DB::connection('pgsql2')->table('faartpvp')->where('faartpvp.codart', $id)->where('faartpvp.status', 'A')->first();

    //dd($articulos);

    return response()->json([

      'titulo' => $articulos->pvpart
    ]);
  }

  public function ingresarPrecio($id)
  {
    $articulos = \App\Caregart::with(['almacen2', 'costoPro'])->where('codart', $id)->first();
   
    $menu = new HomeController();
    $moneda = \App\Famoneda::all();
    $almacen = Auth::user()->isAdmin() ? \App\Almacen::all()  : \App\Almacen::where('codsuc_asoc',session('codsuc'))->get();
    $precio = faartpvp::where('codart', $articulos->codart)->where('status', 'A')->with(['getTasa' => function ($q) {
    $q->where('activo', 'TRUE');
    }])->value('pvpart');

    return view('almacen.ingrePrecioArticulo')->with('menus', $menu->retornarMenu())->with(['articulos' => $articulos, 'moneda' => $moneda, 'alma' => $almacen , 'precio'=>$precio]);
  }

  public function ingresarPrecioP(Request $request, $id)
  {
    $id2  = faartpvp::where('codart','=', $id)->value('id');
    #dd($request->all());
    $rules = [
      'precio'    => 'required',
      'monedas'   => 'required',
    ];

    $validate = \Validator::make($request->only('precio', 'monedas'), $rules);
    if ($validate->fails()) {
      return redirect()->back()->withErrors($validate)->withInput();
    }

    $menu = new HomeController();
    $articulos = \App\Caregart::all();

    try {
      DB::beginTransaction();
      $search = faartpvp::where('codart', $id)->first();
      $guion1 = '-';
      $cont1 = faartpvp::withTrashed()->get()->count();
      $cont1 = $cont1 + 1;
      // $suc1 = Auth::user()->codsuc_reg;
      $suc1=Auth::user()->getCodigoActiveS();
      $codsuc_reg1 = explode('-', $suc1);
      $cod1 = $cont1 . $guion1 . $codsuc_reg1[1];
      
        faartpvp::where('codart','=',$id)->where('status','=','A')->update(['status' => 'I']);
        faartpvp::create([
          'codart' => $id,
          'status' => 'A',
          'pvpart' => $request->precio,
          'codigoid' => $cod1,
          'codsuc_reg' => $suc1,
          'codmone' => $request->monedas,
          'codalm' => ''
        ]);
       
      DB::commit();
    } catch (\Exception $e) {
      DB::rollBack();
      Session::flash('error', $e->getMessage());

      return redirect()->route('listadoArticulos.Almacen');
    }
    Session::flash('success', 'El Precio del Artículo se actualizo Exitosamente');
    return redirect()->route('listadoArticulos.Almacen');
  }

  public function getTallas($id)
  {
    $tallas = \App\facategoria::where('codigoid', $id)->first();

    return response()->json([
      'tallas' => ($tallas->talla === true) ? true : false
    ]);
  }
  public function mostrarAsoc($id)
  {
    $subcategoria = Fasubcategoria::where('id_facategoria', $id)->get();

    $temp = '<option value="" disabled selected>Eliges una Subcategoria</option>';

    foreach ($subcategoria as $key) {
      $temp .= '<option value="' . $key->codigoid . '">' . $key->nomsubcat . '</option>';
    }

    $tallas = \App\Tallas::with('Categoria')->where('codcategoria', $id)->get();
    $string = '';
    foreach ($tallas as $key) {
      $string .= "<option value={$key->codtallas}>{$key->tallas}";
    }

    return response()->json([

      'subcategoria' => $temp,
      'tallas' => $string !== '' ? $string : null,
      //'habTalla' => $this->getTallas($id)
    ]);
  }

  public function searchCode($id)
  {
    $articulos = Caregart::where('codart', $id)->first();

    if ($articulos != null) {
      return response()->json([
        'titulo' => 'Existe El Codigo Registrado'
      ]);
    }

    return response()->json([
      'titulo' => 'No Existe El codigo Registrado'
    ]);
  }

  public function getPartida()
  {
    
    $datos = \App\Cpdeftit::orderBy('codpre','DESC')
    ->whereRaw('LENGTH(codpre) < 14')/*pone el tamaño del campo al cual se quiere filtrar*/
    ->select(['codpre','nompre'])
    ->get();
    
      return DataTables::of($datos)->addColumn('action',function($datos){

             $button = "<a href='#' class='check1 ponercta' 
                        onclick='getPartidas(\"$datos->codpre\")' >
                        <i class='fas fa-check'></i></a>";
              return $button;
            })->rawColumns(['action'])->make(true);
      }

  public function getContabb()
  {
    $datos = \App\contabb::where('cargab','C')
            ->select(['codcta','descta'])->get();
      #dd($datos);
      return DataTables::of($datos)->addColumn('action',function($datos){

              $button = "<a href='#' class='check1 ponercta'  onclick='getId(\"$datos->codcta\")' ><i class='fas fa-check'></i></a>";
              return $button;
            })->rawColumns(['action'])->make(true);
    }

  public function getContabb1()
  { $datos = \App\contabb::where('cargab','C')->select(['codcta','descta'])->get();

    return DataTables::of($datos)->addColumn('action',function($datos){

            $button = "<a href='#' class='check1 ponercta'  onclick='getId2(\"$datos->codcta\")' ><i class='fas fa-check'></i></a>";
            return $button;
          })->rawColumns(['action'])->make(true);
  }

  public function getCideftit()
  { 
    
    $datos = \App\Models\cideftit::select(['codpre','nompre'])
              ->get();
    
    return DataTables::of($datos)->addColumn('action',function($datos){

            $button = "<a href='#' class='check1 ponercta' onclick='getId3(\"$datos->codpre\")' ><i class='fas fa-check'></i></a>";
            return $button;

          })->rawColumns(['action'])->make(true);
  }

  public function changeStatusArt(Request $request)
  {
    //dd($request);
    $articulo = Caregart::where('codart', $request["codart"])->first();
 //dd($articulo);
    if (!$articulo) {
      // session()->flash('error', 'Hubo un error al obtener codigo de articulo');
      // return redirect()->route('listadoArticulos.Almacen');
    }
    if ($articulo->staart === 'I' || !$articulo->staart) {
      //dd($articulo->staart);
      $string = "Se activo el producto {$articulo->desart} correctamente";
      $articulo->staart = 'A';
      $articulo->save();
    } else if ($articulo->staart === 'A') {
     // dd($articulo->staart);
      $string = "Se desactivo el producto {$articulo->desart} correctamente";
      $articulo->staart = 'I';
      $articulo->save();
    }
    // session()->flash('success', $string);
    // return redirect()->route('listadoArticulos.Almacen');
  }
}
