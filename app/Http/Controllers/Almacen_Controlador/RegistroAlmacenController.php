<?php

namespace App\Http\Controllers\Almacen_Controlador;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use Helper;
use Illuminate\Contracts\Auth\Guard;
use App\Sucursal;
use App\User;
use App\Almacen;
use Exception;
use Session;
use Illuminate\Support\Facades\Auth;
use \App\permission_user;
use Carbon\Carbon;
use App\Segususuc;
use App\Repositories\{SucursalRepository};


class RegistroAlmacenController extends Controller
{
     private $guard;
     private $sucursalAu;
	 public function __construct(Guard $guard,SucursalRepository $sucursalAu)
    {
        $this->guard = $guard;
        $this->middleware('auth');
        $this->sucursalAu = $sucursalAu;
        
    }
    public function show()
    {	
    
        if(Auth::user()->role->nombre === 'ADMIN')
        $alma = \App\Almacen::OrderBy('id','DESC')
                ->where('deleted_at',NULL)->paginate(8);
          else 
               $alma = \App\Almacen::OrderBy('id','DESC')
                ->where('codsuc_asoc',$this->sucursalAu->sucursalAuth())
                ->where('deleted_at',NULL)->paginate(8);

        $menu = new HomeController();
        

        return view('almacen.lista')->with('almacen',$alma)->with('menus',$menu->retornarMenu());
    	
    }

    public function editar(Request $request)
    {	



    	 $cod = $this->guard->user()->userSegSuc->loguse;
        //dd($Seg);
       // $this->authorize('role',$cod); 
        $almacen = \App\Almacen::find($request->id);

      	return view('almacen.editarAlmacen')->with('almacen',$almacen);
    }

    public function update(Request $request,$id)
    {	  
        #dd($request->all()); 
        $request->validate([
            // 'codigo'     => 'required',
            'Descripcion'=> 'required',
            'direccion'  => 'required',
        ]);
        
    	$menu = new HomeController();
    	$almacen = \App\Almacen::find($id);

    	#$Auditoria = new \App\Activity();
        
        $sucuse_reg=Auth::user()->getsucUse()->codsuc;
        $guion='-';
        $cont = \App\Almacen::get()->count();
        $cont = $cont+1;
        // $suc = Auth::user()->codsuc_reg;
        $suc=Auth::user()->getCodigoActiveS();
        $codsuc_reg = explode('-',$suc);
        $cod = $cont.$guion.$codsuc_reg[1];
      #  dd($cod);
        try{
            \DB::beginTransaction();

        $almacen->nomalm = $request->Descripcion;

    	//dd($request->codigo);
        $existpfactur = Almacen::where('codsuc_asoc',$almacen->codsuc_asoc)
                        ->where('pfactur',true)->where('codigoid','<>',$almacen->codigoid)->get();
          #  dd($existpfactur);
        //if(!$existpfactur->isEmpty() && $request->pfactur == 'on') throw new Exception("Existe ya un almacen predeterminado para facturar");
        	// $almacen->codalm = $request->codigo;
        	// $almacen->codcat = $request->codcat;
        	$almacen->diralm = $request->direccion;
            $almacen->codsuc_asoc = $request->sucu;
           # $Auditoria->codigoid = $cod;
            $almacen->pfactur = $request->pfactur == 'on' ? TRUE : FALSE;
        	$almacen->save();
            \DB::commit();

    	Session::flash('success', 'El Almacen '.$request->Descripcion.' se actualizó Exitosamente');
        return redirect()->route('Listado.Almacen');
        }catch(Exception $e){
           # dd($e->getMessage());
           \DB::rollback();
        Session::flash('error',$e->getMessage());
        return redirect()->route('Listado.Almacen');    
        }
    
}

    public function actualizarDatos(Request $request,$id)
    { 
        //Gloria A DIOS
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $almacen = \App\Almacen::find($ID);
        $sucursal =  Auth::user()->isAdmin() ? Sucursal::where([['deleted_at',NULL]])->get()  :  Sucursal::where([['deleted_at',NULL],['codsuc',session('codsuc')]])->get();

        $menu = new HomeController();
        return view('almacen.actualizarDatos')->with(
            [
                'menus' => $menu->retornarMenu(),
                'almacen' => $almacen,
                'sucursal' => $sucursal,
            ]   
        ); 
    }

    public function registrar()
    {	
    	$menu = new HomeController();
        $sucursal= Sucursal::where([['deleted_at',NULL]])->whereNotNull('codsuc')->get();
    	return view('almacen.registro')->with(
            [
                'menus' => $menu->retornarMenu(),
                'sucursal' => $sucursal,
            ]
        );
    }

    public function registro(Request $request)
    {	
    	$sucuse_reg=Auth::user()->getsucUse()->codsuc;
        $sep=explode('-', $sucuse_reg);
        
        $request->validate([
            // 'codigo'     => 'required',
            'Descripcion'=> 'required',
            'direccion'  => 'required',
        ]);
        //$existpfactur = Almacen::where('codsuc_asoc',$request->sucuc)->where('pfactur',true)->get();
        try{
            
            $sucuse_reg=Auth::user()->getsucUse()->codsuc;
            $guion='-';
            $cont = \App\Almacen::withTrashed()->get()->count();
            $cont = $cont+1;
            // $suc = Auth::user()->codsuc_reg;
            $suc=Auth::user()->getCodigoActiveS();
            $codsuc_reg = explode('-',$suc);
            $cod = $cont.$guion.$codsuc_reg[1];

           # $alm=Almacen::get()->count();
            #$codigoid=($alm+1).'-'.$sep[1];
            $alm=Almacen::get()->count();
            $codigoid=($alm+1).'-'.$sep[1];
          //  if(!$existpfactur->isEmpty() && $request->pfactur == 'on') throw new Exception("Ya existe un almacen de facturacion predeterminado para esta sucursal");
            
            $ultCodalm=Almacen::Orderbydesc('codalm')->first();
            // dd($ultCodalm);
            //$codalm= ($ultCodalm ? str_pad($ultCodalm->codalm+1, 6, '0', STR_PAD_LEFT) : '000001');
            
            $alma = new \App\Almacen();
            $alma->codalm = $codigoid;
            $alma->nomalm = $request->Descripcion;
            $alma->codcat = $request->tipo;
            $alma->diralm = $request->direccion;
            $alma->codsuc_asoc= $request->sucuc;
            $alma->codigoid= $cod;
            $alma->codsuc_reg= $codsuc_reg;
            $alma->codigoid= $codigoid;
            $alma->codsuc_reg= $sucuse_reg;
            $alma->pfactur = $request->pfactur == 'on' ? TRUE : FALSE;
            $alma->save();
        
        Session::flash('success', 'El Almacén '.$request->Descripcion.', se registro Exitosamente');
        return redirect('/modulo/Almacen/RAlmacen');
    	// $almac = \App\Almacen::OrderBy('id','ASC')->paginate(8);
    	// $menu = new HomeController();
    	// return redirect()->route('Listado.Almacen')->with('menus',$menu->retornarMenu())->with('almacen',$almac);
        }catch(Exception $e){
            Session::flash('error', 'Se presentó un problema en el registro, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
            return redirect('/modulo/Almacen/RAlmacen');
        }
        
    }

    public function eliminar(Request $request,$id)
    {	
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        	
			$almace = \App\Almacen::where('id',$ID)->has('existencia','<=',0)->delete();
            
            if($almace !== 0){
                
	        	return response()->json([
    				'total' => 'SE HA ELIMINADO CON EXITO',
    			]);
            }else{
                return response()->json([
    				'total' => 'NO SE HA ELIMINADO YA QUE EL ALMACEN TIENE INVENTARIO DISPONIBLE O ESTA PREDETERMINADO PARA FACTURAR',
    			]);
            }
    		
    }


    public function filtro(Request $r){
        //dd($r->filtro);
        $menu = new HomeController();


        $almacen = \App\Almacen::where('deleted_at',NULL)->where('nomalm','ilike','%'.$r->filtro.'%')->Orwhere('id','ilike','%'.$r->filtro.'%')->Orwhere('diralm','ilike','%'.$r->filtro.'%')->paginate(5);
        //dd($almacen);
        return view('almacen.lista')->with('menus',$menu->retornarMenu())->with('almacen',$almacen);
    }
}
