<?php

namespace App\Http\Controllers\Almacen_Controlador;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Controllers\HomeController;
use App\Catipent;
use App\Caentalm;
use App\Almacen;
use App\Caartalmubimov;
use App\User;
use Helper;
use Exception;
use Session;
use Illuminate\Support\Facades\Auth;
use \Fpdf;
use App\Http\Controllers\generadorPDF;
use Carbon\Carbon;
use App\Segususuc;

class MovimientosInventarioController extends Controller
{
    use SoftDeletes;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /*Función que ejecuta la inserción de los archivos en la base de datos*/
    public function create(Request $request)
    {
        $valor=request()->all();
        $sucuse_reg=Auth::user()->getsucUse()->codsuc;
        $sep=explode('-', $sucuse_reg);

        $request->validate([
            'descripcion'=>'required|string|min:5|max:50'
        ]);

        try{
            
            $tp=Catipent::get()->count();
            $codigoid=($tp+1).'-'.$sep[1];

            $ultCodent=Catipent::Orderbydesc('codtipent')->first();
            $codtipent= ($ultCodent ? str_pad($ultCodent->codtipent+1, 3, '0', STR_PAD_LEFT) : '001');

            $searchValidation = Catipent::where([['destipent',$valor['descripcion']]])->first();
            if($searchValidation !== null) throw new Exception("Tipo de movimiento existe");
            

            Catipent::create(
                [
                    'codtipent' => $codtipent,
                    'destipent' => $valor['descripcion'],
                    'codsuc_reg' => $sucuse_reg,
                    'codigoid' => $codigoid
                ]
            );
            Session::flash('success', 'El tipo de Movimiento '.$valor['descripcion'].', se registro Exitosamente');
            return redirect()->route('movimientos.inventario.almacen');

        }catch(Exception $e){
            Session::flash('error', 'Se presentó un problema en el registro, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
            return redirect()->route('movimientos.inventario.almacen');
        }

    }  

    /*Función para  ingresar a la edición del archivo*/
    public function edit ($id)
    {
        $menu = new HomeController();
    
        $desc=Helper::desencriptar($id);
        $sep=explode('-', $desc);
        $ID=(int)$sep[1];
        $tipomovimiento =Catipent::findOrFail($ID);
        $accion='edicion';
        return view('/almacen/registrarMovimientoAlmacen')->with('menus',$menu->retornarMenu())->with('tipomovimiento',$tipomovimiento)->with('accion',$accion);
    }

    /*Funcion que realiza la edicion de los campos*/
    public function update(Request $request, $id)
    {
        $valor=request()->all();
        $sep=explode('|', $id);

        $request->validate([
            'descripcion'=>'required|string|min:5|max:50',
        ]);

        try{
            // $tipomovimiento= Catipent::findOrFail($id)->update(['destipent'=>$valor['descripcion']]);
            $tipomovimiento= Catipent::where([['id',$sep[0]],['codigoid',$sep[1]]])->update(['destipent'=>$valor['descripcion']]);
            Session::flash('success', 'El tipo de Movimiento '.$valor['descripcion'].', se actualizó Exitosamente');
            return redirect()->route('movimientos.inventario.almacen');

        }catch(Exception $e){
            Session::flash('error', 'Se presentó un problema en la Edición, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
            return redirect()->route('movimientos.inventario.almacen');
        }

    }

     /*funcion de eliminar el registro*/
    public function delete ($id)
    {
        $desc=Helper::desencriptar($id);
        $sep=explode('|', $desc);
        $ID= (int) $sep[1];
        $codig= (string) $sep[2];
        
        $comprobar= Caentalm::where('codtipent',$codig)->first();
        
        if($comprobar)
        {
             return response()->json([
                'titulo' => 'error tipo de movimiento no puede ser eliminado'
            ]);
        }
        else
        {
            $verifica=Catipent::findOrFail($ID)->update(['deleted_at'=>date('Y-m-d h:m:i')]);
            //Cuando el inventario este registrado validar no eliminar 
            return response()->json([
                'titulo' => 'El tipo de movimiento se elimino'
            ]);
        }
    }

    public function index()
    {	
        $Mov = Catipent::orderBy('id','DESC')->where('deleted_at',NULL)->paginate(8);

    	$menu = new HomeController();
    	return view('almacen.listaTiposdeMovimientos')->with('menus',$menu->retornarMenu())->with('movi',$Mov);
    }

    /*Boton registrar que aparece en el listado*/
    public function nuevoTipoMovimiento()
    {
        $menu = new HomeController();
        $accion='registro';
        return view('/almacen/registrarMovimientoAlmacen')->with('menus',$menu->retornarMenu())->with('accion',$accion);
    }

    /*busqueda de filtro*/
    public function filtro(Request $request)
    {
        $menu = new HomeController();

        $tipomovimiento= Catipent::where('deleted_at',NULL)->where('destipent','ilike','%'.$request->filtro.'%')
        ->OrWhere('codtipent','ilike','%'.$request->filtro.'%')->OrWhere('id','ilike','%'.$request->filtro.'%')->paginate(8);


        return view('almacen.listaTiposdeMovimientos')->with('menus',$menu->retornarMenu())->with('movi',$tipomovimiento);
    }

    // public function registro()
    // {
    //     $menu = new HomeController();

    //     return view('almacen.registrarMovimientoAlmacen')->with('menus',$menu->retornarMenu());
    // }

    public function vistaReporte(){
        $almacenes = Almacen::get();
        return view('/reportes/movimientosReporte')->with(['menus'=> Helper::getmenu(),'almacenes' => $almacenes]);
    }

    public function generarReporte(Request $request){
      
         $desde = $request['desde'];
         $hasta = $request['hasta'];
         $diames = $request['diames'];
         
         switch ($diames) {
            case 'diario':
               $tipo = 'DIARIO';
            break;
            case 'ayer':
                $tipo = 'DÍA ANTERIOR';
            break;
            case 'semanal':
               $tipo = "SEMANAL";
            break;
            case 'semant':
               $tipo = "SEMANA ANTERIOR";
            break;
            case 'mensual':
                $tipo = 'MENSUAL';
            break;
            case 'mesant':
              $tipo = 'MES ANTERIOR';
            break;
            case 'anual':
              $tipo = 'ANUAL';
            break;
            case 'antyear':
              $tipo = 'AÑO ANTERIOR';
            break;
            default:
              $tipo = 'INTERVALO ENTRE '.$desde.' HASTA '.$hasta;
            break;

        }

  try{
            if(!$request->almacen)throw new Exception("Debe seleccionar un almacen");
            if($request->almacen =="TODAS"){
                $general = true;
            }
            else{
                $general = false;
                $codalm = $request->almacen;
            }

        }
        catch(Exception $e){
            return redirect()->route('reportesMinventario')->with('error',$e->getMessage());
        }
        
      ///EVALUA LOS PERIODOS DE TIEMPO SELECCIONADOS Y SI SERA UNA CONSULTA GENERALIZADA O POR SUCURSAL
      try{
               
               if($general){
               $movimientos = Caartalmubimov::with('almacenfrom','almacento','ubifrom','ubito','articulo','talla')
                             ->date(Carbon::parse($desde)->format('Y-m-d'),Carbon::parse($hasta)->format('Y-m-d'))->get();
             
               }
               else{
               
               $movimientos = Caartalmubimov::with('almacenfrom','almacento','ubifrom','ubito','articulo','talla')
                              ->date(Carbon::parse($desde)->format('Y-m-d'),Carbon::parse($hasta)->format('Y-m-d'))
                              ->where('codalmto','=',$codalm)->get();
               
               }
               
        if($movimientos->isEmpty()){
                Session::flash('error', 'No existen registros con los parametros de busqueda ingresados');
                return redirect()->route('reportesMinventario');}
        }//try
        catch(Exception $e){
            Session::flash('error', 'Se presentó un problema en la generación del reporte, Comuniquese con el Administrador del Sistema: '.$e->getMessage());
            return redirect()->route('reportesMinventario');
        }
            $pdf = new generadorPDF();

            $titulo = 'REPORTE DE MOVIMIENTOS DE INVENTARIO';
            $total = $movimientos->count();
            $fecha = date('Y-m-d');
    
            $pdf->AddPage('L');
            $pdf->SetTitle('REPORTE DE MOVIMIENTOS DE INVENTARIO');
            $pdf->SetFont('arial','B',16);
            $pdf->SetWidths(array(90,90));
            $pdf->Ln();
            $pdf->Cell(0,22,utf8_decode($titulo),0,0,'C');
            $pdf->Ln();
            $pdf->SetFont('arial','B',10);
            if($tipo == 'Intervalo'){
                $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo.' '.$desde.' - '.$hasta),0,0,'L');
            }
            else{
                $pdf->Cell(0,0,utf8_decode('TIPO DE REPORTE: '.$tipo),0,0,'L');
            }
            $pdf->Ln(5);
            $pdf->Cell(0,0,utf8_decode('FECHA: '.$fecha),0,0,'L');
            $pdf->Ln(5);
            $pdf->Cell(0,0,utf8_decode('TOTAL: '.$total),0,0,'L');
            $pdf->Ln(10);
            $pdf->SetFont('arial','B',7,5);
            $pdf->SetFillColor(2,157,116);
            $pdf->Cell(35,8,utf8_decode('COD.MOVIMIENTO'),1,0,'C');
            $pdf->Cell(20,8,utf8_decode('TIPO MOV.'),1,0,'C');
            $pdf->Cell(30,8,utf8_decode('ALM. ORIGEN'),1,0,'C');
            $pdf->Cell(30,8,utf8_decode('UBI. ORIGEN'),1,0,'C');
            $pdf->Cell(30,8,utf8_decode('ALM. DESTINO'),1,0,'C');
            $pdf->Cell(30,8,utf8_decode('UBI. DESTINO'),1,0,'C');
            $pdf->Cell(30,8,utf8_decode('ARTICULO'),1,0,'C');
            $pdf->Cell(20,8,utf8_decode('TALLA'),1,0,'C');
            $pdf->Cell(30,8,utf8_decode('CANTIDAD'),1,0,'C');
            $pdf->Cell(25,8,utf8_decode('FECHA'),1,0,'C');
            $pdf->Ln();
            $pdf->SetWidths(array(35,20,30,30,30,30,30,20,30,25));
            $pdf->SetAligns(['C','C','C','C','C','C','C','C','C','C','C','C']);
            $pdf->SetFont('arial','',7,5);
            
            /*return $movimientos;*/

            foreach($movimientos as $movimiento){    
            if($movimiento->almacenfrom)$almacenfrom = $movimiento->almacenfrom->nomalm;
            else $almacenfrom = '.';
            if($movimiento->almacento)$almacento = $movimiento->almacento->nomalm; 
            else $almacento = '.';
            if($movimiento->ubifrom)$ubifrom = $movimiento->ubifrom->nomubi;
            else $ubifrom = '.';
            if($movimiento->ubito)$ubito = $movimiento->ubito->nomubi; 
            else $ubito = '.';
            if($movimiento->talla)$talla = $movimiento->talla->tallas;
            else $talla = '.';

                $pdf->Row(array($movimiento->codmov,$movimiento->desmov,$almacenfrom,$ubifrom,$almacento,$ubito,$movimiento->articulo->desart,$talla,$movimiento->cantmov,$movimiento->created_at->format('Y/m/d')));
            }
            $pdf->SetTextColor(0,0,0);
            $pdf->Ln(5);
            $pdf->Output('I','MOVIMIENTOS INVENTARIO-'.$fecha.'pdf');
            
           exit;
    }
}