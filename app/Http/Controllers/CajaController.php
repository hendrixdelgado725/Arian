<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Http\CajaRequest;
use App\Models\Menu;
use \App\Models\Caja;
use \App\Sucursal;
use App\Fafactur;
use App\User;
Use App\farecarg;
//use App\Logs_reportes;
use App\Turno_cajero;
use App\Models\logs_reportes;
use App\Repositories\BroadcastRepository;
use Exception;
use Session;
use Helper;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use App\permission_user;
use App\Http\Controllers\HomeController;
use App\Segususuc;
use App\Repositories\{CierreRepository,SucursalRepository,FiscalRepositoryApi,PosPrinterApi,

};
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;
use ElephantIO\Client;


class CajaController extends Controller
{
    private $guard;
    private $cierre;
    //public  $wsrepo;
    private $sucursalAu;
    private $fiscal;
    public $posApi;
    public function __construct(
      Guard $guard,
      CierreRepository $cierre,
      SucursalRepository $sucursalAu,
      //BroadcastRepository $wsrepo
      FiscalRepositoryApi $fiscal,
      PosPrinterApi $posApi

    )
    {
        $this->middleware('auth');
        $this->guard  = $guard;
        $this->cierre = $cierre;
        $this->sucursalAu = $sucursalAu;
        //$this->wsrepo = $wsrepo;
        $this->fiscal = $fiscal;
        $this->posApi     = $posApi;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $menu = new HomeController();
        session(['activo' => request()->path()]);

        $cajas = Caja::orderby('id', 'DESC')
        ->where('codsuc',$this->sucursalAu->sucursalAuth())
        ->with('lastfact')
        ->paginate(20);

        $this->updateFacCaja($cajas); 

        return view('cajas.cajalist')->with(['menus' => $menu->retornarMenu(),'caja' => $cajas]);
      }

    public function IvaAdm($id){

        $desc=Helper::desencriptar($id);
        $sep=explode('-', $desc);
        $ID= (int) $sep[1];
        $caja = Caja::where('id',$ID)->with('almacen')->first();
        $menu = new HomeController();
        $iva = farecarg::get();
        session(['activo' => request()->path()]);
        return view('cajas.cajaIva')->with(['menus' => $menu->retornarMenu(), 'iva' => $iva, 'caja' => $caja]);

    }

    public function cambiarIva(Request $request, $id){
     
      if($request->acepiva == 1){
          $aceptaIva = true;
          $codigoIVA = $request->codiva;
      } else {
          $aceptaIva = false;
          $codigoIVA = '';
      }
      
      try{  
        $caja = Caja::findOrFail($id);
        $caja->acepiva = $aceptaIva;
        $caja->codiva = $codigoIVA;
        $caja->save();
        $r = $caja->save();
        if($r){
          if($codigoIVA != ''){
            Session::flash('success','A añadido el IVA a la caja Exitosamente');
            return redirect()->route('cajaList');
          } else {
            Session::flash('success','No se a añadido ningun IVA a la caja');
            return redirect()->route('cajaList');
          }
        }
      }catch(Exception $e){
        dd($e);
        Session::flash('error' ,$e->getMessage());
        return redirect()->route('IvaAdm',$id);
      }

    }
      
    public function create()
    { 
        
        $correla = Caja::getCountCorre()+1 .'F';
        $alm = \App\Almacen::all();
        return view('cajas.cajacreate')
               ->with(['menus'=> $this->getmenu(),
                       'sucursales' => Sucursal::get(),
                       'correla'=> $correla,
                       'alm' => $alm]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
      $sucuse_reg=Auth::user()->getsucUse()->codsuc;
      
      $request->validate([
      'descaj' => 'required',
      'impfisname' => 'required',
      'impfishost' => 'required|ip',
      'impserial' => 'required|max:10',
      'codsuc' => 'required',
      'codalm' => 'required',
      'corfac' => 'required',
      ]);

      try {
        $verif = Caja::where('impfishost',$request->impfishost)->get();
          
         if(!$verif->isEmpty())   throw new Exception("hay otra caja con la direccion IP no puedes tener la misma direccion ", 1); 
        
      } catch(Exception $e){
        
        
        Session::flash('error' ,$e->getMessage());
        return redirect()->route('cajaCreate');
      }


      $correlativo = Caja::getCountCorre()+1 .'F';
      $cocaja =  Caja::getcount()+1;//obtiene el numero total de cajas + 1 y lo concatena con la sucursal a añadir
        $r = Caja::create([
          'descaj' => $request->descaj,
          'impfisname' => $request->impfisname,
          'impfishost' => $request->impfishost,//localhost estaticos por requerimientos de conexion del sistema
          'impserial' => $request->impserial,
          'codsuc' => $request->codsuc,
          'codsuc_reg'=> $sucuse_reg,
          'codigoid' => $cocaja."-".explode('-',$request->codsuc)[1],
          'correlativo' => $correlativo,
          'codalm' => $request->codalm,
          'corfac' => $request->corfac,
          'cornot' => $request->cornot,
          'is_fiscal' => $request->isFiscal
        ]);
        if($r) Session::flash('success','La Caja'.$request['impfisname'].', se registro Exitosamente');
        else Session::flash('error','Error de aplicacion');
      return redirect()->route('cajaList');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
      $desc=Helper::desencriptar($id);
      $sep=explode('-', $desc);
      $ID= (int) $sep[1];
      
      $caja = Caja::where('id',$ID)->with('almacen')->first();
      
      $sucursales = Sucursal::where('deleted_at',null)/* ->where('codsuc','!=',$caja->codsuc) */->get();
      $alm = \App\Almacen::all(); 

      return view('cajas.cajaedit')->with(['menus'=>$this->getmenu(),
             'caja' => $caja,
             'sucursales' => $sucursales,
             'alm' => $alm
             ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      
      
      $request->validate([
        'descaj' => 'required',
        'impfisname' => 'required',
        'impfishost' => 'required',
        'impserial' => 'required|max:10',
        'codsuc' => 'required',
        'corfac' => 'required',
        'cornot' => 'required'
      ]);

      try{
        
        $verif = Caja::where('impfishost',$request->impfishost)->get();
        
        if(!$verif->isEmpty())   throw new Exception("hay otra caja con la direccion IP no puedes tener la misma direccion ", 1); 
        
        
        $caja = Caja::findbycod($id);
        
        $caja->descaj = $request->descaj;
        $caja->impfisname = $request->impfisname;
        $caja->impfishost = $request->impfishost;
        $caja->impserial = $request->impserial;
        //$caja->idsucur = $request->idsucur;
        $caja->codsuc = $request->codsuc;
        $caja->codalm = $request->codalm;
        $caja->corfac = $request->corfac;
        $caja->cornot = $request->cornot;
        $caja->is_fiscal = $request->isFiscal === "true" ? "true" : "false";
        $r = $caja->save();
        if($r){
        Session::flash('success','La Caja'.$request->impfisname.', se actualizó Exitosamente');
        return redirect()->route('cajaList');
        }
      }catch(Exception $e){
        $rand  = rand(1, 9999);
        $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
        $enc=\Helper::EncriptarDatos($nrand.'-'.$id);        
        
        Session::flash('error' ,$e->getMessage());
        return redirect()->route('cajaEdit',$enc);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function filtro(Request $request){
      $menu = new HomeController();
      $cajas = Caja::with('sucursales')->where('descaj','ilike','%'.$request->filtro.'%')
      ->orWhere('impfisname','ilike','%'.$request->filtro.'%')
      ->orWhere('impserial','ilike','%'.$request->filtro.'%')
      ->orWhereHas('sucursales',function($q) use ($request){
          $q->where('nomsucu','ilike','%'.$request->filtro.'%');
      })->orderBy('id','DESC')->paginate(8);
      
      return view('cajas.cajalist')->with(['menus'=>$menu->retornarMenu(),'caja'=>$cajas]);
    }
    
    public function destroy(Request $request, $id)
    {
      $id = Helper::decrypt($id);
      try{
        $deleted = Caja::findbycod($id);
        //DB::statement()//validar que no exista un log con esta caja
        if(!$request->ajax())throw new Exception("Not an Ajax Request");
        $deleted = $deleted->delete();
        if($deleted) return response()->json(['exito' => 'Caja Eliminada Exitosamente']);
      }catch(Exception $e){
        return response()->json(['error' => $e->getMessage()]);
      }
    }
    
    public function getmenu(){
        return Menu::getmenu();
    }

    public function getconfview($id){
        $id = Helper::decrypt($id);
        $caja = Caja::findOrFail($id);
        //dd($caja);
        $lastX  = Logs_reportes::where([
          ['cod_caja',$caja->codigoid],
          ['tipo_reporte','X']
        ])->whereNull('error')->latest('id')->first();

        $lastZ  = Logs_reportes::where([
          ['cod_caja',$caja->codigoid],
          ['tipo_reporte','Z']
        ])->whereNull('error')->latest('id')->first();

        $lastXP = Logs_reportes::where([
          ['cod_caja',$caja->codigoid],
          ['tipo_reporte','XP']
        ])->whereNull('error')->latest('id')->first();
        
        $string_x   = $lastX  ? self::getString($lastX->created_at_system)  : null;
        $string_z   = $lastZ  ? self::getString($lastZ->created_at_system)  : null;
        $string_xp  = $lastXP ? self::getString($lastXP->created_at_system) : null;


        return view('cajas.cajaconf')
        ->with([
          'caja'      => $caja ,
          'menus'     => $this->getmenu(),
          'string_x'  => $string_x,
          'string_z'  => $string_z,
          'string_xp' => $string_xp,
        ]);
    }


    public function sendTickera($id,Request $request) {
      
      


      $success =  $this->posApi->cierre($id,$request->all()['fecha']);
      
      return $success === 200 || $success === true ?
        response()->json(['success' => true], 200) :
        response()->json(['error' => true,'msg' => $success["msg"]]);
    }


    public static function getString($date)
    {
      if(!$date) return null;
      $string_x = Carbon::parse($date)->isToday() ? 'Ultimo Reporte Hoy' : '';
      
      $fecha = Carbon::parse($date)->format('l j n F Y');
      $hora = Carbon::parse($date)->isoFormat('hh:mm:s a');
      
      return $string_x.' '.Helper::getSpanishDate($fecha).' a las '.$hora;
    }

    protected function checkSideKiq(string $shell = '')
    {
      if($shell === '') return false;
      $array =  explode("\n",$shell ?? '');
      foreach ($array as $key => $value) {
        return Str::contains($value,'busy]');
      }
    }

    public function Reporte_old($id,$job){
      //obtener ip de impresora
      // DEBE COINCIDIR CON EL SERVIDOR REDIS CONFIGURADO
      try{
        //$shell =  \shell_exec('ps aux | grep side');
        //$sidekiq = $this->checkSideKiq($shell ?? '');
        //if(!$sidekiq) throw new Exception("No se puede conectar a la impresora fiscal, comuníquese con los administradores de sistema, o pruebe reiniciar el equipo");

        $now = DB::select('select LOCALTIMESTAMP');//LA HORA REAL DEL SISTEMA
        $now[0]->timestamp;
        $before = Logs_reportes::get()->count();
        $codigo = str_pad($before+1,9,'0',STR_PAD_LEFT);
        $Caja= Caja::findOrFail($id);
        $this->wsrepo->EmmitFiscal('ReporteWorker',$Caja->id);
        $redis = new \Predis\Client(array(
          'host' => $Caja->impfishost,
          'port' => '6379',
          //'database' => 12,
        ));
        $sidekiq = new \SidekiqJobPusher\Client($redis, $Caja->impfisname);
        $args = [
          'tipo'       => $job,
          'codigo'     => $codigo,
          'cod_caja'   => $Caja->codigoid,
          'created_at' => $now[0]->timestamp,
          'caja_id'    => $Caja->id,
          '_token'     => \csrf_token()
        ];
        $sidekiq->perform('ReporteWorker', [$args]);
        return response()->json(['exito' => 'Reporte Enviado'],200);

      }catch(Exception $e){
        return response()->json([
          'error'=> $e->getMessage()
        ],500);
        //error: "php_network_getaddresses: getaddrinfo failed: Name or service not known [tcp://N/A:6379]"
          /* Error indica que el ip de la maquina no es igual al del redis */
      }
//      dd($evento);
      
  }

  public function Reporte($id,$job){
    try{
      $now = DB::select('select LOCALTIMESTAMP');//LA HORA REAL DEL SISTEMA
      
      $before = Logs_reportes::get()->count();
      $codigo = str_pad($before+1,9,'0',STR_PAD_LEFT);
      $Caja= Caja::findOrFail($id);
      
      $data = [
        'tipo'       => $job,
        'codigo'     => $codigo,
        'cod_caja'   => $Caja->id,
        'created_at' => isset($now[0]->localtimestamp) ? $now[0]->localtimestamp : $now[0]->timestamp,
      ];

      //$response = Http::post('http://'.$Caja->impfishost.':3500/sendReporte',$data);
      //

        $url = 'http://'.$Caja->impfishost.':3500';

        $options = ['client' => Client::CLIENT_4X];
        $client = Client::create($url, $options);
        $client->connect();
        
        $client->emit('sendReporte',$data);
        $packet = $client->wait('response-reporte');/*configurado por defecto maximun wait 30seg */

        if($packet){

          $data = $packet->data;
          $args = $packet->args;
          // access data
          $response = $data;
        }
        
        $getResponse = $this->getResponse($response['status']);
        $client->disconnect();

      if($getResponse && $job === 'Z'){
         $User = \App\cajaUser::where('loguse',Auth::user()->loguse)->where('codcaja',$Caja->id)->first();
         $User->activo = 'FALSE';
         $User->save();

         //HACER UNA EVALUCION DEL DIA ACTUAL
         $facturaReporte = \App\Fafactur::whereBetween('fecfac',[Carbon::now()->format('Y-m-d'),Carbon::now()->format('Y-m-d')])
                                          ->get()->map(function ($q)
                                          {
                                            return $q->fecfac;
                                          });
          if($facturaReporte->isNotEmpty())//HACE UN UPDATE CON RESPECTO DE LA FACTURA DEL DIA ACTUAL
             $facturaReporteUpdate = \App\Fafactur::whereIn('fecfac',$facturaReporte)->update(['cierre' => TRUE]);
         
         
      }

      if($getResponse !== true) throw new Exception("Error con la api fiscal");
      else {

        logs_reportes::create([
          'tipo_reporte' => $response['tipo_reporte'],
          'codigo' => $response['codigo'],
          'cod_caja' => $response['cod_caja'],
          'created_at_system' => $response['created_at_system'] 
        ]);

      }

      
      return response()->json(['exito' => 'Reporte Enviado','reporte' => $job],200);

    }catch(Exception $e){
      return response()->json([
        'error'=> $e->getMessage()
      ],500);
    }
  }

  public function getResponse($response)
  {
    if($response !== 200) {
      return $response;
    }else{
      return true;
    }
  }

  public function getTimeFormated(){
    
  }

  public function getStatus(Request $request,$id)
  {
      $Caja= Caja::findOrFail($id);
      $getResponse = $this->fiscal->getStatus($Caja->impfishost);
    
      return $getResponse;

    
        $redis = new \Predis\Client(array(
          'host' => $Caja->impfishost,
          'port' => '6379',
        ));
        $sidekiq = new \SidekiqJobPusher\Client($redis, $Caja->impfisname);
        $evento = $sidekiq->perform('StatusWorker','N');
    return response()->json(["exito" => 'Solicitud de Estatus enviada']);
  
  }

  public function getCierreCajaView(){
    session(['activo' => request()->path()]);

      $menus = new HomeController();
      $menus = $menus->retornarMenu();
      $cajas = Caja::get();
    
      return view('reportes.cierreCajaReportes',compact('menus','cajas'));
      
  }

  public function getCierreCaja(Request $request){


    $error = $this->cierre->getCierreCajaR($request);

    if($error){
      Session::flash('warning','No existen datos que mostrar');
      return redirect()->route('CierreCajaView');
    }

  }

  public function getTurnoCaja(Request $request)
  {
    if (!$request->ajax()){
      session::flash('error','Petición no permitida');
      return redirect()->route('CierreCajaView');
    }
    $day = Carbon::parse($request->date)->day;
    $turnos = Turno_cajero::where('codcaja',$request->caja)
    ->where('status',Turno_cajero::CERRADO)
    ->whereDay('created_at',$day)
    ->orderBy('id','asc')
    ->get();
    session(['activo' => request()->path()]);

    if($turnos->isEmpty()){
      return view('reportes.turnosCierreCaja',compact('turnos'));
    }else{
      $all =[
        'codigoid'    => 'ALL',
        'name'        => 'TODOS',
        'name_cajero' => ''
      ];
      $turnos[] = $all;
      $turnos = $turnos->toArray();
      return view('reportes.turnosCierreCaja',compact('turnos'));
    }
  }

  private function updateFacCaja($cajas)
  {
    try
    {
 
  //  if(Fafactur::count() === 0) throw new Exception('Facturas del año pasado');
    
      foreach($cajas as $caja)
      {
        $caja->cornotcre = Fafactur::where('codcaj', $caja->id)->withTrashed()->count();
        $caja->save();
      }

  }
  catch(Exception $e){
    //'/modulo/ConfiguracionGenerales/Cajas/cajas'
    Session::flash('warning', $e->getMessage());
    return redirect()->route('cajaList');
  }

  }

}