<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ColorRequest;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\ColorPro;
use Helper;
use Illuminate\Support\Facades\Auth;
use User;
use App\Caregart;
use Session;
use App\Segususuc;

class ColorProController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $menu = new HomeController();
        $colores = ColorPro::orderBy('id','DESC')->paginate(8);
        return view('/configuracionGenerales/color')->with('menus',$menu->retornarMenu())->with('colores',$colores);
    }

    public function nuevoColor(){
        $menu = new HomeController();
        return view('/configuracionGenerales/registroColor')->with('menus',$menu->retornarMenu());
    }

    public function create(ColorRequest $request){


            $guion='-';
            $cont = ColorPro::withTrashed()->get()->count();
            $cont = $cont+1;
            // $suc = Auth::user()->codsuc_reg;
            $suc=Auth::user()->getCodigoActiveS();
            $codsuc_reg = explode('-',$suc);
            $cod = $cont.$guion.$codsuc_reg[1];

            $comprobar=ColorPro::withTrashed()->where('color',$request['nombreColor'])->orWhere('nomencolor',$request['nomencolor'])->first();
            
            if($comprobar)
            {
                if($comprobar['deleted_at']==NULL)
                {
                    if ($comprobar['nomencolor']==$request['nomencolor'])
                    {
                        Session::flash('error', ' La Nomeclatura  '.$request['nomencolor'].' posee un registro Activo en el sistema, verificar información.');
                    }
                    else
                    {
                        Session::flash('error', ' El Color  '.$request['nombreColor'].' posee un registro Activo en el sistema, verificar información.');   
                    }
                    return redirect()->route('listaColores');
                }
                else
                {
                    $comprobar->restore();
                    $comprobar->update([
                    'color'=>$request['nombreColor'],
                    'nomencolor'=>$request['nomencolor'],
                    'codsuc'=>$suc
                    ]);

                    Session::flash('success', 'El color '.$request['nombreColor'].', se registro Exitosamente');
        return redirect()->route('listaColores');
                }
            }
            ColorPro::create([
                'color'=>$request['nombreColor'],
                'nomencolor'=>$request['nomencolor'],
                'codigoid'=>$cod,
                'codsuc'=>$suc,
            ]);

        Session::flash('success', 'El color '.$request['nombreColor'].', se registro Exitosamente');
        return redirect()->route('listaColores');
    }

    public function edit($id){
        $menu = new HomeController();
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        
        $ID= (int)$sep[1];
        $colorP = ColorPro::find($ID);

        return view('/configuracionGenerales/editarColor')->with('menus',$menu->retornarMenu())->with('colorP',$colorP);
    }

    public function update(ColorRequest $request, $id){
  
        try {
            $comprobar=ColorPro::withTrashed()->where('nomencolor',$request['nomencolor'])->first();
            $colorP = ColorPro::find($id);
            
            if($comprobar !== null)
                if($comprobar->color !== $colorP->color) throw new \Exception("Error en actualizar, porque hay un color con la nomeclatura ".$request['nomencolor'], 500);
            
            
            $colorP->color = $request['nombreColor'];
            $colorP->nomencolor = $request['nomencolor'];
            $colorP->save();

        } catch (\Exception $th) {
            $rand  = rand(1, 9999);
            $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
            $enc=Helper::EncriptarDatos($nrand.'-'.$id);
            
            

            Session::flash('error', $th->getMessage());
            return redirect()->route('editarColor',$enc);
        }
        

        Session::flash('success', 'El color '.$request['nombreColor'].', se actualizó Exitosamente');
        return redirect()->route('listaColores');
    }

    public function delete($id){
        $desc=Helper::desencriptar($id);
        $sep  = explode('-',$desc);
        $ID= (int)$sep[1];
        $color = ColorPro::find($ID);
        $articulo = ColorPro::with('articulos')->has('articulos')->where('id','=',$ID)->get();

        if($articulo->isEmpty()){

            $color->delete();
            return response()->json([
                'titulo' => 'Exito se elimino'
            ]);
        }
        else{
            return response()->json([
                'titulo' => 'error Este color no puede ser eliminado'
            ]);

           }
    }

    public function filtro(Request $request){
        $menu = new HomeController();
        $colores = ColorPro::where('color','ilike','%'.$request->filtro.'%')->orWhere('nomencolor','ilike','%'.$request->filtro.'%')->orderBy('color','ASC')->paginate(8);

        return view('/configuracionGenerales/color')->with('menus',$menu->retornarMenu())->with('colores',$colores);
    }
}
