<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SucursalRequest;
use App\Models\Pais;
use App\Models\Estado;
use App\Models\Menu;
use App\Empresa;
use App\Sucursal;
use App\Almacen;
use App\Farecarg;
use App\Fasuc_rgo;
use App\Famoneda;
use App\Helpers\Helper;
use Carbon\Carbon;
use Session;
use Exception;
use Auth;
use App\Segususuc;
use App\Models\Transpocost;
use App\Caregart;
use App\Facategoria;
use App\faartpvp;
class SucursalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {

        $sucursal =  Sucursal::orderby('id','desc')->with('pais')->with('estados')->paginate(10);
        return view('configuracionGenerales.Sucursales.listSucursal')->with(['menus'=>$this->getmenu(),'sucursal'=>$sucursal,'pais'=>Pais::get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $recarg=Farecarg::get();
        $empresa =  Empresa::get();
        $monedas = Famoneda::get();

        if(!$recarg->isEmpty() && !$empresa->isEmpty())
        {

          $recargos=Farecarg::where('status','S')->get();
          if($recargos->isEmpty())
            return redirect()->route('sucursalList')->with(['error' => 'No existen recargos activos, Verifique Información']);          
          
          return view('configuracionGenerales.Sucursales.createSucursal')->with(
            ['menus'=>$this->getmenu(),
            'pais'=>Pais::get(),
            'almacenes' => Almacen::get(),
            'recargos' => $recargos,
            'empresa' => $empresa,
            'monedas' => $monedas,
            ]);
        }
        else
        {
          return redirect()->route('sucursalList')->with(['error' => 'Para iniciar el proceso debe registrar Recargos en el Sistema y/o Empresas Definidas']);          
        }
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SucursalRequest $request)
    {
      if($request->codrif && strpos($request->codrif,'-')){
        $codrif= $request->tipodoc.str_replace('-',"",$request->codrif);
        $request->merge(['codrif'=> $codrif]);
        $request->validate(
          [
            'codrif' => 'required|unique:pgsql2.'.session('anio').'.fadefsu,codrif,null,id,deleted_at,NULL|between:10,10',
            'codempre' => 'required'
          ]
        );
      }

        $codsuc = $this->getcode(Sucursal::getlastcode());
        $totalsuc =  Sucursal::getcount()+1;
        $codigoid = Fasuc_rgo::getcount()+1;
        // dd($codigoid);

        /*try{*/
          $recargos = array_unique(array_filter($request->codrgo));
          if(count($recargos) === 0) throw new Exception("Debe seleccionar por lo menos un recargo");
          foreach ($recargos as $key => $value) {
          $totable[] = [
            'codrgo' =>  $value,//codrgo
            'codsuc' => $codsuc,
            'codigoid' => $codigoid.'-'.$this->getSuc(),
            'created_at' => Carbon::now(),
          ];
          }
          $r= Sucursal::create([
            'nomsucu'  => strtoupper($request->nomsucu),
            'dirfis'   => strtoupper($request->dirfis),
            'codrif'   => $request->codrif,
            'codpai'   => $request->codpai,
            'codedo'   => $request->codedo,
            'codsuc'   => $codsuc,
            'status'   => 'A',
            'codempre' => $request->codempre,
            'codigoid' => $totalsuc.'-'.explode('-',$codsuc)[1]
          ]);
          $sr = Fasuc_rgo::insert($totable);
          
          /*$suc = Auth::user()->codsuc_reg;*/
          $codsuc_reg1 = explode('-', $codsuc);
          $guion1 = '-';

          if($request->transportnom &&$request->moneda && $request->transportcost){
            
            $categoria = Facategoria::where('nombre','TRANSPORTE')->first();
            $cont = Caregart::withTrashed()->get()->count();
            $cont = $cont + 1;

            $cont1 = faartpvp::withTrashed()->get()->count();
            $cont1 = $cont1 + 1;
           /* $suc1 = Auth::user()->codsuc_reg;*/
            $cod = $cont.$guion1.$codsuc_reg1[1];
            $cod1 = $cont1.$guion1.$codsuc_reg1[1];

            $productotruck = new Caregart();
            $codi = explode('-', $codsuc);
            $codart = 'VITTRANSP-'.$codsuc_reg1[1];

            $productotruck->codart = $codart;
            $productotruck->desart = 'Costo de transporte'/*$request->transportnom*/;
            $productotruck->codcat = $categoria->codigoid;
            $productotruck->codigoid = $cod;
            $productotruck->codsuc_reg = $codsuc;
            $productotruck->artprecio = 'ACTIVO';
            $productotruck->save();
          

            $truckcosto = new faartpvp();
            $truckcosto->codart = $codart;
            $truckcosto->pvpart = $request->transportcost;
            $truckcosto->codsuc_reg = $codsuc;
            $truckcosto->codigoid = $cod1;
            $truckcosto->status = 'A';
            $truckcosto->codmone = $request->moneda;
            $truckcosto->save();

       /*     $transpocost = new Transpocost();
            $transpocost->moneda_id = $request->moneda;
            $transpocost->codsuc = $codsuc;
            $transpocost->costo = $request->transportcost;
            $transpocost->save();*/
          }
          
            if($r)Session::flash('success', 'La Sucursal '.$request->nomsucu.', se registro Exitosamente');
            else Session::flash('error', 'Error de Aplicacion');
   /*     }catch(Exception $e){
          return redirect()->route('sucursalCreate')->with(['error' => $e->getMessage()]);
        }*/

        return redirect()->route('sucursalList');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      //como el "id" viene como un string con un "-" se desencripta como un valor normal
      $id= Helper::decrypt($id);
//      dd($id);
      $sucursal = Sucursal::with('transporCost')->findOrFail($id);
      $categoria = Facategoria::where('nombre','TRANSPORTE')->orwhere('nombre','FLETE')->first();
      //$sucursal = Sucursal::findbycod($id);
      $pais = Pais::get();
      $empresa =  Empresa::get();
      $monedas = Famoneda::get();
      $estado = Estado::where('fapais_id','=',$sucursal->codpai)->orderby('nomedo')->get();
      $recargos = Sucursal::with('recargos')->where('deleted_at',null)->where('codsuc',$sucursal->codsuc)->get();
      $almacenSuc = Almacen::where('codsuc_asoc',$sucursal->codsuc);
      $beta = $recargos->toArray();
      $transporte = Caregart::with('costoUnit')->where('codcat',$categoria->codigoid)->where('codsuc_reg',$sucursal->codsuc)->where('artprecio','ACTIVO')->orderby('id','ASC')->first();
      return view('configuracionGenerales.Sucursales.editSucursal')->with(
        ['menus'=>$this->getmenu(),
        'pais'=>$pais,
        'estado' => $estado,
        'sucursal' => $sucursal,
        'recargos' => Farecarg::get(),
        'sucrgo' => json_encode($recargos),
        'countrcgo'=> $beta[0]["recargoCount"],
        'almacenes' => Almacen::getValid(),
        'empresa' => $empresa,
        'monedas'=>$monedas,
        'transporte'=>$transporte,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SucursalRequest $request, $id)
    {
    
      if($request->codrif && strpos($request->codrif,'-')){
        $codrif= $request->tipodoc.str_replace('-',"",$request->codrif);
        $request->merge(['codrif'=> $codrif]);
        //$request->validate(['codrif' => 'required|unique:pgsql2.'.session('anio').'.fadefsu,codrif,null,id,deleted_at,NULL|between:10,10']);
      }
 // dd($codrif);
      $sucursal = Sucursal::with('transporCost')->where('codigoid',$id)->first();
       // dd($sucursal);
      // $codigos = array_filter($request->codigoid);
      $codigos = array_filter($request->codigoid);

      
      try{
        $recargos = array_unique(array_filter($request->codrgo));
        $contr1 = Fasuc_rgo::withTrashed()->get()->count();
            
        $suc=Auth::user()->getCodigoActiveS();
        $codir = explode('-', $suc);
        $guion2 = '-';
        
        if(count($recargos) === 0) throw new Exception("Debe seleccionar por lo menos un recargo");
        foreach ($recargos as $key => $value) {
          $contr1= $contr1 + 1;
          $codr = $contr1 . $guion2 . $codir[1];

              $totable[] = [
              'codrgo' =>  $value,//codrgo
              'codsuc' => $sucursal->codsuc,
              'codigoid'=> $codr,
              'created_at' => Carbon::now(),
              'updated_at' => Carbon::now(),
              ];
        }
        
        $r = Fasuc_rgo::where('codsuc',$sucursal->codsuc);//borra los recargos que estan asociados y inserta los nuevos
        $r->delete();
        // dd($request->codrif); 
        Fasuc_rgo::insert($totable);
            $sucursal->nomsucu = strtoupper($request->nomsucu);
            $sucursal->dirfis = strtoupper($request->dirfis);
            $sucursal->codrif = $request->codrif;
            $sucursal->codpai = $request->codpai;
            $sucursal->codedo = $request->codedo;
            $sucursal->codempre = $request->codempre ?? 'VIT';
            $r = $sucursal->save();
// dd($r);
     /*   if($sucursal->transporCost != null){
          $transpo = Transpocost::where('codsuc',$sucursal->codsuc)->where('active',true)->get();
          if($sucursal->transporCost->moneda_id != $request->moneda || $sucursal->transporCost->costo != $request->transportcost){
            foreach ($transpo as $t) {
               $t->active = false;
               $t->save();
            }
            $transpocost = new Transpocost();
            $transpocost->moneda_id = $request->moneda;
            $transpocost->codsuc = $sucursal->codsuc;
            $transpocost->costo = $request->transportcost;
            $transpocost->save();
           }
          }else{
            if($request->moneda && $request->transportcost){
              $transpocost = new Transpocost();
              $transpocost->moneda_id = $request->moneda;
              $transpocost->codsuc = $sucursal->codsuc;
              $transpocost->costo = $request->transportcost;
              $transpocost->save();
            }
          }*/

            $categoria = Facategoria::where('nombre','TRANSPORTE')->first();
            $cont = Caregart::withTrashed()->get()->count();
            $cont = $cont + 1;
            // $suc1 = Auth::user()->codsuc_reg;
            $suc1=Auth::user()->getCodigoActiveS();
            $codi = explode('-', $suc1);
            $guion1 = '-';
            $cod = $cont . $guion1 . $codi[1];

            $cont1 = faartpvp::withTrashed()->get()->count();
            $cont1 = $cont1 + 1;
            $cod1 = $cont1.$guion1.$codi[1];

            

          if($sucursal->transporCost != null){
             $transpo = Caregart::with('costoUnit')->where('codsuc_reg',$sucursal->codsuc)->get();
             if($transpo->costoUnit->codmone != $request->moneda || $transpo->costoUnit->pvpart != $request->transportcost){

              foreach ($transpo as $t ) {
                $t->artprecio = 'INACTIVO';
                $t->save();
              }

               $productotruck = new Caregart();
               
               $codart = 'VITTRANSP-'.$codi[1];
               $productotruck->codart = $codart;
               $productotruck->desart = 'Costo de transporte'/*$request->transportnom*/;
               $productotruck->codcat = $categoria->codigoid;
               $productotruck->codigoid = $cod;
               $productotruck->codsuc_reg = $suc1;
               $productotruck->artprecio = 'ACTIVO';
               $productotruck->save();

               $truckcosto = new faartpvp();
               $truckcosto->codart = $codart;
               $truckcosto->pvpart = $request->transportcost;
               $truckcosto->codsuc_reg = $suc1;
               $truckcosto->codigoid = $cod1;
               $truckcosto->codmone = $request->moneda;
               $truckcosto->status = 'A';
               $truckcosto->save();
             }   

          }else{
             
               $productotruck = new Caregart();
               $codi = explode('-', $suc1);
               $codart = 'VITTRANSP-'.$codi[1];
               $productotruck->codart = $codart;
               $productotruck->desart = 'Costo de transporte'/*$request->transportnom*/;
               $productotruck->codcat = $categoria->codigoid ?? '';
               $productotruck->codigoid = $cod1;
               $productotruck->codsuc_reg = $suc1;
               $productotruck->artprecio = 'ACTIVO';
               $productotruck->save();

               $truckcosto = new faartpvp();
               $truckcosto->codart = $codart;
               $truckcosto->pvpart = $request->transportcost;
               $truckcosto->codsuc_reg = $suc1;
               $truckcosto->codigoid = $cod1;
               $truckcosto->codmone = $request->moneda;
               $truckcosto->status = 'A';
               $truckcosto->save();
          }

            if($r)Session::flash('success', 'La Sucursal '.$request->nomsucu.' se actualizó Exitosamente');
            else Session::flash('error', 'Error de Aplicacion');
      }catch(Exception $e){
        // dd($e);
        return redirect()->route('sucursalShow',$id)->with(['error' => $e->getMessage()]);
      }
        return redirect()->route('sucursalList');
    }

    public function filtro (Request $request){

      $sucursales = Sucursal::with('estados')->where('nomsucu','ilike','%'.$request->filtro.'%')
      ->orWhere('nomsucu','ilike','%'.$request->filtro.'%')
      ->orWhereHas('estados',function($q) use ($request){
          $q->where('nomedo','ilike','%'.$request->filtro.'%');
      })->orderBy('id','DESC')->paginate(8);

      return view('configuracionGenerales.Sucursales.listSucursal')->with(['menus'=>$this->getmenu(),'sucursal'=>$sucursales,'sec' => Segususuc::where('loguse',Auth::user()->loguse)->where('activo','TRUE')->first()->getSucursal->nomsucu]);
  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
      try{
      //  $codigoid = Helper::desencriptar($id);//id es el codigo 
        
       // if(!$request->ajax()) throw new Exception('Not an Ajax Request');
        $sucursal = Sucursal::where('id',$id)->first();
        $recargos = Fasuc_rgo::where('codsuc',$sucursal->codsuc);
        $sucursal->delete();
        $recargos->delete();
        return response()->json(['exito' => 'Sucursal Eliminada Exitosamente']);
      }catch(Exception $e){ 
        return response()->json(['error' => $e->getMessage()]);
      }
    }

    public function getmenu(){
        return $this->menu = Menu::getmenu();
    }

    public function getencrypt(){

    }

    public function getSuc(){
      return explode('-',Auth::user()->getCodigoActiveS())[1];
      // return explode('-',Auth::user()->codsuc_reg)[1];
    }

    public function getcode($modelo){

      $numero= ($modelo ? str_pad($modelo+1, 4, '0', STR_PAD_LEFT):'0001');
      $numfinal = 'SUC-'.$numero;
      return $numfinal;
    }
}
