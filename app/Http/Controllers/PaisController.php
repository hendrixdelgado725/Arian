<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pais;
use App\Models\Estado;
use App\Models\Menu;
use Session;
use Exception;
use Helper;
use App\Segususuc;
use Illuminate\Support\Facades\Auth;

class PaisController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paises = Pais::paginate(10);
        return view('pais.paislist')->with('paises',$paises)->with('menus',$this->getmenu());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pais.createp')
                    ->with('menus',$this->getmenu())->with('sec',Segususuc::where('loguse',Auth::user()->loguse)->where('activo','TRUE')->first()->getSucursal->nomsucu);
    }

    public function filtro(Request $request)
    {
      $paises = Pais::where('nompai','ilike','%'.$request->filtro.'%')->paginate(10);
      return view('pais.paislist')
                    ->with('paises',$paises)
                        ->with('menus',$this->getmenu());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request["nompai"] = strtoupper(Menu::eliminar_tildes($request["nompai"]));
        $mensajes = ['nompai.required' => 'Debes Seleccionar un Pais',
                      'nompai.unique' => 'El pais que seleccionaste ya se encuentra registrado'];
        $request->validate([
            'nompai' => 'required|unique:pgsql2.'.session('anio').'.fapais,nompai,NULL,id,deleted_at,NULL'
        ],$mensajes);
        $r = Pais::create([
            'nompai' => $request['nompai']
        ]);
        if($r)Session::flash('success', 'El Pais '.$request['nompai'].', se registro Exitosamente');
        else Session::flash('error', 'No se pudo registrar el pais');
        return redirect()->route('paisList');
                        //depues del store retornar a la url 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $ID= Helper::decrypt($id);
      $pais = Pais::find($ID);
      return view('pais.paisEdit')->with('pais',$pais)->with('menus',$this->getmenu());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $nombrePais = strtoupper(Menu::eliminar_tildes($request["nompai"]));
      $pais = Pais::find($id);
      $pais->nompai = $nombrePais;
      $pais->save();

      Session::flash('success', 'El país se actualizo Exitosamente');
      return redirect('/modulo/ConfiguracionGenerales/DTerritorial/Pais');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroypais(Request $request,$id)
    {
      try{
        if(!$request->ajax())throw new Exception("Not an Ajax Request");
        $id= Helper::decrypt($id);
        $pais = Pais::find($id);
        if(!Pais::candelete($id)->isEmpty()) throw new Exception("Existen Estados Registrados con este País");
        //validar que no exista algo con el pais seleccionado
        $res = $pais->delete();
        if($res) return response()->json(["exito" => "País Eliminado Exitosamente"]);
      }catch(Exception $e){
        return response()->json([
          "error" => $e->getMessage()
        ]);
      }
    }

    public function getmenu(){
        return $this->menu = Menu::getmenu();
    }

    protected function getpaisesPag(){
      return Pais::paginate(10);
    }
}
