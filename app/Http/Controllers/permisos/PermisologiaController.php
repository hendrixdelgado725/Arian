<?php

namespace App\Http\Controllers\permisos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;
use Helper;
use Illuminate\Contracts\Auth\Guard;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
#use App\permission_user;
use Illuminate\Support\Facades\DB;
use App\Segususuc;

use App\rolesusuarios;
use Illuminate\Support\Str;

class PermisologiaController extends Controller
{
  public $guard;
  public function __construct(Guard $guard)
  {
    $this->middleware('auth');
    $this->guard = $guard;
  }
  //para mostrar el listado de los usuarios con sus permisos
  public function index()
  {

    $menu = new HomeController();
    $role = \App\rolesusuarios::all();

    $user = \App\User::whereNull('deleted_at')->whereNotNull('codroles')->where('stablo', 'S')->paginate(8);

    return view('permisos.listPermisos')->with('menus', $menu->retornarMenu())->with('user', $user)->with('roles', $role);
  }

  //busqueda de los usuarios
  public function filtro(Request $request)
  {
    $menu = new HomeController();
    $user = \App\User::whereNull('deleted_at')->where('stablo', 'S')->where('loguse', 'ilike', '%' . $request->filtro . '%')->with('roles')->whereNotNull('codroles')->paginate(8);

    return view('permisos.listPermisos')->with('menus', $menu->retornarMenu())->with('user', $user);
  }
  //edita los roles de cada usuario
  public function addRolPermisos($id)
  {

    $desc = Helper::desencriptar($id);
    $sep  = explode('|', $desc);
    $ID = $sep[1];

    $menu = new HomeController();
    $user = \App\User::whereNull('deleted_at')->where('stablo', 'S')->where('id', $ID)->first();
    
    $roles = \App\rolesusuarios::all();
    $permisos = \App\permission::all();
    #dd($menu->retornarMenu());
    
    return view('permisos.editPermisosUser')->with('menus', $menu->retornarMenu())->with('user', $user)->with('roles', $roles)->with('permisos', $permisos);
  }

  //sirve para agregar los permisos a cada usuario
  public function addPermisos($id)
  {
    #dd($id);
    $menu = new HomeController();
    $paso = $menu->retornarMenu();
    #dd($paso);
    $array = [];
    $rutas = [];
    $i = 0;
    $contar = 0;
    if (is_array($paso['submodulo'][$id])) {
      $contar = $paso['submodulo'][$id];
    }
    $string = '';
    $i = 0;
    $rutas = [];
    foreach ($contar as $key => $value) {

      if (is_array($value)) {
        
        foreach ($value as $key1 => $value1) {
         
          $string .= '<tr>
                    <td class="indice" data-id="' . $i . '">' . $i . '</td>
                    <td><input name="numcuenta[]" value="' . $key1 . '" type="text" class="form-control "></td>
                    <td><input name="numcuenta[]" value="' . '/modulo/' . $value1 . '" type="text" class="form-control rutas"></td>
                    <td><label class="checkbox-inline">
                          <input type="checkbox" id="checkboxEnLinea1" data-id="' . $i . '" class="checkboxEnLinea1" value="contenidos.index-"> Navega todos los contenido del sistema<br>
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" data-id="' . $i . '" id="checkboxEnLinea2" class="checkboxEnLinea1" value="contenidos.edit-"> Editar los contenido del sistema<br>
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" data-id="' . $i . '" id="checkboxEnLinea3" class="checkboxEnLinea1" value="contenidos.delete-"> Eliminar los contenido del sistema<br>
                        </label>
                        <label class="checkbox-inline">
                          <input data-id="' . $i . '" type="checkbox" id="checkboxEnLinea4" class="checkboxEnLinea1" value="contenidos.create-"> Crear los contenido del sistema<br>
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" data-id="' . $i . '" id="checkboxEnLinea5" class="checkboxEnLinea5" value="contenidos.index-contenidos.edit-contenidos.delete-contenidos.create-"> Marcar todos<br>
                        </label>
                        </td>
                   </tr>';
                 

                   $i++;
          
        }
      } else {
        
        $string .= '<tr>
                      <td class="indice">' . $i . '</td>
                      <td><input data-id="' . $i . '" name="numcuenta[]" value="' . $key . '" type="text" class="form-control "></td>
                      <td><input name="numcuenta[]" data-id="' . $i . '" value="' . '/modulo/' . $value . '" type="text" class="form-control rutas"></td>
                      <td><label class="checkbox-inline">
                        <input data-id="' . $i . '" type="checkbox" id="checkboxEnLinea1"  class="checkboxEnLinea1" value="contenidos.index-"> Navega todos los contenido del sistema<br>
                                </label>
                          <label class="checkbox-inline">
                           <input data-id="' . $i . '" type="checkbox" id="checkboxEnLinea2" class="checkboxEnLinea1" value="contenidos.edit-"> Editar los contenido del sistema<br>
                          </label>
                           <label class="checkbox-inline">
                            <input data-id="' . $i . '" type="checkbox" id="checkboxEnLinea3" class="checkboxEnLinea1" value="contenidos.delete-"> Eliminar los contenido del sistema<br>
                                </label>
                                <label class="checkbox-inline">
                                  <input data-id="' . $i . '" type="checkbox" id="checkboxEnLinea4" class="checkboxEnLinea1" value="contenidos.create-"> Crear los contenido del sistema<br>
                                </label>
                                <label class="checkbox-inline">
                                  <input data-id="' . $i . '" type="checkbox" id="checkboxEnLinea5" class="checkboxEnLinea5" value="contenidos.index-contenidos.edit-contenidos.delete-contenidos.create-"> Marcar todos<br>
                                </label>
                                </td>
                     </tr>';
                     $i++;
      }
     

    
      
    }
   
   
    return [

      /*'submodulos' => $array,
      'rutas'    => $rutas*/
      'modulo' => $string,
      
    ];
  }

  //eliminar permisos y configurar el rol vacio
  public function regAdmin(Request $request, $id)
  {
    if ($request->ajax()) {

      //añadiendo el role Admin al usuario
      #$user = \App\User::where('codigoId', $id)->where('stablo', 'S')->update(['codroles' => $id2]);
      #$seg = \App\Segususuc::where('codusuario', $id)->whereNull('deleted_at')->update(['codroles' => $id2]);


      $per = \App\permission_user::where('codusuarios', $id)->whereNull('deleted_at')->first();

      $sucuse_reg = session('codsuc');
      $guion = '-';
      $cont = \App\permission_user::withTrashed()->get()->count();
      $cont = $cont + 1;
      $suc = Auth::user()->codsuc_reg;
      $codsuc_reg = explode('-', $suc);
      $cod = $cont . $guion . $codsuc_reg[1];
      $paso = true;


      $this->regAdmin2($request->datos, $id, count($request->datos), $sucuse_reg, 0, $cont, $codsuc_reg[1], 0, true);
    }


    return response()->json([

      'titulo' => 'por fin'
    ]);
  }
  /* */
  function regAdmin2($array, $id, $tam, $suc, $indice, $cont, $codsuc_reg, $index, $paso)
  {
    
    $permisos = array(
      0 => 'contenidos.index', 1 => 'contenidos.delete',
      2 => 'contenidos.edit', 3 => 'contenidos.create'
    );

    $tty = \App\permission_user::where('codusuarios', $id)->restore();
    $search = \App\permission_user::where('codusuarios', $id)->withTrashed()->get()->count();
    #dd(\App\permission_user::where('codusuarios',$id)->get());
    $guion = '-';
    $cont = \App\permission_user::withTrashed()->get()->count();
    $cont = $cont + 1;
    $suc = Auth::user()->codsuc_reg;
    $codsuc_reg = explode('-', $suc);
    $boo = false;
    $user = \App\User::where('loguse', $id)->whereNull('deleted_at')->first();

    if ($search === 0) { //cuando no existe ningun registro asociado con el usuario

      for ($i = 0; $i < $tam; $i++) {
        $cod = $cont + $i . $guion . $codsuc_reg[1];
        \App\permission_user::insert(
          [
            'codusuarios'  => $id,
            'rutasaccesso' => $array[$i],
            'codpermission' => $permisos[$indice] . "-" . $permisos[$indice + 1] . "-" . $permisos[$indice + 2] . "-" . $permisos[$indice + 3] . '-',
            'deleted_at' => null,
            'codsuc_reg' => $suc,
            'codigoid' => $cod,
            'updated_at' => Carbon::now()
            #'codemp' => $user->codemp


          ]
        );
      }

      $bool = true;
    } else { //cuando existe registro asociado con el usuario

      if ($search !== 0) {
        for ($i = 0; $i < $tam; $i++) {
          $cod = $cont + $i . $guion . $codsuc_reg[1];
          $tty = \App\permission_user::where('codusuarios', $id)->where('rutasaccesso', $array[$i])->first();
          if (!is_null($tty)) {
            #dd($user);
            #dd($tty);
            $t = \App\permission_user::where('codusuarios', $id)->where('rutasaccesso', $array[$i])->update(
              [
                'codpermission' => $permisos[0] . "-" . $permisos[1] . "-" . $permisos[2] . "-" . $permisos[3] . '-'
                # 'codemp' => $user->codemp

              ]
            );
            # var_dump($i);
          } else {
            #                    var_dump("llgoo");
            \App\permission_user::insert(
              [
                'codusuarios'  => $id,
                'rutasaccesso' => $array[$i],
                'codpermission' => $permisos[$indice] . "-" . $permisos[$indice + 1] . "-"
                  . $permisos[$indice + 2] . "-" . $permisos[$indice + 3] . '-',
                'deleted_at' => null,
                'codsuc_reg' => $suc,
                'codigoid' => $cod,
                'updated_at' => Carbon::now()

                #'codemp' => $user->codemp
              ]
            );
          }
        }
      }
    }
  }

  /*corregir cuando se encuentra repetido el permiso*/
  public function rPermisos(Request $request, $id)
  {
    
    $menu = new HomeController();
    
    try {
      # $per = \App\permission_user::where('codusuarios', $id)->delete();

      foreach ($request->perm as $key) {
        $temp = explode('-', $key);
        
        $this->agregarPermisos($temp[0], $temp[1], $id);
      }

      return response()->json([
        'titulo' => Session::flash('success', 'SE REGISTRO CON EXITO')
      ]);
    } catch (Exception $e) {
      return response()->json([
        'titulo' => Session::flash('warning', 'NO SE PUEDE REGISTRAR')
      ]);
    }
  }

  private function agregarPermisos($rutas, $permisos, $id)
  {
    $in = 0;
    $cadena = '';
    $sucuse_reg = session('codsuc');
    $t = \App\permission_user::where('codusuarios', $id)->where('rutasaccesso', $rutas)->delete();
    $guion = '-';
    $cont = \App\permission_user::withTrashed()->get()->count();
    $cont = $cont + 1;
    $suc = Auth::user()->codsuc_reg;
    $codsuc_reg = explode('-', $suc);
    $indice = 0;
    $paso = true;
    $cod = $cont . $guion . $codsuc_reg[1];
    $search = \App\permission_user::where('codusuarios', $id)->withTrashed()->get()->count();

    $user = \App\User::where('codigoid', $id)->whereNull('deleted_at')->first();


    $tty = \App\permission_user::where('codusuarios', $id)->where('rutasaccesso', $rutas)->restore();
    if ($search === 0) {
      #dd("llego 1");
      \App\permission_user::insert([
        'codusuarios' => $id,
        'codpermission' => $permisos . '-',
        'rutasaccesso' => $rutas,
        'codigoid' => $cod,
        'codsuc_reg' => $sucuse_reg,
        'created_at' => Carbon::now()
        #'codemp' => $user->codemp
      ]);
    } else {
      $bus = \App\permission_user::where('codusuarios', $id)->where('rutasaccesso', $rutas)->first();

      if ($bus !== null) {
        $temp = Str::contains($bus->codpermission, $permisos . '-');
        if ($temp === false) {
          \App\permission_user::where('codusuarios', $id)
            ->where('rutasaccesso', $rutas)->update([
              'codpermission' => $bus->codpermission . $permisos . '-',
              'deleted_at' => null #,'codemp' => $user->codemp
            ]);
        } else {

          \App\permission_user::where('codusuarios', $id)
            ->where('rutasaccesso', $rutas)->update([
              'codpermission' => $permisos . '-',
              'deleted_at' => null #'codemp' => $user->codemp
            ]);
        }
      } else {

        \App\permission_user::insert([
          'codusuarios' => $id,
          'codpermission' => $permisos . '-',
          'rutasaccesso' => $rutas,
          'codigoid' => $cod,
          'codsuc_reg' => $sucuse_reg,
          'created_at' => Carbon::now()
          #'codemp' => $user->codemp
        ]);
      }
    }

    # \App\permission_user::where('codusuarios', $id)->restore();
    return false;
  }

//quede eliminar

  public function delete($id)
  {
    $desc = Helper::desencriptar($id);
    $sep  = explode('|', $desc);
    $ID = $sep[1];

    $user = \App\User::where('id', $ID)->where('stablo', 'S')->first();
    $per = \App\permission_user::where('codusuarios', $user->loguse)->whereNull('deleted_at')->get();
    
    
    $paso1 = new HomeController();
    $paso  = $paso1->retornarMenu();
    $rutas = collect();

    $string = '';
    $valores = '';
    $i = 0;
    $permisos = array(
      0 => 'contenidos.index-', 1 => 'contenidos.delete-',
      2 => 'contenidos.edit-', 3 => 'contenidos.create-'
    );
    $per->each(function ($key, $values)
    use (&$rutas, $paso, &$valores, &$i, $permisos, $string) {
      $temp1 = Str::contains($key->codpermission, $permisos[0]);
      $temp2 = Str::contains($key->codpermission, $permisos[1]);
      $temp3 = Str::contains($key->codpermission, $permisos[2]);
      $temp4 = Str::contains($key->codpermission, $permisos[3]);
      foreach ($paso['submodulo'] as $key11 => $value11) {

        if (is_array($paso['submodulo'][$key11])) {
          foreach ($paso['submodulo'][$key11] as $key22 => $value22) {
            if (is_array($paso['submodulo'][$key11][$key22])) {
              foreach ($paso['submodulo'][$key11][$key22] as $key44 => $value44) {
                #var_dump('/modulo/'.$value44);
                if ($key->rutasaccesso === '/modulo/' . $value44) {
                  if ($temp1) {
                    $string .= "<label class='checkbox-inline'>
                                  <input data-id=" . $i . " type='checkbox'
                                   onclick='paso(\"$i\",this.id)' id='0'
                                   class='checkboxEnLinea" . $i . "'
                                    value='contenidos.index-'> Ver Contenido<br>
                                </label>";
                  }
                  if ($temp2) {
                    $string .= " <label class='checkbox-inline'>
                                  <input type='checkbox' data-id=" . $i . "
                                   id='1' onclick='paso(\"$i\",this.id)'
                                    class='checkboxEnLinea" . $i . "'
                                     value='contenidos.delete-'> Eliminar los contenido del sistema<br>
                                </label>";
                  }
                  if ($temp3) {
                    $string .= "<label class='checkbox-inline'>
                                  <input data-id=" . $i . "
                                   type='checkbox' id='2' onclick='paso(\"$i\",this.id)'
                                     class='checkboxEnLinea" . $i . "'
                                       value='contenidos.edit-'> Editar los contenido del sistema<br>
                                </label>";
                  }
                  if ($temp4) {
                    $string .= "<label class='checkbox-inline'>
                                  <input data-id=" . $i . " type='checkbox' id='3'
                                     onclick='paso(\"$i\",this.id)'
                                     class='checkboxEnLinea" . $i . "'
                                      value='contenidos.create-'> Crear los contenido del sistema<br>
                                </label>";
                  }
                  $valores .= "<tr>
                   <td class='id' data-id=" . $i . ">" . $i . "</td>
                   <td id='modulo" . $i . "' data-id=" . $i . "
                   data-id2=" . $key->rutasaccesso . ">" . $key44 . "</td>
                   <td class='indice' data-id =" . $i . " >
                   " . $string . "
                   </td>
                   </tr>";
                }
              }
            } else {
              // foreach ($paso['submodulo'][$key11] as $key33 => $value33) {
              if ($key->rutasaccesso === '/modulo/' . $value22) {

                if ($temp1) {
                  $string .= "<label class='checkbox-inline'>
                                  <input data-id=" . $i . " type='checkbox'
                                   onclick='paso(\"$i\",this.id)' id='0'
                                   class='checkboxEnLinea" . $i . "'
                                    value='contenidos.index-'> Ver Contenido<br>
                                </label>";
                }
                if ($temp2) {
                  $string .= " <label class='checkbox-inline'>
                                  <input type='checkbox' data-id=" . $i . "
                                   id='1' onclick='paso(\"$i\",this.id)'
                                    class='checkboxEnLinea" . $i . "'
                                     value='contenidos.delete-'> Eliminar los contenido del sistema<br>
                                </label>";
                }
                if ($temp3) {
                  $string .= "<label class='checkbox-inline'>
                                  <input data-id=" . $i . "
                                   type='checkbox' id='2' onclick='paso(\"$i\",this.id)'
                                     class='checkboxEnLinea" . $i . "'
                                       value='contenidos.edit-'> Editar los contenido del sistema<br>
                                </label>";
                }
                if ($temp4) {
                  $string .= "<label class='checkbox-inline'>
                                  <input data-id=" . $i . " type='checkbox' id='3'
                                     onclick='paso(\"$i\",this.id)'
                                     class='checkboxEnLinea" . $i . "'
                                      value='contenidos.create-'> Crear los contenido del sistema<br>
                                </label>";
                }
                $valores .= "<tr>
                   <td class='id' data-id=" . $i . ">" . $i . "</td>
                   <td id='modulo" . $i . "' data-id=" . $i . "
                   data-id2=" . $key->rutasaccesso . ">" . $key22 . "</td>
                   <td class='indice' data-id =" . $i . " >
                   " . $string . "
                   </td>
                   </tr>";
              }
              // }

            }
          }
        }
      }

      $i++;
    });

    $stringS = '<table id="idss" data-user=' . $user->loguse . '  class="table">
                  <thead class="thead-dark" >
                    <th>ID</th>
                    <th>Modulo</th>
                    <th>Permiso</th>
                  </thead>
                  <tbody class="detalles" id="detalestable">' . $valores .
      '</tbody>
                </table>';

    return response()->json([

      'modulo' => $stringS,
      'user' => $user->nomuse
    ]);
  }




  public function Search($id)
  {


    $desc = Helper::desencriptar($id);
    $sep  = explode('|', $desc);
    $ID = $sep[1];
    $menu = new HomeController();
    $paso = $menu->retornarMenu();
    //  $valores .= "<tr><td>{$key3}</td> <td>{$value4->codpermission}</td></tr>";
    $per = \App\permission_user::where('codusuarios', $ID)->whereNull('deleted_at')->get();
    $user = \App\User::where('codigoid', $ID)->where('stablo', 'S')->first();
    $temp = [];
    $indice = 0;
    $valores = '';
    $temp1 = [];
    $temp2 = [];

    foreach ($paso['submodulo'] as $key => $value) { //key ojo
      foreach ($per as $key22 => $value22) {
        # code...
        if (is_array($paso['submodulo'][$key])) {
          foreach ($paso['submodulo'][$key] as $key1 => $value1) {
            if (is_array($paso['submodulo'][$key][$key1])) {
              foreach ($paso['submodulo'][$key][$key1] as $key11 => $value11) {
                if (strcmp($value22->rutasaccesso, '/modulo/' . $value11) === 0) {

                  array_push($temp1, $key11);
                  array_push($temp2, $value22->codpermission);
                }
              }
            }
          }
        }
      }
    }

    foreach ($paso['submodulo'] as $key => $value) { //key ojo
      foreach ($per as $key22 => $value22) {

        if (is_array($paso['submodulo'][$key])) {

          foreach ($paso['submodulo'][$key] as $key1 => $value1) {
            if (is_array($paso['submodulo'][$key][$key1]) === false) {
              if (strcmp($value22->rutasaccesso, '/modulo/' . $value1) === 0) {
                array_push($temp1, $key1);
                array_push($temp2, $value22->codpermission);

                #$valores .= "<tr><td>{$key1}</td>
                #           <td>{$value22->codpermission}</td></tr>";
                break;
              }
            }
          }
        }
      }
    }

    $indice = 0;
    foreach ($temp1 as $key) {
      $valores .= "<tr><td>{$key}</td>
                   <td>{$temp2[$indice]}</td></tr>";
      $indice++;
    }

    $string = '<table id="ids" class="table">
                  <thead class="thead-dark">
                    <th>Modulo</th>
                    <th>Permiso</th>
                  </thead>
                  <tbody class="detalles" id="detalestable">' . $valores .
      '</tbody>
                </table>
           ';

    return response()->json([
      'nombre'   => $user->nomuse,
      'roles'    => $user->roles->nombre,
      'permisos' => $per,
      'modulo'   => $string

    ]);
  }



  //sirve para agregar los permisos de un administrador
  public function Admin($id)
  {
    
    if (strcmp($id, 'R-001') === 0) { //cuando el rol es Admin
      $menu = new HomeController();
      $paso = $menu->retornarMenu();
      
      $array = [];
      $rutas = [];
      $modulos = [];
      $indice = 0;
      $submodulos = [];
      $indice2 = 0;
      $sumodulos1 = [];
      $indice3 = 0;
      $ruta1 = [];
      $ruta2 = [];
      if (is_array($paso['submodulo'])) {

        foreach ($paso['submodulo'] as $key => $value) {

          $modulos[$indice] = $key; //para capturar el nombre del modulo
          
          if (is_array($paso['submodulo'][$key])) {

            foreach ($paso['submodulo'][$key] as $key2 => $value2) {

              if (is_array($paso['submodulo'][$key][$key2])) {
                foreach ($paso['submodulo'][$key][$key2] as $key3 => $value3) {

                  $submodulos[$indice2] = $key3;
                  $ruta1[$indice2] = "/modulo/" . $value3;
                  $indice2++;
                }
              } else {

                $submodulos[$indice2] = $key2; //para capturar el nombre del submodulo
                $ruta1[$indice2] = "/modulo/" . $value2;
                $indice2++;
              }
            } //segundo foreach
          } //segundo if

          $indice++;
        } //foreach 1
        
        return response()->json([
          'submodulos' => $submodulos,
          'rutas1'     => $ruta1, //rutas del submodulos
        ]);
      }
    } //termina el if 1
    elseif (strcmp($id, 'R-000') === 0) {
      return response()->json([
        'roles'    => 'R-000',
        'permisos' => " "
      ]);
    }
  }



  //borrar permisos
  public function delPermisos(Request $request)
  {

    
    $user = \App\User::where('loguse', $request->nombre)->where('deleted_at', null)->first();
  
    try {
      $temp = [];
      $unique = collect($request->permisosRutas)->unique(); //saca lo repetido

      if ($unique->count() != 0) {
        
        for ($i = 0; $i < $unique->count(); $i++) {
          array_push($temp, explode('-', $request->permisosRutas[$i]));
        }
      }
      
      for ($i = 0; $i < count($temp); $i++) {
        $consulta = \App\permission_user::where('codusuarios', $user->loguse)->where('rutasaccesso', $temp[$i][1])->where('deleted_at', null)->first();
        
        if (!is_null($consulta->codpermission)) {
          #dd("llegfo"); die;
          if (Str::contains($consulta->codpermission, $temp[$i][0] . '-')) {

            $consulta->codpermission = str_replace($temp[$i][0] . '-', '', $consulta->codpermission);
            if (strcmp($consulta->codpermission, '') === 0) {
              $consulta->deleted_at = Carbon::now();
            } else if (strcmp($temp[$i][0] . '-', 'contenidos.index-') === 0) {

              $consulta->deleted_at = Carbon::now();
            }
            $consulta->save();
          }
        }
      }
    } catch (Exception $e) {
      return response()->json([
        'titulo' => Session::flash('warning', 'EL USUARIO ' . $user->nomuse . ' NO TIENE ESTE PERMISOS O ES PROBABLE QUE SE HAYA ELIMINADO')
      ]);
    }

    return response()->json([
      'titulo' => Session::flash('success', 'SE HA ELIMINADO EL ACCESSO CON RESPECTO AL USUARIO ' . $user->nomuse)
    ]);
  }


  public static function modulos($elementos, $user) //aparece el modulo al que tiene el accesso
  {
    #dd($user);

    $menu = new HomeController();
    $paso = $menu->retornarMenu();
    #$per = \App\permission_user::where('codusuarios', $user)->where('codemp',Auth::user()->getFiscal->codemp)->whereNull('deleted_at')->get();
    $per = \App\permission_user::where('codusuarios', $user)->whereNull('deleted_at')->get();
    

    if (is_array($paso['submodulo'])) {

      foreach ($paso['submodulo'][$elementos] as $key1 => $value1) {
        if (is_array($paso['submodulo'][$elementos][$key1])) {
          foreach ($paso['submodulo'][$elementos][$key1] as $key => $value) {
            foreach ($per as $key1 => $value1) {
              if (strcmp("/modulo/" . $value, $value1->rutasaccesso) === 0) {
                return true;
              }
            }
          }
        } else {
          foreach ($per as $key => $value) {
            if (strcmp($value->rutasaccesso, "/modulo/" . $value1) === 0) {
              return true;
            }
          }
        }
      }
    }

    return false;
  }

  public static function submodulos($elementos, $subElementos, $user) //aparece el submodulo del modulo que tiene accesso
  {
    $menu = new HomeController();
    $paso = $menu->retornarMenu();
    $per = \App\permission_user::where('codusuarios', $user)->whereNull('deleted_at')->get();

    if (is_array($paso['submodulo'])) {
      foreach ($per as $key => $value) {
        if (is_array($paso['submodulo'][$elementos][$subElementos])) {
          foreach ($paso['submodulo'][$elementos][$subElementos] as $key1 => $value1) {
            if (strcmp($value->rutasaccesso, "/modulo/" . $value1) === 0) {
              #dd("llego");
              return true;
            }
          }
        } else {
          if (strcmp($value->rutasaccesso, "/modulo/" . $paso['submodulo'][$elementos][$subElementos]) === 0) {

            return true;
          }
        }
      }
    }

    return false;
  }

  public static function hijosSubmodulos($elementos, $subElementos, $element3, $user)
  {
    $menu = new HomeController();
    $paso = $menu->retornarMenu();
    $per = \App\permission_user::where('codusuarios', $user)->whereNull('deleted_at')->get();

    foreach ($per as $key1 => $value1) {
      if (strcmp($value1->rutasaccesso, "/modulo/" . $paso['submodulo'][$elementos][$subElementos][$element3]) === 0) {
        return true;
      }
    }

    return false;
  }
}
