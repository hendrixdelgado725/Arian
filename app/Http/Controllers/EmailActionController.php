<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use App\Almacen;
use App\Caentalm;
use App\Caregart;
use App\Caartalmubimov;
use App\Caartalmubi;
use App\Catraalm;
use App\Tallas;
use App\Sucursal;
use App\Http\Controllers\Entrada_controlador\EntradaController;
use App\Http\Controllers\TraspasoController;



use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Helper;
use Session;
use Exception;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Mail;
use App\Mail\SolicitudTraspasoMail;
use Illuminate\Support\Facades\Route;

class EmailActionController extends Controller
{

    public $accion;
    public $clase;
    public $string;

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function putChoise($codmov,$action){

        $objeto = $this->getObjeto($codmov,$action);
        $codmov = $codmov;
        $articulos = Caartalmubimov::getArtToEmail($objeto->codmov ?? $objeto->codtra);
        $tipo = $objeto->nommov ?? 'TRA';
        $error = $objeto->statra ?? $objeto->status;
        $desc =  Helper::getSpanishDate($objeto->created_at->format('l j n F Y'));
        return view('almacen.respuestaEmail')->with(['movimiento' => $objeto,'articulos' => $articulos,'desc' => $desc,'tipo' => $tipo,'error' => $error]);
    }


    public function aprobarEmail(Request $request,$codmov,$action)
    {
        if(!$request->ajax()) return "error";
        $codmov = $codmov;
        $this->getMov($codmov,$action);
        $resultJson = $this->doAproAction($codmov);
        $result = $resultJson->getData();
        if(isset($result->exito)){
            Session::flash('success', $this->string.' Aprobado Exitosamente');
            return response()->json([
                'url' => self::RedirectTo($this->accion),
                'exito' =>  $result->exito
            ]);
        }else{
            //Session::flash('error',$result->error);
            return response()->json([
                'url' => self::RedirectTo($this->accion),
                'error' => $result->error
            ]);
        }

    }
    
    public function anularEmail(Request $request,$codmov,$action,$desmov)
    {
        if(!$request->ajax()) return "error";
        $this->getMov($codmov,$action);
        $resultJson = $this->doAnularAction($codmov,$desmov);
        $result = $resultJson->getData();
        if(isset($result->exito)){
            Session::flash('success', $this->string.' Anulado Exitosamente');
            return response()->json(['url' => self::RedirectTo($this->accion),
                'exito' =>  $result->exito
            ]);
        }else{
            return response()->json(['url' => self::RedirectTo($this->accion),
                'error' => $result->error
            ]);
        }

    }

    public function getObjeto($codigo,$action){

        return 
        $action === 'ENT' || $action === 'SAL' ?
        Caentalm::with(["almacenes"])->where('codmov',$codigo)->first() : 
        Catraalm::with(["almorides","almdesdes"])->where('codtra',$codigo)->first();
    }

    public function getMov($codmov,$mov){

        if($mov == 'SAL' || $mov == 'ENT'){
            $this->accion = 'ENT';
            $this->clase = new EntradaController;
            $this->string == 'SAL' ? 'Salida' : 'Entrada';
        }elseif($mov === 'TRA'){
            $this->accion = 'TRA';
            $this->clase = new TraspasoController;
            $this->string = 'Traspaso';
        }
    }

    public function doAproAction($codmov){

        $request = request();
        switch ($this->accion) {
            case 'ENT':
                $codmov = Caentalm::where('codmov',$codmov)->first()->codmov;
                return $this->clase->aprobMov($request,$codmov);
            case 'TRA':
                $codmov = Catraalm::where('codtra',$codmov)->first()->codtra;
                return $this->clase->recepcionTras($request,$codmov);
        }
    }

    public function doAnularAction($codmov,$desmov = ''){

        $request = request();
        switch ($this->accion) {
            case 'ENT':
                $codmov = Caentalm::where('codmov',$codmov)->first()->codmov;
                return $this->clase->anularMov($request,$codmov,$desmov);
            case 'TRA':
                $codmov = Catraalm::where('codtra',$codmov)->first()->codtra;
                return $this->clase->anularTras($request,$codmov,$desmov);
        }
    }

    public static function RedirectTo($modulo){
        if($modulo === 'ENT'){
            return route('EntradaYsalida');
        }else{
            return route('traspasosAlmacen');
        }
    }

}
