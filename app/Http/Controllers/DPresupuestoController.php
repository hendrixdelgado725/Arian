<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Http\Requests\DPresupuestoRequest;
use App\Http\Controllers\HomeController;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use App\Fadurpre;
use Illuminate\Support\Facades\Auth;
use Session;
use App\permission_user;
use App\Segususuc;
use Illuminate\Contracts\Auth\Guard;
class DPresupuestoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Guard $guard){

        
        $menu = new HomeController();
        $duraciones = Fadurpre::orderBy('id','DESC')->whereNULL('deleted_at')->paginate(8);
        return view('/configuracionGenerales/duracionPresupuesto')->with('menus',$menu->retornarMenu())
        ->with('duraciones',$duraciones);
    }

    public function nuevoDPresupuesto(){
        $menu = new HomeController();
        return view('/configuracionGenerales/registroDPresupuesto')->with('menus',$menu->retornarMenu());
    }

    public function create(Request $request){

          $request->validate(
            [
            'duracion' => 'required'
          ]);

        //    $pre = '';
           $guion='-';
            $cont = Fadurpre::get()->count();
            if($cont<9){
                $pre = '00';
                $cont = $cont+1;
            }
            elseif($cont>9 && $cont<98){
                $pre = '0';
                $cont = $cont+1;
            }
            else{
                $cont = $cont+1;
            }

            $comprobar=Fadurpre::where('duracion',$request['duracion'])->whereNULL('deleted_at')->first();
            if($comprobar)
            {
                Session::flash('error', ' La duracion '.$request['duracion'].' días esta registrada en el sistema, verificar información.');
                return redirect()->route('nuevoDPresupuesto');
            }
            // $d=date('ds');
            // $idUser = Auth::user()->id;
            // $cod ='FDP'.$pre.$cont.$idUser.$d;
            // $suc = Auth::user()->codsuc_reg;
            $suc=Auth::user()->getCodigoActiveS();
            // dd($activo); 
            $codsuc_reg = explode('-',$suc);
            $cod = $cont.$guion.$codsuc_reg[1];
    
           Fadurpre::create([
            'duracion'=>$request['duracion'],
            'codigoid'=>$cod,
            'codsuc'=>$suc,
           ]);
    
          Session::flash('success', 'La duración de '.$request['duracion'].' días, se registro Exitosamente');
          return redirect('/modulo/ConfiguracionGenerales/DPresupuesto');
    }

    public function filtro(Request $request){
        $menu = new HomeController();
        $duraciones = Fadurpre::where('duracion','ilike','%'.$request->filtro.'%')->paginate(8);
        
        return view('/configuracionGenerales/duracionPresupuesto')->with('menus',$menu->retornarMenu())->with('duraciones',$duraciones)
               ->with('sec',Segususuc::where('loguse',Auth::user()->loguse)->where('codsuc',session('codsuc'))->first()->getSucursal->nomsucu);
    }

    public function delete($codigoid){

        $duracion = Fadurpre::where('codigoid','=',$codigoid)->first();
        $registro = Fadurpre::where('codigoid','=',$codigoid)->with('presupuesto')->has('presupuesto')->get();

        if($registro->isEmpty()){
            $duracion->delete();
            return response()->json([
                'titulo' => 'Exito se elimino'
            ]);
        }
        else{
            return response()->json([
                'titulo' => 'error Esta duracion no puede ser eliminado'
                ]);
            }
    }
}
