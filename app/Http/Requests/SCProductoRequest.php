<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SCProductoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        switch($this->method()){
        case 'POST' : {
            return [
                'categoriaP'=>'required',
                'nombreSC' => 
                [
                    "required"
                    ,Rule::unique('pgsql2.'.session('anio').'.fasubcategoria','nomsubcat')->whereNull('deleted_at')
                ],
                'nomenSC' => 
                [
                    "required"
                    ,Rule::unique('pgsql2.'.session('anio').'.fasubcategoria','nomensubcat')->whereNull('deleted_at')
                ],
        ];
      }
      case 'PUT' : {
        return [
            'categoriaP'=>'required',
            'nombreSC' => 
            [
                "required"
                ,Rule::unique('pgsql2.'.session('anio').'.fasubcategoria','nomsubcat')->ignore($id,'id')->whereNull('deleted_at')
            ],
            'nomenSC' => 
            [
                "required"
                ,Rule::unique('pgsql2.'.session('anio').'.fasubcategoria','nomensubcat')->ignore($id,'id')->whereNull('deleted_at')
            ],
            
        ];
      }
     }//METHOD
    }
}
