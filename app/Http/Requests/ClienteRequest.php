<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $id = $this->route('id');
        switch($this->method()){
        case 'POST' : {
            return [
                'tipodocumento'=>'required',
                'cedularif'=>'required|unique:pgsql2.facliente,codpro|regex:/[0-9-]/',
                // 'cedularif'=>'required',
                'nombre'=>'required|string|min:5',
                'direccion'=>'sometimes|string',
                'telefono'=>'sometimes|nullable|regex:/[0-9-]/|min:10',
                'email'=>'sometimes|nullable|string',
            ];
      }
      case 'PUT' : {
        return [
            'tipodocumento'=>'required',
            'cedularif' => 
            [
                // "required","regex:/[0-9-]/"
                "required","regex:/[0-9-]/",
                Rule::unique('pgsql2.'.session('anio').'.facliente','codpro')->ignore($id,'id')
            ],
            'nombre'=>'required|string|min:5',
            'direccion'=>'sometimes|string',
            'telefono'=>'sometimes|regex:/[0-9-]/|min:10',
            'email'=>'sometimes|nullable|string',
        ];
      }
     }//METHOD
    }
    
}
