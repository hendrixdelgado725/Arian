<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CProductoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // return [
        //     'nombre'=>'string|required|unique:pgsql2.facategoria,nombre',
        //     'nomenclatura'=>'required|unique:pgsql2.facategoria,nomencat',
        // ];

        $id = $this->route('id');
        switch($this->method()){
        case 'POST' : {
            return [
                 'nombre'=>'string|required|unique:pgsql2.facategoria,nombre',
                #'nombre'=>'string|required',
                 'nomenclatura'=>'required|unique:pgsql2.facategoria,nomencat',
                #'nomenclatura'=>'required',

        ];
      }
      case 'PUT' : {
        return [
            'nombre' => 
            [
                "required"
                , Rule::unique('pgsql2.'.session('anio').'.facategoria','nombre')->ignore($id,'id')
            ],
            'nomenclatura' => 
            [
                "required"
                , Rule::unique('pgsql2.'.session('anio').'.facategoria','nomencat')->ignore($id,'id')
            ],
            
        ];
      }
     }//METHOD
    }
}
