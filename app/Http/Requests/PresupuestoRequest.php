<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PresupuestoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'codpro'=>'required',
            'codmon'=>'required',
            'recargo'=>'required',
            'duracion'=>'required',
            'arts' => 'required|array|min:1',
            'arts.*.cantart' => 'required|min:1',
            'arts.*.codart' => 'required|string',
            'arts.*.descto' => 'required'
        
        ];
    }
}
