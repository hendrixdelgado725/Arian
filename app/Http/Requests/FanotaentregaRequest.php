<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FanotaentregaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'codcli' => 'required',
            'forpag' => 'required',
            'refer' => 'required_if:forpag,TRANSFERENCIA,DEPOSITO',
            'banco' => 'required_if:forpag,TRANSFERENCIA,DEPOSITO',
            'totalNota' => 'required',
            'subtotalNota' => 'required',
            'ivaNota' => 'required',
            'descuentoNota' => 'required',
            'arts' => 'required|array|min:1',
            'arts.*.codart' => 'required',
            'arts.*.cantart' => 'required|min:1',
            'arts.*.seriales' => 'required_if:arts.*.isserial,true',
            'arts.*.descto' => 'required',
            'arts.*.monrgo' => 'required',
        ];
    }
}
