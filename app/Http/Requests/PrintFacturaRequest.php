<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class PrintFacturaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reffac'   => 'required|exists:pgsql2.fafactur,reffac',
            'isFiscal' => [
                'required',
                Rule::in(['fiscal','noFiscal'])
            ],
            'caja'     => 'required|integer'
        ];
    }
}
