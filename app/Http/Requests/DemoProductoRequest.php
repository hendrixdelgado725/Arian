<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DemoProductoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // return [
        //     'nombre'=>'required|unique:pgsql2.demoProducto,nombreDemo',
        //     'nomenclatura'=>'required|unique:pgsql2.demoProducto,nomenDemo',
        // ];

        $id = $this->route('id');
        switch($this->method()){
        case 'POST' : {
            return [
                'nombre'=>'string|required',
                // 'nombre'=>'string|required|unique:pgsql2.demoProducto,nombreDemo',
                'nomenclatura'=>'required',
                // 'nomenclatura'=>'required|unique:pgsql2.demoProducto,nomenDemo',

        ];
      }
      case 'PUT' : {
        return [
            'nombre' => 
            [
                "required","string"
                // ,Rule::unique('pgsql2.'.session('anio').'.demoProducto','nombreDemo')->ignore($id,'id')
            ],
            'nomenclatura' => 
            [
                "required"
                // ,Rule::unique('pgsql2.'.session('anio').'.demoProducto','nomenDemo')->ignore($id,'id')
            ],
            
        ];
      }
     }//METHOD
    }
}
