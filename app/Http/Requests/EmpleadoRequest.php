<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EmpleadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $id = $this->route('id');
        switch($this->method()){
        case 'POST' : {
            return [
                'tipodocumento'=>'required',
                'cedularif'=>'required|regex:/[0-9-]/',
                'nombre'=>'required|string|min:5',
                'direccion'=>'required|string',
                'telefono'=>'sometimes|nullable|regex:/[0-9-]/',
                'telefono2'=>'sometimes|nullable|regex:/[0-9-]/',
                'cargo'=>'required',
                'estatus'=>'required',
                'fechaing'=>'required',
                'fechaegre'=>'nullable',
                // 'email'=>'required|string',
            ];
      }
      case 'PUT' : {
        return [
            'tipodocumento'=>'required',
            'cedularif' => 
            [
               "required","regex:/[0-9-]/",
                Rule::unique('pgsql2.'.session('anio').'.nphojint','codemp')->ignore($id,'id')
            ],
            'nombre'=>'required|string|min:5',
            'direccion'=>'required|string',
            'telefono'=>'sometimes|nullable|regex:/[0-9-]/',
            'telefono2'=>'sometimes|nullable|regex:/[0-9-]/',
            'cargo'=>'required',
            'estatus'=>'required',
            'fechaing'=>'required',
            // 'email'=>'required|string',
        ];
      }
     }//METHOD
    }
}
