<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ColorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // return [
        //     'nombreColor'=>'required|unique:pgsql2.colorPro,color',
        //     'nomencolor'=>'required|unique:pgsql2.colorPro,nomencolor',
        // ];

        $id = $this->route('id');
        switch($this->method()){
        case 'POST' : {
            return [
                'nombreColor'=>'required',
                // 'nombreColor'=>'required|unique:pgsql2.colorPro,color',
                'nomencolor'=>'required',
                // 'nomencolor'=>'required|unique:pgsql2.colorPro,nomencolor',

            ];
      }
      case 'PUT' : {
        return [
            'nombreColor' => 
            [
                "required"
                // ,Rule::unique('pgsql2.'.session('anio').'.colorPro','color')->ignore($id,'id')
            ],
            'nomencolor' => 
            [
                "required"
                // ,Rule::unique('pgsql2.'.session('anio').'.colorPro','nomencolor')->ignore($id,'id')
            ],
            
        ];
      }
     }//METHOD
    
    }
}
