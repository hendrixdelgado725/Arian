<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PagoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'despag' => 'required|string|min:10|max:255',
            'tippag' => 'required|string',
            'numord' => 'required|string',
            'facturas' => 'required|array|min:1',
            'facturas.*.fecfac' => 'required|date',
            'facturas.*.fecrecfac' => 'required|date',
            'facturas.*.numfac' => 'required|string|unique:App\Models\Cafacpag,numfac',
            'facturas.*.numctr' => 'required|string|unique:App\Models\Cafacpag,numctr',
            'facturas.*.rifalt' => 'required|string',
            'facturas.*.monret' => 'required|string',
            'facturas.*.totfac' => 'required|string',
            'facturas.*.desfac' => 'required|string',
            'facturas.*.exeiva' => 'required|string',
            'facturas.*.impiva' => 'required|string',
            'facturas.*.basimp' => 'required|string',
            'facturas.*.porret' => 'required|string',
            'facturas.*.desfac' => 'required|string',
            'facturas.*.exeiva' => 'required|string',
            'facturas.*.moniva' => 'required|numeric',
        ];
    }
}
