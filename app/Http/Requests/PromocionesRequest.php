<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PromocionesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'form.desprom' => 'required|string|max:255',
            'form.codigoid' => 'required',
            // //'codprom' => 'required',
            'form.codalm' => 'required',
            'form.nomart' => 'required',
        ];
    }


    public function messages()
    {
        return [
            'form.desprom.required' => 'El campo descripción es obligatorio.',
            'form.codigoid.required' => 'El almacen es obligatorio.',
            'form.codalm.required' => 'La moneda es obligatoria.',
            'form.nomart.required' => 'Nombre del articulo es obligatio.',
        ];
    }
}
