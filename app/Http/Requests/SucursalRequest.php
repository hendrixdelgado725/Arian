<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SucursalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //$string = $this->method();
        $id = $this->route('id');
        //dd($id);
        /* dd($this->method()); */
        switch($this->method()){
            case 'POST' : {
                return [
/*                     'codrif' => 'required|unique:pgsql2.'.session('anio').'.fadefsu,codrif,null,id,deleted_at,NULL|between:10,10', */
                    'nomsucu' => 'required|unique:pgsql2.'.session('anio').'.fadefsu,nomsucu,null,id,deleted_at,NULL',
                    'dirfis' => 'required',
                    'codpai' => 'required',
                    'codedo' => 'required',
                ];
            }
            case 'PUT' : {
                return [
                    'codrif' => 
                    [
                        "required",
                        Rule::unique('pgsql2.'.session('anio').'.fadefsu')->where(function ($query){
                            return $query->where('deleted_at',NULL);
                        })->ignore($id,'codigoid')
                    ],
                    'nomsucu' => 
                    [
                        "required",
                        Rule::unique('pgsql2.'.session('anio').'.fadefsu')->where(function ($query){
                            return $query->where('deleted_at',NULL);
                        })->ignore($id,'codigoid')
                    ],
                    'dirfis' => 'required',
                    'codpai' => 'required',
                    'codedo' => 'required',
                ];
            }
        }

        //valor dd($this->route('id'));
        //dd($this->$id);

    }

    /* public function messages(){
        return [
            'codrif.required' => 'Debes insertar el rif',
            'codrif.unique' => 'El RExiste un rif igual'
        ];
    } */
}
