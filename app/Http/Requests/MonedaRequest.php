<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MonedaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       

        $id = $this->route('id');
        switch($this->method()){
        case 'POST' : {
            return [
                'nombre'=>'required|string',
                // 'nombre'=>'required|string|unique:pgsql2.famoneda,nombre',
                'nomenclatura'=>'required|string',

                // 'nomenclatura'=>'required|string|unique:pgsql2.famoneda,nomenclatura',
            ];
      }
      case 'PUT' : {
        return [
            'nombre' => 
            [
                "required","string"
                // Rule::unique('pgsql2.'.session('anio').'.famoneda','nombre')->ignore($id,'id')
            ],
            'nomenclatura' => 
            [
                "required"
                // Rule::unique('pgsql2.'.session('anio').'.famoneda','nomenclatura')->ignore($id,'id')

            ],
            
        ];
      }
     }//METHOD
    }
}
