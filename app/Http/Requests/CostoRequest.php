<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CostoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $id = $this->route('id');
        switch($this->method()){
        case 'POST' : {
            return [
                'codcen' => ["required", Rule::unique('pgsql2.'.session('anio').'.cadefcen', 'codcen')],
                'descen' => 'required | min: 10 | max: 100',
                'codemp' => 'required | different:cedenc',
                'cedenc' => 'required | different:codemp'
        ];
      }
      case 'PUT' : {
        return [
            'codcen' => ["required", Rule::unique('pgsql2.'.session('anio').'.cadefcen', 'codcen')->ignore($id, 'id')],
            'descen' => 'required | min: 10 | max: 100',
            'codemp' => 'required | different:cedenc',
            'cedenc' => 'required | different:codemp'
        ];
      }
     }//METHOD
    }
}
