<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaEntregaArt extends Model
{
    protected $connection='pgsql2';
    protected $table = 'nota_entrega_art';
    protected $fillable = [
       'id','codart','desart','cantidad','codtalla','preunit','costo','codigoid','codsuc','numero_nota','moneda','coin_name', 
       'descuento_id', 'cambio', 'canrec', 'monto_descuento', 'recibido'
   ];

   public function moneda(){
   	return $this->belongsTo('App\Famoneda','moneda','codigoid');
   }
}
