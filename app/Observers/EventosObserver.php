<?php

namespace App\Observers;

use App\Caregart;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
class EventosObserver
{
    /**
     * Handle the caregart "created" event.
     *
     * @param  \App\Caregart  $caregart
     * @return void
     */
    public function created($caregart)
    {
     #   dd();
        try {
         //code...
     
        $userAgentResolver = Config::get('audit.resolver.user_agent');
        $ip = Config::get('audit.resolver.ip_address');
        $user = Config::get('audit.resolver.user');
        $us = call_user_func([$user,'resolve']);
        #$this->getAuditEvents($event->evento);
        #$us->getMorphClass(),$event->modelo->getMorphClass()
        $url = Config::get('audit.resolver.url');
        #$events = Config::get('audit.events');
        #dd($event->modelo->id);
        $temp = [];
        #$this->updated($event->modelo->codigoid,$event->modelo->toArray(),$event->evento);

     
            $guion='-';
            $cont12 = \App\Audit::withTrashed()->get()->count();
            $cont12 = $cont12+1;
            $suc12 = Auth::user()->codsuc_reg;
            $codsuc_reg1 = explode('-',$suc12);
            $cod2 = $cont12.$guion.$codsuc_reg1[1];

        
        \App\audit::insert([

        'user_type' => $us->getMorphClass(),
        'user_id'   => $us->codigoid,
        'event' => 'created',
        'auditable_id' => $caregart->codigoid,
        'new_values' =>  json_encode($caregart->toArray()),
        'url' => call_user_func([$url,'resolve']),
        'ip_address' => call_user_func([$ip,'resolve']),
        'user_agent' => call_user_func([$userAgentResolver,'resolve']),
        'auditable_type' => $caregart->getMorphClass(),
        'created_at' => Carbon::now(),
        'codsuc_reg' => $suc12,
        'codigoid' => $cod2,
        #'old_values' => 
        ]);
        
        } catch (\Throwable $th) {
            //throw $th;
        }


    }

    /**
     * Handle the caregart "updated" event.
     *
     * @param  \App\Caregart  $caregart
     * @return void
     */
    public function updated($caregart)
    {
        try{

        $userAgentResolver = Config::get('audit.resolver.user_agent');
        $ip = Config::get('audit.resolver.ip_address');
        $user = Config::get('audit.resolver.user');
        $us = call_user_func([$user,'resolve']);
        #$this->getAuditEvents($event->evento);
        #$us->getMorphClass(),$event->modelo->getMorphClass()
        $url = Config::get('audit.resolver.url');
        #$events = Config::get('audit.events');
        #dd($event->modelo->id);
        $temp = [];
        #$this->updated($event->modelo->codigoid,$event->modelo->toArray(),$event->evento);


            $guion='-';
            $cont12 = \App\Audit::withTrashed()->get()->count();
            $cont12 = $cont12+1;
            $suc12 = Auth::user()->codsuc_reg;
            $codsuc_reg1 = explode('-',$suc12);
            $cod2 = $cont12.$guion.$codsuc_reg1[1];


        \App\audit::insert([

        'user_type' => $us->getMorphClass(),
        'user_id'   => $us->codigoid,
        'event' => 'updated',
        'auditable_id' => $caregart->codigoid,
        'url' => call_user_func([$url,'resolve']),
        'ip_address' => call_user_func([$ip,'resolve']),
        'user_agent' => call_user_func([$userAgentResolver,'resolve']),
        'auditable_type' => $caregart->getMorphClass(),
        'created_at' => Carbon::now(),
        'codsuc_reg' => $suc12,
        'codigoid' => $cod2,
        'old_values' => json_encode($caregart->toArray())
        ]);

        }catch(\Throwable $th)
        {

        }
    }

    /**
     * Handle the caregart "deleted" event.
     *
     * @param  \App\Caregart  $caregart
     * @return void
     */
    public function deleted($caregart)
    {
        try{

        #dd("llego");
        $userAgentResolver = Config::get('audit.resolver.user_agent');
        $ip = Config::get('audit.resolver.ip_address');
        $user = Config::get('audit.resolver.user');
        $us = call_user_func([$user,'resolve']);
        #$this->getAuditEvents($event->evento);
        #$us->getMorphClass(),$event->modelo->getMorphClass()
        $url = Config::get('audit.resolver.url');
        #$events = Config::get('audit.events');
        #dd($event->modelo->id);
        $temp = [];
        #$this->updated($event->modelo->codigoid,$event->modelo->toArray(),$event->evento);


            $guion='-';
            $cont12 = \App\Audit::withTrashed()->get()->count();
            $cont12 = $cont12+1;
            $suc12 = Auth::user()->codsuc_reg;
            $codsuc_reg1 = explode('-',$suc12);
            $cod2 = $cont12.$guion.$codsuc_reg1[1];

        
        \App\audit::insert([

        'user_type' => $us->getMorphClass(),
        'user_id'   => $us->codigoid,
        'event' => 'deleted',
        'auditable_id' => $caregart->codigoid,
        'new_values' =>  json_encode($caregart->toArray()),
        'url' => call_user_func([$url,'resolve']),
        'ip_address' => call_user_func([$ip,'resolve']),
        'user_agent' => call_user_func([$userAgentResolver,'resolve']),
        'auditable_type' => $caregart->getMorphClass(),
        'created_at' => Carbon::now(),
        'codsuc_reg' => $suc12,
        'codigoid' => $cod2,
        #'old_values' => 
        ]);

        }catch(\Throwable $th){

        }

    }

    /**
     * Handle the caregart "restored" event.
     *
     * @param  \App\Caregart  $caregart
     * @return void
     */
    /* public function restored(Caregart $caregart)
    {
        //
    } */

    /**
     * Handle the caregart "force deleted" event.
     *
     * @param  \App\Caregart  $caregart
     * @return void
     */
    public function forceDeleted(Caregart $caregart)
    {
        //
    }
}
