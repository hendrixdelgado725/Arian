<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Fafacturpago extends Model implements Auditable
{
     use SoftDeletes,\OwenIt\Auditing\Auditable;

    protected $connection='pgsql2';
    protected $table = 'fafacturpagos';
    protected $fillable = [
        'reffac','fpago','codpago','monto','montocambio','moneda','banco','tipocredito','codsuc','codigoid'
   ];


   public function moneda(){
    return $this->belongsTo('App\Famoneda','moneda','codigoid');
   }

   public function fpago(){
   	return $this->belongsTo('App\Fatippag','codpago','codigoid');
   }
}
