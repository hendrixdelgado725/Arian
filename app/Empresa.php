<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Activitylog\Traits\LogsActivity;


class Empresa extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql2';
    protected $table = "fadefinst";
    protected $fillable = [
        'dirfis',
        'nomrazon',
        'codalm',
        'codrif',
        'tipemp',
        'logo',
    ];

    protected static $logAttributes = ['dirfis','nomrazon','codalm','codrif'];
    protected static $logName = 'fadefinst';
    protected static $logFillable = true;


    public function bancos(){
        return $this->hasMany(Bancos::class,'rifempre','codrif');
    }

}