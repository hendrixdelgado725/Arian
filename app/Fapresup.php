<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;


class Fapresup extends Model implements Auditable
{
    use SoftDeletes,\OwenIt\Auditing\Auditable;
    protected $connection='pgsql2';
    protected $table = 'fapresup';
    protected $fillable = [
        'codpre','codcli','monpre','subtotalpre','mondesc','monrgo',
        'moneda_id','faconpag_id','fafordes_id','fatippag_id',
        'farecarg_id','duracion_id','vencimiento','autor','codsuc','monto_tasa','fatasacamb_id','estatus','codigoid','facturado', 'cedaut',
   ];


    protected static $logAttributes = [
      'codpre','monpre','subtotalpre','mondesc','monrgo','moneda_id','faconpag_id','fafordes_id','fatippag_id','farecarg_id','duracion_id','vencimiento','autor','codsuc','monto_tasa','fatasacamb_id','status'];
    protected static $logName = 'fapresup';
    protected static $logFillable = true;
   

   public function moneda(){
    return $this->belongsTo('App\Famoneda','moneda_id','codigoid');
   }

   public function register(){
    return $this->hasOne('App\User', 'cedemp', 'cedaut');
   }

   public function cliente(){
    return $this->belongsTo('App\Facliente','codcli','codpro');
   }

   public function valido(){
    return $this->belongsTo('App\Fadurpre','duracion_id','codigoid');
   }

   public function articulos(){
     return $this->hasMany('App\Fapreserart','codpre','codpre');
   }

   public function detarticulos(){
    return $this->hasManyThrough('App\Caregart','App\Fapreserart','codpre','codart','codpre','codart');
  }

  public function fpago(){
      return $this->belongsTo('App\Fatippag','fatippag_id','id');
  }

  public function recargo(){
      return $this->belongsTo('App\Farecarg','farecarg_id');
  }

  public function descuento(){
      return $this->belongsTo('App\Fadescto','fadescto_id');
  }

  public function tCambio(){
      return $this->belongsTo('App\Fatasacamb','fatasacamb_id','codigoid');
  }

  public function cambioMoneda(){
    return $this->hasOneThrough('App\Famoneda','App\Fatasacamb','codigoid','codigoid','fatasacamb_id','id_moneda','codigoid');
  }

  public function cambioMoneda2(){
    return $this->hasOneThrough('App\Famoneda','App\Fatasacamb','codigoid','codigoid','fatasacamb_id','id_moneda2','codigoid');
  }

  public function sucursales(){
    return $this->belongsTo('App\Sucursal','codsuc','codsuc');
  }

  // public function tallas(){
  //   return $this->hasOne('App\Tallas','talla','tallas');
  // }

  public function descuentoArt(){
      return $this->hasManyThrough('App\fadescto','App\fapreserart','codpre','codigoid','codpre','descuento_id','codigoid');
  }


//   public function tCambioMoneda(){
//       return $this->hasManyThrough('App\Famoneda','App\Fatasacamb','fatasacamb_id','id_moneda','id');
//   }
}
