<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Tsmovlib extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $timestamps = false;
    protected $connection='pgsql2';
    protected $table = 'tsmovlib';

    protected $fillable = [
        'numcue','reflib','feclib','tipmov','deslib','monmov','codcta','numcom','feccom','status','stacon','fecing','fecanu','tipmovpad','reflibpad','transito','numcomadi','feccomadi','nombensus','orden','horing','stacon1','motanu','refpag','loguse','cedrif','codmon','valmon','codconcepto','stadif','codpro','tipmovaju','reflibaju','coddirec'
   ];

}
