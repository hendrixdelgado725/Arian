<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
class Tallas extends Model implements Auditable
{
    use SoftDeletes,\OwenIt\Auditing\Auditable;


    protected $connection = 'pgsql2';
    protected $table = 'tallas';
    protected $fillable = [
        'tallas', 'codtallas','id','created_at','updated_at','deleted_at','codigoid','codsuc_reg', 'codcategoria'
    ];

    protected static $logAttributes = ['tallas','codtallas','codigoid'];
    protected static $logName = 'tallas';
    protected static $logFillable = true;

    public function Categoria()
   	{
   		return $this->hasOne(Facategoria::class,'codigoid','codcategoria');
   	}

    public function articulos()
    {
        return $this->hasMany(Caarttal::class, 'codtalla','codtallas');
    }
}
