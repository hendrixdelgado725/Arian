<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Spatie\Activitylog\Traits\LogsActivity;
//use OwenIt\Auditing\Contracts\Auditable;

class Logs_reportes extends Model
{
	use SoftDeletes;

    protected $connection='pgsql';
    protected $table = 'logs_reportes';
    protected $fillable = [
      'tipo_reporte',
      'fecha',
      'hora',
      'error',
      'serial_imp',
   ];


}
