<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Catraalm extends Model implements Auditable
{
	use SoftDeletes,\OwenIt\Auditing\Auditable;
    protected $connection = 'pgsql2';

    protected $table = 'catraalm';

    protected $fillable = [
        'codtra',
        'fectra',
        'destra',
        'almori',
        'codubiori',
        'almdes',
        'codubides',
        'id',
        'statra',
        'obstra',
        'codemptra',
        'fadefveh_id',
        'fadefcho_id',
        'fecsal',
        'feclle',
        'fecanu',
        'desanu',
        'usuanu',
        'apro_by_user',
        'fecapro',
        'codigoid',
        'codsuc'
    ];

    public static function GetLastCode(){
        return self::get()->count();
    }

    public static function getList(){
        return self::get()->count();
    }

    public function articulos(){
        return $this->hasManyThrough('App\Caregart','App\Caartalmubimov','codmov','codart','codtra','codart');
    }

    public function tallas(){
        return $this->hasManyThrough('App\Tallas','App\Caartalmubimov','codmov','codtallas','codtra','codtalla');
    }

    public function artmov(){
        return $this->hasMany('App\Caartalmubimov','codmov','codtra');
    }

    public function almorides(){
        return $this->hasOne(Almacen::class,'codalm','almori');
    }
    
    public function almdesdes(){
        return $this->hasOne(Almacen::class,'codalm','almdes');
    }

    public static function getFull(){
        return self::with('almdesdes')->with('almorides')->select('*',
                DB::raw("to_char(fectra,'dd/mm/yyyy') as fecha")
                )->paginate(10);
    }

/*     public function movimientos(){
        return $this->hasOne(Catipent::class,'codtipent','tipmov');
    } */
    /* public static function getFull(){
        return self::with('almacenes')->with('movimientos')->select('*',DB::raw("to_char(fecrcp,'dd/mm/yyyy') as fecrcpnormal"))->paginate(10);
    } */

    public static function findByCod($codmov){
        return self::where('codtra',$codmov)
        ->with(['almorides' => function ($q){
            $q->select(['codalm','nomalm']);
        }])->with(['almdesdes' => function ($q){
            $q->select(['codalm','nomalm']);
        }])->first();
    }
    public static function findByCodDetalle($codmov){
        return self::where('codtra',$codmov)->first();
        }

    public function scopeDate($query,$minDate = null,$maxDate = null){
        return ($minDate != null && $maxDate != null) ? $query->whereDate('created_at','>=',$minDate)->whereDate('created_at','<=',$maxDate) : $query;
    }

    public function scopeAlmacen($query,$codalm = null){
        return ($codalm != null) ? $query->where('codalm',$codalm) : $query;
    }

    public function scopeCodigo($query,$codFrom = null,$codTo = null){
        return ($codFrom != null && $codTo != null) ? $query->where('codtra','>=',$codFrom)->where('codtra','<=',$codTo) : $query;
    }

}
