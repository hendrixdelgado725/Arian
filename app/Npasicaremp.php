<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Npasicaremp extends Model implements Auditable
{
    use SoftDeletes,\OwenIt\Auditing\Auditable;

    protected $fillable = [
        'nomemp','nomcar','codcar','codemp','codigoid','codsuc', 'codcom', 'fecasi'
    ];

    protected $connection='pgsql2';
    protected $table = 'npasicaremp';
    
}
