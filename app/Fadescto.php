<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;


class Fadescto extends Model implements Auditable
{
    
    use SoftDeletes,\OwenIt\Auditing\Auditable;
    protected $connection='pgsql2';
    protected $table = 'fadescto';

    protected static $logAttributes = ['coddesc','desdesc','tipdesc'];
    protected static $logName = 'fadescto';
    protected static $logFillable = true;

    protected $fillable =['coddesc','desdesc','tipdesc','mondesc','diasapl','codcta','tipret','id','comret','porbas','created_at','updated_at','deleted_at','codsuc_reg', 'codsuc','codigoid'];
}
