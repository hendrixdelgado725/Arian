<?php

namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Caja;
use App\User;
use OwenIt\Auditing\Contracts\Auditable;

class cajaUser extends Authenticatable implements Auditable
{
    	use SoftDeletes;
		use \OwenIt\Auditing\Auditable;

    	protected $connection = 'pgsql2';
		protected $table = 'caja_user';
		protected $fillable = ['loguse'];

		public function caja()
		{
			return $this->hasOne(Caja::class,'id','codcaja');
		}

		public function usuarios()
		{
			return $this->hasOne(User::class,'loguse','loguse');
		}
}
