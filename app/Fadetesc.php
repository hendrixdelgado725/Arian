<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fadetesc extends Model implements Auditable
{
    //
    use SoftDeletes,\OwenIt\Auditing\Auditable;
  
    protected $connection='pgsql2';
    protected $table = 'fadetesc';

     protected $fillable =['codesc','codtallas','id','cantidad','codigoid','codsuc_reg','created_at','updated_at','deleted_at','coddefesc'];

    protected static $logAttributes = ['codesc','codtallas','id','cantidad','codigoid','codsuc_reg'];
    protected static $logName = 'fadetesc';
    protected static $logFillable = true;

    public function tallas()
   {
    return $this->belongsTo(Tallas::class,'codtallas');
   }

   public function getDescTalla()
   {
       return Tallas::where([['codtallas',$this->codtallas]])->first();
   }
}
