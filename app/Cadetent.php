<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Cadetent extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;


    protected $connection = 'pgsql2';
    protected $fillable = ['rcpart','codart','codalm','canrec','created_at',
                            'codsuc','codubi','cosart','montot', 'fecven'];
    protected $table = 'cadetent';

}
