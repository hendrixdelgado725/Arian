<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Facategoria extends Model implements Auditable
{
	use SoftDeletes,\OwenIt\Auditing\Auditable;

	protected $connection = 'pgsql2';
	protected $table = 'facategoria';
	protected $fillable = ['nombre','nomencat','codigoid','codsuc','talla'];


	protected static $logAttributes = ['nombre','nomencat','codigoid','codsuc','talla'];
    protected static $logName = 'facategoria';
    protected static $logFillable = true;
	
	public function subCategoria(){
		return $this->hasMany('App\Fasubcategoria','id_facategoria','codigoid');
	}

	public function scopeCategorias($query, $categoria)
	{
		return (!$categoria || count($categoria)=== 0 ) ? $query : $query->whereIn('codigoid',$categoria);
	}

	public function categoryArt()
	{
		return $this->belongsTo(Caregart::class, 'codigoid', 'codcat');
	}

}
