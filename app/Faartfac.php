<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;
use App\Models\Serial;

class Faartfac extends Model implements Auditable
{
    use SoftDeletes,\OwenIt\Auditing\Auditable;

    protected $connection='pgsql2';
    protected $table = 'faartfac';
    protected $fillable = [
        'reffac','codart','precio','monrgo','totart','recargo','cantot','costo','desart','descuento','mondes','talla','codalm','codsuc','codigoid','coddescuento','id',
        'predivisa','costodescuento','descrip_art','porcentajedescuento'
   ];

   public function factura(){
   	return $this->belongsTo('App\Fafactur','reffac','reffac');
   }

   public function tallainfo(){
   	return $this->belongsTo('App\Tallas','talla','codtallas');
   }

   public function sucursal(){
    return $this->belongsTo('App\Sucursal','codsuc','codsuc');
   }

   public function descuento(){
     return $this->belongsTo('App\Fadescto','coddescuento','codigoid');
   }

   public function articulo(){
    return $this->hasOne(Caregart::class,'codart','codart');
  }

  public function scopeDate($query,$minDate = null,$maxDate = null){
    // dd($minDate);
    return ($minDate != null && $maxDate != null) ? $query->whereDate('created_at','>=',$minDate)->whereDate('created_at','<=',$maxDate) : $query;
  }

  public function scopeCategoria($query,$categoria = null){
     return ($categoria) ? 
     $query
     ->with('articulo.facategoria')
     ->whereHas('articulo.facategoria',function($q) use($categoria){
         $q->where('codcat',$categoria);
     }) : $query;
  }

  public function scopeProducto($query,$producto = null){/*  */
     return ($producto) ? $query->where('codart',$producto) : $query;
  }

  public function scopeDescrip($query,$descrip = null)
  {
    return $descrip ? $query->where('codart','like','%'.$descrip.'%') : $query;
  }

  public function scopeMarca($query , $marca = null)
  {
    return $marca ? 
    $query->with('articulo.getMarca')
          ->whereHas('articulo.getMarca',function ($q) use ($marca){
            $q->where('codigoid',$marca);
          }) : $query;
  }
  public function Serial()
  {
     return $this->hasMany(Serial::class,'factura','reffac');
  }
  public function getSerial()
  {
     return $this->hasOne(Serial::class,'codart','codart');
  }
  function articuloRgo(){
    return $this->hasOne('\App\Fargoart','refdoc','reffac');
  }

  function scopeLastAndFirstFactura($query,$codsuc,$desde,$hasta,$cajas,$request,$turno) {
    
    return $query->whereHas('factura',function($q) use($codsuc,$desde,$hasta,$cajas,$request,$turno){
      $q->where('codcaj','=',$cajas->id)
       ->where('is_fiscal',$cajas->is_fiscal)
       ->where('fecfac','>=',$desde)
       ->where('fecfac','<=',$hasta)
       
       ->when(isset($request->all()['turno']) === true,function ($q) use ($request,$turno){
         switch ($turno) {
           case 'vespertino':
             $q->where('turnos',$turno);
             break;
           case 'matutino':
             $q->where('turnos',$turno);
             break;
   
             default:
             $q->where('turnos','ilike','%%');
             break;
         }
       }); 
    })->where('codsuc','=',$codsuc)->get();
  }
}
