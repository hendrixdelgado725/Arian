<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;
class permission extends Model implements Auditable
{

	use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    protected $connection = 'pgsql';
    protected $table = 'permissions'; 
    protected static $logAttributes = ['slug','description','name'];
    protected static $logName = 'permissions';
    protected static $logFillable = true;
}
