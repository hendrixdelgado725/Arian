<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Fasuc_rgo extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

        protected $connection='pgsql2';
        protected $table = 'fasuc_rgo';
    
        protected $fillable =['codsuc','codrgo','codigoid'];
        protected static $logAttributes = ['codsuc','codrgo','codigoid'];
        protected static $logName = 'fasuc_rgo';
        protected static $logFillable = true;

        public static function getrecargo($codsuc){
        return self::where('codsuc',$codsuc)->get();
        }

        public static function getbycodId($codigoid){
        return self::where('codigoid',$codigoid)->get();
        }

        public static function getcount(){
            return self::withTrashed()->get()->count();
        }

        public function recargo(){
            return $this->belongsTo('App\Farecarg','codrgo','codrgo');
        }
    
}
