<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contabc1 extends Model
{

    protected $connection='pgsql2';
    protected $table = 'contabc1';

    protected $fillable = [
        'numcom','feccom','debcre','codcta','refasi','desasi','monasi','monasimone'
   ];

}
