<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;
class Faforpag extends Model implements Auditable
{
    use SoftDeletes,\OwenIt\Auditing\Auditable;
    
    protected $connection='pgsql2';
    protected $table = 'faforpag';

    protected $fillable =['reffac','tippag','nropag','nomban','numero','id','codmov','codcaj','codtippago','codigoid','codsuc','codpago','moneda','refbillete','tasacod'];

    protected static $logAttributes = ['tippag','nropag','diralm'];
    protected static $logName = 'faforpag';
    protected static $logFillable = true;

    public function monedas(){
    	return $this->belongsTo('App\Famoneda','moneda','codigoid');
    }

    public function tipomoneda(){
        return $this->hasOne(Famoneda::class,'codigoid','moneda');
    }

    public function tasamoneda(){
        return $this->hasOne(Fatasacamb::class,'codigoid','tasacod');
    }

    public function tipopago(){
        return $this->hasOne('App\Fatippag','id','tippag');
    }

    public function getFactura()
    {
        return $this->hasOne('App\Fafactur','reffac','reffac');
    }



}
