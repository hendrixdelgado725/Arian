<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Audit extends Model
{
    use SoftDeletes;
    protected $connection = 'pgsql';
    protected $table = 'audits';
    protected $fillable = ['old_values','new_values'];
    
    public function user()
    {
        return $this->hasOne(User::class,'codigoid','user_id');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y H:i');
    }

    public function getEventAttribute($value)
    {
        return strtoupper($value);
    }

}
