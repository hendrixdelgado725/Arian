<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Fafactuart extends Model implements Auditable
{
    use SoftDeletes,\OwenIt\Auditing\Auditable;

    protected $connection='pgsql2';
    protected $table = 'fafactuart';
    protected $fillable = [
        'reffac','codart','descripcion','cantidad','descuento','descontado','preunit','costo','monrecargo','recargo','talla','codalm','codsuc','codigoid','coddescuento'
   ];

   public function factura(){
   	return $this->belongsTo('App\Fafactur','reffac','reffac');
   }

   public function tallainfo(){
   	return $this->belongsTo('App\Tallas','talla','codtallas');
   }

   public function sucursal(){
    return $this->belongsTo('App\Sucursal','codsuc','codsuc');
   }

   public function descuento(){
     return $this->belongs('App\Fadescto','coddescuento','codigoid');
   }
}
