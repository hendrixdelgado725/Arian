<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Fapreserart extends Model implements Auditable
{
    use SoftDeletes,\OwenIt\Auditing\Auditable;
    protected $connection='pgsql2';
    protected $table = 'fapreserart';
    protected $fillable = [
        'refpre', 'codpre','codart','desart','cantart','preunit','codigoid','codsuc',
        'monto_descuento','descuento_id','descuento','talla','codtalla', 'totart'
   ];

   public function demografia(){
       return $this->hasOneThrough('App\DemoProducto','App\Caregart','codart','codigoid','codart','codemo');
   }

   public function categoria(){
    return $this->hasOneThrough('App\Facategoria','App\Caregart','codart','codigoid','codart','codcat');
   }

   public function subcategoria(){
    return $this->hasOneThrough('App\Fasubcategoria','App\Caregart','codart','codigoid','codart','codsubcat');
   }

   public function discount(){
    return $this->hasOne('App\Fadescto','codigoid','descuento_id');
   }

   public function articulo(){
    return $this->hasOne('App\Caregart','codart','codart');
   }
}
