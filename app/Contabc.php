<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Contabc extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $connection='pgsql2';
    protected $table = 'contabc';

    protected $fillable = [
        'numcom','feccom','descom','stacom','reftra','totdeb','totcre','totdif','mensaje','grid','btnactualizar','moncom' ,
   ];
}