@extends('layouts.app2')

@section('content')

<body style="min-height: 496.8px;background-image: url('{{ asset('/images/fasvit2.jpg')}}');">

<div class="login-box">

<div class="card" style="
width: 20rem; 
margin-left: auto; 
margin-right: auto;
background-color: rgba(0, 0, 0, 0.2);
backdrop-filter: blur(6px);
">

    <div class="card-header text-white" style="text-align: center;">
        <h2>FASVIT</h2>
    </div>

    <div class="card-body" style="justify-content: center;" >

    <div class="col" style="display: grid; margin: auto;">
        <a href="{{ route('fasvit.registro') }}" class="btn btn-info">
            <i class="fa fa-user-plus" aria-hidden="true"></i>
            <b>REGISTRAR FAMILIAR</b></a>
    </div>
    
    <div class="col pt-4" style="display: grid; margin: auto;">
        <a href="{{ route('fasvit.tratamiento') }}" class="btn btn-info">
            <i class="fa fa-medkit" aria-hidden="true"></i> 
            <b>REGISTRAR TRATAMIENTO</b></a>
    </div>

    </div>

    <div class="card-footer" style="display: grid; margin: auto;">
        <a href="{{ URL::previous() }}" class="btn btn-secondary">
            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
            <b>Volver</b></a>
    </div>

</div>

</div>

@endsection