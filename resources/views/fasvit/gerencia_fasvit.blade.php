@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Listado de Entradas Por Almacen')


@section('content')

@component('layouts.contenth')
    @slot('titulo')
      Gerencia de tratamientos
    @endslot
  @endcomponent

  <gerencia 
	:gerencias="{{ json_encode($gerencias) ?? json_encode('{}') }}"
	:especs="{{ json_encode($especs) ?? json_encode('{}') }}"
  ></gerencia>

  @endsection
@section('lista')