@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Patologias')


@section('content')

<section class="content pt-5">

    <form-patologia type="{{ $type }}"
    :patologias="{{ json_encode($patologias) ?? json_encode('{}') }}">  

    </form-patologia>

</section>


@endsection

