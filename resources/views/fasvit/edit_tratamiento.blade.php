@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Listado de Entradas Por Almacen')


@section('content')
  @component('layouts.contenth')
    @slot('titulo')
      Solicitud del trabajador
    @endslot
  @endcomponent
<form-tratamientos
type="{{ $type }}"
:info="{{ json_encode($info) ?? json_encode('{}') }}"
:info_empleado="{{ json_encode($empleado) ?? json_encode('{}') }}"
:infomedicamentos="{{ json_encode($infomedicamentos) ?? json_encode('{}') }}"
:infobeneficiario="{{ json_encode($infobeneficiario) ?? json_encode('{}') }}"
>
</form-tratamientos>
@endsection
