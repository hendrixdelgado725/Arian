@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Listado de Entradas Por Almacen')


@section('content')

@component('layouts.contenth')
    @slot('titulo')
      Listado de Solicitudes del trabajador
    @endslot
  @endcomponent
    <section class="content">
      <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
              @include('vendor/flash.flash_message')
                <div class="card-header">
                  <h3 class="card-title"><b></b></h3>
                  <div class="card-tools">
                    <div class="row">
                      
                        <a href="{{ route('fasvit.registro') }}" class="btn btn-info"><b>REGISTRAR SOLICITUD</b></a>                               
                      <div class="col">
                      {{-- <form action="{{route('filtroEntradas')}}" method="post">
                        <div class="input-group">
                          @csrf
                          <input type="text" placeholder="Buscar por nombre de Almacen" name="filtro"  class="form-control float-right">
                          <button type="submit" style="height: 38px; width: 38px" class="btn btn-default btn-sm float-left"><i class="fas fa-search"></i></button>
                          <a href="{{route('ListadoEntrada')}}" class="btn btn-default"><i class="fas fa-arrow-left mt-1"></i></a>                       
                        </div>
                      </form> --}}
                      </div>
                    </div><!--row -->
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive text-center">
                  <table class="table table-hover">
                    <thead>
                      <tr style="text-align:center">
                        <th>Cedula</th>
                        <th>Nombre Trabajador</th>
                        <th>Fecha Solicitud</th>
                        <th>Status</th>
                        <th>Acción</th>
                      </tr>
                    </thead>
                    <tbody style="text-align:center">
                      @foreach($solicitudes as $key => $value)
                        <tr>
                          <td>{{ $value->codemp }}</td>
                          <td>{{ $value->trabajador->nomemp }}</td>
                          <td>{{ $value->created_at->format('d/m/y') }}</td>
                          <td>
                            @if($value->estatus == 'E')
                              Espera
                            @elseif($value->estatus == 'A')
                              Aprobado
                            @elseif($value->estatus == 'R')
                              Rechazado
                            @endif
                          </td>
                          <td>
                            <div class="row">
                              <form action="{{ route('fasvit.pdfFamilia') }}" method="POST">
                                @csrf
                                <input type="hidden" name="codigo" value="{{ $value->codigo }}">
                                <button type="submit" class="btn btn-danger">
                                  <i class="fas fa-file-pdf"></i>
                                </button>
                              </form>
                              <a href="{{ route('fasvit.edit', $value->codigo) }}" class="btn btn-warning" style="
                                height: 1.9rem;
                            ">
                                <i class="fas fa-edit"></i>
                              </a>
                            </div>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                  {!!$solicitudes->appends(request()->input())->render()!!}
                  {{-- col8  --}}
            </div>
            {{-- <div>
              <a href="{{ url()->previous() }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
            </div> --}}

              
            </div>
          </div>
      </div>
        {{-- row --}}
    </div>
    {{-- container fluid --}}

    {{-- <form id="sendPdf" method="POST" action="{{route('generarReporteEntrada')}}" style="display:none">
      @csrf
        <input id="codfrom" type="text" name="codfrom">
        <input id="codto" type="text" name="codto">
        <input id="diames" type="text" name="diames">
        <input id="tipmov" type="text" name="tipmov">
    </form> --}}
  </section>


@endsection
@section('lista')
  
<script>
  
  $(function () {
  /*   submitPdf()

    function submitPdf() {
      $(document).on('click','.sendPdf',function (e) {
        e.preventDefault();
        let valor = $(this).closest('tr').data('id')
          $('input#codfrom').val(valor)
          $('input#codto').val(valor)
          $('input#diames').val('byCod')
          $('input#tipmov').val($(this).closest('tr').data('type'))
          $('#sendPdf').submit()
      })
    } */
  })

</script>
@endsection
