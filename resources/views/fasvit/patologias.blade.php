@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Patologias')


@section('content')

<section class="content p-5">
    <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
          <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>
                <div class="card-tools">
                  <div class="row pr-3">
                    
                      <a href="{{ route('fasvit.registro.patologia') }}" class="btn btn-info"><b>REGISTRAR PATOLOGIA</b></a>                               

                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>ID</th>
                      <th>Especialidad</th>
                      <th>Nombre</th>
                      <th>Monto</th>
                      <th>Acción</th>
                    </tr>
                  </thead>
                  <tbody style="text-align:center">

                    @foreach ($patologias as $patologia)
                        <tr>
                            <td>{{ $patologia->id }}</td>
                            <td>{{ $patologia->especialidad->nombreesp }}</td>
                            <td>{{ $patologia->nombre }}</td>
                            <td>{{ $patologia->monto }}</td>
                            <td>
                              <form action="{{ route('fasvit.patologia.edit', $patologia->id) }}">
                                @csrf
                                <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i></button>
                              </form>
                              
                              <form action="{{ route('fasvit.patologia.destroy', $patologia->id) }}" method="POST">
                                @csrf
                                @method('DELETE') 
                                <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                              </form>
                         
                            </td>
                        </tr>
                    @endforeach
                      
                  </tbody>
                </table>

               
             </div>
            
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>

</section>


@endsection

