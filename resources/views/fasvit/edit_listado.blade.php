@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Listado de Entradas Por Almacen')


@section('content')
  @component('layouts.contenth')
    @slot('titulo')
      Listado de Solicitudes del trabajador
    @endslot
  @endcomponent
  <form-familiares
    tipos_parentesco = "{{ $tipos_parentesco->toJson() ?? '[]' }}"
    :info="{{ json_encode($info) ?? json_encode('{}') }}"
    :info_empleado="{{ json_encode($empleado) ?? json_encode('{}') }}"
    :familiares_siga="{{ json_encode($familiares_siga) ?? json_encode('[]') }}"
    type="{{ $type }}"
  >  
  </form-familiares>
@endsection
