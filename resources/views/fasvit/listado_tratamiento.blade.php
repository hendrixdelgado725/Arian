@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Listado de Entradas Por Almacen')


@section('content')

@component('layouts.contenth')
    @slot('titulo')
      Listado de Tratamientos solicitados
    @endslot
  @endcomponent
    <section class="content">
      <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
              @include('vendor/flash.flash_message')
                <div class="card-header">
                  <h3 class="card-title"><b></b></h3>
                  <div class="card-tools">
                    <div class="row">

                      <div class="col">
                        <a href="{{ route('fasvit.tratamiento') }}" class="btn btn-info">
                      <b>REGISTRAR TRATAMIENTO</b></a>
                      </div>

                    </div><!--row -->
                  </div>
                </div>
                <!-- /.card-header -->

                  <div class="card-body table-responsive text-center">
                  <table class="table table-hover">
                    <thead>
                      <tr style="text-align:center">
                        <th>Cédula</th>
                        <th>Nombre Trabajador</th>
                        <th>Fecha Solicitud</th>
                        <th>Status</th>
                        <th>Acción</th>
                      </tr>
                    </thead>
                    <tbody style="text-align:center">
                      @foreach($solicitudes as $key => $value)
                        <tr>
                          <td>{{ $value->codemp }}</td>
                          <td>{{ $value->titular->nomemp }}</td>
                          <td>{{ $value->created_at->format('d/m/y') }}</td>
                          <td>
                            @if($value->status == 'E')
                              Espera
                            @elseif($value->status == 'A')
                              Aprobado
                            @elseif($value->status == 'R')
                              Rechazado
                            @endif
                          </td>
                          <td>
                            <div class="row">
                              <form action="{{ route('fasvit.tratamientopdf', $value->codigo) }}"> <!--  route('fasvit.tratamientopdf, $value->id') -->
                                @csrf
                                <input type="hidden" value="{{ $value->codigo }}">
                                <button type="submit" class="btn btn-danger" {{ $value->status != 'A' ? 'disabled' : '' }} >
                                  <i class="fas fa-file-pdf"></i>
                                </button>
                                  
                              </form>
                              <a href="{{ route('fasvit.tratamiemtoedit', $value->codigo) }}" class="btn btn-warning" style="
                                height: 1.9rem;
                            ">
                                <i class="fas fa-edit"></i>
                              </a>
                            </div>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                  {!!$solicitudes->appends(request()->input())->render()!!}
                  {{-- col8  --}}
            </div>

            </div>
          </div>
      </div>
        {{-- row --}}
    </section>

@endsection
@section('lista')