<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
	<link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ionicons2.0.1.min.css') }}">
    <link rel="stylesheet" href="{{ asset('cssadminlte/adminlte.min.css') }}">
     <link href="{{ public_path('css/app.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
        
        <table class="table-bordered center mb-3" width="835">
          
          <thead>

            <tr>
              <th rowspan="2">  
                    <img src="/images/Logo-vit.png" height="100px" width="150px" style="background-size: cover;"> 
              </th>

              <th colspan="3">
                <h6 class="p-3" style="text-align: center;"> VENEZOLANA DE INDUSTRIA TECNOLÓGICA, C.A  </h6>
              </th>

               <th rowspan="2">
                <p>Código: {{ $info->codigo }}</p>
                <p>Año: {{ $info->created_at->format('Y') }}</p>

              </th>

              

              </tr>

              <tr>

              <th>
                <h6 class="p-3" style="text-align: center;"> FORMULARIO PARA REGISTRO DE PACIENTES CON TRATAMIENTOS CRÓNICOS </h6>
              </th>

            </tr>
          </thead>

        </table>

        <table class="table-bordered center" width="835">

        	<thead>
        		<tr>
        			<th class="table-active" colspan="5" style="text-align: center;"> 
        				DATOS DEL TRABAJADOR Y BENEFICIARIO
        			</th>
        		</tr>
        	</thead>
        		
        	<tbody>
        		<tr>
        			<td colspan="3"><b>Fecha de registro: </b>	
        				{{ $info->created_at->format('d/m/y') }}
        			</td>
        		

        			<td colspan="2"> <b>Ciudad:  </b></td>
        		</tr>

        		<tr>
        			<td colspan="3"><b>Nombre y apellido del titular:</b>
        				{{ $empleado->nomemp }}
        			</td>

        			<td colspan="2"><b> Cédula de identidad: </b>
        				{{ $empleado->cedemp }}
        			</td>
        		</tr>

        		<tr>
        			<td colspan="3"> <b> Teléfono celular: </b>{{ $empleado->celemp }}
        			</td>

        			<td colspan="2"> <b> Teléfono de habitación: </b> {{ $empleado->telhab }}
        			</td>
        		</tr>

        		<tr>
        			<td colspan="5"> <b>Correo electrónico: </b> {{ $empleado->emaemp }}
        			</td>
        		</tr>

        		<tr>
        			
        			<td colspan="3">
        				<b>Nombres y Apellidos del paciente: </b>{{ $infobeneficiario->nomfam }}
        			</td>

        			<td>
        				<b>Cédula de Identidad: </b>{{ $infobeneficiario->cedfam }}
        			</td>

        			<td>
        				<b>Parentesco: </b>
        				<p>{{ $parentesco->despar }}</p>
        			</td>

        		</tr>

        		<tr>
        			<th class="table-active" colspan="5" style="text-align: center;">
        				DIAGNOSTICO DEL PACIENTE
        			</th>
        		</tr>

        		<tr >
        			<td colspan="5"><b>Descripción: </b> 
        				{{ $info->diagnostic->diagnostico }}
        			</td>
        		</tr>

        		<tr>
        			<th class="table-active" colspan="5" style="text-align: center;">
        				DATOS DEL RÉCIPE E INDICACIÓN MÉDICA
        			</th>
        		</tr>

        	</tbody>

        </table>

        <table class="table-bordered center" width="835" style="text-align: center;">

        	<thead >
        		<th>Medicamento</th>
        		<th>Dosis</th>
        		<th>Frecuencia</th>
        		<th>Duración</th>
        		<th>Cantidad</th>
        	</thead>

        	@foreach ($infomedicamentos as $medicamento)

        	<tbody>
        		<td>{{ $medicamento->nombre }}</td>
        		<td>{{ $medicamento->dosis }}</td>
        		<td>{{ $medicamento->frecuencia }}</td>
        		<td>{{ $medicamento->duracion }}</td>
        		<td>{{ $medicamento->cantidad }}</td>

        		@endforeach


        	</tbody>
        	
        </table>

        <table class="table-bordered center" width="835" style="text-align: center;" >
 
     		<tbody>


		    <tr>
		      <th class="table-active" colspan="4">VIGENCIAS</th>
		    </tr>

		    <tr>
		      <td colspan="2"><b>Informe médico</b></td>
		      <td colspan="2"><b>Récipe médico</b></td>
		    </tr>

   			 <tr>

   			 	<td><b>Fecha desde</b></td>
   			 	<td><b>Fecha hasta</b></td>
   			 	<td><b>Fecha desde</b></td>
   			 	<td><b>Fecha hasta</b></td>
    	
    		</tr>

    		<tr>
    			<td>{{ $info->diagnostic->fecdsdinf }}</td>
    			<td>{{ $info->diagnostic->fechstinf }}</td>
    			<td>{{ $info->diagnostic->fecdsdrec }}</td>
    			<td>{{ $info->diagnostic->fechstrec }}</td>
    		</tr>

    		<tr>
    			<th class="table-active" colspan="4">OBSERVACIONES</th>
			</tr>

			<tr>
				<td colspan="4" rowspan="3">
					<p>{{ $info->diagnostic->observaciones }}</p>
				</td>
			</tr>

 			</tbody>

       	</table>

	
</body>
</html>


