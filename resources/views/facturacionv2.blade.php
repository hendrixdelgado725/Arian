<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link href="/css/select2.min.css" rel="stylesheet">
  <title>Facturacion</title>
  <link rel="icon" href="{!! asset('images/arian/principal/ARIAN_negro_negro.png') !!}"/>
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('cssadminlte/adminlte.min.css') }}">
  <!-- Google Font: Source Sans Pro -->
  {{-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> --}}
  <style>
      .cargando {
      width: 100%;height: 100%;
      overflow: hidden; 
      top: 0px;
      left: 0px;
      z-index: 10000;
      text-align: center;
      position:absolute; 
      background-color: /*#FFFFFF*/ #FFF;
      opacity:0.9;
      filter:alpha(opacity=40);
    
   }
   .image{
    margin-top: 500px
   }
   .modal-heigth{
    max-height: 100% !important;
   }
  </style>
</head>
<body   class="hold-transition sidebar-collapse layout-top-nav" data-url2="{{ route('closeBox',encrypt(Auth::user()->loguse)) }}" data-url="{{ route('updatedWindows',encrypt(Auth::user()->loguse)) }}">
  <div class="cargando" style="display:none">
    <img class="image" width="50" height="50" src="{{asset('/gif/Gear-0.2s-800px.gif')}}">
  </div>
<div class="wrapper">

{{-- @extends('layouts.app') --}}
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @csrf
    <!-- Content Header (Page header) -->
    <div class="content-header" style="background-color: black ; padding-bottom : 0px">
      <div class="row">
        <div class="col-2">
          <div class="login-logo ml-4" style="text-align: left">
             <img src="{{ asset('/images/arian/inicio/arian-logo.png') }}" alt="Arian"  height="40px" >
          </div>
        </div>
        <div class="col-4">
          <div class="opscajas" style="text-align: left; display:inline">
            <div class="col-sm-12">
              <h4 class="m-0 text-white cajas"><b>CAJA CERRADA</b>
                <i class="fa fa-lock" id="abrirCaja"></i>
                <i class="fa fa-lock-open" style="display: none" id="cerrarCaja"></i>
              </h4>
              <h4 class="m-0 text-white">
                <b>
                  <span style="color: blue">VENDEDOR</span> :  {{ Auth::user()->nomuse}}
                </b>
              </h4>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div>
        <div class="col-4">
          <a type="button" id="home" class="btn btn-warning mt-3" ><b>REGRESAR A MENU PRINCIPAL </b><i class="fas fa-home"></i> 
              </a>
        </div>
        <div>
          <h4 id="tasaC" style="color: #eaf4fff7">
            Tasa de Cambio : {{ $tasa->valor }} Bs
          </h4>
          @if($caja2->is_fiscal === "false")
          <h4 style="color: #eaf4fff7">
            Turno  : {{ $caja2->turno->tipoturno ?? '' }}
          </h4>
          @endif
        </div>
      </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" style="min-height: 496.8px;background-image: url('{{ asset('/images/arian/inicio/arian-fondo.png')}}');">
      
      <div id="app">
        

          <div class="row justify-content-center responsive p-0">
            <div class="col-lg-4">
              <div class="card card-primary card-outline" id="card-cliente">
               
                <img src="{{ asset('/images/arian/facturacion/boton_datos_cliente.png') }}" height="50px" style="display: block;margin: 0 auto;width: 100%;">
                <form id="formPersona">
                <div class="card-body disabledbutton" id="cardCliente">
                  <div class="form-group row">
                      <label for="tipodocumento" class="col-4 col-form-label">TIPO DE DDOCUMENTO</label> 
                      <div class="col-8">
                      <select class="form-control" style="width: 100%;" tabindex="-1" aria-hidden="true" id="tipodocumento" name="tipodocumento" class="@error('tipodocumento') is-invalid @enderror">
                        <option  value="V">V</option>
                        <option  value="E">E</option>
                        <option  value="J">J</option>
                        <option  value="G">G</option>
                        <option  value="P">P</option>
                        <option  value="R">R</option>
                        <option  value="C">C</option>
                    </select>
                      </div>
                      </div>
                     
                    <div class="form-group row">
                      <label for="documento" class="col-4 col-form-label">N° DOCUMENTO</label> 
                      <div class="col-8">
                        <input id="documento" onKeypress = "javascript:return NumerosReales(event)" onblur="persona2()" id="documento" name="documento" type="text" class="form-control">
                      </div>
                      </div>

                      <div class="form-group row">
                      <label for="nombre" class="col-4 col-form-label">NOMBRE</label> 
                      <div class="col-8">
                        <input id="nombre" name="nombre" onkeyup="pasarMayusculas(this.value, this.id)"  type="text" class="form-control">
                      </div>
                      </div>

                   <!--    <div class="form-group row">
                       <label for="direccion" class="col-4 col-form-label">Dirección Fiscal</label>
                        <div class="col-8">
                         <textarea type="text" onkeyup="pasarMayusculas(this.value, this.id)" class="form-control" class="@error('direccion') is-invalid @enderror" placeholder="Avenida..." name="direccion" id="direccion"> </textarea>
                        </div>
                        @error('direccion')
                       <div class="alert alert-danger">{!!$message!!}</div>
                        @enderror
                     </div> -->
                    
                    <div class="row">
                    <div class="col-5">
                        <select name="clientes" id="clientes" class="form-control select2" data-dropdown-css-class="select2-danger" data-href="{{route('getCliente')}}" class="@error('clientes') is-invalid @enderror" aria-hidden="true" style="text-align: center;">
                            <option disabled selected>BUSCAR CLIENTE</option>
                              @foreach($clientes as $cliente)
                               <option value="{{$cliente->codpro}}">{{$cliente->codpro .' / '. $cliente->nompro}}</option>
                              @endforeach
                              </select>
                      <!--<button class="btn btn-success"><b>F-1 SELECCIONAR CLIENTE</b></button>-->
                    </div>
                    <div class="col-5">
                      <button type="button" class="button border1 border11 ml-4 nuevoC" data-toggle="modal" data-target="#nuevocliente"><b>NUEVO CLIENTE</b></button>
                    </div>
                    </div>
               
                  </div>
                </form>
              </div>

           <div class="col-12">
              <div class="card card-primary card-outline">
                <!-- <div class="card-header" style="background: linear-gradient(90deg, blue, purple);">
                  <h5 class="card-title m-0" style="color: white"><i class="fas fa-edit"></i><b> OPCIONES</b></h5>
                </div> -->
                <img src="{{ asset('/images/arian/facturacion/boton_opciones.png') }}" height="50px" alt="Opciones de Caja" style="display: block;margin: 0 auto;width: 100%;">
                <div class="card-body disabledbutton" id="opcionesF">
                  
            <div class="row">

              <button type="button" id="descuentob" class="btn mb-1 bg-gradient-primary boton-caja">
                 <div class="inner">
                  <h6 class="h-botton" ><b>F1</b></h6>  
                </div>
                <b>DESCUENTO</b>
              </button>

              <a class="m-1" id="facturar" data-href="{{route('facturar')}}">
                    <img src="{{ asset('/images/arian/facturacion/boton_facturar.png') }}" height="90px" style="display: block;margin: 0 auto;width: 100%;">
                  </a>
         
              <button type="button" id="presupuesto" class="btn mb-1 bg-gradient-secondary boton-caja">
                 <div class="inner">
                  <h6 class="h-botton" ><b>F4</b></h6>  
                </div>
                <b>PRESUPUESTO</b>
              </button>

              <button type="button" id="nota" class="btn mr-1 bg-gradient-default boton-caja">
                <b>NOTA DE ENTREGA</b>
              </button>
              @if($caja2->is_fiscal === "true")
              <button type="button" id="devolucion" class="btn mr-1 bg-gradient-warning boton-caja">
                <div class="inner">
                  <h6 class="h-botton" ><b>F8</b></h6>  
                </div>
                <b>DEVOLUCIÓN</b>
              </button>
              @endif

              @if($caja2->is_fiscal === "false")
              <button type="button" id="cierreTT"  data-turnos="{{$caja2->turno->caja}}" class="btn mr-1 bg-gradient-warning boton-caja">
                <div class="inner">
                  <h6 class="h-botton" ><b>F8</b></h6>  
                </div>
                <b>CIERRE TURNOS</b>
              </button>
              @endif

              <button type="button" id="limpiar" class="btn bg-gradient-danger btn-lg boton-caja">
                <div class="inner">
                  <h6 class="h-botton" ><b>F9</b></h6>  
                </div>
                <b>LIMPIAR</b>
              </button>
              
              {{--<button type="button" id="limpiar" class="btn bg-gradient-danger btn-lg boton-caja">
                <div class="inner">
                <a href="#" data-r="X" id="reporte"  ><b>Enviar Reporte X</b></a>
                </div>
              </button>--}}
              
              
              @if($caja2->is_fiscal === "true")
              <a  data-r="X" id="reporte" data-url="{{route('cajaReporte',array($caja,'X'))}}"  type="button" class="btn bg-gradient-dark btn-lg boton-caja reporteX" href='{{route('cajaReporte',array($caja,'X'))}}'>
                <div class="inner">
                    <h6 class="h-botton" style="font-size: 18px;"><b>SUPR</b></h6>
                </div>
                <b>Reporte X</b>
              </a>
              @endif


              @if($caja2->is_fiscal === "true")
              <a  data-r="Z" id="reporte" data-url="{{route('cajaReporte',array($caja,'Z'))}}" type="button" class="btn bg-gradient-info btn-lg boton-caja reporteZ" href='{{route('cajaReporte',array($caja,'Z'))}}'>
                <div class="inner">
                    <h6 style="font-size: 18px;" class="h-botton" ><b>ESC</b></h6>
                </div>
                <b>Reporte Z</b>
              </a>
              @endif

              <a  id="arqueoCaja" type="button" class="btn bg-gradient-default btn-lg boton-caja">
                <div class="inner">
                    <h6 style="font-size: 18px;" class="h-botton" ><b>INS</b></h6>
                </div>
                <b>Arqueo de Caja</b>
              </a>

              <form-api></form-api>

                {{--route('cajaReporte',array($caja->id,'X'))--}}
                {{--route('cajaReporte',array($caja->id,'XP'))--}}
                {{--route('cajaReporte',array($caja->id,'Z'))--}}
            </div>

          </div> <!--row-->
        </div>
        <div  class="card card-primary card-outline">
                <!-- <div class="card-header" style="background: linear-gradient(90deg, blue, purple);">
                  <h5 class="card-title m-0" style="color: white"><i class="fas fa-edit"></i><b> OPCIONES</b></h5>
                </div> -->
                <div class="card-body disabledbutton" >
                  
            <div class="row">
              <h4 class="m-0 text-white">
                <b id="updte">
                  <span  style="color: blue">Proxima Factura </span> : <label style="color:#000">{{$UltimaF}}</label>
                </b>
                <br/>
                <b id="updte2">
                  <span  style="color: blue">Proxima Tickera </span> : <label style="color:#000">{{$UltimaT}}</label>
                </b>
              </h4>
            </div>

          </div> <!--row-->
        </div>
        
      </div>
            
    </div>
          
          
            <div class="col-lg-8 facturacion"  id="card-facturacion">
              <div class="card card-primary card-outline">
                <!-- <div class="card-header" style="background: linear-gradient(90deg, blue, purple);">
                  <h5 class="card-title m-0" style="color: white"><i class="fas fa-file-alt"></i><b> DETALLES DE FACTURA</b></h5>
                </div> -->
                <img src="{{ asset('/images/arian/facturacion/boton_detalles_factura.png') }}" height="50px" style="display: block;margin: 0 auto;width: 100%;">
                   <form action="{{route('facturar')}}" id="formFacturacion" method="POST">
              {{csrf_field()}}
                <div class="card-body disabledbutton" id="cardFactura">
                  <div class="row">
                    <div class="col-6">
           
                        <div class="form-group row mb-2 ml-3">
                          
                        {{-- <label for="text1" id="labelIva" class="col-4 col-form-label totally labelbutton"> <i class="fa fa-edit"></i> IVA ({{$recargo? $recargo->monrgo: ''}})</label>  --}}
                        <label for="text1" id="Iva" class="col-4 col-form-label totally"> <i class="fa fa-edit"></i> IVA ({{$recargo? $recargo->monrgo: ''}})</label> 
                        <div class="col-6">
                          <input TabIndex ='-1' readonly id="iva" name="iva" type="text" value="0" class="form-control mt-1 " style="font-size:180%;font-weight:bold">
                        </div>
                      </div>
                      <div class="form-group row mb-1 ml-3">
                        <label for="descuento" class="col-4 col-form-label totally">DESCUENTO</label> 
                        <div class="col-6">
                          <input TabIndex ='-1' readonly id="descuento" name="descuento" type="text" value="0" class="form-control mt-1"  style="font-size:180%;font-weight:bold">
                        </div>
                      </div>
                      <div class="form-group row mb-1 ml-3 tasa">
                        <label for="tasa" id="labelTasa" class="col-4 col-form-label totally">TASA $</label> 
                        <div class="col-6">
                          <input TabIndex ='-1' readonly id="tasa" name="tasa" type="text" class="form-control mt-1" style="font-size:180%;font-weight:bold;">
                        </div>
                      </div>
                      <div class="form-group row mb-1 ml-3 ">
                        <label for="diferencia" id="labelDiferencia" class="col-4 col-form-label totally">DIFERENCIA Bs</label> 
                        <div class="col-6">
                          <input TabIndex ='-1' readonly id="diferencia" name="diferencia" type="text" class="form-control mt-1" style="font-size:180%;font-weight:bold;">
                        </div>
                      </div>
                      <div class="form-group row mb-1 ml-3 moneda">
                        <label for="moneda" id="labelMoneda" class="col-4 col-form-label totally" id="moneda">MONEDA : BOLIVARES </label> 
                      </div>
                      <div class="form-group row mb-2 ml-3 transporte">
                        <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                         <input type="checkbox" class="custom-control-input" id="customSwitch3" name="transporte">
                         <label class="custom-control-label" for="customSwitch3"> TRANSPORTE </label>
                        </div>
                      </div>
                      
                    </div>
                    
                    <div class="col-6">
                    
                      <div class="form-group row mb-2">
                        <label for="subtotal" class="col-4 col-form-label totally">SUBTOTAL GENERAL</label> 
                        <div class="col-6">
                          <input TabIndex ='-1' readonly id="subtotal" name="subtotal" type="text" value="0" class="form-control mt-1"  style="font-size:180%;font-weight:bold">
                        </div>
                      </div>
                      <div class="form-group row mb-2">
                        <label for="subtotal1" class="col-4 col-form-label totally">SUBTOTAL CON DESCUENTO</label> 
                        <div class="col-6">
                          <input TabIndex ='-1' readonly id="subtotal1" name="subtotal1" type="text" value="0" class="form-control mt-1"  style="font-size:180%;font-weight:bold">
                        </div>
                      </div>
                      <div class="form-group row mb-2">
                        <label for="total" class="col-4 col-form-label totally">TOTAL</label> 
                        <div class="col-6">
                          <input TabIndex ='-1' readonly id="total" name="total" type="text" value="0" class="form-control mt-1" style="font-size:180%;font-weight:bold">
                        </div>
                      </div>
                      <div class="form-group row mb-2 bolivarrestan">
                        <label for="bolivarrestan" id="labelRestan" class="col-4 col-form-label totally">RESTANTE Bs</label> 
                        <div class="col-6">
                          <input TabIndex ='-1' readonly id="bolivarrestan" name="bolivarrestan" type="text" value="0" class="form-control mt-1"  style="font-size:180%;font-weight:bold;">
                        </div>
                      </div>
                      <div class="form-group row mb-2 tasarestan">
                        <label for="tasa" id="labelTasa" class="col-4 col-form-label totally">RESTANTE $</label> 
                        <div class="col-6">
                          <input TabIndex ='-1' readonly id="tasarestan" name="tasarestan" type="text" value="0" class="form-control mt-1"  style="font-size:180%;font-weight:bold;">
                        </div>
                      </div>
                      <div class="form-group row mb-2 transporcost">
                        <label for="tasa" id="labelTransporcost" class="col-4 col-form-label totally">COSTO TRANSPORTE</label> 
                        <div class="col-6">
                          <input TabIndex ='-1' readonly id="transporcost" name="transporcost" type="text" value="0" class="form-control mt-1"  style="font-size:180%;font-weight:bold;">
                        </div>
                      </div>
                    </div>
                  </div>
                    <div>
                      <input id="inventario" type="hidden" name="inventario">
                    </div>
                    <div>
                      <input id="pagos" type="hidden" name="pagos">
                    </div>
                    <div class="col-6">
                      <input id="monrgo" name="monrgo" type="hidden" value="{{$recargo?$recargo->monrgo:''}}">
                    </div>
                    <input type="hidden" id="recargos_json" data-value="{{ json_encode($recargo) }}">
                  <div class="row">
                    <div class="col-12">
                      <div class="input-group ml-3 col-12 cuadroarticulos">
                        <div class="col-5">

                          <select name="articulos" id="articulo" style="width: 90%; text-align: center;" data-href="{{route('getPrecio')}}" data-href2="{{route('getTallas')}}" class="form-control select2 articulos" data-dropdown-css-class="select2-danger" class="@error('articulos') is-invalid @enderror" aria-hidden="true">
                            <option disabled selected>BUSCAR ARTICULO</option>
                            @foreach($articulos as $articulo)
                            <option value="{{$articulo->codart}}"da>{{$articulo->codart .' / '. $articulo->desart}}</option>
                              @endforeach
                          </select>  
                        </div>
{{-- 

                        <div class="col-2">
                          <select name="tallas"  disabled id="tallas" class="form-control"  data-dropdown-css-class="select2-danger" class="@error('tallas') is-invalid @enderror" aria-hidden="true">
                            <option disabled selected>TALLA</option>
                          </select>
                        </div> --}}

                         <div class="col-2">
                          <input id="cantidad" value="1" disabled name="cantidad" data-href="{{route('getExistenciaArt')}}" onKeypress = "javascript:return SoloNumeros(event)" type="text" placeholder="CANTIDAD" class="form-control" data-toggle="tooltip" data-html="true" title="">
                         </div>
                         
                         <div class="col-2">
                          <input readonly id="preunit" TabIndex ='-1' name="preunit" type="text" value="0" placeholder="0" class="form-control">
                         </div>


                          <input readonly type="hidden" id="preunit2"  name="preunit2" value="0" placeholder="0" class="form-control">
                       

                         <div class="col-3">
                          <input readonly id="precio" TabIndex ='-1' name="precio" type="text" value="0" placeholder="0" class="form-control">
                          {{--<input class="form-control" readonly  type="text" name="serial" id="serialEquipo" style="display:block;margin-top:9px" placeholder="Serial Equipo">--}}
                         </div>
                         
                           <input readonly type="hidden" id="precio2"  name="precio2" value="0" placeholder="0" class="form-control">
                          
                        </div>

                    <div class="input-group mb-2 mt-4 ml-2 col-12 cuadropagos" style="display: none">
                      <div class="col-3">
                        <select name="fpagos" id="fpagos" class="form-control" data-dropdown-css-class="select2-danger" class="@error('fpagos') is-invalid @enderror select" aria-hidden="true">
                            <option disabled selected>FORMA DE PAGO</option>
                              @foreach($fpagos as $fpago)
                               <option value="{{$fpago->destippag}}">{{$fpago->destippag}}</option>
                              @endforeach
                              </select>

                               <select name="tipocredito" id="tipocredito" class="form-control mt-2" style="display:none" data-dropdown-css-class="select2-danger" class="@error('tipocredito') is-invalid @enderror" aria-hidden="true">
                              <option disabled selected>SELECCIONE</option>
                               <option value="VISA">VISA</option>
                               <option value="MASTERCARD">MASTERCARD</option>
                         </select>
                       </div>

                       <div class="col-3 ml-1">
                          <input id="montopago" name="montopago" value="0" onKeypress = "javascript:return NumerosReales(event)" type="text" placeholder="0" class="form-control" data-toggle="tooltip" data-html="true" title="">
                       </div>

                       <div class="col-3 ml-1">
                         <select name="bancos" id="bancos" class="form-control" class="@error('bancos') is-invalid @enderror" aria-hidden="true">
                              <option disabled selected>SELECCIONE BANCO</option>
                              @foreach($bancos as $banco)
                               <option value="{{$banco->nomban}}">{{$banco->nomban}}</option>
                              @endforeach
                         </select>
                        </div>

                         <div class="col-2 ml-1">
                            <button type="button" id="pagocompleto" disabled class="button ml-1 mb-4 pagartodo"><b>COMPLETO</b>
                            </button>

                          
                             <button type="button" id="pagorestante" style="display: none" disabled class="button pagarrestante ml-1 mb-4"><b>RESTANTE</b>
                            </button>

                             <a type="button" id="monedapago" class="button monedapago btn btn-warning"><i class="fas fa-dollar-sign"></i>
                            </a>
                         </div>

                          <div class="ml-2 mt-1">
                          <!--   <label>Ref.Billete</label> -->
                            <input  type="text" name="refbillete" id="refbillete" placeholder="N° Referencia">
                          </div>
                          <div class="ml-2 mt-1">
                            <div class="form-group">
                              <div class="input-group">
                                <div class="input-group-prepend">
                                 <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                                </div>
                                <input type="text" style="text-align: center"  name="cantcambiar" id="cantcambiar"  type="text" value="0" placeholder="0" onKeypress = "javascript:return NumerosReales(event)">
                              </div>
                            </div>
                          </div>
                          <div class="ml-2 mt-1">
                            <p style="text-align: center;"><i class="fas fa-equals"></i></p>
                          </div>
                          <div class="ml-2 mt-1">
                            <div class="form-group">
                              <div class="input-group">
                                <div class="input-group-prepend">
                                 <span class="input-group-text" >Bs</span>
                                </div>
                               <input type="text" style="text-align: center" readonly  name="montocambiar" id="montocambiar"  type="text" value="0" placeholder="0" onKeypress = "javascript:return SoloNumeros(event)">
                              </div>
                            </div>
                          </div>
                         </div>
                        </div>
                      </div>

                        <!--botones-->
                        <div class="row">
                           <div class="col-4 ml-3">
                             <button type="button" id="añadir" style="display:block;" disabled data-href="{{route('getArt')}}" class="button border1 border11 mt-2 ml-3 añadir"><b><i class="fas fa-plus"></i> AÑADIR ARTICULO</b>
                             </button>
                             
                             <button type="button" id="añadirpago" style="display:none;" disabled class="button mt-2 ml-2 añadirp"><b>AÑADIR PAGO</b></button>
                           </div>
                           
                          <div class="col-4">
                            <button type="button" id="pago" class="button mt-2 ml-4 tpago" style="display:none;"><b>FORMA DE PAGO</b></button>
                            
                              
                            <button type="button" id="listarticulos" style="display:none" class="button mt-2 ml-2 tart"><b>ARTICULOS</b>
                            </button>
                        
                          </div>
                          
                      </div>
                       
                        </form>
                       
                      <div class="card-body table-responsive p-0" id="artitabla" style="height: 600px;display:block">
                        <table class="table table-head-fixed">
                          <thead>
                            <tr style="text-align:center">
                              <th>°</th>
                              <th>CODIGO</th>
                              <th>ARTICULO</th>
                              <th>DIVISA</th>
                              <th>COSTO</th>
                              <th>SERIAL</th>
                              <th>CANTIDAD</th>
                              <th>PRECIO</th>
                              <th>DESCUENTO</th>
                              <th>DESCONTADO</th>
                              <th>IVA</th>
                              <th>ACCION</th>

                            </tr>
                          </thead>
                          <tbody class="arts">
                           <!--  <tr>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr> -->
                          </tbody>
                        </table>
                      </div>
                        <div class="card-body table-responsive p-0" id="pagotabla" style="height: 300px; display:none">
                        <table class="table table-head-fixed">
                          <thead>
                            <tr style="text-align:center">
                              <th>°</th>
                              <th>MONEDA</th>
                              <th>FORMATO</th>
                              <th>MONTO</th>
                              <th>BANCO</th>
                              <th>CREDITO</th>
                              <th>REF</th>
                              <th>ACCION</th>
                            </tr>
                          </thead>
                          <tbody class="cosaspagos">
                           <!--  <tr>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr> -->
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  </div>
                </div>
                
              </div><!-- /.card -->
              
            </div>
            <!-- /.col-md-6 -->
          </div>
          <!-- /.row -->
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>

    <div class="col" style="display:none">
      <a type="button" data-href="{{route('getPersonaF')}}" class="btn btn-info mt-4 ml-2" id="verificarCliente"><b>Verificar Cliente</b></a>
    </div>
    <input id="clienttojson" type="hidden" value="{{$clienttojson}}">
    <input id="arttojson" type="hidden" value="{{$arttojson}}">
    <input id="fpagotojson" type="hidden" value="{{$fpagotojson}}">
    <input id="bancostojson" type="hidden" value="{{$bancostojson}}">
    <input id="tasatojson" type="hidden" value="{{$tasatojson}}">
    <input id="recargostojson" type="hidden" value="{{$recargostojson}}">
    <input id="monedastojson" type="hidden" value="{{$monedastojson}}">
    <input id="todastasastojson" type="hidden" value="{{$todastasastojson}}">
    <input id="monedac" type="hidden">
    <input id="monedacvalor" type="hidden">
    <input id="transportevalor" type="hidden" value="{{$transporte}}">
    <input type="hidden" id="printFactura"  value="{{route('printFactura')}}">
    
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!--modal Login Caja-->

{{-- VISTA MODAL BLOQUEO DE CAJA --}}

  <form id="formSecureCaja" action="">
    <input type="hidden" name="" id="urlCheckCaja" value="{{route('abrirCaja')}}">
    <input type="hidden" name="" id="tokenUser" value="{!!encrypt(Auth::user()->loguse)!!}">
    <input type="hidden" name="" id="loginCaja" value="{{ route('loginCaja') }}">
    <div class="modal fade" id="lockedCaja" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            {{-- <h5 class="modal-title" id="staticBackdropLabel">Seguridad de Caja</h5> --}}
            {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button> --}}
          </div>
          <div class="modal-body">
            <div class="row justify-content-center">
              <h4> Caja Cerrada <i class="fa fa-lock"></i></h4>
            </div>
              <div id="abrirCajaMenu">
                  <div class="form-group">
                    <label for="cajaSelected" class="col-sm-2 col-form-label">Cajas Asociada</label>
                    <div class="col-sm-12">
                      <select id="cajaSelected" class="form-control cajs" style="width: 100%;"  data-select2-id="1" tabindex="-1" aria-hidden="true" class="@error('tipodocumento') is-invalid @enderror">
                      </select>
                    </div>
                </div>
                <div class="form-group ">
                  <label for="passwordCaja" class="col-sm-2 col-form-label">Password</label>
                  <div class="col-sm-12">
                    <input type="password" class="form-control" id="passwordCaja">
                  </div>
                </div>
              </div>
          </div>
          <div class="modal-footer">
            {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
            <button type="submit" class="btn btn-primary"><b>ABRIR CAJA</b></button>
          </div>
        </div>
      </div>
    </div>
  </form>
    
         <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Abrir Caja</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
                <div class="modal-body" >

              <form class="form-horizontal" method="post" data-login="{{ route('loginCaja') }}" data-user="{!!encrypt(Auth::user()->loguse)!!}" data-url="{{ route('abrirCaja') }}">
                            <!-- form start -->
                            {{csrf_field()}}
                <div class="card-body">
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Cajas Asociada</label>
                    <div class="col-sm-12">
                      <select class="form-control cajs" style="width: 100%;" data-caja="{{ route('getCajas') }}" data-select2-id="1" tabindex="-1" aria-hidden="true" name="tipodocumento" class="@error('tipodocumento') is-invalid @enderror">
                        
                    </select>
                      <input type="text" name="_token" style="display: none;" value="{!!csrf_token()!!}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-12">
                      <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                    </div>
                  </div>
                
                </div>
                <!-- /.card-body -->
                
              </form>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
               <button type="submit" class="btn btn-info" id="enviar"><b>Seleccionar</b></button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


  <!--fin modal login Caja-->
  <form action="" id="formCliente">
            <div class="modal fade" id="nuevocliente" tabindex="-1" role="dialog" aria-labelledby="nuevocliente" aria-hidden="true">
           <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content"> 
            <div class="modal-header">
             <h5 class="modal-title" id="nuevoclientelabel"><b> Agregar Cliente</b></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
            </div>
             <div class="modal-body content-centered">
             <div class="row">
                 <div class="col-sm-6">
                 <div class="form-group">
                  <label for="tipodocumento">Tipo de documento</label>
                    <select class="form-control" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" id="tipodocumento2" name="tipodocumento" class="@error('tipodocumento') is-invalid @enderror">
                        <option data-select2-id="3">V</option>
                        <option data-select2-id="43">E</option>
                        <option data-select2-id="44">J</option>
                        <option data-select2-id="45">G</option>
                        <option data-select2-id="46">P</option>
                        <option data-select2-id="47">R</option>
                        <option data-select2-id="48">C</option>
                    </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                    @error('tipodocumento')
                     <div class="alert alert-danger">{!!$message!!}</div>
                   @enderror
                  </div>     
                 </div>

                 <div class="col-sm-6">
                <div class="form-group">
                <label>Numero de documento<span style="color:red"> *</span></label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">V/J</span>
                  </div>
                 
                  
                  <input type="text" class="form-control"  onKeypress = "javascript:return NumerosReales(event)" placeholder="Ejem: 1234567" name="cedularifmodal" 
                  id="cedularifmodal" class="@error('cedularifmodal') is-invalid @enderror">
                </div>
                @error('cedularifmodal')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
               </div>
               </div>

               <div class="form group">
                <label for="nombre">Nombre<span style="color:red"> *</span></label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Aa</span>
                  </div>
                  <input type="text" class="form-control" onkeyup="pasarMayusculas(this.value, this.id)" class="@error('nombremodal') is-invalid @enderror" placeholder="Ejem: Juan Gutierrez" name="nombremodal" id="nombremodal">
                </div>
                @error('nombremodal')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                <div class="form group">
                    <label for="direccion">Dirección</label>
                <div class="input-group mb-3">
                  <textarea type="text" class="form-control" onkeyup="pasarMayusculas(this.value, this.id)" class="@error('direccion') is-invalid @enderror" placeholder="Dirección Fiscal..." name="direccionmodal" id="direccionmodal"> </textarea>
                </div>
                @error('direccionmodal')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                <div class="form group">
                <label for="telefono">Numero de teléfono (opcional)</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-phone-square"></i></span>
                  </div>
                  <input type="tel" class="form-control" onKeypress = "javascript:return NumerosReales(event)" class="@error('telefono') is-invalid @enderror" placeholder="Ejem: 041..." name="telefono" id="telefono"> 
                </div>
                @error('telefono')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                <div class="form group">
                <label for="email">Correo Electrónico (opcional)</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="email" class="form-control" class="@error('email') is-invalid @enderror"  placeholder="Ejem: Juan@email.com" name="email" id="emailcliente"> 
                </div>
                @error('email')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                 
            </div> <!--modal body -->

           <div class="modal-footer" id="loading">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><b> Cerrar</b></button>
            <button type="button" class="btn btn-success " data-href="{{route('registrarclienteModal')}}" id="guardarClienteF"><b> Guardar</b></button>
           </div>
         </div>
       </div>
     </div>
    </form>

    <!--modal descuento-->
    <form id="formDescuento">
       <div class="modal fade" id="descuentoM" tabindex="-1" role="dialog" aria-labelledby="descuentoM" aria-hidden="true">
           <div class="modal-dialog modal-dialog-centered modal-lg"  role="document">
            <div class="modal-content" > 
            <div class="modal-header">
             <h5 class="modal-title" style="text-align:center" id="descuentoM"><b>OPCIONES DE DESCUENTO</b></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
            </div>
             <div class="modal-body content-centered">
              <div class="row mb-4" style="text-align:center">
               <div class="col">
                 <input type="radio" id="descuentoR"  checked name="tdescuento" value="general">
                 <label for="tdescuento">GENERAL</label><br>
               </div>
               <div class="col">
                 <input type="radio" id="descuentoR" name="tdescuento" value="articulo">
                 <label for="tdescuento">POR ARTICULO</label><br>
               </div>
             </div>

                  <div class="card-body table-responsive p-0" id="tablaInventario" style="height: 250px; display:none">
                        <table class="table table-head-fixed">
                          <thead>
                            <tr style="text-align:center">
                              <th>CODIGO</th>
                              <th>ARTICULO</th>
                              <th>SELECCIONAR</th>
                            </tr>
                          </thead>
                          <tbody class="listInventario">

                          </tbody>
                        </table>
                      </div>
            
                <div style="display: block" id="descuentosOpciones">
                <!--  <h6 style="text-align:center" class="title"><b>SELECCIONE EL DESCUENTO O INGRESE MANUALMENTE</b></h6> -->
                 <br>
                 <div class="row" style="text-align:center">
                  <div class="col-6">
                        <button type="button" id="precargado" class="button border1 border11 mb-3 precargado">
                          <b>PRECARGADO</b>
                        </button>
                  </div>

                   <div class="col-6">
                         <button type="button" id="manual" class="button border1 border11 mb-3 manual">
                          <b>INGRESAR MANUALMENTE</b>
                        </button>
                  </div>
                 </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                   
                          <select name="descuentosSelect" id="descuentosSelect" class="form-control" data-dropdown-css-class="select2-danger" disabled class="@error('descuentosSelect') is-invalid @enderror select" aria-hidden="true">
                            <option disabled selected>SELECCIONE</option>
                              @foreach($descuentos as $descuento)
                               <option value="{{$descuento->mondesc}}">{{$descuento->mondesc}}</option>
                              @endforeach
                              </select>
                              </div>
                    </div>
                    <div class="col-sm-6">

                      <div class="form-group">
                        <input type="text"  class="form-control" value="0" name="descuentoInput" onKeypress = "javascript:return SoloNumeros(event)" id="descuentoInput">
                       </div>
                    </div>
                  </div>
                  </div>

                 
            </div> <!--modal body -->

           <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><b> Cerrar</b></button>
            <button type="button" class="btn btn-success" disabled id="guardarDescuento"><b> Guardar</b></button>
           </div>
         </div>
       </div>
     </div>
    </form>
    <!--Fin modal descuento-->

    <!--Modal devolución-->
    <form id="formDevolucion">
      <div class="modal fade" id="devolucionM" tabindex="-1" role="dialog" aria-labelledby="devolucionM" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
           <div class="modal-content"> 
             <div class="modal-header">
             <h5 class="modal-title" style="text-align:center" id="devolucionM"><b>DEVOLUCION</b></h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
               </button>
             </div>
           <div class="modal-body content-centered">
             <div class="form-group">
               <div class="row">
                 <div class="col-lg-4 offset-lg-3">
                   <input type="text" id="codfactura" class="ml-3 form-control" name="codfactura" placeholder="N° de Factura">
                 </div>
                 <div class="col-lg-4 ">
                   <button type="button" class="btn btn-info ml-3" id="buscarFactura" data-href="{{route('buscarFactura')}}">BUSCAR</button>
                 </div>
               </div>
               <div class="row  mt-2 ">
                 <div class="ErrorTxt col-6 offset-3">

                 </div>
               </div>
             </div>
           </div>
             <div class="card-body table-responsive p-0" id="tablaDevolucionFac">
               <table class="table table-head-fixed">
                 <thead>
                   <tr style="text-align:center">
                     <th>N° FACTURA</th>
                     <th>DOCUMENTO CLIENTE</th>
                     <th>CLIENTE</th>
                     <th>MONTO</th>
                     <th>ANULAR</th>
                   </tr>
                 </thead>
                 <tbody class="listFactura">
                 </tbody>
               </table>
             </div>
             <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><b> Cerrar</b></button>
              <button type="button" class="btn btn-success" id="anulacion" style="display:none" data-href="{{route('anularFactura')}}"><b> Guardar</b></button>
             </div>
           </div> <!--modal body -->
        </div>
      </div>
   </form>
    <!--Fin modal devolución-->

        <!--Modal iva-->
    <form id="formIva">
       <div class="modal fade" id="ivaM" tabindex="-1" role="dialog" aria-labelledby="ivaM" aria-hidden="true">
           <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content"> 
            <div class="modal-header">
             <h5 class="modal-title" style="text-align:center" id="ivaM"><b>SELECCIÓN DE IVA</b></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
            </div>
             <div class="modal-body content-centered" style="text-align:center">
               <div class="col ml-2 mr-4">
                <select name="recargos" id="recargos" data-href="{{route('getRecargo')}}" class="form-control" data-dropdown-css-class="select2-danger" class="@error('recargos') is-invalid @enderror" aria-hidden="true">
                  <option disabled selected>-- IVA --</option>
                  <option value="00.00"> SIN IVA </option>
                 @foreach($recargos as $iva)
                  <option value="{{$iva->monrgo}}">{{$iva->nomrgo}}</option>
                  @endforeach
                </select>
               </div>
            </div> <!--modal body -->

           <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><b> CERRAR</b></button>
            <button type="button" disabled class="btn btn-info ml-3" id="guardarIva" ><b>GUARDAR</b></button>
           </div>
         </div>
       </div>
     </div>
    </form>
    <!--Fin modal iva-->

    <!--Modal presupuesto facturacion-->
     <form id="formPresupuesto">
       <div class="modal fade" id="presupuestoM" tabindex="-1" role="dialog" aria-labelledby="presupuestoM" aria-hidden="true">
           <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content"> 
            <div class="modal-header">
             <h5 class="modal-title" style="text-align:center" id="presupuestoM"><b>FACTURACION DE PRESUPUESTO</b></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
            </div>
             <div class="modal-body content-centered">
              <div class="row mb-4" style="text-align:center">
               <div class="col">
                 <label>N° DE PRESUPUESTO: </label>
                 <input type="text" id="codpresupuesto" class="ml-3" name="codpresupuesto" placeholder="Ingrese el N° de Presupuesto...">

                  <button type="button" class="btn btn-info ml-3" id="buscarPresupuesto" data-href="{{route('buscarPresupuestoFac')}}">BUSCAR</button>
               </div>
               <!-- <div class="col">
                 <input type="radio" id="descuentoR" name="tdescuento" value="articulo">
                 <label for="tdescuento">POR ARTICULO</label><br>
               </div> -->
             </div>

                  <div class="card-body table-responsive p-0" id="tablaPresupuestoFac" style="height: 150px; display:none">
                        <table class="table table-head-fixed">
                          <thead>
                            <tr style="text-align:center">
                              <th>N° PRESUPUESTO</th>
                              <th>DOCUMENTO CLIENTE</th>
                              <th>CLIENTE</th>
                              <th>SELECCIONAR</th>
                            </tr>
                          </thead>
                          <tbody class="listPresupuesto">

                          </tbody>
                        </table>
                      </div>

                 
            </div> <!--modal body -->

           <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><b> Cerrar</b></button>
            <button type="button" class="btn btn-success" id="seleccionPre" style="display:none" data-href="{{route('seleccionarPre')}}"><b> Guardar</b></button>
           </div>
         </div>
       </div>
     </div>
    </form>
    <!--Fin Modal presupuesto facturacion-->

    <!-- Modal moneda de pago -->
      <form id="formMoneda">
       <div class="modal fade" id="monedaM" tabindex="-1" role="dialog" aria-labelledby="monedaM" aria-hidden="true">
           <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content"> 
            <div class="modal-header">
             <h5 class="modal-title" style="text-align:center" id="monedaM"><b>LISTA DE MONEDAS</b></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
            </div>
             <div class="modal-body content-centered">
              <div class="col">
                <select name="monedas" id="monedas" class="form-control"  data-dropdown-css-class="select2-danger" aria-hidden="true">
                  <option disabled selected>--SELECCIONE MONEDA --</option>
                  @foreach($monedas as $moneda)
                  <option value="{{$moneda->codigoid}}">{{$moneda->nombre}}</option>
                  @endforeach
                </select>
              </div>

            </div> <!--modal body -->

           <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><b> Cerrar</b></button>
            <button type="button" class="btn btn-success" id="selecMoneda" data-href="{{route('getMoneda')}}" data-href2="{{route('getTasaMoneda')}}"><b> Guardar</b></button>
           </div>
         </div>
       </div>
     </div>
    </form>
    <!-- -->

        <!--Modal presupuesto nota entrega-->
     <form id="formNota">
       <div class="modal fade" id="notaM" tabindex="-1" role="dialog" aria-labelledby="notaM" aria-hidden="true">
           <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content"> 
            <div class="modal-header">
             <h5 class="modal-title" style="text-align:center" id="notaM"><b>FACTURACION DE NOTA ENTREGA</b></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
            </div>
             <div class="modal-body content-centered">
              <div class="row mb-4" style="text-align:center">
               <div class="col">
                 <label>N° DE NOTA: </label>
                 <input type="text" id="codnota" class="ml-3" name="codnota" placeholder="Ingrese el N° de Nota...">

                  <button type="button" class="btn btn-info ml-3" id="buscarNota" data-href="{{route('buscarNotaFac')}}">BUSCAR</button>
               </div>
               <!-- <div class="col">
                 <input type="radio" id="descuentoR" name="tdescuento" value="articulo">
                 <label for="tdescuento">POR ARTICULO</label><br>
               </div> -->
             </div>

                  <div class="card-body table-responsive p-0" id="tablaNotaFac" style="height: 150px; display:none">
                        <table class="table table-head-fixed">
                          <thead>
                            <tr style="text-align:center">
                              <th>N° NOTA DE ENTREGA</th>
                              <th>DOCUMENTO CLIENTE</th>
                              <th>CLIENTE</th>
                              <th>SELECCIONAR</th>
                            </tr>
                          </thead>
                          <tbody class="listNota">

                          </tbody>
                        </table>
                      </div>

                 
            </div> <!--modal body -->

           <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><b> Cerrar</b></button>
            <button type="button" class="btn btn-success" id="seleccionNota" style="display:none" data-href="{{route('seleccionarNota')}}"><b> Guardar</b></button>
           </div>
         </div>
       </div>
     </div>
    </form>
    <div class="modal fade" id="modalSerial" tabindex="-1" role="dialog" aria-labelledby="notaM" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
       <div class="modal-content" style = "height: 800px"> 
       <div class="modal-header">
        <h5 class="modal-title" style="text-align:center" id="producto"><b>SERIALES DEL PRODUCTO </b></h5>
        
       </div>
       <div class="row" style="margin-top: 31px;">         
         <div style="margin-left: 31px; " class="col-md-4">
           
            <label for="searchSeriales">Serial: </label> <input id="serialesValues"  placeholder="Buscar Seriales" type="text" class="form-control">
            
          </div>
          
          <div style="margin-top: 31px;" class="col-md-4">
          <button class="btn btn-primary" id="searchSeriales">Buscar</button>
          </div>
       </div>

        <div class="modal-body content-centered">
         

             <div class="card-body table-responsive p-0"  style="height: 400px;">
                   <table class="table table-head-fixed">
                     <thead>
                       <tr style="text-align:center">
                          
                         <th>SERIAL</th>    
                         <th>FACTURAR</th>                     
                       </tr>
                     </thead>
                     <tbody id="listaSeriales">
                     
                     </tbody>
                   </table>
                 </div>

            
       </div> <!--modal body -->

      <div class="modal-footer">
       <button type="button" class="btn btn-danger" id="cerrarSerial" ><b> Cerrar</b></button>
      </div>
    </div>
  </div>
</div>


   {{-- <div class="modal fade" id="descripcionAdd" tabindex="-1" role="dialog" >
          <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
           <div class="modal-content"> 
             <div class="modal-header">
             <h5 class="modal-title" style="text-align:center" ><b>DESCRIPCION DEL ARTICULO </b></h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
               </button>
             </div>
           <div class="modal-body content-centered">
             <div class="form-group">
               <div class="row">
                 <div class="col-lg-11 offset-lg-0">
                   <label>ARTICULO</label>
                   <textarea id="articuloDesc"  class="ml-3 form-control"></textarea> 
                   <label>DESCRIPCION</label>
                   <input type="text" id="descArticulo" class="form-control">
                 </div>
               </div>
             </div>
           </div>
            
             <div class="modal-footer">
              
              <button type="button" id="guardarDescripcion" class="btn btn-success" ><b> Guardar</b></button>
             </div>
           </div> <!--modal body -->
        </div>
      </div> --}}
      <div class="modal fade" id="arqueCaja" tabindex="-1" role="dialog" >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
         <div class="modal-content"> 
           <div class="modal-header">
           <h5 class="modal-title" style="text-align:center" ><b>ARQUEO DE CAJA DE HOY {{$dia}}</b></h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
             </button>
           </div>
         <div class="modal-body content-centered">
           <div class="form-group">
             <div class="row">
               <div class="col-lg-11 offset-lg-0" id="totalCaja">
                 
                 
               </div>
             </div>
           </div>
         </div>
          
           
         </div> <!--modal body -->
      </div>
    </div>

    <!-- Modal AUTOTIZACION CON CLAVE PARA ELIMINAR ARTICULO / PRODUCTO-->
<div class="modal fade" id="exampleModalCenter" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"><strong>INGRESE CLAVE PARA ELIMINAR ARTICULO</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

        <div class="modal-body">
          <div class="col-12 pb-2">
            <label>Seleccione Jefe</label><br>
            <select class="select2" name="idjefes" id="idjefes">
              <option value="" disabled selected>Seleccione Jefe que autoriza</option>
              @foreach($jefes as $jefesSelect)
                  <option value="{{ $jefesSelect->id }}">{{ $jefesSelect }}</option>
              @endforeach
           </select>
          </div>
          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
          <input type="hidden" id="usuarioLogeado" value="{{ auth()->user()->loguse }}">
          <div class="col-12">
            <label>Clave</label>
            <input class="form-control" type="password" name="claveAutorizacion" id="claveAutorizacion" placeholder="ingrese la clave">
          </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" id="cerrarModal" data-dismiss="modal">Cancelar</button>
          <button type="submit" id="PermisoClave"  class="btn btn-success" data-href="{{route('validacionContraseña')}}">Autorizar</button>
        </div>

    </div>
  </div>
</div>


    <!-- Modal AUTOTIZACION CON CLAVE PARA AÑADIR DESCUENTO -->
    <div class="modal fade" id="modalClaveDescuento" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle"><strong>INGRESE CLAVE PARA PERMITIR DESCUENTO</strong></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
    
            <div class="modal-body">
              <div class="col-12 pb-2">
                <label>Seleccione Jefe</label><br>
                <select class="select2" name="idjefesDescuentos" id="idjefesDescuentos">
                  <option value="" disabled selected>Seleccione Jefe que autoriza</option>
                  @foreach($jefes as $jefesSelect)
                      <option value="{{ $jefesSelect->id }}">{{ $jefesSelect }}</option>
                  @endforeach
               </select>
              </div>
              <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
              <input type="hidden" id="usuarioLogeado" value="{{ auth()->user()->loguse }}">
              <div class="col-12">
                <label>Clave</label>
                <input class="form-control" type="password" name="claveAutorizacionDescuento" id="claveAutorizacionDescuento" placeholder="ingrese la clave">
              </div>
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" id="cerrarModal" data-dismiss="modal">Cancelar</button>
              <button type="submit" id="PermisoClaveDescuento" class="btn btn-success" data-href="{{route('validacionContraseña')}}">Autorizar</button>
            </div>
    
        </div>
      </div>
    </div>


      {{-- <conexion-fiscal channel="{{ $caja }}"></conexion-fiscal> --}}


  <!-- Main Footer -->
  <footer class="main-footer" style="position:relative; margin-left: 0px;text-align: center; background-color: #201d1c">
    <strong id="year"></strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

<!-- Bootstrap 4 -->
{{-- <script src="asset{{'plugins/bootstrap/js/bootstrap.bundle.min.js'}}"></script> --}}
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}" ></script>

<!-- AdminLTE App -->
<script src="{{ asset('jsadminlte/adminlte.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- jQuery -->
<!-- ChartJS -->
<script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('plugins/sparklines/sparkline.js') }}"></script>
<!-- JQVMap -->
<script src="{{ asset('plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('plugins/jqvmap/maps/jquery.vmap.world.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>

<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('jsadminlte/adminlte.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('jsadminlte/pages/dashboard.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('jsadminlte/demo.js') }}"></script>
<script src="{{asset('js/sweetalert2.0.js')}}"></script>
{{-- <script src="{{ asset('sweetalert2/dist/sweetalert2.all.js') }}"></script>   --}}
{{-- <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script> --}}
<script src="{{ asset('js/app.js') }}" ></script>
<script src="{{ asset('js/select2.full.min.js') }}" ></script>
<script src="{{ asset('js/jquery.mask.js') }}" ></script>
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('js/select2.full.min.js') }}" ></script>


</body>
</html>
 <script src="{{asset('js/persona.js')}} "></script>
 <script src="{{asset('js/chequeo.js')}} "></script>
 <script src="{{asset('js/clienteModal2.js')}} "></script>
 <script src="{{asset('js/facturacion.js')}} "></script>
 <script src="{{ asset('js/caja/sendreporte.js') }}" ></script>
 <script src="{{asset('js/socket.js')}}"></script>
 <link href="{{ asset('/css/facturacion.css') }}" rel="stylesheet">

<script>
  window.isFiscal = "{{$isfiscal}}"
   var currentYear = new Date();
   window.route = "{{Request::route()->getName()}}"
   window.caja = "{{$caja}}"
   window.socket = "http://{{$caja2->impfishost}}:3500"
   window.ip = "{{$caja2->impfishost}}"
   
   document.getElementById('year').innerHTML = `Copyright &copy; ${currentYear.getFullYear()} <a>VIT Venezolana de Industria Tecnologica</a>.`
   document.onkeydown = (e) => e.type === 'keydown' && e.keyCode === 45 ? arqueo(e) : null
   window.routeCliente = "{{route('registrarclienteModal')}}"
   
  async function arqueo(e){
    e.preventDefault()
    await fetch('/modulo/Facturacion/arqueoCaja')
            .then((res) => res.json())
            .then(({total,devoluciones}) => {
              document.getElementById("totalCaja").innerHTML = `<h4>Ventas: ${total}</h4><br/>
                                                                <h4>Devoluciones: ${devoluciones}</h4><br/>
                                                                <h4>Total de Ventas: ${((devoluciones-total) < 0) ? (Math.abs(devoluciones-total)).toFixed(2) : (devoluciones-total).toFixed(2)}</h4>`
              $('#arqueCaja').modal('show');
            })
  }

   document.getElementById('arqueoCaja').addEventListener('click',async function (e) {
      e.preventDefault();
      
      await fetch('/modulo/Facturacion/arqueoCaja')
            .then((res) => res.json())
            .then(({total,devoluciones}) => {
              document.getElementById("totalCaja").innerHTML = `<h4>Ventas: ${total}</h4><br/>
                                                                <h4>Devoluciones: ${devoluciones}</h4><br/>
                                                                <h4>Total de Ventas: ${((devoluciones-total) < 0) ? (Math.abs(devoluciones-total)).toFixed(2) : (devoluciones-total).toFixed(2)}</h4>`
              $('#arqueCaja').modal('show');
              console.log(devoluciones-total)
            })
            
            
      
      
   })



   
</script>

<style>

.select2 {
  display:block;
  height:50px;
  width:450px;
}

</style>
<!--  <script type="text/javascript">
    $(document).ready(function () {
          var url = $('body').data('url');
        $.get(url,function (result) { //nos trae el estatus si la caja esta abierta o cerrada 
          
          if(result.status === 'TRUE'){ //condicion si la caja esta abierta en el momento que se recarge la pagina ella se comprobara 
                                        //si sigue abierta 

            $('h4.cajas').html('Caja Abierta <i class="fa fa-lock" id="abrirCaja"></i><i class="fa fa-lock-open" style="display: none" id="cerrarCaja">'+' '+result.nombreCaja+' '+'</i>')

            ccaja = result.nombreCaja;
            $('#cardFactura').removeClass('disabledbutton');//meter en la otra funcion de ajax
            $('#cardFactura').removeClass('disabledbutton');
            $('#cardCliente').removeClass('disabledbutton');
            $('#opcionesF').removeClass('disabledbutton');
            cajaStatus = true;
            $('#abrirCaja').hide();
            $('#cerrarCaja').show();
            $('#modal-default').modal('hide');
            $('input.caja').val(" ");
            $('input#inputPassword3').val(" ");


          }else{ //aqi se encierra la caja, si se recarga la pagina ella se comprobara si esta cerrada 

            $('#cardFactura').addClass("disabledbutton");
            $('#cardCliente').addClass("disabledbutton");
            $('#opcionesF').addClass("disabledbutton");
            cajaStatus = false;
            $('#abrirCaja').show();
            $('#cerrarCaja').hide();
            $('h4.cajas').html('Caja Cerrada <i class="fa fa-lock" id="abrirCaja"></i><i class="fa fa-lock-open" style="display: none" id="cerrarCaja"></i>')
            $('input.caja').val(" ");
            $('input#inputPassword3').val(" ");
          }


        })
        

        // inicializamos el plugin
        $('select.select2').select2({});
    });
   </script>
 -->