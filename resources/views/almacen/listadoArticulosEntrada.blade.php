@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Listado de Entradas Por Almacen')


@section('content')

@component('layouts.contenth')
    @slot('titulo')
      Listado de Entradas/Salidas
    @endslot
  @endcomponent
    <section class="content">
      <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
              @include('vendor/flash.flash_message')
                <div class="card-header">
                  <h3 class="card-title"><b></b></h3>
                  <div class="card-tools">
                   <div class="row">
                      <div class="col">
                        <a href="{{ route('CreateEntrada') }}" class="btn btn-info"><b>REGISTRAR ENTRADA/SALIDA</b></a>
                      </div>  
                      <div class="col">
                      <form action="{{route('filtroEntradas')}}" method="post">
                            <div class="input-group">
                            @csrf
                            <select name="tipo" class="form-control float-right">
                              <option selected value="0" disabled>Elige un tipo</option>
                              <option value="ENT">ENTRADA</option>
                              <option value="SAL">SALIDA</option>
                            </select>
                            <br>
                            <input type="text" placeholder="Buscar por nombre de Almacen" name="filtro"  class="form-control float-right">
                            
                            <button type="submit" style="height: 38px; width: 38px" class="btn btn-default btn-sm float-left"><i class="fas fa-search"></i></button>
                            <a href="{{route('logins')}}" class="btn btn-default"><i class="fas fa-arrow-left mt-1"></i></a>                       
                          </div>
                          </form>
                      </div>
                   </div><!--row -->
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive text-center">
                  <table class="table table-hover">
                    <thead>
                      <tr style="text-align:center">
                        <th>Código Movimiento</th>
                        <th>Almacén Destino</th>
                        <th>Tipo de Movimiento</th>
                        <th>Descripción Movimiento</th>
                        <th>Fecha</th>
                        <th>Estatus</th>
                        <th>Acción</th>
                      </tr>
                    </thead>
                    <tbody style="text-align:center">
                  @foreach($entradas as $movimiento)
                      @php
                          /* $codenc= Helper::EncriptarDatos($movimiento->codmov); */
                          $codenc= $movimiento->codmov;
                      @endphp
                      <tr style="text-align: center;" data-id="{{$codenc}}" data-type="{{$movimiento->nommov}}">
                        <td>{{$movimiento->codmov}}</td>
                        <td>{{$movimiento->almacenes == null ? 'N/A' : $movimiento->almacenes->nomalm}}</td>
                        <td>{{$movimiento->movimiento}}</td>

                        @if($movimiento->movimientos)
                        <td>{{$movimiento->movimientos->destipent}}</td>
                        @else
                        <td>N/D</td>
                        @endif
                        <td>{{$movimiento->fecrcpnormal}}</td>
                        <td>
                          @if ($movimiento->status === 'E')
                              <span class="badge bg-info">ESPERA</span>
                            @elseif ($movimiento->status === 'APR')
                              <span class="badge bg-success">APROBADO</span>
                            @elseif ($movimiento->status === 'ANL')
                              <span class="badge bg-danger">ANULADO</span>
                          @endif
                        </td>
                        <td>
                          
                          @if ($movimiento->status === 'E')
                            
                          {{-- <a class="sendPdf" href="" data-toggle="tooltip" data-placement="top" title="Generar PDF">
                            <i class="far fa-file-pdf"></i>
                          </a> --}}
                            {{-- /
                            <a class="sendMail" href="{{route('entradaMail',$codenc)}}" data-toggle="tooltip" data-placement="top" title="Enviar Correo">
                              <i class="fas fa-paper-plane"></i>
                            </a> --}}
                            <a target="_blank" href="#" class="sendPdf" data-toggle="tooltip" data-placement="top" title="Generar PDF" >
                              <i class="far fa-file-pdf"></i>
                            </a>
                            /
                            <a class="aprobar"  href="{{route('aprobMov',$codenc)}}">
                              <i class="fa fa-check green"></i>
                            </a>
                            @can('delete',App\permission_user::class)
                            /
                            <a class="anular"  href="{{route('anularMov',$codenc)}}">
                              <i class="fa fa-times red"></i>
                            </a>
                            @endcan
                            /
                            <span data-toggle="modal" data-target="#modal-detalle" class="modaltrigger">
                              <a class="detalle"   href="{{route('EntSalDetalle',$codenc)}}" data-toggle="tooltip" data-placement="top" title="Detalles de Movimiento" data-toggle="tooltip" data-placement="top">
                                <i class="fa fa-list "></i>
                              </a>
                            </span>
                          @elseif ($movimiento->status === 'APR')
                          <a target="_blank" href="#" class="sendPdf" data-toggle="tooltip" data-placement="top" title="Generar PDF" >
                            <i class="far fa-file-pdf"></i>
                          </a>
                            /
                            <span data-toggle="modal" data-target="#modal-detalle" class="modaltrigger">
                              <a class="detalle"  href="{{route('EntSalDetalle',$codenc)}}" data-toggle="tooltip" data-placement="top" title="Detalles de Movimiento" data-toggle="tooltip" data-placement="top">
                                <i class="fa fa-list "></i>
                              </a>
                            </span>
                            @elseif ($movimiento->status === 'ANL')
                            <a target="_blank" href="#" class="sendPdf" data-toggle="tooltip" data-placement="top" title="Generar PDF" >
                              <i class="far fa-file-pdf"></i>
                            </a>
                              /

                            <span data-toggle="modal" data-target="#modal-detalle" class="modaltrigger">
                              <a class="detalle"  href="{{route('EntSalDetalle',$codenc)}}" data-toggle="tooltip" data-placement="top" title="Detalles de Movimiento" data-toggle="tooltip" data-placement="top">
                                <i class="fa fa-list "></i>
                              </a>
                            </span>
                          @endif
                        </td>
                      </tr>
                    
                    @endforeach 
                    </tbody>
                  </table>
                  {!!$entradas->appends(request()->input())->render()!!}
                  {{-- col8  --}}
            </div>
            {{-- <div>
              <a href="{{ url()->previous() }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
            </div> --}}
            </div>
          </div>
      </div>
        {{-- row --}}


    </div>
    <div class="modal fade" id="modal-detalle" style="display: none;" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content" style = "width: 1000px">
          <div class="modal-header">
            <h4 class="modal-title">Detalle de Movimiento</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="divalert">

            </div>
            <table class="table">
              <thead class="thead-dark">
                <th>Articulo</th>
                <th>Descripcion</th>
                <th>Almacen Origen</th>
                <th>Color</th>
                {{-- <th>Talla</th> --}}
                <th>Cantidad Movilizada</th>
                <th>Seriales</th>
                <th>Precio Actual</th>
              </thead>
              <tbody class="detalles" id="detalestable">

              </tbody>
            </table>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            {{-- traspasosAlmacenAnul --}}
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>

    {{-- container fluid --}}

    <form id="sendPdf" target="_blank" method="POST" action="{{route('generarReporteEntrada')}}" style="display:none">
      @csrf
        <input id="codfrom" type="text" name="codfrom">
        <input id="codto" type="text" name="codto">
        <input id="diames" type="text" name="diames">
        <input id="tipmov" type="text" name="tipmov">
    </form>
  </section>


@endsection
@section('lista')
  
<script>
  
  $(function () {
  submitPdf()

  function submitPdf() {
    $(document).on('click','.sendPdf',function (e) {
      e.preventDefault();
      let valor = $(this).closest('tr').data('id')
        $('input#codfrom').val(valor)
        $('input#codto').val(valor)
        $('input#diames').val('byCod')
        $('input#tipmov').val($(this).closest('tr').data('type'))
        
        $('#sendPdf').submit()
    })
  }

  $('[data-toggle="tooltip"]').tooltip()
  getDetalle()

  function getDetalle(){
      $(document).on('click','.detalle',function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        console.log(url);
        $('#detalestable').html('')
          $('.divalert').html('')
          $.get(url,function(result){
            if(result){
              console.log(result)

                result.articulos ? $('#detalestable').append(result.articulos) : toastError("No se pudieron Cargar los articulos por favor recargue la pagina")
                result.detalle ? $('.divalert').append(result.detalle) : toastError("No se pudo cargar el detalle")
              setTimeout(() => {
                $('.modal-dialog .overlay').hide();
                //hideDiv()
              }, 10);  
            }else{
              toastError("No se pudo cargar el betamax")
            }
          })
      })
    }

  function changeStatus(row,type) {//Funcion cambia el estado de la transaccion o traspaso sin recargar la pagina
      let mot = type === 'A' ?  '<span class="badge bg-success">APROBADO</span>' : '<span class="badge bg-danger">ANULADO</span>'
      let td = $(row).children().last();//el ultimo hijo del tr es decir en donde estan los enlaces
      let prev = $(td).prev();//el ultimo hijo del tr es decir en donde estan los enlaces
      let span = $(td).children().last()//ultimo hijo del td que es el enlace del modal
      $(prev).html(mot)
      let pdf = type === 'A' ? '<a href="" data-toggle="tooltip" data-placement="top" title="" class="sendPdf" data-original-title="Generar PDF"><i class="far fa-file-pdf"></i></a> ' : '';
      $(td).html(/* mot+' / '+ */
      pdf+
      '/ <span data-toggle="modal" data-target="#modal-detalle" class="modaltrigger">'+$(span).html()+'</span>')
      //toolTip()
    }

  $(document).ready(function() {
  
    $('.aprobar').click(function(e){
          e.preventDefault();
          var url = $(this).attr('href');
          var row  = $(this).closest('tr')
          Swal.fire({
          title: 'Seguro que Desea Aprobar?',
          text: "Esta accion no se puede revertir",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si!, aprobar'
        }).then((result) => {
          if (result.value) {
            $.get(url,function(result){
              if(result.exito){
                changeStatus(row,'A')
                Swal.fire(
                  'Movimiento Aprobado!',
                  'Con Exito.',
                  'success',
                )
              }else if(result.error){
                Swal.fire(
                  'Error en ejecucion',
                    result.error,
                  'error',
                )
              }
            })
           }else{
            Swal.fire({
            icon: 'warning',
            title: 'Accion Cancelada',
            //footer: '<a href>Why do I have this issue?</a>'
            timer:1500
          })
           }
          
        });

    });
});

$(document).ready(function () {
  $('.anular').click(function (e) {
    e.preventDefault()
  var url = $(this).attr('href');
  var row  = $(this).closest('tr')

  Swal.fire({
  title: 'Especifique el Motivo de Anulacion',
  input: 'text',
  inputAttributes: {
    autocapitalize: 'off'
  },
  showCancelButton: true,
  confirmButtonText: 'Anular',
  showLoaderOnConfirm: true,
  preConfirm: (desanu) => {
    if(desanu.length=== 0) {
      Swal.showValidationMessage('Debe Introducir el motivo de anulacion')
      return
      }
      return fetch(url+'/'+desanu)
      .then(response => {
        return response.json()
      })
      .catch(error => {
        Swal.showValidationMessage(
          `Fallo la Anulacion: ${error}`
        )
      })
  },
  allowOutsideClick: () => !Swal.isLoading()
}).then((result) => {
  if (result.value.exito) {
    changeStatus(row,'R')
    Swal.fire(
      'Movimiento Anulado!',
      'Con Exito.',
      'success',
    )
  }else{
    Swal.fire(
      'Error en ejecucion',
        result.value.error,
      'error',
    )
  }
  })
})
})



$('.sendMail').click(function(e){
    e.preventDefault();
      var url = $(this).attr('href');
      Swal.fire({
      title: 'Seguro que desea Reenviar el Correo ?',
      text: "Esta accion no se puede revertir",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si!, Enviar'
    }).then((result) => {
      if (result.value) {
        $.get(url,function(result){
          if(result.exito){
            Swal.fire(
              'Correo Enviado Exitosamente!',
              'Con Exito. ('+result.correo+')',
              'success',
            )
          }else if(result.error){
            Swal.fire(
              'Error en ejecucion',
                result.error,
              'error',
            )
          }
        })//get
      }else{
          Swal.fire({
          icon: 'warning',
          title: 'Accion Cancelada',
          //footer: '<a href>Why do I have this issue?</a>'
          timer:1500
        })//Swal
      }
    });//Then
});//function evento




})

</script>
@endsection
