@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Notas de Entrega')
@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Lista de Notas de Entrega
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">
                  <div class="col-sm-4 mt-2">

                    @can('create', App\permission_user::class)
                      <a href="{{route('nuevaNotaEntrega')}}" class="btn btn-info"><b>REGISTRAR</b></a>

                  </div>
                    @endcan

                <div class="col mt-2">
                  <form action="{{route('buscarNota')}}" method="GET">
                    @csrf
                     <div class="input-group">
                      <input type="text" name="filtro" placeholder="Buscar " class="form-control float-right">
                      <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Buscar"><i class="fas fa-search"></i></button>
                      <a href="{{URL::previous()}}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                       
                     </div>
                  </form>
                  
                </div><!--col-->
                 </div><!--row -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>N° NOTA</th>
                      <th>Almac&eacute;n Origen</th>
                      <th>Destino</th>
                      <th>Creado Por</th>
                      <th>Creado El</th>
                      <th>Acci&oacute;n</th>
                    </tr>
                  </thead>
                  <tbody style="text-align:center"> 
                    @foreach($notas as $nota)
                    @php
                    $rand  = rand(1, 9999);
                    $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                    $enc=Helper::EncriptarDatos($nrand.'-'.$nota->id);
                    @endphp
                    <tr>
                      <td>{{ $nota->numero }}</td>
                      <td>{{ $nota->almorigen->nomalm  ?? '' }}</td>
                      @if($nota->almdestino)
                      <td>{{ $nota->almdestino->nomalm ?? ''}}</td>
                      @else
                      <td>{{$nota->cliente->nompro ?? ''}}</td>
                      @endif
                      <td>{{ $nota->autor->nomuse ?? ''}}</td>
                      <td>{{ $nota->created_at }}</td>
                      <td class="acciones"> 
                        <a href="{{route('detalleNota',$enc)}}">
                          <i class="fa fa-eye blue" title="Detalles"></i>
                        </a>
                        @if(!$nota->extraido)
                        /
                       <a href="{{route('descontarArt')}}" id="descontar" data-target-id="{{$enc}}"><i class="fa fa-box blue" title="Descontar Inventario"></i>
                       </a>
                       @else
                       /
                        <a href="{{route('pdfNota',$enc)}}">
                          <i class="fa fa-file blue" title="Detalles"></i>
                        </a>
                       @endif
                        
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!!$notas->appends(request()->input())->render()!!}
                {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section> 
  
  
@endsection
@section('script')
<!-- <script src="{{asset('js/factura.js')}} "></script> -->
<script src="{{asset('js/notaEntrega.js')}} "></script>
@endsection




