@extends('layouts.app')
@extends('layouts.menu')
  @section('content')

    @component('layouts.contenth')
    @slot('titulo')
      Movimiento de Almacen
    @endslot
  @endcomponent
    <section class="content-wrapper">
      <div class="container-fluid">
        <div class="container">
          <div class="col-10">
            <div class="card card-info">
              @if($accion=="registro")
                <div class="card-header">
                    <h3 class="card-title"><b>REGISTRO TIPO DE MOVIEMIENTO</b></h3>  
                </div>
                <form action="{{route('registrarTdeMovimiento')}}" id="saveForm" method="post">
                  {{csrf_field()}}
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label>Descripción</label>
                            <div class="input-group mb-3">
                              <input type="text" class="form-control" class="@error('descripcion') is-invalid @enderror" placeholder="Ejem: Recepción" name="descripcion" id="descripcion" onkeyup="pasarMayusculas(this.value, this.id)" maxlength="100" minlength="2">
                            </div>
                            @error('descripcion')
                                <div class="alert alert-danger">{!!$message!!}</div>
                            @enderror
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="card-footer">
                    <button type="button" id="boton" class="btn btn-success">Guardar Cambios</button>
                    <a href="{{route('movimientos.inventario.almacen')}}" class="btn btn-default retorno" ><i class="fa fa-chevron-circle-left"></i> Volver</a>
                  </div>
                </form>
              @elseif($accion=="edicion")
                <div class="card-header">
                  <h3 class="card-title"><b>EDICIÓN TIPO DE MOVIMIENTO</b></h3>
                </div>
                @php
                  $id= $tipomovimiento->id.'|'.$tipomovimiento->codigoid;
                @endphp
                <form action="{{route('actualizartipmovimiento',$id)}}" method="POST">
                  {{csrf_field()}}
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-2">
                        <div class="form group">
                          <label for="id">ID</label>
                          <div class="input-group mb-3">
                              <input type="text" class="form-control" disabled=true value="{{$tipomovimiento->id}}" class="@error('id') is-invalid @enderror" name="id" id="id">
                          </div>
                          @error('id')
                            <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                        </div>
                      </div>

                      <div class="col-sm-10">
                        <div class="form-group">
                          <label>Descripción</label>
                          <div class="input-group mb-3">
                            <input type="text" class="form-control" class="@error('descripcion') is-invalid @enderror"
                     name="descripcion" id="descripcion" value="{{$tipomovimiento->destipent}}" onkeyup="pasarMayusculas(this.value, this.id)" maxlength="100" minlength="2">
                          </div>
                          @error('descripcion')
                        <div class="alert alert-danger">{!!$message!!}</div>
                        @enderror
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="card-footer">
                        <button type="submit"  class="btn btn-success">Guardar Cambios</button>
                        <a href="{{route('movimientos.inventario.almacen')}}" class="btn btn-default retorno" ><i class="fa fa-chevron-circle-left"></i> Volver</a>
                  </div>
                </form>
              @endif
            </div>
          </div>
        </div>
      </div>
    </section>
  @endsection
    
  @section('script')
  <script src="{{asset('js/chequeo.js')}} "></script>
  <script>

    document.getElementById("boton").addEventListener("click",function(){

      console.log("llegooton")

      $("#boton").attr("disabled", true);

      setTimeout(() => {
        
        document.getElementById('saveForm').submit();
      }, 1000);
    })

  </script>
  @endsection