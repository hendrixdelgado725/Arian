
@extends('layouts.app')
@extends('layouts.menu')
@if($accion == 'edicion')
@section('titulo','Edición Escala')
@else
@section('titulo','Registro Escala')
@endif
  @section('content')

    @component('layouts.contenth')
    @slot('titulo')
     Registro de Edicion Escala
    @endslot
  @endcomponent

  <section class="content-wrapper">
  	<div class="container-fluid">
  		<div class="container">
  			<div class="col-10">
  				<div class="card card-info">

  					@if($accion=="registro")

  						<div class="card-header">
  							<h3 class="card-title"><b>REGISTRO ESCALA</b></h3>
  						</div>
  						<form action="{{route('registrarEscala')}}" method="post">
  							{{csrf_field()}}
  							<div class="card-body">
                  @include('vendor/flash.flash_message')
  								<div class="row">

  									<div class="col-sm-6">
  										<div class="form-group">
  											<label>Referencia</label>
  											<div class="input-group mb-3">
  												<div class="input-group-prepend">
						                    	<span class="input-group-text">A-1</span>
					                 			</div>
  												<input type="text" class="form-control" placeholder="Ejem: AF1567" name="referencia" 
						                  id="referencia"  onkeyup="pasarMayusculas(this.value, this.id)" class="@error('referencia') is-invalid @enderror" >
						                	</div>
							                @error('referencia')
							                  <div class="alert alert-danger">{!!$message!!}</div>
							                @enderror
  											</div>
  										</div>

                      <div class="col-sm-12">
                        <div class="form group">
                          <label>Nombre</label>
                          <div class="input-group mb-3">
                        <div class="input-group-prepend">
                              <span class="input-group-text">A</span>
                          </div>
                            <input type="text" class="form-control" placeholder="" name="nombre" 
                          id="nombre" onkeyup="pasarMayusculas(this.value, this.id)" class="@error('nombre') is-invalid @enderror">
                          </div>
                          @error('nombre')
                            <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                        </div>
                      </div>

                      <div class="col-sm-12">
                        <div class="form-group">
                          <label>Artículo</label>
                            <div class="input-group mb-3">
                              <select style="width:100%" class="form-control select2" id="articulo" name="articulo" tabindex="-1" onchange="actualizaTallas(this.id,1,'div_tallas_')" required="required" value="">
                                <option selected="selected" data-select2-id="" disabled>Seleccione Artículo Asociar</option>
                                @foreach ($articulo as $art)
                                  <option value="{{$art->codart}}">{{$art->desart}}</option>
                                @endforeach
                                </select>
                            </div>
                            @error('articulo')
                            <div class="alert alert-danger">{!!$message!!}</div>
                            @enderror
                        </div>
                      </div>

                      <div class="col-sm-12">
                        <div class="form-group">
                          <label>Asociación</label>
                          <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table class="table table-bordered ">
                              <thead>
                                <th>Cod.</th>
                                <th>Talla</th>
                                <th>Cantidad</th>
                              </thead>
                              <tbody>
                                @for ($i = 0; $i < 12; $i++)
                                  <tr data-row="{{$i}}">
                                    <td>
                                      <div id="div_tallas_oc{{'_'.$i}}"></div>
                                      {{--$i--}} 
                                      <input type="hidden" class="form-control codtalla" name="div_tallas_hd_{{$i}}" id="div_tallas_hd_{{$i}}">
                                    </td>
                                    <td width="100%" name="tallas">
                                      <div id="div_tallas{{'_'.$i}}">
                                      </div>
                                    </td>
                                    <td>
                                      <div id="div_tallas_ct{{'_'.$i}}">
                                      </div>
                                    </td>
                                  </tr>
                                @endfor
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>

  									</div>
  								</div>
                  <dir>
                    <input type="hidden" name="datos" id="datos">  
                  </dir>
                  
                  <div class="card-footer">
                  <button type="submit" class="btn btn-success" class="verificarpasswd">Guardar Cambios</button>
                  <a href="{{route('listaEscala')}}" class="btn btn-default retorno" ><i class="fa fa-chevron-circle-left"></i> Volver</a>
                </div>
  							</div>
  						</form>

  					@elseif($accion=="edicion")
              <div class="card-header">
                <h3 class="card-title"><b>EDICIÓN ESCALA</b></h3>
              </div>
              @php
                $id=$escala->id.'|'.$escala->codigoid;
              @endphp

              <form action="{{route('actualizarEscala',$id)}}" method="POST">
                {{csrf_field()}}
                <div class="card-body">
                  <div class="row">

                    <div class="col-sm-2">
                      <div class="form group">
                        <label for="id">ID</label>
                        <div class="input-group mb-3">
                          <input type="text" class="form-control" disabled=true value="{{$escala->id}}" class="@error('escala') is-invalid @enderror" name="escala" id="escala">
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <div class="form group">
                        <label>Referencia</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                              <span class="input-group-text">A-1</span>
                          </div>
                          <input type="text" class="form-control" disabled=true value="{{$escala->codesc}}" class="@error('codesc') is-invalid @enderror" name="codesc" id="codesc">
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-12">
                      <div class="form group">
                        <label>Nombre</label>
                        <div class="input-group mb-3">
                      <div class="input-group-prepend">
                            <span class="input-group-text">A</span>
                        </div>
                          <input type="text" class="form-control" placeholder="" name="nombre" 
                        id="nombre" onkeyup="pasarMayusculas(this.value, this.id)" value="{{$escala->nomesc}}" class="@error('nombre') is-invalid @enderror">
                        </div>
                        @error('nombre')
                          <div class="alert alert-danger">{!!$message!!}</div>
                        @enderror
                      </div>
                    </div>

                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Artículo</label>
                          <div class="input-group mb-3">
                            <select style="width:100%" class="form-control select2" id="articulo" name="articulo" tabindex="-1" onchange="actualizaTallas(this.id,1,'div_tallas_')" required="required" value="{{$escala->codart}}">                              
                                <option value="{{$escala->codart}}">{{$escala->getArticulo()->desart}}</option>                              
                              </select>
                          </div>
                          @error('articulo')
                          <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                      </div>
                    </div>

                    <div class="col-sm-12">
                      <div class="form-group">
                        <label>Asociación</label>
                          <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table class="table table-bordered ">
                              <thead>
                                <th>Cod.</th>
                                <th>Talla</th>
                                <th>Cantidad</th>
                                <th>Desactivar</th>
                              </thead>
                              <tbody>
                                @php $i=0; @endphp
                                @foreach($detescala as $det)
                                {{--@php dd($det); @endphp--}}
                                  <tr data-row="{{$i}}">
                                    <td>
                                      </br>
                                      <label>{{$det->codtallas}}</label>
                                      <div>
                                        <input type="hidden" class="form-control id" name="div_tallas_oc{{'_'.$i}}" id="div_tallas_oc{{'_'.$i}}" value="{{$det->codtallas}}">
                                      </div>
                                    </td>
                                    <td>
                                      <input type="text" class="form-control tallas" disabled name="div_tallas{{'_'.$i}}" id="div_tallas{{'_'.$i}}" class="form-control" value="{{$det->getDescTalla()->tallas}}">
                                    </td>
                                    <td>
                                      <input type="text" class="form-control cantidad" name="div_tallas_ct{{'_'.$i}}" id="div_tallas_ct{{'_'.$i}}" class="form-control" onKeypress="javascript:return SoloNumeros(event)" value="{{$det->cantidad}}">
                                    </td>
                                    <td style="text-align:center">
                                      <a id="deleterowtalla" class="deleterowtalla" data-target-artid="{{$det->id}}" href=""><i class="fa fa-trash"></i></a>
                                    </td>
                                  </tr>
                                  @php $i++; @endphp
                                @endforeach
                                @if(count($tallsinasociar)>0)
                                  @foreach($tallsinasociar as $noasic)
                                    <tr data-row="{{$i}}">
                                      <td>
                                        </br>
                                        <label>{{$noasic[0]->codtallas}}</label>
                                        <div>
                                        <input type="hidden" name="div_tallas_oc{{'_'.$i}}" id="div_tallas_oc{{'_'.$i}}" value="{{$noasic[0]->codtallas}}">
                                      </div>
                                      </td>
                                      <td>
                                        <input type="text" disabled name="div_tallas{{'_'.$i}}" id="div_tallas{{'_'.$i}}" class="form-control" value="{{$noasic[0]->tallas}}">
                                      </td>
                                      <td>
                                        <input type="text" name="div_tallas_ct{{'_'.$i}}" id="div_tallas_ct{{'_'.$i}}" class="form-control" onKeypress="javascript:return SoloNumeros(event)" >
                                      </td>
                                      <td>{{--boton eliminar--}}</td>
                                    </tr>
                                  @php $i++; @endphp
                                  @endforeach
                                @endif
                              </tbody>
                            </table>
                          </div>
                      </div>
                    </div>

                  </div>
                </div>

                <div>
                  <input id="borrados" type="hidden" name="borrados">
                </div>

                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Guardar Cambios</button>
                  <a href="{{route('listaEscala')}}" class="btn btn-default retorno" ><i class="fa fa-chevron-circle-left"></i> Volver</a>
                </div>
              </form>

  					  @endif

  				</div>
  			</div>
  		</div>
  	</div>
  </section>


  @endsection
@section('script')
<script type="text/javascript">
  $(document).ready(function () {
      // inicializamos el plugin
      $('select.select2').select2({
      });
  });
</script>
<script src="{{asset('js/chequeo.js')}} "></script>
<script src="{{asset('js/funciones.js')}} "></script>
<style>
.my-custom-scrollbar {
position: relative;
height: 250px;
overflow: auto;
}
.table-wrapper-scroll-y {
display: block;
}
</style>
@endsection