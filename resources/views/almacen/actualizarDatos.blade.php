@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Editar Almacen')
  @section('content')
  @component('layouts.contenth')
    @slot('titulo')
      <!-- EDICI&Oacute;N ALMAC&Eacute;N -->
    @endslot
  @endcomponent

<section class="content-wrapper">
    <div class="content-fluid">

    <div class="container">

        
        <div class="col-sm-10">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">  EDICIÓN ALMACÉN</h3>

              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
              <form action="{{route('Update.Almacen',$almacen->id)}}" method="POST">
              {{csrf_field()}}
                <div class="card-body">
                 <div class="row">
                  <div class="col-sm-2">
                    <div class="form group">
                      <label for="id">ID</label>
                      <div class="input-group mb-3">
                        <input type="text" class="form-control" disabled=true value="{{$almacen->id}}" class="@error('codigo') is-invalid @enderror" name="codigo" id="codigo">
                      </div>
                    </div>
                  </div>
                 <!-- <div class="col-sm-6">
                <div class="form-group">
                <label>Numero de Almacen</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">cod</span>
                  </div>
                  <input type="text" class="form-control" value="{{ $almacen->codalm }}" placeholder="EJ:000001" name="codigo" 
                  id="cedularifcliente" class="@error('codigo') is-invalid @enderror">
                </div>
                @error('codigo')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
               </div>	 -->
                 
                 <div class="col-sm-10">
                    <div class="form group">
                    <label for="nombre">Descripción</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Aa</span>
                      </div>
                      <input type="text" class="form-control" class="@error('Descripcion') is-invalid @enderror"  name="Descripcion" id="DescriPAlmacen" value="{{ $almacen->nomalm }}" onkeyup="pasarMayusculas(this.value, this.id)" >
                    </div>
                    @error('Descripcion')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                 </div>

                 <div class="col-sm-12">
                  <div class="form group">
                      <label for="direccion">Dirección</label>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Aa</span>
                    </div>
                    <textarea  type="text" class="form-control" class="@error('direccion') is-invalid @enderror"  name="direccion" id="TipoAlmacen"  onkeyup="pasarMayusculas(this.value, this.id)">{{ $almacen->diralm }}</textarea>
                  </div>
                  @error('direccion')
                      <div class="alert alert-danger">{!!$message!!}</div>
                  @enderror
                  </div> 
                </div>
                
                
                <div class="col-sm-12">
                  <div class="form-group">
                    <label >Sucursal Asociada</label>
                      <div class="input-group mb-3">
                        <select class="form-control select2" value="{{$almacen->codsuc_asoc}}"  style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="sucu" required="">
                
                          @foreach ($sucursal as $suc)
                          
                              <option value="{{$suc->codsuc}}" {{$almacen->codsuc_asoc === $suc['codsuc'] ? 'selected' : ''}}>{{ $suc["nomsucu"]}}</option>
                            @endforeach
                        </select>
                      </div>
                    </div>
                </div>

                <div class="col-sm-12">
                  <div class="form-group">
                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                    <input type="checkbox" class="custom-control-input" id="customSwitch3" {{$almacen->pfactur === true ? 'checked' : ''}} name="pfactur">
                      <label class="custom-control-label" for="customSwitch3">Almacén predeterminado para Facturar : {{$almacen->pfactur === true ? 'SI' : 'NO'}}</label>
                    </div>
                  </div>
                </div>

               </div>

                

                <!-- <div class="form group">
                    <label for="direccion">Tipo</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Tipo</span>
                  </div>
                  <input value="{{ $almacen->codcat }}" type="text" class="form-control" class="@error('tipo') is-invalid @enderror"  name="codcat" id="TipoAlmacen">
                </div>
                </div>
                @error('tipo')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror -->
                           
                </div>
                
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Guardar Cambios</button>
                   <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-chevron-circle-left"></i> Atras</a>
                </div>
              </form>

            </div>
                    
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->

            
         </form>
        </div>
            <!-- /.card -->
       </div>
      </div>
    </div>
    </section>
  @endsection

  @section('script')
  <script src="{{asset('js/chequeo.js')}} "></script>
  
  <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
    
        $('#customSwitch3').on('change',function(){
          $('.custom-control-label').html('')
          const opt = document.getElementById("customSwitch3").checked === true ? 'Si' : 'No';
          $('.custom-control-label').html('Almacen Predeterminado Para Facturar : '+opt)
        })
    });
   </script>
@endsection
    
