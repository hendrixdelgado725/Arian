@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Registro Nota Entrega')

  @section('content')
<section class="content">
  <div class="container-fluid">
    <nota-almacen :clientes="{{json_encode($clientes)}}" :almacenes="{{json_encode($almacenes)}}"></nota-almacen>
  </div>
</div>
</div> 

<input id="getarts" style="display:none"  data-href="{{route('getArts')}}">

</div> 
  @endsection
  @section('script')
   <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
    });
   </script>
  <script src="{{asset('js/chequeo.js')}} "></script>
  <script src="{{asset('js/presupuesto.js')}} "></script>
    <style>
     .my-custom-scrollbar {
      position: relative;
      height: 300px;
      overflow: auto;
       }
     .table-wrapper-scroll-y {
     display: block;
       }
     </style>

  @endsection