@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Registrar Ubicación')
@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      <!-- Ubicacion de almacen -->
    @endslot
  @endcomponent
  {{-- CONTENT --}}
  <section class="content">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-8">
          
          <div class="card card-info">
            @include('vendor/flash.flash_message')
            <div class="card-header">
            <h3 class="card-title"><b>REGISTRO UBICACI&Oacute;N</b></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{route('ubiSave')}}" method="post">
              {{csrf_field()}}
              <div class="card-body">

              <div class="form group">
                <label for="nomubi">Nombre Ubicaci&oacute;n</label>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">ABC</span>
                    </div>
                    <input type="text" class="form-control" name="nomubi" id="nomubi" onkeyup="pasarMayusculas(this.value, this.id)" class="@error('nomubi') is-invalid @enderror" value="{{old('nomubi')}}">
                  </div>
                  @error('nomubi')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
              </div>
              <div class="form group">
                <label for="codalm">Almac&eacute;n</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <!-- <span class="input-group-text"></span> -->
                  </div>
                  <select class="form-control select2"  name="codalm" 
                  id="codalm" class="@error('codalm') is-invalid @enderror">
                    @foreach($almacenes as $key)
                      @php
                        $selected = old('codalm') === $key->id ? 'selected' : '';
                      @endphp
                      <option value="{{$key->codalm}}" {{$selected}}>{{ $key->nomalm }}</option>
                    @endforeach
                </select>
                </div>
                @error('codalm')
                <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
              </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button id="createUbi" type="submit" class="btn btn-success"><b>Guardar Cambios</b></button>
              <a href="{{ url()->previous() }}" class="btn btn-default ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
            </div>
            </form>
            </div>
            <!-- /.card -->
            </div>
        </div>
      </div>
        {{-- row --}}
    </div>
    {{-- container fluid --}}
  </section>
@endsection
@section('script')
    {{-- Script jquery --}}
<script>
      $(document).ready(()=>{
        
      })

</script>
<script src="{{asset('js/chequeo.js')}} "></script>
<script type="text/javascript">
  $(document).ready(function () {
      // inicializamos el plugin
      $('select.select2').select2({
      });
  });
</script>
@endsection