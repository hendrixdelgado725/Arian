@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Listado Traspasos Almacen')


@section('content')

@component('layouts.contenth')
    @slot('titulo')
      Listado de Traspasos
    @endslot
  @endcomponent
    <section class="content">
      <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
              @include('vendor/flash.flash_message')
                <div class="card-header">
                  <h3 class="card-title"><b></b></h3>
                  <div class="card-tools">
                   <div class="row">
                      <div class="col">
                        {{-- @can('create',App\permission_user::class)  --}}
                        <a href="{{ route('traspasosAlmacenCreate') }}" class="btn btn-info"><b>REGISTRAR TRASPASO</b></a>
                        {{-- @endcan --}}
                      </div>  

                      <div class="col">
                          <form action="" method="post">
                            <div class="input-group">
                            @csrf
                            <input type="text" placeholder="Buscar por nombre de Almacen" name="filtro"  class="form-control float-right">
                            <button type="submit" style="height: 38px; width: 38px" class="btn btn-default btn-sm float-left"><i class="fas fa-search"></i></button>
                            <a href="{{route('traspasosAlmacen')}}" class="btn btn-default"><i class="fas fa-arrow-left mt-1"></i></a>                       
                          </div>
                          </form>
                      </div>
                   </div><!--row -->
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive text-center">
                  <table class="table table-hover">
                    <thead>
                      <tr style="text-align:center">
                        <th>Codigo Movimiento</th>
                        <th>Almacen Origen</th>
                        <th>Almacen Destino</th>
                        <th>Fecha</th>
                        <th>Estatus</th>
                        <th>Acción</th>
                      </tr>
                    </thead>
                    <tbody style="text-align:center">
                  @foreach($traspasos as $trasp)
                     @php
                          /* $codtra= Helper::EncriptarDatos($trasp->codtra); */
                          $codtra = $trasp->codtra
                      @endphp        
                      <tr style="text-align: center;" data-id={{$trasp->codtra}}>
                        <td>{{$trasp->codtra}}</td>
                        <td>{{$trasp->almorides->nomalm ?? '' }}</td> 
                        <td>{{$trasp->almdesdes->nomalm ?? '' }}</td>
                        <td>{{$trasp->fecha}}</td>
                        <td>
                          @if ($trasp->statra === 'E')
                              <span class="badge bg-info">EN ESPERA</span>
                          @elseif ($trasp->statra === 'A')
                              <span class="badge bg-success">APROBADO</span> 
                          @elseif ($trasp->statra === 'R')
                              <span class="badge bg-danger">ANULADO</span>
                          @endif  
                        </td>
                        <td>
                          @if ($trasp->statra === 'E')
                            <a class="sendPdf" href="_blank" data-toggle="tooltip" data-placement="top" title="Generar PDF">
                              <i class="far fa-file-pdf"></i>
                            </a>
                            /
                            <a class="sendMail" href="{{route('sendMail',$codtra)}}" data-toggle="tooltip" data-placement="top" title="Enviar Correo">
                              <i class="fas fa-paper-plane"></i>
                            </a>
                            /
                            <a class="aprobar"  href="{{route('traspasosAlmacenAprob',$codtra)}}" data-toggle="tooltip" data-placement="top" title=" Hacer Recepcion">
                              <i class="fa fa-check green"></i>
                            </a>
                            /
                            @can('delete',App\permission_user::class) 
                            <a class="anular"  href="{{route('traspasosAlmacenAnul',$codtra)}}" data-cod={{$codtra}}  data-toggle="tooltip" data-placement="top" title="Anular Traspaso">
                              <i class="fa fa-times red"></i>
                            </a>
                            /
                            @endcan
                            <span data-toggle="modal" data-target="#modal-detalle" class="modaltrigger">
                              <a class="detalle"  href="{{route('trgetDetalle',$codtra)}}" data-toggle="tooltip" data-placement="top" title="Detalles de Traspaso" data-toggle="tooltip" data-placement="top">
                                <i class="fa fa-list "></i>
                              </a> 
                            </span>
                          @elseif ($trasp->statra === 'A')
                          <a href="_blank" data-toggle="tooltip" data-placement="top" title="Generar PDF">
                            <i class="fas fa-pdf"></i>
                          </a>
                          /
                          <span data-toggle="modal" data-target="#modal-detalle" class="modaltrigger">
                            <a class="detalle"  href="{{route('trgetDetalle',$codtra)}}">
                              <i class="fa fa-list green"></i>
                            </a>
                          </span>
                            @elseif ($trasp->statra === 'R')
                            <span data-toggle="modal" data-target="#modal-detalle" class="modaltrigger">
                              <a href="{{route('trgetDetalle',$codtra)}}" class="detalle">
                                <i class="fa fa-list " data-toggle="tooltip" data-placement="top" title="Ver Motivo de Anulacion"></i>
                              </a>
                            </span>
                            @endif
                        </td>
                      </tr>
                    @endforeach 
                    </tbody>
                  </table>
                  {!!$traspasos->appends(request()->input())->render()!!}

                  {{-- col8  --}}
  
            </div>
            {{-- <div>
              <a href="{{ url()->previous() }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
            </div> --}}
            </div>
            
          </div>
      </div>
        {{-- row --}}
    </div>
    {{-- container fluid --}}
        <div class="modal fade" id="modal-detalle" style="display: none;" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Detalle de Traspaso</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="divalert">

                </div>
                <table class="table">
                  <thead class="thead-dark">
                    <th>Articulo</th>
                    <th>Descripcion</th>
                    <th>Color</th>
                    <th>Almacen Origen</th>
                    <th>Almacen Destino</th>
                    <th>Talla</th>
                    <th>Cantidad Movilizada</th>
                  </thead>
                  <tbody class="detalles" id="detalestable">

                  </tbody>
                </table>
              </div>
              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                {{-- traspasosAlmacenAnul --}}
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

      <form id="sendPdf" method="POST" action="{{route('generarReporteTraspaso')}}" style="display:none">
        @csrf
          <input id="codfrom" type="text" name="codfrom">
          <input id="codto" type="text" name="codto">
          <input id="diames" type="text" name="diames">
      </form>

    </section>


@endsection
@section('script')
{{-- <script src="{{asset('js/traspasos.js')}} "></script> --}}
<script>
  $(function () {
    toolTip()
    getDetalle();
    aprobar();
    anular();
    sendMsj()
    submitPdf()

    function submitPdf() {
      $(document).on('click','.sendPdf',function (e) {
        e.preventDefault();
        let valor = $(this).closest('tr').data('id')
          $('input#codfrom').val(valor)
        $('input#codto').val(valor)
        $('input#diames').val('byCod')
        $('#sendPdf').submit()
      })
    }

    function toolTip() {
      $('[data-toggle="tooltip"]').tooltip({
        trigger : 'hover'
      })  
    }

    function changeStatus(row,type) {//Funcion cambia el estado de la transaccion o traspaso sin recargar la pagina
      let mot = type === 'A' ?  '<span class="badge bg-success">APROBADO</span>' : '<span class="badge bg-danger">ANULADO</span>'
      let td = $(row).children().last();//el ultimo hijo del tr es decir en donde estan los enlaces
      let prev = $(td).prev();//el ultimo hijo del tr es decir en donde estan los enlaces
      let span = $(td).children().last()//ultimo hijo del td que es el enlace del modal
      $(prev).html(mot)
      $(td).html(/* mot+' / '+ */'<span data-toggle="modal" data-target="#modal-detalle" class="modaltrigger">'+$(span).html()+'</span>')
      toolTip()
    }

    function getDetalle(){
      $(document).on('click','.detalle',function (e) {
        e.preventDefault()
        var url = $(this).attr('href');
          let tabla = $('#detalestable').html('')
          let div = $('.divalert').html('')
          $.get(url,function(result){
            if(result){
                $(tabla).html(result.articulos)
                $(div).html(result.detalle)
            }else{
              toastError("No se pudo cargar el betamax")
            }
          })
      })
    }

      function aprobar(){
        $('.aprobar').click(function(e){
            e.preventDefault();
              var url = $(this).attr('href');
              var row  = $(this).closest('tr')
              Swal.fire({
              title: 'Seguro que Desea Aprobar?',
              text: "Esta accion no se puede revertir",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si!, aprobar'
            }).then((result) => {
              console.log(result)
              if (result.value) {
                $.get(url,function(result){
                  if(result.exito){
                    changeStatus(row,'A')
                    Swal.fire(
                      'Movimiento Aprobado!',
                      'Con Exito.',
                      'success',
                    )
                  }else if(result.error){
                    Swal.fire(
                      'Error en ejecucion',
                        result.error,
                      'error',
                    )
                  }else if(result.codubi === 'notEqual'){
                    Swal.fire({
                      title: 'Seguro que Desea Aprobar?',
                      html : result.html,
                      text: "Hubo un error con el Beta ",
                      icon: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Si!, aprobar'
                    })
                  }
                })
              }else{
                Swal.fire({
                icon: 'warning',
                title: 'Accion Cancelada',
                //footer: '<a href>Why do I have this issue?</a>'
                timer:1500
              })
              }
            });
        });
      }

    function anular() {
      
      $('.anular').click(function (e) {
        e.preventDefault()
      var row  = $(this).closest('tr')
      var url = $(this).attr('href');
      var token = $("input[name='_token']").val();
      let cod = $(this).data('cod')
      Swal.fire({
      title: 'Especifique el Motivo de Anulacion',
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: 'Anular',
      showLoaderOnConfirm: true,
      preConfirm: (desanu) => {
        if(desanu.length=== 0) {
          Swal.showValidationMessage('Debe Introducir el motivo de anulacion')
          return
          }
          return fetch(url+'/'+desanu)
          .then(response => {
            return response.json()
          })
          .catch(error => {
            Swal.showValidationMessage(
              `Fallo la Anulacion: ${error}`
            )
          })
      },
      allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
      if (result.value.exito) {
        changeStatus(row,'R')
        Swal.fire(
          'Movimiento Anulado!',
          'Con Exito.',
          'success',
        )
      }else{
        Swal.fire(
          'Error en ejecucion',
            result.value.error,
          'error',
        )
      }
      })
    })
    }


    function sendMsj() {

    $('.sendMail').click(function(e){
        e.preventDefault();
          var url = $(this).attr('href');
          Swal.fire({
          title: 'Seguro que desea Reenviar el Correo ?',
          text: "Esta accion no se puede revertir",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si!, Enviar'
        }).then((result) => {
          if (result.value) {
            $.get(url,function(result){
              if(result.exito){
                Swal.fire(
                  'Correo Enviado Exitosamente!',
                  'Con Exito. ('+result.correo+')',
                  'success',
                )
              }else if(result.error){
                Swal.fire(
                  'Error en ejecucion',
                    result.error,
                  'error',
                )
            //   }else if(result.codubi === 'notEqual'){
            //     Swal.fire({
            //       title: 'Seguro que Desea Aprobar?',
            //       html : result.html,
            //       text: "Hubo un error con el Beta ",
            //       icon: 'warning',
            //       showCancelButton: true,
            //       confirmButtonColor: '#3085d6',
            //       cancelButtonColor: '#d33',
            //       confirmButtonText: 'Si!, aprobar'
            //     })
              }
            })//get
          }else{
              Swal.fire({
              icon: 'warning',
              title: 'Accion Cancelada',
              //footer: '<a href>Why do I have this issue?</a>'
              timer:1500
            })//Swal
          }
        });//Then
    });//function evento
  }

  })


</script>
@endsection
