@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Editar Ubicación')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      <!-- Ubicacion de almacen -->
    @endslot
  @endcomponent
  {{-- CONTENT --}}
  <section class="content">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-8">
          
          <div class="card card-info">
            @include('vendor/flash.flash_message')
            <div class="card-header">
            <h3 class="card-title"><b>EDICI&Oacute;N UBICACI&Oacute;N</b></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{route('ubiEdit',[$ubicacion->id,$almubi->id])}}" method="post">
              {{csrf_field()}}
              <div class="card-body">
              <div class="form group">
                <label for="nomubi">Nombre</label>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">ABC</span>
                    </div>
                  <input type="text" class="form-control" name="nomubi" id="nomubi" value="{{$ubicacion->nomubi}}" onkeyup="pasarMayusculas(this.value, this.id)" class="@error('nomubi') is-invalid @enderror" value="{{old('nomubi')}}">
                  </div>
                  @error('nomubi')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
              </div>
              <div class="form-group">
                <label for="codalm">Seleccione Almac&eacute;n a registrar</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    
                  </div>
                  <select class="form-control select2"  name="codalm[]" 
                  id="codalm" class="@error('codalm') is-invalid @enderror" multiple>
                  
                  <option>Seleccione Almacen a registrar</option>
                    @foreach($almacenes as $key)
                      @foreach ($almubi2 as $item)
                      @php
                          
                          if ($key->codalm === $item->codalm){
                            
                            $select = 'selected';
                            break;
                          }
                          else $select = '';
                          
                      @endphp
                      @endforeach
                      <option value="{{$key->codalm}}" {{$select}}>{{ $key->nomalm }}</option>
                    @endforeach
                </select>
                </div>
                @error('codalm')
                <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                <div id="miDiv"></div>
              </div>
              
            <!-- /.card-body -->
            <div class="card-footer">
              <button id="createUbi" type="submit" class="btn btn-success"><b>Guardar Cambios</b></button>
              <a href="{{ url()->previous() }}" class="btn btn-default ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
            </div>
            </form>
            </div>
            <!-- /.card -->
            </div>
        </div>
      </div>
        {{-- row --}}
    </div>
    {{-- container fluid --}}
  </section>
@endsection
@section('script')
    
<script src="{{asset('js/chequeo.js')}} "></script>
<script type="text/javascript" defer>


$(document).ready(function () {

  // inicializamos el plugin
      $('select.select2').select2({});
      
      
      var nuevoInput = document.createElement("input")
      nuevoInput.setAttribute("type","hidden")
      nuevoInput.setAttribute("name","datosEliminado[]")
      
      var valoresIniciales = $('#codalm').val()
      
      $('#codalm').on('change',function () {
        var valoresActuales = $(this).val();
        var valoresEliminados = valoresIniciales.filter(x => !valoresActuales.includes(x))
        nuevoInput.setAttribute("value",valoresEliminados)
      })
      
      document.getElementById("miDiv").appendChild(nuevoInput);
  });
</script>
@endsection