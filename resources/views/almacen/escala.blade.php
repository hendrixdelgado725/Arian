@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Escala')

@section('content')

 @component('layouts.contenth')
    @slot('titulo')
      Listado Escala
    @endslot
  @endcomponent

<section class="content">
	<div class="content-fluid">
		<div class="card">
			@include('vendor/flash.flash_message')
			<div class="card-header">
				<h3 class="card-title"></h3>

				<div class="card-tools">
					 <div class="row">
					 	<div class="col-sm-4 mt-2">
					    @can('create',App\permission_user::class) 

					 		<a href="{{route('nuevaEscala')}}" class="btn btn-info"><b>REGISTRAR</b></a>
					 	@endcan
					 	</div>	

					 	<div class="col mt-2">
					 		<form action="{{route('buscarEscala')}}" method="POST">
					 			@csrf
					 			<div class="input-group">
					 				<input type="text" name="filtro" placeholder="Buscar" class="form-control float-right">
                      				<button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                      				<a href="{{route('listaEscala')}}" class="btn btn-default"><i class="fas fa-arrow-left"></i></a>                       
					 			</div>
					 		</form>
					 	</div>
					 </div>
				</div>

				<div class="card-body table-responsive p-0 text-center">
					<table class="table table-hover">
						<thead>
							<tr style="text-align:center; font-weight: bold; ">
								<td>ID</td>
								<td>Referencia</td>
								<td>Descripci&oacute;n</td>
								<td>Acci&oacute;n</td>
							</tr>
						</thead>

						<tbody>
							@foreach($escala as $esc)
								@php
									$rand  = rand(1, 9999);
									$nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
									$enc=Helper::EncriptarDatos($nrand.'-'.$esc->id);
									$borr=Helper::EncriptarDatos($nrand.'|'.$esc->id.'|'.$esc->codigoid);
								@endphp
								<tr>
									<td style="text-align:center">{{$esc->id}}</td>
									<td style="text-align:center">{{$esc->codesc}}</td>
									<td>{{$esc->nomesc}}</td>
									<td>
	                  					<div class="button_action" style="text-align:center">
	                  				    @can('edit',App\permission_user::class) 

					                      <a title="Edición" href="{{ route('editarEscalaForm',$enc)}}"><i class="fa fa-edit blue"></i></a>
					                      /
					                    @endcan
					                      @can('delete',App\permission_user::class) 

					                      <a href="{{route('eliminarEscala', $borr)}}" id="deleteescala" 
					                      data-id="{{$esc->id}}" class="borrarescala">
					                        <i class="fas fa-trash red"></i>
					                      </a>
	                      				  @endcan
	                      				</div>
                  					</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					{!!$escala->render()!!}
				</div>

			</div>
		</div>
	</div>
</section>
 @endsection
 @section('script')
<script src="{{asset('js/funciones.js')}} "></script>
@endsection