@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Registro del Almacen')



  @section('content')
    @component('layouts.contenth')
    @slot('titulo')
      <!-- Registrar Almacen -->
    @endslot
  @endcomponent

  <section class="content">
    <div class="content-fluid">
      
    <div class="container">
        
        <div class="col-sm-12">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><b>REGISTRO ALMAC&Eacute;N</b></h3>
                
              </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form action="{{route('Registrar.Almacen')}}" method="post">
              {{csrf_field()}}
                <div class="card-body">
                 <div class="row">
                 <!-- <div class="col-sm-6">
                <div class="form-group">
                <label>Numero de Almacen</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">cod</span>
                  </div>
                  <input type="text" class="form-control codigo" placeholder="EJ:000001" name="codigo" 
                  id="cedularifcliente">
                </div>
                @error('codigo')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
               </div>	 -->
                
                <div class="col-sm-12">
                <div class="form group">
                <label for="nombre">Descripción</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Aa</span>
                  </div>
                   <input type="text" class="dirA form-control" name="Descripcion" id="DescriPAlmacen" onkeyup="pasarMayusculas(this.value, this.id)">
                </div>
                @error('Descripcion')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div>

                <div class="col-sm-12">
                <div class="form group">
                    <label for="direccion">Dirección</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Aa</span>
                  </div>
                   <textarea type="text"  class="desc form-control" name="direccion" id="TipoAlmacen" onkeyup="pasarMayusculas(this.value, this.id)"></textarea>
                </div>
                </div>
                @error('direccion')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>  
                </div>

                <div class="col-sm-12">
                  <div class="form-group">
                  <label >Sucursal Asociar</label>
                  <div class="input-group mb-3">
                    <select style="width:100%" class="form-control select2 roles" id="sucuc" name="sucuc" tabindex="-1"  required="required" value="">
                        <option selected="selected" data-select2-id="" disabled>Seleccione la Sucursal</option>
                        @foreach ($sucursal as $suc)
                          <option value="{{$suc["codsuc"]}}">{{$suc["nomsucu"]}}</option>
                        @endforeach
                    </select>
                  </div>
                </div>
                </div>
                
                <div class="col-sm-12">
                  <div class="form-group">
                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                    <input type="checkbox" class="custom-control-input" id="customSwitch3" name="pfactur">
                      <label class="custom-control-label" for="customSwitch3">Almacén predeterminado para Facturar : No</label>
                    </div>
                  </div>
                </div>
              </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Guardar Cambios</button>
                <a href="{{ route('Listado.Almacen') }}" class="btn btn-default"><i class="fa fa-chevron-circle-left"></i> Volver</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->
    </div>
  </section>

  @endsection

  @section('script')
    <script src="{{asset('js/chequeo.js')}} "></script>
    <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
    });
   </script>
  @endsection

  @section('mayuscula')
    <script >
      $(document).ready(function(){
          $('.codigo').keyup(function(tecla){

            var texto = $(this).val().toUpperCase();
            $(this).val(texto);
          });

          $('.desc').keyup(function(t){
               var texto = $(this).val().toUpperCase();
            $(this).val(texto);

          });

          $('.dirA').keyup(function(t){
               var texto = $(this).val().toUpperCase();
            $(this).val(texto);

          });

          $('#customSwitch3').on('change',function(){
            $('.custom-control-label').html('')
            const opt = document.getElementById("customSwitch3").checked === true ? 'Si' : 'No';
            $('.custom-control-label').html('Almacen Predeterminado Para Facturar : '+opt)
          })

      });

    </script>
  @endsection
    
