@extends('layouts.app')
{{-- @extends('layouts.menu') --}}
@section('titulo','Estatus Inventario')

@section('content')
    <section class="content">
      <div class="container-fluid">
        <div class="row justify-content-center">
          <div class="col-8">
              <div class="card card-primary">
                @include('vendor/flash.flash_message')
                  <div class="card-header">
                  <h3 class="card-title"><b>
                    @if ($movimiento->codtra)
                      Solicitud de Traspaso
                    @elseif($movimiento->nommov === 'SAL')
                      Solicitud de Salida de Almacen
                    @elseif($movimiento->nommov === 'ENT')
                      Solicitud de Entrada a Almacen
                    @endif
                  </b></h3>
                  </div>
                  <div class="card-body">
                    {{-- <form action="{{route('inventarioEstatus')}}" method="post"> --}}
                      @if ($error !== 'E')
                      <div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-exclamation-triangle"></i> Atencion!</h5>
                      <strong>Este Movimiento se encuentra {{$error = $error =='APR' || $error =='A' ? 'Aprobado' : 'Rechazado'}}</strong>
                      </div>
                      <a href="{{route('home')}}" class="btn btn-primary"><i class="fas fa-home mr-3"></i><b>Ir a Home</b></a>
                      @endif
                      @csrf
                      <div class="form-group row">
                        <label for="descripcion">Codigo</label>
                        <input type="text" class="form-control" id="obser" value="{{$movimiento->codmov ?? $movimiento->codtra}}" readonly></input>
                      </div>
                      <div class="form-group row">
                        <label for="descripcion">Descripcion</label>
                        <textarea class="form-control" id="descripcion" rows="3" readonly>{{$desc}}</textarea>
                      </div>

                      <div class="form-group row">
                        <label for="obser">Observacion</label>
                      <input type="text" class="form-control" id="obser" value="{{$movimiento->obstra ?? $movimiento->desrcp}}" readonly>
                      </div>
                      @if ($movimiento->codtra)
                        <div class="form-group row">
                          <label for="almori">Almacen Origen</label>
                        <input type="text" class="form-control" id="almori" value="{{$movimiento->almorides->nomalm}}" readonly>
                        </div>
                        <div class="form-group row">
                          <label for="almdes">Almacen Destino</label>
                        <input type="text" class="form-control" id="almdes" value="{{$movimiento->almdesdes->nomalm}}" readonly>
                        </div>
                      @elseif($movimiento->nommov === 'SAL')
                        <div class="form-group row">
                          <label for="almori">Almacen Origen</label>
                        <input type="text" class="form-control" id="almori" value="{{$movimiento->almacenes->nomalm}}" readonly>
                        </div>
                      @elseif($movimiento->nommov === 'ENT')
                        <div class="form-group row">
                          <label for="almdes">Almacen Destino</label>
                        <input type="text" class="form-control" id="almdes" value="{{$movimiento->almacenes->nomalm}}" readonly>
                        </div>
                      @endif
                      
                      <div class="form-group row">
                        <label for="almori">Fecha</label>
                      <input type="text" class="form-control" id="almori" value="{{$movimiento->created_at->format('d/m/Y')}}" readonly>
                      </div>
                  <!-- /.card-header -->
                  @if (isset($articulos))
                    <table class="table table-hover">
                      <thead>
                        <tr style="text-align:center">
                          <th>Codigo</th>
                          <th>Descripcion</th>
                          <th>Color</th>
                          <th>Categoria</th>
                          <th>Subcategoria</th>
                          <th>Demografia</th>
                          <th>Talla</th>
                          <th>Cantidad</th>
                        </tr>
                      </thead>
                      <tbody style="text-align:center">
                        @foreach ($articulos as $item)
                        <tr>
                            <td>{{$item->articulo->codart}}</td>
                            <td>{{$item->articulo->desart}}</td>
                            <td>{{$item->color->color}}</td>
                            <td>{{$item->categoria->nombre}}</td>
                            <td>{{$item->subCategoria->nomsubcat}}</td>
                            <td>{{$item->demografia->nombreDemo}}</td>
                            <td>{{$item->talla->tallas}}</td>
                            <td>{{$item->cantmov}}</td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  @else
                  
                  @endif
                </div>
                <div class="card-footer">
                    @if($error === 'E')
                    <a  href="{{route('aprobacionEmail',[Helper::crypt($movimiento->id),$tipo])}}" class="btn btn-primary aprobar"><i class="fas fa-check mr-2"></i><b>Aprobar</b></a>
                    <a  href="{{route('anulacionEmail',[Helper::crypt($movimiento->id),$tipo])}}" class="btn btn-danger anular"><i class="fas fa-times mr-2"></i><b>Rechazar</b></a>
                    @endif
                </div>
              </div>
              {{-- Card --}}
            </div>
            {{-- Col --}}
        </div>
      {{-- row --}}
    </div>
    {{-- container fluid --}}
  </section>
@endsection

@section('script')
  <script type="text/javascript">
  $(document).ready(function () {
    aprobar();
    rechazar();

    function aprobar() {
      $(document).on('click','.aprobar',function(e){
        e.preventDefault();
        let url = $(this).attr('href')
        Swal.fire({
              title: 'Seguro que Desea Aprobar?',
              text: "Esta accion no se puede revertir",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si!, aprobar'
            }).then((result) => {
              if (result.value) {
                $.get(url,function(result){
                  console.log(result)
                  if(result.exito){
                    Swal.fire(
                      'Movimiento Aprobado!',
                      'Con Exito.',
                      'success',
                    )
                  }else if(result.error){
                    Swal.fire(
                      'Error en ejecucion',
                        result.error,
                      'error',
                    )
                  }
                })
              }else{
                Swal.fire({
                icon: 'warning',
                title: 'Accion Cancelada',
                //footer: '<a href>Why do I have this issue?</a>'
                timer:1500
                })
              }
            });
      })
    }
    
    function rechazar() {
      $(document).on('click','.anular',function(e){
        e.preventDefault();
        let url = $(this).attr('href')
        Swal.fire({
      title: 'Especifique el Motivo de Anulacion',
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: 'Anular',
      showLoaderOnConfirm: true,
      preConfirm: (desanu) => {
        if(desanu.length=== 0) {
          Swal.showValidationMessage('Debe Introducir el motivo de anulacion')
          return
          }
          return fetch(url+'/'+desanu,{
            headers: {
            'Content-Type': 'application/json',
            "X-Requested-With": "XMLHttpRequest"
        }
          })
          .then(response => {
            return response.json()
          })
          .catch(error => {
            Swal.showValidationMessage(
              `Fallo la Anulacion: ${error}`
            )
          })
      },
      allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
          console.log(result)
          if (result.value.exito) {
            Swal.fire(
              'Movimiento Anulado!',
              'Con Exito.',
              'success',
              )
            //window.locationf = result.url
          }else{
            Swal.fire(
              'Error en ejecucion',
              result.value.error,
              'error',
              )
            }
            //window.locationf = result.url
          });
        })
      }
    });
  </script>
@endsection