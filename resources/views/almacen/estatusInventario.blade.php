@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Estatus Inventario')

@section('content')
  @component('layouts.contenth')
    @slot('titulo')
    @endslot
  @endcomponent
    <section class="content">
      <status-inventario 
      :almacenes="{{json_encode($almacenes) ?? json_encode('[]')}}">
  </section>
@endsection