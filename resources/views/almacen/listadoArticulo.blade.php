@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Listado de Articulos')
@section('content')
  {{-- cabeza del pagina  --}}
  
 {{-- container fluid --}}
 {{-- <div style="display:none">
  <form id="changeStatusArticulo" action="{{route('changeStatusArticulo')}}" method="POST">
    @method('PUT')
    @csrf
    <input type="text" id="codart" name="codart">
    <button id="changeStatusSubmit" type="submit">Save</button>
  </form>
</div> --}}

 <listado_articulos></listado_articulos>
@endsection

@section('eliminarArticulos')
<style>
.div-con-texto {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.dropdown-toggle::after { 
  content: none; 
} 
</style>
<script>
/* 

$(function () {
$('[data-toggle="precio"]').tooltip()
})

  $('.borrar').click(function(e){
          e.preventDefault();
            Swal.fire({
            title: '¿Seguro que desea eliminar este Articulos?',
            text: "¡Despues de esto no se encontrara en la lista!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar'
          }).then((result) => {
              if (result.value == true) {


              var row = $(this).parents('tr');
              //var form = $(this).parents('data-id');
              var url = $(this).attr('href');
              $.get(url,row,function(result1) {

                if (result1.titulo === 'SE HA ELIMINADO CON EXITO') {


                      Swal.fire(
                        'SE HA ELIMINADO CON EXITO',
                        '.',
                        'success',

                      );

                      row.fadeOut();
                }else if (result1.titulo === 'NO SE PUEDE ELIMINAR EL ARTICULO POR TIENE FACTURA') {


                      Swal.fire(
                        'NO SE PUEDE ELIMINAR EL ARTICULO POR TIENE FACTURA',
                        '.',
                        'warning',

                      );
                }




              })
              }  //para cancelar el sweetalert2
          });

  });

  $( function(){
    document.getElementById("intro").focus();
    // $(document).on('keydown','input.intro',function(e){
    //       if(e.which === 13) {
    //         e.preventDefault()
    //         var inputs = $(this).closest('form').find(':input:visible');
    //         inputs.eq( inputs.index(this)+ 1 ).focus();

            

    //       }
          
    //   })

    $('[data-toggle="tooltip"]').tooltip()

    $(document).on('click','.changeStatusArticulo',function(e){
      e.preventDefault();
      let url = $(this).data('href')
      console.log(url)
      let codart = $(this).data('codart')
      let desart = $(this).data('desart')
      let msg = $(this).data('status') === 'A' ? 'desactivar': 'activar';
      Swal.fire({
            title: `¿Seguro que desea ${msg} ${desart}?`,
            text: `¡Ahora el producto ${msg == 'activar' ? 'saldra' : 'no saldra'} para facturacion/pedidos!`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: `Si, ${msg}!`
          }).then(function(result){
              if(result.value===true){
                $('#changeStatusArticulo #codart').val(codart)
                $('#changeStatusSubmit').trigger('click')
                // $.ajax({
                //   url:url,
                //   type:"GET",
                //   data:{
                //     codart,
                //     desart
                //   },success:function(){
                //     window.location.href = "/modulo/Almacen/Artculos";
                //     }
                // })
              }
              
            
             

          })
    })
  }) */
  
  
</script>

@endsection
