@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Listado de Entradas Por Almacen')


@section('content')

@component('layouts.contenth')
    @slot('titulo')
      Listado de Entradas
    @endslot
  @endcomponent
    <section class="content">
      <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
              @include('vendor/flash.flash_message')
                <div class="card-header">
                  <h3 class="card-title"><b></b></h3>
                  <div class="card-tools">
                   <div class="row">
                      <div class="col">
                        @can('create',App\permission_user::class) 
                        <a href="{{ route('CreateSalida') }}" class="btn btn-info"><b>REGISTRAR SALIDA</b></a>
                      </div>  
                       @endcan
                      <div class="col">
                          <form action="" method="post">
                            <div class="input-group">
                            @csrf
                            <input type="text" placeholder="Buscar por nombre de Almacen" name="filtro"  class="form-control float-right">
                            <button type="submit" style="height: 38px; width: 38px" class="btn btn-default btn-sm float-left"><i class="fas fa-search"></i></button>
                            <a href="{{route('ListadoSalida')}}" class="btn btn-default"><i class="fas fa-arrow-left mt-1"></i></a>                       
                          </div>
                          </form>
                      </div>
                   </div><!--row -->
                  </div>
                </div>
  
                <!-- /.card-header -->
                <div class="card-body table-responsive text-center">
                  <table class="table table-hover">
                    <thead>
                      <tr style="text-align:center">
                        <th>Codigo</th>
                        <th>Codigo del Almacen</th>
                        <th>Nombre de Almacen</th>
                        <th>Direccion del Almacen</th>
                        <th>Acción</th>
                      </tr>
                    </thead>
                    <tbody style="text-align:center">
                  {{--@foreach($almacen as $alma)
                      @php
                      $rand  = rand(1, 9999);
                      $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                      $enc=Helper::EncriptarDatos($nrand.'-'.$alma['id']);          
  
                      @endphp           
                        
                        <tr style="text-align: center;">
                        
                        <td>{{$alma->id}}</td>
                        <td>{{$alma->codalm}}</td>
                        <td>{{$alma->nomalm}}</td>
                        <td>{{$alma->diralm}}</td>      
                        
                        <td>
                          <a  href="{{ route('ActualizarDatos.Almacen', ['id'=>$enc]) }}">
                            <i class="fa fa-edit blue"></i>
                          </a>
                          /
                            <a class="borrar" data-id="{{ $alma->id }}"  href="{{ route('Eliminar.Almacen',$enc) }} ">
                            <i class="fa fa-trash red"></i>
                          </a>
                        </td>
                      </tr>
                    
                    @endforeach--}}
                    </tbody>
                  </table>
                  {{--{!!$almacen->render()!!}--}}
                  {{-- col8  --}}
  
            </div>
            {{-- <div>
              <a href="{{ url()->previous() }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
            </div> --}}
            </div>
            
          </div>
      </div>
        {{-- row --}}
    </div>
    {{-- container fluid --}}
    </section>


@endsection
@section('lista')
  
<script>
  
  $(document).ready(function() {
  
    $('.borrar').click(function(e){
        e.preventDefault();
          Swal.fire({
          title: 'Seguro que Desea Eliminar?',
          text: "Despues de esto no se encontrara en la lista!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            
          if (result.value) {
            Swal.fire(
              'Ha sido Eliminado!',
              'Con Exito.',
              'success',

            )
            
            var row = $(this).parents('tr');
            //var form = $(this).parents('data-id');
            var url = $(this).attr('href');
            console.log(url);
            /*console.log(form.serialize());*/
            $.get(url,row,function(result1){
              row.fadeOut();
              /*$('#tablas').html(result1.total);*/
            })
           }else{
            Swal.fire({
            icon: 'warning',
            title: 'No ha sido Eliminado',
            //footer: '<a href>Why do I have this issue?</a>'
            timer:1500
          })
           }
          
        });

    });
});
</script>
@endsection
