@extends('layouts.app')
@extends('layouts.menu')
  @section('content')


@component('layouts.contenth')
@slot('titulo')
  Registro de Entrada
@endslot
@endcomponent

<section class="content">
  <div class="container-fluid">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12">        
          <div class="card card-info">
                <div class="card-header">
                  <h3 class="card-title mt-2"><b>REGISTRAR ENTRADAS/SALIDAS</b></h3>
                    <div class="d-flex flex-row-reverse bd-highlight">
                    </div>
                </div>   
                  <!-- /.card-header -->
                  <!-- form start -->
                <div class="card-body">
                  <form id="saveform" action="{{ route('SaveEntrada')}}" method="post">
                    {{csrf_field()}}
                      @include('vendor/flash.flash_message')
                      <div class="row">
                        <div class="col-sm-8">
                          <div class="form-group">
                            <label for="movinvent" >Movimiento de Inventario</label>
                              <!-- <div class="col-lg-8 col-sm-12"> -->
                                <select name="movinvent" id="movinvent" class="form-control select2 formselect" onchange="actualizarRefEntrada(this.value),changealm()">
                                    <option  value selected > Seleccione el Movimiento</option>
                                    <option value="ENT">ENTRADA</option>
                                    <option value="SAL">SALIDA</option>
                                </select>
                              <!-- </div> -->
                            @error('movinvent')
                              <div class="alert alert-danger">{!!$message!!}</div>
                            @enderror
                          </div>
                        </div>
                        <div class="col-sm-4">
                        <div class="form-group">
                          <label for="id">Fecha</label>
                            <div class="input-group mb-3">
                              <input type="text" class="form-control" disabled=true value="{{date("d/m/Y")}}" >
                              <input type="hidden" class="form-control" value="{{date("d/m/Y")}}" name="fecha" id="fechax">
                            </div>
                            @error('fecha')
                              <div class="alert alert-danger">{!!$message!!}</div>
                            @enderror
                        </div>
                        </div>

                        <div class="col-lg-8 col-sm-10">
                          <div class="form-group">
                            <label for="id" >Almacén</label>
                              <!-- <div class="col-lg-8 col-sm-9"> -->
                                <select name="codalm" id="codalm" class="form-control codalm select3" required="required" onchange="changealm()">
                                  <option value="">Seleccione Almacen</option>
                                  @foreach ($alm as $index => $value)
                                  <optgroup label="Sucursal :  {{$index}}">
                                    @foreach ($value as $item)
                                        @php
                                        $string = $item->pfactur === true ? ' -- Facturacion' : ''
                                        @endphp
                                      <option value="{{$item->codalm}}">{{$item->nomalm.$string}}</option>
                                      @endforeach
                                    </optgroup>
                                  @endforeach
                                </select>
                              <!-- </div> -->
                              @error('codalm')
                              <div class="alert alert-danger">{!!$message!!}</div>
                            @enderror
                          </div>
                        </div>
                        
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label for="id">Entrada</label>
                              <div class="input-group mb-3">
                                <input type="text" class="form-control" disabled=true value="{{--date("d/m/Y")--}}" name="" id="entrada">
                                <input type="hidden" class="form-control" value="{{--date("d/m/Y")--}}" name="entrada" id="entradax">
                              </div>
                              @error('entrada')
                              <div class="alert alert-danger">{!!$message!!}</div>
                            @enderror
                          </div>
                        </div>
                        
                        <div class="col-sm-8">
                          <div class="form-group">
                            <label>Proveedor</label>
                              <div class="input-group mb-3">
                                <select name="codpro" id="codpro" class="form-control codpro select3" >
                                  <option>Seleccione Proveedor</option>
                                  @foreach ($provee as $item)
                                  
                                    <option value="{{$item->codpro}}">{{$item->nompro."  RIF:".$item->rifpro}} </option>
                        
                                  @endforeach
                                </select>
                                  <div class="input-group-append">
                                    <button type="button" class="btn btn-success" data-toggle="modal"
                                    data-target="#modal-proveedor"><b>Añadir Proveedor</b></button>
                                  </div>
                                </div>
                                @error('codpro')
                              <div class="alert alert-danger">{!!$message!!}</div>
                                @enderror
                            </div>
                          </div>

                        <div class="col-sm-4">
                          <div class="form-group">
                            <label for="nrocontro">Referencia Factura</label>
                              <div class="input-group mb-3">
                                <input type="text" class="form-control" value="{{--date("d/m/Y")--}}" name="nrocontro" id="nrocontro" onkeyup="pasarMayusculas(this.value,this.id)" maxlength="20">
                              </div>
                              @error('nrocontro')
                              <div class="alert alert-danger">{!!$message!!}</div>
                            @enderror
                          </div>
                        </div>

                        <div class="col-sm-8">
                          <div class="form-group">
                            <label for="fecfac" >Fecha Factura o Referencia</label>
                              <input type="date" class="form-control" name="fecfac" id="fecfac">
                              <small id="fecfac" class="form-text text-muted">
                                Presione el icono para ver calendario
                              </small>
                                @error('fecfac')
                                  <div class="alert alert-danger">{!!$message!!}</div>
                                @enderror
                            </div>
                          </div>

                        <div class="col-sm-2">
                          <div class="form-group">
                            <label for="monrcp">Monto Total</label>
                                <input type="text" class="form-control" readonly value="{{--date("d/m/Y")--}}" name="monrcp" id="monrcp" data-mask="000.000.000,00" data-mask-reverse="true">
                              </div>
                              @error('monrcp')
                              <div class="alert alert-danger">{!!$message!!}</div>
                            @enderror
                          </div>

                        <div class="col-sm-2">
                          <div class="form-group">
                            <label for="typeCoin">Moneda</label>
                                <select name="typeCoin" id="typeCoin" class="form-control">
                                  @foreach ($moneda as $item)
                                  <option value="{{$item->codigoid}}">{{$item->nombre}}</option>
                                  @endforeach
                                </select>
                              </div>
                              @error('monrcp')
                              <div class="alert alert-danger">{!!$message!!}</div>
                            @enderror
                          </div>
                          

                        </div>

                        

                        <div class="col-sm-12">
                          <div class="form-group">
                            <label for="obsent">Observacion</label>
                            <!-- <div class="col-lg-8 col-sm-12"> -->
                            <textarea name="observacion" id="obsent" class="form-control" onkeyup="pasarMayusculas(this.value, this.id)"></textarea>
                            <!-- </div> -->
                            @error('observacion')
                              <div class="alert alert-danger">{!!$message!!}</div>
                            @enderror
                          </div>
                        </div>

         

                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="table">Seleccion de Articulos</label>
                           <small id="emailHelp" class="form-text text-muted">OJO: Tengan en cuenta de hacer su salida correctamente con su seriales respectivo</small> 
                          <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table id="table" class="table table-bordered table-responsive">
                              <thead>
                                <th>#</th>
                                <th></th>
                                <th>Artículo</th>
                                <th>Seriales</th>
                                <th>Precio</th>
                                <th>Cantidad</th>
                                <th>Ubicacion de Almacen</th>
                              </thead>
                              <tbody class="">
                                @php $limite = 10; @endphp
                                @for ($i = 0; $i < $limite; $i++)
                                <tr data-id="{{$i}}">
                                  <td> {{$i+1}} </td>
                                  <td><a class="cleanLine" href="">
                                    <i class="fa fa-trash "></i>
                                  </td>
                                  <td class="required">
                                    <select name="articulos[]" class="form-control select2art fillable required articulos2 n2{{$i}}" name="articulos2" id="selectA{{$i}}" data-ide="addSerial{{$i}}" data-row="{{$i}}" onchange="buscarTallas(this.value,2,this,{{$i}})" data-required="">
                                      <option value="" disabled selected>Seleccione Artículo</option>
                                    </select>
                                  </td>
                                  <td class="required">

                                    {{-- <div class="buscarSerial">
                                      <select name="salida"  name="seriales[]" class="form-control select2art seriales" id="seriales{{$i}}" onclick="cargarSerial(this.id,{{($i)}})">
                                        <option value="" disabled selected>Seleccione Serial</option>
                                      </select>
                                    </div> --}}

                                    <div class="agregarSerial">
                                      <input type="hidden"  name="seriales[]" class="cleanLine seriales" id="seriales{{$i}}" ><a data-toggle="modal"  style="margin-left: 20px; display:none" href="#" id="addSerial{{$i}}" onclick="cargarSerial(this.id,{{($i)}})" ><i class="fa fa-book"></i></a>
                                    </div>
            
                                  </td>
                                  <td>
                                    <input type="number" id="precio{{$i}}" onchange="calculo(this.id,{{$i}})" name="preunit[]" class="form-control fillable toCero precio1" value="0">
                                  </td>
                                  <td class="required">
                                                                                                                     {{-- ojo modifique aqui        --}}
                                      <input type="text" name="cantidad[]" id="cant{{$i}}" onchange="calculo(this.id,{{$i}})" data-cantidad="cant{{$i}}" valor="0" class="form-control fillable inputCant toEmpty required cant cantidad1" 
                                       data-toggle="tooltip" data-html="true" title=""> 
                                  </td>
                                  <td class="required">
                                    <select name="ubicacion[]" class="form-control select2mov codubi fillable required" data-row="{{$i}}">
                                      <option  value="" selected>UBICACION</option>
                                    </select>
                                  </td>
                                  <td class="required" style="display: none">
                                    <input type="text" name="tallasArtx[]" class="fillable 
                                    toCero" value="0">
                                  </td>
                                </tr>
                                @endfor 
                              </tbody>
                            </table>
                          </div>
                        </div>
                        </div>
                    </div>
                          <div class="card-footer">
                            <button type="button" id="guardar2" class="btn btn-success mt-4 ml-2 guardar"><b>Guardar Cambios</b></button>
                            <a href="{{ url()->previous() }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                          </div>
                      <input id="errorart" name="errorart[]" type="hidden" value="">
                      <input id="validart" name="validart[]" type="hidden" value="">
                      <input id="tallasArt" name="tallasArt[]" type="hidden" value="">
                      </div>
                  </form>

                  <div class="modal fade show" id="modal-proveedor" aria-modal="true">
                    <form action="#" id="saveProvee">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="modal-title">Registrar Proveedor</h4>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                              <label for="nompro">Nombre Proveedor</label>
                              <input type="text" onkeyup="pasarMayusculas(this.value,this.id)" class="form-control" name="nompro" id="nompro">
                            </div>
                            
                            <div class="row">
                              <div class="col-sm-2">
                                <div class="form-group">
                                <label for="tipodoc">Rif</label>
                                  <select class="form-control"  aria-hidden="true" id="tipodoc" name="tipodoc" class="@error('tipodoc') is-invalid @enderror">
                                      <option selected value="E">E</option>
                                      <option value="J">J</option>
                                      <option value="G">G</option>
                                  </select>
                                  @error('tipodoc')
                                    <div class="alert alert-danger">{!!$message!!}</div>
                                  @enderror
                              </div>     
                            </div>
                            <div class="col-sm-10">
                              <div class="form-group">
                              <label>N&uacute;mero</label>
                                  <div class="input-group-prepend">
                                  <input data-mask="99999999-9" type="text" class="form-control" placeholder="Ejem: 1234567" name="rifpro" 
                                  id="rifpro" class="@error('rifpro') is-invalid @enderror"
                                value="">
                              @error('rifpro')
                                <div class="alert alert-danger">{!!$message!!}</div>
                              @enderror
                              </div>
                            </div>
                            </div>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                          </div>
                        </div>
                      </form>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->

                   </div>
                 </div>
                 <div class="modal fade show" id="addSerial" aria-modal="true">
                    <form action="" class="cargarSeriales" data-serial="{{route('cargaSerial')}}">

                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                     <div class="modal-content">
                        <div class="modal-header label">
                          <h2>Agregar Seriales</h2>
                          <button type="button" onclick="addSerial()" class="btn btn-primary">
                             Agregar
                         </button>
                        
                          <button type="button" class="close" onclick="cerrar()" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                          </button>
                        </div>   
                        <div class="modal-body container">
                          <table id="datos2" class="table" style="width:100%">
                           <thead class="thead-dark" >
                              <tr align="center">
                                  <th>id</th>
                                  <th>Codigo</th>
                                  <th>Accion</th>
                              </tr>
                              </thead>
                              <tbody id="filasSeriales" align="center">
                                
                              </tbody>
                          </table>
                    
                        </div>
                        <div class="modal-footer">
                          <button type="submit" id="CargarSerial" class="btn btn-primary">Cargar</button>
                        </div>
                      
                      </div>
                    </div>
                     </div>
                    </div>
                  </form>
          {{--  CARD --}}
        </div>
        {{-- col  --}}
      </div>
        {{-- row justify center --}}
      </div>
      {{-- container --}}
    </div>
    {{-- container-fluid --}}
  <input id="arttojson" type="hidden" value="">
  <input id="urlgetart" type="hidden" value="{{route('getArtInfo')}}">
  <input id="urlgetartalm" type="hidden" value="{{route('getListArt')}}">
  {{-- <input id="getalmubi" type="hidden" value="{{route('getalmubi')}}"> --}}
  <input id="artToSave" type="hidden">


</section>
@endsection
@section('script')
  <script src="{{asset('js/chequeo.js')}} "></script>
  <script src="{{asset('js/funciones.js')}} "></script>
  <script src="{{asset('js/entrada.js')}} "></script>
<style>
  .my-custom-scrollbar {
    position: relative;
    height: 300px;
    overflow: auto;
    }
  .table-wrapper-scroll-y {
  display: block;
    }
</style>
<script>

  

$( document ).ready(function() {

$("#mostrar_fileupload_contrato").click(function(){
    $('#ArchivoContrato').toggle(1000,function() {

    });
});
});


let serialesInventarios = [];
let add = "";
let $u = 0;
let cptid = ""
let $indice = 0;
let key = []
let codart = []
cargarSeriales()
let count = 0;
let cargar = false;
let codigosArt = []
let noBorrar = []
let pasoGeneral = false;
let idGeneral = ''
document.onkeydown = (e) => e.type === 'keydown' && e.keyCode === 113 && cargar === true ? addSerial() : null
function cerrar(){
  cargar = false;
  $('#addSerial').modal('hide')
}

// $(document).ready(function() {
//     $('.buscarSerial').hide();

//     $('#movinvent').change(function() {
//         if ($(this).val() === 'SAL') {
//             $('.buscarSerial').show();
//             $('.agregarSerial').hide(); 
//         } else {
//             $('.buscarSerial').hide();
//             $('.agregarSerial').show();
//         }
//     });
// });



function cursor(id) {
  console.log(id)
  document.getElementById(id).focus()
}

$(function(){

       $(document).on('change','.articulos2',function(){
        let index2 = $(this).data('row')
        
        if(codigosArt[index2] !== undefined) {
          codigosArt.splice(index2,1)
          codigosArt[index2] = $(this).val()
          return;
        }
        
        codigosArt = [...codigosArt,$(this).val()]
        
      })

       document.getElementById('guardar2').addEventListener('click',async ()=>{
        
      //     //serialesInventarios,keyseriales
            let field = document.getElementsByClassName('articulos2')
            let cant = document.getElementsByClassName('cant')

             $("#guardar2").attr("disabled", true);
            // setTimeout(() => {
            //     $("#guardar2").attr("disabled", false);
            // }, 10000);

            let romper2 = false;
            for (let index = 0; index < field.length; index++) {
              const element = field[index]; //obtengo el elemento
              const cantidad = cant[index].value; //obtengo la cantidad de producto $('select.n2'+index).data('ide')
              if($('option:selected', element).data('foo')){
                   
                let tam = serialesInventarios.filter(element => Object.keys(element)[0] === $('select.n2'+index).data('ide'));
                
                if(tam.length !== parseInt(cantidad)){
                  romper2 = true;
                  break;
                  
                }
              }
             
            }

            if(romper2){
              swalError("Hay seriales que son menor que la cantidad verifique")
              $("#guardar2").attr("disabled", false);
              return;
            }

             $.ajax({
               type:'GET',
               url:"/modulo/Facturacion/searchArt",
               data:{
                 'codigosArt':codigosArt,
                 'seriales': serialesInventarios,
                 'key':key
               },success:function({msg,codart,field}){
                  if(msg === "Este Articulo esta validado para tener seriales, pero usted va a dar entrada sin seriales!" || 
                     msg === 'Este Articulo no esta validado para tener seriales, pero usted va a dar entrada sin seriales!'){
                    let romper = false;
                    console.log(msg === 'Este Articulo no esta validado para tener seriales, pero usted va a dar entrada sin seriales!')

                    if(msg === 'Este Articulo no esta validado para tener seriales, pero usted va a dar entrada sin seriales!'){
                      Swal.fire({
                          title: `¿${msg+' quiere continuar'}?`,
                          icon: 'warning',
                          showCancelButton: true,
                          confirmButtonColor: '#3085d6',
                          cancelButtonColor: '#d33',
                          confirmButtonText: 'Si',
                          cancelButtonText: 'No'
                      }).then((result) => {
                          if(result.value){
                            $("#guardar2").attr("disabled", true);
                            document.getElementById('saveform').submit();    
                          }
                          $("#guardar2").attr("disabled", false);
                      })
                    }else{
                      if(field !== null){
                      for (let index = 0; index < field.length; index++) {
                        
                        if(serialesInventarios[index][field[index]] === ''){
                          romper = true
                          break;
                        }
                      }
                      
                       if(romper){
                        swalError(msg)
  
                       }else{
                        if(pasoGeneral === false){
                          swalError("Los Producto no tiene seriales cargado Carguelo por favor")
                          return;
                        }
                          
                        $("#guardar2").attr("disabled", true);
                        document.getElementById('saveform').submit();

                       }
                    }else{
                      swalError(msg)
                    }
                    }
                   }
                }
             })
           //
          
       })
    })


function addSerial(){


    $indice++;
    serialesInventarios = [...serialesInventarios,{[`${cptid}`]:""}]  
    noBorrar = [...noBorrar,{[`${cptid}`]:""}]
    count++;
    key = [...key,cptid]
    codart = [...codart,window.codart];
    add = '<tr id="rowF'+$indice+'" class="rowF2"><td>'+$indice+'</td><td><input maxlength="30"  onkeyup="notVoid(this)" class="form-control '+cptid+' intro" id="add'+$u+'" value=""  onblur="Ingresar(this.id,'+$indice+','+cptid+')"  type="text"></td><td><a href="#" onclick="deleteItem('+$indice+','+$u+')"><i class="fa fa-trash"></i></a></td></tr><br>'
    $('#filasSeriales').append(add)
    $u = $u +1
}

function Ingresar(id2,pos,{id}){
   console.log(id2,pos-1,id,serialesInventarios,window.codart)
   serialesInventarios[pos-1][id] = $('#'+id2).val()
   console.log($('#'+id2).val())
   
   if($('#'+id2).val() !== ""){
     
     let repetidos = serialesInventarios.filter((value,index) => (index !== (pos-1) && serialesInventarios[index][id] === $('#'+id2).val()) ) //busca si hay un valor repetidos
     
       if(repetidos.length > 0){
          
        Swal.fire({
                type: 'error',
                title: 'Error, Seriales Repetidos',
                showConfirmButton: false,
                timer: 2000 // es ms (mili-segundos)
            })
            
        
         $('#'+id2).val("")
         cursor(id2) 
          return;
        }


        $.ajax({
              type:'GET',
               url:"/modulo/Facturacion/searchSeriales",
               data:{
                 'seriales': $('#'+id2).val(),
                 'codart':   window.codart,
                 'tipmov': $('#movinvent').val(),
               },success:function(result){
                  if(result.error){
                    Swal.fire({
                        icon: 'error',
                        title: result.msg,
                        showConfirmButton: false,
                        timer: 3000 // es ms (mili-segundos)
                    })      
                    
                    $('#'+id2).val("")
                    cursor(id2) 
                    return;
                  }else{
                    Swal.fire({
                        icon: 'success',
                        title: result.msg,
                        showConfirmButton: false,
                        timer: 3000 // es ms (mili-segundos)
                    })      
                  }
               }


        })

     
   }
 
      noBorrar[pos-1][id] = $('#'+id2).val()
      let temp = serialesInventarios[pos-1][id] !== ''
      if(temp){
        cursor('add'+pos)
        temp = false;
        return
      }
   
    
   
   
}

function deleteItem(pos,posA){
       
       $("#rowF"+pos).remove()
       noBorrar.splice(posA,1)
       serialesInventarios.splice(posA,1)
       key.splice(posA,1);
       codart.splice(posA,1)
       console.log(pos,posA)
       $('#filasSeriales').children('.rowF2').remove();
       console.log(serialesInventarios,noBorrar);
       $u = 0;
       $indice = 0
       if(serialesInventarios.length > 0){
          serialesInventarios.map((value,index) => {
            $indice = $indice+1;

              if(cptid === Object.keys(value)[0]){
                add = '<tr id="rowF'+$indice+'" class="rowF2"><td>'+$indice+'</td><td><input onkeyup="notVoid(this)" onblur="Ingresar(this.id,'+$indice+','+cptid+')" maxlength="30" class="form-control '+cptid+' intro" value="'+noBorrar[index][cptid]+'" id="add'+$u+'" name="seriales['+value[cptid]+']"  type="text"></td><td><a href="#" onclick="deleteItem('+$indice+','+$u+')"><i class="fa fa-trash"></i></a></td></tr><br>'
                $('#filasSeriales').append(add)
              }
              
            $u = $u+1;
          });
       }
}
function cargarSerial(id,index){

  cptid = id
  $('#filasSeriales').children('.rowF2').remove();
  //cargarSerial 
  window.codart = $('#selectA'+index).val()
  $(function() {

    $u = 0;
    $indice = 0
    count = 0;
      console.log(serialesInventarios,id);
      if(serialesInventarios.length > 0){
        
        if(serialesInventarios !== undefined){
          
          serialesInventarios.map((value,index) => {
            $indice = $indice+1
            if(Object.keys(value)[0] === id){//comparo los id
              
              add = '<tr id="rowF'+$indice+'" class="rowF2"><td>'+$indice+'</td><td><input  onkeyup="notVoid(this),ingresar('+$indice+','+$u+',this.id)" maxlength="30" class="form-control '+cptid+' intro"  id="add'+$u+'" value="'+value[id]+'" name="seriales['+value[cptid]+']" onblur="Ingresar(this.id,'+$indice+','+cptid+')"  type="text"></td><td><a href="#" onclick="deleteItem('+$indice+','+$u+')"><i class="fa fa-trash"></i></a></td></tr><br>'
              count++;
              $('#filasSeriales').append(add)
             
                
            }
            $u = $u+1;
            
          })
          
          }
      }
      

    $('#addSerial').modal('show')
    cargar = true;
  })

 }

function cargarSeriales(){

$(document).on('submit','.cargarSeriales',function (e) {
  e.preventDefault()
  $('#guardar2').attr('disabled','true')
  let data = {
    _token : $("input[name='_token']").val(),
    seriales: serialesInventarios,
    key:key,
    codart:codart,
    codalm:codalm
  }
  
  $.ajax({
        type: "POST",
        url: $(this).data('serial'),
        dataType: "json",
        data:data,
        success: function ({error,exito,paso}) {
            if(error){
              swalError(error)
              pasoGeneral = paso
              $('#guardar2').removeAttr('disabled')
              return;
            }else{
              Swal.fire('Guardado!',exito, 'success')
              $('#guardar2').removeAttr('disabled')
              pasoGeneral = paso
              var id = setInterval(() => {
                $('#addSerial').modal('hide')
                clearInterval(id)
              }, 5);
              return;
            }
        }
      });
})  

}


 $(function () {

      $(document).on('keydown','input.intro',function(e){
        
          if(e.which === 13 || e.which === 9) {
            e.preventDefault()
            var inputs = $(this).closest('form').find(':input:visible');
            inputs.eq( inputs.index(this)+ 1 ).focus();

            

          }
          
      })

      $(document).on('blur','.cant',function(e){
        const {type} = window.event;
        let elem = document.getElementById('selectA'+$(this).attr('valor'))
        
        window.isserial = type === 'blur' ? $('option:selected', elem).data('foo') : window.isserial
        
        if((count > parseInt($('#'+e.currentTarget.id).val()) || count < parseInt($('#'+e.currentTarget.id).val())) && window.isserial === true){
          swalError("no puedes ingresar la cantidad mayor que la cantidad de seriales ingresado");
          $('#guardar2').attr('disabled','true')
         
        }else {

          $('#guardar2').removeAttr('disabled')
        }
        console.log(count)
      })

     
 })
 function notVoid({value,id}){
  const pattern = /^[A-Za-z0-9]+$/g//cuando el valor no posee caracteres
  document.getElementById(id).value = (pattern.test(value.trim())) ? value.trim() : ''
 }     
 function ingresar(index,u,id){
    console.log(id)
 }
</script>
  @endsection
