@extends('layouts.app')
@extends('layouts.menu')
  @section('content')

  @component('layouts.contenth')
    @slot('titulo')
      Registro Traspasos
    @endslot
  @endcomponent
    
<section class="content">
      <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-12">        
              <div class="card card-info">
              @include('vendor/flash.flash_message')
                <div class="card-header">
                  <h3 class="card-title mt-2"><b>REGISTRAR TRASPASO</b></h3>
                  <div class="d-flex flex-row-reverse bd-highlight">
                  </div>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <div class="card-body">
                <form id="saveform" action="{{ route('traspasosAlmacenSave')}}" method="post">
                      @csrf
                      <div class="row">
                      <div class="col-sm-8">
                      <div class="form-group">
                        <label for="almori" >Seleccion de Almacen Origen</label>
                          <div class="col-lg-8 col-sm-12">
                            <select name="almori" id="almori" class="form-control almori  select2form" onchange="changeAlm(this.id)">
                              <option value selected>Seleccione el Almacen Origen</option>
                              @foreach ($alm as $index => $value)
                                  <optgroup label="Sucursal :  {{$index}}">
                                    @foreach ($value as $item)
                                        @php
                                        $string = $item->pfactur === true ? ' -- Facturacion' : ''
                                        @endphp
                                      <option value="{{$item->codalm}}">{{$item->nomalm.$string}}</option>
                                    @endforeach
                                  </optgroup>
                                @endforeach
                            </select>
                            {{old('almori')}}
                          </div>
                          @error('almori')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                      </div>
                    </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="fechax">Fecha</label>
                            <div class="col-sm-12">
                              <input type="text" class="form-control" disabled=true value="{{date("d/m/Y")}}" name="fechax" id="fechax">
                              <input type="hidden" class="form-control" value="{{date("d/m/Y")}}" name="fecha" id="fecha">
                            </div>
                        </div>
                      </div>
                          <div class="col-sm-8">
                            <div class="form-group">
                              <label for="tiptras">Tipo de Traspaso</label>
                              <div class="col-lg-8 col-sm-12">
                                <select name="tiptras" id="tiptras" class="form-control tiptras select2form" onchange="changeTipTrasp(this.id)">
                                  <option value selected>Seleccione Tipo de Traspaso</option>
                                  <option value="INT">Interno</option>
                                  <option value="EXT">Externo</option>
                                </select>
                              </div>
                                  @error('tiptras')
                            <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label for="codtraspasox">Traspaso</label>
                              <div class="col-sm-12">
                                <input type="text" class="form-control" disabled=true value="{{$reftra}}" name="codtraspaso" id="codtraspasox">
                                <input id="reftra" name="reftra" type="hidden" class="form-control" disabled=true value="{{$reftra}}" name="codtraspaso" id="codtraspaso">
                              </div>
                            </div>
                          </div>
                        </div>
                      <div class="row">
                      <div class="col-sm-8 offset">
                        <div class="form-group">
                          <label for="almdes" >Seleccion de Almacen Destino</label>
                          <div class="col-lg-8 col-sm-12">
                            <select name="almdes" id="almdes" class="form-control almdes select2form" onchange="changeAlm(this.id)">
                              <option value selected>Seleccione el Almacen</option>
                                @foreach ($alm as $index => $value)
                                <optgroup label="Sucursal :  {{$index}}">
                                  @foreach ($value as $item)
                                      @php
                                      $string = $item->pfactur === true ? ' -- Facturacion' : ''
                                      @endphp
                                    <option value="{{$item->codalm}}">{{$item->nomalm.$string}}</option>
                                  @endforeach
                                </optgroup>
                                @endforeach
                              </select>
                            </div>
                            @error('almdes')
                            <div class="alert alert-danger">{!!$message!!}</div>
                            @enderror
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <div class="form-group">
                          <label for="obstra">Observacion</label>
                          <div class="col-lg-8 col-sm-12">
                            <textarea name="obstra" id="obstra" class="form-control" onkeyup="pasarMayusculas(this.value, this.id)">{{old('obstra')}}</textarea>
                          </div>
                          @error('obstra')
                          <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                        </div>
                      </div>
                    <div class="form-group">
                      <label for="table">Seleccion de Articulos</label>
                          <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table id="table" class="table table-bordered table-responsive">
                              <thead>
                                <tr>
                                  <th>Nro</th> 
                                  <th></th>
                                  <th>Articulo</th>
                                  <th>Tallas</th>
                                  <th>Cantidad</th>
                                  {{-- <th>Existencia Actual</th> --}}
                                  <th>Ubicacion de Almacen Origen</th>
                                  <th>Ubicacion de Almacen Destino</th>
                                </tr>
                              </thead>
                              <tbody class="" id="tableArt">
                                @php
                                    $limite = 20;
                                @endphp
                                @for ($i = 1; $i < $limite; $i++)
                                <tr data-id="{{$i}}">
                                  <td>
                                    {{$i}}
                                  </td>
                                  <td>
                                    <a class="cleanLine" href="">
                                    <i class="fa fa-trash "></i>
                                  </td>
                                  <td>
                                    <select name="articulos[]"  class="selectart form-control select2art fillable required" onchange="buscarTallasTras(this)">
                                      <option value="" selected>Seleccione Articulo</option>
                                    </select>
                                  </td>
                                  <td>
                                    <select class="form-control select2 cambios fillable required" name="talla[]" multiple="">
                                      <option value="N/D" selected>Sin Talla</option>
                                    </select>
                                  </td>
                                  <td>
                                      <input type="text" name="cantidad[]" id="cant{{$i}}" class="form-control cantosave toEmpty fillable required inputCant" oninput="validateCant(this.id)" data-toggle="tooltip" data-html="true" title=""> 
                                  </td>
                                  {{-- <td>
                                      <input type="text" class="form-control exiact" readonly> 
                                  </td> --}}
                                  <td>
                                    <select name="ubicacionori[]" class="form-control select2movori ubiOri ubicacion fillable required">
                                      <option  value="" selected>Ubicacion Origen</option>
                                    </select>
                                  </td>
                                  <td>
                                    <select name="ubicaciondes[]" class="form-control select2movdes ubiDes ubicacion fillable required">
                                      <option value="" selected>Ubicacion Origen</option>
                                    </select>
                                  </td>
                                  <td style="display: none">
                                    <input type="text" name="tallasArtx[]" class=" validTallas toCero fillable" value="0">
                                  </td> 
                                </tr>
                                @endfor 
                              </tbody>
                          </table>
                      </div>
                    </div>
                    <div class="row">
                      <button type="submit" id="guardar" class="btn btn-success mt-4 ml-2 guardar"><b>Guardar Cambios</b></button>
                      <a href="{{ url()->previous() }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                    </div>
                      <input id="errorart" name="errorart[]" type="hidden" value="">
                      <input id="validart" name="validart[]" type="hidden" value="">
                      <input id="tallasArt" name="tallasArt[]" type="hidden" value="">
                  </form>
                </div>
                </div>
          </div>
        </div>
      </div>
        {{-- row --}}
    </div>
    {{-- container fluid --}}
  {{-- <input id="getalmubi" type="hidden" value="{{route('getalmubi')}}"> --}}
</section> 
  @endsection
  @section('script')
  <script src="{{asset('js/chequeo.js')}} "></script>
  <script src="{{asset('js/traspasos.js')}} "></script>
<style>
  .my-custom-scrollbar {
    position: relative;
    height: 300px;
    overflow: auto;
    }
  .table-wrapper-scroll-y {
  display: block;
    }
</style>
  @endsection

