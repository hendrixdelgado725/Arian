  @extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Ubicaciones de almacen')

@section('content')
@component('layouts.contenth')
    @slot('titulo')
      Listado Ubicaciones de Almac&eacute;n
    @endslot
  @endcomponent
    <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')

              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">
                   <div class="col-sm-4 mt-2">
                    @can('create',App\permission_user::class) 
                  <a href="{{ route('ubiCreate') }}" class="btn btn-info"><b>REGISTRAR</b></a>
                    @endcan
               </div>
               <br> 
               <div class="col">
                  <div class="row">
                    <div class="col mt-2">
                      
                        <form action="{{ route('ubicacionFind') }}" method="get">
                         <div class="input-group">
                          @csrf
                         <input type="text" placeholder="Buscar" name="filtro" value="{{$string ?? ''}}"  class="form-control float-right">
                          <button type="submit" style="height: 38px; width: 38px" class="btn btn-default btn-sm float-left"><i class="fas fa-search"></i></button>
                           <button href="#" class="btn btn-default"><i class="fas fa-arrow-left"></i></button> 
                        </div>
                        </form>
                    </div>
                  </div>
                 </div>
                
                 </div><!--row -->
                </div>
              </div>
              <!-- /.card-header -->

              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>ID</th>
                      <th>Nombre</th>
                      <th>Almac&eacute;n</th>
                      <th>Acci&oacute;n</th>
                    </tr>
                  </thead>
                  <tbody style="text-align:center">
                    @php
                        $i = 1;
             
                    @endphp
                  @foreach ($ubicaciones as $key)
                    @php
                    $rand  = rand(1, 9999);
                    $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                    $enc=Helper::EncriptarDatos($nrand.'-'.$key->id);
                    $string2= '';                   
                    foreach ($key->almacenes as $item)
                             $string2 .= $item->nomalm.'<br>';
                    
                    
                    @endphp
                      <tr>
                        <td>{{$i}}</td>
                        <td>
                              {!!Helper::filtroBold($key->nomubi,$string ?? null)!!}
                        </td>
                          <td>
                           <i class="fa fa-search-plus" data-html="true"  style="font-size: 0.7em;" data-toggle="tooltip" data-placement="right" title="{!! htmlspecialchars($string2) !!}" id="desplegar" data-placement="top">
                          </td>
                        <td>
                          @can('edit',App\permission_user::class) 
                          <a class="" href="{{ route('ubiShow',$enc) }}">
                            <i class="fa fa-edit"></i>
                          </a>
                          /
                          @endcan
                          @can('delete',App\permission_user::class) 
                          <a class="borrar" href="{{ route('ubiDelete',$enc) }}">
                            <i class="fa fa-trash "></i>
                          </a>
                          @endcan
                        </td>
                      </tr>
                      @php
                          $i++;

                      @endphp
                      @endforeach
                    </tbody>
                  </table>
                {!! $ubicaciones->appends(Request::only('filtro'))->render() !!}
                
              </div>
            </div>
          </div>
        </div>
        {{-- row --}}
      </div>
      {{-- container fluid --}}
@endsection
 </section>
    

@section('script')

<script>
  
  $(function () {
    var tooltip = [].slice.call(document.querySelectorAll('[data-toggle="tooltip"]'));
    var tooltipList = tooltip.map(function(val){
      
        return new bootstrap.Tooltip(val);
    })
  })

  $('.borrar').click(function(e){
          e.preventDefault();
            Swal.fire({
            title: '¿Seguro que desea eliminar este Articulos?',
            text: "¡Despues de esto no se encontrara en la lista!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar'
          }).then((result) => {
              if (result.value == true) {
              var row = $(this).parents('tr');
              //var form = $(this).parents('data-id');
              var url = $(this).attr('href');
              $.get(url,function(result) {
                  
                if (result.exito) {
                      Swal.fire(
                        'SE HA ELIMINADO CON EXITO',
                        '.',
                        'success',
                      );
                      row.fadeOut();
                }else if(result.error){
                  Swal.fire({
                    icon: 'warning',
                    title: result.error,
                    timer:3000
                  })
                }

              })   
              }  //para cancelar el sweetalert2
          });

  });
</script>
@endsection




  