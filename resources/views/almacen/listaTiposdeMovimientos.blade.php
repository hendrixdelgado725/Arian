@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Tipo de Movimiento')
@section('content')

@component('layouts.contenth')
    @slot('titulo')
      Listado Tipo de Movimiento
    @endslot
@endcomponent

<section class="content">
    <div class="container-fluid">
    <!-- <div class="row justify-content-center"> -->
      <!-- <div class="col-12"> -->
    <div class="card">
      @include('vendor/flash.flash_message')
        <div class="card-header">
            <h3 class="card-title"><b></b></h3>
                <div class="card-tools">
                  <div class="row">
                    <div class="col-sm-4 mt-2">
                      @can('create',App\permission_user::class) 
                        <a href="{{route('nuevoTipoMovimiento')}}" class="btn btn-info"><b>REGISTRAR</b></a>
                      @endcan
                    </div>

                    <div class="col mt-2">
                      <form action="{{route('buscarMovimiento')}}" method="POST">
                        @csrf
                        <div class="input-group">
                          <input type="text" name="filtro" placeholder="Buscar" class="form-control float-right">
                          <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                          
                        </div>
                      </form>
                    </div>
                  </div>
                </div> 
        </div>
              <!-- /.card-header -->
        <div class="card-body table-responsive p-0 text-center">
            <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                     <th>ID</th>
                      <!-- <th>Código</th> -->
                      <th>Descripci&oacute;n</th>
                      <th>Acci&oacute;n</th>
                    </tr>
                  </thead>
                  <tbody >
                    @php
                        $i = 1
                    @endphp
                   @foreach($movi as $movie)
                     @php
                        $rand  = rand(1, 9999);
                        $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                        $enc=Helper::EncriptarDatos($nrand.'-'.$movie->id);
                        $borr=Helper::EncriptarDatos($nrand.'|'.$movie->id.'|'.$movie->codigoid)
                      @endphp
                      <tr>
                      <td style="text-align: center;">{{ $i }}</td>
                      <!-- <td>{{-- $movie->codtipent --}}</td> -->
                      <td style="text-align: center;">{{ $movie->destipent }}</td>                     
                      <td>
                        <div class="button_action" style="text-align:center">
                          @can('edit',App\permission_user::class) 
                          <a title="Edición" href="{{ route('editarTdeMovimientoForm',$enc)}}"><i class="fa fa-edit blue"></i></a>
                          /
                          @endcan
                          @can('delete',App\permission_user::class) 
                          <a href="{{ route('eliminarTipoMovimiento', $borr)}}" id="deletetipmovimiento" 
                          data-id="{{$movie->id}}" class="borrarmovimiento">
                            <i class="fas fa-trash red"></i>
                          </a>
                          @endcan
                        </div>
                      </td>
                    </tr>
                  
                    @php
                        $i++;
                    @endphp
             
                  @endforeach
                  </tbody>
            </table>
                {!!$movi->render()!!}
                {{-- col8  --}}
          </div>
      </div>
        <!-- </div> -->
    <!-- </div> -->
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section>

@endsection

@section('script')
<script src="{{asset('js/funciones.js')}} "></script>
@endsection

