@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Registro del Almacen')



  @section('content')
    @component('layouts.contenth')
    @slot('titulo')
      Ingresar Precio
    @endslot
  @endcomponent
  <section class="content-wrapper">
    <div class="content-fluid">

    <div class="container">

        <div class="col-10">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><b>INGRESAR PRECIO DEL ARTICULO</b></h3>

              </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form action="{{route('ingresarPrecioP.Almacen',$articulos->codart)}}" method="post">
               {{csrf_field()}}
               <div class="card-body">
                 
              
                 <div class="row ">
                  <div class="form-group col-sm-6">
                    <label class="ml-1">Codigo del Articulo:  </label>{{ $articulos->codart }}
                  </div>
                  <div class="col-sm-12 row col-sm-6">
                  <div class="form-group ">
                  <label class="ml-2">Descripci&oacute;n del Art&iacute;culo:  </label>{{ $articulos->desart }}
                  </div>
                 </div>


                </div>
             <div class="col-sm-12 row ">
                <div class="form-group col-sm-6 ">
                <label>Precio</label>
                <div class="input-group mb-3 ">
                  <input class="form-control" type="text" onKeypress = "javascript:return NumerosReales(event)"  name="precio" value="{{$precio ?? ''}}" placeholder="0.00">
                
                </div>
              
              </div>

                {{-- <div class="form-group col-sm-6 ">
                  <label>Ingresar Nuevo precio </label>
                  <div class="input-group mb-3 ">
                    <div class="input-group-prepend">
                      <!-- <span class="input-group-text">Talla</span> -->
                    </div>
                  <input class="form-control" type="text" onKeypress = "javascript:return NumerosReales(event)" name="precio" value="" placeholder="0.00">
                  </div>
                  @error('precio')
                    <div class="alert alert-danger">{!!$message!!}</div>
                  @enderror
                  </div> --}}
             </div>
             

             <div class="col-sm-12 row">
              {{-- <div class="form-group col-sm-6" >
              <label>Moneda</label>
              <div class="input-group mb-3 ">
                <div class="input-group-prepend">
                  <!-- <span class="input-group-text">Talla</span> -->
                </div>
                <input class="form-control" type="text"  disabled name="precioActual" value="{{$articulos->moneda ? $articulos->moneda->nombre  : ''}}">
              </div>
          
              </div> --}}

              <div class="form-group col-sm-6" >
                <label>Moneda</label>
                <div class="input-group mb-3 ">
                  <div class="input-group-prepend">
                    <!-- <span class="input-group-text">Talla</span> -->
                  </div>
                  <select class="form-control select2" name="monedas">
                    @foreach ($moneda as $key)
                      @php
                        if($articulos->moneda){
                          $selected = $articulos->costoPro->codmone === $key->codigoid ? 'selected' : '';
                        }
                      @endphp
                      
                      <option value="{{$key->codigoid}}" {{$selected ?? ''}}>{{$key->nombre}}</option>
                    @endforeach
                  </select>
                </div>
                @error('monedas')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

             </div>
             {{-- @php
                 dd($articulos->almacen);
             @endphp --}}
{{--              <div class="col-sm-12 row">
                <div class="form-group col-sm-6">
                  <label>Almac&eacute;n </label>
                  <div class="input-group mb-3">
                    <input class="form-control" type="text"  disabled name="precioActual" value="{{$articulos->almacen2 ? $articulos->almacen2->nomalm : ''}}">
                  </div>
                </div>

                <div class="form-group col-sm-6">
                  <label>Almac&eacute;n </label>
                <div class="input-group mb-3">
                 <select class="form-control select2" name="alm">
                   <option value="0" selected disabled>Seleccione Un Almacen</option>
                   @foreach ($alma as $key)
                       <option value="{{ $key->codalm}}">{{ $key->nomalm}}</option>
                   @endforeach
                 </select>
                </div>
              
                  @error('alm')
                    <div class="alert alert-danger">{!!$message!!}</div>
                  @enderror
                </div>

             </div> --}}

                  <button id="saveSubmit" type="submit" class="btn btn-success"><b>Guardar Cambios</b></button>

                 <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-chevron-circle-left"></i> Volver</a>
                </div>

                </div>

                <!-- /.card-body -->


              </form>
            </div>

            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->
    </div>
  </section>
  @endsection

  <script type="text/javascript" src="{{asset('js/chequeo.js')}}">

  </script>
