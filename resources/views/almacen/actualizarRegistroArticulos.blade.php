@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Actualizar Registro de Articulos')
  @section('content')

    @component('layouts.contenth')
    @slot('titulo')
      Actualizar Articulos
    @endslot
  @endcomponent

    <section class="content">
    <div class="container-fluid">
    <div>
      
      <div class="col-sm-12">
        @include('vendor/flash.flash_message')
        <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">

                <h3 class="card-title"><b>EDITAR ARTICULOS ({{ $articulos->desart }})</b></h3>
              </div>

              <!-- /.card-header -->
              <!-- form start -->
            

                  <form  action="{!! route('editarArticulos.Almacen',$articulos->codart) !!}" method="post" enctype="multipart/form-data">
                  {{csrf_field()}}
                    <div class="card-body" >

                     <div class="row">
                     <div class="col-sm-2">
                     <div class="form-group">
                      <label for="tipodocumento">C&oacute;digo</label>
                         <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">A-1</span>
                      </div>
                      <input type="text" class="codigo form-control" readonly value="{{$articulos->codart}}" placeholder="Ejem: A0002-0642" name="codigos"
                      id="codigos" class="@error('codigos') is-invalid @enderror" maxlength="17"
                      
                      >
                    </div>
                    @error('codigos')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                     </div>
                     <div class="col-sm-4">
                      <div class="form-group">
                      <label>Nombre</label>
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text">N</span>
                        </div>
                        <input type="text" class="form-control desc " value="{{$articulos->nomart}}" placeholder="Ejem: 1234567"  name="nomart"
                        id="nomart" class="@error('nomart') is-invalid @enderror"
                      value="{{old('nomart')}}">
                      </div>
                      @error('nomart')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                      </div>
                     </div>
                   <div class="col-sm-6">
                    <div class="form-group">
                    <label>Descripci&oacute;n</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">D</span>
                      </div>
                      <input type="text" class="form-control desc" value="{{$articulos->desart}}" placeholder="Ejem: 1234567"  name="Descripcion"
                      id="Descripcion" class="@error('Descripcion') is-invalid @enderror"
                    value="{{old('Descripcion')}}">
                    </div>
                    @error('Descripcion')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                   </div>
                   </div>
                   <div class="form group detalle">

                  <div class="row">
                       <div class="col-sm-6">
                    <div class="form-group">
                    <label>Categor&iacute;a </label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <!-- <span class="input-group-text">Cat</span> -->
                      </div>
                      <select class="form-control select2 categorias" data-url="{{ route('listadoArticulos.Almacenista') }}" data-url2="{{ route('listadoArticulos.getTallas') }}" name="nomcategoria">
                        <option value="" selected>Seleccione Categoria</option>
                      @foreach($categoria as $key)
                        <option value="{{$key->codigoid}}"> {{ $key->nombre}}</option>

                      @endforeach
                      @if ($articulos->facategoria)
                        <option selected value="{{$articulos->codcat}}"> {{ $articulos->facategoria->nombre }}</option>
                      @endif
                      </select>
                    </div>
                    @error('nomcategoria')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                   </div>
                  </div>

                   <div class="col-sm-6">
                    <div class="form-group">
                    <label>Subcategor&iacute;a </label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <!-- <span class="input-group-text">Nomod</span> -->
                      </div>
                      <select class="form-control select2 cambioss1" name="nomsubcategorias">
                        <option value="" disabled selected>Eliges una Subcategoria</option>
                      @foreach($sub as $key)
                        <option value="{{$key->codigoid}}"> {{ $key->nomsubcat}}</option>

                      @endforeach
                      @if ($articulos->subcategoria)
                        <option selected value="{{$articulos->codsubcat}}"> {{ $articulos->subcategoria->nomsubcat }}</option>
                      @endif
                      </select>
                    </div>
                    @error('nomsubcategorias')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                   </div>

                   {{-- <div class="col-sm-6 tallas">

                    <div class="form-group ">
                    <label>Almacenes </label>
                    <div class="input-group mb-3 ">
                      <div class="input-group-prepend ">
                        <!-- <span class="input-group-text">Almacenes</span> -->
                      </div>
                     <select class="form-control select2 cambios Almacenesss" name="almacenes[]" multiple>
                     <option value="N/A"  disabled>Seleccione los Almacenes</option>
  
                      @foreach($almacenes as $key)
                        <option value="{{$key->codalm}}"> {{ $key->nomalm}}</option>
                      @endforeach
                      @if ($articulos->almacenes)
                      @foreach($articulos->almacenes as $key)
                        <option value="{{ $key->almacenes->codalm}}" selected>{{ $key->almacenes->nomalm}}</option>
                      @endforeach
                      @endif
                      </select>
                    </div>
                    @error('almacenes[]')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                   </div> --}}
                   <div class="col-sm-6 tallas">

                    <div class="form-group ">
                    <label>Tallas </label>
                    <div class="input-group mb-3 ">
                      <div class="input-group-prepend ">
                        <!-- <span class="input-group-text">tallas</span> -->
                      </div>
                     <select class="form-control select2 cambios tallasss" name="tallas[]" multiple>
                     <option value="N/A"  disabled>Seleccione la tallas</option>
  
                      @foreach($tallas as $key)
                        <option value="{{$key->codtallas}}"> {{ $key->tallas}}</option>
                      @endforeach
                      @if ($articulos->allTallas)
                      @foreach($articulos->allTallas as $key)
                        <option value="{{ $key->tallas->codtallas}}" selected>{{ $key->tallas->tallas}}</option>
                      @endforeach
                      @endif
                      
                      </select>
                    </div>
                     {{-- <p style="color:#000; display:none;" class="infoT alert alert-warning">Tienes que colocar la talla es obligatorio</p> --}}
                    @error('tallas[]')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                   </div>
                   <div class="col-sm-6 ">
                    <div class="form-group">
                    <label>Marcas</label>
                    <div class="input-group mb-3 ">
                      <div class="input-group-prepend">
                        <!-- <span class="input-group-text">Talla</span> -->
                      </div>
                     <select class="form-control select2 " name="marca" data-placeholder="Seleccione Marca">
                      <option value="#" selected disabled>Seleccione la Marca</option>
                      @foreach($marcas as $key)
                        <option value="{{ $key->codigoid }}">{{ $key->defmodelo}}</option>
                      @endforeach
                      @if ($articulos->getMarca)
                        <option selected value="{{ $articulos->getMarca->codigoid }}">{{ $articulos->getMarca->defmodelo }}</option>
                      @endif
                      </select>
                    </div>
                    @error('marca')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                   </div>

                   <div class="col-sm-6">
                    <div class="form-group">
                    <label>Cuenta Venta Contado:</label>
                    <div class="input-group mb-3">
                      <input type="text" readonly class="form-control ccosto" name="cCosto"
                      id="cedularifcliente" class="@error('merma') is-invalid @enderror"
                      value="{{$articulos->codcta}}"><button type="button" onclick="modal()" class="btn btn-black"  name="button">...</button>
                    </div>
                    @error('cCosto')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                   </div>
    
                   <div class="col-sm-6">
                    <div class="form-group">
                    <label>Cuenta Costo:</label>
                    <div class="input-group mb-3">
                      <input type="text" readonly class="form-control cTransitoria" name="cTransitoria"
                      id="cedularifcliente" class="@error('cTransitoria') is-invalid @enderror"
                      value="{{$articulos->ctatra}}"><button type="button" onclick="modal1()" class="btn btn-black"  name="button">...</button>
                    </div>
                    @error('cTransitoria')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                   </div>
    
                   <div class="col-sm-6">
                    <div class="form-group">
                    <label>Cuenta Ingresos Por Venta:</label>
                    <div class="input-group mb-3">
                      <input type="text" readonly class="form-control cgasto"  name="cGasto"
                      id="cedularifcliente" class="@error('cTransitoria') is-invalid @enderror"
                      value="{{$articulos->ctadef}}">
                    <button type="button" onclick="modal2()" class="btn btn-black"  name="button">...</button>
                    </div>
                    @error('cTransitoria')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                   </div>

                   <div class="col-sm-6">
                    <div class="form-group">
                    <label>Color</label>
                      <select class="form-control select2" name="nomcolor">
                          <option value=" " selected>Seleccione un Color</option>
                        @foreach($color as $key)
                          <option value="{{$key->codigoid}}"> {{ $key->color }}</option>
                        @endforeach
                        @if ($articulos->color)
                          <option value="{{$articulos->color->codigoid}}" selected> {{ $articulos->color->color }}</option>
                        @endif
                        </select>
                    @error('nomcolor')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                   </div>

                  </div>

                 
               

                   {{-- <div class="col-sm-6 ">
                    <div class="form-group">
                    <label>Precio</label>
                    <div class="input-group mb-3 ">
                      <div class="input-group-prepend">
                        <!-- <span class="input-group-text">Talla</span> -->
                      </div>
                     <input class="form-control" type="text" name="precio" value="{{$articulos->faartpvp->pvpart ?? ''}}">
                    </div>
                    @error('marca')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                   </div> --}}

                   {{-- <div class="col-sm-6 ">
                    <div class="form-group">
                    <label>Moneda</label>
                    <div class="input-group mb-3 ">
                      <div class="input-group-prepend">
                        <!-- <span class="input-group-text">Talla</span> -->
                      </div>
                      <select class="form-control select2" name="monedas">
                        <option value="" selected disabled>Seleccione una Moneda</option>
                        @foreach ($moneda as $key)
                          <option value="{{$key->codigoid}}">{{$key->nombre}}</option>
                        @endforeach
                        @if ($articulos->moneda)
                          <option selected value="{{$articulos->moneda->codigoid}}">{{$articulos->moneda->nombre}}</option>
                        @endif
                      </select>
                    </div>
                    @error('marca')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                   </div> --}}

                   {{-- <div class="col-sm-6">
                    <div class="form-group">
                    <label>Almacen </label>
                    <div class="input-group mb-3">

                       <select class="form-control select2" name="alm">
                         <option selected >Seleccione Un Almacen</option>
                         @foreach ($alma as $key)
                             <option selected>{{ $key->nomalm}}</option>
                         @endforeach
                         @if ($articulos->almacen)
                           <option selected value="{{$articulos->almacen->codalm}}" >{{ $articulos->almacen->nomalm}}</option>

                         @endif
                       </select>
                    </div>
                    @error('cantidad')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                   </div> --}}

                   {{-- <div class="col-sm-6">
                    <div class="form-group">
                    <label>Foto </label>
                    <div class="input-group mb-3">
                      <div class="input-group">
                        <label class="input-group-btn">
                      <span class="btn btn-primary btn-file">
                                Banner <input accept=".jpg,.png,.jpeg,.gif" class="hidden" name="banner" type="file" id="banner">
                            </span>
                        </label>
                        <input class="form-control" id="banner_captura" readonly="readonly" name="banner_captura" type="text" value="">
                    </div>
                    </div>
                    @error('banner_captura')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                   </div> --}}

                   <div class="col-sm-6">
                    <div class="form-group">
                    <label>Codigo Partida </label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">0-9</span>
                      </div>
                        <input class="form-control partidas" readonly  onKeypress = "javascript:return NumerosReales(event)"  name="partidas" type="text" value="{{$articulos->codpar}}">
                        <button type="button" class="btn btn-black" onclick="codpartida()" name="button">...</button>
                    </div>
                    @error('cantidad')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                   </div> 

                   <div class="col-sm-6">
                    <div class="form-group">
                    <label>Codigo N/P</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">0-9</span>
                      </div>
                        <input class="form-control codigo"  name="np" type="text" value="{{$articulos->codsgp}}">
                    </div>
                    @error('cantidad')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                   </div> 
                   <div class="col-sm-6">
                    <div class="form-check">
                      <input class="form-check-input" type="radio" onclick="showSerial(this.value)" {{$articulos->tipo === 'COMPRA' ? 'checked' : ''}} name="tipo" id="flexRadioDefault1" value="A">
                      <label class="form-check-label" for="flexRadioDefault1">
                        Compra
                      </label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input" type="radio" onclick="showSerial(this.value)" {{$articulos->tipo === 'SERVICIO' ? 'checked' : ''}} name="tipo" id="flexRadioDefault2" value="S">
                      <label class="form-check-label" for="flexRadioDefault2">
                        Servicio
                      </label>
                    </div>
                    @error('tipo')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                   </div>
                   <div class="col-sm-6">
                    <div class="form-group">
                      <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                        <input type="checkbox" {{$articulos->tipreg === 'F' ? 'checked' : ''}}  value="{{$articulos->tipreg === 'F' ? 'F' : null}}" onclick="checkbox(this.id)" class="custom-control-input" id="customSwitch3" name="pfactur">
                        <label class="custom-control-label" id="customSwitch4" for="customSwitch3">Articulo Predeterminado Para Facturar : {{$articulos->tipreg === 'F' ? 'Si' : 'No'}}</label>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group" id="serialCheck">
                      <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                        <input type="checkbox" {{$articulos->isserial === true ? 'checked' : ''}}  value="{{$articulos->isserial === true ? true : false}}" onclick="checkbox(this.id)" class="custom-control-input" id="serial" name="pSerial">
                        <label class="custom-control-label" id="customSwitch45" for="serial">Articulo tiene Serial : {{$articulos->isserial === true ? 'Si' : 'No'}} </label>
                      </div>
                    </div>
                  </div>

                   <div class="col-sm-6 ">
                    <div class="form-group">
                      
                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                     <input type="checkbox" {{ $articulos->ivastatus === "true" ? 'checked' : ''}} value="{{ $articulos->ivastatus }}" onclick="checkboxIva(this.id)" class="custom-control-input" id="iva" name="iva">
                     <label class="custom-control-label" id="customSwitch45" for="iva">Iva {{$recargos.'%'}}</label>
                    </div>
                    @error('marca')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                   </div>

                  </div>


                   </div>
                   <div class="card-footer">
                    <button id="saveSubmit" type="submit" class="btn btn-success"><b>Guardar Cambios</b></button>

                     <a href="{{ url('/modulo/Almacen/Artculos') }}" class="btn btn-default"><i class="fa fa-chevron-circle-left"></i> Volver</a>
                    </div>
                   </div>
                <!-- /.card -->
               </div>
                <!-- /.col-lg-6 -->
              </div>
                <!-- /.row -->

             {{-- {!!Form::close()!!}    --}}
          </form>
        </div>
            <!-- /.card -->
       </div>
      </div>
      </div>
  </section>
  

  @endsection
  <div class="modal fade" id="modalempleado" tabindex="10" role="dialog" aria-labelledby="modalempleado" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
   <div class="modal-content">
   <div class="modal-header label">
     <h2>Listado</h2>
     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
     </button>
   </div>

    <div class="modal-body container">
      <table id="datos" class="table" style="width:100%">
       <thead class="thead-dark" >
       <tr>
           <th>Codigo</th>
           <th>Descripcion</th>
           <th>Accion</th>
         </tr>
        </thead>


      </table>

    </div>
  <div class="modal-footer">
     <button type="button" class="btn btn-primary" data-dismiss="modal"><b> Cerrar</b></button>
    </div>
  </div>
</div>
</div>
<div class="modal fade" id="modalempleado2" tabindex="10" role="dialog" aria-labelledby="modalempleado" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
   <div class="modal-content">
   <div class="modal-header label">
     <h2>Listado de Presupuesto</h2>
     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
     </button>
   </div>

    <div class="modal-body container">
      <table id="datos2" class="table" style="width:100%">
       <thead class="thead-dark" >
       <tr>
           <th>Codigo</th>
           <th>Descripcion</th>
           <th>Accion</th>
         </tr>
        </thead>


      </table>

    </div>
  <div class="modal-footer">
     <button type="button" class="btn btn-primary" data-dismiss="modal"><b> Cerrar</b></button>
    </div>
  </div>
</div>
</div>
  @section('mayuscula')
    <script >
      window.onload = function() {
        var id;
        document.getElementsByName('tipo').forEach(element => {
          if(element.checked) {
            id = element.value;
          }
        });
        showSerial(id);
      };

function checkbox(id) {
      var checked = $('#'+id).is(':checked')
        if (checked) {
            if(id === 'serial'){
              $('#customSwitch45').html('Articulo tiene Serial :Si ');
              $('#'+id).val(true);
            }else{
              $('#customSwitch4').html('Articulo Predeterminado Para Facturar : Si');
              $('#'+id).val('F');
            }
        }else{
          if(id === 'serial'){
              $('#customSwitch45').html('Articulo tiene Serial : No ');
            }else{
              $('#customSwitch4').html('Articulo Predeterminado Para Facturar : No');
            }
            
        }
    }


    function checkboxIva(id) {
      var checked = $('#'+id).is(':checked')
        if (checked) {
            if(id === 'iva'){
              $('#'+id).val("true");
            }else{

              $('#'+id).val('False');
            }
        }
    }
    
    function showSerial(id){
      var serialCheck = document.getElementById('serialCheck');
      var serial = document.getElementById('serial');
      console.log(serial)
      if(id === 'A'){
        serialCheck.style.display = 'inline';
      }
      else {
        serialCheck.style.display="none";
        serial.checked = false;
        checkbox('serial')
      }
    }
    
    function getPartidas(id) {
      $('.partidas').val(id);
      $('#modalempleado2').modal('hide');
    }
function codpartida(){
  $('#modalempleado2').modal({ backdrop: "static", keyboard: false });
  $('#datos2').DataTable({
          serverSide: true,
          processing:true,
          ajax: {
            url: '{{route('getPartida.Almacen')}}',
            type: 'GET'
          },columns:[
            {data:'codpre'},
            {data:'nompre'},
            {data:'action'}
          ],
          destroy:true,
          searching: true,
      });
}

function modal(){
      $('#modalempleado').modal({ backdrop: "static", keyboard: false });
      $('#datos').DataTable({
          serverSide: true,
          processing:true,
          ajax: {
            url: '{{route('getContabb.Almacen')}}',
            type: 'GET'
          },columns:[
            {data:'codcta'},
            {data:'descta'},
            {data:'action'}
          ],
          destroy:true,
          searching: true,
      });


    }

    function modal1(){
      $('#modalempleado').modal({ backdrop: "static", keyboard: false });
      $('#datos').DataTable({
          serverSide: true,
          processing:true,
          ajax: {
            url: '{{route('getContabb1.Almacen')}}',
            type: 'GET'
          },columns:[
            {data:'codcta'},
            {data:'descta'},
            {data:'action'}
          ],
          destroy:true,
          searching: true,
      });
    }


    function modal2(){
      $('#modalempleado').modal({ backdrop: "static", keyboard: false });
      $('#datos').DataTable({
          serverSide: true,
          processing:true,
          ajax: {
            url: '{{route('getCideftit')}}',
            type: 'GET'
          },columns:[
            {data:'codpre'},
            {data:'nompre'},
            {data:'action'}
          ],
          destroy:true,
          searching: true,
      });
    }


    function getId(id) {
      $('.ccosto').val(id);
      $('#modalempleado').modal('hide');
    }

    function getId2(id) {
      $('.cTransitoria').val(id);
      $('#modalempleado').modal('hide');
    }

    function getId3(id) {
      
      $('.cgasto').val(id);
      $('#modalempleado').modal('hide');

              $.get(url,function(result) {
                 if (result.titulo === 'Existe El Codigo Registrado') {
                   Swal.fire({
                   title: 'Existe El Codigo Registrado',
                   text: "Registra Con Otro Codigo",
                   icon: 'warning',
                   showCancelButton: false,
                   confirmButtonColor: '#3085d6',
                   cancelButtonColor: '#d33',
                   confirmButtonText: 'Entendido'
                 });
               }else {

                 Swal.fire({
                 title: 'No Existe El Codigo Registrado',
                 text: "Se Puede Registra Con Este Codigo",
                 icon: 'success',
                 showCancelButton: false,
                 confirmButtonColor: '#3085d6',
                 cancelButtonColor: '#d33',
                 confirmButtonText: 'Entendido'
               });


               }



              });
          }


      $(document).ready(function(){
        $('.form-controls').keyup(function(tecla) {
            var texto =  $(this).val().toUpperCase();
            $(this).val(texto);
        });

        $('.codigo').keyup(function(tecla){

          var texto = $(this).val().toUpperCase().replace(/ /g,"");

          $(this).val(texto);
          });

          $('.desc').keyup(function(t){
               var texto = $(this).val().toUpperCase();
               $(this).val(texto);

          });

        // $(document).on('change','.btn-file :file',function(){
        //   var input = $(this);
        //   var numFiles = input.get(0).files ? input.get(0).files.length : 1;
        //   var label = input.val().replace(/\\/g,'/').replace(/.*\//,'');
        //   input.trigger('fileselect',[numFiles,label]);
        // });
        //
        // $('.btn-file :file').on('fileselect',function(event,numFiles,label){
        //   var input = $(this).parents('.input-group').find(':text');
        //   var log = numFiles > 1 ? numFiles + ' files selected' : label;
        //   if(input.length){ input.val(log); }else{ if (log) alert(log); }
        // });

         $('.select2').select2();


          $('select.categorias').change(function(){

              var url = $(this).data('url')+'/'+$(this).val();

              $.get(url,function (result) {

                //console.log(result.subcategoria);
                $('select.cambioss1').html(result.subcategoria);

              });

            var url2 = $(this).data('url2')+'/'+$(this).val();
              $.get(url2,function(result) {

                  if (result.tallas) {
                    $('.infoT').css('display','block');
                  }else if(result.tallas === false || result.tallas === null){
                    $('.infoT').css('display','none');
                  }
              });

               if ( $(this).val() === '3-0002' || $(this).val() === '4-0002') {

                $('div.muestra').hide();//oculta el campo talla

               }else{   

                $('div.muestra').show();//muestra el campo talla

               }


        });

      });

    </script>
  @endsection
