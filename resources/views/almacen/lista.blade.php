@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Listado de Almacenes')


@section('content')

@component('layouts.contenth')
    @slot('titulo')
      Listado de Almac&eacute;n
    @endslot
  @endcomponent
    <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">

            <div class="card">
              @include('vendor/flash.flash_message')

              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">

                   @can('create',App\permission_user::class)
                      
                   <div class="col-sm-4 mt-2">
                     <a href="{{ route('Registro.Almacen') }}" class="btn btn-info"><b>REGISTRAR</b></a>
                  </div>
                   @endcan
               <br>
               <div class="col">
                  <div class="row">
                    <div class="col mt-2">
                      
                          
                        <form action="{{ route('Filtro.Almacen')}}" method="post">
                            <div class="input-group">
                          @csrf
                          <input type="text" placeholder="Buscar " name="filtro"  class="form-control float-right">
                          <button type="submit" style="height: 38px; width: 38px" class="btn btn-default btn-sm float-left"><i class="fas fa-search"></i></button>
                          <a href="{{route('Listado.Almacen')}}" class="btn btn-default"><i class="fas fa-arrow-left"></i></a>
                           </div>
                        </form>
                      
                    </div>
                  </div>
                 </div>
                
                 </div><!--row -->
                </div>
              </div>

              <!-- /.card-header -->
              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>ID</th>
                      {{-- <th>Codigo del Almacen</th> --}}
                      <th>Descripción</th>
                      <th>Direcci&oacute;n</th>
                      <th>Sucursal Asociada</th>
                      <th>Predeterminado a Facturar</th>
                      <th>Acción</th>
                    </tr>
                  </thead>
                  @php
                  $i = 1;    
                  @endphp
                  <tbody style="text-align:center">
                  @foreach($almacen as $alma)
                     @php
                    $rand  = rand(1, 9999);
                    $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                    $enc=Helper::EncriptarDatos($nrand.'-'.$alma['id']);    
                     @endphp 

                      
                      <tr style="text-align: center;">
                      
                      <td>{{$i}}</td>
                      {{-- <td>{{$alma->codalm}}</td>  --}}
                      <td>{{$alma->nomalm}}</td>
                      <td>{{$alma->diralm}}</td>      
                      <td>{{$alma->codsuc_asoc ? $alma->getNombreSucursal() : '-'}}</td>
                      <td>{{$alma->pfactur ? 'Si' : 'No'}}</td>
                      <td>
                       @can('edit',App\permission_user::class)
                         <a  href="{{ route('ActualizarDatos.Almacen', ['id'=>$enc]) }}">
                          <i class="fa fa-edit blue"></i>
                        </a>
                        /
                        @endcan
                         @can('delete',App\permission_user::class)                   
                         <a class="borrar" href="{{ route('Eliminar.Almacen',['id'=>$enc]) }}">
                          <i class="fa fa-trash red"></i>
                        </a>     
                        @endcan
                          
                      </td>
                    </tr>
                  

                    @php
                        $i++;
                    @endphp
                  @endforeach
                  </tbody>
                  
                </table>
                {!!$almacen->render()!!}
                {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}


@endsection
@section('lista')
  
<script>
  
  $(document).ready(function() {
  
    $('.borrar').click(function(e){
        e.preventDefault();
          Swal.fire({
          title: 'Seguro que Desea Eliminar?',
          text: "Despues de esto no se encontrara en la lista!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, Eliminar!'
        }).then((result) => {
            
          if (result.value) {
            
            
            var row = $(this).parents('tr');
            //var form = $(this).parents('data-id');
            var url = $(this).attr('href');
            console.log(url);
            /*console.log(form.serialize());*/
            $.get(url,row,function(result1){
              
              console.log(result1)
              if(result1.total === 'SE HA ELIMINADO CON EXITO'){
                Swal.fire(
                'Ha sido Eliminado!',
                'Con Exito.',
                'success',
  
              )
              
                row.fadeOut();

              }else{
                Swal.fire({
                  icon: 'warning',
                  title: result1.total,
                  //footer: '<a href>Why do I have this issue?</a>'
                  timer:2000
                })
              }

              /*$('#tablas').html(result1.total);*/
            })
           }else{
            Swal.fire({
            icon: 'warning',
            title: 'No ha sido Eliminado',
            //footer: '<a href>Why do I have this issue?</a>'
            timer:1500
          })
           }
          
        });

    });
});
</script>
@endsection
