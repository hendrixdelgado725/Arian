@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Detalles de Nota Entrega')
@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<div class="container">
  <div class="row">
    <div class="col-sm-11">
      <!-- Main content -->
      <div class="invoice p-1 mb-3">
        <!-- Encabezado Enmarcado-->
        <div class="border border-dark mr-1">
          <div class="col-11 d-flex justify-content-center" style="margin-bottom: -5px;">
            <h4>NOTA DE ENTREGA N° {{$nota->numero}}</h4>
          </div>
        </div>
        <!-- /. Fin del Encabezado Enmarcado -->

        <!-- Encabezado-->
        <!-- Fin del Encabezado--> 

        <div class="row invoice-info">
         @if($nota->cod_cliente)
          <div class="col-sm-6 invoice-col border-left">
            <div class="col-12 mt-4">
             <b>CLIENTE:</b> {{ $nota->cliente->nompro }}
            </div>
            <div class="col-12 mt-4">
             <b>CEDULA / RIF:</b> {{ $nota->cod_cliente }}
            </div>
            <div class="col-12 mt-4">
             <b>DIRECCIÓN: </b> {{ $nota->cliente->dirpro }}
            </div>
          </div>
          <!-- /.col -->
          <div class="col-sm-6 invoice-col border-left">
            <div class="col-12 mt-4">
             <b>TELEFONO:</b> {{ $nota->cliente->telpro }}
            </h8>
          </div>
          <div class="col-12 mt-4">
           <b>CORREO ELECTRONICO:</b> {{ $nota->cliente->email }}
          </div>
        </div>
        @else
          <div class="col-sm-6 invoice-col border-left">
            <div class="col-12 mt-4">
             <b>ALMACEN:</b> {{ $nota->almdestino->nomalm }}
            </div>
            <div class="col-12 mt-4">
             <b>CODIGO ALMACEN</b> {{ $nota->almdes }}
            </div>
            <div class="col-12 mt-4">
             <b>DIRECCIÓN: </b> {{ $nota->almdestino->diralm }}
            </div>
          </div>
        @endif
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table Primary -->
      <div class="row">
        <div class="col-12 mt-4 table-responsive" style="margin-bottom:-10px;">
          <table class="table table-sm">
            <thead>
              <tr style="text-align:center">
                <th>CREADO EL</th>
                <th>ELABORADO POR</th>
              </tr>
              <tr style="text-align:center">
                <td>{{ $nota->created_at }}</td>
                <td>{{ $nota->autor->nomuse }}</td>
              </tr>
            </thead>
          </table>
        </div>
      </div>

      <!-- Table Secundary -->
      <div class="row" style="margin-top: -10px;">
        <div class="col-12 mt-4 table-responsive">
                  <div class="table-header">
          <h6 class="table-title m-0 mt-4 mb-4" style="text-align:center"><b>ARTICULOS</b></h6>
          </div>
          <table class="table table-sm">
            <thead>
              <tr style="text-align:center">
                <th>CODIGO</th>
                <th>DESCRIPCIÓN</th>
                <th>TALLA</th>
                <th>CANTIDAD</th>
                <th>MONTO UNIT.</th>
                <th>MONTO</th>
                <th>MONEDA</th>
              </tr>
              @foreach ($articulos as $articulo)
              <tr style="text-align:center">
                <td>{{ $articulo->codart}}</td>
                <td>{{ $articulo->desart }}</td>
                @if($articulo->codtalla)
                <td>{{ $articulo->codtalla }}</td>
                @else
                <td>N/A</td>
                @endif
                <td>{{ $articulo->cantidad }}</td>
                <td>{{ number_format($articulo->preunit, 2, ',', '.').' '}}</td>
                <td>{{ number_format($articulo->costo, 2, ',', '.').' '}}</td>
                <td>{{ $articulo->coin_name }}</td>
              </tr>
              @endforeach
            </thead>
          </table>
        </div>
      </div>


      <div class="col-5 mt-4 mb-4" style="margin-left: 60%; margin-top: -20px;">
        <div class="table-responsive " >
          <table class="table table-sm " >
            
            <tbody>
              <tr>
               <th>Total Bs</th>
                <td><strong>{{ number_format($nota->total, 2, ',', '.')}}</strong></td>
              </tr>
              @if($nota->tasa)
              <tr>
                <th>Tasa </th>
                <td><strong>{{number_format($nota->tasa->valor, 2, ',', '.').' '}}</strong></td>
              </tr>
              @endif
              @if($nota->cambio)
              <tr>
              <th>Cambio</th>
                <td><strong>{{ number_format($nota->cambio, 2, ',', '.')}}</strong></td>
              </tr>
              @endif
            </tbody>
            
          </table>
          </div>
        </div>
        <div class="row no-print" style="margin-top: -20px; margin-bottom: -5px;">
        <div class="col-12 mb-4">
        <a href="{{ URL::previous() }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>

        </div>
        </div>
      </div>
      <!-- this row will not appear when printing -->
   <!--    <div class="row no-print card-footer">
        <div class="col-12 mb-4">
        <a href="{{ URL::previous() }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>

        </div>
      </div> -->
   </div>
   <!-- /.invoice -->
 </div>
</div>


@endsection