@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Registrar Precio')
  @section('content')

    @component('layouts.contenth')
    @slot('titulo')
      Ingresar Precio
    @endslot
  @endcomponent
    
    <section class="content-wrapper">
    <div class="container-fluid">
      <div class="container">
        
        <div class="col-10">
            <div class="card card-info">
              @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b>REGISTRO PRECIO</b></h3>
              </div>
              <form action="{{ route('ingresarPrecios.Almacen') }}" method="post">
              {{csrf_field()}}
                <div class="card-body">
                 <div class="row">
                 <div class="col-sm-6">
                 <div class="form-group">
                  <label for="tipodocumento">Art&iacute;culo</label>
                  <div class="form-group">
                    <select class="control form-control select2" id="select2" data-url = "{{ route('filtroPrecios.Almacen') }}" name="codigos">
                      <option disabled selected data-placeholder></option>
                      @foreach($articulos as $key)
                        <option  enabled class="selector" value="{{$key->codart}}"> {{ $key->desart }}</option>
                      @endforeach
                    </select>
                  @error('codigos')
                  <div class="alert alert-danger">{!!$message!!}</div>
                  @enderror
                  </div>
                </div>     
                </div> 
              </div>


                <div class="form group detalle " style="display: none;">
                <label for="nombre">Almac&eacute;n</label>
                <div class="input-group mb-3">
                  <div class="col-sm-6">
                      <!-- select -->
                      <div class="form-group">
                        
                        <select class="form-control select3" id="select2" name="nomalm">
                          <option></option>
                        @foreach($alma as $key)
                          <option value="{{$key->codalm}}"> {{ $key->nomalm }}</option>
                        @endforeach
                        </select>
                        @error('nomalm')
                          <div class="alert alert-danger">{!!$message!!}</div>
                        @enderror
                      </div>
                    </div>
                </div>
                </div>

                
              <div class="form group detalle" style="display: none;">
                <label for="telefono">Moneda</label>
                <div class="input-group mb-3">
                  <div class="col-sm-6">
                  <div class="form-group">
                        
                        <select class="form-control select3" name="monedass">
                          <option></option>
                        @foreach($monedas as $key)
                          <option value="{{$key->codigoid}}">{{ $key->nombre }}</option>
                        @endforeach
                        </select>

                @error('monedass')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                      </div>
                    </div>
                </div>
              </div>

              <div class="form group detalle " style="display: none;">
                <label for="precio">Definir Costo</label>
                <div class="input-group mb-3">
                <div class="col-sm-6">
                  <div class="form-group">
                    <input type="text" data-mask="###.###.##0,00" data-mask-reverse="true" placeholder="Ejem: 200" name="precio" id="precio" class="form-control precios @error('precio') is-invalid @enderror" autocomplete="off">
                    </div>
                    @error('precio')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    
                  </div>
                </div>
                </div>
                
           <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Guardar Cambios</button>
                   <a href="{{ url('/modulo/Almacen/Artculos') }}" class="btn btn-default"><i class="fa fa-chevron-circle-left"></i> Volver</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->

            
         </form>
        </div>
            <!-- /.card -->
     
     
  </section>
  @endsection

@section('cargar')
<script src="{{asset('js/chequeo.js')}} "></script>
<script type="text/javascript">
    $(document).ready(function(){

         $('#select2').select2({

              placeholder: "Seleccione una opción",
              width:'100%'
              
         });
        $('.control').on('change',function(){
           
           var valor = $(this).val();
           var url = $(this).data('url')+'/'+valor;

           console.log(url)
           
           $.get(url,function(resultado) {
             
             $('input.precio').val(resultado.titulo);

           });

            $('.detalle').show(); 

            $('.select3').select2({

              placeholder: "Seleccione una opción",
              
         });

        });

    });

  </script>
@endsection
