@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Registrar Articulos')
  @section('content')
    @component('layouts.contenth')
    @slot('titulo')
      Registrar Articulos
    @endslot
  @endcomponent

    <section class="content" >
    <div class="container-fluid">
    <div>

        <div class="col-sm-12">
        @include('vendor/flash.flash_message')

            <!-- general form elements -->
            <div class="card card-info" style="padding-right: -1000px">
              <div class="card-header">
                <h3 class="card-title"><b>REGISTRO ARTICULO</b></h3>

              </div>
              <!-- /.card-header -->
              <!-- form start -->
             {{-- {!! Form::open(['route'=>'RegistrarArticulos.Almacen','method'=>'POST','files'=>'TRUE'],'id'=> 'saveArticulo') !!} --}}
              <form  action="{!! route('RegistrarArticulos.Almacen') !!}" method="post" enctype="multipart/form-data">
              {{csrf_field()}}
                <div class="card-body" >

                 <div class="row">
                  <div class="col-sm-2">
                  <div class="form-group">
                    <label for="tipodocumento">C&oacute;digo</label>
                      <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text">A-1</span>
                    </div>
                    <input type="text" class="codigo form-control" onblur="search(this.value)" placeholder="Ejem: A0002-0642" name="codigos"
                    id="codigos" class="@error('codigos') is-invalid @enderror" maxlength="17"
                    value="{{old('codigos')}}"
                    >
                  </div>
                  @error('codigos')
                    <div class="alert alert-danger">{!!$message!!}</div>
                  @enderror
                  </div>
                  </div>

                  <div class="col-sm-4">
                    <div class="form-group">
                    <label>Nombre</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">N</span>
                      </div>
                      <input type="text" class="form-control desc" placeholder="Ejem: Articulo"  name="nomart"
                      id="nomart" class="@error('nomart') is-invalid @enderror"
                    value="{{old('nomart')}}">
                    </div>
                    @error('nomart')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                    <label>Descripci&oacute;n</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">D</span>
                      </div>
                      <input type="text" class="form-control desc" placeholder="Ejem: Nuevo"  name="Descripcion"
                      id="Descripcion" class="@error('Descripcion') is-invalid @enderror"
                    value="{{old('Descripcion')}}">
                    </div>
                    @error('Descripcion')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                  </div>
               </div>




               <div class="form group detalle">

              <div class="row">
                   <div class="col-sm-6">
                <div class="form-group">
                <label>Categor&iacute;a </label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <!-- <span class="input-group-text">Cat</span> -->
                  </div>
                  <select class="form-control select2 categorias" data-url="{{ route('listadoArticulos.Almacenista') }}" 
                          data-url2="{{ route('listadoArticulos.getTallas') }}" name="nomcategoria">
                    <option value="" selected>Seleccione Categoria</option>
                  @foreach($categoria as $key)
                    <option value="{{$key->codigoid}}"> {{ $key->nombre}}</option>
                  @endforeach
                  </select>
                </div>
                @error('nomcategoria')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
               </div>
              </div>

               <div class="col-sm-6">
                <div class="form-group">
                <label>Subcategor&iacute;a </label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <!-- <span class="input-group-text">Nomod</span> -->
                  </div>
                  <select class="form-control select2 cambioss1" name="nomsubcategorias">
                    <option value="" disabled selected>Elige una Subcategoria</option>
                  @foreach($sub as $key)
                    <option value="{{$key->codigoid}}"> {{ $key->nomsubcat}}</option>
                  @endforeach
                  </select>
                </div>
                @error('nomsubcategorias')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
               </div>

               <div class="col-sm-6 ">
                <div class="form-group">
                <label>Marcas</label>
                <div class="input-group mb-3 ">
                  <div class="input-group-prepend">
                    <!-- <span class="input-group-text">Talla</span> -->
                  </div>
                 <select class="form-control select2 " name="marca" data-placeholder="Seleccione Marca">
                  <option value="#" selected disabled>Seleccione la Marca</option>
                  @foreach($marcas as $key)
                    <option value="{{ $key->codigoid }}">{{ $key->defmodelo}}</option>
                  @endforeach
                  </select>
                </div>
                @error('marca')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
               </div>
               <div class="col-sm-6">
                <div class="form-group">
                <label>Cuenta Venta Contado:</label>
                <div class="input-group mb-3">
                  <input type="text" readonly class="form-control ccosto" name="cCosto"
                  id="cedularifcliente" class="@error('merma') is-invalid @enderror"
                value=""><button type="button" onclick="modal()" class="btn btn-black"  name="button">...</button>
                </div>
                @error('cCosto')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
               </div>

               <div class="col-sm-6">
                <div class="form-group">
                <label>Cuenta Costo:</label>
                <div class="input-group mb-3">
                  <input type="text" readonly class="form-control cTransitoria" name="cTransitoria"
                  id="cedularifcliente" class="@error('cTransitoria') is-invalid @enderror"
                value=""><button type="button" onclick="modal1()" class="btn btn-black"  name="button">...</button>
                </div>
                @error('cTransitoria')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
               </div>

               <div class="col-sm-6">
                <div class="form-group">
                <label>Cuenta Ingresos Por Venta:</label>
                <div class="input-group mb-3">
                  <input type="text" readonly class="form-control cgasto" name="cGasto"
                  id="cedularifcliente" class="@error('cTransitoria') is-invalid @enderror"
                value="">
                <button type="button" onclick="modal2()" class="btn btn-black"  name="button">...</button>
                </div>
                @error('cTransitoria')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
               </div>
               <div class="col-sm-6">
                <div class="form-group">
                <label>Color</label>
                  <select class="form-control select2" name="nomcolor">
                      <option value=" " selected>Seleccione un Color</option>
                    @foreach($color as $key)
                      <option value="{{$key->codigoid}}"> {{ $key->color }}</option>
                    @endforeach
                    </select>
                @error('nomcolor')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
               </div>

              </div>
                {{-- <div class="col-sm-6 tallas">
                <div class="form-group ">
                <label>Almacenes</label>
                <div class="input-group mb-3 ">
                  <div class="input-group-prepend ">
                    <!-- <span class="input-group-text">Talla</span> -->
                  </div>
                 <select class="form-control select2 cambios almacenes" name="almacenes[]" multiple>
                 <option value="" selected disabled>Seleccione los almacenes</option>

                 @foreach($almacenes as $key)
                    <option value="{{$key->codalm}}"> {{ $key->nomalm}}</option>
                  @endforeach 
                  </select>
                </div>
                 <p style="color:#000; display:none;" class="infoT alert alert-warning">Tienes que colocar la talla es obligatorio</p>
                @error('almacenes')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
               </div> --}}
               <div class="col-sm-6 tallas">
                <div class="form-group ">
                <label>Tallas</label>
                <div class="input-group mb-3 ">
                  <div class="input-group-prepend ">
                    <!-- <span class="input-group-text">Talla</span> -->
                  </div>
                 <select class="form-control select2 cambios tallas" name="tallas[]" multiple>
                 <option value="" selected disabled>Seleccione las tallas</option>

                 @foreach($tallas as $key)
                    <option value="{{$key->codtallas}}"> {{ $key->tallas}}</option>
                  @endforeach 
                  </select>
                </div>
                 <p style="color:#000; display:none;" class="infoT alert alert-warning">Tienes que colocar la talla es obligatorio</p>
                @error('tallas')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
               </div>
               <div class="col-sm-6">
                <div class="form-group">
                <label>Codigo Partida </label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">0-9</span>
                  </div>
                    <input class="form-control partidas" readonly  onKeypress = "javascript:return NumerosReales(event)"  name="partidas" type="text" value="">
                    <button type="button" class="btn btn-black" onclick="codpartida()" name="button">...</button>
                </div>
                @error('cantidad')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
               </div> 
               <div class="row">
                
               </div>
               <div class="col-sm-6">
                <div class="form-group">
                <label>Codigo N/P </label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">0-9</span>
                  </div>
                    <input class="form-control"  onKeypress = "javascript:return NumerosReales(event)"  name="np" type="text" value="">

                </div>
                @error('cantidad')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
               </div> 
               <div class="row" style="width: 100%">
                <div class="col-sm-6">
                  <div class="form-check">
                    <input class="form-check-input" type="radio" onclick="showSerial(this.value)" name="tipo" id="flexRadioDefault1" value="A">
                    <label class="form-check-label" for="flexRadioDefault1">
                      Compra
                    </label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" onclick="showSerial(this.value)" name="tipo" id="flexRadioDefault2" value="S">
                    <label class="form-check-label" for="flexRadioDefault2">
                      Servicio
                    </label>
                  </div>
                  @error('tipo')
                    <div class="alert alert-danger">{!!$message!!}</div>
                  @enderror
                 </div>
                 <div class="col-sm-6">
                  <div class="form-group">
                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                    <input type="checkbox" onclick="checkbox(this.id)" value="" class="custom-control-input" id="customSwitch3" name="pfactur">
                      <label class="custom-control-label" id="customSwitch4" for="customSwitch3">Articulo Predeterminado Para Facturar : No</label>
                    </div>
                  </div>
                </div>
  
                <div class="col-sm-6" >
                  <div class="form-group" id="serialCheck">
                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                    <input type="checkbox" onclick="checkbox(this.id)" value="" class="custom-control-input" id="serial" name="pSerial">
                      <label class="custom-control-label" id="customSwitch45" for="serial">Articulo tiene Serial : No</label>
                    </div>
                  </div>
               </div>
               <div class="col-sm-6">
                <div class="form-group">
                  <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                  <input type="checkbox" class="custom-control-input" id="ivastatus" name="ivastatus">
                    <label class="custom-control-label" id="customSwitch45" for="ivastatus">IVA 16%:</label>
                  </div>
                </div>
               </div>
               
              </div>

              </div>


               </div>
               
               </div>
               <div class="card-footer">
                <button id="saveSubmit" type="submit" class="btn btn-success"><b>Guardar Cambios</b></button>

                 <a href="{{ url('/modulo/Almacen/Artculos') }}" class="btn btn-default"><i class="fa fa-chevron-circle-left"></i> Volver</a>
                </div>
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->

         {{-- {!!Form::close()!!}    --}}
      </form>
        </div>
            <!-- /.card -->
       </div>
      </div>
      </div>
  </section>
  <div class="modal fade" id="modalempleado" tabindex="10" role="dialog" aria-labelledby="modalempleado" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
     <div class="modal-content">
     <div class="modal-header label">
       <h2>Listado</h2>
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
       </button>
     </div>

      <div class="modal-body container">
        <table id="datos" class="table" style="width:100%">
                           <thead class="thead-dark" >
                           <tr>
                               <th>Codigo</th>
                               <th>Descripcion</th>
                               <th>Accion</th>
                             </tr>
                            </thead>


                          </table>

                        </div>
                      <div class="modal-footer">
                       <button type="button" class="btn btn-primary" data-dismiss="modal"><b> Cerrar</b></button>
                      </div>
                    </div>
                  </div>
                </div>
   <div class="modal fade" id="modalempleado2" tabindex="10" role="dialog" aria-labelledby="modalempleado" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
   <div class="modal-content">
   <div class="modal-header label">
     <h2>Listado de Presupuesto</h2>
     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
     </button>
   </div>

    <div class="modal-body container">
      <table id="datos2" class="table" style="width:100%">
       <thead class="thead-dark" >
       <tr>
           <th>Codigo</th>
           <th>Descripcion</th>
           <th>Accion</th>
         </tr>
        </thead>


      </table>

    </div>
  <div class="modal-footer">
     <button type="button" class="btn btn-primary" data-dismiss="modal"><b> Cerrar</b></button>
    </div>
  </div>
</div>
</div>
  @endsection

  @section('mayuscula')
    <script >
function checkbox(id) {
      console.log(id)
      var checked = $('#'+id).is(':checked')
        if (checked) {
          if(id === 'serial'){
            $('#customSwitch45').html('Articulo tiene Serial : Si');
            $('#'+id).val('T');
            return
          }
            $('#customSwitch4').html('Articulo Predeterminado Para Facturar : Si');
            $('#customSwitch3').val('F');
        }else{
          if(id === 'serial'){
            $('#customSwitch45').html('Articulo tiene Serial : No');
            $('#'+id).val('F');
            return
          }

            $('#customSwitch4').html('Articulo Predeterminado Para Facturar : No');
        }
    }
    
    function showSerial(id){
      var serialCheck = document.getElementById('serialCheck');
      var serial = document.getElementById('serial');
      console.log(serial)
      if(id === 'A'){
        serialCheck.style.display = 'inline';
      }
      else {
        serialCheck.style.display="none";
        serial.checked = false;
        checkbox('serial')
      }
    }

    function getPartidas(id) {
      
      $('.partidas').val(id);
      $('#modalempleado2').modal('hide');
    }

function codpartida(){
  $('#modalempleado2').modal({ backdrop: "static", keyboard: false });
  $('#datos2').DataTable({
          serverSide: true,
          processing:true,
          ajax: {
            url: '{{route('getPartida.Almacen')}}',
            type: 'GET'
          },columns:[
            {data:'codpre'},
            {data:'nompre'},
            {data:'action'}
          ],
          destroy:true,
          searching: true,
      });
}
      function search(value) {
        var url = "{!! route('search.code') !!}"+"/"+value;

          $.get(url,function(result) {
             if (result.titulo === 'Existe El Codigo Registrado') {
               Swal.fire({
               title: 'Existe El Codigo Registrado',
               text: "Registra Con Otro Codigo",
               icon: 'warning',
               showCancelButton: false,
               confirmButtonColor: '#3085d6',
               cancelButtonColor: '#d33',
               confirmButtonText: 'Entendido'
             });
           }else {

             Swal.fire({
             title: 'No Existe El Codigo Registrado',
             text: "Se Puede Registra Con Este Codigo",
             icon: 'success',
             showCancelButton: false,
             confirmButtonColor: '#3085d6',
             cancelButtonColor: '#d33',
             confirmButtonText: 'Entendido'
           });


           }



          });
      }

      $(document).ready(function(){

          $('#saveArticulo').on('submit',function(e){
            e.preventDefault();
            $(this).prop('disabled',true)
            $('#saveSubmit').trigger('click');
          })


          $('.codigo').keyup(function(tecla){

            var texto = $(this).val().toUpperCase().replace(/ /g,"");
            
            $(this).val(texto);
          });

          $('.desc').keyup(function(t){
               var texto = $(this).val().toUpperCase();
               $(this).val(texto);

          });

        $('.select2').select2({
            width:'100%'
         });

        $('.cambios').change(function(){

            $('.oculto').show();

        });


        $('select.categorias').change(function(){

              var url = $(this).data('url')+'/'+$(this).val();

              $.get(url,function (result) {

                //console.log(result.subcategoria);
                $('select.cambioss1').html(result.subcategoria);
                if(result.tallas !== null){
                  $('select.talla').html(result.tallas);
                }

              });

              var url2 = $(this).data('url2')+'/'+$(this).val();
              $.get(url2,function(result) {

                  if (result.tallas) {
                    $('.infoT').css('display','block');
                  }else if(result.tallas === false || result.tallas === null){
                    $('.infoT').css('display','none');
                  }
              });
             
             
             
               if ( $(this).val() === '3-0002' || $(this).val() === '4-0002') {

                $('div.tallas').hide();//oculta el campo talla

               }else{

                $('div.tallas').show();//muestra el campo talla

               }


        });





      });

    function modal(){
      $('#modalempleado').modal('show');
      $('#datos').DataTable({
          serverSide: true,
          processing:true,
          ajax: {
            url: '{{route('getContabb.Almacen')}}',
            type: 'GET'
          },columns:[
            {data:'codcta'},
            {data:'descta'},
            {data:'action'}
          ],
          
          searching: true,
          retrieve: true
      });


    }

    function modal1(){
      $('#modalempleado').modal('show');
      $('#datos').DataTable({
          serverSide: true,
          processing:true,
          ajax: {
            url: '{{route('getContabb1.Almacen')}}',
            type: 'GET'
          },columns:[
            {data:'codcta'},
            {data:'descta'},
            {data:'action'}
          ],
          destroy:true,
          searching: true,
      });
    }
    
      function modal2(){
      $('#modalempleado').modal('show');
      $('#datos').DataTable({
          serverSide: true,
          processing:true,
          ajax: {
            url: '{{route('getCideftit')}}',
            type: 'GET'
          },columns:[
            {data:'codpre'},
            {data:'nompre'},
            {data:'action'}
          ],
          destroy:true,
          searching: true,
      });

      
    }
    function getId(id) {
     
      $('.ccosto').val(id);
      $('#modalempleado').modal('hide');
    }

    function getId2(id) {
      $('.cTransitoria').val(id);
      $('#modalempleado').modal('hide');
    }

    function getId3(id) {
      $('.cgasto').val(id);
      $('#modalempleado').modal('hide');
    }


    </script>


  @endsection
