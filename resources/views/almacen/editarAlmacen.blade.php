@extends('layouts.app')

@section('content')
	
			   {!! Form::open(['route'=>['Almacen.actualizar',$almacen],'method' => 'PUT']) !!}
				  
					            <!-- general form elements -->
	    <div class="container">
		    <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edicion de Almacen</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Número Almacen</label>
                    <input type="text" class="form-control" value="{{ $almacen->codcat }}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Descripcion</label>
                    <input type="text" class="form-control"  placeholder="Password" value="{{ $almacen->nomalm }}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Tipo</label>
                    <textarea type="text" class="form-control" id="exampleInputPassword1">{{ $almacen->diralm }}</textarea>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Actualizar</button>
                </div>
              </form>
            </div>
		  </div>
            
            <!-- /.card -->
   				{!! Form::close() !!}
        	</div>
@endsection