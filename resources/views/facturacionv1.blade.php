
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Facturacion</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('cssadminlte/adminlte.min.css') }}">

  <!-- Google Font: Source Sans Pro -->
  {{-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> --}}
</head>
<body class="hold-transition sidebar-collapse layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  {{-- <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
      <a href="../../index3.html" class="navbar-brand">
        <img src="../../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Facturacion</span>
      </a>
      
      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
          </li>
          <li class="nav-item">
            <a href="index3.html" class="nav-link">Home</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">Contact</a>
          </li>
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Dropdown</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <li><a href="#" class="dropdown-item">Some action </a></li>
              <li><a href="#" class="dropdown-item">Some other action</a></li>

              <li class="dropdown-divider"></li>

              <!-- Level two dropdown-->
              <li class="dropdown-submenu dropdown-hover">
                <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">Hover for action</a>
                <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                  <li>
                    <a tabindex="-1" href="#" class="dropdown-item">level 2</a>
                  </li>

                  <!-- Level three dropdown-->
                  <li class="dropdown-submenu">
                    <a id="dropdownSubMenu3" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">level 2</a>
                    <ul aria-labelledby="dropdownSubMenu3" class="dropdown-menu border-0 shadow">
                      <li><a href="#" class="dropdown-item">3rd level</a></li>
                      <li><a href="#" class="dropdown-item">3rd level</a></li>
                    </ul>
                  </li>
                  <!-- End Level three -->

                  <li><a href="#" class="dropdown-item">level 2</a></li>
                  <li><a href="#" class="dropdown-item">level 2</a></li>
                </ul>
              </li>
              <!-- End Level two -->
            </ul>
          </li>
        </ul>

        <!-- SEARCH FORM -->
        <form class="form-inline ml-0 ml-md-3">
          <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
              <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </form>
      </div>

      <!-- Right navbar links -->
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="fas fa-comments"></i>
            <span class="badge badge-danger navbar-badge">3</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="../../dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    Brad Diesel
                    <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">Call me whenever you can...</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="../../dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    John Pierce
                    <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">I got your message bro</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="../../dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    Nora Silvester
                    <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">The subject goes here</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
          </div>
        </li>
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-bell"></i>
            <span class="badge badge-warning navbar-badge">15</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-header">15 Notifications</span>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-envelope mr-2"></i> 4 new messages
              <span class="float-right text-muted text-sm">3 mins</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-users mr-2"></i> 8 friend requests
              <span class="float-right text-muted text-sm">12 hours</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-file mr-2"></i> 3 new reports
              <span class="float-right text-muted text-sm">2 days</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button"><i
              class="fas fa-th-large"></i></a>
        </li>
      </ul>
    </div>
  </nav> --}}
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Starter Pages
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="#" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Active Page</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Inactive Page</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Simple Link
                <span class="right badge badge-danger">New</span>
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    {{-- <div class="content-header">
    </div> --}}
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      
      <div id="app">

          <div class="row">

            <div class="col-8">
              <div class="card card-primary card-outline">
              {{--<div class="card-header">
                  <h5 class="card-title m-0"><i class="fas fa-edit"></i> Detalles de Factura</h5>
                </div> --}}
                <div class="card-body">
                  {{-- <div class="row">
                    <div class="col-5">
                      <div class="form-group row">
                        <label for="text1" class="col-2 col-form-label">Forma de Pago</label> 
                        <div class="col-8">
                          <input id="text1" name="text1" type="text" class="form-control">
                        </div>
                      </div>
                      
                      <div class="form-group row">
                        <label for="text1" class="col-2 col-form-label">Descuento</label> 
                        <div class="col-8">
                          <input id="text1" name="text1" type="text" class="form-control">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="text1" class="col-2 col-form-label">Escala</label> 
                        <div class="col-8">
                          <input id="text1" name="text1" type="text" class="form-control">
                        </div>
                      </div>
                    </div>
                  </div> --}}

                  <div class="row">
                    <div class="col-12">
                      <div class="input-group mb-2 col-6">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                          </div>
                          <input id="find" name="find" type="text" class="form-control" placeholder="CTRL+F BUSCAR ARTICULO">
                        </div>
                      <div class="card-body table-responsive p-0" style="height: 550px;">
                        <table class="table table-head-fixed">
                          <thead>
                            <tr>
                              <th>CODIGO</th>
                              <th>ARTICULO</th>
                              <th>DESCRIPCION</th>
                              <th>Status</th>
                              <th>Valor Unitario</th>
                              <th>Descuento</th>
                              <th>Accion</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>183</td>
                              <td>John Doe</td>
                              <td>11-7-2014</td>
                              <td><span class="tag tag-success">Approved</span></td>
                              <td>Bacon ipsum dolor sit</td>
                              <td>0</td>
                              <td>
                                <a href="" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                            <tr>
                              <td>219</td>
                              <td>Alexander Pierce</td>
                              <td>11-7-2014</td>
                              <td><span class="tag tag-warning">Pending</span></td>
                              <td>Bacon ipsum dolor sit </td>
                              <td>0</td>
                              <td>
                                <a href="" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                            <tr>
                              <td>657</td>
                              <td>Bob Doe</td>
                              <td>11-7-2014</td>
                              <td><span class="tag tag-primary">Approved</span></td>
                              <td>Bacon ipsum dolor sit amet</td>
                              <td>0</td>
                              <td>
                                <a href="" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                            <tr>
                              <td>175</td>
                              <td>Mike Doe</td>
                              <td>11-7-2014</td>
                              <td><span class="tag tag-danger">Denied</span></td>
                              <td>Bacon ipsum dolor sit amet salami v</td>
                              <td>0</td>
                              <td>
                                <a href="" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                            <tr>
                              <td>134</td>
                              <td>Jim Doe</td>
                              <td>11-7-2014</td>
                              <td><span class="tag tag-success">Approved</span></td>
                              <td>Bacon ipsum dolor </td>
                              <td>0</td>
                              <td>
                                <a href="" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                            <tr>
                              <td>494</td>
                              <td>Victoria Doe</td>
                              <td>11-7-2014</td>
                              <td><span class="tag tag-warning">Pending</span></td>
                              <td>Bacon ipsum dolor sit a</td>
                              <td>0</td>
                              <td>
                                <a href="" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                            <tr>
                              <td>832</td>
                              <td>Michael Doe</td>
                              <td>11-7-2014</td>
                              <td><span class="tag tag-primary">Approved</span></td>
                              <td>Bacon ipsum dolor</td>
                              <td>0</td>
                              <td>
                                <a href="" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                            <tr>
                              <td>982</td>
                              <td>Rocky Doe</td>
                              <td>11-7-2014</td>
                              <td><span class="tag tag-danger">Denied</span></td>
                              <td>Bacon ipsum dolor sit</td>
                              <td>0</td>
                              <td>
                              <a href="" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>



                  </div>
                </div>
                
              </div><!-- /.card -->

              <div class="col-4">
                {{-- <div class="card card-primary card-outline">
                    <div class="card-body montos"> --}}
                      <div class="form-group row mb-0">
                        <label for="text1" class="col-3 col-form-label" style="font-size:180%">IVA</label> 
                        <div class="col-6 ">
                          <input id="text1" name="text1" type="text" class="form-control mt-2 ml-5" style="font-size:180%;font-weight:bold">
                        </div>
                      </div>
                      <div class="form-group row mb-0">
                        <label for="text1" class="col-3 col-form-label" style="font-size:180%">DESCUENTO</label> 
                        <div class="col-6">
                          <input id="text1" name="text1" type="text" class="form-control mt-2 ml-5"  style="font-size:180%;font-weight:bold">
                        </div>
                      </div>
                      <div class="form-group row mb-0">
                        <label for="text1" class="col-3 col-form-label" style="font-size:180%">SUBTOTAL</label> 
                        <div class="col-6">
                          <input id="text1" name="text1" type="text" class="form-control mt-2 ml-5"  style="font-size:180%;font-weight:bold">
                        </div>
                      </div>
                      <div class="form-group row mb-0">
                        <label for="text1" class="col-3 col-form-label" style="font-size:180%">TOTAL</label> 
                        <div class="col-6">
                          <input id="text1" name="text1" type="text" class="form-control mt-2 ml-5"  style="font-size:180%;font-weight:bold">
                        </div>
                    {{-- </div>
                  </div> --}}
                </div>


                <div class="card card-primary card-outline">
                  {{-- <div class="card-header">
                    <h5 class="card-title m-0"><i class="fas fa-edit"></i>Opciones</h5>
                  </div> --}}
                  <div class="card-body">
                    
                    <div class="row">

                      <a href="" class="m-1">
                        <div class="small-box bg-success p-2">
                          <div class="inner">
                          <h6>TECLA</h6>
                          <h6>FACTURAR</h6>
                        </div>
                        <div class="icon">
                          <i class="fas fa-print"></i>
                        </div>
                        <a href="#" class="small-box-footer">
                          More info <i class="fas fa-arrow-circle-right"></i>
                        </a>
                      </div>
                      </a>

                      <a href="" class="m-1">
                        <div class="small-box bg-success gris p-2">
                          <div class="inner">
                          <h6>TECLA</h6>
                          <h6>CANCELAR</h6>
                        </div>
                        <div class="icon">
                          <i class="fas fa-print"></i>
                        </div>
                        <a href="#" class="small-box-footer">
                           <i class="fas fa-arrow-circle-right"></i>
                        </a>
                      </div>
                    </a>
                    </div>
                  </div>
                </div>
                <div class="card card-primary card-outline">
                  {{-- <div class="card-header">
                    <h5 class="card-title m-0"><i class="fas fa-edit"></i> Datos del CLiente</h5>
                  </div> --}}
                  <div class="card-body">
                    <form action="">
  
                      <div class="form-group row">
                        <label for="text1" class="col-4 col-form-label">CLIENTE</label> {{-- CHANGE ON RES --}}
                        <div class="col-8">
                          <input id="text1" name="text1" type="text" class="form-control">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="text1" class="col-4 col-form-label">VENDEDOR</label> {{-- CHANGE ON RES --}}
                        <div class="col-8">
                          <input id="text1" name="text1" type="text" class="form-control">
                        </div>
                      </div>
                      <div class="">
                        <button class="btn btn-success"><i class="fas fa-search"></i><b> SELECCIONAR CLIENTE</b></button>
                      </div>
                    </form>
                    </div>
                </div>
              </div>

              <div class="col-8">
                <div class="card card-primary card-outline">
                  {{-- <div class="card-header">
                    <h5 class="card-title m-0"><i class="fas fa-edit"></i> Datos del CLiente</h5>
                  </div> --}}
                  <div class="card-body">
                              <div class="row">
                              <a href="" class="m-1">
                                <div class="small-box bg-primary p-2">
                                  <div class="inner">
                                  <h6>TECLA</h6>
                                  <h6>CORTE X</h6>
                                </div>
                                <div class="icon">
                                  <i class="fas fa-shopping-cart"></i>
                                </div>
                          <p></p>

{{--                                 <a href="#" class="small-box-footer">
                                  More info <i class="fas fa-arrow-circle-right"></i>
                                </a> --}}
                              </div>
                            </a>
  
                            <a href="" class="m-1">
                              <div class="small-box bg-danger p-2">
                                <div class="inner">
                                <h6>TECLA</h6>
                                <h6>DEVOLUCION</h6>
                              </div>
                              <div class="icon">
                                <i class="fas fa-shopping-cart"></i>
                              </div>
                          <p></p>

                              {{-- <a href="#" class="small-box-footer">
                                More info <i class="fas fa-arrow-circle-right"></i>
                              </a> --}}
                            </div>
                          </a>
            
                          <a href="" class="m-1">
                            <div class="small-box bg-info p-2">
                              <div class="inner">
                              <h6>TECLA</h6>
                              <h6>CORTE Z</h6>
                            </div>
                            <div class="icon">
                              <i class="fas fa-shopping-cart"></i>
                            </div>
                          <p></p>

                            {{-- <a href="#" class="small-box-footer">
                              More info <i class="fas fa-arrow-circle-right"></i>
                            </a> --}}
                          </div>
                        </a>
  
                                <a href="" class="m-1">
                                  <div class="small-box bg-success p-2">
                                    <div class="inner">
                                    <h6>TECLA</h6>
                                    <h6>DESCUENTO</h6>
                                  </div>
                                  <div class="icon">
                                    <i class="fas fa-print"></i>
                                  </div>
                                  <p></p>
                                  {{-- <a href="#" class="small-box-footer">
                                    <i class="fas fa-arrow-circle-right"></i>
                                  </a> --}}
                                </div>
                              </a>

                              <a href="" class="m-1">
                                <div class="small-box bg-warning p-2">
                                  <div class="inner">
                                  <h6>TECLA</h6>
                                  <h6>&nbsp;ATRAS&nbsp;</h6>
                                </div>
                                <div class="icon">
                                  <i class="fas fa-chevron-circle-left"></i>
                                </div>
                                <p></p>
                                {{-- <a href="#" class="small-box-footer">
                                  <i class="fas fa-arrow-circle-right"></i>
                                </a> --}}
                              </div>
                            </a>

                              <a href="" class="m-1">
                                <div class="small-box bg-danger p-2">
                                  <div class="inner">
                                  <h6>TECLA</h6>
                                  <h6>&nbsp;SALIR&nbsp;</h6>
                                </div>
                                <div class="icon">
                                  <i class="fas fa-bell"></i>
                                </div>
                                <p></p>
                                {{-- <a href="#" class="small-box-footer">
                                  <i class="fas fa-arrow-circle-right"></i>
                                </a> --}}
                              </div>
                            </a>
                            </div>
                    </div>
                </div>
              </div>
              <div class="col-4">

              
            </div>


              
            </div>
            <!-- /.col-md-6 -->
          </div>
          <!-- /.row -->
      </div>
    </div>
    <!-- /.content -->
  </div>

  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->


  <div class="modal fade" id="modal-xl" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Registrar Cliente</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="">
            <div class="form-group row">
              <label for="cedula" class="col-1 col-form-label">CEDULA</label> 
              <div class="col-10">
                <input id="cedula" name="cedula" type="text" class="form-control insertcliwithenter">
              </div>
            </div>
            <div class="form-group row">
              <label for="nombres" class="col-1 col-form-label">NOMBRES</label> 
              <div class="col-10">
                <input id="nombres" name="nombres" type="text" class="form-control insertcliwithenter">
              </div>
            </div>
            <div class="form-group row">
              <label for="apellidos" class="col-1 col-form-label">APELLIDOS</label> 
              <div class="col-10">
                <input id="apellidos" name="apellidos" type="text" class="form-control insertcliwithenter">
              </div>
            </div>
            <div class="form-group row">
              <label for="dircli" class="col-1 col-form-label">DIRECCION</label> 
              <div class="col-10">
                <input id="dircli" name="dircli" type="text" class="form-control insertcliwithenter">
              </div>
            </div> 
          </form>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <div class="modal fade" id="modal-xl" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Registrar Cliente</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="">
            <div class="form-group row">
              <label for="cedula" class="col-1 col-form-label">CEDULA</label> 
              <div class="col-10">
                <input id="cedula" name="cedula" type="text" class="form-control insertcliwithenter">
              </div>
            </div>
            <div class="form-group row">
              <label for="nombres" class="col-1 col-form-label">NOMBRES</label> 
              <div class="col-10">
                <input id="nombres" name="nombres" type="text" class="form-control insertcliwithenter">
              </div>
            </div>
            <div class="form-group row">
              <label for="apellidos" class="col-1 col-form-label">APELLIDOS</label> 
              <div class="col-10">
                <input id="apellidos" name="apellidos" type="text" class="form-control insertcliwithenter">
              </div>
            </div>
            <div class="form-group row">
              <label for="dircli" class="col-1 col-form-label">DIRECCION</label> 
              <div class="col-10">
                <input id="dircli" name="dircli" type="text" class="form-control insertcliwithenter">
              </div>
            </div> 
          </form>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <!-- Main Footer -->
  {{-- <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer> --}}
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

<!-- Bootstrap 4 -->
{{-- <script src="asset{{'plugins/bootstrap/js/bootstrap.bundle.min.js'}}"></script> --}}
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}" ></script>

<!-- AdminLTE App -->
<script src="{{ asset('jsadminlte/adminlte.js') }}"></script>

</body>
</html>

<style>
/* ESTILOS MIGUELO */
.table td{
  padding: .25rem !important;
}

.gris {
  background-color: darkslategrey !important;
}

.card-body .montos {
  padding: 1em !important;
}

/*
1440 de ancho 
.sizem {
font-size : 150%;

} 
1264 de ancho {
 
  font size: 150%;
}


LABEL CLIENTE Y VENDEDOR CAMBIAR AL CAMBIAR RES
de COL-4 a COL-3
*/




</style>
