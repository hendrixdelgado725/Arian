@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Editar Permisos de Usuario')
@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Permisos
    @endslot
  @endcomponent
 @if (strcmp($user->codroles,'R-001') === 0)
    <section class="content" >
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-12">
          
          <div class="card card-info" >
            @include('vendor/flash.flash_message')
            <div class="card-header ">
            <h3 class="card-title admin" data-admin="{{ route('admin',['id'=>$user->codroles]) }}"><b>AÑADIR PERMISOS A ({{$user->nomuse}})</b></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
              <form id="create" data-url="{{ route('regAdmin',['id'=>$user->loguse,'id2'=>'R-001']) }}" method="get">
                {{csrf_field()}}
                <div class="card-body">
                  
                 
                 
                  <div class="form-group" >
                    <h6><b>Modulos(En caso del Modulo de Seguridad es solo para los administradores del Sistemas) para habilitar los permisos siempre y cuando que el permiso de leer este activo</b></h6>
                  
                     <div class="table-wrapper-scroll-y my-custom-scrollbar areaTexto" >
                       <div class="col-sm-12">
                          <label for="cargo">Permisos</label>
                           <div class="form-group selector">
                            <textarea rows="10" cols="180" readonly class="area" name="reg" style="color:#000; text-align: justify;"></textarea>
                           </div>
                        </div>    
                    </div>
                  </div>
              <!-- /.card-body -->
                <div class="card-footer">
                  <button id="createcountry" type="submit"  class="btn btn-success btnss" ><b>Guardar Cambios</b></button>
                  <a href="{{ URL::previous() }}" class="btn btn-default ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                </div>
              </form>
                
            </div>


            <!-- /.card -->
            </div>
        </div>
      </div>
        {{-- row --}}
 
    {{-- container fluid --}}
  </section>
 @else
<section class="content1" >
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-12">
          
          <div class="card card-info" >
            @include('vendor/flash.flash_message')
            <div class="card-header">
            <h3 class="card-title"><b>AÑADIR PERMISOS A ({{$user}})</b></h3>
            </div>
            <!-- /.card-header --> 
            <!-- form start -->
            {{----}}
              <form action="#" data-url="{{ route('rPermisos',$user->loguse) }}" id="create"  method="post">
                {{csrf_field()}}
                <div class="card-body">
                  
                  <div class="form-group">
                  {{-- <label for="tipodoc">Seleccione el Modulo</label>
                    <select data-url = "{{ route('modPer') }}"  name="entidades[]" class="form-control entidad select2">
                        <option value="0" selected disabled> Seleccione el modulo</option>
                      </select> --}}
                      <div class="row">

                        @foreach ($menus['submodulo'] as $element => $value)
                        <div class="col">
                          <button class="btn btn-light border border-dark " type="button" data-toggle="collapse" data-target="#collapseExample{{ $loop->index }}" aria-expanded="false" aria-controls="collapseExample{{ $loop->index }}">
                            {{ $element }} 
                          </button>
                          @if (is_array($menus['submodulo'][$element]))
                            @foreach ($menus['submodulo'][$element] as $element2 => $value2)
                            
                            <div class="collapse" id="collapseExample{{ $loop->parent->index }}">
                              <div class="row">
                                
                                <button type="button" class="lista" data-toggle="collapse" data-target="#multiCollapseExample{{$loop->parent->index.$loop->index}}" aria-expanded="false" aria-controls="multiCollapseExample2" style="list-style: none;">
                                  {{ $element2 }}
                                </button>
                                @if(!is_array($value2))
                                  <div class="collapse multi-collapse" id="multiCollapseExample{{$loop->parent->index.$loop->index}}">
                                    <td >
                                      <input style="display: none"  name="numcuenta[]" data-id="a{{$loop->index.$loop->parent->index}}" value="/modulo/{{ $value2 }}" type="text" class="form-control rutas" >
                                    </td>
                                    <ul style="list-style: none">
                                      <li>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" id="checkboxEnLinea1" data-id="{{$loop->index.$loop->parent->index}}" class="checkboxEnLinea1" value="/modulo/{{ $value2 }}-contenidos.index-"> Navega todos los contenido del sistema<br>
                                        </label>
                                      </li>
                                      <li>
                                        <label class="checkbox-inline">
                                          <input data-id="{{$loop->index.$loop->parent->index}}" type="checkbox" id="checkboxEnLinea4" class="checkboxEnLinea1" value="/modulo/{{ $value2 }}-contenidos.create-"> Crear los contenido del sistema<br>
                                        </label>
                                      </li>
                                      <li>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" data-id="{{$loop->index.$loop->parent->index}}" id="checkboxEnLinea2" class="checkboxEnLinea1" value="/modulo/{{ $value2 }}-contenidos.edit-"> Editar los contenido del sistema<br>
                                        </label>
                                      </li>
                                      <li>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" data-id="{{$loop->index.$loop->parent->index}}" id="checkboxEnLinea3" class="checkboxEnLinea1" value="/modulo/{{ $value2 }}-contenidos.delete-"> Eliminar los contenido del sistema<br>
                                        </label>
                                      </li>
                                      <li>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" data-id="{{$loop->index.$loop->parent->index}}" id="checkboxEnLinea5" class="checkboxEnLinea5" value="contenidos.index-contenidos.edit-contenidos.delete-contenidos.create-"> Marcar todos<br>
                                        </label>
                                      </li>
                                    </ul>
                                  </div>
                                  @endif
                                  @if (is_array($menus['submodulo'][$element][$element2]))
                                  @foreach ($menus['submodulo'][$element][$element2] as $element3 => $value3)
                                  <div class="row ml-2" style="width: 100%;">
                                    <button type="button" class="lista" data-toggle="collapse" data-target="#multiCollapseExample{{$loop->parent->parent->index.$loop->parent->index.$loop->index}}" aria-expanded="false" aria-controls="multiCollapseExample2" style="list-style: none;">
                                      {{ $element3 }}
                                    </button>
                                  </div>
                                  @if(!is_array($value3))
                                  <div class="collapse multi-collapse" id="multiCollapseExample{{$loop->parent->parent->index.$loop->parent->index.$loop->index}}">
                                    <td class="rutas">
                                      <input style="display: none" name="numcuenta[]" data-id="a{{$loop->parent->parent->index.$loop->parent->index.$loop->index}}" value="/modulo/{{ $value3 }}" type="text" class="form-control rutas" >
                                    </td>
                                    <ul style="list-style: none">
                                      <li>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" id="checkboxEnLinea1" data-id="a{{$loop->parent->parent->index.$loop->parent->index.$loop->index}}" class="checkboxEnLinea1" value="/modulo/{{ $value3 }}-contenidos.index-"> Navega todos los contenido del sistema<br>
                                        </label>
                                      </li>
                                      <li>
                                        <label class="checkbox-inline">
                                          <input data-id="a{{$loop->parent->parent->index.$loop->parent->index.$loop->index}}" type="checkbox" id="checkboxEnLinea4" class="checkboxEnLinea1" value="/modulo/{{ $value3 }}-contenidos.create-"> Crear los contenido del sistema<br>
                                        </label>
                                      </li>
                                      <li>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" data-id="a{{$loop->parent->parent->index.$loop->parent->index.$loop->index}}" id="checkboxEnLinea2" class="checkboxEnLinea1" value="/modulo/{{ $value3 }}-contenidos.edit-"> Editar los contenido del sistema<br>
                                        </label>
                                      </li>
                                      <li>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" data-id="a{{$loop->parent->parent->index.$loop->parent->index.$loop->index}}" id="checkboxEnLinea3" class="checkboxEnLinea1" value="/modulo/{{ $value3 }}-contenidos.delete-"> Eliminar los contenido del sistema<br>
                                        </label>
                                      </li>
                                      <li>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" data-id="a{{$loop->parent->parent->index.$loop->parent->index.$loop->index}}" id="checkboxEnLinea5" class="checkboxEnLinea5" value="contenidos.index-contenidos.edit-contenidos.delete-contenidos.create-"> Marcar todos<br>
                                        </label>
                                      </li>
                                    </ul>
                                  </div>
                                  @endif
                                  @endforeach
                                  @endif
                                </div>
                                
                              </div>
                                @endforeach
                                @endif
                                
                        </div>
                        @endforeach                               
                              {{-- <p class="ml-3">submodulo: {{ $element2 }}</p> --}}
                      </div>
                  
                  </div>
                 
                  {{-- <div class="form-group" >
                    <h6><b>Modulos(En caso del Modulo de Seguridad es solo para los administradores del Sistemas) para habilitar los permisos siempre y cuando que el permiso de leer este activo</b></h6>
                    <div class="table-wrapper-scroll-y my-custom-scrollbar texto" >
                      <table class="table table-bordered">
                        <thead>
                          <th>Num</th>
                          <th>Modulo</th>
                          <th>Rutas del modulo</th>
                          <th>Permisos</th>
                        </thead>
                        <tbody>

                            
                        </tbody>
                      </table>
                    </div> --}}
                  
                  </div>
              <!-- /.card-body -->
                <div class="card-footer">
                  <a id="createcountry" href="#" class="btn btn-success createcountry" ><b>Guardar Cambios</b></a>
                  <a href="{{ URL::previous() }}" class="btn btn-default ml-4"><i class="fas fa-chevron-circle-left"></i><b>Volver</b></a>
                </div>
              </form>
                
            </div>


            <!-- /.card -->
            </div>
        </div>
      </div>
        {{-- row --}}
    {{-- container fluid --}}

           {{--  <div class="modal fade" id="modalempleado" tabindex="-1" role="dialog" aria-labelledby="modalempleado" aria-hidden="true">
           <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content"> 
            <div class="modal-header">
             <h5 class="modal-title" id="modalempleadolabel">DETALLES DEL USUARIO</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
            </div>
             <div class="modal-body content-centered">
              <div class="col-sm-12">
              <label for="cargo">Modulo</label>
               <div class="form-group">
                <input class="form-control" type="text" id="pass_mod" disabled>
               </div>
              </div>
                  
               <div class="col-sm-12">
              <label for="cargo"></label>
               <div class="form-group selector">
                <select data-row="{{$i}}" name="entidades[]" class="form-control entidades" >
                  
                </select>
               </div>
              </div>           
           <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal"><b> Cerrar</b></button>
           </div>
         </div>
       </div>
     </div>
   </div>


--}}
  </section>


 @endif
@endsection
@section('script')
 <script>
$(document).on('click', '.list > li ', function () {
    $(this).next('ul').toggle();
})
   $(document).ready(function() {
              
      $('.entidad').on('change',function(e) {
        
        var indice = $(this); 
        let temp=[];
        var valor = $(this).val();
        var url = $(this).data('url')+"/"+valor.toString();
        let datos = [];
        let temp1; 

        var campos = $('input.valorRute');
        var temp2 = $(indice).data('row');
        
        $('#pass_mod').val(e.target.value);

        $.get(url,function(resul) {
         
              $('tbody').html(resul.modulo);
         });

         
      });
      var temp = [];
      var rutas = [];
       $(document).on('click','.checkboxEnLinea1',function() {//queda en la espera 
               if ($(this).is(':checked')) {
                  temp.push($(this).val());

               }
               else{
                indice = temp.indexOf($(this).val())
                temp.splice(indice,1);
               }

               var dataId = $(this).data('id');
               var input = $('input[data-id="' + dataId + '"].checkboxEnLinea5');
               var num = $('[data-id="' + dataId + '"].checkboxEnLinea1:checked').length;
               if(num === 4){
                input.prop('checked', true);
               }
               else {
                input.prop('checked', false);
               }
        });

         $(document).on('click','.checkboxEnLinea5',function() {//queda en la espera 
                  
                  var pos = $('.rutas');
                  var check = $('.checkboxEnLinea1');
                  
                  if($(this).is(':checked')){
                    var poss = $(this).data('id');
                      for(var i = check.length-1; i >= 0; i--) {
                          if ($(check[i]).data('id') === $(this).data('id') ) {
                              $(check[i]).prop('checked',true);
                              temp = temp.filter(a => a !== $(check[i]).val());
                              temp.push($(check[i]).val());
                          }
                      }
                  }
                  else {
                    var poss = $(this).data('id');
                      for(var i = check.length-1; i >= 0; i--) {
                          if ($(check[i]).data('id') === $(this).data('id')) {
                              $(check[i]).prop('checked',false);

                              temp = temp.filter(a => a !== $(check[i]).val());
                          }
                      }
                  }
        });


        var form = $('form#create').data('url');
        $('.createcountry').click(function(t) {
            
            
            $.ajax({
              type:'POST',
              url:form,
              data:{
                "_token": "{{ csrf_token() }}",
                perm:temp

              },
              success:function(t1) {
                  window.location.href = "/modulo/Seguridad/Permisos";
              }


            });
        });


  
       
        var url2 = $('form#create').data('url');
        var url11 = $('h3.admin').data('admin');
         
        
        
          $.get(url11,function (result) {
            
               
               $.each(result.submodulos,function(key,reg) {
                
                    $('textarea.area').append(result.rutas1[indice]+"\n");

                    indice++;
                });

               
                
       $('button.btnss').click(function(e) {
             e.preventDefault()
          $.ajax({

            type:'POST',
            url:url2,
            data:{
             "_token":"{{csrf_token()}}",
             datos: result.rutas1,
            },
            success:function(resultados) {
                
              if (resultados.titulo === 'por fin' ) {

                 Swal.fire(
                  'Hecho',
                  'Se Agrego su rol',
                  'success',

                ).then(function() {
                    
                      
                      window.location.href = "/modulo/Seguridad/Permisos";
                    
                });  


              }
                
            
              }
          });

         });
         });

        indice = 0;
        $('textarea.area').empty();
    
        $('.select2').select2({
            theme: "bootstrap"
        });

   });

 </script>

 
@endsection
<style>
.my-custom-scrollbar {
position: relative;
height: 300px;
overflow: auto;
}
.table-wrapper-scroll-y {
display: block;
}


#menu > ul {
  display: none;
  list-style-type:none;
} 
  
#menu > ul > li {
    list-style:none; 
} 
  
#menu > li {
   list-style:none;   
  font-weight: bold;
}
  

a {
    color: red;
}

.lista {
  border: 0;
  background-color: rgb(255, 255, 255);
  text-align: left;
} 

.lista::before {
  content: "►";
	color: rgb(0, 0, 0);
	display: inline-block;
	width: 1em; 
}

/* .lista::after {
  content: "▼";
	color: rgb(0, 0, 0);
	display: inline-block;
	width: 1em;
	margin-left: -1em 
} */
</style>