@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Permisos de Usuarios')


@section('content')

@component('layouts.contenth')
    @slot('titulo')
      Listado de Usuarios Con Permisos
    @endslot
  @endcomponent
    <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">

            <div class="card">

 @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">


               <br>
               <div class="col">
                  <div class="row">
                    <div class="col mt-2">

                        <form action="{{ route('Buscarr')}}" method="post">
                            <div class="input-group">
                          @csrf
                          <input type="text" placeholder="Buscar " name="filtro"  class="form-control float-right">
                          <button type="submit" style="height: 38px; width: 38px" class="btn btn-default btn-sm float-left"><i class="fas fa-search"></i></button>
                          
                           </div>
                        </form>

                    </div>
                  </div>
                 </div>

                 </div><!--row -->
                </div>
              </div>

              <!-- /.card-header -->
              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>ID</th>
                      <th>Usuario</th>
                      <th>Nombre</th>
                      <th>Rol</th>
                      <th>Acción</th>
                    </tr>
                  </thead>
                  <tbody style="text-align:center">
                  @foreach($user as $use)
                     @php
                    $rand  = rand(1, 9999);
                    $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                    $enc=Helper::EncriptarDatos($nrand.'|'.$use->id);
                    @endphp


                      <tr style="text-align: center;">
                      <td>{{  $use->id  }}</td>
                      <td>{{  $use->loguse  }}</td>
                      <td>{{  $use->nomuse  }}</td>
                      <td>{{  $use->roles->nombre ?? 'vacio' }}</td>
                      <td>
                      @if ($use->codroles !== 'R-000')

                        @can('edit', App\permission_user::class)

                       <a  href="{{ route('addRolPermisos',$enc) }}">
                        <i class="fa fa-edit" title="Registrar permisos"></i>
                        </a>
                        /
                        @endcan

                        {{-- @can('viewAny', App\permission_user::class)

                         <a class="mostrar" href="{{ route('search',$enc) }}">
                          <i class="fa fa-book red"></i>
                        </a>
                        /
                        @endcan  --}}
                       @can('delete',App\permission_user::class)
                        <a class="delete" href="{{ route('delete',$enc) }}">
                          <i class="fa fa-trash" title="Borrar permisos"></i>
                        </a>
                        @endcan
                      @endif

                      </td>
                    </tr>


                    @endforeach

                  </tbody>

                </table>
                 {!!$user->appends(Request::only('filtro'))->render()!!}
                {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}

          <div class="form-group">
          <div class="modal fade" id="modalempleados" tabindex="10"  role="dialog" aria-labelledby="modalempleado"
           aria-hidden="true" >
           <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header label">

              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
            </div>
             <div class="modal-body content-centered">

               <div class="col-sm-12">
              <label for="cargo">Permisoss</label>
                <div class="input-group lis">
                  <form  class="enviar container-fluid" >

                  </form>
                </div>
              </div>
              </div>
           <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-borrar" onclick="Borrar()"
            data-dismiss="modal" ><b> Cerrar</b></button>
           </div>
         </div>
       </div>
     </div>

    </div>


   </div>

@endsection
@section('lista')

<script>

    let permisos = [];
    let rutas = [];
    let  pos = 0;
    let rut = '';
    var pasos = false;
  $(document).ready(function(et) {


    //  $('.mostrar').on('click',function(e) {

    //    e.preventDefault();
    //    var url   = $(this).attr('href');
    //    var data  = $('h5#modalempleadolabel')
    //    var texto = $('div.selector');
    //    var datos = [];
    //     var indice = 0;
    //    $.get(url,function (result) {

    //      $('div.label').
    //       html('<h5 class="modal-title" id="modalempleadolabel" >DETALLES DEL USUARIO Y SUS PERMISOS '+result.nombre+' </h5>')
    //       var t = $(result.modulo).attr('id');
          
    //       $('#modalempleado').modal({ backdrop: "static", keyboard: false });
    //       $(texto).html(result.modulo);
    //      // $('#'+t).DataTable();
    //    // $.each(result.permisos,function(key,reg) {

    //      // });
            

    //    });
    //       $(texto).empty();

    // });

    //lista modal de eliminar accesso
    $('.delete').on('click',function(e) {

       e.preventDefault();
       var url   = $(this).attr('href');
       var data  = $('h5#modalempleadolabel')
       var texto = $('form.enviar');
       $.get(url,function (result) {

          console.log($(result[0]));
          $('div.label').html('<h5 class="modal-title" id="modalempleadolabel" >ELIMINAR ACCESSO DEL USUARIO\n Y SUS PERMISOS (<strong>'+result.user+'</strong>)</h5>')

         var t = $(result.modulo).attr('id');
         $(texto).html(result.modulo);
          console.log(t)
          
          

          $('#modalempleados').modal({ backdrop: "static", keyboard: false });
          $('#'+t).DataTable();




         });
          $(texto).empty();
       });
});

function paso(i,id) {
      var val = $('#'+id).val();
      var rutas = $('#modulo'+i).data('id2');
      permisos.push(val+rutas);
      pasos = true;
      
}


async function Borrar() {
  
  if (pasos === true) {
    var rutas = "{{route('delPermisos')}}";
    let datos = {
        "_token":"{{csrf_token()}}",
        "nombre":$('#idss').data('user'),
        "permisosRutas":permisos
    }    
    
    await $.post(rutas,datos,function (result) {
          window.location.href = "/modulo/Seguridad/Permisos";
          });

    permisos = [];//reinicia el array
  
  }else{
    Swal.fire({
        title: 'Tienes Que Marcar La Casilla Confirmar Para Eliminar',
        icon: 'warning',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'OK'
      });
  }

    

}



</script>
@endsection