
  @if(Session::has('error'))
    @if ($message = Session::get('error'))
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fas fa-ban"></i> <b>Error!</b></h5>
      <strong>{{ $message }}</strong>
      </div>
      {{Session::forget('error')}}
    @endif
  @endif

  @if(Session::has('info'))
    @if ($message = Session::get('info'))
      <div class="alert alert-info alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fas fa-info"></i> <b>Alerta!</b></h5>
      <strong>{{ $message }}</strong>
      </div>
      {{Session::forget('info')}}
    @endif
  @endif

  @if(Session::has('warning'))
    @if ($message = Session::get('warning'))
      <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fas fa-exclamation-triangle"></i> <b>Alerta!</b></h5>
      <strong>{{ $message }}</strong>
      </div>
      {{Session::forget('warning')}}
    @endif
  @endif
  
  @if(Session::has('success'))
    @if ($message = Session::get('success'))
    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-check"></i> <b>Exito!</b></h5>
    <strong>{{ $message }}</strong>
    </div>
    {{Session::forget('success')}}
    @endif
  @endif

