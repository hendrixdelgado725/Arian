@extends('layouts.app2')

@section('content')
 
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
    <!-- Content Header (Page header) -->
   

    <!-- Main content -->
    <section class="content">
      <div class="error-page mt-">
        <h2 class="headline text-warning"> 401</h2>

        <div class="error-content">
          <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! No tienes los credenciales de administrador.</h3>

          <p>
            Consulte con el Administrador del Sistema para que pueda Entrar a este modulo.
            {{-- <a href="{{ route('login') }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i>Volver</a> --}}
          </p>

         
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>

        </div>
    </div>
</div>

  @endsection