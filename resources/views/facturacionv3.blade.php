<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link href="/css/select2.min.css" rel="stylesheet">
  <title>Facturacion</title>
  <link rel="icon" href="{!! asset('images/arian/principal/ARIAN_negro_negro.png') !!}"/>
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('cssadminlte/adminlte.min.css') }}">
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Google Font: Source Sans Pro -->
  {{-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> --}}
  
</head>
<body   class="hold-transition sidebar-collapse layout-top-nav" data-url2="{{ route('closeBox',encrypt(Auth::user()->loguse)) }}" data-url="{{ route('updatedWindows',encrypt(Auth::user()->loguse)) }}">
  
<div id="app">

{{-- @extends('layouts.app') --}}
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @csrf
    <!-- Content Header (Page header) -->
    <div class="content-header" style="background-color: black ; padding-bottom : 0px">
      <div class="row">
        <div class="col-2">
          <div class="login-logo ml-4" style="text-align: left">
             <img src="{{ asset('/images/arian/inicio/arian-logo.png') }}" alt="Arian"  height="40px" >
          </div>
        </div>
        <div class="col-4">
          <div class="opscajas" style="text-align: left; display:inline">
            <div class="col-sm-12">
              <h4 class="m-0 text-white cajas"><b>CAJA CERRADA</b>
                <i class="fa fa-lock" id="abrirCaja"></i>
                <i class="fa fa-lock-open" style="display: none" id="cerrarCaja"></i>
              </h4>
              <h4 class="m-0 text-white">
                <b>
                  <span style="color: blue">VENDEDOR</span> :  {{ Auth::user()->nomuse}}
                </b>
              </h4>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div>
        <div class="col-4">
          <a type="button" id="home" class="btn btn-warning mt-3" ><b>REGRESAR A MENU PRINCIPAL </b><i class="fas fa-home"></i> 
              </a>
        </div>
        <div>
          <h4 style="color: #eaf4fff7">
            Tasa de Cambio : {{ $tasa->valor }} Bs
          </h4>
        </div>
      </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div  class="content" style="min-height: 496.8px;background-image: url('{{ asset('/images/arian/inicio/arian-fondo.png')}}');">
      
      <div >
          <div class="row justify-content-center responsive p-0">
            <div class="col-lg-4">
              <cliente-component></cliente-component>

           <div class="col-12">
                    <opciones-component></opciones-component>

            </div>
          </div>

                    <detalles-component></detalles-component>
            
                  </div>
                </div>
                
              </div><!-- /.card -->
              
            </div>
            <!-- /.col-md-6 -->
          </div>
          <!-- /.row -->
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>

    <div class="col" style="display:none">
      <a type="button" data-href="{{route('getPersonaF')}}" class="btn btn-info mt-4 ml-2" id="verificarCliente"><b>Verificar Cliente</b></a>
    </div>
    <input id="clienttojson" type="hidden" value="{{$clienttojson}}">
    <input id="arttojson" type="hidden" value="{{$arttojson}}">
    <input id="fpagotojson" type="hidden" value="{{$fpagotojson}}">
    <input id="bancostojson" type="hidden" value="{{$bancostojson}}">
    <input id="tasatojson" type="hidden" value="{{$tasatojson}}">
    <input id="recargostojson" type="hidden" value="{{$recargostojson}}">
    <input id="monedastojson" type="hidden" value="{{$monedastojson}}">
    <input id="todastasastojson" type="hidden" value="{{$todastasastojson}}">
    <input id="monedac" type="hidden">
    <input id="monedacvalor" type="hidden">
    <input id="transportevalor" type="hidden" value="{{$transporte}}">
    <input type="hidden" id="printFactura"  value="{{route('printFactura')}}">
    
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!--modal Login Caja-->

{{-- VISTA MODAL BLOQUEO DE CAJA --}}

  <form id="formSecureCaja" action="">
    <input type="hidden" name="" id="urlCheckCaja" value="{{route('abrirCaja')}}">
    <input type="hidden" name="" id="tokenUser" value="{!!encrypt(Auth::user()->loguse)!!}">
    <input type="hidden" name="" id="loginCaja" value="{{ route('loginCaja') }}">
    <div class="modal fade" id="lockedCaja" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            {{-- <h5 class="modal-title" id="staticBackdropLabel">Seguridad de Caja</h5> --}}
            {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button> --}}
          </div>
          <div class="modal-body">
            <div class="row justify-content-center">
              <h4> Caja Cerrada <i class="fa fa-lock"></i></h4>
            </div>
              <div id="abrirCajaMenu">
                  <div class="form-group">
                    <label for="cajaSelected" class="col-sm-2 col-form-label">Cajas Asociada</label>
                    <div class="col-sm-12">
                      <select id="cajaSelected" class="form-control cajs" style="width: 100%;"  data-select2-id="1" tabindex="-1" aria-hidden="true" class="@error('tipodocumento') is-invalid @enderror">
                      </select>
                    </div>
                </div>
                <div class="form-group ">
                  <label for="passwordCaja" class="col-sm-2 col-form-label">Password</label>
                  <div class="col-sm-12">
                    <input type="password" class="form-control" id="passwordCaja">
                  </div>
                </div>
              </div>
          </div>
          <div class="modal-footer">
            {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
            <button type="submit" class="btn btn-primary"><b>ABRIR CAJA</b></button>
          </div>
        </div>
      </div>
    </div>
  </form>
    
         <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Abrir Caja</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
                <div class="modal-body" >

              <form class="form-horizontal" method="post" data-login="{{ route('loginCaja') }}" data-user="{!!encrypt(Auth::user()->loguse)!!}" data-url="{{ route('abrirCaja') }}">
                            <!-- form start -->
                            {{csrf_field()}}
                <div class="card-body">
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Cajas Asociada</label>
                    <div class="col-sm-12">
                      <select class="form-control cajs" style="width: 100%;" data-caja="{{ route('getCajas') }}" data-select2-id="1" tabindex="-1" aria-hidden="true" name="tipodocumento" class="@error('tipodocumento') is-invalid @enderror">
                        
                    </select>
                      <input type="text" name="_token" style="display: none;" value="{!!csrf_token()!!}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-12">
                      <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                    </div>
                  </div>
                
                </div>
                <!-- /.card-body -->
                
              </form>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
               <button type="submit" class="btn btn-info" id="enviar"><b>Seleccionar</b></button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


  <!--fin modal login Caja-->
  <form action="" id="formCliente">
            <div class="modal fade" id="nuevocliente" tabindex="-1" role="dialog" aria-labelledby="nuevocliente" aria-hidden="true">
           <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content"> 
            <div class="modal-header">
             <h5 class="modal-title" id="nuevoclientelabel"><b> Agregar Cliente</b></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
            </div>
             <div class="modal-body content-centered">
             <div class="row">
                 <div class="col-sm-6">
                 <div class="form-group">
                  <label for="tipodocumento">Tipo de documento</label>
                    <select class="form-control" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" id="tipodocumento2" name="tipodocumento" class="@error('tipodocumento') is-invalid @enderror">
                        <option data-select2-id="3">V</option>
                        <option data-select2-id="43">E</option>
                        <option data-select2-id="44">J</option>
                        <option data-select2-id="45">G</option>
                        <option data-select2-id="46">P</option>
                        <option data-select2-id="47">R</option>
                        <option data-select2-id="48">C</option>
                    </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                    @error('tipodocumento')
                     <div class="alert alert-danger">{!!$message!!}</div>
                   @enderror
                  </div>     
                 </div>

                 <div class="col-sm-6">
                <div class="form-group">
                <label>Numero de documento<span style="color:red"> *</span></label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">V/J</span>
                  </div>
                 
                  
                  <input type="text" class="form-control"  onKeypress = "javascript:return NumerosReales(event)" placeholder="Ejem: 1234567" name="cedularifmodal" 
                  id="cedularifmodal" class="@error('cedularifmodal') is-invalid @enderror">
                </div>
                @error('cedularifmodal')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
               </div>
               </div>

               <div class="form group">
                <label for="nombre">Nombre<span style="color:red"> *</span></label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Aa</span>
                  </div>
                  <input type="text" class="form-control" onkeyup="pasarMayusculas(this.value, this.id)" class="@error('nombremodal') is-invalid @enderror" placeholder="Ejem: Juan Gutierrez" name="nombremodal" id="nombremodal">
                </div>
                @error('nombremodal')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                <div class="form group">
                    <label for="direccion">Dirección</label>
                <div class="input-group mb-3">
                  <textarea type="text" class="form-control" onkeyup="pasarMayusculas(this.value, this.id)" class="@error('direccion') is-invalid @enderror" placeholder="Dirección Fiscal..." name="direccionmodal" id="direccionmodal"> </textarea>
                </div>
                @error('direccionmodal')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                <div class="form group">
                <label for="telefono">Numero de teléfono (opcional)</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-phone-square"></i></span>
                  </div>
                  <input type="tel" class="form-control" onKeypress = "javascript:return NumerosReales(event)" class="@error('telefono') is-invalid @enderror" placeholder="Ejem: 041..." name="telefono" id="telefono"> 
                </div>
                @error('telefono')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                <div class="form group">
                <label for="email">Correo Electrónico (opcional)</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="email" class="form-control" class="@error('email') is-invalid @enderror"  placeholder="Ejem: Juan@email.com" name="email" id="emailcliente"> 
                </div>
                @error('email')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                 
            </div> <!--modal body -->

           <div class="modal-footer" id="loading">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><b> Cerrar</b></button>
            <button type="button" class="btn btn-success " data-href="{{route('registrarclienteModal')}}" id="guardarClienteF"><b> Guardar</b></button>
           </div>
         </div>
       </div>
     </div>
    </form>

    <!--modal descuento-->
    <form id="formDescuento">
       <div class="modal fade" id="descuentoM" tabindex="-1" role="dialog" aria-labelledby="descuentoM" aria-hidden="true">
           <div class="modal-dialog modal-dialog-centered modal-lg"  role="document">
            <div class="modal-content" > 
            <div class="modal-header">
             <h5 class="modal-title" style="text-align:center" id="descuentoM"><b>OPCIONES DE DESCUENTO</b></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
            </div>
             <div class="modal-body content-centered">
              <div class="row mb-4" style="text-align:center">
               <div class="col">
                 <input type="radio" id="descuentoR"  checked name="tdescuento" value="general">
                 <label for="tdescuento">GENERAL</label><br>
               </div>
               <div class="col">
                 <input type="radio" id="descuentoR" name="tdescuento" value="articulo">
                 <label for="tdescuento">POR ARTICULO</label><br>
               </div>
             </div>

                  <div class="card-body table-responsive p-0" id="tablaInventario" style="height: 250px; display:none">
                        <table class="table table-head-fixed">
                          <thead>
                            <tr style="text-align:center">
                              <th>CODIGO</th>
                              <th>ARTICULO</th>
                              <th>SELECCIONAR</th>
                            </tr>
                          </thead>
                          <tbody class="listInventario">

                          </tbody>
                        </table>
                      </div>
            
                <div style="display: block" id="descuentosOpciones">
                <!--  <h6 style="text-align:center" class="title"><b>SELECCIONE EL DESCUENTO O INGRESE MANUALMENTE</b></h6> -->
                 <br>
                 <div class="row" style="text-align:center">
                  <div class="col-6">
                        <button type="button" id="precargado" class="button border1 border11 mb-3 precargado">
                          <b>PRECARGADO</b>
                        </button>
                  </div>

                   <div class="col-6">
                         <button type="button" id="manual" class="button border1 border11 mb-3 manual">
                          <b>INGRESAR MANUALMENTE</b>
                        </button>
                  </div>
                 </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                   
                          <select name="descuentosSelect" id="descuentosSelect" class="form-control" data-dropdown-css-class="select2-danger" disabled class="@error('descuentosSelect') is-invalid @enderror select" aria-hidden="true">
                            <option disabled selected>SELECCIONE</option>
                              @foreach($descuentos as $descuento)
                               <option value="{{$descuento->mondesc}}">{{$descuento->mondesc}}</option>
                              @endforeach
                              </select>
                              </div>
                    </div>
                    <div class="col-sm-6">

                      <div class="form-group">
                        <input type="text"  class="form-control" value="0" name="descuentoInput" onKeypress = "javascript:return SoloNumeros(event)" id="descuentoInput">
                       </div>
                    </div>
                  </div>
                  </div>

                 
            </div> <!--modal body -->

           <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><b> Cerrar</b></button>
            <button type="button" class="btn btn-success" disabled id="guardarDescuento"><b> Guardar</b></button>
           </div>
         </div>
       </div>
     </div>
    </form>
    <!--Fin modal descuento-->

    <!--Modal devolución-->
    <form id="formDevolucion">
      <div class="modal fade" id="devolucionM" tabindex="-1" role="dialog" aria-labelledby="devolucionM" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
           <div class="modal-content"> 
             <div class="modal-header">
             <h5 class="modal-title" style="text-align:center" id="devolucionM"><b>DEVOLUCION</b></h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
               </button>
             </div>
           <div class="modal-body content-centered">
             <div class="form-group">
               <div class="row">
                 <div class="col-lg-4 offset-lg-3">
                   <input type="text" id="codfactura" class="ml-3 form-control" name="codfactura" placeholder="N° de Factura">
                 </div>
                 <div class="col-lg-4 ">
                   <button type="button" class="btn btn-info ml-3" id="buscarFactura" data-href="{{route('buscarFactura')}}">BUSCAR</button>
                 </div>
               </div>
               <div class="row  mt-2 ">
                 <div class="ErrorTxt col-6 offset-3">

                 </div>
               </div>
             </div>
           </div>
             <div class="card-body table-responsive p-0" id="tablaDevolucionFac">
               <table class="table table-head-fixed">
                 <thead>
                   <tr style="text-align:center">
                     <th>N° FACTURA</th>
                     <th>DOCUMENTO CLIENTE</th>
                     <th>CLIENTE</th>
                     <th>MONTO</th>
                     <th>ANULAR</th>
                   </tr>
                 </thead>
                 <tbody class="listFactura">
                 </tbody>
               </table>
             </div>
             <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><b> Cerrar</b></button>
              <button type="button" class="btn btn-success" id="anulacion" style="display:none" data-href="{{route('anularFactura')}}"><b> Guardar</b></button>
             </div>
           </div> <!--modal body -->
        </div>
      </div>
   </form>
    <!--Fin modal devolución-->

        <!--Modal iva-->
    <form id="formIva">
       <div class="modal fade" id="ivaM" tabindex="-1" role="dialog" aria-labelledby="ivaM" aria-hidden="true">
           <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content"> 
            <div class="modal-header">
             <h5 class="modal-title" style="text-align:center" id="ivaM"><b>SELECCIÓN DE IVA</b></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
            </div>
             <div class="modal-body content-centered" style="text-align:center">
               <div class="col ml-2 mr-4">
                <select name="recargos" id="recargos" data-href="{{route('getRecargo')}}" class="form-control" data-dropdown-css-class="select2-danger" class="@error('recargos') is-invalid @enderror" aria-hidden="true">
                  <option disabled selected>-- IVA --</option>
                  <option value="00.00"> SIN IVA </option>
                 @foreach($recargos as $iva)
                  <option value="{{$iva->monrgo}}">{{$iva->nomrgo}}</option>
                  @endforeach
                </select>
               </div>
            </div> <!--modal body -->

           <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><b> CERRAR</b></button>
            <button type="button" disabled class="btn btn-info ml-3" id="guardarIva" ><b>GUARDAR</b></button>
           </div>
         </div>
       </div>
     </div>
    </form>
    <!--Fin modal iva-->

    <!--Modal presupuesto facturacion-->
     <form id="formPresupuesto">
       <div class="modal fade" id="presupuestoM" tabindex="-1" role="dialog" aria-labelledby="presupuestoM" aria-hidden="true">
           <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content"> 
            <div class="modal-header">
             <h5 class="modal-title" style="text-align:center" id="presupuestoM"><b>FACTURACION DE PRESUPUESTO</b></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
            </div>
             <div class="modal-body content-centered">
              <div class="row mb-4" style="text-align:center">
               <div class="col">
                 <label>N° DE PRESUPUESTO: </label>
                 <input type="text" id="codpresupuesto" class="ml-3" name="codpresupuesto" placeholder="Ingrese el N° de Presupuesto...">

                  <button type="button" class="btn btn-info ml-3" id="buscarPresupuesto" data-href="{{route('buscarPresupuestoFac')}}">BUSCAR</button>
               </div>
               <!-- <div class="col">
                 <input type="radio" id="descuentoR" name="tdescuento" value="articulo">
                 <label for="tdescuento">POR ARTICULO</label><br>
               </div> -->
             </div>

                  <div class="card-body table-responsive p-0" id="tablaPresupuestoFac" style="height: 150px; display:none">
                        <table class="table table-head-fixed">
                          <thead>
                            <tr style="text-align:center">
                              <th>N° PRESUPUESTO</th>
                              <th>DOCUMENTO CLIENTE</th>
                              <th>CLIENTE</th>
                              <th>SELECCIONAR</th>
                            </tr>
                          </thead>
                          <tbody class="listPresupuesto">

                          </tbody>
                        </table>
                      </div>

                 
            </div> <!--modal body -->

           <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><b> Cerrar</b></button>
            <button type="button" class="btn btn-success" id="seleccionPre" style="display:none" data-href="{{route('seleccionarPre')}}"><b> Guardar</b></button>
           </div>
         </div>
       </div>
     </div>
    </form>
    <!--Fin Modal presupuesto facturacion-->

    <!-- Modal moneda de pago -->
      <form id="formMoneda">
       <div class="modal fade" id="monedaM" tabindex="-1" role="dialog" aria-labelledby="monedaM" aria-hidden="true">
           <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content"> 
            <div class="modal-header">
             <h5 class="modal-title" style="text-align:center" id="monedaM"><b>LISTA DE MONEDAS</b></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
            </div>
             <div class="modal-body content-centered">
              <div class="col">
                <select name="monedas" id="monedas" class="form-control"  data-dropdown-css-class="select2-danger" aria-hidden="true">
                  <option disabled selected>--SELECCIONE MONEDA --</option>
                  @foreach($monedas as $moneda)
                  <option value="{{$moneda->codigoid}}">{{$moneda->nombre}}</option>
                  @endforeach
                </select>
              </div>

            </div> <!--modal body -->

           <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><b> Cerrar</b></button>
            <button type="button" class="btn btn-success" id="selecMoneda" data-href="{{route('getMoneda')}}" data-href2="{{route('getTasaMoneda')}}"><b> Guardar</b></button>
           </div>
         </div>
       </div>
     </div>
    </form>
    <!-- -->

        <!--Modal presupuesto nota entrega-->
     <form id="formNota">
       <div class="modal fade" id="notaM" tabindex="-1" role="dialog" aria-labelledby="notaM" aria-hidden="true">
           <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content"> 
            <div class="modal-header">
             <h5 class="modal-title" style="text-align:center" id="notaM"><b>FACTURACION DE NOTA ENTREGA</b></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
            </div>
             <div class="modal-body content-centered">
              <div class="row mb-4" style="text-align:center">
               <div class="col">
                 <label>N° DE NOTA: </label>
                 <input type="text" id="codnota" class="ml-3" name="codnota" placeholder="Ingrese el N° de Nota...">

                  <button type="button" class="btn btn-info ml-3" id="buscarNota" data-href="{{route('buscarNotaFac')}}">BUSCAR</button>
               </div>
               <!-- <div class="col">
                 <input type="radio" id="descuentoR" name="tdescuento" value="articulo">
                 <label for="tdescuento">POR ARTICULO</label><br>
               </div> -->
             </div>

                  <div class="card-body table-responsive p-0" id="tablaNotaFac" style="height: 150px; display:none">
                        <table class="table table-head-fixed">
                          <thead>
                            <tr style="text-align:center">
                              <th>N° NOTA DE ENTREGA</th>
                              <th>DOCUMENTO CLIENTE</th>
                              <th>CLIENTE</th>
                              <th>SELECCIONAR</th>
                            </tr>
                          </thead>
                          <tbody class="listNota">

                          </tbody>
                        </table>
                      </div>

                 
            </div> <!--modal body -->

           <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><b> Cerrar</b></button>
            <button type="button" class="btn btn-success" id="seleccionNota" style="display:none" data-href="{{route('seleccionarNota')}}"><b> Guardar</b></button>
           </div>
         </div>
       </div>
     </div>
    </form>
    <div class="modal fade" id="modalSerial" tabindex="-1" role="dialog" aria-labelledby="notaM" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
       <div class="modal-content" style = "height: 800px"> 
       <div class="modal-header">
        <h5 class="modal-title" style="text-align:center" id="producto"><b>SERIALES DEL PRODUCTO </b></h5>
        
       </div>
       <div class="col-md-3">
        <label for="searchSeriales">Serial</label> <input id="serialesValues"  placeholder="Buscar Seriales" type="text" class="form-control">
        <button class="btn btn-primary" id="searchSeriales">Buscar</button>
      </div>
        <div class="modal-body content-centered">
         

             <div class="card-body table-responsive p-0"  style="height: 400px;">
                   <table class="table table-head-fixed">
                     <thead>
                       <tr style="text-align:center">
                          
                         <th>SERIAL</th>    
                         <th>FACTURAR</th>                     
                       </tr>
                     </thead>
                     <tbody id="listaSeriales">
                     
                     </tbody>
                   </table>
                 </div>

            
       </div> <!--modal body -->

      <div class="modal-footer">
       <button type="button" class="btn btn-danger" id="cerrarSerial" ><b> Cerrar</b></button>
      </div>
    </div>
  </div>
</div>


   {{-- <div class="modal fade" id="descripcionAdd" tabindex="-1" role="dialog" >
          <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
           <div class="modal-content"> 
             <div class="modal-header">
             <h5 class="modal-title" style="text-align:center" ><b>DESCRIPCION DEL ARTICULO </b></h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
               </button>
             </div>
           <div class="modal-body content-centered">
             <div class="form-group">
               <div class="row">
                 <div class="col-lg-11 offset-lg-0">
                   <label>ARTICULO</label>
                   <textarea id="articuloDesc"  class="ml-3 form-control"></textarea> 
                   <label>DESCRIPCION</label>
                   <input type="text" id="descArticulo" class="form-control">
                 </div>
               </div>
             </div>
           </div>
            
             <div class="modal-footer">
              
              <button type="button" id="guardarDescripcion" class="btn btn-success" ><b> Guardar</b></button>
             </div>
           </div> <!--modal body -->
        </div>
      </div> --}}
      <div class="modal fade" id="arqueCaja" tabindex="-1" role="dialog" >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
         <div class="modal-content"> 
           <div class="modal-header">
           <h5 class="modal-title" style="text-align:center" ><b>ARQUEO DE CAJA DE HOY {{$dia}}</b></h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
             </button>
           </div>
         <div class="modal-body content-centered">
           <div class="form-group">
             <div class="row">
               <div class="col-lg-11 offset-lg-0" id="totalCaja">
                 
                 
               </div>
             </div>
           </div>
         </div>
          
           
         </div> <!--modal body -->
      </div>
    </div>


      {{-- <conexion-fiscal channel="{{ $caja }}"></conexion-fiscal> --}}


  <!-- Main Footer -->
  <footer class="main-footer" style="position:relative; margin-left: 0px;text-align: center; background-color: #201d1c">
    <strong id="year"></strong> All rights reserved.
  </footer>
</div>
</body>
</html>
