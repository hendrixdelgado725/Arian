@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Registro Pais')
@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
    @endslot
  @endcomponent
  {{-- CONTENT --}}
  <section class="content">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-8">
          
          <div class="card card-info">
            @include('vendor/flash.flash_message')
            <div class="card-header">
            <h3 class="card-title"><b>EDITAR PA&Iacute;S</b></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{route('paisUpdate', $pais)}}" method="post">
              {{csrf_field()}}
              <div class="card-body">
              <div class="form group">
                <label for="descaj">Pa&iacute;s</label>
                <div class="input-group mb-3">
     
                  <input class="form-control" name="nompai" value="{{ $pais->nompai }}"
                  id="nompai" class="@error('nompai') is-invalid @enderror" />
                  
                </div>
                @error('nompai')
                <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
              </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button id="createcountry" type="submit" class="btn btn-success"><b>Guardar Cambios</b></button>
              <a href="{{route('paisList')}}" class="btn btn-default ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
            </div>
            </form>
            </div>
            <!-- /.card -->
            </div>
        </div>
      </div>
        {{-- row --}}
    </div>
    {{-- container fluid --}}
  </section>
@endsection
@section('script')
    {{-- Script jquery --}}
<script>
  $(document).ready(()=>{
    putcountry();
    $('select').select2();
      function putcountry(){//se insertaran paises a la configuracion del sistema siempre y cuando Existan
      let uri = 'https://restcountries.eu/rest/v2/lang/es'
      $.getJSON(uri).done(
        (data)=>{
          $.each(data,(index,val) =>{
            $('#nompai').append(
              '<option value ="'+val.nativeName+'" >'+val.nativeName+' / '+val.alpha2Code+'</option>'
            )
          })
        })
      }
  })

</script>
<script>
      $(document).ready(()=>{
        $('select.select2').select2({
        });
      })
</script>
@endsection