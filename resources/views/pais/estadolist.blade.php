@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Estados')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Listado Estados
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <!-- <div class="row justify-content-center"> -->
      <!-- <div class="col-12"> -->
        
        <div class="card">
          @include('vendor/flash.flash_message')
          <div class="card-header">
            <h3 class="card-title"></h3>
              <div class="card-tools">
                <div class="row">
                	<div class="col-sm-4 mt-2">
                    @can('create', App\permission_user::class)

                    <a href="{{route('estadoCreate')}}" class="btn btn-info mt-2"><b>REGISTRAR</b></a>
                    @endcan
                  </div>
                  <div class="col mt-2">
                    <form action="{{route('estadoFiltro')}}" method="GET">
                      @csrf
                      <div class="input-group mt-2">
                        <input type="text" name="filtro" placeholder="Buscar" class="form-control float-right">
                        <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                        <a href="{{route('estadoList')}}" class="btn btn-default"><i class="fas fa-arrow-left"></i></a>                       
                      </div>
                    </form>
                  </div>
                </div>
              </div>
          </div>


          <div class="card-body">
            <table class="table text-center">
              <thead>
                <tr>
                  <th scope="col">#</th> -
                  <th scope="col">Nombre</th>
                  <th scope="col">Pais</th>
                  <th scope="col">Acci&oacute;n</th>
                </tr>
              </thead>
              <tbody>
                @if (count($estados)>0)
                @foreach ($estados as $item)
                @php
                    $id=Helper::crypt($item->id);
                @endphp
                <tr>
                  <td>{{$estados->firstItem() + $loop->index}}</td>
                  <td>{{$item->nomedo}}</td>
                  <td>{{$item->pais->nompai}}</td>
                <td>
                  @can('edit', App\permission_user::class)
                  <a href="{{route('estadoShow',$id)}}"><i class="fa fa-edit"></i></a>
                  /   
                  @endcan
                  @can('delete', App\permission_user::class)
                      
                  <a id="delete" href="{{route('estadoDelete',$id)}}"><i class="fa fa-trash red"></i></a>
                  @endcan
                </td>
                </tr>
                @endforeach
                @else
                  {!! "No hay datos que mostrar" !!}
                @endif
              </tbody>
            </table>
            {!!$estados->render()!!}
              {{-- col8  --}}
          </div>
          </div>
        <!-- </div> -->
    <!-- </div> -->
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section>
@endsection
@section('script')
<script>
  $(function(){
      deletestado();
      function deletestado(){
        $(document).on('click','a#delete',function(e){
          e.preventDefault();
          const row = $(this).parents('tr');
          const url = $(this).attr('href');
		  		Swal.fire({
				  title: 'Seguro que Desea Eliminar?',
				  text: "Despues de esto no se encontrara en la lista!",
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si, Borrar!'
        }).then((result) => {//dio click en el boton aceptar
          if(result.value){
            $.get(url,function(result){
              if (result.exito) {
                Swal.fire(
                  'Ha sido Eliminado!',
                  'Con Exito.',
                  'success',
                  )  
                  row.fadeOut();
                }else if(result.error){
                  Swal.fire({
                    icon: 'warning',
                    title: result.error,
                    timer:1500
                  })
                }
              })
            }
          })
        })
      }
  });
</script>
@endsection