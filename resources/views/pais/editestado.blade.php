@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Editar Estado')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Estado
    @endslot
  @endcomponent
  {{-- CONTENT --}}
  <section class="content">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-8">
          <div class="card card-info">
            @include('vendor/flash.flash_message')
            <div class="card-header">
            <h3 class="card-title"><b>EDICI&Oacute;N  ESTADO</b></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{route('estadoUpdate',$estado->id)}}" method="POST">
              @method('PUT')
              {{csrf_field()}}
              <div class="card-body">
              <div class="form group">
                <label for="fapais_id">Pa&iacute;s</label>
                <div class="input-group mb-3">
                  <!-- <div class="input-group-prepend">
                    <span class="input-group-text"></span>
                  </div> -->
                  <select class="select2 form-control @error('fapais_id') is-invalid @enderror"  name="fapais_id" 
                  id="fapais_id">
                    @foreach($paises as $key)
                      @php($selected = $key->id === $estado->fapais_id ? 'selected' : '')
                      <option value="{{$key->id}}" {{$selected}}>{{ $key->nompai }}</option>
                    @endforeach
                </select>
                </div>
                @error('fapais_id')
                <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
              </div>
              <div class="form group">
                <label for="descaj">Nombre del Estado</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Aa</span>
                  </div>
                  <input class="form-control" name="nomedo" onkeyup="pasarMayusculas(this.value, this.id)"
                  id="nomedo" class="@error('nomedo') is-invalid @enderror" value="{{$estado->nomedo}}">
                </div>
                @error('nomedo')
                <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
              </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button id="createcountry" type="submit" class="btn btn-success"><b>Guardar Cambios</b></button>
              <a href="{{route('estadoList')}}" class="btn btn-default ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
            </div>
            </form>
            </div>
            <!-- /.card -->
            </div>
        </div>
      </div>
        {{-- row --}}
    </div>
    {{-- container fluid --}}
  </section>
@endsection
@section('script')
    {{-- Script jquery --}}
 <script src="{{asset('js/chequeo.js')}} "></script>
<script>
      $(document).ready(()=>{
        $('select.select2').select2({
        });
      })
</script>
@endsection