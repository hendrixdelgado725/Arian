<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html >
  <head>
  <!-- If you delete this tag, the sky will fall on your head -->
    <meta name="viewport" content="width=device-width" />
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>A1WIN-MAIL</title>
      <style type="text/css">
        @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@700&display=swap');
        *{
          margin: 0;
          border: 0;
          padding: 0;
        }
        body{
          font-family: 'Roboto', sans-serif;
          background-color: gainsboro;
          font-size: 18px;
          max-width: 860px;
          margin: 0 auto;
          padding : 2%;
          color: darkslategrey;
        }
        #wrapper{
          background:ghostwhite;
        }
        #logo{
          margin: 2% 0 0 5%;
          float: left;
        }
        .one-col{
          padding: 2%;
        }
        h2{
          letter-spacing: 1%;
        }
        p{
          text-align: justify;
        }
        .button-holder{
          float: right;
          margin: 0 2% 4% 0;
        }
        .btnrec{
          float: right;
          background: #ec2f2f;
          color: #f6faff;
          text-decoration: none;
          padding: 8px 12px;
          border-radius: 8px;
          letter-spacing: 1px;
          margin: auto 3px;
        }
        .btnapr{
          float: right;
          background: #61dc5a;
          color: #f6faff;
          text-decoration: none;
          padding: 8px 12px;
          border-radius: 8px;
          letter-spacing: 1px;
          margin: auto 3px;
        }
        .btnapr:hover{background: #73e16c;}
        .btnrec:hover{background: #ee4848;}
        .line{
          clear:both;
          height: 2px;
          margin: 4% auto;
          opacity: 0.2;
          width: 96%;
        }
        .lineart{
          clear:both;
          height: 2px;
          margin: 2% auto;
          opacity: 0.2;
          width: 96%;
        }
        table {
            border-collapse: collapse;
            width: 100%;
            display: table;
            box-sizing: border-box;
            border-spacing: 2px;
            font-size: 0.9em;
        }
        thead tr{
          background-color: #337388;
          color: #f6faff;
          text-align: left;
          font-weight: bold;
        }
        th,td{
          padding: 8px 10px;
        }
        tbody tr{
          border-bottom: 1px solid #dddddd;
        }
        tbody tr:nth-of-type(even){
          border-bottom: 1px solid #f3f3f3;
        }
        tbody tr:nth-last-of-type(){
          border-bottom: 2px solid #f3f3f3;
        }
      </style>
  </head>
    <body>
      <div id="wrapper">
        <header>
          <div id="logo">
            <img width="80" height="120" src="A1win-logo-Negro-120.jpg" alt="">
          </div>
          <div class="line"></div>

          <div class="one-col">
            <h1>{{$subject}}</h1>
            <p>
              Se ha creado un {{$movimiento}} en la fecha {{$movimiento->fechaformat}}
            </p>
              <h4>
                Almacen Origen : {{$movimiento->almorides->nomalm}} <br>
                Almacen Destino : {{$movimiento->almdesdes->nomalm}}
              </h4>
          <br>
            <h1>Articulos</h1>
            <br>
          <table>
            <table>
              <thead>
                <th>Codigo</th>
                <th>Descripcion</th>
                <th>Color</th>
                <th>Categoria</th>
                <th>Subcategoria</th>
                <th>Demografia</th>
                <th>Talla</th>
                <th>Cantidad</th>
              </thead>
              <tbody>
                @foreach ($articulos as $item)
                  <tr>
                    <td>{{$item->articulo->codart}}</td>
                    <td>{{$item->articulo->desart}}</td>
                    <td>{{$item->color->color}}</td>
                    <td>{{$item->categoria->nombre}}</td>
                    <td>{{$item->subCategoria->nomsubcat}}</td>
                    <td>{{$item->demografia->nombreDemo}}</td>
                    <td>{{$item->talla->tallas}}</td>
                    <td>{{$item->cantmov}}</td>
                    {{-- almacenfrom->nomalm
                    almacento->nomalm
                    codubifrom->nomubi
                    codubito->nomubi --}}
                  </tr>
                @endforeach
              </tbody>
            </table>
          </table>
          <div class="line"></div>
          <div class="button-holder">
            <a href="" class="btnrec">Rechazar</a>
            <a href="" class="btnapr">Aprobar</a>
          </div>
          <div class="line"></div>
          </div>
        </header>
      </div>
    </body>
</html>