@component('mail::message')
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <meta name="viewport" content="width=device-width"/>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>MAIL VIT</title>
      
  </head>
<body>
<div class="" style="background-color: rgb(24, 19, 19); align-content: center; border-radius: 15px">
    <img src="{{ asset('/images/arian/principal/arian-logo-blanco.png') }}" height="70px" style="margin-right: auto; margin-left: auto; display: block;">
</div>

<div class="" style="background-color: #8dc3f2; border-radius: 15px;">
    <h4 style="text-align: center">INFORME DE REQUISICIÓN</h4>
</div>

<div class="">
    <p style="text-align: center">Una nueva requisición ha sido registrada en el departamento: <b>{{ $data->ccosto->descen }}</b></p>
</div>

<div class="container">
    <h3 style="text-align: center">DETALLES DEL REQUERIMIENTO</h3>
</div>

<div class="container" style="margin: auto;">
    <table style="margin: auto;">
        <thead class="bg-info">
            <tr style="text-align: center">
                <th>NÚMERO</th>
                <th>SOLICITANTE</th>
                <th>FECHA</th>
            </tr>
        </thead>
        <tbody style="text-align: center">
            <tr>
                <td> {{ $data->reqart }} </td>
                <td> {{ $data->solicitante->nomemp }} </td>
                <td> {{ $data->fecreq }} </td>
            </tr>
            <tr style="text-align: center">
                <td colspan="3" class="bg-info" style="border: 1px solid;">
                    <p style="text-align: center;"><b>JUSTIFICACIÓN</b></p>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="d-flex">
                    {{ $data->desreq }}
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div class="container text-center mt-2">
    <h3 style="text-align: center">ARTÍCULOS</h3>
</div>

<div class="container" style="margin: auto;">
    <table class="table" style="margin: auto;">
        <thead class="bg-info">
            <tr>
                <th>Código</th>
                <th>Artículo</th>
                <th>Cantidad</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data->articulos as $key)
            <tr>
                <td> {{ $key->codart }} </td>
                <td> {{ $key->desart }} </td>
                <td> {{ $key->canreq }} </td>
            </tr>
            @endforeach


</tbody>
</table>
</div>

@component('mail::button', ['url' => route('showReqP', $data->id)])
Ir a sistema
@endcomponent

<div style="background-color: rgb(0, 0, 0); color: rgb(240, 240, 240); margin: 0px auto; padding: 10px; box-shadow: rgb(153, 153, 153) 2px 2px 5px; border-radius: 7px; text-align: center; margin-top: 10px">
    <strong>Copyright &copy; 2021 <a>VIT Venezolana de Industria Tecnologica</a>.</strong> <br>
    All rights reserved.
 </div>
</body>
@endcomponent

</html>


<style>
    table {
        border: #b2b2b2 1px solid;
    }
    td, th {
        border: black 1px solid;
    }
    h3 {
        font-family: Arial, Helvetica, sans-serif;
        text-align: center;
    }
    .container{
        margin: auto;
        align-content: center;
    }
</style>