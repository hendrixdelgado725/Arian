
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html >
  <head>
  <!-- If you delete this tag, the sky will fall on your head -->
    <meta name="viewport" content="width=device-width" />
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>A1WIN-MAIL</title>
      
  </head>
    <body style="margin : 0; padding: 0;">
      <div style=" margin : 0 auto; max-width : 1000px ; text-align:center; background:#CBD8C9; border-radius: .45em; padding : 15px">
        <header>
          <div>
            <img width="80" height="120" src="{{asset('/images/A1winlogoNegro120.png')}}" style="display:block;padding:2px 0;
            "/>
          </div>
          <div>
          <h3 style="
            color:#293241;
            line-height: 30px;
            font-family: 'Helvetica';
          ">{{$subject}}</h3>
          <h3 style="
          color:#293241;
          line-height: 30px;
          font-family: 'Helvetica';
          "
          >Numero : </h3><h3 style="font-family: 'Helvetica';">{{$movimiento->codtra ?? $movimiento->codmov}}</h3>
            <p style="
            margin: 0 auto;
            color:#293241;
            margin : 0;
            text-align: center;
            font-size: 110%;
            font-family: 'Helvetica';
            ">
              Se ha generado {{$text}} el dia
              {{$fechaFormat}}
              a las {{$hora}}
            </p>
              <h3 style="color: #293241;font-family: 'Helvetica';">Observacion : {{$movimiento->obstra ?? $movimiento->desrcp}}</h3>
              <h3 style="color: #293241;font-family: 'Helvetica';">Fecha : {{$movimiento->created_at->format('d/m/Y')}}</h3>
              <p style=" margin: 0 auto;
              color:#293241;
              margin : 0;
              text-align: center;
              font-family: 'Helvetica';
              font-weight: bold;
              font-size:120%;
              ">
                @if (!$movimiento->nommov)
                  {!!'<strong>Almacen Origen</strong> : '.$movimiento->almorides->nomalm ?? ''!!} <br>
                  {!!'<strong>Almacen Destino</strong> : '.$movimiento->almdesdes->nomalm ?? ''!!}
                  @else
                  @php
                    $string = $movimiento->nommov === 'SAL' ? '<strong>Almacen Origen : </strong>'.$movimiento->almacenes->nomalm : "Almacen Destino : ".$movimiento->almacenes->nomalm; 
                    @endphp
                  {!!$string!!}
                  @endif
              </p>
            <div style="margin: 0; border-radius: .45em; font-family: 'Helvetica'; text-align:center">
              <h3 align="center" style="color:#1A1A1A ; line-height: 10px; padding-top: 20px;">Articulos</h3>
              <table style="border-collapse: collapse; margin:0 auto; font-size :120%">
                  <thead>
                    <th style="display: table-cell; font-weight:bold; text-align: center; padding: 10px;border: 1px solid #F6F2F2; color: #21271F;">Codigo</th>
                    <th style="display: table-cell; font-weight:bold; text-align: center; padding: 10px;border: 1px solid #F6F2F2; color: #21271F;">Descripcion</th>
                    <th style="display: table-cell; font-weight:bold; text-align: center; padding: 10px;border: 1px solid #F6F2F2; color: #21271F;">Color</th>
                    <th style="display: table-cell; font-weight:bold; text-align: center; padding: 10px;border: 1px solid #F6F2F2; color: #21271F;">Categoria</th>
                    <th style="display: table-cell; font-weight:bold; text-align: center; padding: 10px;border: 1px solid #F6F2F2; color: #21271F;">Subcategoria</th>
                    <th style="display: table-cell; font-weight:bold; text-align: center; padding: 10px;border: 1px solid #F6F2F2; color: #21271F;">Demografia</th>
                    <th style="display: table-cell; font-weight:bold; text-align: center; padding: 10px;border: 1px solid #F6F2F2; color: #21271F;">Talla</th>
                    <th style="display: table-cell; font-weight:bold; text-align: center; padding: 10px;border: 1px solid #F6F2F2; color: #21271F;">Cantidad</th>
                  </thead>
                  <tbody>
                    @foreach ($articulos as $item)
                      <tr>
                        <td style="padding: 10px; text-align: center;border: 1px solid #F6F2F2;">{{$item->articulo->codart}}</td>
                        <td style="padding: 10px; text-align: center;border: 1px solid #F6F2F2;">{{$item->articulo->desart}}</td>
                        <td style="padding: 10px; text-align: center;border: 1px solid #F6F2F2;">{{$item->color}}</td>
                        <td style="padding: 10px; text-align: center;border: 1px solid #F6F2F2;">{{$item->categoria->nombre}}</td>
                        <td style="padding: 10px; text-align: center;border: 1px solid #F6F2F2;">{{$item->subCategoria}}</td>
                        <td style="padding: 10px; text-align: center;border: 1px solid #F6F2F2;">{{$item->demografia }}</td>
                        <td style="padding: 10px; text-align: center;border: 1px solid #F6F2F2;">{{$item->talla->tallas ?? 'N/A'}}</td>
                        <td style="padding: 10px; text-align: center;border: 1px solid #F6F2F2;">{{$item->cantmov}}</td>
                        {{-- almacenfrom->nomalm
                        almacento->nomalm
                        codubifrom->nomubi
                        codubito->nomubi --}}
                      </tr>
                    @endforeach
                  </tbody>
              </table>
            </div>
            <div aling="center" style="margin-top:10px; padding-top:12px;padding-right:10px;padding-bottom:12px;padding-left:10px;">
              <p style="font-size: 18px; line-height: 20px; word-break: break-word; text-align: center; margin: 0;font-family: 'Poppins', Arial, Helvetica, sans-serif;"><b>
                
                Para Aprobar o Rechazar esta solicitud <a style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #f7fff7; 
                border-radius: .25rem; -webkit-border-radius: .25rem; -moz-border-radius: .25rem; width: auto; width: auto;
                background-color: #1a99b5;
                border: 1px solid #1eb3d2;
                padding: .375rem .75rem;
                font-family: 'Poppins', Arial, Helvetica, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;" href="{{route('emailChoise',[$movimiento->codtra ?? $movimiento->codmov,$tipo])}}">Click Aqui</a>
                </b>
              </p>
              {{-- <a href="{{route('aprobacionEmail',[Helper::crypt($movimiento->id),$tipo])}}" target="_blank" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #f7fff7; 
              border-radius: .25rem; -webkit-border-radius: .25rem; -moz-border-radius: .25rem; width: auto; width: auto;background-color: #28a745;
              border: 1px solid #28a745;
              padding: .375rem .75rem;
              font-family: 'Poppins', Arial, Helvetica, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;"><strong>Aprobar</strong></a> --}}
              {{-- <a href="{{route('anulacionEmail',[Helper::crypt($movimiento->id),$tipo])}}" target="_blank" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #f7fff7; 
              border-radius: .25rem; -webkit-border-radius: .25rem; -moz-border-radius: .25rem; width: auto; width: auto;
              border: 1px solid #bd2130;
              background-color: #c82333;
              padding: .375rem .75rem;
              font-family: 'Poppins', Arial, Helvetica, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;"><strong>Rechazar</strong></a> --}}
            </div>
            
          </div>
          <p style="font-size: 14px; line-height: 1.5; word-break: break-word; text-align: center; margin: 0;">
            <em>
              Por favor, NO responda a este mensaje, es un envío automático.
            </em>
          </p>
        </header>
      </div>
    </body>
</html>
