
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html >
  <head>
  <!-- If you delete this tag, the sky will fall on your head -->
    <meta name="viewport" content="width=device-width" />
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>MAIL VIT</title>
      
  </head>
  <style>

  </style>
    <body style="margin : 0; padding: 0;">
      <div style="
        width: 100%;
        height : 100px;
        display:flex;
        align-items:center;
        align-content:center;
        justify-content: center;
        background:#182222;
      ">
        <table>
          <td>
            <img src="{{ asset('/images/arian/principal/arian-logo-blanco.png') }}" height="70px" class="logo-arian" style="margin-left: 30px;">
          </td>
        </table>
      </div>
      <div style=" margin : 0 auto; max-width : 1000px ; text-align:center; background:#eaefff; border-radius: .45em; padding : 15px">
        <header>
        El trabajador <b>{{ $nomemp }}</b> de Cédula de identidad {{ $codemp }} ha registrado una solicitud para ingresar familiares al seguro
        <div aling="center" style="margin-top:10px; padding-top:12px;padding-right:10px;padding-bottom:12px;padding-left:10px;">
          <a style="
            border-radius:10px;
            padding : 0.5rem;
            background : #182222;
            color : #fff;
            font-weight : 600;
            text-decoration: none;
            border: none;
            font-family:monospace
            "
            href="{{ route('fasvit.edit', $id) }}"
          >
            Ver solicitud
          </a>  
        </div>
        
        </div>
          <p style="font-size: 14px; line-height: 1.5; word-break: break-word; text-align: center; margin: 0;">
            <em>
              Por favor, NO responda a este mensaje, es un envío automático.
            </em>
          </p>
        </header>
      </div>
    </body>
</html>
