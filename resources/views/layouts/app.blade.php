<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">﻿
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link href="/css/select2.min.css" rel="stylesheet">
  <title>ARIAN</title>
  
  <link rel="icon" sizes="114x114" href="{!! asset('images/arian/principal/ARIAN_negro_negro.png') !!}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('js/DataTables/datatables.min.css') }}" />
  <link rel="stylesheet" href="{{asset('cssadminlte/adminlte.min.css')}}">
  {{-- <link rel="dns-prefetch" href="//fonts.gstatic.com">--}}
  <link href="{{ asset('css/fontfamily.css') }}" rel="stylesheet">
  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  {{-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> --}}
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('css/ionicons2.0.1.min.css') }}">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ asset('plugins/jqvmap/jqvmap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('cssadminlte/adminlte.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.css') }}">

  <!-- Google Font: Source Sans Pro -->
  {{-- <link href="{{ asset('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700') }}" rel="stylesheet">
  --}}
  {{-- <link rel="stylesheet" href="{{ asset('adminlte/css/adminlte.min.css') }} ">

  --}}
  <style>
  .cargando {
  width: 100%;height: 100%;
  overflow: hidden; 
  top: 0px;
  left: 0px;
  z-index: 10000;
  text-align: center;
  position:absolute; 
  background-color: /*#FFFFFF*/ #FFF;
  opacity:0.9;
  filter:alpha(opacity=40);

  
}
.imagen{
  margin-top: 500px
  }
  </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed">

  <div id="app">


    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->

      <div class="container-fluid" style="margin-left: 14px;">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
          </li>
          <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> -->
        </ul>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Left Side Of Navbar -->
          <ul class="navbar-nav mr-auto">

          </ul>

          <!-- Right Side Of Navbar -->
          <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            @guest
            <li class="nav-item">
              <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @else
            <li class="nav-item dropdown" style="margin-right:-5px;">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->nomuse}}<span class="caret"></span>
              </a>

              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                  {{ __('Cerrar Session') }}<br>

                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              </div>
            </li>
            @endguest
          </ul>
        </div>
      </div>

    </nav>
    <!-- /.navbar -->
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background-color: black">
      <!-- Brand Logo -->
      <a href="{{ route('home') }}" class="brand-link">
        <!-- <img src="{{ asset('img/A1WIN2.jpg') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8;margin-left: 2px">
      <span class="brand-text font-weight-light">FACTURACION</span> -->
        <img class="logo-arian" src="{{ asset('/images/arian/principal/arian-logo-blanco.png') }}" height="70px" style="margin-left: 30px">
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="{{ asset('img/logito.jpeg') }}" class="img-circle elevation-2" alt="User Image">
          </div>
            <div class="info">
              <a href="#" class="d-block" style="color: white; font-size:75%"><b>{{ Auth::user()->nomuse}}</b></a>
              <a class="d-block" style="color: #fff;  font-size:75%">Sucursal :{{ session('sucu') }}</a>
              <a class="d-block" style="color: #fff;  font-size:75%">Año Fiscal: {{ session('fiscal') }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
              with font-awesome or any other icon font library -->
            @yield('menu')
          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>
    {{-- MAIN CONTENT tu modulo va aqui --}}
    <div class="content-wrapper">
      @yield('content')

    </div>
    {{-- /MAIN CONTENT --}}
    <footer style="text-align: center; background-color: #201d1c" class="main-footer">
      <!-- To the right -->
      <!-- Default to the left -->
      <strong id="year"></strong> All rights reserved.
    </footer>
  </div>
  <!-- jQuery -->
  <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
  <!-- jQuery -->
  <!-- ChartJS -->
  <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
  <!-- Sparkline -->
  <script src="{{ asset('plugins/sparklines/sparkline.js') }}"></script>
  <!-- JQVMap -->
  <script src="{{ asset('plugins/jqvmap/jquery.vmap.min.js') }}"></script>
  <script src="{{ asset('plugins/jqvmap/maps/jquery.vmap.world.js') }}"></script>
  <!-- jQuery Knob Chart -->
  <script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }}"></script>
  <!-- daterangepicker -->
  <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>

  <!-- Tempusdominus Bootstrap 4 -->
  <script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
  <!-- Summernote -->
  <script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>
  <!-- overlayScrollbars -->
  <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('jsadminlte/adminlte.js') }}"></script>
  <script src="{{asset('js/entrada.js')}} "></script>

  <script src="{{ asset('sweetalert2/dist/sweetalert2.all.js') }}"></script>
  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{ asset('js/select2.full.min.js') }}"></script>
  <script src="{{ asset('js/jquery.mask.js') }}"></script>
  <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/DataTables/datatables.min.js') }}"></script>
  <script>
    $(document).ready(function() {
      let secondSon = $('li.nav-item.menu-open');
      let son = $('a.nav-link.active');
      if ((secondSon.length === 1 && son.length === 0) || (secondSon.length && son.length)) return;
      let parent = $(son).parent().parent().parent();
      let papaUpa = $(parent).parent();
      let papaUpa1 = $(papaUpa).parent();
      $(parent).addClass('menu-open');
      $(papaUpa1).addClass('menu-open');
      $(papaUpa1).children('a.nav-link').eq(0).addClass('active');
    })
    var currentYear = new Date();
    document.getElementById('year').innerHTML = `Copyright &copy; ${currentYear.getFullYear()} <a>VIT Venezolana de Industria Tecnologica</a>.`
  </script>
  @yield('script')
  @yield('lista')
  @yield('eliminarArticulos')
  @yield('cargar')
  @yield('mayuscula')
</body>
</html>
