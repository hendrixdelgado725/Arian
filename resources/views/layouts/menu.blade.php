
@section('content')
<div class="container overflow-auto">
    <div class="row justify-content-center">
        <div class="col-md-8">
          @if(Session::has('flash_message'))
            <div class="card">
                  <div class="card-body">
                    {{-- @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif --}}
                  <h1>
                <div class="alert alert-light" role="alert">
                  {{Session::get('flash_message')}}
                </div>
                </h1>
              </div>
            </div>
          @endif
        </div>
    </div>
</div> 
@endsection

@section('menu')
@if (is_array($menus['submodulo']))
    @foreach ($menus['submodulo'] as $element => $value)
            @php
                $permisos = \App\Http\Controllers\permisos\PermisologiaController::modulos($element,Auth::user()->loguse); 
            @endphp
           @if ($permisos)
              @php
                $iconos = \App\Http\Controllers\HomeController::AsocIcon($element);
                $uri = Route::getFacadeRoot()->current()->uri();
                $ruta = substr($uri,7,strlen($uri));
                foreach ($value as $key => $valol) {
                  if(is_array($valol)){
                    foreach ($valol as $KEY) {
                      if(substr($ruta,0,strlen($KEY)) === $KEY){
                        $rutaEdit = substr($ruta,0,strlen($KEY));
                      }
                      if(isset($rutaEdit)) break;
                    }
                  }else{
                    if(in_array(substr($ruta,0,strlen($valol)),$value)){
                        $rutaEdit = substr($ruta,0,strlen($valol));
                    }
                  }
                  if(isset($rutaEdit)) break;
                }
                $MainRuta = isset($rutaEdit) ? $rutaEdit : $ruta;
                $KeyActive = array_search($MainRuta,$value);
                $menu = array_search($MainRuta,$value) ? 'menu-open' : '';
                $active = $menu === 'menu-open' ? 'active' : '';
              @endphp
            <li  class="nav-item has-treeview {{$menu}}">
              <a href="#" class="nav-link icono-menu {{$active}}">

              {!! htmlspecialchars_decode($iconos) !!}
              <p>
                {{ $element }}
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
              @if (is_array($menus['submodulo'][$element]))
              @foreach ($menus['submodulo'][$element] as $element2 => $value2)
                @php
                  $permisos2 = \App\Http\Controllers\permisos\PermisologiaController::submodulos($element,$element2,Auth::user()->loguse);
                  $ruta1 = Route::getFacadeRoot()->current()->uri();
                  $KeyActive = is_array($value2) ? array_search($MainRuta,$value2) : '';
                @endphp
                @if ($permisos2)
                <ul class="nav nav-treeview" style="background-color: #2c2830">
                  @php 
                  @endphp
                  <li class="nav-item">
                    @php
//                     $iconos2 = \App\Http\Controllers\HomeController::AsocIcon2($element,$element2);
                      $iconos3 = \App\Http\Controllers\HomeController::blank($element2);
                    @endphp
 
                  <a  href="{{ route('registro',['id'=>print_r($value2,true)]) }} " {{ $iconos3 === '_blank' ? 'target=$iconos3' : ''}} class="nav-link {{$MainRuta === $value2 ? 'active' : ''}}">
                    {{-- <!--   {!! htmlspecialchars_decode($iconos2) !!} --> --}}
                      <p>{{ $element2 }}</p>

                      @if (is_array($menus['submodulo'][$element][$element2]))
                        
                        <i class="fas fa-angle-left right"></i>
                      @endif
                    </a>
                    @if (is_array($menus['submodulo'][$element][$element2]))
                      @foreach ($menus['submodulo'][$element][$element2] as $element3 => $value3)
                        @php
                          $permisos3 = \App\Http\Controllers\permisos\PermisologiaController::hijosSubmodulos($element,$element2,$element3,Auth::user()->loguse);
                          $ruta2 = Route::getFacadeRoot()->current()->uri();

                          
                        @endphp
                        @if ($permisos3)
                        <ul class="nav nav-treeview">
                          <li class="nav-item">
                            <!-- <a href="{{ $beta = route('registro',['id'=>print_r($value3,true)]) }}" class="nav-link {{Helper::getActiveLink($ruta2,$value3)}}" > -->
                          <!--     <i class="nav-icon fas fa-copy"></i> -->


                         <!--    @php
                                $iconos3 = \App\Http\Controllers\HomeController::AsocIcon3($element,$element2,$element3);
                              @endphp -->
                      
                            <a href="{{ $beta = route('registro',['id'=>print_r($value3,true)]) }}" class="nav-link  {{$element3 === $KeyActive ? 'active' : ''}}" >
                        <!--       {!! htmlspecialchars_decode($iconos3) !!} -->
                              <p>{{ $element3 }}</p>
                              {{-- <i class="fas fa-angle-left right"></i> --}}
                            </a>
                          </li>
                        </ul>     
                        @endif
                      @endforeach
                    @endif
                  </li>
                </ul>  
                @endif   
              @endforeach
            @endif
          </li>
        @endif
      @endforeach
    @endif
@endsection
<script>
</script>