<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">﻿
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Registro de familiares en el seguro de venezolana de industria tecnológica VIT"/>
    <title>ARIAN</title>
    <link rel="icon" type="image/png" href="{!! asset('images/arian/principal/ARIAN_negro_negro.png') !!}"/>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/fontfamily.css') }}" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ionicons2.0.1.min.css') }}">
    <link rel="stylesheet" href="{{ asset('cssadminlte/adminlte.min.css') }}">
    

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="sidebar-mini layout-fixed">
  <div id="app" class="wrapper">
  <nav>
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            </ul>
        </div>
    </div>
  </nav>

  <div class="content-wrapper">
    @yield('content')
  </div>
    <footer class="main-footer fasvit">
        <strong >Copyright &copy; 2021 <a>VIT Venezolana de Industria Tecnologica</a>.</strong> All rights reserved.
    </footer>
  </div>
</body>
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('jsadminlte/adminlte.js') }}"></script>
@yield('scripts')
</html>
