@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Listado Auditoria')


@section('content')

@component('layouts.contenth')
    @slot('titulo')
      Auditoria
    @endslot
  @endcomponent
    <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">

            <div class="card">


              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">

              <div class="col">
                  <div class="row">
                    <div class="col mt-2">
                      
                          
                        <form action="{{ route('filtroAuditoria.lista')}}" method="get">
                            <div class="input-group">
                          @csrf
                          <input type="text" placeholder="Ej: created, fafactur" name="filtro"  class="form-control float-right">
                          <button type="submit" style="height: 38px; width: 38px" class="btn btn-default btn-sm float-left"><i class="fas fa-search"></i></button>
                          <a href="{{ route('Auditoria.lista') }}" class="btn btn-default"><i class="fas fa-arrow-left"></i></a>  
                           </div>
                        </form>
                      
                    </div>
                  </div>
                 </div>
                
                 </div><!--row -->
                </div>
              </div>

              <!-- /.card-header -->
              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>IP</th>
                      <th>Tabla</th>
                      <th>Usuario</th>
                      <th>Descripci&oacute;n</th>
                      <th>Fecha</th>
                      <th>Visualizar</th>

                    </tr>
                  </thead>
                  <tbody style="text-align:center">
                 @foreach($auditor as $auditar)
                    @php
                    $rand  = rand(1, 9999);
                    $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                    $enc=Helper::EncriptarDatos($nrand.'-'.$auditar->id);

                  @endphp
                     <tr style="text-align: center;">
                      
                      <td>{{$auditar->ip_address}}</td>
                      <td>{{$auditar->auditable_type}}</td>
                      <td>{{$auditar->user->nomuse}}</td>
                      <td>{{$auditar->event}}</td>
                      <td>{{ date('Y-m-d', strtotime($auditar->created_at)) }}</td>
                       <td>
                        <a  class="view" href="#" data-url="{{ route('Auditoria.actividad',$enc) }}">
                          <i class="fa fa-book red"></i>
                         </a>
                       </td>
                    </tr>
                  @endforeach
                  </tbody>
                  
                </table>
                {!!$auditor->appends(Request::only('filtro'))->render()!!}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}

                   
  {{-- container fluid --}}
 <div class="modal fade" id="modalAuditoria" tabindex="10" role="dialog" aria-labelledby="modalempleado" aria-hidden="true" >
           <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content"> 
            <div class="modal-header label">
             
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
            </div>
             <div class="modal-body content-centered">
                
                 
               <div class="col-sm-12">
                <div class="table-wrapper-scroll-y my-custom-scrollbar texto" >
                
                </div>

                <label for="">Valores Creados:</label><br> 
                <div class="caja">
                
                </div>

                <label for="">Valores Actualizado:</label><br> 
                <div class="caja2">
                
                </div>

              </div>
              </div>           
           <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-borrar" data-dismiss="modal"><b> Cerrar</b></button>
           </div>
         </div>
       </div>
     </div>
  </div>
@endsection
@section('lista')
  
<script>
    
  $(document).ready(function() {
      let elemento1 = null;
      let elementos2 = null;
      $('.view').click(function (e) {
            e.preventDefault();
            let temp = [];
            let url = $(this).data('url'); 
            
            let datos = [];
            let datos2 = [];
            $.get(url,function(resultado) {
             
             $('.modal-header').html('<label for="cargo">Actividades del Usuario '+resultado.nombreUser);///quede aqui
             /*+
                                                                 */
              $('div.texto').html('<label for="cargo">Origen: '+resultado.Ip+
                                  '<br><br><label>Tabla: '+resultado.Tabla);
              console.log(resultado.old_values);
              elemento1 = $('div.caja');
              //console.log(resultado.new_values);
              new_values = JSON.parse(resultado.new_values);

             if(new_values !== null){
              for(var i in Object.values(new_values)){
                
                datos.push(Object.keys(new_values)[i]+': '+Object.values(new_values)[i]+'<br>');
                
              }
             }else{
              elementos2 = $('div.caja2');
              old_values = JSON.parse(resultado.old_values);
              
             
              console.log(typeof datos2)
              
              for (var i in Object.values(old_values)) {
                
                 datos2.push(Object.keys(old_values)[i]+': '+Object.values(old_values)[i]+'<br>');
                  
                 
              }
             }
             
              
               elemento1.html('<label>'+datos);
               recorrer = 0;

             
              
              elementos2.html('<label>'+datos2);
              
              datos2 = null;
              datos  = null;
              
            });


            $('#modalAuditoria').modal('show');  
      });
  });

</script>
@endsection
