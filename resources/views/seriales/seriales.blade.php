@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Consultar Seriales')

@section('content')
@component('layouts.contenth')
    @slot('titulo')
        Consultar Seriales
    @endslot
  @endcomponent
<div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">

            <div class="card">

 @include('vendor/flash.flash_message')
       



              <listado-seriales></listado-seriales>

             

          </div>
        </div>
    </div>

    <div class="form-group">
          <div class="modal fade" id="modalempleados" tabindex="10"  
           role="dialog" aria-labelledby="modalempleado"
           aria-hidden="true" >
           <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header label" id="header">

            </div>
             <div class="modal-body content-centered">

               <div class="col-sm-12">
              <label for="cargo"></label>
                <div class="input-group lis" id="lis">
                 
                </div>
              </div>
              </div>
           <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-borrar" 
            data-dismiss="modal" ><b> Cerrar</b></button>
           </div>
         </div>
       </div>
     </div>

    </div>

      {{-- row --}}
  </div>
  @section('lista')
  <script>
      
       async function visualizar(event) {
            let url = $(event).data('href');    
            let serial = await fetch(url);


            if(serial.ok){
             let res = await serial.json();
             var fact = $(event).data('fac');

            
             serialNull = (res.serial !== null) ? res.serial : 'NO TIENE SERIAL';
             serialFacturado = (res.facturar_serial !== false) ? '<i class="fa fa-check">' : '<i class="fa fa-ban">';
             $('#header').html(`<h2>Factura: <a target="_blank" href="/modulo/Facturacion/DetalleSerial/${fact}"   >${res.factura}</a></h2> <h2>Serial: ${serialNull}</h2>`);
             $('#lis').html(
                            '<h6>Descripcion articulo: '+res.get_articulo.desart+'</h6></br>'+
                            '<h6>Serial Facturado: '+serialFacturado+'</h6><br>'
                            )

            }
            $('#modalempleados').modal('show')
            
        }
  </script>
  @endsection
  @endsection
  
