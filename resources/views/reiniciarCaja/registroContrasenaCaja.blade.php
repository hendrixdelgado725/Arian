@extends('layouts.app')
@extends('layouts.menu')
  
  @section('content')
    @component('layouts.contenth')
    @slot('titulo')
      Contraseña de caja
    @endslot
  @endcomponent
    
    <section class="content-wrapper" >
    <div class="container-fluid">
    <div class="container">
        
        <div class="col-10">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title mt-2"><b>CREAR CONTRASEÑA A LA CAJA</b></h3>
                <div class="d-flex flex-row-reverse bd-highlight">
                <!-- <div>
                   <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fas fa-chevron-circle-left"></i><b> Atras</b></a>
                  </div> -->
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form action="{{ route('registroCaja.lista') }}" method="post">
              {{csrf_field()}}
                <div class="card-body">
                 <div class="row">
                 <div class="col-sm-4">
                 <div class="form-group">
                  <label for="tipodocumento">Elige al Usuario</label>
                   <select name="user" class="form-control select2" data-url="{{ route('searchCaja.lista') }}">
                      <option value="0" disabled selected>Elige Un Usuario</option>
                      @foreach ($user as $element)
                        <option value="{{$element->loguse ?? ''}}-{{ $element->getSucursal->codsuc ?? '' }}">{{$element->loguse ?? ''}}-{{ $element->getSucursal->nomsucu ?? ''}}</option>
                      @endforeach
                   </select>
                    @error('user')
                     <div class="alert alert-danger">{!!$message!!}</div>
                   @enderror
                </div>     
                 </div>
              
               <div class="col-sm-8">
                <div class="form-group">
                <label>Numero de documento</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">V/J</span>
                  </div>
                  <input type="text" class="form-control ced"  placeholder="Ejem: 1234567" name="cedularif" 
                  id="cedularifcliente"  class="@error('cedularif') is-invalid @enderror">
                </div>
                @error('cedularif')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
               </div>

               
               </div>

             

                <div class="form group">
                <label for="nombre">Sucursal</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Aa</span>
                  </div>
                  <input type="text" class="form-control sucu" class="@error('nombre') is-invalid @enderror"
                   placeholder="Ejem: Sucursal" name="nombre">
                </div>
                @error('nombre')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                <div class="form group">
                    <label for="direccion">Caja a Asociar</label>
                <div class="input-group mb-3">
                  <select name="caja" class="form-control select2">
                    <option value="0">Seleccione la Caja</option>
                     @foreach($caja as $elemen)
                      <option value="{{$elemen->id}}">{{ $elemen->descaj}}</option>
                     @endforeach
                  </select>
                </div>
                @error('caja')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                 <div class="form group">
                  <label for="direccion">Ingresar contraseña de caja</label>
                <div class="input-group mb-3">
                  <input type="password" class="form-control contra" class="@error('nombre') is-invalid @enderror"
                   placeholder="Ejem: password" name="contra">
                  <label>la contraseña tiene que tener un maximo 8 caracteres y minimo de 16,debe contener por lo menos una letra
                         mayuscula y minuscula,debe contener numero y carecter especial</label>
                </div>
                @error('contra')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                  <div class="form-check">
                    <input type="checkbox" class="form-check-input check" >
                    <label class="form-check-label" for="exampleCheck1">Mostrar Contraseña</label>
                  </div>

                <div class="form group">
                  <label for="direccion">Confirmar contraseña de caja</label>
                <div class="input-group mb-3">
                  <input type="password" class="form-control sucu" class="@error('nombre') is-invalid @enderror"
                   placeholder="Ejem: confirmar password" name="Confircontra">
                </div>
                @error('Confircontra')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                <!-- /.card-body -->
               <div class="row">
                <button type="submit" class="btn btn-success mt-4 ml-2"><b>Registrar Caja</b></button>
                <div>
                  <a href="{{ URL::previous() }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                </div>
               </div>
                <!-- <div class="card-footer">
                </div> -->
              </form>
            </div>
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->

            
         </form>
        </div>
            <!-- /.card -->
       </div>
      </div>
      </div>
      </section>
  @endsection


  @section('script')
  <script>

    $(document).ready(function() {
      
              $('select.select2').select2();

              $('select.select2').on('change',function(e) {
                  e.preventDefault();
                  let userCaja = $(this).val();
                  let url = $(this).data('url')+'/'+userCaja;

                  $.get(url,function(result) {
                    $('input.ced').val(result.cedula);
                    $('input.sucu').val(result.sucursal);
                  });
                   
              });

              $('input.check').click(function(e) {
                  
                  if ($('input.check').is(':checked')) {

                    $('input.contra').attr('type', 'text');
                  }else{
                    $('input.contra').attr('type', 'password');
                  } 
 
              });

    });

  </script>
    @endsection
    