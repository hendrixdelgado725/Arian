@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Reiniciar Contraseña')

@section('content')

@component('layouts.contenth')
    @slot('titulo')
      Crear y Reiniciar Contraseña para la caja Asignada
    @endslot
@endcomponent
    <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
              @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">

              <div class="col">
                  <div class="row">
                    <div class="col-xs-12">
                      
                        <form method="post" action="{{ route('filtro') }}">
                            <div class="input-group">
                             @csrf
                          <div class="card-tools mr-2">
                             @can('edit', App\permission_user::class)
                            <a href="{{ route('regCaja.lista') }}" class="btn btn-info"><b>REGISTRAR CLAVE DE CAJA A USUARIO</b></a>
                            @endcan
                          </div>
                          <input type="text" placeholder="Buscar por nombre de Usuario" name="filtro" class="form-control float-right">
                          
                          <button type="submit" style="height: 38px; width: 38px" class="btn btn-default btn-sm float-left"><i class="fas fa-search"></i></button>
                           
                           </div>
                        </form>
                     
                    </div>
                  </div>
                 </div>
                
                 </div><!--row -->
                </div>
              </div>

              <!-- /.card-header -->
              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>ID</th>
                      <th>Usuario</th>
                      <th>Caja Asignada</th>
                      <th>Sucursal</th>
                      <th>Fecha</th>
                      <th>cedula</th>
                      <th>Accion</th>

                    </tr>
                  </thead>
                  <tbody style="text-align:center">

                    @foreach ($cajaUser as $element)
                    @php
                      $rand  = rand(1, 9999);
                      $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                      $enc=Helper::EncriptarDatos($nrand.'-'.$element->codigoid);
                      //$borr=Helper::EncriptarDatos($nrand.'|'.$tal->id.'|'.$tal->codigoid);
                    @endphp
                     <tr style="text-align: center;">
                      <td>{{$element->id}}</td>
                      <td>{{$element->loguse}}</td>
                      <td>{{$element->caja->descaj ?? ''}}</td>
                      <td>{{$element->codsucu}}</td>
                      <td>{{$element->created_at}}</td>
                      <td>{{$element->cedula}}</td>
                      <td>
                         @can('edit', App\permission_user::class)
                        <a href="{{ route('updateCaja.lista',$enc) }}" class="edit">
                          <i class="fa fa-edit blue"></i>
                        </a>
                        /
                        @endcan
                         @can('delete', App\permission_user::class)
                        <a class="borrar"  href="{{ route('deleteCaja.lista',$element->id) }}">
                          <i class="fa fa-trash blue"></i>
                        </a>
                         @endcan
                      </td>
                        
                    @endforeach

                  </tbody>
                </table>
                  {{ $cajaUser->appends(Request::only('filtro'))->render() }}

          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
{{--   --}}
                   
  {{-- container fluid --}}
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form method="post" class="contenido" data-url="{{ route('updatesCaja.lista') }}">
                  {{csrf_field()}}
                  <div class="row">
                <div class="col-sm-12">
                <div class="table-wrapper-scroll-y my-custom-scrollbar texto" >
                 <div class="form-group">
                  <label for="direccion">Usuarios:</label><br>
                  <input type="text" disabled class="form-control user" class="@error('nombre') is-invalid @enderror"
                   placeholder="Ejem: password" name="contra">
                </div>
                <div class="form-group">
                  <label  for="direccion">Contraseña Nueva:</label><br>
                  <input type="password" class="form-control pass" 
                   placeholder="Ejem: password" name="contra" >
                   <span id="exampleInputEmail1-error"  
                    class="error invalid-feedback"></span>
                   <label>la contraseña tiene que tener un maximo 8 caracteres y minimo de 16,debe contener por lo menos una letra
                         mayuscula y minuscula,debe contener numero y carecter especial</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input check checked" >
                    <label class="form-check-label" for="exampleCheck1">Mostrar Contraseña</label>
                </div>


                <div class="form-group">
                  <label  for="direccion">Confirmar <br>  Contraseña:</label><br>
                  <input type="password" class="form-control Confpass" 
                   placeholder="Ejem: password" name="contra">
                   <span id="exampleInputEmail1-error2"  
                    class="error invalid-feedback"></span>
                </div>

                <div style="display: none;"e class="input-group mb-3">
                  <input type="text" value="{!! csrf_token() !!}" class="form-control Confpass" class="@error('nombre') is-invalid @enderror"
                   placeholder="Ejem: password" name="contra">
                </div>

                <div class="form-group cajass">
                  <label for="caja">Caja a seleccionar</label>
                  <div class="col-sm-12">
                    <select id="caja" name="caja" style="width:100% !important;" class="form-control select2 caja" data-url="{{ route('searchCaja.lista') }}">
                    </select>
                  </div>
                </div>
                </div>
               </div>                
                  </div>               
                </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-cerrar">Actualizar</button> <button type="button" onclick="cerraModal()" class="btn btn-danger">Cerrar</button>

      </div>
    </div>
  </div>
</div>
  </div>
@endsection


@section('script')
<script>
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

  function cerraModal() {
    $('#exampleModal').modal('hide');
    $('.pass').removeClass('is-invalid');
    $('.Confpass').removeClass('is-invalid');
    $('#exampleInputEmail1-error').text('');   
    $('#exampleInputEmail1-error2').text('');
    $('input.user').val('');
    $('input.pass').val('');
    $('select.caja').val();
    $('input.Confpass').val('');
  }

  $(document).ready(function() {
      $('select.select2').select2();


          $('input.check').click(function(e) {
                  
                  if ($('input.check').is(':checked')) {

                    $('input.pass').attr('type', 'text');
          
                  }else{
                    $('input.pass').attr('type', 'password');
                  } 
 
              });

          $('input.user').val();
          $('input.pass').val();

      $('a.edit').on('click',function(e) {
      
            $('#exampleModal').modal({ backdrop: "static", keyboard: false });
            e.preventDefault();
            let url = $(this).attr('href');
           
            $.get(url,function(resultados) {
                //console.log(resultados.loguse);
                $('input.user').val(resultados.loguse);
               
                $('div.userr').html('<h3>Editar a Usuario '+resultados.nombre+'</h3>')
                $('div.cajass select').html(resultados.codcaja);
                $('div.modal-header').html('<h5 class="modal-title" id="exampleModalLabel">Usuarios '+resultados.nombre+'</h5>')
                let url1 = $('form.contenido').data('url')+'/'+resultados.codigoid;
                
                $('button.btn-cerrar').on('click',function(e) {
                  e.preventDefault();
                  /*
                    remueve las validaciones al momento de hacer peticion ajax
                    */
                  $('.pass').removeClass('is-invalid');
                  $('.Confpass').removeClass('is-invalid');
                  $('#exampleInputEmail1-error').text('');   
                  $('#exampleInputEmail1-error2').text('');                
              $.ajax({

                    type:'POST',
                    data:{
                      id2:$('input.user').val(),
                      id3:$('input.pass').val(),
                      id4:$('select.caja').val(),
                      id5:$('input.Confpass').val(),
                      
                    },
                    url:url1,
                    success:function(result){
                      //console.log(result.titulo);

                          //En caso que no se actualize
                         window.location.href = '/modulo/Seguridad/ReiniciarContraCaja'
                      
                      $('input.user').val(" ");
                      $('input.pass').val(" ");
                      $('select.caja').attr('selected');//OJO ARREGLAR DETALLES DEL SELECT
                      $('input.Confpass').val(" ");

                
                    }

                  }).catch(e => {
                    
                    if(e.status === 422){
                        /*error del cliente cuando no cumple*/
                        
                        if(e.responseJSON.errors.id3[0] !== undefined){
                          switch(e.responseJSON.errors.id3[0]){
                            case 'El campo <b>id3</b> es requerido.':
                            $('.pass').addClass('is-invalid');
                            $('#exampleInputEmail1-error').text("El campo es requerido.");     
                            break;

                            case 'The id3 must be at least 8 characters.':
                            $('.pass').addClass('is-invalid');
                            $('#exampleInputEmail1-error').text("La Contraseña debe contener minimo 8 hasta 16 caracteres");     
                            break;

                          }
                          
                        }
                        if(e.responseJSON.errors.id5[0] !== undefined){
                          switch(e.responseJSON.errors.id5[0]){
                            case 'El campo <b>id5</b> es requerido.':
                              $('.Confpass').addClass('is-invalid');
                              $('#exampleInputEmail1-error2').text("El campo es requerido.");  
                            break;

                            case 'The id5 must be at least 8 characters.':
                              $('.Confpass').addClass('is-invalid');
                              $('#exampleInputEmail1-error2').text("La Contraseña debe contener minimo 8 hasta 16 caracteres.");  
                            break;
                          }
                          
                        }

                      
                    }
                  });
                 

                });
            });


            $('input.checked').click(function(e) {
              
              if ($('input.check').is(':checked')) {

                $('input.contra').attr('type', 'text');
            
              }else{
            
                $('input.contra').attr('type', 'password');
            
              } 

          });

      });

      
      $('a.borrar').on('click',function(e) {
          e.preventDefault();
       let url2 = $(this).data('pes');

           Swal.fire({
            title: 'Seguro que Desea Eliminar ',
            text: "Despues de esto no se encontrara en la lista!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {

          if (result.value) {
           
              
              var row = $(this).parents('tr');
              //var form = $(this).parents('data-id');
              let url = $(this).attr('href');
              /*console.log(form.serialize());*/
              $.get(url,row,function(result1){

                if (result1.titulo === 'SE HA ELIMINADO CON EXITO') {

                    Swal.fire(
                    'Ha sido Eliminado!',
                    'Con Exito.',
                    'success',

                    );

                row.fadeOut();
                }

              
              });
             
             }else{
              Swal.fire({
              icon: 'warning',
              title: 'No ha sido Eliminado',
              timer:1500
              });
             }

        });
      });

  });

</script>
@endsection

