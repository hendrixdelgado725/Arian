@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Listado Auditoria')


@section('content')

@component('layouts.contenth')
    @slot('titulo')
      Lista de Permisos de Los Usuarios y Sus Cajas
    @endslot
  @endcomponent
    <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">

            <div class="card">


              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">

              <div class="col">
                  <div class="row">
                    <div class="col-xs-12">
                      
                          
                        <form action="#" method="post">
                            <div class="input-group">
                          @csrf
                          <input type="text" placeholder="Buscar por nombre de Almacen" name="filtro"  class="form-control float-right">
                          <button type="submit" style="height: 38px; width: 38px" class="btn btn-default btn-sm float-left"><i class="fas fa-search"></i></button>
                          
                           </div>
                        </form>
                      
                    </div>
                  </div>
                 </div>
                
                 </div><!--row -->
                </div>
              </div>

              <!-- /.card-header -->
              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>Ip</th>
                      <th>Tabla</th>
                      <th>Usuario</th>
                      <th>Descripcion</th>
                      <th>Fecha</th>
                      <th>Visualizar</th>

                    </tr>
                  </thead>
                  <tbody style="text-align:center">
                     <tr style="text-align: center;">
                      
                  </tbody>
                  
                </table>
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}

                   
  {{-- container fluid --}}
 <div class="modal fade" id="modalAuditoria" tabindex="10" role="dialog" aria-labelledby="modalempleado" aria-hidden="true" >
           <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content"> 
            <div class="modal-header label">
             
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
            </div>
             <div class="modal-body content-centered">
                
                 
               <div class="col-sm-12">
                <div class="table-wrapper-scroll-y my-custom-scrollbar texto" >
                
                </div>

                <label for="">Valores Creados:</label><br> 
                <div class="caja">
                
                </div>

                <label for="">Valores Actualizado:</label><br> 
                <div class="caja2">
                
                </div>

              </div>
              </div>           
           <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-borrar" data-dismiss="modal"><b> Cerrar</b></button>
           </div>
         </div>
       </div>
     </div>
  </div>
@endsection
@section('lista')
  
<script>
    

</script>
@endsection
