@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Reporte de Clientes')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      REPORTES DE CLIENTES
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">
                <div class="col">
                  
                </div><!--col-->
                 </div><!--row -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body text-center">
               
                  <form action="{{route('generarReporteCliente')}}" target="_blank" id="formulario" method="post">
                  {{csrf_field()}}
                  <h6 class="table-title m-0 mt-4 mb-4" style="text-align:center"><b>Consulta de Clientes</b></h6>
                  <div class="row">
                   <div class="col-sm-4 ml-4">
                    <div class="form-group">
                     <label for="sucursal">Sucursal</label>
                      <select class="form-control select2" id="selecciones" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="sucursal" class="@error('sucursal') is-invalid @enderror">
                        <option selected="selected" disabled>-- SELECCIONE UNA SUCURSAL --</option>
                        @foreach ($sucursales as $sucursal)
                           <option value="{{$sucursal->codsuc}}">{{$sucursal->nomsucu}}</option>
                           @endforeach
                           <option value="TODAS">TODAS</option>
                       </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                         @error('sucursal')
                        <div class="alert alert-danger">{!!$message!!}</div>
                        @enderror
                       </div>  
                     </div>
                     <div class="col-sm-4 ml-4">
                      <div class="form-group">
                       <label for="sucursal">Desde</label>
                        <select class="form-control select2" id="selecciones2" style="width: 90%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="desde" class="@error('desde') is-invalid @enderror">
                          
                         </select>
                         <span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                           @error('desde')
                          <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                         </div>
                         
                         
                       </div>
                       <div class="col-sm-3 ml-4">
                        <div class="form-group">
                         <label for="sucursal">Hasta</label>
                          <select class="form-control select3" id="selecciones3" style="width: 90%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="hasta" class="@error('hasta') is-invalid @enderror">
                            
                           </select>
                           <span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                             @error('hasta')
                            <div class="alert alert-danger">{!!$message!!}</div>
                            @enderror
                           </div>
                           
                           
                         </div>
                        
                   </div>
                   
                    
                      
              
                <div class="card-footer">
                  <div class="col">
                        <button type="submit"  class="btn btn-info ml-3 mt-4"><b>GENERAR REPORTE</b>
                        </button>
                  </div>
                  
                </div> 
                     
  
                    </form>
                
                {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section>

  
@endsection
@section('script')
   <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        
        
        
    });
   </script>
  <script src="{{asset('js/reportes.js')}} "></script>
  @endsection