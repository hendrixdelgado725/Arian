@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Libro de Ventas')
@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
     ESTATUS DE SERIALES
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">
                <div class="col">
                  
                </div><!--col-->
                 </div><!--row -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body text-center">
               
                  <form action="{{route('EReportespdf')}}" id="formulario" method="post" target="_blank">
                  {{csrf_field()}}
                  <h6 class="table-title m-0 mt-4 mb-4" style="text-align:center"><b>SELECCIÓN DE SUCURSAL Y CAJA </b></h6>
                  <div class="row">
                   <div class="col-sm-5 ml-4 mr-4">
                     <div class="form-group">
                      <label for="sucursal">Sucursal</label>
                        <select class="form-control select2" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" id="sucursal2" data-href="{{route('searchAlmacen')}}" name="sucursal" class="@error('sucursal') is-invalid @enderror">
                           <option selected="selected" disabled>-- SELECCIONE UNA SUCURSAL --</option>
                        @foreach ($sucursales as $sucursal)
                           <option value="{{$sucursal->codsuc}}">{{$sucursal->nomsucu}}</option>
                           @endforeach
                           <!-- <option value="TODAS">TODAS</option> -->
                        </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                      </div>     
                     </div>


                     <div class="col-sm-5 ml-4 mr-4">
                       <div class="form-group">
                         <label for="caja">ALMACÉN</label>
                           <select class="form-control select3" style="width: 100%;" tabindex="-1" aria-hidden="true" name="caja" id="cajas" class="@error('caja') is-invalid @enderror">
                           <option selected="selected" disabled>-- SELECCIONE UN ALMACÉN --</option>
                         </select>
                      </div>     
                     </div>

                   </div>

                    <h6 class="table-title m-0 mt-4 mb-4" style="text-align:center"><b>SELECCIÓN DE PARAMETROS DE INVENTARIO</b></h6>
                   
                   

                   <div class="row">

                     {{-- <div class="col mt-4">
                      <div class="form-group">
                       <div class="icheck-primary d-inline">
                        <input checked type="radio" id="todas" name="status" value="todas">
                        <label for="todas">TODAS</label> 
                        </div>
                      </div>
                      </div> --}}

                     <div class="col mt-4">
                      <div class="form-group">
                       <div class="icheck-primary d-inline">
                        <input type="radio" id="activa" name="status" value="activa">
                        <label for="activa">ACTIVAS</label> 
                        </div>
                      </div>
                      </div>

                      {{-- <div class="col mt-4">
                      <div class="form-group">
                      <div class="icheck-primary d-inline">
                        <input type="radio" id="anulada" name="status" value="salidas">
                        <label for="anulada">SALIDA</label>
                        </div>
                      </div> 
                   </div> --}}
                 </div>


              
                <div class="card-footer">
                <div class="col">
                        <button type="submit" class="btn btn-info ml-3 mt-4"><b>GENERAR REPORTE</b>
                        </button>
                </div>
                <div class="col">
                     <button type="button" id="limpiar" class="btn btn-warning ml-3 mt-4"><b>REINICIAR</b>
                    </button>
                  </div> 
                </div> 
                     
  
                    </form>
                
                {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section>

  
@endsection
@section('script')
   <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
        $('select.select3').select2({
        });
    });
   </script>
    <script src="{{asset('js/reportes.js')}} "></script>
  @endsection