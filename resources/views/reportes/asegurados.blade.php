@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Reporte de Clientes')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Reporte de familiares
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
        <div class="card">
          @include('vendor/flash.flash_message')
            <div class="card-header">
              <h3 class="card-title">
                <b>
                  Reporte de familiares
                </b>
              </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body text-center">
            <form action="{{route('fasvit.reporte_view.pdf')}}" id="formulario" method="post">
                {{csrf_field()}}
              <h6 class="table-title m-0 mt-4 mb-4" style="text-align:center"><b></b></h6>
                <div class="row" style="justify-content: center">
                <div class="offset-2 col-sm-4 ml-4">
                  <div class="form-group">
                  <label for="desde">Desde</label>
                    <select class="form-control select2" name="desde" id="desde">
                      @foreach ($first_select as $item)
                        <option value="{{ $item->codemp }}">{{ $item->nomemp }}</option>
                      @endforeach
                    </select>
                  </div>     
                </div>

                <div class="col-sm-4 ml-4">
                  <div class="form-group">
                  <label for="hasta">Hasta</label>
                    <select class="form-control select2" name="hasta" id="hasta">
                      @foreach ($second_select as $item)
                        <option value="{{ $item->codemp }}">{{ $item->nomemp }}</option>
                      @endforeach
                    </select>
                  </div>     
                </div>
              </div>
              
              <div class="row" style="justify-content: center">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label for="gerencia">Gerencia</label>
                    <select class="form-control select2" name="gerencia" id="gerencia">
                      <option value="all">TODAS</option>
                      @foreach ($gerencias as $item)
                        <option value="{{ $item->codniv }}">{{ $item->desniv }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-12">
                  <div class="card-body">
                    <div class="col">
                      <button type="submit" class="btn btn-info ml-3 mt-4">
                        <b>GENERAR REPORTE</b>
                      </button>
                    </div>
                    <div class="col">
                      <button type="button" id="limpiar" class="btn btn-warning ml-3 mt-4">
                        <b>REINICIAR</b>
                      </button>
                    </div>         
                  </div>
                </div> 
            </form>
          </div>
        </div>
      </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}

  {{-- <asegurados
  :first = "{{ json_encode($first_select) ?? json_encode('[]') }}"
  :second = "{{ json_encode($second_select) ?? json_encode('[]') }}"
  :gerencias = "{{ json_encode($gerencias) ?? json_encode('[]') }}"
  >
    
  </asegurados> --}}
</section>

  
@endsection
@section('script')
   <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
    });
   </script>
  <script src="{{asset('js/reportes.js')}} "></script>
  @endsection