@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Reporte de Productos Vendidos')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      REPORTES DE PRODUCTOS VENDIDOS
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"></h3>

                <div class="card-tools">
                 <div class="row">
                <div class="col">
                  
                </div><!--col-->
                 </div><!--row -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body text-center">
                  <form action="{{route('generarReporteProductoV2')}}" id="formulario" method="post">
                  {{csrf_field()}}
                  <h6 class="table-title m-0 mt-4 mb-4" style="text-align:center">SELECCIÓN DE SUCURSAL Y PARAMETROS DE FECHA</h6>
                  
                  <div class="row">
                   <div class="col-sm-4 ml-4">
                    <div class="form-group">
                     <label for="sucursal">SUCURSAL</label>
                      <select class="form-control select2" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="sucursal" class="@error('sucursal') is-invalid @enderror">
                        <option selected="selected" disabled>-- SELECCIONE UNA SUCURSAL --</option>
                        @foreach ($sucursales as $sucursal)
                           <option value="{{$sucursal->codsuc}}">{{$sucursal->nomsucu}}</option>
                           @endforeach
                           <!-- <option value="TODAS">TODAS</option> -->
                       </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                         @error('sucursal')
                        <div class="alert alert-danger">{!!$message!!}</div>
                        @enderror
                       </div>     
                     </div>
                   
                    <div class="col-sm-3 ml-3">
                     <div class="form-group">
                      <label for="desde">DESDE</label>
                       <input type="date" class="form-control" class="@error('desde') is-invalid @enderror" name="desde" id="desde">
                     </div>
                    </div>
                     
                    <div class="col-sm-3 ml-3">
                     <div class="form-group">
                      <label for="hasta">HASTA</label>
                       <input type="date" class="form-control" class="@error('hasta') is-invalid @enderror" name="hasta" id="hasta">
                     </div>
                    </div>

                    <div class="col-sm-3 ml-3">
                     <div class="form-group">
                      <label for="hasta">ARTICULO</label>
                       <select name="codart" id="" class="form-control select2">
                         <option value="">Seleccione Articulo</option>
                          @foreach ($articulos as $item)
                            <option value="{{$item->codart}}">{{$item->desart." "."[{$item->codart}]"}}</option>
                          @endforeach
                       </select>
                     </div>
                    </div>

                    <div class="col-sm-3 ml-3">
                     <div class="form-group">
                      <label for="hasta">CATEGORIA</label>
                      <select name="codcat[]" id="" class="form-control select2" multiple>
                        <option value="">Seleccione Categoria</option>
                          @foreach ($categorias as $item)
                            <option value="{{$item->codigoid}}">{{$item->nombre}}</option>
                          @endforeach
                       </select>
                     </div>
                    </div>
                   </div>

                   <div class="row">
                     

                    <div class="col-sm-3 ml-3">
                      <div class="form-group">
                        <label>MARCAS</label>
                          <select name="marca[]" id="" class="form-control select2" multiple>
                          <option value="">Seleccione Marca</option>
                            @foreach ($marcas as $item)
                              <option value="{{$item->codigoid}}">{{$item->defmodelo}}</option>
                            @endforeach
                          </select>
                      </div>
                    </div>

                    <div class="col-sm-3 ml-3">
                      <div class="form-group">
                        <label>DESCRIPCION</label>
                      <input type="text" name="descrip" id="tiempo" class="form-control">
                      </div>
                    </div>

                   </div>
                <!--     <div class="col-sm-6">
                      <div class="form-group">
                        <label>HORARIO</label>
                      <input type="time" name="tiempo" id="tiempo" class="form-control" format="H:i:s">
                      </div>
                      </div> -->
          <!--     <h6 class="table-title m-0 mt-4 mb-4" style="text-align:center"><b>PARAMETROS DE TIEMPO</b></h6> -->
                   <div class="row">
                   <div class="col-sm-3 mt-4">
                      <div class="form-group">
                       <div class="icheck-primary d-inline">
                        <input type="radio" id="diario" data-href="{{route('getFecha')}}" name="diames" class="diames" value="diario">
                        <label for="diario">DÍA DE HOY</label> 
                        </div>
                      </div>
                      </div>

                      <div class="col-sm-3 mt-4">
                      <div class="form-group">
                      <div class="icheck-primary d-inline">
                        <input type="radio" id="ayer" data-href="{{route('getFecha')}}" name="diames" class="diames" value="ayer">
                        <label for="ayer">DÍA ANTERIOR</label>
                        </div>
                      </div> 
                      </div>

                      <div class="col-sm-3 mt-4">
                      <div class="form-group">
                      <div class="icheck-primary d-inline">
                        <input type="radio" id="semanal" data-href="{{route('getFecha')}}" name="diames" class="diames" value="semanal">
                        <label for="semanal">SEMANA ACTUAL</label>
                      </div>
                     </div>
                     </div>

                     <div class="col-sm-3 mt-4">
                      <div class="form-group">
                      <div class="icheck-primary d-inline">
                        <input type="radio" id="semant" data-href="{{route('getFecha')}}" name="diames" class="diames" value="semant">
                        <label for="semant">SEMANA PASADA</label>
                      </div>
                     </div>
                     </div>
                     </div>
                    
                      <div class="row">
                      <div class="col-sm-3 mt-4">
                      <div class="form-group">
                      <div class="icheck-primary d-inline">
                        <input type="radio" id="mensual" data-href="{{route('getFecha')}}" name="diames" class="diames" value="mensual">
                        <label for="mensual">MES ACTUAL</label>
                      </div>
                     </div>
                     </div>
          
                     <div class="col-sm-3 mt-4">
                      <div class="form-group">
                      <div class="icheck-primary d-inline">
                        <input type="radio" id="mesant" data-href="{{route('getFecha')}}" name="diames" class="diames" value="mesant">
                        <label for="mesant">MES ANTERIOR</label><br>
                        </div>
                      </div>
                      </div>

                      <div class="col-sm-3 mt-4">
                      <div class="form-group">
                      <div class="icheck-primary d-inline">
                        <input type="radio" id="anual" data-href="{{route('getFecha')}}" name="diames" class="diames" value="anual">
                        <label for="anual">ANUAL</label><br>
                        </div>
                      </div>
                      </div>

                      <div class="col-sm-3 mt-4">
                      <div class="form-group">
                      <div class="icheck-primary d-inline">
                        <input type="radio" id="antyear" data-href="{{route('getFecha')}}" name="diames" class="diames" value="antyear">
                        <label for="antyear">AÑO PASADO</label><br>
                        </div>
                      </div>
                      </div>
                   </div>

                <div class="card-footer">
                  <div class="col">
                        <button type="submit" class="btn btn-info ml-3 mt-4"><b>GENERAR REPORTE</b>
                        </button>
                  </div>
                   <div class="col">
                      <button type="button" id="limpiar" class="btn btn-warning ml-3 mt-4"><b>REINICIAR</b>
                      </button>
                   </div> 
                </div> 
                     
  
                    </form>
                
                {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section>

  
@endsection
@section('script')
   <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
          // theme:"bootstrap"

        });
    });
   </script>
   <script src="{{asset('js/reportes.js')}} "></script>
  @endsection