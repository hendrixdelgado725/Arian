@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Reporte de Categorías')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Reporte de Artículos por Categoría
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
        <div class="card">
          @include('vendor/flash.flash_message')
            <div class="card-header">
              <h3 class="card-title">
                <b>
                  Reporte de Artículos por Categoria
                </b>
              </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body text-center">
            <form action="{{route('generarArtCategorias')}}" id="formulario" method="post" target="_blank">
                {{csrf_field()}}
              <h6 class="table-title m-0 mt-4 mb-4" style="text-align:center"><b></b></h6>
                <div class="row" style="justify-content: center">
                <div class="offset-2 col-sm-4 ml-4">
                  <div class="form-group">
                  <label for="desde">Almacén</label>
                    <select class="form-control select2" name="codalm" id="codalm">
                      <option selected="selected" disabled>-- SELECCIONE UN ALMACEN --</option>
                      @foreach ($almacenes as $almacen)
                         <option value="{{$almacen->codalm}}">{{$almacen->nomalm}}</option>
                         @endforeach
                    </select>
                  </div>     
                </div>

                <div class="col-sm-4 ml-4">
                  <div class="form-group">
                  <label for="hasta">Categoría</label>
                    <select class="form-control select2" name="codcat" id="codcat">
                      <option selected="selected" disabled>-- SELECCIONE UNA CATEGORIA --</option>
                      <option value="TODAS">TODAS</option>
                      @foreach ($categorias as $categoria)
                         <option value="{{$categoria->codigoid}}">{{$categoria->nombre}}</option>
                         @endforeach
                    </select>
                  </div>     
                </div>

              </div>
              
              {{--  <div class="row" style="justify-content: center">
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label for="gerencia">Gerencia</label>
                      <select class="form-control select2" name="gerencia" id="gerencia">
                        <option value="all">TODAS</option>
                
                      </select>
                    </div>
                  </div>
                </div> --}}
              
              <div class="row">
                <div class="col-12">
                  <div class="card-body">
                    <div class="col">
                      <button type="submit" class="btn btn-info ml-3 mt-4">
                        <b>GENERAR REPORTE</b>
                      </button>
                    </div>
                    <div class="col">
                      <button type="button" id="limpiar" class="btn btn-warning ml-3 mt-4">
                        <b>REINICIAR</b>
                      </button>
                    </div>         
                  </div>
                </div> 
            </form>
          </div>
        </div>
      </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}

  {{-- <asegurados
  :first = "{{ json_encode($first_select) ?? json_encode('[]') }}"
  :second = "{{ json_encode($second_select) ?? json_encode('[]') }}"
  :gerencias = "{{ json_encode($gerencias) ?? json_encode('[]') }}"
  >
    
  </asegurados> --}}
</section>

  
@endsection
@section('script')
   <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
    });
   </script>
  <script src="{{asset('js/reportes.js')}} "></script>
  @endsection