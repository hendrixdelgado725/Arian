@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Reportes de Facturas')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
        LISTADO RESUMEN ORDENES DE COMPRA
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')
              



              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Reportes Compras</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Reportes Servicios</a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">


              <div class="card-body text-center">   
               
                <form action="{{route('generarReporteCompra')}}" id="formulario" method="post" target="_blank">
                {{csrf_field()}}
                <h6 class="table-title m-0 mt-4 mb-4" style="text-align:center"><b>CRITERIOS DE SELECCIÓN (COMPRAS)</b></h6>
                <div class="row">

                  <div class="col-sm-2 mt-5">
                      <label>Proveedores:</label>
                  </div>

                 <div class="col-sm-4 ml-6">
                  <div class="form-group">
                   <label for="sucursal">DESDE:</label>
                    <select class="form-control select2" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="desdeProveedorCompra" class="@error('sucursal') is-invalid @enderror">

                      <option selected disabled> -- SELECCIONE UN PROVEEDOR -- </option>

                      @foreach ($provees as $proveedores)

                        <option value="{{ $proveedores->id }}">{{ $proveedores->codpro }} - {{ $proveedores->nompro }}</option>

                      @endforeach
                        
                     </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                       @error('desdeProveedorCompra')
                      <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                     </div>     
                   </div>

                   <div class="col-sm-4 ml-6 mr-6">
                    <div class="form-group">
                      <label for="sucursal">HASTA:</label>
                       <select class="form-control select2" style="width: 100%;" data-select2-id="3" tabindex="-1" aria-hidden="true" name="hastaProveedorCompra" class="@error('sucursal') is-invalid @enderror">
                        <option selected disabled> -- SELECCIONE UN PROVEEDOR -- </option>

                         @foreach ($provees as $proveedores)
 
                           <option value="{{ $proveedores->id }}">{{ $proveedores->codpro }} - {{ $proveedores->nompro }}</option>
 
                         @endforeach
                           
                        </select><span dir="ltr" data-select2-id="4" style="width: 100%;"><span class="selection">
                          @error('hastaProveedorCompra')
                         <div class="alert alert-danger">{!!$message!!}</div>
                         @enderror
                        </div>      
                   </div>
                 
                 </div>

                 <div class="row">

                  <div class="col-sm-2 mt-2">
                      <label>Orden de Compra:</label>
                  </div>

                 <div class="col-sm-4 ml-6">
                  <div class="form-group">
      
                    <select class="form-control select2" style="width: 100%;" data-select2-id="5" tabindex="-1" aria-hidden="true" name="desdeOrdenCompra" class="@error('sucursal') is-invalid @enderror">
                      <option selected disabled> -- SELECCIONE UNA ORDEN DE COMPRA -- </option>
                     
                      @foreach ($ordenCompra as $compras)

                        <option value="{{ $compras->fecord }}">{{ $compras->ordcom }}</option>

                      @endforeach

                     </select><span dir="ltr" data-select2-id="6" style="width: 100%;"><span class="selection">
                       @error('desdeOrdenCompra')
                      <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                     </div>     
                   </div>

                   <div class="col-sm-4 ml-6 mr-6">
                    <div class="form-group">
      
                      <select class="form-control select2" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" name="hastaOrdenCompra" class="@error('sucursal') is-invalid @enderror">
                        <option selected disabled> -- SELECCIONE UNA ORDEN DE COMPRA -- </option>
                       
                        @foreach ($ordenCompra as $compras)

                        <option value="{{ $compras->fecord }}">{{ $compras->ordcom }}</option>

                        @endforeach
                    
                       </select><span dir="ltr" data-select2-id="8" style="width: 100%;"><span class="selection">
                         @error('hastaOrdenCompra')
                        <div class="alert alert-danger">{!!$message!!}</div>
                        @enderror
                       </div>     
                   </div>        
              
                 </div>

                 <div class="row">

                  <div class="col-sm-2 mt-2">
                      <label>Fecha:</label>
                  </div>

                 <div class="col-sm-4 ml-6">
                  <div class="form-group">
               
                      <input type="date" name="desde" class="form-control"/>

                       @error('status')
                      <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                     </div>     
                   </div>

                   <div class="col-sm-4 ml-6 mr-6">
                     <div class="form-group">
                     
                      <input type="date" name="hasta" class="form-control"/>

                    </div>     
                   </div>
                           
                 </div>

                 <div class="row">

                  <div class="col-sm-2 mt-2">
                      <label>Estatus:</label>
                  </div>

                 <div class="col-sm-4 ml-6">
                  <div class="form-group">
              
                    <select class="form-control" style="width: 100%;" tabindex="-1" aria-hidden="true" id="status" name="status" class="@error('status') is-invalid @enderror">
                        
                        <option selected="selected" value="TODAS">Todas</option>
                        <option value="A">Activas</option>
                        <option value="N">Anuladas</option>
                  
                     </select><span dir="ltr" style="width: 100%;"><span class="selection">
                       @error('status')
                      <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                     </div>     
                   </div>
              
                 </div>

            
              <div class="card-footer">
                <div class="col">
                      <button type="submit" class="btn btn-info ml-3 mt-4"><b>GENERAR REPORTE</b>
                      </button>
                </div>
                <div class="col">
                    <button type="button" id="limpiar" class="btn btn-warning ml-3 mt-4"><b>REINICIAR</b>
                    </button>
                </div> 
              </div> 
                   

                  </form>
              
            
        </div>  

                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">


              <div class="card-body text-center">   
               
                <form action="{{route('generarReporteServicio')}}" id="formulario" method="post" target="_blank">
                {{csrf_field()}}
                <h6 class="table-title m-0 mt-4 mb-4" style="text-align:center"><b>CRITERIOS DE SELECCIÓN (SERVICIOS)</b></h6>
                <div class="row">

                  <div class="col-sm-2 mt-5">
                      <label>Proveedores:</label>
                  </div>

                 <div class="col-sm-4 ml-6">
                  <div class="form-group">
                   <label for="sucursal">DESDE:</label>
                    <select class="form-control select2" style="width: 100%;" data-select2-id="9" tabindex="-1" aria-hidden="true" name="desdeProveedorServico" class="@error('sucursal') is-invalid @enderror">
                      <option selected disabled> -- SELECCIONE UN PROVEEDOR -- </option>

                      @foreach ($provees as $proveedores)

                        <option value="{{ $proveedores->id }}">{{ $proveedores->codpro }} - {{ $proveedores->nompro }}</option>

                      @endforeach
                        
                     </select><span dir="ltr" data-select2-id="10" style="width: 100%;"><span class="selection">
                       @error('desdeProveedorServico')
                      <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                     </div>     
                   </div>

                   <div class="col-sm-4 ml-6 mr-6">
                    <div class="form-group">
                      <label for="sucursal">HASTA:</label>
                       <select class="form-control select2" style="width: 100%;" data-select2-id="11" tabindex="-1" aria-hidden="true" name="hastaProveedorServico" class="@error('sucursal') is-invalid @enderror">
                        
                        <option selected disabled> -- SELECCIONE UN PROVEEDOR -- </option>

                         @foreach ($provees as $proveedores)
 
                           <option value="{{ $proveedores->id }}">{{ $proveedores->codpro }} - {{ $proveedores->nompro }}</option>
 
                         @endforeach
                           
                        </select><span dir="ltr" data-select2-id="12" style="width: 100%;"><span class="selection">
                          @error('hastaProveedorServico')
                         <div class="alert alert-danger">{!!$message!!}</div>
                         @enderror
                        </div>      
                   </div>
                 
                 </div>

                 <div class="row">

                  <div class="col-sm-2 mt-2">
                      <label>Orden de Servicio:</label>
                  </div>

                 <div class="col-sm-4 ml-6">
                  <div class="form-group">
      
                    <select class="form-control select2" style="width: 100%;" data-select2-id="13" tabindex="-1" aria-hidden="true" name="desdeOrdenServico" class="@error('sucursal') is-invalid @enderror">
                      <option selected disabled> -- SELECCIONE UNA ORDEN DE SERVICIO -- </option>

                      @foreach ($ordenServicio as $servicio)

                        <option value="{{ $servicio->id }}">{{ $servicio->ordcom }}</option>

                      @endforeach

                     </select><span dir="ltr" data-select2-id="14" style="width: 100%;"><span class="selection">
                       @error('desdeOrdenServico')
                      <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                     </div>     
                   </div>

                   <div class="col-sm-4 ml-6 mr-6">
                    <div class="form-group">
      
                      <select class="form-control select2" style="width: 100%;" data-select2-id="15" tabindex="-1" aria-hidden="true" name="hastaOrdenServico" class="@error('sucursal') is-invalid @enderror">
                       <option selected disabled> -- SELECCIONE UNA ORDEN DE SERVICIO -- </option>
                       
                        @foreach ($ordenServicio as $servicio)

                          <option value="{{ $servicio->id }}">{{ $servicio->ordcom }}</option>

                        @endforeach
                    
                       </select><span dir="ltr" data-select2-id="16" style="width: 100%;"><span class="selection">
                         @error('hastaOrdenServico')
                        <div class="alert alert-danger">{!!$message!!}</div>
                        @enderror
                       </div>     
                   </div>        
              
                 </div>

                 <div class="row">

                  <div class="col-sm-2 mt-2">
                      <label>Fecha:</label>
                  </div>

                 <div class="col-sm-4 ml-6">
                  <div class="form-group">
               
                      <input type="date" name="desde" class="form-control"/>

                       @error('desde')
                      <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror

                     </div>     
                   </div>

                   <div class="col-sm-4 ml-6 mr-6">
                     <div class="form-group">
                     
                      <input type="date" name="hasta" class="form-control"/>

                      @error('hasta')
                      <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror

                    </div>     
                   </div>
                           
                 </div>

                 <div class="row">

                  <div class="col-sm-2 mt-2">
                      <label>Estatus:</label>
                  </div>

                 <div class="col-sm-4 ml-6">
                  <div class="form-group">
              
                    <select class="form-control" style="width: 100%;" tabindex="-1" aria-hidden="true" id="status" name="status" class="@error('sucursal') is-invalid @enderror">
                        
                        <option selected="selected" value="TODAS">Todas</option>
                        <option value="A">Activas</option>
                        <option value="N">Anuladas</option>
                  
                     </select><span dir="ltr" style="width: 100%;"><span class="selection">
                       @error('status')
                      <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                     </div>     
                   </div>
              
                 </div>

            
              <div class="card-footer">
                <div class="col">
                      <button type="submit" class="btn btn-info ml-3 mt-4"><b>GENERAR REPORTE</b>
                      </button>
                </div>
                <div class="col">
                    <button type="button" id="limpiar" class="btn btn-warning ml-3 mt-4"><b>REINICIAR</b>
                    </button>
                </div> 
              </div> 
                   

                  </form>
              
            
        </div>

                </div>
              </div>
         




          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section>

  
@endsection
@section('script')
   <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
    });
   </script>
   <script src="{{asset('js/reportes.js')}} "></script>
  @endsection