@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Reporte de Productos Vendidos v2')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      PRODUCTOS VENDIDOS RESUMIDOS
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">
                <div class="col">
                  
                </div><!--col-->
                 </div><!--row -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body text-center">
               
                  <form action="{{route('ProductosVendidosResumidos')}}" id="formulario" method="post" target="_blank">
                  {{csrf_field()}}
                  <h6 class="table-title m-0 mt-4 mb-4" style="text-align:center"><b>SELECCIÓN DE SUCURSAL Y CAJA</b></h6>
                  <div class="row">
                   <div class="col-sm-5 ml-4 mr-4">
                    <div class="form-group">
                     <label for="sucursal">Sucursal</label>
                      <!-- <select class="form-control select2" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="sucursal" class="@error('sucursal') is-invalid @enderror"> -->
                        <select class="form-control select2" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" id="sucursal" data-href="{{route('getCajasPre')}}" name="sucursal" class="@error('sucursal') is-invalid @enderror">
                        <option selected="selected" disabled>-- SELECCIONE UNA SUCURSAL --</option>
                        @foreach ($sucursales as $sucursal)
                           <option value="{{$sucursal->codsuc}}">{{$sucursal->nomsucu}}</option>
                           @endforeach
                          <!--  <option value="TODAS">TODAS</option> -->
                       </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                         @error('sucursal')
                        <div class="alert alert-danger">{!!$message!!}</div>
                        @enderror
                       </div>     
                     </div>
                    
                    <div class="col-sm-5 ml-4 mr-4">
                       <div class="form-group">
                         <label for="caja">Caja</label>
                           <select class="form-control select3" style="width: 100%;" tabindex="-1" aria-hidden="true" name="caja" id="cajas" class="@error('caja') is-invalid @enderror">
                           <option selected="selected" disabled>-- SELECCIONE UNA CAJA --</option>
                         </select>
                      </div>     
                     </div>


                   </div>

                   <h6 class="table-title m-0 mt-4 mb-4" style="text-align:center"><b>SELECCIÓN DE PARAMETROS DE FECHA</b></h6>

                   <div class="row">
                      <div class="col-sm-5 mr-4 ml-4">
                    <div class="form-group">
                    <label for="desde"> Desde</label>
                     <input type="date" class="form-control" class="@error('desde') is-invalid @enderror" name="desde" id="desde">
                    </div>
                    </div>
                     
                   <div class="col-sm-5 mr-4 ml-4">
                   <div class="form-group">
                   <label for="hasta"> Hasta</label>
                     <input type="date" class="form-control" class="@error('hasta') is-invalid @enderror" name="hasta" id="hasta">
                   </div>
                   </div>
                  </div>
                         
                   
          <!--     <h6 class="table-title m-0 mt-4 mb-4" style="text-align:center"><b>PARAMETROS DE TIEMPO</b></h6> -->
                   <div class="row">
                   <div class="col-sm-3 mt-4">
                      <div class="form-group">
                       <div class="icheck-primary d-inline">
                        <input type="radio" id="diario" data-href="{{route('getFecha')}}" name="diames" class="diames" value="diario">
                        <label for="diario">Día de hoy </label> 
                        </div>
                      </div>
                      </div>

                      <div class="col-sm-3 mt-4">
                      <div class="form-group">
                      <div class="icheck-primary d-inline">
                        <input type="radio" id="ayer" data-href="{{route('getFecha')}}" name="diames" class="diames" value="ayer">
                        <label for="ayer">Día anterior</label>
                        </div>
                      </div> 
                      </div>

                      <div class="col-sm-3 mt-4">
                      <div class="form-group">
                      <div class="icheck-primary d-inline">
                        <input type="radio" id="semanal" data-href="{{route('getFecha')}}" name="diames" class="diames" value="semanal">
                        <label for="semanal">Semana Actual</label>
                      </div>
                     </div>
                     </div>

                     <div class="col-sm-3 mt-4">
                      <div class="form-group">
                      <div class="icheck-primary d-inline">
                        <input type="radio" id="semant" data-href="{{route('getFecha')}}" name="diames" class="diames" value="semant">
                        <label for="semant">Semana pasada</label>
                      </div>
                     </div>
                     </div>
                     </div>
                    
                      <div class="row">
                      <div class="col-sm-3 mt-4">
                      <div class="form-group">
                      <div class="icheck-primary d-inline">
                        <input type="radio" id="mensual" data-href="{{route('getFecha')}}" name="diames" class="diames" value="mensual">
                        <label for="mensual">Mes actual</label>
                      </div>
                     </div>
                     </div>
          
                     <div class="col-sm-3 mt-4">
                      <div class="form-group">
                      <div class="icheck-primary d-inline">
                        <input type="radio" id="mesant" data-href="{{route('getFecha')}}" name="diames" class="diames" value="mesant">
                        <label for="mesant">Mes anterior</label><br>
                        </div>
                      </div>
                      </div>

                      <div class="col-sm-3 mt-4">
                      <div class="form-group">
                      <div class="icheck-primary d-inline">
                        <input type="radio" id="anual" data-href="{{route('getFecha')}}" name="diames" class="diames" value="anual">
                        <label for="anual">Anual</label><br>
                        </div>
                      </div>
                      </div>

                      <div class="col-sm-3 mt-4">
                      <div class="form-group">
                      <div class="icheck-primary d-inline">
                        <input type="radio" id="antyear" data-href="{{route('getFecha')}}" name="diames" class="diames" value="antyear">
                        <label for="antyear">Año pasado</label><br>
                        </div>
                      </div>
                      </div>
                   </div>
              
                <div class="card-footer">
                  <div class="col">
                        <button type="submit" class="btn btn-info ml-3 mt-4"><b>GENERAR REPORTE</b>
                        </button>
                  </div>
                   <div class="col">
                      <button type="button" id="limpiar" class="btn btn-warning ml-3 mt-4"><b>REINICIAR</b>
                      </button>
                   </div> 
                </div> 
                     
  
                    </form>
                
                {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section>

  
@endsection
@section('script')
   <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
        $('select.select3').select2({
        });
    });
   </script>
   <script src="{{asset('js/reportes.js')}} "></script>
  @endsection