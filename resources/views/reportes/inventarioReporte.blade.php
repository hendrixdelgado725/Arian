@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Reportes de Inventario')
@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      REPORTES INVENTARIO
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">
                <div class="col">
                  
                </div><!--col-->
                 </div><!--row -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body text-center">
               
                  <form action="{{route('generarReporteERAlmacen')}}" id="formulario" method="post" target="_blank">
                  {{csrf_field()}}
                  <h6 class="table-title m-0 mt-4 mb-4" style="text-align:center"><b>SELECCIÓN DE ALMACEN Y OTROS PARÁMETROS</b></h6>
                  <div class="row">

                   <div class="col-sm-4">
                    <div class="form-group">
                     <label for="almacen">Almacén</label>
                      <select class="form-control select2" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="almacen" class="@error('almacen') is-invalid @enderror">
                        <option selected="selected" disabled>-- SELECCIONE UN ALMACEN --</option>
                        @foreach ($almacenes as $almacen)
                           <option value="{{$almacen->codalm}}">{{$almacen->nomalm}}</option>
                           @endforeach
                           <!-- <option value="TODAS">TODOS</option> -->
                       </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                         @error('sucursal')
                        <div class="alert alert-danger">{!!$message!!}</div>
                        @enderror
                      </div>     
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                          <label for="almacen">Categoria de Producto</label>
                          <select class="form-control select2" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="codcat" class="@error('codcat') is-invalid @enderror">
                          <option selected="selected" disabled>-- SELECCIONE UNA CATEGORIA --</option>
                          @foreach ($categorias as $item)
                          <option value="{{$item->codigoid}}">{{$item->nombre}}</option>
                          @endforeach
                        </select>
                        @error('codcat')
                        <div class="alert alert-danger">{!!$message!!}</div>
                        @enderror
                        </div>
                      </div>

                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="almacen">Producto</label>
                        <select class="form-control select2" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="codart" class="@error('codart') is-invalid @enderror">
                          <option selected="selected" disabled>-- SELECCIONE UN PRODUCTO --</option>
                        @foreach ($productos as $item)
                        <option value="{{$item->codart}}">{{$item->codart." ".$item->desart}}</option>
                        @endforeach
                      </select>
                      @error('codart')
                      <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                    </div>
                    </div>
                    
              </div>
              <div class="card-footer">
                <div class="col">
                        <button type="submit" class="btn btn-info ml-3 mt-4"><b>GENERAR REPORTE</b>
                        </button>
                </div>
                <div class="col">
                     <button type="button" id="limpiar" class="btn btn-warning ml-3 mt-4"><b>REINICIAR</b>
                    </button>
                  </div> 
                </div> 
                
                     
  
                    </form>
                
                {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section>

  
@endsection
@section('script')
   <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
    });
   </script>
   <script src="{{asset('js/reportes.js')}} "></script>
  @endsection