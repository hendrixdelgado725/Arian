@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Cajas')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Cambio de IVA (Administrador)
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-6">
        <div class="card">
          @include('vendor/flash.flash_message')
          <div class="card-footer">
            <h3 class="card-title"></h3>
              <div class="card-tools">
                <div class="row">
                  Selección IVA
                  
                </div>
              </div>
          </div>

          <form action="{{ route('cambiarIva', $caja) }}" method="post">
            {{ csrf_field() }}
        
            <div class="card-body">
                <div class="form-check pb-3">
                    <input class="form-check-input" type="checkbox" value="1" name="acepiva" id="acepiva"
                           {{ $caja->acepiva == true ? 'checked' : '' }}
                           onchange="toggleIvaSelect(this)">  <label class="form-check-label">
                        Esta caja va a facturar con IVA?
                    </label>
                </div>
        
                <select class="form-control select2" id="codiva" required="required" name="codiva"
                        data-dropdown-css-class="select2-danger" aria-hidden="true"  class="@error('iva') is-invalid @enderror"
                        @if (!$caja->acepiva) disabled @endif>  <option value="">Seleccione IVA</option>
                    @foreach ($iva as $item)
                        <option value="{{ $item->codrgo }}"
                                @if ($caja->codiva === $item->codrgo) selected @endif>{{ $item->nomrgo }}</option>
                    @endforeach
                </select>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-success"><b>Guardar Cambios</b></button>
                <a href="{{ url()->previous() }}" class="btn btn-default ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
            </div>
        </form>

          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section>
@endsection
@section('script')
<script>
  $(document).ready(()=>{
    deletecaja();
      function deletecaja(){//se insertaran paises a la configuracion del sistema siempre y cuando Existan
        $('a#delete').click(function(e){
          e.preventDefault();
          const row = $(this).parents('tr');
          const url = $(this).attr('href');
		  		Swal.fire({
				  title: 'Seguro que Desea Eliminar?',
				  text: "Despues de esto no se encontrara en la lista!",
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, delete it!'
        }).then((result) => {//dio click en el boton aceptar
          if(result.value){
            $.get(url,function(result){
              if (result.exito) {
                Swal.fire(
                  'Caja Eliminada!',
                  'Con Exito.',
                  'success',
                )  
              row.fadeOut();
              }else if(result.error){
                Swal.fire({
                  icon: 'warning',
                  title: result.error,
                  timer:1500
                  })
                }
            })
          }      
        })
		});
    }
  })

</script>

<script>
  $(document).ready(()=>{
    $('select.select2').select2({
    });

    $('#fiscal').click(function(){

      if($(this).is(':checked')) $(this).val($(this).is(':checked'))
      else $(this).val($(this).is(':checked'))
      
    })
  })


  function toggleIvaSelect(checkbox) {
  const ivaSelect = document.getElementById('codiva');
  ivaSelect.disabled = !checkbox.checked;
}

</script>
@endsection