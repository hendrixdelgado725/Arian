@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Configuración Caja')

  @section('content')
  <div class="container">
    <div class="page-inner">
       @component('layouts.contenth',[
        '_breadcrumb'=> [
        'Cajas' => route('cajaList')
        ]
       ])
        @slot('titulo')
          CONFIGURACI&Oacute;N DE CAJA
        @endslot
      @endcomponent
      <div class="row justify-content-center">
        <div class="col-10">
            <!-- general form elements -->
            <div class="card">
            @include('vendor/flash.flash_message')
            
            <!-- /.card-header -->
            <!-- form start -->
            {{-- <form action="{{route('cajaCreate')}}" method="post"> --}}
              {{csrf_field()}}

              <div class="card-body">
              <div class="form group">
                <label for="descaj">Nombre de Caja</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"></span>
                  </div>
                  <input type="text" class="form-control" placeholder="" name="descaj" 
                id="descaj" class="@error('descaj') is-invalid @enderror" value="{{$caja->descaj}}" disabled>
                </div>
                @error('descaj')
                <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
              </div>
              
              <div class="form group">
                <label for="impfisname">Nombre de Impresora Fiscal</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"></span>
                  </div>
                <input type="text" class="form-control" class="@error('impfisname') is-invalid @enderror" placeholder="" name="impfisname" id="impfisname" value="{{$caja->impfisname}}" disabled>
                </div>
                @error('impfisname')
                <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
              </div>
              
              <div class="form group">
                <label for="impfishost">IP de Impresora Fiscal</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"></span>
                  </div>
                <input type="text" class="form-control" class="@error('impfishost') is-invalid @enderror" placeholder="" name="impfishost" id="impfishost" value="{{$caja->impfishost}}" disabled>
                </div>
            @error('impfishost')
            <div class="alert alert-danger">{!!$message!!}</div>
            @enderror
          </div>
          
          <div class="form group">
            <label for="impserial">Serial de Impresora Fiscal</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text"></span>
              </div>
            <input type="tel" class="form-control" class="@error('impserial') is-invalid @enderror" placeholder="" name="impserial" id="impserial" value="{{$caja->impserial}}" disabled> 
            </div>
            @error('impserial')
            <div class="alert alert-danger">{!!$message!!}</div>
            @enderror
          </div>

           <div class="form group">
              <label for="correlativo">Correlativo</label>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text"></span>
                </div>
                <input readonly type="tel" class="form-control" class="@error('correlativo') is-invalid @enderror" placeholder="" name="correlativo" id="correlativo"
              value="{{$caja->correlativo}}"> 
              </div>
              @error('correlativo')
              <div class="alert alert-danger">{!!$message!!}</div>
              @enderror
            </div>
            <!-- /.card-body -->
                <div class="card-footer">
                  <div class="col-sm-4">
                    <a href="{{route('cajaReporte',array($caja->id,'X'))}}" data-r="X" id="reporte" class="btn btn-info btn-block ruledis" ><b>Enviar Reporte X</b></a>
                      @isset($string_x)
                        <div class="alert alert-warning">{!!$string_x!!}</div>
                      @endisset
                    <a href="{{route('cajaReporte',array($caja->id,'XP'))}}" data-r="XP" id="reporte" class="btn btn-info btn-block ruledis" ><b>Enviar Reporte X parcial</b></a>
                      @isset($string_xp)
                        <div class="alert alert-warning">{!!$string_xp!!}</div>
                      @endisset
                    <a href="{{route('cajaReporte',array($caja->id,'Z'))}}" data-r="Z" id="reporte" class="btn btn-info btn-block ruledis" ><b>Enviar Reporte Z</b></a>
                      @isset($string_z)
                        <div class="alert alert-warning">{!!$string_z!!}</div>
                      @endisset
                    {{-- <a href="{{route('getStatus',$caja->id)}}" id="reporte" data-fiscal="true" class="btn btn-success btn-block getStatus" ><b>Estatus Impresora</b></a> --}}
                    <a href="{{route('sendTickera',$caja->id)}}" id="reporte" data-fecha="ayer" class="btn btn-success btn-block getStatus" ><b>Enviar cierre tickera anterior</b></a>


                    <small id="emailHelp" class="form-text text-muted">Si la impresora no da respuesta. Verifique bien su conexi&oacute;n</small>
                  </div>
                  <a href="{{ url()->previous() }}" class="btn btn-default ml-2 mt-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                {{-- </form> --}}
            </div>
            <!-- /.card -->
            </div>
        </div>
      </div>
        {{-- row --}}
    </div>
  </div>
  <input id="inputdis" type="hidden" data-disabled="disabled">
{{-- <conexion-fiscal channel="{{ $caja->id }}"></conexion-fiscal> --}}

</div>
@endsection

@section('script')
  <script src="{{ asset('js/caja/sendreporte.js') }}" ></script>
@endsection