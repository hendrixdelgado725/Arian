@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Editar Caja')

@section('content')
    {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Caja
    @endslot
  @endcomponent
    {{-- CONTENT --}}
  <section class="content">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-8">
          @include('vendor/flash.flash_message')

          <div class="card card-info">
            <div class="card-header">
            <h3 class="card-title mt-2"><b>Edici&oacute;n de Caja</b></h3>
            {{-- <div class="d-flex flex-row-reverse bd-highlight">
              <div>
                 <a href="{{ url()->previous() }}" class="btn btn-danger"><i class="fas fa-arrow-left"></i><b> Atras</b></a>
                </div>
              </div> --}}
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{route('cajaUpdate',$caja->id)}}" method="post">
              @method('PUT')
              {{csrf_field()}}
              <div class="card-body">
              <div class="form group">
                <label for="descaj">Nombre de Caja</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"></span>
                  </div>
                  <input type="text" class="form-control" placeholder="" name="descaj" 
                  id="descaj" class="@error('descaj') is-invalid @enderror"
                value="{{$caja->descaj}}" >
                </div>
                @error('descaj')
                <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
              </div>
              
              <div class="form group">
                <label for="impfisname">Nombre de Impresora Fiscal</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"></span>
                  </div>
                  <input type="text" class="form-control" class="@error('impfisname') is-invalid @enderror" placeholder="" name="impfisname" id="impfisname" value="{{$caja->impfisname}}">
                </div>
                @error('impfisname')
                <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
              </div>
              
              <div class="form group">
                <label for="impfishost">IP de Impresora Fiscal</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"></span>
                  </div>
                  <input type="text" class="form-control" class="@error('impfishost') is-invalid @enderror" placeholder="" name="impfishost" id="impfishost" value="{{$caja->impfishost}}">
                </div>
            @error('impfishost')
            <div class="alert alert-danger">{!!$message!!}</div>
            @enderror
          </div>
          
            <div class="form group">
              <label for="impserial">Serial de Impresora Fiscal</label>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text"></span>
                </div>
                <input type="tel" class="form-control" class="@error('impserial') is-invalid @enderror" placeholder="" name="impserial" id="impserial" value="{{$caja->impserial}}"> 
              </div>
              @error('impserial')
              <div class="alert alert-danger">{!!$message!!}</div>
              @enderror
            </div>

                <div class="form group">
                  <label for="corfac">Correlativo de caja</label>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"></span>
                    </div>
                    <input  {{ Auth::user()->roles->nombre === "ADMIN" ? '' : 'disabled' }} type="number" class="form-control" class="@error('corfac') is-invalid @enderror" placeholder="" name="corfac" id="corfac" value="{{$caja->corfac}}"> 
                  </div>
                  @error('corfac')
                  <div class="alert alert-danger">{!!$message!!}</div>
                  @enderror
                </div>      
                
                <div class="form group">
                  <label for="corfac">Correlativo de Nota de Credito</label>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"></span>
                    </div>
                    <input  {{ Auth::user()->roles->nombre === "ADMIN" ? '' : 'disabled' }} type="number" class="form-control" class="@error('cornot') is-invalid @enderror" placeholder="" name="cornot" id="cornot" value="{{$caja->cornot}}"> 
                  </div>
                  @error('cornot')
                  <div class="alert alert-danger">{!!$message!!}</div>
                  @enderror
                </div>      

            <div class="form group">
              <label for="idsucur">Sucursal</label>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <!-- <span class="input-group-text"></span> -->
                </div>
                <select id="codsuc" class="form-control select2" name="codsuc">
                  <!-- <option value="">Seleccione Sucursal</option> -->
                  @if($caja->codsuc!=NULL)
                    <option value="{{$caja->idsucur}}" selected="selected">{{$caja->sucur->nomsucu}}</option>
                  @else
                    <option selected="selected" data-select2-id="">Seleccione Sucursal</option>
                  @endif
                  @foreach ($sucursales as $item)

                  @php($selected = $item->codsuc === $caja->codsuc ? 'selected' : '')
                    <option value="{{$item->codsuc}}" {{$selected}} >{{$item->nomsucu}}</option>
                  @endforeach
                </select> 
              </div>
              @error('codsuc')
              <div class="alert alert-danger">{!!$message!!}</div>
              @enderror
            </div>

            <div class="form group">
              <label for="idsucur">Asociar Almacen</label>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <!-- <span class="input-group-text"></span> -->
                </div>
                <select id="codsuc" class="form-control select2" name="codalm">
                 <option data-select2-id="" disabled>Seleccione Almacen</option>
                 @foreach ($alm as $item)
                    <option {{$item->codalm === $caja->codalm ? 'selected' : ''}} value="{{$item->codalm}}">{{$item->nomalm}}</option>
                  @endforeach
                </select> 
              </div>
              @error('codalm')
              <div class="alert alert-danger">{!!$message!!}</div>
              @enderror
            </div>
            <div class="form group">
              <label for="idsucur">Es Fiscal</label>
              <div class="input-group mb-3">
                
                  <input name="isFiscal" {{$caja->is_fiscal === "true" ? 'checked' : ''}} id="fiscal" type="checkbox" value="{{$caja->is_fiscal === "true" ? 'true' : 'false'}}">
              </div>
              
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button type="submit" class="btn btn-success"><b>Guardar Cambios</b></button>
              <a href="{{ url()->previous() }}" class="btn btn-default ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
            </div>
            </form>
            </div>
            <!-- /.card -->
            </div>
        </div>
      </div>
        {{-- row --}}
    </div>
    {{-- container fluid --}}
  </section>
@endsection
@section('script')
<script src="{{asset('js/caja/sendreporte.js')}}"></script>
<script>
      $(document).ready(()=>{
        $('select.select2').select2({
        });

        $('#fiscal').click(function(){

          if($(this).is(':checked')) $(this).val($(this).is(':checked'))
          else $(this).val($(this).is(':checked'))
          
        })


      })
</script>
@endsection