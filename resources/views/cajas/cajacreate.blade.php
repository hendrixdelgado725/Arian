@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Registrar Caja')

@section('content')
    {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      <!-- Caja -->
    @endslot
  @endcomponent
    {{-- CONTENT --}}
  <section class="content">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-8">
          <div class="card card-info">
            @include('vendor/flash.flash_message')
            <div class="card-header">
            <h3 class="card-title"><b>Registro de Caja</b</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{route('cajaSave')}}" method="post">
              {{csrf_field()}}
              <div class="card-body">
              <div class="form group">
                <label for="correlativo">Correlativo</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"></span>
                  </div>
                  <input type="text" readonly class="form-control" placeholder="" name="correlativo" 
                  id="correlativo" class="@error('correlativo') is-invalid @enderror"
                onkeyup="pasarMayusculas(this.value,this.id)" value="{{$correla}}">
                </div>
                @error('correlativo')
                <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
              </div>
              <div class="form group">
                <label for="descaj">Nombre de Caja</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"></span>
                  </div>
                  <input type="text" class="form-control" placeholder="" name="descaj" 
                  id="descaj" class="@error('descaj') is-invalid @enderror"
                onkeyup="pasarMayusculas(this.value,this.id)" value="{{old("descaj")}}">
                </div>
                @error('descaj')
                <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
              </div>
              
              <div class="form group">
                <label for="impfisname">Nombre de Impresora Fiscal</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"></span>
                  </div>
                  <input type="text" class="form-control" class="@error('impfisname') is-invalid @enderror" placeholder="" name="impfisname" id="impfisname" onkeyup="pasarMayusculas(this.value,this.id)"
                  value="{{old("impfisname")}}"
                  >
                </div>
                @error('impfisname')
                <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
              </div>
              
              <div class="form group">
                <label for="impfishost">IP de Impresora Fiscal</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"></span>
                  </div>
                <input type="text" class="form-control" class="@error('impfishost') is-invalid @enderror" placeholder="" name="impfishost" id="impfishost" data-mask="099.099.099.099" maxlength="10" value="{{old("impfishost")}}">
                </div>
                @error('impfishost')
                <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
              </div>
              
              <div class="form group">
                <label for="impserial">Serial de Impresora Fiscal</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"></span>
                  </div>
                  <input type="tel" class="form-control" class="@error('impserial') is-invalid @enderror" placeholder="" name="impserial" id="impserial" onkeyup="pasarMayusculas(this.value,this.id)"
                value="{{old("impserial")}}"> 
                </div>
                @error('impserial')
                <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
              </div>


              <div class="form group">
                <label for="corfac">Correlativo de caja</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"></span>
                  </div>
                  <input  {{ Auth::user()->roles->nombre === "ADMIN" ? '' : 'disabled' }} type="number" class="form-control" class="@error('corfac') is-invalid @enderror" placeholder="" name="corfac" id="corfac"> 
                </div>
                @error('corfac')
                <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
              </div>      
              
              <div class="form group">
                <label for="corfac">Correlativo de Nota de Credito</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"></span>
                  </div>
                  <input  {{ Auth::user()->roles->nombre === "ADMIN" ? '' : 'disabled' }} type="number" class="form-control" class="@error('cornot') is-invalid @enderror" placeholder="" name="cornot" id="cornot" > 
                </div>
                @error('cornot')
                <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
              </div>     


              
            
            <div class="form-group">
              <label for="codsuc">Sucursal</label> 
              <div>
                <select class="form-control select2" id="codsuc" required="required" name="codsuc" data-dropdown-css-class="select2-danger"caria-hidden="true" class="@error('codsuc') is-invalid @enderror">
                  <option value="">Seleccione Sucursal</option>
                  @foreach ($sucursales as $item)
                    <option value="{{$item->codsuc}}">{{$item->nomsucu}}</option>
                  @endforeach
                </select>
              </div>
              @error('codsuc')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
            </div>
            <div class="form group">
              <label for="idsucur">Asociar Almacen</label>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <!-- <span class="input-group-text"></span> -->
                </div>
                <select id="codsuc" class="form-control select2" name="codalm">
                   <option value="">Seleccione Almacen</option> 
                 
                  @foreach ($alm as $item)
                    <option value="{{$item->codalm}}">{{$item->nomalm}}</option>
                  @endforeach
                </select> 
              </div>
              @error('codalm')
              <div class="alert alert-danger">{!!$message!!}</div>
              @enderror
            </div>

            <div class="form group">
              <label for="idsucur">Es Fiscal</label>
              <div class="input-group mb-3">
                
                  <input name="isFiscal" id="fiscal" type="checkbox" >
              </div>
              
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-success"><b>Guardar Cambios</b></button>
                <a href="{{ url()->previous() }}" class="btn btn-default ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
            </div>
            </form>
            </div>
            <!-- /.card -->
            </div>
        </div>
      </div>
        {{-- row --}}
    </div>
    {{-- container fluid --}}
  </section>
@endsection
@section('script')
<script src="{{asset('js/caja/sendreporte.js')}}"></script>
<script>
      $(document).ready(()=>{
        $('select.select2').select2({
        });

        $('#fiscal').click(function(){

          if($(this).is(':checked')) $(this).val($(this).is(':checked'))
          else $(this).val($(this).is(':checked'))
          
        })
      })
</script>
@endsection