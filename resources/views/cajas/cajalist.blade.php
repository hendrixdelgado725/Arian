@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Cajas')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Listado de Cajas
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
        <div class="card">
          @include('vendor/flash.flash_message')
          <div class="card-header">
            <h3 class="card-title"></h3>
              <div class="card-tools">
                <div class="row">
                	<div class="col-sm-4 mt-2">
                    @can('create', App\permission_user::class)
                        <a href="{{route('cajaCreate')}}" class="btn btn-info mt-2"><b>REGISTRAR</b></a>
                    @endcan
                  </div>
                  <div class="col mt-2">
                    <form action="{{route('buscarCaja')}}" method="GET">
                      @csrf
                      <div class="input-group mt-2">
                        <input type="text" name="filtro" placeholder="Buscar" class="form-control float-right">
                        <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                        <a href="{{route('cajaList')}}" class="btn btn-default"><i class="fas fa-arrow-left"></i></a>            
                      </div>
                    </form>
                  </div>
                </div>
              </div>
          </div>
          <div class="card-body">
            <table class="table text-center">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Nombre</th>
                  <th scope="col">Impresora Fiscal</th>
                  <th scope="col">Host Impresora Fiscal</th>
                  <th scope="col">Serial Impresora</th>
                  <th scope="col">Nro Facturas</th>
                  <!--<th scope="col">Última factura</th>-->
                  <th scope="col">Correlativo</th>
                  <th scope="col">Acci&oacute;n</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($caja as $item)
                  @php
                    $rand  = rand(1, 9999);
                    $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                    $enc=Helper::EncriptarDatos($nrand.'-'.$item->id);
                    /*if ($item->lastfact != null) {
                      $enc2=Helper::EncriptarDatos($nrand.'-'.$item->lastfact->id);
                    }else{
                      $enc2=null;
                    }*/
                  @endphp
                  <tr>
                    <td>{{$caja->firstItem() + $loop->index}}</td>
                    <td>{{$item['descaj']}}</td>
                    <td>{{$item['impfisname']}}</td>
                    <td>{{$item['impfishost']}}</td>
                    <td>{{$item['impserial']}}</td>
                    <td>{{$item['cornotcre']}}</td>
                    <!--<td>
                      {{--@if($enc2 == null)
                        Sin factura
                        @else
                          <a href="{{route('detallesFactura',$enc2)}}">
                            <i class="fa fa-eye blue"></i>
                          </a>
                      @endif--}}
                    </td>-->
                    <td>
                      {{$item['corfac']}}
                    </td>
                    <td>
                      @if (strcmp(auth()->user()->codroles,'R-001') === 0)
                      <a href="{{route('IvaAdm',$enc)}}"><i class="fa fa-percent"></i>
                      </a>
                        /
                      @endif
                      {{--@can('viewAny', App\permission_user::class)--}}
                    <a href="{{route('cajaConf',$enc)}}"><i class="fa fa-cog"></i>
                      {{--@endcan--}}
                      @can('edit', App\permission_user::class)
                    </a>
                      /
                      <a href="{{route('cajaEdit',$enc)}}"><i class="fa fa-edit"></i></a>
                    @endcan
                    @can('delete',App\permission_user::class)
                      /
                      <a id="delete" href="{{route('cajaDelete',$enc)}}"><i class="fa fa-trash red"></i></a>
                    @endcan
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            {!!$caja->appends(request()->input())->render()!!}

              {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section>
@endsection
@section('script')
<script>
  $(document).ready(()=>{
    deletecaja();
      function deletecaja(){//se insertaran paises a la configuracion del sistema siempre y cuando Existan
        $('a#delete').click(function(e){
          e.preventDefault();
          const row = $(this).parents('tr');
          const url = $(this).attr('href');
		  		Swal.fire({
				  title: 'Seguro que Desea Eliminar?',
				  text: "Despues de esto no se encontrara en la lista!",
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, delete it!'
        }).then((result) => {//dio click en el boton aceptar
          if(result.value){
            $.get(url,function(result){
              if (result.exito) {
                Swal.fire(
                  'Caja Eliminada!',
                  'Con Exito.',
                  'success',
                )  
              row.fadeOut();
              }else if(result.error){
                Swal.fire({
                  icon: 'warning',
                  title: result.error,
                  timer:1500
                  })
                }
            })
          }      
        })
		});
    }
  })

</script>
@endsection