@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Estadisticas - Ventas Anuales')
@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      GRAFICOS DE VENTAS ANUALES
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <form action="{{route('buscarGVA')}}" method="GET">
                 @csrf
                  <div class="row form-group">
                   <div class="col mt-2">
                    <select name="sucursales" id="sucursales"  class="form-control select2" data-dropdown-css-class="select2-danger" class="@error('sucursales') is-invalid @enderror" aria-hidden="true">
                      <option value="TODAS" selected>-SUCURSAL-</option>
                       @foreach($sucursales as $sucursal)
                      <option value="{{$sucursal->codsuc}}">{{$sucursal->nomsucu}}</option>
                      @endforeach
                   </select>
                  </div>
                 <div class="col mt-2">
                   <button type="submit" class="btn btn-info"><b> FILTRAR </b></button>
                 </div>
                </div> <!--row -->
               </form>
              </div><!--card tools-->
            </div>   <!-- /.card-header -->
             
              <div class="card-body table-responsive p-0 text-center">

                  {{ $chart->container() }}
                  <script src=https://cdnjs.cloudflare.com/ajax/libs/echarts/4.0.2/echarts-en.min.js charset=utf-8></script>
                  {{ $chart->script() }}
              </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section>

  
@endsection
@section('script')
   <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
    });
   </script>
@endsection



