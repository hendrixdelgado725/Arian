@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Patologias')


@section('content')

<section class="content p-5">
    <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-10">
          <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title P-2"><b>Parametrización</b></h3>

                {{-- <div class="card-tools">
                  <div class="row pr-3">
                      <a href="{{ route('fasvit.registro.patologia') }}" class="btn btn-info"><b>REGISTRAR PATOLOGIA</b></a>                               
                  </div>
                </div> --}}

              </div>
              <!-- /.card-header -->

              <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                  <a class="nav-item nav-link active" id="nav-empresa-tab" data-toggle="tab" href="#nav-empresa" role="tab" aria-controls="nav-empresa" aria-selected="true">Empresa</a>
                  <a class="nav-item nav-link" id="nav-articulos-tab" data-toggle="tab" href="#nav-articulos" role="tab" aria-controls="nav-articulos" aria-selected="false">Articulos</a>
                  <a class="nav-item nav-link" id="nav-moneda-tab" data-toggle="tab" href="#nav-moneda" role="tab" aria-controls="nav-moneda" aria-selected="false">Moneda</a>
                </div>
              </nav>
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-empresa" role="tabpanel" aria-labelledby="nav-empresa-tab">

                    <div class="p-3 col-12">

                        <form>
                            <div class="form-row">
                              <div class="col">
                                <label>Nombre o razón social de la empresa</label>
                                <input type="text" class="form-control" placeholder="Nombre o razón social de la empresa" disabled value="{{ $empresa->nomrazon }}">
                              </div>
                              <div class="col">
                                <label>Rif de la empresa</label>
                                <input type="text" class="form-control" placeholder="Rif de la empresa" disabled value="{{ $empresa->codrif }}">
                              </div>
                              <div class="col">
                                <label>Tipo</label>
                                <input type="text" class="form-control" placeholder="Tipo empresa" disabled value="{{ $empresa->tipemp }}">
                              </div>
                            </div>
                            <div class="form-row pt-2">
                                <div class="col">
                                  <label>Dirección Fiscal</label>
                                  <input type="text" class="form-control" placeholder="Direccion de la empresa" disabled value="{{ $empresa->dirfis }}">
                                </div>
                              </div>
                        </form>

                    </div>

                </div>
                <div class="tab-pane fade" id="nav-articulos" role="tabpanel" aria-labelledby="nav-articulos-tab">

                  <div class="d-flex justify-content-center">
                  
                    <div class="p-3 mt-3 col-8 d-flex justify-content-center shadow p-3 mb-5 ">

                      <parametrizacion-articulos></parametrizacion-articulos>

                    </div>
                
                  </div>
                  
                </div>
                <div class="tab-pane fade" id="nav-moneda" role="tabpanel" aria-labelledby="nav-moneda-tab">Moneda</div>
              </div>



              {{-- <div class="card-body table-responsive text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>ID</th>
                      <th>Especialidad</th>
                      <th>Nombre</th>
                      <th>Monto</th>
                      <th>Acción</th>
                    </tr>
                  </thead>
                  <tbody style="text-align:center">

                
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                          
                        </tr>
               
                      
                  </tbody>
                </table>
             </div> --}}
            
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>

</section>


@endsection

