@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Usuarios')

@section('content')

 @component('layouts.contenth')
    @slot('titulo')
      Listado Usuarios
    @endslot
  @endcomponent

<section class="content">
	<div class="content-fluid">
		<div class="card">
			@include('vendor/flash.flash_message')
			<div class="card-header">
				<h3 class="card-title"></h3>

				<div class="card-tools">
					 <div class="row">
					 	<div class="col-sm-4 mt-2">
					 		@can('create', App\permission_user::class)
					 		<a href="{{route('nuevoUsuario')}}" class="btn btn-info"><b>REGISTRAR</b></a>
					 		@endcan
					 	</div>	

					 	<div class="col mt-2">
					 		<form action="{{route('buscarUsuario')}}" method="POST">
					 			@csrf
					 			<div class="input-group">
					 				<input type="text" name="filtro" placeholder="Buscar" class="form-control float-right">
                      				<button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                      				<a href="{{route('listaUsuario')}}" class="btn btn-default"><i class="fas fa-arrow-left"></i></a>                       
					 			</div>
					 		</form>
					 	</div>
					 </div>
				</div>
			</div>

			<div class="card-body table-responsive p-0 text-center">
				<table class="table table-hover">
					<thead>
					<tr style="text-align:center; font-weight: bold; ">
						<td>ID</td>
                  		<td>Usuario</td>
                  		<td>Cédula Identidad</td>
                  		<td>Nombre</td>
                  		<td>Estatus</td>
                  		<TD>Creado en</TD>
                  		<td>Acci&oacute;n</td>
					</tr>
					</thead>

					<tbody>
						@foreach($usuario as $usu)
							@php
                    			$rand  = rand(1, 9999);
                    			$nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                				$enc=Helper::EncriptarDatos($nrand.'-'.$usu->id);
                  			@endphp
                  			<tr style="text-align:center">
                  				<!-- <td style="text-align:center">{{$usu->id}}</td> -->
                  				<td>{{$usu->id}}</td>
                  				<td>{{$usu->loguse}}</td>
                  				<td>{{$usu->cedemp}}</td>
                  				<td>{{$usu->nomuse}}</td>
              					@if($usu->stablo=='S')
			                    	<td ><i class="fas fa-check"></i></td>
			                    @elseif($usu->stablo=='N')
			                    	<td ><i class="fas fa-ban"></i></td>
			                    @endif
			                    <td>{{$usu->getNombreSucursal()}}</td>
                  				<td>
                  					@can('edit', App\permission_user::class)
                  					    
                  					<div class="button_action">
				                      <a title="Edición" href="{{ route('editarUsuarioForm',$enc)}}"><i class="fa fa-edit blue"></i></a>
                      				</div>
                  					@endcan
                  				</td>
                  			</tr>
						@endforeach
					</tbody>
				</table>
				{!!$usuario->render()!!}
			</div>
		</div>
	</div>
</section>

 @endsection