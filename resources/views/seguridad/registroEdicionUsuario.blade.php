@extends('layouts.app')
@extends('layouts.menu')
@if($accion == 'edicion')
@section('titulo','Edición Usuario')
@else
@section('titulo','Registro Usuario')
@endif
@section('content')
@component('layouts.contenth')
@slot('titulo')
<!-- Usuario -->
@endslot
@endcomponent

<section class="content-wrapper">
  <div class="container-fluid">
    <div class="container">
      <div class="col-10">
        <div class="card card-info">
          @include('vendor/flash.flash_message')
          @if($accion=="registro")
          <div class="card-header">
            <h3 class="card-title"><b>REGISTRO USUARIO</b></h3>
          </div>
          <form action="{{route('registrarUsuario')}}" method="post">
            {{csrf_field()}}
            <div class="card-body">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Número de documento</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">V/J</span>
                      </div>
                      <input type="text" class="form-control" placeholder="Ejem: 1234567" name="cedularif" id="cedularif" onKeypress="javascript:return SoloNumeros(event)" class="@error('cedularif') is-invalid @enderror" onblur="buscarDatos(this.value,1); verificarUsuario(this.value);">
                    </div>
                    @error('cedularif')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="form group">
                    <label>Nombre</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">A</span>
                      </div>
                      <input type="text" class="form-control" placeholder="Ejem: María Gutíerrez" name="nombre" id="nombre" onkeyup="pasarMayusculas(this.value, this.id)" class="@error('nombre') is-invalid @enderror">
                    </div>
                    @error('nombre')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="form group">
                    <label for="direccion">Dirección</label>
                    <div class="input-group mb-3">
                      <textarea type="text" onkeyup="pasarMayusculas(this.value, this.id)" class="form-control" class="@error('direccion') is-invalid @enderror" placeholder="Habitacional..." name="direccion" id="direccion"> </textarea>
                    </div>
                    @error('direccion')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Teléfono</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-phone-square"></i></span>
                      </div>
                      <input type="text" class="form-control" placeholder="Ejem: 0412000000" name="telefono" id="telefono" onKeypress="javascript:return SoloNumeros(event)" class="@error('telefono') is-invalid @enderror">
                    </div>
                    @error('telefono')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="form group">
                    <label for="email">Correo Electrónico</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                      </div>
                      <input type="email" class="form-control" class="@error('email') is-invalid @enderror" placeholder="Ejem: Maria@email.com" name="email" id="email">
                    </div>
                    @error('email')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Asociación</label>
                    <div class="table-wrapper-scroll-y my-custom-scrollbar">
                      <table class="table table-bordered ">
                        <thead>
                          <th>N°</th>
                          <th>Sucursal</th>
                        </thead>
                        <tbody>
                          @for ($i = 0; $i < count($sucursal); $i++) <tr data-row="{{$i}}">
                            <td>{{$i}}</td>
                            <td width="100%">
                              <select data-row="{{$i}}" class="form-control select2" name="sucur[]" data-dropdown-css-class="select2-danger" caria-hidden="true">
                                <option selected="selected" data-select2-id="" disabled>Seleccione Sucursal Asociar</option>
                                @foreach ($sucursal as $sc)
                                <option value="{{$sc["codsuc"]}}">{{$sc["nomsucu"]}}</option>
                                @endforeach
                              </select>
                            </td>
                            </tr>
                            @endfor
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Estatus</label>
                    <div class="input-group mb-3">
                      <select style="width:100%" class="form-control " id="estatus" name="estatus" tabindex="-1" required="required" value="">
                        <option value='S'>ACTIVO</option>
                        <option value='N'>INACTIVO</option>
                      </select>
                    </div>
                    @error('estatus')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form group">
                    <label for="nombre">Fecha Inactivo</label>
                    <div class="input-group mb-3">
                      <input type="date" class="form-control" class="@error('fecha') is-invalid @enderror" name="fecha" id="fecha">
                    </div>
                    @error('fecha')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Usuario</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                      </div>
                      <input id="usuario" type="text" name="usuario" class="form-control" onkeyup="pasarMayusculas(this.value, this.id)" placeholder="Ejem: MGUTIERREZ" onblur="verificarLogin(this.value)">
                    </div>
                    @error('usuario')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Roles</label>
                    <div class="input-group mb-3">
                      <select style="width:100%" class="form-control select3 roles" id="rol" name="rol" tabindex="-1" required="required" value="">
                        <option selected="selected" data-select2-id="" disabled>Seleccione rol del usuario</option>
                        @foreach ($roles as $rol)
                        <option value="{{$rol["codroles"]}}">{{$rol["nombre"]}}</option>
                        @endforeach
                        @error('rol')
                        <div class="alert alert-danger">{!!$message!!}</div>
                        @enderror
                      </select>
                     
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="Password" class="col-sm-4 control-label">Contraseña</label>
                  <div class="input-group col-sm-12">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-key"></i></span>
                    </div>
                    <input id="password" type="password" name="password" class="form-control" title="Usar una contraseña mayor o igual 6 digitos." placeholder="Contraseña">
                    @error('password')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    <span>&nbsp; &nbsp; &nbsp; &nbsp;</span>
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-key"></i></span>
                    </div>
                    <input id="password_confirmation" type="password" name="password_confirmation" class="form-control" data-toggle="tooltip" data-placement="top" title="Usar una contraseña mayor o igual a 6 digitos." placeholder="Confirmar contraseña">
                    @error('password_confirmation')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                  </div>
                </div>
              </div>

            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-success" class="verificarpasswd">Guardar Cambios</button>
              <a href="{{route('listaUsuario')}}" class="btn btn-default retorno"><i class="fa fa-chevron-circle-left"></i> Volver</a>
            </div>
          </form>
          {{-- EDICION DE USUARIO--}}
          @elseif ($accion=="edicion")
          <div class="card-header">
            <h3 class="card-title"><b>EDICIÓN USUARIO</b></h3>
          </div>
          @php $id=$usuario->id.'|'.$usuario->codigoid; @endphp
          <form action="{{route('actualizarusuarios',$id)}}" method="POST">
            {{csrf_field()}}
            <div class="card-body">
              <div class="row">
                <div class="col-sm-2">
                  <div class="form group">
                    <label for="id">ID</label>
                    <div class="input-group mb-3">
                      <input type="text" class="form-control" disabled=true value="{{$usuario->id}}" class="@error('codigo') is-invalid @enderror" name="codigo" id="codigo">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form group">
                    <label>Número de documento</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">V/J</span>
                      </div>
                      <input type="text" class="form-control" disabled=true value="{{$usuario->cedemp}}" class="@error('cedularif') is-invalid @enderror" name="cedularif" id="cedularif">
                    </div>
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="form group">
                    <label for="id">Nombre</label>
                    <div class="input-group mb-3">
                      <input type="text" class="form-control" class="@error('nombre') is-invalid @enderror" name="nombre" id="nombre" value="{{$usuario->nomuse}}" onKeypress="javascript:return NumerosReales(event)" autocomplete="off" maxlength="17">
                    </div>
                    @error('nombre')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="form group">
                    <label for="direccion">Dirección</label>
                    <div class="input-group mb-3">
                      <textarea type="text" onkeyup="pasarMayusculas(this.value, this.id)" class="form-control" class="@error('direccion') is-invalid @enderror" name="direccion" id="direccion">{{$usuario->diremp}} </textarea>
                    </div>
                    @error('direccion')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Teléfono</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-phone-square"></i></span>
                      </div>
                      <input type="text" class="form-control" name="telefono" id="telefono" onKeypress="javascript:return SoloNumeros(event)" class="@error('telefono') is-invalid @enderror" value="{{$usuario->telemp}}">
                    </div>
                    @error('telefono')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="form group">
                    <label for="email">Correo Electrónico</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                      </div>
                      <input type="email" class="form-control" class="@error('email') is-invalid @enderror" name="email" id="email" value="{{$usuario->emaemp}}">
                    </div>
                    @error('email')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Asociación</label>
                    <div class="table-wrapper-scroll-y my-custom-scrollbar">
                      <table class="table table-bordered">
                        <thead>
                          <th>N°</th>
                          <th>Sucursal</th>
                          <th>Desactivar</th>
                        </thead>
                        <tbody>
                          {{-- Sucursales asignadas --}}

                          @for ($i = 0; $i < count($ususucursal); $i++) @php $selected=[]; $su='' ; @endphp <tr>
                            <td>
                              <input type="hidden" class="form-control id" value="{{$ususucursal[$i]->id}}">
                              {{$i}}
                            </td>
                            <td width="100%">
                              @php
                                  //dd($ususucursal[$i]->SucursalUse()->codsuc);
                              @endphp
                              <select name="sucur[]" class="form-control select2 sucursal">
                                <option value="{{$ususucursal[$i]->SucursalUse()->codsuc ?? ''}}">{{$ususucursal[$i]->SucursalUse()->nomsucu ?? ''}}</option>
                              </select>
                            </td>
                            <td style="text-align:center">
                              <a id="deleterow" class="deleterow" data-target-artid="{{$ususucursal[$i]->id}}" href=""><i class="fa fa-trash"></i></a>
                            </td>
                            </tr>
                            @endfor

                            {{-- Sucursales no asignadas --}}
                            @for (; $i<(count($sucursal)); $i++) <tr>
                              <td>
                                {{$i}}
                              </td>
                              <td width="100%">
                                <select data-row="{{$i}}" name="sucur[]" class="form-control select2 sucursal">
                                  <option value="">Seleccione Sucursal Asociar</option>
                                  @foreach ($sucursal as $sc)
                                  <option value="{{$sc["codsuc"]}}">{{$sc["nomsucu"]}}</option>
                                  @endforeach
                                </select>
                              </td>
                              <td></td>
                              </tr>
                              @endfor
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Estatus</label>
                    <div class="input-group mb-3">
                      <select style="width:100%" class="form-control select2 required" id="estatus" name="estatus" tabindex="-1" required="required" value="{{$usuario->stablo}}">
                        @if ($usuario->stablo=="S")
                        <option value="S" selected="selected">ACTIVO</option>
                        <option value="N">INACTIVO</option>
                        @elseif ($usuario->stablo=="N")
                        <option value="S">ACTIVO</option>
                        <option value="N" selected="selected">INACTIVO</option>
                        @else
                        <option value="S">ACTIVO</option>
                        <option value="N" selected="selected">INACTIVO</option>
                        @endif
                      </select>
                    </div>
                    @error('estatus')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form group">
                    <label for="nombre">Fecha Inactivo</label>
                    <div class="input-group mb-3">
                      <input type="date" class="form-control" class="@error('fechainactivo') is-invalid @enderror" name="fecha" id="fechainactivo" value="{{$usuario->feccad}}">
                    </div>
                    @error('fecha')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Usuario</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                      </div>
                      <input id="usuario" type="text" name="usuario" disabled=true class="form-control" onkeyup="pasarMayusculas(this.value, this.id)" placeholder="Ejem: MGUTIERREZ" onblur="verificarLogin(this.value)" value="{{$usuario->loguse}}">
                    </div>
                    @error('usuario')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Roles</label>
                    <div class="input-group mb-3">
                      <select class="form-control select3" value="{{$usuario->codroles}}" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="rol" required="">

                        @if($usuario->codroles!=NULL)
                        <option value="{{$usuario->codroles}}" selected="selected">{{$usuario->role->nombre ?? 'N/A'}}</option>
                        @else
                        <option selected="selected" data-select2-id="" disabled>Seleccione rol del usuario</option>
                        @endif
                        {{--@php dd($droles); @endphp--}}
                        @foreach ($droles as $rl)
                        <option value="{{$rl->codroles}}">{{$rl->nombre}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="Password" class="col-sm-4 control-label">Contraseña</label>
                  <div class="input-group col-sm-12">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-key"></i></span>
                    </div>
                    <input id="password" type="password" name="password" class="form-control" title="Usar una contraseña mayor o igual 6 digitos." placeholder="Contraseña">
                    @error('password')
                    <div class="alert alert-danger" value={{$usuario->pasuse}}>{!!$message!!}</div>
                    @enderror
                    <span>&nbsp; &nbsp; &nbsp; &nbsp;</span>
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-key"></i></span>
                    </div>
                    <input id="password_confirmation" type="password" name="password_confirmation" class="form-control" data-toggle="tooltip" data-placement="top" title="Usar una contraseña mayor o igual a 6 digitos." placeholder="Confirmar contraseña">
                    @error('password_confirmation')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                  </div>
                </div>
              </div>

              <div>
                <input id="borrados" type="hidden" name="borrados">
              </div>

              <div class="card-footer">
                <button type="submit" class="btn btn-success">Guardar Cambios</button>
                <a href="{{route('listaUsuario')}}" class="btn btn-default retorno"><i class="fa fa-chevron-circle-left"></i> Volver</a>
              </div>
          </form>

          @endif
        </div>
      </div>
    </div>
    @if($accion=='edicion')
    <input id="sucujson" type="hidden" value="{{$sucujson}}">
    <!-- <input id="ususucvalues" type="hidden" value="{{--$ususucursal--}}"> -->
    @endif
  </div>
</section>

@endsection

@section('script')
<script src="{{asset('js/chequeo.js')}} "></script>
<script src="{{asset('js/funciones.js')}} "></script>
<script type="text/javascript">
  $(document).ready(function() {
    // inicializamos el plugin
    $('select.select2').select2({});
    $('select.select3').select2({});
  });

</script>
@endsection

<style>
  .my-custom-scrollbar {
    position: relative;
    height: 180px;
    overflow: auto;
  }

  .table-wrapper-scroll-y {
    display: block;
  }

</style>
<!-- @section('js')
@parent
@endsection -->
