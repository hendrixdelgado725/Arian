@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Editar Contraseña de Usuario')
  @section('content')
    @component('layouts.contenth')
      @slot('titulo')
        Editar Contraseña
      @endslot
@endcomponent

    <section class="content-wrapper">
      <div class="container-fluid">
        <div class="container">
          <div class="col-10">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title"><b>EDITAR CONTRASEÑA</b></h3>
              </div>
              @php $id=$usuario->id.'|'.$usuario->codigoid; @endphp
              <form action="{{route('actualizarPassw',$id)}}" method="post">
                {{csrf_field()}}
                <div class="card-body">
                  @include('vendor/flash.flash_message')
                  <div class="row">
                    
                    <div class="col-sm-6">
                      <div class="form group">
                        <label for="id">Usuario</label>
                        <div class="input-group mb-3">
                          <input type="text" class="form-control" disabled=true value="{{$usuario->loguse}}" class="@error('usuario') is-invalid @enderror" name="usuario" id="usuario">
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="Password" class="col-sm-4 control-label">Contraseña</label>
                      <div class="input-group col-sm-12">
                      <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fa fa-key"></i></span>
                      </div>
                      <input id="password" type="password" name="password" class="form-control" title="Usar una contraseña mayor o igual 6 digitos." placeholder="Contraseña" >
                      @error('password')
                        <div class="alert alert-danger" value={{$usuario->pasuse}}>{!!$message!!}</div>
                      @enderror
                      <span>&nbsp; &nbsp; &nbsp; &nbsp;</span>
                      <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fa fa-key"></i></span>
                      </div>
                      <input id="password_confirmation" type="password" name="password_confirmation" class="form-control" data-toggle="tooltip" data-placement="top" title="Usar una contraseña mayor o igual a 6 digitos." placeholder="Confirmar contraseña">
                      @error('password_confirmation')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                      </div>
                    </div>

                  </div>  
                </div>

                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Guardar Cambios</button>
                  <!-- <a href="{{}}" class="btn btn-default retorno" ><i class="fa fa-chevron-circle-left"></i> Volver</a> -->
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  @endsection