@extends('layouts.app')
@extends('layouts.menu')

@section('titulo','Arian - Principal')
@section('content')
<!-- <img src="/images/Wallpaper4v3.png" class="mt-3 mb-1 wall" width="1200" height="650" style="border-radius: 25px;-box-shadow: 0 0 8px 8px rgba(0, 0, 0, 0.1);"> -->
 <body>
<div style="background-position: center;
  background-repeat: no-repeat;
  background-size: contain;">
 <img src="/images/arian/principal/Fondo-4.png" height="900px" style="background-size: cover;" srcset="/images/arian/principal/Fondo-4.png"> 
<div class="container">

         
    <div class="row justify-content-center">
        <div class="col-md-8">
                @if(Session::has('flash_message'))
                
            <div class="card">

                  <div class="card-body">
                    {{-- @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif --}}

                   <h1>
                <div class="alert alert-light" role="alert">
                  {{-- {{Session::get('flash_message')}} --}}

                </div>
                
               
                </h1>
                   </div>
            </div>
                    @endif
      
        </div>
    </div>
    <div class="faded-img">
    
    </div>
</div>
</div>
</body>
@endsection

@section('script')
<script src="{{ asset('js/script.js') }}" ></script>
<link href="{{ asset('/css/home.css') }}" rel="stylesheet">
@endsection

