@extends('layouts.app')
@extends('layouts.menu')
@section('titulo', 'Centros de costo')
@section('content')
@component('layouts.contenth')
@slot('titulo')
  Listado de Turnos
  @endslot
@endcomponent
<section class="content">
  <div class="content-fluid">
    <div class="card">
      @include('vendor/flash.flash_message')
      <div class="card-header">
        <h3 class="card-title"></h3>
        <div class="card-tools">
          
          <div class="row">
            <div class="col-sm-4 mt-2">
                @can('create', App\permission_user::class)
                <a href="{{ route('TurnosCreate') }}" class="btn btn-info"><b>REGISTRAR</b></a>    
                @endcan
                </div>

                <div class="col mt-2">
                  <form action="{{ route('filtroTurnos') }}" method="GET">
                      @csrf
                      <div class="input-group">
                        <input type="text" name="filtro" placeholder="Buscar" class="form-control float-right">
                        <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                        <a href="{{URL::previous()}}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                       
                      </div>
                  </form>
                </div>
              </div>
        </div>
      </div>

      <div class="card-body table-responsive p-0 text-center">
        <table class="table table-hover">
          <thead>
            <tr style="text-align:center; font-weight: bold;">
              <td>ID</td>
              <td>Numero Turno</td>
              <td>Caja</td>
              <td>Cajero</td>
              <td>Tipo Turno</td>
              <td>Supervisor</td>
              <td>Fecha Inicio</td>
              <td>Estatus</td>
              <td>Acciones</td>
            </tr>
          </thead>
          <tbody>
            @foreach($turno as $tur)
                <tr style="text-align:center;">
                    <td> {{ $tur->id }} </td>
                    <td> {{ $tur->numtur }} </td>
                    <td> {{ $tur->Caja->descaj ?? 'N/A'}} </td>
                    <td> {{ $tur->Cajero->usuarios->nomuse ?? 'N/A'}} </td>
                    <td> {{ $tur->tipoturno }} </td>     
                    <td> {{ $tur->Supervisor->nomuse }} </td>
                    <td> {{ $tur->inicio }} </td>
                    <td> {{ $tur->estatus ? 'Activo' : 'Inactivo'; }} </td>     
                    <td>
                      
                      
                      
                        <a href="{{route('TurnosEdit', $tur->id) }}"><i class="fas fa-edit"></i></a>
                      
     
                      
                      
                        <a class="borrar" href="#" data-url='{{route('TurnosDestroy', $tur->id)}}'><i class="fas fa-trash"></i></a>
                      
                 
                    </td>
              </tr>
            @endforeach
          </tbody>
        </table>
        {!!$turno->appends(request()->input())->render()!!}
      </div>
    </div>
    
  </div>


@endsection
@section('script')
<script type="text/javascript">

$('.borrar').click(function(e){
          e.preventDefault();
            Swal.fire({
            title: '¿Seguro que desea eliminar este Articulos?',
            text: "¡Despues de esto no se encontrara en la lista!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar'
          }).then((result) => {
              if (result.value == true) {
              var row = $(this).parents('tr');
              //var form = $(this).parents('data-id');
              var url = $(this).data('url');
              
              
              $.get(url,function(result) {
                  
                if (result.exito) {
                      Swal.fire(
                        'SE HA ELIMINADO CON EXITO',
                        '.',
                        'success',
                      );
                      row.fadeOut();
                }else if(result.error){
                  Swal.fire({
                    icon: 'warning',
                    title: 'No se puede eliminar por que esta caja tiene factura realizada',
                    timer:3000
                  })
                }

              })   
              }  //para cancelar el sweetalert2
          });

  });


 </script>
@endsection