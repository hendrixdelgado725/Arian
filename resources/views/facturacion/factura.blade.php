@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Facturas')
@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Lista de Facturas
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
               
                 <div class="row-2">
                 <div class="col" class="d-flex justify-content-center">
<!--                   @can('create', App\permission_user::class)
                   <a href="{{route('nuevaTasa')}}" class="btn btn-info mt-2"><b> REGISTRAR NUEVA TASA</b></a>   
                  @endcan -->
                   
                  </div>

                <div class="col">
                  <form action="{{route('filtroFactura')}}" method="GET">
                    @csrf
                     <div class="input-group mt-2">
                      <input type="text" name="filtro" placeholder="Buscar Factura..." class="form-control float-right">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                      <a href="{{ URL::previous() }}" class="btn btn-default"><i class="fas fa-arrow-left"></i></a>                       
                     </div>
                  </form>
                </div><!--col-->
                 </div><!--row -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>N° FACTURA</th>
                      <th>CLIENTE</th>
                      <th>CAJA</th>
                      <th>SUCURSAL</th>
                      <th>ESTATUS</th>
                      <th>CREADO EL</th>
                      <th>ACCIÓN</th>
                    </tr>
                  </thead>
                  <tbody style="text-align:center"> 
                    @foreach($facturas as $factura)
                    @php
                    $rand  = rand(1, 9999);
                    $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                    $enc=Helper::EncriptarDatos($nrand.'-'.$factura->id);
                    @endphp
                    <tr>
                      <td>{{$factura->reffac}}</td>
                      <td>{{$factura->cliente->nompro}}</td>
                      <td>{{optional($factura->caja)->descaj ?? $factura->defcaja->descaj}}</td>
                      <td>{{optional($factura->sucursales)->nomsucu ?? 'SIN SUCURSAL'}}</td>
                      @if($factura->status=='A')
                      <td class="status">ACTIVA</td>
                      @else
                      <td>ANULADA</td>
                      @endif
                      <td>{{$factura->created_at ? date("d-m-Y", strtotime($factura->created_at)) : date("d-m-Y", strtotime($factura->fecfac)) }}</td>
                      <td class="acciones"> 
                       
                        @if($factura->status == 'A')
                        <a class="anular" data-id="{{$enc}}" href="{{route('anularFactura2',$enc)}}">
                          <i class="fa fa-ban blue"></i>
                        </a> /
                         @endif
                           <a href="{{route('detallesFactura',$enc)}}">
                          <i class="fa fa-eye blue"></i>
                        </a>
                        @can('edit', App\permission_user::class)
                        @endcan
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!!$facturas->appends(request()->input())->render()!!}
               {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section> 
  
  
@endsection
@section('script')

<script src="{{asset('js/factura.js')}} "></script>
@endsection




