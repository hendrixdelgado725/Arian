@extends('layouts.app3')
{{-- @extends('layouts.menu') --}}
@section('titulo', 'Nota Entrega Facturación')
@section('content')
@component('layouts.contenth')
    @slot('titulo')
        {{-- Registrar Nota de Entrega de Facturación --}}
    @endslot
@endcomponent

<fnotaentrega
:user="{{ json_encode($user) ?? json_encode('{}') }}"
:tasa="{{ json_encode($tasa) }}"
:articulos="{{ json_encode($articulos) }}"
:recargos="{{ json_encode($recargos) }}"
:descuentos="{{ json_encode($descuentos) }}"
:monedas="{{ json_encode($monedas) }}"
:bancos="{{ json_encode($bancos) }}"
:cajas="{{ json_encode($cajas) }}"/>






@endsection