@extends('layouts.app')
@extends('layouts.menu')
@section('titulo', 'Nota Entrega Facturación')
@section('content')
@component('layouts.contenth')
@slot('titulo')
Detalles de nota de entrega
@endslot
@endcomponent
    <fnotashow :nota="{{ json_encode($nota) }}"></fnotashow>
@endsection