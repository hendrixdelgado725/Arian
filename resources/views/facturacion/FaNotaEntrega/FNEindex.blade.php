@extends('layouts.app')
@extends('layouts.menu')
@section('titulo', 'Nota Entrega Facturacion')
@section('content')
@component('layouts.contenth')
@slot('titulo')
  Listado Notas de Entrega de Facturación
  @endslot
@endcomponent
<section class="content">
  <div class="content-fluid">
    <div class="card">
      @include('vendor/flash.flash_message')
      <div class="card-header">
        <h3 class="card-title"></h3>
        <div class="card-tools">
          
          <div class="row">
            <div class="col-sm-4 mt-2">
                @can('create', App\permission_user::class)
                <a href="{{ route('FNECreate') }}" class="btn btn-info" target="_blank"><b>REGISTRAR</b></a>    
                @endcan
                </div>

                <div class="col mt-2">
                  <form action="" method="GET">
                      @csrf
                      <div class="input-group">
                        <input type="text" name="filtro" placeholder="Buscar" class="form-control float-right" value="">
                        <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                        <a href="" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                       
                      </div>
                  </form>
                </div>
              </div>
        </div>
      </div>

      <div class="card-body table-responsive p-0 text-center">
        <table class="table table-hover">
          <thead>
            <tr style="text-align:center; font-weight: bold;">
              <td>Código</td>
              <td>Fecha</td>
              <td>Cliente</td>
              <td>Estatus</td>
              <td>Monto</td>
              <td>Acción</td>
            </tr>
          </thead>
          <tbody>
            @if($notas->isEmpty())
                <td> NO HAY REGISTROS</td>
            @endif
            @foreach($notas as $nota)
              <tr style="text-align:center;">
                <td>{{ $nota->codnota }}</td>
                <td>{{ $nota->fecnota }}</td>
                <td>{{ $nota->cliente->nompro }}</td>
                <td>{{ $nota->status }} </td>
                <td>{{ $nota->monto }}</td>
                <td>
                  @can('edit',App\permission_user::class)
                  <a href="{{ route('FNEshow', $nota->id) }}" data-toggle="tooltip" data-placement="top" title="Proforma"> 
                  <i class="fa fa-list"></i>
                  </a>
                  /
                  <a target='_blank' href="{{route('FNEpdf', $nota->codnota)}}" data-toggle="tooltip" data-placement="top" title="PDF"><i class="fa fa-file-pdf"></i></a>
                    </a>
                  @endcan
                </td>
              </tr>
              @endforeach
          </tbody>
        </table>
         {!!$notas->appends(request()->input())->render()!!}
      </div>
    </div>
    
  </div>


@endsection
@section('script')
<script type="text/javascript">

 </script>
@endsection