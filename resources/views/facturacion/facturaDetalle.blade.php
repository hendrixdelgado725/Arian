@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Detalle de Factura')
@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <!-- Main content -->
      <div class="invoice p-1 mb-3">
        <!-- Encabezado Enmarcado-->
        <div class="border border-dark">
          <div class="col-11 d-flex justify-content-center" style="margin-bottom: -5px;">
            
            @if ($factura->is_fiscal === true || $factura->is_fiscal === null)
              <h4>FACTURA N° {{$factura->reffac}}</h4>  
            @else  
            <h4>TICKERA N°   {{$factura->reffac}}</h4>  
            @endif
          </div>
          <div class="col-11 d-flex justify-content-center" style="margin-bottom: -5px;">
           @if($factura->status == 'A')
            <h6><b>ESTATUS: ACTIVO</b></h6>
            @else
            <h6><b>ESTATUS: ANULADO </b></h6>
           @endif
          </div>
        </div>
        <!-- /. Fin del Encabezado Enmarcado -->

        <!-- Encabezado-->
        <!-- Fin del Encabezado-->

        <div class="row invoice-info">
          <div class="col-sm-6 invoice-col border-left">
            <div class="col-12 mt-4">
             <b>CLIENTE:</b> {{$factura->cliente->nompro }}
            </div>
            <div class="col-12 mt-4">
             <b>CEDULA / RIF:</b> {{$factura->codcli}}
            </div>
          </div>
          <!-- /.col -->
          @php
          $fanotcre = \App\Fanotcre::where('reffac',$factura->reffac)->first();
          
        @endphp
          @if($noEncontrado && $factura->motanu === null && ($factura->is_fiscal === true || $factura->is_fiscal === false))
         <div class="col-sm-6 invoice-col border-left">
          <div class="col-12 mt-4">
           <button class="btn btn-primary imprime" data-fac="{{route('printFactura')}}" data-detalles="{{$factura}}">IMPRIMIR FACTURA</button>
           <input type="text" name="_token" style="display: none;" value="{!!csrf_token()!!}">

          </div>
        </div>
        @endif
        
        @if($factura->status === 'N' && $fanotcre === null)
        <div class="col-sm-6 invoice-col border-left">
         <div class="col-12 mt-4">
          
          <label>MOTIVO DE ANULACION: {{strtoupper($factura->motanu)}}</label>

         </div>
       </div>
       @endif
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table Primary -->
      <div class="row">
        <div class="col-12 mt-4 table-responsive" style="margin-bottom:-10px;">
          <table class="table table-sm">
            <thead>
              <tr style="text-align:center">
                <th>CREADO EL</th>
                <th>ELABORADO POR</th>
              </tr>
              <tr style="text-align:center">
                <td>{{$factura->created_at ? date("d-m-Y", strtotime($factura->created_at)) : date("d-m-Y", strtotime($factura->fecfac))}}</td>
                <td>{{$factura->reapor}}</td>
              </tr>
            </thead>
          </table>
        </div>
      </div>

       <div class="row" style="margin-top: -10px;">
       <div class="col-12 mt-4 table-responsive">
        <div class="table-header">
          <h6 class="table-title m-0 mt-4 mb-4" style="text-align:center"><b>FORMAS DE PAGO</b></h6>
          </div>
          <table class="table table-sm">
            <thead>
              <tr style="text-align:center">
                <th>PAGO</th>
                <th>BANCO</th>
                <th>CRÉDITO</th>
                <th>MONTO</th>
                <th>REF.</th>
              </tr>
              @foreach ($factura->pagos as $pago)
              <tr style="text-align:center">
                <td>{{$pago->fpago ? $pago->fpago : $pago->tipopago->destippag}}</td>
                <td>{{$pago->nomban}}</td>
                <td>{{$pago->tipocredito?? 'NINGUNO'}}</td>
                <td>{{number_format($pago->monpag, 2, ',', '.')}}</td>
                <td>{{$pago->refbillete ?? $pago->numero}}</td>
              </tr>
              @endforeach
            </thead>
          </table>
        </div>
      </div>

      <!-- Table Secundary -->
      <div class="row" style="margin-top: -10px;">
       <div class="col-12 mt-4 table-responsive">
        <div class="table-header">
          <h6 class="table-title m-0 mt-4 mb-4" style="text-align:center"><b>ARTÍCULOS</b></h6>
          </div>
          <table class="table table-sm">
            <thead>
              <tr style="text-align:center">
                <th>CÓDIGO</th>
                <th>PRODUCTO</th>
                <th>IVA</th>
                <th>CANTIDAD</th>
                <th>DESCUENTO</th>
                <th>PORCENTAJE DESCUENTO</th>
                <th>DIVISA</th>
                <th>MONTO UNIT. BS</th>
                <th>MONTO TOTAL BS</th>
                <th>SERIALES</th>
              </tr>
              {{$montsub=0 }}
             
              @foreach ($factura->articulos as $articulo)
              <tr style="text-align:center">
                
                <td>{{$articulo->codart}}</td>
                <td>{{$articulo->desart}}</td>
                <td>@if($articulo->articulo->ivastatus === "true")
                    <i class="fas fa-check"></i>{{ $factura->porrecargo }}
                    @else
                    <i class="fas fa-ban"></i>
                    @endif
                </td>
                <td>{{$articulo->cantot}}</td>
               
                <td>{{$articulo->mondes}}</td>
                <td>{{optional($articulo)->Descuento->mondesc ?? 'N/A'}}</td>
                <td>{{$articulo->predivisa}}</td>
                <td>{{number_format($articulo->precio, 2, ',', '.')}}</td>
                @if($articulo->articulo->ivastatus === "true")
                {{-- muestra el total con iva --}}
                <td>{{number_format($articulo->totart, 2, ',', '.')}}</td>
                @else
                {{-- muestra el total sin iva --}}
                <td>{{number_format($articulo->precio * $articulo->cantot, 2, ',', '.')}}</td>
                @endif
                

                <td ><a href="#"  id="showSerial" data-codart="{{$articulo->codart}}" data-factura="{{$factura->reffac}}"><i class="fa fa-book"></i></a></td>
              </tr>
              {{$montsub+=$articulo->precio}}
              
              @endforeach
            </thead>
          </table>
        </div>
      </div>


      <div class="col-5 mt-4 mb-4" style="margin-left: 60%; margin-top: -20px;">
        <div class="table-responsive " >
          <table class="table table-sm " >
            
            <tbody>
              <tr>
                <th>Sub-Total Bs.</th>
                <td><strong>{{number_format($montsub, 2, ',', '.')}}</strong></td>
              </tr>
              @if ($factura->mondesc)
              <tr>
                <th>Monto descuento</th>
                <td><strong>{{number_format($factura->mondesc, 2, ',', '.')}}</strong></td>
              </tr>
              @endif
              <tr>
                <th>IVA ({{$factura->porrecargo ?? $factura->fargoarticulo->tiporecargo->monrgo ?? 'Sin recargo'}})</th>
                <td><strong>{{number_format($monrec, 2, ',', '.')}}</strong></td>
              </tr>
              <tr>
                <th>Total Bs. S</th>
                <td><strong>{{number_format($factura->monfac, 2, ',', '.')}}</strong></td>
              </tr>
            </tbody>
            
          </table>
          </div>
        </div>
        <div class="row no-print" style="margin-top: -20px; margin-bottom: -5px;">
          <div class="col-12 mb-4">
           <a href="{{ URL::previous() }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
        </div>
        </div>
      </div>
      <!-- this row will not appear when printing -->
    <!--   <div class="row no-print">
        <div class="col-12 mb-4">
        <a href="{{ URL::previous() }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>

        </div>
      </div> -->
   </div>
   <!-- /.invoice -->
 </div>
 <div class="modal fade" id="modalSerial" tabindex="-1" role="dialog" aria-labelledby="notaM" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
   <div class="modal-content"> 
   <div class="modal-header">
    <h5 class="modal-title" style="text-align:center" id="producto"><b>SERIALES DEL PRODUCTO </b></h5>
    
   </div>
    <div class="modal-body content-centered">
     

         <div class="card-body table-responsive p-0"  style="height: 150px;">
               <table class="table table-head-fixed">
                 <thead>
                   <tr style="text-align:center">
                      
                     <th>SERIAL</th>    
                     <th>FACTURADO</th>                     
                   </tr>
                 </thead>
                 <tbody id="listaSeriales">
                 
                 </tbody>
               </table>
             </div>

        
   </div> <!--modal body -->

  <div class="modal-footer">
   <button type="button" class="btn btn-danger" data data-dismiss="modal" ><b> Cerrar</b></button>
  </div>
</div>
</div>
</div>
</div>
@endsection
@section('script')
<script>

  $(document).ready(function() {
          $(document).on('click','#showSerial',async function(e) {
              var codart = $(this).data('codart');
              var factura = $(this).data('factura');
            try {
              var serial = await fetch("{{route('seriales')}}",{
                method:'POST',
                body: JSON.stringify({
                  codart,
                  factura,
                  "_token":"{{csrf_token()}}"
                }),
                headers:{
                  'Content-Type': 'application/json',
                  
                }
              });
              var data = await serial.json();

              var row = '';
             console.log(data.seriales)
              if(data.seriales === "NO TIENE SERIALES REGISTRADO!")  throw("NO TIENE SERIALES REGISTRADO!")

              if(data.seriales.length !== 0){
                //la propiedad facturado trae del campo deleted_at de la tabla serial para validar si no esta devuelto la factura
                data.seriales.map(res =>(res.facturado !== null) ? 
                row+=`<tr>
                <td align="center">${res.serial !== null ? res.serial : "NO TIENE SERIAL"}</td><td align="center"><i class="fa fa-ban"></i></td></tr>` : row+=`<tr><td align="center">${res.serial}</td><td align="center"><i class="fa fa-check"></i></td></tr>`)

              }
              
              $('#listaSeriales').html(row)
              $('#modalSerial').modal('show')

            } catch (error) {
                Swal.fire({
                    icon  :'error',
                    title:error,
                    toast : true
                  })
            }
           
              


             
          })


          $(document).on('click','.imprime',function(){
            let {reffac,codcaj,is_fiscal} = $(this).data('detalles')
            $('.imprime').attr('disabled',true)

              $.ajax({
               url:$(this).data('fac'),
               type:'POST',
               dataType : 'json',
               data:{
                  _token:$('input[name="_token"]').val(),
                  reffac:reffac,
                  isFiscal:is_fiscal,
                  caja:codcaj
              },
             }).then(async (data) => {
              console.log(data);

              
              await Swal.fire({
                    icon: 'success',
                    title: "Impreso con Exito",
                    showConfirmButton: false,
                    timer: 1200
                  })
                 // window.location.reload();  
             }).catch(async (error)=> {
              
              await Swal.fire({
                    icon: 'warning',
                    title: error['responseJSON']['msg'],
                    showConfirmButton: false,
                    timer: 1200
                  })
                  $('.imprime').attr('disabled',false)
              })

              //
          })
  })

</script>
@endsection
