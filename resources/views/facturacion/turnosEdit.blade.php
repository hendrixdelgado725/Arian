@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Turnos')


@section('content')

<section class="content pt-5">

   <form-turnos type="{{ $type }}"
   :turno="{{ json_encode($turno) ?? json_encode('{}') }}"></form-turnos>

</section>


@endsection
