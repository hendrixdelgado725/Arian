@extends('layouts.app')
@extends('layouts.menu')
@section('titulo', 'Edición Centro de Costo')

@section('content')
@component('layouts.contenth')
    @slot('titulo')
        Edición de Proveedores
    @endslot
@endcomponent

<section class="content-wrapper">
    <div class="container-fluid">
    <div class="container">
        
        <div class="col-10">
            <!-- general form elements -->
            <div class="card card-info">
              @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title mt-2"><b>EDICION DE PROVEEDORES</b></h3>
                <div class="d-flex flex-row-reverse bd-highlight">
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form action="{{ route('updateProveedor', $proveedor->id) }}" method="post">
                @method('POST')
              {{csrf_field()}}
                <div class="card-body">
                @include('vendor/flash.flash_message')


                <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="nompro">Nombre</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"
                          >A</span
                        >
                      </div>
                      <input
                        type="text"
                        maxlength="100"
                        class="form-control"
                        placeholder="Nombre del proveedor"
                        v-uppercase
                        name="nompro"
                        value="{{ $proveedor->nompro }}"
                      />
                     
                    </div>
                      @error('nompro')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="email">Correo</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"
                          >@</span
                        >
                      </div>
                      <input
                        type="email"
                        maxlength="100"
                        class="form-control"
                        placeholder="Correo del proveedor"
                        name="email"
                        value="{{ $proveedor->email }}"
                      />
                    </div>
                      @error('email')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                  </div>
                </div>

                <div class="col-sm-2">
                  <label for="nitpro">Tipo de persona</label>
                  <div class="form-check">
                    <input
                      class="form-check-input"
                      type="radio"
                      value="J"
                      name="nitpro"
                      {{ $proveedor->nitpro === 'J' || $proveedor->nitpro === 'J-' ? 'checked' : '' }}
                    />
                    <label class="form-check-label" for="exampleRadios1">
                      Jurídica
                    </label>
                  </div>
                  <div class="form-check">
                    <input
                      class="form-check-input"
                      type="radio"
                      value="N"
                      name="nitpro"
                      {{ $proveedor->nitpro === 'N' || $proveedor->nitpro === 'N-' ? 'checked' : '' }}
                    />
                    <label class="form-check-label" for="exampleRadios2">
                      Natural
                    </label>
                  </div>
                  <div class="form-check">
                    <input
                      class="form-check-input"
                      type="radio"
                      value="G"
                      name="nitpro"
                      {{ $proveedor->nitpro === 'G' || $proveedor->nitpro === 'G-' ? 'checked' : '' }}
                    />
                    <label class="form-check-label" for="exampleRadios3">
                      Gubernamental
                    </label>
                  </div>
                </div>
           

                <div class="col-sm-4">
                  <label for="rifpro">RIF</label>
                  <input
                    type="text"
                    class="form-control"
                    name="rifpro"
                    value="{{ $proveedor->rifpro}}"
                  />
                      @error('rifpro')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                </div>

                <div class="col-sm-2">
                  <label for="nacpro">Nacionalidad</label>
                  <div class="form-check">
                    <input
                      class="form-check-input"
                      type="radio"
                      id=""
                      value="N"
                      name="nacpro"
                      {{ $proveedor->nacpro === 'N' ? 'checked' : '' }}
                    />
                    <label class="form-check-label" for="exampleRadios1">
                      Venezolana
                    </label>
                  </div>
                  <div class="form-check">
                    <input
                      class="form-check-input"
                      type="radio"
                      id=""
                      value="P"
                      name="nacpro"
                      {{ $proveedor->nacpro === 'P' ? 'checked' : '' }}
                    />
                    <label class="form-check-label" for="exampleRadios2">
                      Extranjera
                    </label>
                  </div>
                </div>

                <div class="col-sm-2">
                  <label for="tipo">Actividad Profesional</label>
                  <div class="form-check">
                    <input
                      class="form-check-input"
                      type="radio"
                      id=""
                      value="C"
                      name="tipo"
                      {{ $proveedor->tipo === 'C' ? 'checked' : '' }}
                    />
                    <label class="form-check-label" for="exampleRadios1">
                      Contratista
                    </label>
                  </div>
                  <div class="form-check">
                    <input
                      class="form-check-input"
                      type="radio"
                      id=""
                      value="P"
                      name="tipo"
                      {{ $proveedor->tipo === 'P' ? 'checked' : '' }}
                    />
                    <label class="form-check-label" for="exampleRadios2">
                      Cooperativa
                    </label>
                  </div>
                  <div class="form-check">
                    <input
                      class="form-check-input"
                      type="radio"
                      id=""
                      value="O"
                      name="tipo"
                      {{ $proveedor->tipo === 'O' ? 'checked' : '' }}
                    />
                    <label class="form-check-label" for="exampleRadios3">
                      Proveedor
                    </label>
                  </div>
                </div>

                <div class="col-sm-2">
                  <label for="estpro">Proveedor Activo</label>
                  <div class="form-check">
                    <input
                      class="form-check-input"
                      type="radio"
                      value="A"
                      name="estpro"
                      {{ $proveedor->estpro === 'A' ? 'checked' : '' }}
                    />
                    <label class="form-check-label" for="exampleRadios1">
                      Si
                    </label>
                  </div>
                  <div class="form-check">
                    <input
                      class="form-check-input"
                      type="radio"
                      value="I"
                      name="estpro"
                      {{ $proveedor->estpro === 'I' ? 'checked' : '' }}
                    />
                    <label class="form-check-label" for="exampleRadios2">
                      No
                    </label>
                  </div>
                </div>

              </div>

              <div class="row mt-3">
               
               <div class="col-sm-6">
                 <div class="form-group">
                   <label for="dirpro">Dirección</label>
                   <div class="input-group">
                     <input
                       type="text"
                       maxlength="100"
                       class="form-control"
                       placeholder="Dirrección del proveedor"
                       value="{{ $proveedor->dirpro }}"
                       name="dirpro"
                     />
                   </div>
                      @error('dirpro')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                 </div>
               </div>

               <div class="col-sm-6">
                 <div class="form-group">
                   <label for="telpro">Teléfonos</label>
                   <div class="input-group">
                     <input
                       type="text"
                       maxlength="100"
                       class="form-control"
                       placeholder="Teléfonos del proveedor"
                       value="{{ $proveedor->telpro }}"
                       name="telpro"
                     />
                   </div>
                   @error('telpro')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                 </div>
               </div>

             </div>
                <!-- /.card-body -->

                <div class="card-footer mt-2" style="background-color: white;">
                    <a href="{{ route('proveedores') }}" class="btn btn-default retorno" ><i class="fa fa-chevron-circle-left"></i> Volver</a>
                    <button type="submit" class="btn btn-success">Guardar Cambios</button> 
                </div>

              </form>
            </div>
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->

            
         </form>
        </div>
            <!-- /.card -->
       </div>
      </div>
      </div>
      </section>      

@endsection

@section('script')
   <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
    });

    function pasarMayusculas(texto, elemento_id)
    {
    document.getElementById(elemento_id).value=texto.toUpperCase();
    }
   </script>
  <script src="{{asset('js/reportes.js')}} "></script>
@endsection