@extends('layouts.app')
@extends('layouts.menu')
@section('titulo', 'Centros de costo')
@section('content')
@component('layouts.contenth')
@slot('titulo')
  Listado de Proveedores
  @endslot
@endcomponent
<section class="content">
  <div class="content-fluid">
    <div class="card">
      @include('vendor/flash.flash_message')
      <div class="card-header">
        <h3 class="card-title"></h3>
        <div class="card-tools">
          
          <div class="row">
            <div class="col-sm-4 mt-2">
                @can('create', App\permission_user::class)
                <a href="{{ route('provCreate') }}" class="btn btn-info"><b>REGISTRAR</b></a>    
                @endcan
                </div>

                <div class="col mt-2">
                  <form action="{{ route('proveedores') }}" method="GET">
                      @csrf
                      <div class="input-group">
                        <input type="text" name="filtro" placeholder="Buscar" class="form-control float-right" value="{{$filtro}}">
                        <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                        <a href="{{URL::previous()}}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                       
                      </div>
                  </form>
                </div>
              </div>
        </div>
      </div>

      <div class="card-body table-responsive p-0 text-center">
        <table class="table table-hover">
          <thead>
            <tr style="text-align:center; font-weight: bold;">
              <td>Código</td>
              <td>Nombre</td>
              <td>RIF</td>
              <td>Acciones</td>
            </tr>
          </thead>
          <tbody>
            @foreach($proveedores as $prov)
              <tr style="text-align:center;">
                <td> {{ $prov->codpro }} </td>
                <td> {{ $prov->nompro }} </td>
                <td> {{ $prov->rifpro }} </td>     
                <td>
                  <a href="{{route('editProv', $prov)}}" data-toggle="tooltip" data-placement="top" title="Presupuesto"> {{-- Vista de presupuesto --}}
                  <i class="fa fa-edit"></i>
                  </a>
                  /
                  <a href="{{route('destroyProveedor', $prov)}}" onclick="confirmation(event)" class="borrar" id="deleteProveedpr"
                        data-id="{{$prov}}" data-toggle="tooltip" data-placement="top" title="Eliminar">
                        <i class="fas fa-trash red"></i>
                  </a>
                </td>
              </tr>
              @endforeach
          </tbody>
        </table>
        {!!$proveedores->appends(request()->input())->render()!!}
      </div>
    </div>
    
  </div>


@endsection
@section('script')
<script type="text/javascript">

function confirmation(ev) {
  ev.preventDefault();
  Swal.fire({
            title: '¿Está seguro de eliminar este Proveedor?',
            text: '¡Esta acción es irreversible!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'No, cancelar'
        }).then((result) => {
            if (result.value) {
                // Proceder con la eliminación usando AJAX
                const url = ev.target.closest('a').href;
                $.ajax({
                    url: url,
                    method: 'GET',
                    success: function(response) {
                        if (response.success) {
                            // Manejar la eliminación exitosa
                            Swal.fire({
                                title: 'Eliminado',
                                text: 'El proveedor se ha eliminado correctamente.',
                                icon: 'success',
                                confirmButtonText: 'Aceptar'
                            }).then((result) => {
                                if (result.value) {
                                    // Actualizar la interfaz de usuario (por ejemplo, eliminar la fila)
                                    $(this).parents('tr').fadeOut();
                                    window.location = response.redirect 
                                }
                            });
                        } else {
                            // Manejar la respuesta de error
                            Swal.fire({
                                title: 'Error',
                                text: response.message,
                                icon: 'error',
                                confirmButtonText: 'Aceptar'
                            });
                        }
                    },
                    error: function(error) {
                        Swal.fire({
                            title: 'Error',
                            text: 'Se ha producido un error al eliminar el proveedor.',
                            icon: 'error',
                            confirmButtonText: 'Aceptar'
                        });
                    }
                });
            }
        });
   
    }


 </script>
@endsection