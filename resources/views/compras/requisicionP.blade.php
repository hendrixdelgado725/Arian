@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Empresas')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Listado de Pedidos
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
        <div class="card">
          @include('vendor/flash.flash_message')
          <div class="card-header">
            <h3 class="card-title"></h3>
              <div class="card-tools">
                <div class="row">
                <div class="col-sm-4 mt-2" >
                  @can('create',App\permission_user::class)
                  <a href="{{ route('createRequisicionP') }}" class="btn btn-info  mt-2" target="_blank"><b>REGISTRAR</b></a>
                  @endcan
                </div>
                <div class="col mt-2">
                  
                  <form action="{{ route('requisicionP') }}" method="GET">
                    @csrf
                    <div class="input-group  mt-2">
                      <input type="text" name="filtro" placeholder="Buscar" class="form-control float-right" value="{{$filtro}}">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                      <a href="{{URL::previous()}}" class="btn btn-default"><i class="fas fa-arrow-left"></i></a>                       
                    </div>
                  </form>
                </div>
              </div>
              </div>
          </div>
          <div class="card-body">
            <table class="table text-center">
              <thead>
                <tr>
                  <th scope="col">Código</th>
                  <th scope="col">Descripción</th>
                  <th scope="col">Fecha</th>
                  <th scope="col">Estado</th>
                  <th scope="col">Acción</th>
                </tr>
              </thead>
              <tbody>
                  @foreach($reqs as $req) 
                  <tr data-id="{{$req->reqart}}">
                    <td> {{ $req->reqart }} </td>
                    <td> {{ $req->desreq }} </td>
                    <td> {{ $req->fecreq }} </td>
                    <td> {{ $req->stareq }} </td>
                    <td>
                      @if ($req->getRawOriginal('stareq') === 'A')
                          <a class="sendPdf" href="" data-toggle="tooltip" data-placement="top" title="Generar PDF">
                            <i class="far fa-file-pdf"></i>
                          </a>
                          /
                      @endif
                      @can('edit',App\permission_user::class)
                      <a href="{{ route('showReqP', $req->id) }}" title="Detalles"><i class="fa fa-list"></i></a>
                      
                      @endcan

                      @can('edit',App\permission_user::class)
                    @if($req->getRawOriginal('stareq') === 'U')

                        /

                        <a href="{{ route('reenviarReqPriv', $req->reqart) }}" data-toggle="tooltip" data-placement="top" title="Reenvío">
                          <i class="fa fa-envelope">
                          </i>
                        </a>

                        @endif
                        @endcan
                    </td>
                    
                  </tr>
                  @endforeach
                </tbody>
              </table>
            
          </div>
          {!!$reqs->appends(request()->input())->render()!!}
          </div>

  </div>
  <form id="sendPdf" method="POST" action="{{route('generarReporteRequisicion')}}" style="display:none">
      @csrf
        <input id="reqart" type="text" name="reqart" > 
    </form>
</section>
@endsection
@section('script')
<script>
  $(document).ready(()=>{

    $(document).on('click','.sendPdf',function (e) {
      e.preventDefault();
      let valor = $(this).closest('tr').data('id')
        $('input#reqart').val(valor)
        $('#sendPdf').submit()
        console.log("ssss")
    })
  

    deleteempresa();
      function deleteempresa(){//se insertaran paises a la configuracion del sistema siempre y cuando Existan
        $('a#delete').click(function(e){
          e.preventDefault();
          const row = $(this).parents('tr');
          const url = $(this).attr('href');
		  		Swal.fire({
				  title: 'Seguro que Desea Eliminar?',
				  text: "Despues de esto no se encontrara en la lista!",
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si, Eliminar!'
        }).then((result) => {//dio click en el boton aceptar
          if(result.value){
            $.get(url,function(result){
              if (result.exito) {
                Swal.fire(
                  'Ha sido Eliminado!',
                  'Con Exito.',
                  'success',
                )  
              row.fadeOut();
              }else if(result.error){
                Swal.fire({
                  icon: 'warning',
                  title: result.error,
                  timer:1500
                  })
                }
            })
          }      
        })
		});
    }
  })

</script>
@endsection