@extends('layouts.app3')
@extends('layouts.menu')
@section('titulo','A1WIN - Registro de Ordenes')

@section('content')
  @component('layouts.contenth')
    @slot('titulo')
      Registrar Ordenes
    @endslot
  @endcomponent
 <ordenes
 :costos="{{ json_encode($costos) }}"
 :solicitante="{{ json_encode($solicitante) }}"
 :condiciones="{{ json_encode($condiciones) }}"
 :formasent="{{ json_encode($formasEnt) }}"
 :tipofinan="{{ json_encode($tipoFinan) }}"
 :moneda="{{ json_encode($moneda) }}"
 :recargos="{{ json_encode($recargos) }}"
 ></ordenes>
@endsection
