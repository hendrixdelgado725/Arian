@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Registro de Nota de Entrega')

@section('content')
  @component('layouts.contenth')
    @slot('titulo')
      <h3 class="text-center aling-item-center"> Registrar Nota de Entrega </h3>
    @endslot
  @endcomponent
 <form-notaentrega :orden="{{ json_encode($ordenes) ?? json_encode('{}') }}"></form-notaentrega>
@endsection
