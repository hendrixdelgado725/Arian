@extends('layouts.app')
@extends('layouts.menu')
@section('titulo', 'Detalles de Orden')

@section('content')
@component('layouts.contenth')
@slot('titulo')
    Detalles de la Orden
@endslot
@endcomponent

<section class="content">
<div class="container-fluid">
    <div class="col-14">
        <div class="">
            @if($ordenes->staord === 'N' && $ordenes->afepre === 'N')
                <p class="text-center text-danger"> <b>Razón de anulación: </b> {{ $ordenes->monord ?? 'No hay motivo específico' }} </p>
                @endif
            <div class="card card-info">
            @include('vendor/flash.flash_message')
            <div class="card-header">
            <h3 class="card-title mt-2"><b>Detalles de la Compra</b></h3>
            <div class="d-flex flex-row-reverse bd-highlight">
                
            </div>
            </div>
            <div class="card-body">
            <div class="row">
                <div class="col-sm-4">
                <div class="form-group">
                    <label for="empsol">Solicitante</label>
                    <div>
                    <input
                        type="text"
                        class="form-control"
                        disabled
                        value="{{ $ordenes->solicitante->nomemp ?? ''}}"
                    />
                    </div>
                </div>
                </div>

                <div class="col-sm-4">
                <div class="form-group">
                    <label for="codcen">Departamentos</label>
                    <input
                        type="text"
                        class="form-control"
                        disabled
                        value="{{ $ordenes->ccosto->descen ?? '' }}"
                    />
                </div>
                </div>
                <div class="col-sm-3">
                <div class="form-group">
                    <div>
                    <p>
                        <b>Número: {{ $ordenes->ordcom }}</b>
                    </p>
                    <p>
                        <b>Fecha: {{ $ordenes->fecord }}</b>
                    </p>
                    @if($ordenes->pedido)
                    <p>
                        <b>Numero de pedido: {{ $ordenes->pedido->reqart }}</b>
                    </p>
                    @endif
                    </div>
                    <span class="text-red" style="font-size: 80%"></span>
                </div>
                </div>
            </div>
            <div class="row">

            <div class="col-sm-1 text-center align-item-center mb-4">
                  <label for="nitpro">Tipo de Orden</label>
                  <div class="form-check">
                    <input
                      class="form-check-input"
                      type="radio"
                      name="tipord"
                      id=""
                      value="C"
                      {{$ordenes->tipord === 'C' ? 'checked' : ''}}
                      disabled
                    />
                    
                      Compra
                    
                  </div>
                  <div class="form-check">
                    <input
                      class="form-check-input"
                      type="radio"
                      name="tipord"
                      id=""
                      value="S"
                      disabled
                      {{$ordenes->tipord === 'S' ? 'checked' : ''}}
                    />
                      Servicio
                  </div>

                </div>

                <div class="col-sm-4 offset-sm-3">
                <div class="form-group">
                    <label for="copro">Proveedor</label>
                    <input
                        type="text"
                        class="form-control"
                        disabled
                        value="{{ $ordenes->proveedor->nompro ?? '' }}"
                    />
                </div>
                </div>
                
               
            </div>

            <div class="row">
              <div class="col-sm-4">
                    <div class="form-group">
                      <label>Condicion de pago</label>
                      <div>
                      <input
                        type="text"
                        class="form-control"
                        disabled
                        value="{{ $ordenes->condicionPago->desconpag ?? '' }}"
                    />

                      </div>
                    </div>
                  </div>

                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Forma de entrega</label>
                      <div>
                      <input
                        type="text"
                        class="form-control"
                        disabled
                        value="{{ $ordenes->formaEntrega->desforent ?? ''}}"
                    />
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>Tipo de financiamiento</label>
                      <div>
                      <input
                        type="text"
                        class="form-control"
                        disabled
                        value="{{ $ordenes->tipoFinancia->nomext ?? '' }}"
                        />

                      </div>
                    </div>
                  </div>
            </div>

            <div class="row">
             

             <div class="col-sm-4">
                   <div class="form-group">
                     <label>Tipo de moneda</label>
                     <div>
                     <input
                        type="text"
                        class="form-control"
                        disabled
                        value="{{ $ordenes->tipoMoneda->nommon ?? '' }}"
                        />
                     </div>
                   </div>
                 </div>

                 <div class="col-sm-4">
                   <div class="form-group">
                     <label>Valor de moneda</label>
                     <div>
                     <input
                        type="text"
                        class="form-control numeros"
                        disabled
                        value="{{ $ordenes->valmon }}"
                        />

                     </div>
                   </div>
                 </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                      <label>Tasa de cambio</label>
                      <div>
                      <input
                         type="text"
                         class="form-control"
                         disabled
                         value="{{ $ordenes->tasa}}"
                         />
                      </div>
                    </div>
                  </div>


           </div>

            <div class="row mx-auto pb-3">
                <div class="col">
                <label for="desreq">Justificación</label>
                <textarea
                    class="form-control ta"
                    placeholder="Justificación"
                    cols="30"
                    rows="3"
                    v-uppercase
                    disabled
                >{{ $ordenes->desord }}</textarea>
                </div>
            </div>

            <div>
                <div style ="text-align:center;">
                    <label>Artículos</label>
                    </div>
                <div class="table-responsive">
                <table class="table table-bordered table-hover" id="tabla">
                    <thead class="thead-light">
                    <tr style="text-align: center">
                        <th>Nro.</th>
                        <th>Descripción</th>
                        <th>Unidad de medida</th>
                        <th>Cant. requerida</th>
                        <th>Costo</th>
                        <th>Recargo</th>
                        <th>Descuento</th>
                        <th>Monto IVA</th>
                        <th>Total</th>
                        <th>Total ($)</th>
                    </tr>
                    </thead>

                    <tbody style="text-align: center">
                    @foreach ($ordenes->articulos as $item)
                        
                    <tr>
                        <td>{{ $loop->index }}</td>
                        <td>
                            <input
                            class="form-control"
                            disabled
                            type="text"
                            value="{{ $item->desart }}"
                            disabled
                        />
                        </td>

                        <td>
                        <input
                        class="form-control"
                            place
                            name="unimed[]"
                            type="text"
                            value="{{ $item->unimed }}"
                            disabled
                        />
                        </td>

                        <td>
                        <input
                            class="form-control numeros"
                            placeholder="Ej: 10.00"
                            type="number"
                            min="0.01"
                            value="{{ $item->canord }}"
                            step="0.01"
                            disabled
                        />
                        </td>
                        <td>
                        <input
                            type="number"
                            class="form-control numeros"
                            min="0.01"
                            step="0.01"
                            value="{{ $item->preart }}"
                            disabled
                        />
                        </td>
                        <td>
                        <input
                            type="number"
                            class="form-control numeros"
                            min="0.01"
                            step="0.01"
                            value="{{ $item->rgoart }}"
                            disabled
                        />
                        </td>
                        
                        <td>
                        <input
                            type="number"
                            class="form-control numeros"
                            value="{{ $item->dtoart }}"
                            disabled
                            />
                        </td>
                        <td><input type="number" class="form-control numeros" value="{{ $item->rgoart }}" disabled></td>
                        <td>
                            <input
                            type="number"
                            class="form-control numeros"
                            disabled 
                            value="{{ $item->totart }}"
                            
                            
                            />
                        </td>
                        <td><input type="number" class="form-control numeros" value="{{ $item->montasa }}" disabled></td>

                    </tr>
                    @endforeach
                </tbody>
                </table>
                </div>

                
                <div class="col-sm-1" style="float: right">
                    <div style="display: none;">
                        <input type="text" id="numero" value="{{ $ordenes->ordcom }}">
                        <input type="text" id="cedula" value="{{ Auth::user()->cedemp }}">
                    </div>

                </div>
                <div class="row pt-4 mx-3" style="display: block">
                    <div class="col-sm-1" style="float: right">
                      <label for="monto">Monto ($):</label>
                      <input
                      class="form-control numeros"
                      type="number"
                      step="any"
                      disabled
                      
                      value="{{ $ordenes->montasa }}"
                      />
                  </div>
                    <div class="col-sm-1" style="float: right">
                        <label for="monto">Monto:</label>
                        <input
                        class="form-control numeros"
                        type="number"
                          step="any"
                          disabled
                          
                          value="{{ $ordenes->monord }}"
                        />
                    </div>
                    <div class="col-sm-1" style="float: right">
                      <label for="monto">IVA:</label>
                      <input
                        class="form-control numeros"
                        type="number"
                        step="any"
                        disabled
                        
                        value="{{ $ordenes->moniva }}"
                      />
                    </div>
                    <div class="col-sm-1" style="float: right">
                        <label for="monto">Subtotal:</label>
                        <input
                          class="form-control numeros"
                          type="number"
                          step="any"
                          disabled
                          
                          value="{{ $ordenes->subtotal }}"
                        />
                      </div>
                    </div>
            </div>

            </div>
            
            <!-- /.card-body -->
            
            <div class="card-footer" style="background-color: white;">
                <a href="{{ route('Ordenes') }}" class="btn btn-default retorno" ><i class="fa fa-chevron-circle-left"></i> Volver</a>
                @if($ordenes->staord !== 'A' && $ordenes->staord !== 'N')
                <button type="submit" onclick="action('S')" class="btn btn-success">Aceptar</button>
                <button type="submit" onclick="action('N')" class="btn btn-danger">Rechazar</button>
                @endif    
            </div>
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col-lg-6 -->
        </div>
        <!-- /.row -->

        
        </form>
    </div>
        <!-- /.card -->
    </div>
    </div>
    </div>
    </section>      

@endsection

@section('script')

<script>
    function action(val){
        let numero = document.getElementById("numero").value 
        let cedula = document.getElementById("cedula").value 
        console.log(numero)
        if(val === 'S'){
            Swal.fire({
                title: '¿Seguro que quiere aceptar?',
                text: "No se puede revertir el cambio",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar'
                }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: "{{ route('actionOrd') }}",
                        data: {
                            "param": val,
                            "user": cedula,
                            "ordcom": numero,
                            "_token": $("meta[name='csrf-token']").attr("content")
                        },
                        dataType: "json",
                        method: "POST",
                        success: function(result){
                            if(result){
                                Swal.fire(
                                    'Aceptado',
                                    'La compra ha sido aceptada correctamente.',
                                    'success'
                                    );
                                    window.location = '{{ route('Ordenes') }}';
                            } else {
                                Swal.fire(
                                    'Error',
                                    'Ha habido un error en aceptar la compra',
                                    'error'
                                    )
                            }
                        }
                    })
                }
                })
        } else {
            Swal.fire({
                title: 'Exprese el motivo de la anulación',
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Enviar',
                showLoaderOnConfirm: true,
                preConfirm: (motanu) => {
                    $.ajax({
                        url: "{{ route('actionOrd') }}",
                        data: {
                            "param": val,
                            "user": cedula,
                            "ordcom": numero,
                            "motanu": motanu,
                            "_token": $("meta[name='csrf-token']").attr("content")
                        },
                        dataType: "json",
                        method: "POST",
                        success: function(result){
                            if(result){
                                Swal.fire(
                                    'Procesado',
                                    'La compra ha sido rechazada correctamente.',
                                    'success'
                                    );
                                    window.location = '{{ route('Ordenes') }}';
                            } else {
                                Swal.fire(
                                    'Error',
                                    'Ha habido un error en rechazar la compra',
                                    'error'
                                    )
                            }
                        }
                    })

                },
                })
        }

}; 
</script>
<style>
    .numeros {
        text-align: right;
    }
</style>

@endsection