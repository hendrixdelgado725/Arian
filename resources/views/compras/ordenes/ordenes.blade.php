@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Empresas')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Listado de Ordenes
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
        <div class="card">
          @include('vendor/flash.flash_message')
          <div class="card-header">
            <h3 class="card-title"></h3>
              <div class="card-tools">
                <div class="row">
                <div class="col-sm-4 mt-2" >
                  @can('create',App\permission_user::class)
                  <a href="{{ route('OrdenesCreate') }}" class="btn btn-info  mt-2" target="_blank"><b>REGISTRAR</b></a>
                  @endcan
                </div>
                <div class="col mt-2">
                  
                  <form action="{{ route('Ordenes') }}" method="GET">
                    @csrf
                    <div class="input-group  mt-2">
                      <input type="text" name="filtro" placeholder="Buscar" class="form-control float-right" value="{{$filtro}}">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                      <a href="{{URL::previous()}}" class="btn btn-default"><i class="fas fa-arrow-left"></i></a>                       
                    </div>
                  </form>
                </div>
              </div>
              </div>
          </div>

          <nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">COMPRAS</a>
    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">SERVICIOS</a>
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
  <div class="card-body">
            
            <table class="table text-center">

                <thead>
                    <tr>
                    <th scope="col">Código</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Fecha</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Acción</th>
                    </tr>
                </thead>
                
                <tbody>
                
                @foreach($compras as $compra) 
                  <tr data-id="{{$compra->ordcom}}">
                    <td> {{ $compra->ordcom }} </td>
                    <td> {{ $compra->desord }} </td>
                    <td> {{ $compra->fecord }} </td>
                    <td> {{ $compra->staord }} </td>
                    <td>
                      @if($compra->getRawOriginal('staord') === 'A')
                      <a class="sendPdf" href="" data-toggle="tooltip" data-placement="top" title="Generar PDF" >
                        <i class="far fa-file-pdf"></i>
                        </a>
                        /
                      @endif
                      @if ($compra->afepre === 'P' && $compra->getRawOriginal('staord') === 'A')
                      <a href="{{ route('GenerarNotaEntrega', $compra->id) }}" title="Generar nota de entrega"><i class="fa fa-file"></i></a>
                      /
                      
                      @endif
                      
                      @if($compra->getRawOriginal('staord') !== 'P')
                      @can('edit',App\permission_user::class)
                      <a href="{{ route('showOrden', $compra->id) }}" title="Detalles"><i class="fa fa-list"></i></a>
                      
                      @endcan
                      @endif
                    </td>
                  </tr>
                  @endforeach
                
                </tbody>
              </table>
            
          </div>
          {!!$compras->appends(request()->input())->render()!!}

  </div>
  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
  <div class="card-body">
            
            <table class="table text-center">

                <thead>
                    <tr>
                    <th scope="col">Código</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Fecha</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Acción</th>
                    </tr>
                </thead>
                
                <tbody>
                @foreach($servicios as $servicio) 
                  <tr data-id="{{$servicio->ordcom}}">
                    <td> {{ $servicio->ordcom }} </td>
                    <td> {{ $servicio->desord }} </td>
                    <td> {{ $servicio->fecord }} </td>
                    <td> {{ $servicio->staord }} </td>

                    <td>

                      @if($servicio->getRawOriginal('staord') === 'A')
                      <a class="sendPdf" href="" data-toggle="tooltip" data-placement="top" title="Generar PDF" >
                        <i class="far fa-file-pdf"></i>
                        </a>
                        /
                      @endif
                      
                      @if ($servicio->afepre === 'P' && $servicio->getRawOriginal('staord') === 'A')
                      <a class="sendPdf" href="" data-toggle="tooltip" data-placement="top" title="Generar PDF">
                        <i class="far fa-file-pdf"></i>
                        </a>
                      <a href="{{ route('GenerarNotaEntrega', $servicio->id) }}" title="Generar Nota de Entrega"><i class="fa fa-file"></i></a>
                      /
                      @endif
                      
                      @if($servicio->getRawOriginal('staord') !== 'P')
                      @can('edit',App\permission_user::class)
                      <a href="{{ route('showOrden', $servicio->id) }}" title="Detalles"><i class="fa fa-list"></i></a>
                      
                      @endcan
                      @endif

                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            
          </div>
          {!!$servicios->appends(request()->input())->render()!!}

  </div>
</div>

          
          </div>
        <!-- </div> -->
    <!-- </div> -->
      {{-- row --}}
  </div>
  {{-- container fluid --}}
  <form id="sendPdf" method="POST" action="{{route('generarReporteOrdenes')}}" style="display:none">
    @csrf
      <input id="ordcom" type="text" name="ordcom" > 
  </form>

</section>
@endsection
@section('script')
<script>
  $(document).ready(()=>{

    $(document).on('click','.sendPdf',function (e) {
      e.preventDefault();
      let valor = $(this).closest('tr').data('id')
        $('input#ordcom').val(valor)
        $('#sendPdf').submit()
    })

    deleteempresa();
      function deleteempresa(){//se insertaran paises a la configuracion del sistema siempre y cuando Existan
        $('a#delete').click(function(e){
          e.preventDefault();
          const row = $(this).parents('tr');
          const url = $(this).attr('href');
		  		Swal.fire({
				  title: 'Seguro que Desea Eliminar?',
				  text: "Despues de esto no se encontrara en la lista!",
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si, Eliminar!'
        }).then((result) => {//dio click en el boton aceptar
          if(result.value){
            $.get(url,function(result){
              if (result.success) {
                Swal.fire(
                  'Ha sido Eliminado!',
                  'Con Exito.',
                  'success',
                )  
                location.reload();
              row.fadeOut();
              }else if(result.error){
                Swal.fire({
                  icon: 'warning',
                  title: result.error,
                  timer:1500
                  })
                }
            })
          }      
        })
		});
    }
  })

</script>
@endsection