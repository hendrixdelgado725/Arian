@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Edición de Requisicion')
@section('content')

    @component('layouts.contenth')
        @slot('titulo')
        Detalles de requisición
        @endslot
    @endcomponent

    <requisicion
    type="{{ $type }}"
    :req="{{ json_encode($req) ?? json_encode('{}') }}"
    :arts="{{ json_encode($articulos) ?? json_encode('{}') }}"
    :partida="{{ json_encode($partida) ?? json_encode('{}') }}"
    :user="{{ json_encode($user) ?? json_encode('{}') }}"
    ></requisicion>

    @endsection