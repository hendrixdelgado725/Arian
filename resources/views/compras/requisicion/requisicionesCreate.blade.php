@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Registro de Requisicion')
@section('content')

    @component('layouts.contenth')
        @slot('titulo')
        Registro de Requisición 
        @endslot
    @endcomponent

    <requisicion></requisicion>

    @endsection

    @section('script')
   <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
    });

    function pasarMayusculas(texto, elemento_id)
    {
    document.getElementById(elemento_id).value=texto.toUpperCase();
    }
   </script>
  <script src="{{asset('js/reportes.js')}} "></script>
  @endsection
  <style>
    .my-custom-scrollbar {
position: relative;
height: 300px;
overflow: auto;
}
.table-wrapper-scroll-y {
display: block;
}
.ta {
resize: none;
}
  </style>