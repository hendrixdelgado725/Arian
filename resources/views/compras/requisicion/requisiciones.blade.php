@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Clientes')

@section('content')
  @component('layouts.contenth')
    @slot('titulo')
      Listado de Requisiciones
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">
                  <div class="col-sm-4 mt-2">
                     
                   <a href="{{ route("createReq") }}" class="btn btn-info mt-2"><b> REGISTRAR</b></a>
                   
                  </div>

                <div class="col mt-2">

                  <form action="" method="GET">
                    @csrf
                     <div class="input-group mt-2">
                      <input type="text" name="filtro" placeholder="Buscar" class="form-control float-right">
                      <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Buscar"><i class="fas fa-search"></i></button>  
                      <a href="{{URL::previous()}}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                    
                     </div>
                  </form>
                  
                </div><!--col-->
                 </div><!--row -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>Número</th>
                      <th>Justificación</th>
                      <th>Fecha</th>
                      <th>Estatus</th>
                      <th>Acción</th>
                    </tr>
                  </thead>
                  <tbody style="text-align:center; font-size: 0.9rem;">
                    @foreach($reqs as $req)
                    <tr>
                      <td>{{ $req->reqart }}</td>
                      <td>{{ $req->desreq }}</td>
                      <td>{{ $req->fecreq }}</td>
                      @if($req->stareq === 'U')
                      <td>PENDIENTE POR APROBACIÓN DEL GERENTE DE ÁREA</td>
                      @elseif($req->stareq === 'A')
                        @if($req->aprreq === 'G')
                        <td>APROBADO POR GERENTE DE ÁREA / PENDIENTE POR PRESUPUESTO</td>
                        @elseif($req->aprreq === 'C')
                        <td>REVISADO POR PRESUPUESTO / PENDIENTE POR APROBAR</td>
                        @elseif($req->aprreq === 'P')
                        <td>APROBADA POR PRESUPUESTO / PENDIENTE POR COMPRA</td>
                        @elseif($req->aprreq === 'S')
                        <td>APROBADA</td>
                        @elseif($req->aprreq === 'R')
                        <td>RECHAZADA</td>
                        @elseif($req->aprreq === 'N' || (is_null($req->aprreq)))
                        <td>PENDIENTE POR REVISIÓN</td>
                        @endif 
                      @elseif($req->stareq === 'N')
                      <td>ANULADA</td>
                      @elseif($req->stareq === 'X')
                      <td>RECHAZADO POR EL GERENTE DE ÁREA</td>
                      @endif
                      
                      <td>
                         
                        <a href="{{ route('editReq', $req->reqart) }}" data-toggle="tooltip" data-placement="top" title="Editar">
                          <i class="fa fa-edit blue"></i>
                        </a>

                        @if($req->stareq === 'U')

                        /

                        <a href="{{ route('reenviarReq', $req->reqart) }}" data-toggle="tooltip" data-placement="top" title="Reenvío">
                           <i class="fa fa-envelope">
                          </i>
                        </a>

                        @endif
{{-- 
                        /

                        <a href="{{ route('sendpreReq', $req->reqart) }}" data-toggle="tooltip" data-placement="top" title="Presupuesto">
                          <i class="fa fa-envelope"></i>
                        </a> --}}

{{-- 
                        /
                        @if($req->stareq === 'A')
                        <a class="anular" href="" title="Anular requisición">
                          <i class="fa fa-ban blue"></i>
                        </a>

                        @elseif($req->stareq === 'N')
                        <a href="" title="Aceptar requisición">
                          <i class="fa fa-check blue"></i>
                        </a>
                        
                        @endif
                        --}}
 
                        
                      </td>

                    </tr>
                    @endforeach
                  </tbody>
                </table>
               
                
                {{-- col8  --}}
          </div>
          {!!$reqs->appends(request()->input())->render()!!}
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section>

  
@endsection
@section('script')
   <script type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
    });

    $('.anular').click(function(e){
      e.preventDefault();
      Swal.fire({
        title: '¿Desea anular este requerimiento?',
        text: 'Motivo (Opcional)',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#36A304',
        cancelButtonColor: '#ED0303',
        confirmButtonText: 'Anular',
        cancelButtonText: 'Cancelar',
        input: 'text',
      }).then((result) => {
        if (result.value){
          Swal.fire('Result:'+result.value)
        }
      })
    }) 
   </script>
@endsection
