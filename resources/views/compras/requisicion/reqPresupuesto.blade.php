@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Presupuesto para Requisicion')
@section('content')

    @component('layouts.contenth')
        @slot('titulo')
        Presupuesto para Requisición
        @endslot
    @endcomponent

    <reqpresupuesto
    :req="{{ json_encode($req) ?? json_encode('{}') }}"
    :user="{{ json_encode($user) ?? json_encode('{}') }}"
    :precom="{{ json_encode($precom) ?? json_encode('{}') }}"
    ></reqpresupuesto>
 
    @endsection