@extends('layouts.app')
@extends('layouts.menu')
@section('titulo', 'Detalles de requisición')

@section('content')
@component('layouts.contenth')
@slot('titulo')
    Detalles del Pedido
@endslot
@endcomponent

<section class="content">
<div class="container-fluid">
    <div class="col-14">
        <div class="">
            @if($req->stareq === 'N' && $req->aprreq === 'N')
                <p class="text-center text-danger"> <b>Razón de anulación: </b> {{ $req->motreq ?? 'No hay motivo específico' }} </p>
                @endif
            <div class="card card-info">
            @include('vendor/flash.flash_message')
            <div class="card-header">
            <h3 class="card-title mt-2"><b>Detalles de la Compra</b></h3>
            <div class="d-flex flex-row-reverse bd-highlight">
                
            </div>
            </div>
            <div class="card-body">
            <div class="row">
                <div class="col-sm-3">
                <div class="form-group">
                    <label for="empsol">Solicitante</label>
                    <div>
                    <input
                        type="text"
                        class="form-control"
                        disabled
                        value="{{ $req->solicitante->nomemp }}"
                    />
                    </div>
                </div>
                </div>

                <div class="col-sm-5">
                <div class="form-group">
                    <label for="codcen">Departamentos</label>
                    <input
                        type="text"
                        class="form-control"
                        disabled
                        value="{{ $req->ccosto->descen ?? ''}}"
                    />
                </div>
                </div>
                <div class="col-sm-3">
                <div class="form-group">
                    <div>
                    <p>
                        <b>Número: {{ $req->reqart }}</b>
                    </p>
                    <p>
                        <b>Fecha: {{ $req->fecreq }}</b>
                    </p>
                    </div>
                    <span class="text-red" style="font-size: 80%"></span>
                </div>
                </div>
            </div>

        <div class="row mx-auto pb-3">
                <div class="col">
                <label for="desreq">Justificación</label>
                <textarea
                    class="form-control ta"
                    placeholder="Justificación"
                    cols="30"
                    rows="3"
                    v-uppercase
                    disabled
                >{{ $req->desreq ?? ''}}</textarea>
                </div>
            </div>

            <div>
                <div style ="text-align:center;">
                    <label>Artículos</label>
                    </div>
                <div class="table-responsive">
                <table class="table table-bordered table-hover" id="tabla">
                    <thead class="thead-light">
                    <tr style="text-align: center">
                        <th>Nro.</th>
                        <th>Descripción</th>
                        <th>Unidad de medida</th>
                        <th>Cant. requerida</th>
                        {{-- <th>Costo</th>
                        <th>Recargo</th>
                        <th>Descuento</th>
                        <th>Total</th> --}}
                    </tr>
                    </thead>

                    <tbody style="text-align: center">
                    @foreach ($req->articulos as $item)
                        
                    <tr>
                        <td>{{ $loop->index }}</td>
                        <td>
                            <input
                            class="form-control"
                            disabled
                            type="text"
                            value="{{ $item->desart }}"
                            disabled
                        />
                        </td>

                        <td>
                        <input
                        class="form-control"
                            place
                            name="unimed[]"
                            type="text"
                            value="{{ $item->unimed }}"
                            disabled
                        />
                        </td>

                    

                        <td>
                        <input
                            class="form-control"
                            placeholder="Ej: 10.00"
                            type="number"
                            min="0.01"
                            value="{{ $item->canreq }}"
                            step="0.01"
                            disabled
                        />
                        </td>
                       {{--  <td>
                        <input
                            type="number"
                            class="form-control"
                            min="0.01"
                            step="0.01"
                            value="{{ $item->costo }}"
                            disabled
                        />
                        </td>
                        <td>
                        <input
                            type="number"
                            class="form-control"
                            min="0.01"
                            step="0.01"
                            value="{{ $item->monrgo }}"
                            disabled
                        />
                        </td>
                        
                        <td>
                        <input
                            type="number"
                            class="form-control"
                            value="{{ $item->mondes }}"
                            disabled
                            />
                        </td>
                        <td>
                        <input
                            type="number"
                            class="form-control"
                            disabled 
                            value="{{ $item->montot }}"
                            style="text-align: right"
                        
                            />
                        </td> --}}

                    </tr>
                    @endforeach
                </tbody>
                </table>
                </div>

                
                <div class="col-sm-1" style="float: right">
                    <div style="display: none;">
                        <input type="text" id="numero" value="{{ $req->reqart }}">
                        <input type="text" id="cedula" value="{{ Auth::user()->cedemp }}">
                    </div>
                    {{-- <label for="monto">Monto:</label>
                    <input
                    class="form-control"
                    type="number"
                    step="0.01"
                    disabled
                    style="text-align: right"
                    value="{{ $req->monreq }}"
                    /> --}}
                </div>
            </div>

            </div>
            
            <!-- /.card-body -->
            
            <div class="card-footer" style="background-color: white;">
                <a href="{{ route('requisicionP') }}" class="btn btn-default retorno" ><i class="fa fa-chevron-circle-left"></i> Volver</a>
                @if($req->stareq !== 'P' && $req->stareq !== 'N' && $req->stareq !== 'A' && $req->stareq !== 'S')
                <button type="submit" onclick="action('S')" class="btn btn-success">Aceptar</button>
                <button type="submit" onclick="action('N')" class="btn btn-danger">Rechazar</button>
                @endif    
            </div>
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col-lg-6 -->
        </div>
        <!-- /.row -->

        
        </form>
    </div>
        <!-- /.card -->
    </div>
    </div>
    </div>
    </section>      

@endsection

@section('script')

<script>
    function action(val){
        let numero = document.getElementById("numero").value 
        let cedula = document.getElementById("cedula").value 
        console.log(numero)
        if(val === 'S'){
            Swal.fire({
                title: '¿Seguro que quiere aceptar?',
                text: "No se puede revertir el cambio",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar'
                }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: "{{ route('actionReqP') }}",
                        data: {
                            "param": val,
                            "user": cedula,
                            "reqart": numero,
                            "_token": $("meta[name='csrf-token']").attr("content")
                        },
                        dataType: "json",
                        method: "POST",
                        success: function(result){
                            if(result){
                                Swal.fire(
                                    'Aceptado',
                                    'La compra ha sido aceptada correctamente.',
                                    'success'
                                    );
                                    window.location = '{{ route('requisicionP') }}';
                            } else {
                                Swal.fire(
                                    'Error',
                                    'Ha habido un error en aceptar la compra',
                                    'error'
                                    )
                            }
                        }
                    })
                }
                })
        } else {
            Swal.fire({
                title: 'Exprese el motivo de la anulación',
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Enviar',
                showLoaderOnConfirm: true,
                preConfirm: (motanu) => {
                    $.ajax({
                        url: "{{ route('actionReqP') }}",
                        data: {
                            "param": val,
                            "user": cedula,
                            "reqart": numero,
                            "motanu": motanu,
                            "_token": $("meta[name='csrf-token']").attr("content")
                        },
                        dataType: "json",
                        method: "POST",
                        success: function(result){
                            if(result){
                                Swal.fire(
                                    'Procesado',
                                    'La compra ha sido rechazada correctamente.',
                                    'success'
                                    );
                                    window.location = '{{ route('requisicionP') }}';
                            } else {
                                Swal.fire(
                                    'Error',
                                    'Ha habido un error en rechazar la compra',
                                    'error'
                                    )
                            }
                        }
                    })

                },
                })
        }
        /* $.ajax({
        url: "{{ route('actionReqP')}}",
        data: "param="+val+"&_token={{ csrf_token()}}",
        dataType: "json",
        method: "POST",
        success: function(result)
        {
            if (result['result'] == 'ok')
            {

            }
            else
            {

            }
        },
        fail: function(){
        },
        beforeSend: function(){
        }
    });*/
}; 
</script>

@endsection