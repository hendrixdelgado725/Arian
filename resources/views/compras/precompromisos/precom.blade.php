@extends('layouts.app')
@extends('layouts.menu')
@section('titulo', 'Centros de costo')

@section('content')
@component('layouts.contenth')
  @slot('titulo')
      Lista de Precompromisos
  @endslot
@endcomponent

<section class="content">
  <div class="content-fluid">
    <div class="card">
      @include('vendor/flash.flash_message')
      <div class="card-header">
        <h3 class="card-title"></h3>
        <div class="card-tools">
          <div class="row">
            <div class="col-sm-4 mt-2">
                    
                </div>

                <div class="col mt-2">
                  <form action="" method="GET">
                    @csrf
                    <div class="input-group">
                      <input type="text" name="filtro" placeholder="Buscar " class="form-control float-right">
                          <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                          <a href="{{URL::previous()}}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                       
                    </div>
                  </form>
                </div>
              </div>
        </div>
      </div>

      <div class="card-body table-responsive p-0 text-center">
        <table class="table table-hover">
          <thead>
            <tr style="text-align:center; font-weight: bold;">
              <td>Referencia</td>
              <td>Descripción</td>
              <td>Fecha</td>
              <td>Monto</td>
              <td>Estatus</td>
              <td>Acción</td>
            </tr>
          </thead>
          <tbody>
            @foreach($precoms as $precom)
              <tr style="text-align:center; font-size: 0.9rem;">
                <td> {{ $precom->refprc }} </td>
                <td> {{ $precom->desprc }} </td>
                <td> {{ $precom->fecprc }} </td>
                <td> {{ $precom->monprc }} </td>
                @if ($precom->staprc === 'E')
                    
                <td> En espera </td>
                
                @elseif ($precom->staprc === 'A')

                <td>Aprobado</td>

                @elseif($precom->staprc === 'N')

                <td>Rechazado</td>

                @endif
                           
                  <td>
                      @can('edit',App\permission_user::class) 
                    <a href="{{ route('detPrecom', $precom->refprc) }}">
                      <i class="fa fa-edit blue"></i>
                    </a>
                    @endcan 
                    {{-- 
                    /
                      @can('delete',App\permission_user::class) 
                    <a href="" class="borrar" id="deleteCCosto"
                      data-id="" data-toggle="tooltip" data-placement="top" title="Eliminar">
                      <i class="fas fa-trash red"></i>
                    </a>
                    @endcan --}}

                </td>
              </tr>
              @endforeach
          </tbody>
        </table>
        {!!$precoms->appends(request()->input())->render()!!}
        
      </div>
    </div>
    
    {{-- <a type="button" data-toggle="modal" data-target="#modetalles" 
    data-toggle="tooltip" data-placement="top" title="Detalles">
    <i class="fas fa-search-plus"></i>
    </a> --}}
    <!-- </div>
  </div> -->
  </div>

  <div class="modal fade" id="modetalles" tabindex="-1" role="dialog" aria-labelledby="modalempleado" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
     <div class="modal-content"> 
     <div class="modal-header">
      <h5 class="modal-title" id="modalempleadolabel">DETALLES DE CENTRO DE COSTO</h5>
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
       </button>
     </div>
      <div class="modal-body content-centered">
       <div class="col-sm-12">
       <label for="phone">Código</label>
        <div class="form-group">
         <input class="form-control" type="text" id="pass_codigo" readonly>
        </div>
       </div>
       <div class="col-sm-12">
       <label for="codigo">Descripción</label>
        <div class="form-group">
         <input class="form-control" type="text" id="pass_descripcion" readonly>
        </div>
       </div>
       <div class="col-sm-12">
        <label for="fecing">Resposable</label>
         <div class="form-group">
          <input class="form-control" type="text" id="pass_responsable" readonly>
         </div>
      </div>
       <div class="col-sm-12">
        <label for="fecret">Encargado</label>
         <div class="form-group">
          <input class="form-control" type="text" id="pass_encargado" readonly>
         </div>
      </div>
     </div>

    <div class="modal-footer">
     <button type="button" class="btn btn-primary" data-dismiss="modal"><b> Cerrar</b></button>
    </div>
  </div>
</div>
</div>
</section>

@endsection
@section('script')
<script src="{{ asset('js/eliminarCCosto.js') }}" ></script>
<script type="text/javascript">
$(function () {
$('[data-toggle="tooltip"]').tooltip()
  });

  $(document).ready(function () {
  $("#modetalles").on("show.bs.modal", function (e) {
      var codigo = $(e.relatedTarget).data('target-codigo');
      var descripcion = $(e.relatedTarget).data('target-descripcion');
      var responsable = $(e.relatedTarget).data('target-responsable');
      var encargado = $(e.relatedTarget).data('target-encargado');
      $('#pass_codigo').val(codigo);
      //$('#pass_cargo').val(cargo);
      $('#pass_descripcion').val(descripcion);
      $('#pass_responsable').val(responsable);
      $('#pass_encargado').val(encargado);



  });
});

 </script>
@endsection

