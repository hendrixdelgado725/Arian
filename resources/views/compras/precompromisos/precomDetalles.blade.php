@extends('layouts.app')
@extends('layouts.menu')
@section('titulo', 'Detalles del precompromiso')

@section('content')
@component('layouts.contenth')
    @slot('titulo')
        Detalles del precompromiso
    @endslot
@endcomponent

    <section class="content">
        <div class="container-fluid">
          <div class="row justify-content-center">
            <div class="col-10">
              
              <div class="card card-info">
                @include('vendor/flash.flash_message')
                <div class="card-header">
                <h3 class="card-title">Precompromiso</h3>
                </div>
                  <form action="" method="post">
                    {{csrf_field()}}
                  <input type="hidden" name="oldrif" value="">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group">
                            <label for="refprc">Referencia</label>
                              <input type="text" class="form-control" value="{{ $precom->refprc }}" readonly>
                          </div>     
                        </div>
                        <div class="col-sm-2">
                          <div class="form-group">
                          <label>Fecha</label>
                            <div class="input-group mb-3">
                              <input type="text" class="form-control"  
                            value="{{ $precom->fecprc }}" readonly>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-1">
                          <div class="form-group">
                          <label>Tipo</label>
                            <div class="input-group mb-3">
                              <input type="text" class="form-control" 
                            value="{{ $precom->tipprc }}" readonly>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="form-group">
                            <label for=""style="opacity: 0.0;">Tipo</label>
                            <div class="input-group mb-3">
                              @if($precom->tipprc === 'SAE')
                              <input type="text" class="form-control" value="SOLICITUD DE PRE-COMPROMISOS" readonly>
                              @endif
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label for="">Descripción del precompromiso</label> 
                        <div class="input-group">
                          <textarea id="" name="" type="text" class="form-control" readonly>{{ $precom->desprc }}
                          </textarea>
                        </div>
                      </div>
                      <div class="row">
                        
                        <div class="col-sm-2">
                          <div class="form-group">
                            <label for="beneficiario">Beneficiario</label>
                            <div class="input-group">
                              <input type="text" class="form-control" value="{{ $precom->cedrif }}" readonly>  
                            </div> 
    
                          </div>
  
                        </div>

                        <div class="col-sm-3">
                          <div class="form-group">
                            <label for="" style="opacity: 0.0;">Tipo</label>
                            <div class="input-group">
                              <input type="text" class="form-control" value="BENEFICIARIO POR ASIGNAR" readonly >
                            </div>
                          </div>
                        </div>
                      </div>  
                      
                      <div class="form-group py-3">
                        <h6 style="text-align: center"><b>Imputaciones</b></h6>
                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                          <table class="table table-bordered">
                            <thead>
                              <th>N°</th>
                              <th>Código presupuestario</th>
                              <th>Monto</th>
                              <th>Comprometido</th>
                              <th>Causado</th>
                              <th>Pagado</th>
                              <th>Ajuste</th>
                            </thead>
                            <tbody>
                              @foreach ($precom->articles as $item)
                              <tr>
                                <td>{{ $loop->index }}</td>
                                <td width="30%"> {{ $item->codpre }} </td>
                                <td> {{ $item->monimp }} </td>
                                <td> {{ $item->moncom }} </td>
                                <td> {{ $item->moncau }} </td>
                                <td> {{ $item->monpag }} </td>
                                <td> {{ $item->monaju }} </td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                      </div>

                      <div class="form-group py-3" style="width:70em; margin: auto;">
                        <h6 style="text-align: center;"><b>Saldos</b></h6>
                        <table class="table table-bordered">
                          <thead>
                            <th>Ajustado</th>
                            <th>Precompromiso</th>
                            <th>Comprometido</th>
                            <th>Causado</th>
                            <th>Pagado</th>
                            <th>Total</th>
                            <th>Total Eventos</th>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                <input type="text" class="form-control" readonly value="{{ $precom->salaju }}">
                              </td>
                              <td>
                                <input type="text" class="form-control" readonly value="{{ $precom->salaju }}">
                              </td>
                              <td>
                                <input type="text" class="form-control" readonly value="{{ $precom->salaju }}">
                              </td>
                              <td>
                                <input type="text" class="form-control" readonly value="{{ $precom->salaju }}">
                              </td>
                              <td>
                                <input type="text" class="form-control" readonly value="{{ $precom->salaju }}">
                              </td>
                              <td>
                                <input type="text" class="form-control" readonly value="{{ $precom->salaju }}">
                              </td>
                              <td>
                                <input type="text" class="form-control" readonly value="{{ $precom->salaju }}">
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    <div class="card-footer">
                      <a href="{{ url()->previous() }}" class="btn btn-default ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                    </div>
                  </form>
                </div>
                </div>
            </div>
          </div>
        </div>
@endsection

@section('script')
  
@endsection