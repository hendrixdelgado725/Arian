@extends('layouts.app3')
@extends('layouts.menu')
@section('titulo','A1WIN - Registro de Requisición')

@section('content')
  @component('layouts.contenth')
    @slot('titulo')
      Registrar Pedido
    @endslot
  @endcomponent
  @include('vendor/flash.flash_message')
  <form-requisionesp
  :costos="{{ json_encode($costos) }}"
  :user="{{ json_encode($user) }}"
/></form-requisionesp>
{{-- 
:costos="{{ json_encode($tasa) }}"
:articulos="{{ json_encode($articulos) }}"
:recargos="{{ json_encode($recargos) }}"
:descuentos="{{ json_encode($descuentos) }} --}}"
@endsection
