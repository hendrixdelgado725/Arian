@extends('layouts.app')
@extends('layouts.menu')
@section('titulo', 'Cotizaciones')

@section('content')

    @component('layouts.contenth')
        @slot('titulo')
        Listado Cotizaciones
        @endslot
    @endcomponent

    <section class="content">
        <div class="container-fluid">
            <div class="card">
                @include('vendor.flash.flash_message')
                <div class="card-header">
                    <h3 class="card-title"></h3>
                    <div class="card-tools">
                        <div class="row">
                            <div class="col-sm-4 mt-2">
                                @can('create', App\permission_user::class)
                                    <a href="{{ route('cotCreate') }}" class="btn btn-info mt-2" target="_blank">REGISTRAR</a>
                                @endcan
                            </div>
                            <div class="col mt-2">
                                <form action="" method="GET">
                                    @csrf
                                    <div class="input-group">
                                        <input type="text" name="filtro" placeholder="Buscar" class="form-control float-right">
                                        <button type="submit" class="btn btn-default" data-toogle=tooltip>
                                            <i class="fas fa-search"></i>
                                        </button>
                                        <a href="" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar">
                                            <i class="fas fa-arrow-left"></i>
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-body table-responsive p-0 text-center">
                    <table class="table table-hover">
                        <thead>
                            <tr style="text-align: center; font-weight: bold;">
                                <th>Referencia</th>
                                <th>Fecha</th>
                                <th>Descripción</th>
                                <th>Proveedor</th>
                                <th>Pedido</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($cots as $cot)
                                
                        <tr style="text-align: center;">
                            <td>{{ $cot->refcot }}</td>
                            <td>{{ $cot->feccot }}</td>
                            <td>{{ $cot->descot }}</td>
                            <td>{{ $cot->codpro }}</td>
                            <td>{{ $cot->refsol }}</td>
                        </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="card-footer">
                    {!!$cots->appends(request()->input())->render()!!} 
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
@endsection