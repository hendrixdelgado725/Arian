@extends('layouts.app3')
@extends('layouts.menu')
@section('titulo','A1WIN - Registro de Cotizacion')

@section('content')
  @component('layouts.contenth')
    @slot('titulo')
      Registrar Cotización
    @endslot
  @endcomponent
 <cotizacion 
 numero="{{ $numero }}" 
 :monedas="{{ json_encode($monedas) ?? json_encode('[]') }}" 
 :tasa="{{ json_encode($tasa) ?? json_encode('{}') }}" 
 :proveedores="{{ json_encode($proveedores) ?? json_encode('[]') }}" 
 :formas="{{ json_encode($formas) ?? json_encode('[]') }}" 
 :condiciones="{{ json_encode($condiciones) ?? json_encode('[]') }}"
 ></cotizacion>
@endsection
