@extends('layouts.app3')
@extends('layouts.menu')
@section('titulo','Registro de pago')
@section('content')
@component('layouts.contenth')
@slot('titulo')
    Registro de pago
@endslot
@endcomponent

<formpagos numero="{{ $numero }}" :tipos="{{ json_encode($tipos) ?? json_encode('[]') }}" :monedas="{{ json_encode($monedas) ?? json_encode('[]')}}" :ivas="{{ json_encode($ivas) ?? json_encode('[]')}}" :bancos="{{ json_encode($bancos) ?? json_encode('[]')}}" :tasa="{{ json_encode($tasa) ?? json_encode('[]')}}"></formpagos>

@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
    });

    function pasarMayusculas(texto, elemento_id)
    {
    document.getElementById(elemento_id).value=texto.toUpperCase();
    }
</script>
<script src="{{asset('js/reportes.js')}} "></script>
@endsection