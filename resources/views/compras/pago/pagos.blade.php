@extends('layouts.app')
@extends('layouts.menu')
@section('titulo', 'Listado de pagos')
@section('content')
@component('layouts.contenth')
@slot('titulo')
  Listado de Pagos
  @endslot
@endcomponent
<section class="content">
  <div class="content-fluid">
    <div class="card">
      @include('vendor/flash.flash_message')
      <div class="card-header">
        <h3 class="card-title"></h3>
        <div class="card-tools">
          
          <div class="row">
            <div class="col-sm-4 mt-2">
                @can('create', App\permission_user::class)
                <a href="{{ route('pagoCreate') }}" class="btn btn-info" target="_blank"><b>REGISTRAR</b></a>    
                @endcan
                </div>

                <div class="col mt-2">
                  <form action="" method="GET">
                      @csrf
                      <div class="input-group">
                        <input type="text" name="filtro" placeholder="Buscar" class="form-control float-right" value="">
                        <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                        <a href="" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                       
                      </div>
                  </form>
                </div>
              </div>
        </div>
      </div>

      <div class="card-body table-responsive p-0 text-center">
        <table class="table table-hover">
          <thead>
            <tr style="text-align:center; font-weight: bold;">
              <td>Número</td>
              <td>N° Orden</td>
              <td>Fecha</td>
              <td>Monto</td>
              <td>Acciones</td>
            </tr>
          </thead>
          <tbody>
            @foreach($pagos as $pago)
              <tr style="text-align:center;">
                <td> {{ $pago->numpag }} </td>
                <td> {{ $pago->numord }} </td>
                <td> {{ $pago->fecpag }} </td>
                <td> {{ $pago->monpag }} </td>     
                <td>
                  <a class="sendPdf" href="{{ route('generarReportePagos', $pago->id) }}" data-toggle="tooltip" data-placement="top" title="Generar PDF" >
                    <i class="far fa-file-pdf"></i>
                  </a>
                    /
                  @can('edit',App\permission_user::class)
                  <a href="{{ route('editPago', $pago->id) }}" data-toggle="tooltip" data-placement="top" title="Detalles">
                    <i class="fa fa-list"></i>
                  </a>
                  @endcan
                  {{-- @can('delete',App\permission_user::class)
                  /
                  <a href="" id="" 
                  data-id="" class="" data-toggle="tooltip" data-placement="top" title="Eliminar">
                    <i class="fas fa-trash red"></i>
                  </a>
                  @endcan --}}
                </td>
              </tr>
              @endforeach
          </tbody>
        </table>
      </div>
      <div class="card-footer">
        {!!$pagos->appends(request()->input())->render()!!} 
      </div>
    </div>
    
  </div>

@endsection
@section('script')
<script>


</script>

@endsection