@extends('layouts.app')
@extends('layouts.menu')
@section('titulo', 'Pagos')
@section('content')
@component('layouts.contenth')
@slot('titulo')
  Detalles del pago
  @endslot
@endcomponent
<editpago :pago="{{ json_encode($pago) ?? json_encode('{}')}}"/>
@endsection