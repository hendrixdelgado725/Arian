@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Registro de Punto de Cuenta')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Punto de Cuenta
    @endslot
  @endcomponent
  {{-- CONTENT --}}
  <section class="content">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-10">
          
          <div class="card card-info">
            @include('vendor/flash.flash_message')
            <div class="card-header">
            <h3 class="card-title"><b>Registro de Punto de Cuenta</b></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
              <form id="create" action="{{ route('puntoStore') }}" method="post">
                {{csrf_field()}}
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                        <label for="numpta">N° Punto de Cuenta</label>
                          <input type="text" class="form-control" name="numpta" maxlength="14" data-mask="###-#####-####" data-mask-pattern="\w" data-role="input-mask">
                          @error('numpta')
                            <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                      </div>     
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="">Usuario</label>
                        <div class="input-group mb-3">
                          <input type="text" class="form-control" name="user" value="{{ Auth::user()->nomuse }}" readonly>
                          <input type="text" class="form-control" name="user" value="{{ Auth::user()->cedemp }}" style="display: none;">
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                      <label>Fecha</label>
                        <div class="input-group mb-3">
                          <input type="text" class="form-control" name="fecpta" 
                          id="codrif" class="@error('codrif') is-invalid @enderror" readonly
                        value="{{ $date }}"
                          >
                        </div>
                      @error('fecpta')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form group">
                        <label for="codemp">Asunto</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"
                              ><i class="fas fa-user"></i
                            ></span>
                          </div>
                          <textarea
                            class="form-control"
                            name="asunto"
                            id=""
                            cols="3"
                            rows="3"
                          ></textarea>
                        </div>
                      </div>
                    </div>
  
                    <div class="col-sm-6">
                      <div class="form group">
                        <label for="">Exposición de Motivos</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"
                              ><i class="fa fa-comment"></i
                            ></span>
                          </div>
                          <textarea
                            class="form-control"
                            name="motivo"
                            id=""
                            cols="3"
                            rows="3"
                          ></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <h6><b>Imputaciones presupuestarias</b></h6>
                    <div class="table-wrapper-scroll-y my-custom-scrollbar">
                      <table class="table table-bordered">
                        <thead>
                          <th>N°</th>
                          <th>Código presupuestario</th>
                          <th>Descripción</th>
                          <th>Monto</th>
                        </thead>
                        <tbody>
                          @for ($i = 0; $i < 10; $i++)
                            <tr data-row="{{$i}}">
                            <td>{{$i}}</td>
                            <td>
                                <div clas="input-group mb-3">
                                  <input data-row="{{$i}}" id="{{$i}}" class="form-control partidas" readonly type="text" value="">
                                  <button type="button" class="btn btn-black" id="{{$i}}" onclick="codpartida(this.id)" name="button">...</button>
                                </div>
                            </td>
                            <td width="60%">
                              <input data-row="{{$i}}" id="id{{$i}}" class="form-control partidas" readonly name="partidas[{{$i}}]" type="text" value="" style="display: none;">
                              <input data-row="{{$i}}" id="nombre{{$i}}" class="form-control partidas" readonly type="text" value="">
                            </td>
                          <td><input data-row="{{$i}}" name="monto[]" id="{{$i}}" type="number" placeholder="" class="form-control" data-mask="0000.00" ></td>
                          </tr>
                          @endfor
                        </tbody>
                      </table>
                    </div>
                  </div>
              <!-- /.card-body -->
                <div class="card-footer">
                  <button id="createcountry" type="submit" class="btn btn-success" ><b>Guardar Cambios</b></button>
                  <a href="{{ url()->previous() }}" class="btn btn-default ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
        </div>
      </div>
        {{-- row --}}
    </div>
    {{-- container fluid --}}
  </section>
  <div class="modal fade" id="modalempleado2" tabindex="10" role="dialog" aria-labelledby="modalempleado" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
     <div class="modal-content">
     <div class="modal-header label">
       <h2>Listado de Presupuesto</h2>
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
       </button>
     </div>
  
      <div class="modal-body container">
        <table id="datos2" class="table" style="width:100%">
         <thead class="thead-dark" >
         <tr>
             <th>Codigo</th>
             <th>Descripcion</th>
             <th>Accion</th>
           </tr>
          </thead>
  
  
        </table>
  
      </div>
    <div class="modal-footer">
       <button type="button" class="btn btn-primary" data-dismiss="modal"><b> Cerrar</b></button>
      </div>
    </div>
  </div>
  </div>
@endsection
@section('script')
<script src="{{asset('js/empresa/empresa.js')}}"></script>
<script src="{{asset('js/chequeo.js')}} "></script>
<script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
        $('select.select3').select2({
        });
    });
   </script>
@endsection
@section('mayuscula')
<script>
let getId = null;

function codpartida(id){
  console.log(id)
  $('#modalempleado2').modal({ backdrop: "static", keyboard: false });
  $('#datos2').DataTable({
          serverSide: true,
          processing:true,
          ajax: {
            url: '{{route('datosCreate')}}',
            type: 'GET',
            

          },columns:[
            {data:'codpre'},
            {data:'nompre'},
            {data:'action'}
          ],
          destroy:true,
          searching: true,
      });
   getId = id;
   console.log(getId)

}
function getPartidas(id) {
      console.log(id);
      $('#'+getId).val(id.codpre);
      $('#id'+getId).val(id.id);
      $('#nombre'+getId).val(id.nompre);
      $('#modalempleado2').modal('hide');
    }
</script>
@endsection
<style>
.my-custom-scrollbar {
position: relative;
height: 300px;
overflow: auto;
}
.table-wrapper-scroll-y {
display: block;
}
</style>