@extends('layouts.app')
@extends('layouts.menu')
@section('titulo', 'Centros de costo')
@section('content')
@component('layouts.contenth')
@slot('titulo')
  Puntos de cuenta
  @endslot
@endcomponent
<section class="content">
  <div class="content-fluid">
    <div class="card">
      @include('vendor/flash.flash_message')
      <div class="card-header">
        <h3 class="card-title"></h3>
        <div class="card-tools">
          <div class="row">
            <div class="col-sm-4 mt-2">
                @can('create', App\permission_user::class)
                <a href="{{ route('PtoctaCreate') }}" class="btn btn-info"><b>REGISTRAR</b></a>    
                @endcan
                </div>

                <div class="col mt-2">
                  <form action="" method="GET">
                    @csrf
                    <div class="input-group">
                      <input type="text" name="filtro" placeholder="Buscar " class="form-control float-right">
                          <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                          <a href="{{URL::previous()}}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                       
                    </div>
                  </form>
                </div>
              </div>
        </div>
      </div>

      <div class="card-body table-responsive p-0 text-center">
        <table class="table table-hover">
          <thead>
            <tr style="text-align:center; font-weight: bold;">
              <td>N° Punto de cuenta</td>
              <td>Estatus</td>
              <td>Fecha</td>
              <td>Acciones</td>
            </tr>
          </thead>
          <tbody>
            @foreach($puntos as $punto)
              <tr style="text-align:center;">
                <td> {{ $punto->numpta }} </td>
                <td> </td>
                <td> {{ $punto->fecpta }} </td>     
                <td>
                  <a href="" data-toggle="tooltip" data-placement="top" title="Presupuesto"> {{-- Vista de presupuesto --}}
                  <i class="fa fa-edit"></i>
                  </a> 
                </td>
              </tr>
              @endforeach
          </tbody>
        </table>
        {!!$puntos->appends(request()->input())->render()!!}
      </div>
    </div>
    
  </div>


@endsection
@section('script')
<script type="text/javascript">




 </script>
@endsection