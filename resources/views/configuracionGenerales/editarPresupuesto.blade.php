
@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Editar Presupuesto')

  @section('content')

<!--   <div class="content-wrapper">
  <section class="content-header">
   <div class="container-fluid">
     <div class="row mb-2">
      <div class="col-sm-6">
       
      </div>
     </div>
   </div>
  </section>
</div> -->
    
<section class="container-fluid">
    
        
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-info">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title mt-2"><b>EDITAR ARTICULOS DE PRESUPUESTO</b></h3>
                <div class="d-flex flex-row-reverse bd-highlight">
                <!-- <div>
                   <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fas fa-chevron-circle-left"></i><b> Atras</b></a>
                  </div> -->
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form action="{{route('actualizarPresupuesto',['id'=>$presupuesto->id,'id2' => $id])}}" method="POST">
              {{csrf_field()}}
               <div class="card-body">
                <div class="row">

                 <div class="col-sm-10">
                  <div class="form-group">
                   <label for="cliente" class="control-label">Cliente</label>
                    <select name="cliente" class="form-control select2 select2-danger" data-dropdown-css-class="select2-danger" class="@error('cliente') is-invalid @enderror" aria-hidden="true"  id="clientes" style="width: 100%;">
                   <option disabled selected>SELECCIONE UN CLIENTE</option>
                    <option value="{{$presupuesto->cliente->codpro}}" selected>{{$presupuesto->cliente->nompro}}</option>
                   </select>
                   @error('cliente')
                     <div class="alert alert-danger">{!!$message!!}</div>
                   @enderror
                  </div>
                 </div>

                 <div class="col-sm-2">
                  <a type="button" class="btn btn-info mt-4 ml-2" data-toggle="modal" data-target="#nuevocliente"><b>Nuevo Cliente</b></a>
                 </div>
                </div> <!--row -->
<!-- -->
                  <div class="form-group table-responsive">
                    <h6><b>Articulos</b></h6>
                    <div class="table-wrapper-scroll-y my-custom-scrollbar">
                      <table class="table table-bordered" style="width: 100%; border-collapse: collapse;">
                        <thead>
                          <!-- <th>N°</th> -->
                          <th style='width:150px'>Articulo</th>
                          <th>Cantidad</th>
                          <th>Descuento</th>
                          <th>Eliminar</th>
                        </thead>
                        <tbody>
                          {{-- para los articulos seleccionados --}}
                          @for ($i = 0; $i < $presupuesto->articulos->count(); $i++)
                          <?php $selected = [];$art = '';?>
                          <tr>
                            <td style="display:none">
                             <input type="hidden" class="form-control id" value="{{$presupuesto->articulos[$i]->id}}">
                            </td>
                            <td style="width: 35%">
                              <select name="articulos[]" class="form-control select2 articulo" data-href="{{route('getTalla')}}">
                                <option value="{{$presupuesto->articulos[$i]->codart}}">{{$presupuesto->articulos[$i]->codart.'-'.$presupuesto->articulos[$i]->desart}}</option>
                              </select>
                             
                            </td>
                                <td>
                                  <input name="cantidad[]" type="text" class="form-control cantidad" value="{{$presupuesto->articulos[$i]->cantart}}">
                                </td>
                                <td>
                             <div>
                              <select name="descuentos[]" class="form-control descuento" class="@error('descuentos[]') is-invalid @enderror" aria-hidden="true">
                               @if($presupuesto->articulos[$i]->discount)
                               <option selected value="{{$presupuesto->articulos[$i]->discount->desdesc}}">{{$presupuesto->articulos[$i]->discount->desdesc}}</option>
                               @endif
                               @foreach($descuentos as $descuento)
                               <option value="{{$descuento->desdesc}}">{{$descuento->desdesc}}</option>
                               @endforeach
                              </select>
                             </div>
                            </td>
                                <td style="text-align:center">
                                  <a id="deleterow" class="deleterow" data-target-artid="{{$presupuesto->articulos[$i]->id}}" href=""><i class="fa fa-trash"></i></a>
                                </td>
                              </tr>
                          @endfor
                              {{-- para los articulos a agregar --}}
                            @for(;$i < 11-$presupuesto->articulos->count(); $i++)
                              <tr data-row="{{$i}}">
                              <td style="display:none">{{$i}}</td>
                              <td>
                              <select data-row="{{$i}}" name="articulos[]" class="form-control select2 articulos" data-href="{{route('getTalla')}}">
                                  <option value="">Seleccione un articulo</option>
                                  @foreach ($articulos as $articulo)
                                    <option value="{{$articulo->codart}}">{{$articulo->codart.' - '.$articulo->desart}}</option>
                                  @endforeach
                                </select>
                              </td>
                              <!-- <td class="talla">
                              <select name="tallas[]" class="form-control tallas" data-dropdown-css-class="select2-danger" aria-hidden="true">
                               <option selected value="">SELECCIONE UNA TALLA</option>
                               @foreach ($tallas as $item)
                               <option value='{{$item->codtallas}}'>{{$item->tallas}}</option>

                               @endforeach
                              </select>
                              @error('tallas[]')
                                <div class="alert alert-danger">{!!$message!!}</div>
                              @enderror
                            </td> -->
                            <td><input data-row="{{$i}}" name="cantidad[]" type="text" class="form-control"></td>
                            <td>
                             <div>

                              <select name="descuentos[]" class="form-control descuento" class="@error('descuentos[]') is-invalid @enderror" aria-hidden="true">
                                  <option value="">Seleccione un descuento</option>
                               @foreach($descuentos as $descuento)
                               <option value="{{$descuento->desdesc}}">{{$descuento->desdesc}}</option>
                               @endforeach
                              </select>
                             </div>
                            </td>
                            </tr>
                            @endfor
                        </tbody>
                      </table>
                    </div>
                  </div>

            <div class="row">
            <div class="col-sm-6">
                  <div class="form-group">
                  <label for="cargo">Moneda</label>
                    <select class="form-control" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="moneda" class="@error('moneda') is-invalid @enderror">
                    <option selected="selected" disabled>SELECCIONE UNA MONEDA</option>
                        <option value="{{$presupuesto->moneda->nombre}}" selected>{{$presupuesto->moneda->nombre}}</option>
                    </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                       @error('moneda')
                       <div class="alert alert-danger">{!!$message!!}</div>
                        @enderror
                       </div>     
                      </div>
                      
               <div class="col-sm-6">
                 <div class="form-group">
                  <label for="cargo">Tasa de Cambio</label>
                    <select class="form-control" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="tasa" class="@error('tasa') is-invalid @enderror">
                    <option selected="selected" disabled>SELECCIONE UNA TASACIÓN</option>
                        @if($presupuesto->tcambio != null)
                        <option selected value="{{$presupuesto->tcambio->id}}">{{'1 '. $presupuesto->cambioMoneda->nombre.' = '.$presupuesto->tcambio->valor.' '.$presupuesto->cambioMoneda2->nomenclatura}}</option>
                        @else
                        <option value="" selected>SIN TASACIÓN</option>
                        @endif
                    </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                    @error('tasa')
                     <div class="alert alert-danger">{!!$message!!}</div>
                   @enderror
                     </div>     
                 </div>

                </div> <!--row --> 

                 <div class="row">
                      <div class="col-sm-6">
                       <div class="form-group">
                       <label for="cargo">Recargo</label>
                        <select class="form-control" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="recargo" class="@error('recargo') is-invalid @enderror">
                         <option>{{$presupuesto->recargo->nomrgo}}</option>
                        </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                         @error('recargo')
                         <div class="alert alert-danger">{!!$message!!}</div>
                         @enderror
                        </div>     
                       </div>

                       <!-- <div class="col-sm-6">
                        <div class="form-group">
                        <label for="cargo">Descuento</label>
                         <select class="form-control" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="descuento" class="@error('descuento') is-invalid @enderror">
                           @if($presupuesto->descuento)
                           <option value="{{$presupuesto->descuento->desdesc}}" selected>{{$presupuesto->descuento->desdesc}}</option>
                           @else
                           <option selected disabled>SIN DESCUENTO</option>
                           @endif
                         </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                           @error('descuento')
                          <div class="alert alert-danger">{!!$message!!}</div>
                           @enderror
                         </div>     
                        </div> -->

                    <!--     <div class="col-sm-6">
                         <div class="form-group">
                         <label for="cargo">Formas de Pago</label>
                          <select class="form-control" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="fpago" class="@error('fpago') is-invalid @enderror">
                           <option selected="selected" disabled>SELECCIONE UNA FORMA DE PAGO</option>
                           @if($presupuesto->fpago)
                           <option value="{{$presupuesto->fpago->destippag}}" selected>{{$presupuesto->fpago->destippag}}</option>
                           @endif
                           </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                          @error('fpago')
                          <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                           </div>     
                          </div> -->
                       <div class="col-sm-6">
                        <div class="form-group">
                        <label for="cargo">Duración de Presupuesto</label>
                         <select class="form-control" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="duracion" class="@error('duracion') is-invalid @enderror">
                          <option selected="selected" disabled>SELECCIONE UNA DURACIÓN</option>
                           @if($presupuesto->valido)
                           <option value="{{$presupuesto->valido->duracion}}" selected>{{$presupuesto->valido->duracion. ' Días'}}</option>
                           @endif
                         </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                           @error('duracion')
                          <div class="alert alert-danger">{!!$message!!}</div>
                           @enderror
                         </div>     
                        </div>
                      </div> <!-- row-->
                    
                    <div class="row">
                    </div>

                    <div>
                    <input id="borrados" type="hidden" name="borrados">
                    </div>
                      
               
                <!-- /.card-body -->
               <div class="row">
                <button type="submit" class="btn btn-success mt-4 ml-2"><b>Guardar Cambios</b></button>
                <div>
                  <a href="{{ route('listaPresupuesto') }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                </div>
               </div>
                <!-- <div class="card-footer">
                </div> -->
              </form>
            </div>
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          
            <!-- /.row -->

            
         </form>
        </div>
            <!-- /.card -->
            <div class="modal fade" id="nuevocliente" tabindex="-1" role="dialog" aria-labelledby="nuevocliente" aria-hidden="true">
           <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content"> 
            <div class="modal-header">
             <h5 class="modal-title" id="nuevoclientelabel"><b> Agregar Cliente</b></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
            </div>
             <div class="modal-body content-centered">
             <div class="row">
                 <div class="col-sm-6">
                 <div class="form-group">
                  <label for="tipodocumento">Tipo de documento</label>
                    <select class="form-control" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="tipodocumento" class="@error('tipodocumento') is-invalid @enderror">
                        <option selected="selected" data-select2-id="3">V</option>
                        <option data-select2-id="43">E</option>
                        <option data-select2-id="44">J</option>
                        <option data-select2-id="45">G</option>
                        <option data-select2-id="46">P</option>
                        <option data-select2-id="47">R</option>
                        <option data-select2-id="48">C</option>
                    </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                    @error('tipodocumento')
                     <div class="alert alert-danger">{!!$message!!}</div>
                   @enderror
                  </div>     
                 </div>

                 <div class="col-sm-6">
                <div class="form-group">
                <label>Numero de documento</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">V/J</span>
                  </div>
                  <input type="text" class="form-control" placeholder="Ejem: 1234567" name="cedularif" 
                  id="cedularifcliente" class="@error('cedularif') is-invalid @enderror">
                </div>
                @error('cedularif')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
               </div>
               </div>

               <div class="form group">
                <label for="nombre">Nombre o razón social</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Aa</span>
                  </div>
                  <input type="text" class="form-control" class="@error('nombre') is-invalid @enderror" placeholder="Ejem: Juan Gutierrez" name="nombre" id="nombrecliente">
                </div>
                @error('nombre')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                <div class="form group">
                    <label for="direccion">Dirección Fiscal</label>
                <div class="input-group mb-3">
                  <textarea type="text" class="form-control" class="@error('direccion') is-invalid @enderror" placeholder="Dirección Fiscal..." name="direccion" id="direccioncliente"> </textarea>
                </div>
                @error('direccion')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                <div class="form group">
                <label for="telefono">Numero de teléfono</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-phone-square"></i></span>
                  </div>
                  <input type="tel" class="form-control" class="@error('telefono') is-invalid @enderror" placeholder="Ejem: 041..." name="telefono" id="telefonocliente"> 
                </div>
                @error('telefono')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                <div class="form group">
                <label for="email">Correo Electrónico</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="email" class="form-control" class="@error('email') is-invalid @enderror"  placeholder="Ejem: Juan@email.com" name="email" id="emailcliente"> 
                </div>
                @error('email')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
              
                 
            </div> <!--modal body -->

           <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><b> Cerrar</b></button>
            <button type="button" class="btn btn-success"  data-dismiss="modal"><b> Guardar</b></button>
           </div>
         </div>
       </div>
     </div>


</div>
</div> 
<input id="arttojson" type="hidden" value="{{$arttojson}}">
<input id="descutojson" type="hidden" value="{{$descutojson}}">
</div> 
  @endsection
  @section('script')
   <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
    });
   </script>
    <script src="{{asset('js/presupuesto.js')}} "></script>
    <style>
     .my-custom-scrollbar {
      position: relative;
      height: 300px;
      overflow: auto;
       }
     .table-wrapper-scroll-y {
     display: block;
       }
     </style>

  @endsection