@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Editar Empresa')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      <!-- Empresa -->
    @endslot
  @endcomponent
  {{-- CONTENT --}}
  <section class="content">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-10">
          
          <div class="card card-info">
            @include('vendor/flash.flash_message')
            <div class="card-header">
            <h3 class="card-title">EDICI&Oacute;N EMPRESA</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
              <form action="{{route('EmpresaUpdate',$empresa->id)}}" method="post" enctype="multipart/form-data">
                {{-- @method('PUT') --}}
                {{csrf_field()}}
              <input type="hidden" name="oldrif" value="{{$empresa->codrif}}">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                        <label for="tipodoc">Documento</label>
                          <select class="form-control select2" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="tipodoc" class="@error('tipodoc') is-invalid @enderror">
                              <option value="V"{{substr($empresa->codrif,0,1)==='V' ? 'selected' : ''}}>V</option>
                              <option value="E"{{substr($empresa->codrif,0,1)==='E' ? 'selected' : ''}}>E</option>
                              <option value="J"{{substr($empresa->codrif,0,1)==='J' ? 'selected' : ''}}>J</option>
                              <option value="G"{{substr($empresa->codrif,0,1)==='G' ? 'selected' : ''}}>G</option>
                          </select>
                          @error('tipodoc')
                            <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                      </div>     
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                      <label>N&uacute;mero</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">V/J</span>
                          </div>
                          <input type="text" class="form-control" placeholder="Ejem: 1234567" name="codrif" 
                          id="codrif" class="@error('codrif') is-invalid @enderror"
                        value="{{substr($empresa->codrif,1)}}" data-mask="99999999-9" readonly>
                        </div>
                      @error('codrif')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                      </div>
                    </div>

                    <div class="col-sm-4">
                      <div class="form-group">
                      <label>Tipo</label>
                        <div class="input-group mb-3">
                          <select class="form-control" name="tipemp" id="tipemp">
                              <option value="0"{{substr($empresa->tipemp,0,1)==='0' ? 'selected' : ''}}>PÚBLICA</option>
                              <option value="1"{{substr($empresa->tipemp,0,1)==='1' ? 'selected' : ''}}>PRIVADA</option>
                          </select>
                        </div>
                      </div>
                    </div>

                  </div>

                  
                  <div class="row">
                    <div class="col-md-8">
                      <div class="form-group">
                        <label for="nomrazon">Nombre o Raz&oacute;n Social</label> 
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <div class="input-group-text">A</div>
                          </div> 
                          <input id="nomrazon" name="nomrazon" type="text" class="form-control @error('nomrazon') is-invalid @enderror" value="{{$empresa->nomrazon}}" onkeyup="pasarMayusculas(this.value, this.id)">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="dirfis">Direcci&oacute;n Fiscal</label> 
                      <textarea id="dirfis" name="dirfis" cols="40" rows="3" class="form-control @error('dirfis') is-invalid @enderror" onkeyup="pasarMayusculas(this.value, this.id)">{{$empresa->dirfis}}</textarea>
                        @error('dirfis')
                            <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="logo">Logo</label> 
                        <div class="input-group">
                          <label for="file-upload" class="custom-file-upload">
                            <i class="fa fa-upload"></i> Subir imagen
                        </label>
                        <input name="logo" id="file-upload" type="file" onchange="mostrarImagen(event)" class="@error('logo') is-invalid @enderror" value="{{old('logo')}}"/>
                        </div>
                        @error('logo')
                            <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                      </div>
                      
                      <img class="logo" id="oldLogo" src="{{asset($empresa->logo)}}" alt="Logo">
                      <img class="logo" id="logo" src="#" alt="Vista previa de la imagen" style="display: none;">
                    </div>
                  </div>
                  {{-- 
                          <img id='logo' src="{{asset($empresa->logo)}}" alt="Logo" class="rounded">
                    
                    
                    <div class="form-group">
                    <label for="codalm">Seleccion de Almacen</label> 
                    <div>
                      <select id="codalm" name="codalm" class="custom-select">
                        @foreach ($almacenes as $item)
                        @php($selected = $item->codalm === $empresa->codalm)
                          <option value="{{$item->codalm}}" {{$selected}}>{{$item->codalm." / ".$item->nomalm}}</option>
                        @endforeach
                      </select>
                    </div>
                    @error('codalm')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                  </div> --}}
                  {{-- @php(var_dump($bankempre)) --}}
                  <div class="form-group">
                    <h6><b>Informaci&oacute;n Bancaria</b></h6>
                    <div class="table-wrapper-scroll-y my-custom-scrollbar">
                      <table class="table table-bordered">
                        <thead>
                          <th>N°</th>
                          <th>Entidad Bancaria</th>
                          <th>N&uacute;mero de Cuenta</th>
                          <th>Limpiar</th>
                        </thead>
                        <tbody>
                          {{-- para los bancos seleccionados --}}
                          @for ($i = 0; $i < count($bankempre); $i++)
                          <?php $selected = [];$numcuenta = '';?>
                          <tr>
                            <td>{{$i}}</td>
                            <td width="50%">
                              <select name="entidad[]" class="form-control entidad select3">
                                <option value="{{$bankempre[$i]->nomban}}">{{$bankempre[$i]->nomban." / ".substr($bankempre[$i]->codban,0,4)}}</option>
                              </select>
                            </td>
                                <td >
                                  <input name="numcuenta[]" type="text" placeholder="Ingrese Numero de cuenta" class="form-control numcuenta" value="{{$bankempre[$i]->codban}}" data-mask="0000-0000-0000-0000-0000">
                                </td>
                                <td>
                                  <a id="deleterow" class="deleterow" href=""><i class="fa fa-trash"></i></a>
                                </td>
                              </tr>
                          @endfor
                              {{-- para los bancos a agregar --}}
                            @for ( ;$i < count($bancos)-count($bankempre); $i++)
                              <tr data-row="{{$i}}">
                              <td>{{$i}}</td>
                              <td width="50%">
                              <select data-row="{{$i}}" name="entidad[]" class="form-control entidad select3">
                                  <option value="">Seleccione el banco</option>
                                  @foreach ($bancos as $item)
                                    <option value="{{$item["nombanc"]}}" data-num="{{$item["num"]}}">{{$item["nombanc"]." / ".$item["num"]}}</option>
                                  @endforeach
                                </select>
                              </td>
                            <td><input data-row="{{$i}}" name="numcuenta[]" type="text" placeholder="Ingrese Numero de cuenta" class="form-control numcuenta" data-mask="0000-0000-0000-0000-0000" ></td>
                            </tr>
                            @endfor
                        </tbody>
                      </table>
                    </div>
                  </div>
              <!-- /.card-body -->
                <div class="card-footer">
                  <button id="createcountry" type="submit" class="btn btn-success"><b>Guardar Cambios</b></button>
                  <a href="{{ url()->previous() }}" class="btn btn-default ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
        </div>
      </div>
        {{-- row --}}
    </div>
  <input id="bankvalues" type="hidden" value="{{$bankempre}}">
  <input id="banktojson" type="hidden" value="{{$banktojson}}">
    {{-- container fluid --}}
  </section>
@endsection
@section('script')
<script src="{{asset('js/empresa/empresa.js')}}"></script>
<script src="{{asset('js/chequeo.js')}} "></script>
<script>
      $(document).ready(()=>{
        $('select.select2').select2({
        });
        $('select.select3').select2({
        });
      })

      function mostrarImagen(event) {
    // Obtener el archivo seleccionado
    const archivo = event.target.files[0];

    // Crear una URL temporal para la imagen
    const urlImagen = URL.createObjectURL(archivo);

    // Mostrar la imagen en la vista previa
    const imagenPreview = document.getElementById('logo');
    const imagenVieja = document.getElementById('oldLogo');
    imagenPreview.src = urlImagen;
    imagenPreview.style.display = 'block';
    imagenVieja.style.display = 'none';
}
</script>
@endsection

<style>
.my-custom-scrollbar {
position: relative;
height: 300px;
overflow: auto;
}
.table-wrapper-scroll-y {
display: block;
}

.logo {
  width: 175px;
    height: 175px;
    margin: auto; 
    display: block;
  }

  input[type="file"] {
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    border-radius: 0.25rem;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
</style>