@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Empresas')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Listado de Empresas
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <!-- <div class="row justify-content-center"> -->
      <!-- <div class="col-12"> -->
        <div class="card">
          @include('vendor/flash.flash_message')
          <div class="card-header">
            <h3 class="card-title"></h3>
              <div class="card-tools">
                <div class="row">
                <div class="col-sm-4 mt-2" >
                   @can('create',App\permission_user::class)
                  <a href="{{route('EmpresaCreate')}}" class="btn btn-info  mt-2"><b>REGISTRAR</b></a>
                  @endcan
                </div>
                <div class="col mt-2">
                  
                  <form action="{{route('buscar')}}" method="post">
                    @csrf
                    <div class="input-group  mt-2">
                      <input type="text" name="filtro" placeholder="Buscar" class="form-control float-right">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                      <a href="{{route('EmpresaList')}}" class="btn btn-default"><i class="fas fa-arrow-left"></i></a>                       
                     </div>
                  </form>
                </div>
              </div>
              </div>
          </div>

          <div class="card-body">
            <table class="table text-center">
              <thead>
                <tr>
                  <th scope="col">Logo</th>
                  <th scope="col">RIF</th>
                  <th scope="col">Nombre</th>
                  <th scope="col">Acci&oacute;n</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($empresas as $item)
                @php
                        $rand  = rand(1, 9999);
                        $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                        $id=Helper::EncriptarDatos($nrand.'-'.$item->id);
                      @endphp
                  <tr>
                    <td> 
                      <img id='logo' src="{{asset($item->logo)}}" alt="Logo" class="rounded"> 
                    </td>
                    <td>{{$item->codrif}}</td>
                    <td>{{$item->nomrazon}}</td>
                    <td>
                      @can('edit',App\permission_user::class)
                      <a href="{{route('EmpresaShow',$id)}}"><i class="fa fa-edit"></i></a>
                      /
                      @endcan
                       @can('delete',App\permission_user::class)
                      <a id='delete' href="{{route('EmpresaDelete',$id)}}" data-id="{{$item->id}}"><i class="fa fa-trash red"></i></a>
                      @endcan
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              {!!$empresas->appends(request()->input())->render()!!}

              {{-- col8  --}}
          </div>
          </div>
        <!-- </div> -->
    <!-- </div> -->
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section>
@endsection
@section('script')
<script>
  $(document).ready(()=>{
    deleteempresa();
      function deleteempresa(){//se insertaran paises a la configuracion del sistema siempre y cuando Existan
        $('a#delete').click(function(e){
          e.preventDefault();
          const row = $(this).parents('tr');
          const url = $(this).attr('href');
		  		Swal.fire({
				  title: 'Seguro que Desea Eliminar?',
				  text: "Despues de esto no se encontrara en la lista!",
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si, Eliminar!'
        }).then((result) => {//dio click en el boton aceptar
          if(result.value){
            $.get(url,function(result){
              if (result.exito) {
                Swal.fire(
                  'Ha sido Eliminado!',
                  'Con Exito.',
                  'success',
                )  
              row.fadeOut();
              }else if(result.error){
                Swal.fire({
                  icon: 'warning',
                  title: result.error,
                  timer:1500
                  })
                }
            })
          }      
        })
		});
    }
  })

</script>
@endsection
<style>
  #logo {
    width: 50px;
    height: 50px;
  }
</style>