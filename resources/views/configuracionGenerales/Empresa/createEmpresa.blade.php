@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Registro de Empresa')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Empresa
    @endslot
  @endcomponent
  {{-- CONTENT --}}
  <section class="content">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-10">
          
          <div class="card card-info">
            @include('vendor/flash.flash_message')
            <div class="card-header">
            <h3 class="card-title"><b>Registro de Empresa</b></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
              <form id="create" action="{{route('EmpresaSave')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                        <label for="tipodoc">Documento</label>
                          <select class="form-control" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="tipodoc" class="@error('tipodoc') is-invalid @enderror">
                              <option data-select2-id="3">V</option>
                              <option data-select2-id="43">E</option>
                              <option data-select2-id="44">J</option>
                              <option data-select2-id="45">G</option>
                          </select>
                          @error('tipodoc')
                            <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                      </div>     
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                      <label>N&uacute;mero</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">V/J</span>
                          </div>
                          <input data-mask="99999999-9" type="text" class="form-control" placeholder="Ejem: 1234567" name="codrif" 
                          id="codrif" class="@error('codrif') is-invalid @enderror"
                        value="{{old('codrif')}}"
                          >
                        </div>
                      @error('codrif')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                      <label>Tipo</label>
                        <div class="input-group mb-3">
                          <select class="form-control" name="tipemp" id="tipemp" value="{{old('tipemp')}}">
                            <option selected disabled>Seleccione Tipo de Empresa</option>
                            <option value="0">PÚBLICA</option>
                            <option value="1">PRIVADA</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="col-md-8">
                      <div class="form-group">
                        <label for="nomrazon">Nombre o Raz&oacute;n Social</label> 
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <div class="input-group-text">A</div>
                          </div> 
                          <input id="nomrazon" name="nomrazon" type="text" class="form-control @error('nomrazon') is-invalid @enderror" value="{{old('nomrazon')}}" onkeyup="pasarMayusculas(this.value, this.id)">
                        </div>
                        @error('nomrazon')
                            <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                      </div>
                      <div class="form-group">
                        <label for="dirfis">Dirección Fiscal</label> 
                        <textarea  id="dirfis" name="dirfis" cols="40"  onkeyup="pasarMayusculas(this.value, this.id)" rows="3" class="form-control @error('dirfis') is-invalid @enderror">{{old('dirfis')}} </textarea>
                        @error('dirfis')
                            <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="logo">Logo</label> 
                        <div class="input-group">
                          <input name="logo" type="file" onchange="mostrarImagen(event)" class="form-control-file @error('logo') is-invalid @enderror" value="{{old('logo')}}">
                          <label for="file-upload" class="custom-file-upload">
                            <i class="fa fa-upload"></i> Subir imagen
                        </label>
                        <input name="logo" id="file-upload" type="file" onchange="mostrarImagen(event)" class="@error('logo') is-invalid @enderror" value="{{old('logo')}}"/>
                        </div>
                        @error('logo')
                            <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                      </div>
                      <img id="logo" src="#" alt="Vista previa de la imagen" style="display: none;">
                    </div>
                  </div>
                  <div class="form-group">
                    <h6><b>Informaci&oacute;n Bancaria</b></h6>
                    <div class="table-wrapper-scroll-y my-custom-scrollbar">
                      <table class="table table-bordered">
                        <thead>
                          <th>N°</th>
                          <th>Entidad Bancaria</th>
                          <th>N&uacute;mero de Cuenta</th>
                        </thead>
                        <tbody>
                          @for ($i = 0; $i < count($bancos); $i++)
                            <tr data-row="{{$i}}">
                            <td>{{$i}}</td>
                            <td width="55%">
                            <select data-row="{{$i}}" name="entidad[]" class="form-control entidad select3">
                                <option value="">Seleccione el banco</option>
                                @foreach ($bancos as $item)
                                  <option value="{{$item["nombanc"]}}" data-num="{{$item["num"]}}">{{$item["nombanc"]." / ".$item["num"]}}</option>
                                @endforeach
                              </select>
                            </td>
                          <td><input data-row="{{$i}}" name="numcuenta[]" type="text" placeholder="Ingrese Numero de cuenta" class="form-control numcuenta" data-mask="0000-0000-0000-0000-0000" ></td>
                          </tr>
                          @endfor
                        </tbody>
                      </table>
                    </div>
                  </div>
              <!-- /.card-body -->
                <div class="card-footer">
                  <button id="createcountry" type="submit" class="btn btn-success" ><b>Guardar Cambios</b></button>
                  <a href="{{ url()->previous() }}" class="btn btn-default ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
        </div>
      </div>
        {{-- row --}}
    </div>
    {{-- container fluid --}}
  </section>
@endsection
@section('script')
<script src="{{asset('js/empresa/empresa.js')}}"></script>
<script src="{{asset('js/chequeo.js')}} "></script>
<script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
        $('select.select3').select2({
        });
    });

    function mostrarImagen(event) {
    // Obtener el archivo seleccionado
    const archivo = event.target.files[0];

    // Crear una URL temporal para la imagen
    const urlImagen = URL.createObjectURL(archivo);

    // Mostrar la imagen en la vista previa
    const imagenPreview = document.getElementById('logo');
    imagenPreview.src = urlImagen;
    imagenPreview.style.display = 'block';
}
   </script>
@endsection
<style>
.my-custom-scrollbar {
position: relative;
height: 300px;
overflow: auto;
}
.table-wrapper-scroll-y {
display: block;
}

#logo {
    width: 175px;
    height: 175px;
    margin: auto
  }

  input[type="file"] {
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    border-radius: 0.25rem;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
</style>