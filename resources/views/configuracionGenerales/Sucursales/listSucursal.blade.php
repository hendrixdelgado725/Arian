@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Sucursales')
@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Listado de Sucursales
    @endslot
  @endcomponent
  {{-- CONTENT --}}
  <section class="content">
    <div class="container-fluid">
      <!-- <div class="row justify-content-center"> -->
          <!-- <div class="col-12"> -->
              <div class="card">
              @include('vendor/flash.flash_message')
                <div class="card-header">
                  <h3 class="card-title"></h3>
                    <div class="card-tools">
                      <div class="row">
                        <div class="col-sm-4 mt-2">
                          @can('create', App\permission_user::class)
                            <a href="{{route('sucursalCreate')}}" class="btn btn-info mt-2"><b>REGISTRAR</b></a>
                          @endcan
                        </div>
                        <div class="col mt-2">
                          <form action="{{route('buscarSucursal')}}" method="GET">
                            @csrf
                            <div class="input-group mt-2">
                              <input type="text" name="filtro" placeholder="Buscar" class="form-control float-right">
                              <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                              <a href="{{route('sucursalList')}}" class="btn btn-default"><i class="fas fa-arrow-left"></i></a>                       
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="card-body">
                  <table class="table text-center">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Direcci&oacute;n Fiscal</th>
                        <th scope="col">Pa&iacute;s</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Acci&oacute;n</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($sucursal as $item)
                      @php
                      $id = Helper::crypt($item->id)
                      @endphp
                      <tr>
                          <td>{{$sucursal->firstItem() + $loop->index}}</td>
                          <td>{{$item->nomsucu}}</td>
                          <td>{{$item->dirfis}}</td>
                          <td>{{$item->pais->nompai ?? ''}}</td>
                          <td>{{$item->estados->nomedo ?? ''}}</td>
                          <td>
                            @can('edit', App\permission_user::class)
                            <a href="{{route('sucursalShow',$id)}}"><i class="fa fa-edit"></i></a>
                            /
                            @endcan
                            @can('delete', App\permission_user::class)
                            <a id="delete" data-name="{{$item->nomsucu}}" href="{{route('sucursalDelete',$item->id)}}"><i class="fa fa-trash red"></i></a>
                            @endcan      
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                  {!!$sucursal->appends(request()->input())->render()!!}
                    {{-- col8  --}}
                </div>

              </div>
              <!-- /.card -->
          <!-- </div> -->
        <!-- </div> -->
    </div>
    {{-- container fluid --}}
  </section>
@endsection
@section('script')
<script src="{{ asset('js/sucursales/sucursales.js') }}" ></script>
@endsection