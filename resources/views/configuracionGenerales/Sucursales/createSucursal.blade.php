@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Registro Sucursal')
@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
     <!--  Sucursal -->
    @endslot
  @endcomponent
  {{-- CONTENT --}}
  <section class="content">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-8">
          <div class="card card-info">
            @include('vendor/flash.flash_message')
            <div class="card-header">
            <h3 class="card-title"><b>REGISTRO SUCURSAL</b></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
              <form action="{{route('sucursalSave')}}" method="post">
                {{csrf_field()}}
                <div class="card-body">

                  <div class="form-group">
                    <label for="nomsucu">Nombre</label> 
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <div class="input-group-text">Aa</div>
                      </div> 
                      <input id="nomsucu" name="nomsucu" type="text" onkeyup="pasarMayusculas(this.value,this.id)" class="form-control @error('nomsucu') is-invalid @enderror"
                    value="{{old('nomsucu')}}">
                    </div>
                    @error('nomsucu')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                  </div>

                  <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                        <label for="tipodoc">Documento</label>
                          <select class="form-control select2" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="tipodoc" class="@error('tipodoc') is-invalid @enderror">
                              <option selected data-select2-id="43">E</option>
                              <option data-select2-id="44">J</option>
                              <option data-select2-id="45">G</option>
                          </select>
                          @error('tipodoc')
                            <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                      </div>     
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                      <label>N&uacute;mero</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"></span>
                          </div>
                          <input data-mask="99999999-9" type="text" class="form-control" placeholder="Ejem: 1234567" name="codrif" 
                          id="codrif" class="@error('codrif') is-invalid @enderror"
                        value="{{substr(old('codrif'),1)}}">
                        </div>
                      @error('codrif')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="dirfis">Direcci&oacute;n Fiscal</label> 
                    <textarea id="dirfis" name="dirfis" cols="40" rows="3" onkeyup="pasarMayusculas(this.value,this.id)" class="form-control @error('dirsucu') is-invalid @enderror">{{old('dirfis')}}</textarea>
                    @error('dirfis')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                  </div>

                  <div class="form-group">
                    <label for="codempre">Empresa</label> 
                    <select name="codempre" id="codempre" class="form-control">
                      <option value="" selected>Seleccione Empresa</option>
                      @foreach ($empresa as $item)
                        <option value="{{$item->codrif}}" {{old('codempre') ===$item->codrif ? 'selected':''}}>{{$item->nomrazon}}</option>
                      @endforeach
                    </select>
                    @error('codempre')
                        <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                  </div>

                  <div class="form-group mt-2">
                      <div class="row">
                        <div class="col-xl-12 col-md-8">
                          <h6><b>Selecci&oacute;n Recargos</b></h6>
                        <!-- <h6 id="h6recargos" class="mt-2"><b>Cantidad de Recargos Selecionados : </b></h6> -->
                          <div class="table-wrapper-scroll-y my-custom-scrollbar">
                            <table class="table table-bordered">
                              <thead>
                                <th>N°</th>
                                <th>Recargo</th>
                              </thead>
                              <tbody>
                                @for ($i = 0; $i < count($recargos); $i++)
                                  <tr data-row="{{$i}}">
                                    <td>{{$i}}</td>
                                    <td width="100%">
                                  <select data-row="{{$i}}" name="codrgo[]" class="form-control entidad select3">
                                    <option value="">Seleccione Recargo</option>
                                      @foreach ($recargos as $item)
                                      <option value="{{$item->codrgo}}" data-num="{{$item->nomrgo}}">{{$item->nomrgo}}</option>
                                      @endforeach
                                    </select>
                                  </td>
                                </tr>
                                @endfor
                              </tbody>
                            </table>
                          </div>

                       <div class="row">
<!--                         <div class="col-sm-4">
                          <div class="form-group">
                          <label for="transportnom">Nombre de Producto</label> 
                           <div class="input-group"> 
                            <input id="transportnom" name="transportnom" type="text" class="form-control @error('transportnom') is-invalid @enderror"
                    value="{{old('transportnom')}}">
                           </div>
                           @error('transportnom')
                            <div class="alert alert-danger">{!!$message!!}</div>
                           @enderror
                         </div>
                         </div> -->

                         <div class="col-sm-6">
                          <div class="form-group">
                          <label for="transportcost">Costo Transporte</label> 
                           <div class="input-group">
                            <div class="input-group-prepend">
                             <div class="input-group-text"><i class="fas fa-truck"></i></div>
                            </div> 
                            <input id="transportcost" name="transportcost" type="text" onKeypress = "javascript:return NumerosReales(event)" class="form-control @error('transportcost') is-invalid @enderror"
                    value="{{old('transportcost')}}">
                           </div>
                           @error('transportcost')
                            <div class="alert alert-danger">{!!$message!!}</div>
                           @enderror
                         </div>
                         </div>

                         <div class="col-sm-6">
                           <div class="form-group">
                          <label for="moneda">Moneda</label> 
                           <select name="moneda" id="moneda" class="form-control select2">
                            <option value="" selected>Seleccione Moneda</option>
                            @foreach ($monedas as $moneda)
                            <option value="{{$moneda->codigoid}}" {{old('moneda') ===$moneda->codigoid ? 'selected':''}}>{{$moneda->nombre}}</option>
                            @endforeach
                           </select>
                           @error('moneda')
                            <div class="alert alert-danger">{!!$message!!}</div>
                           @enderror
                         </div>
                         </div>
                      </div>

                        

                         


                        </div>
                      </div>
                    </div>
                    
                  <div class="row">
                    <div class="col-sm-6">
                      <label for="codpai" >Pa&iacute;s</label>
                        <div class="form-group">
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text">V/J</span>
                            </div>
                        <select id="codpai" name="codpai" class="custom-select"
                          data-uri="{{route('getEstJson')}}"
                        >
                          @foreach ($pais as $item)
                            <option value="{{$item->id}}">{{$item->nompai}}</option>
                          @endforeach
                        </select>
                          @error('codpai')
                            <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                        </div>     
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                      <label for="codedo">Estado</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">V/J</span>
                          </div>
                          <select id="codedo" name="codedo" class="custom-select">
                            <option value="">Seleccione Estado</option>
                          </select>
                        </div>
                      @error('codedo')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                      </div>
                    </div>
                  </div>
                  
              <!-- /.card-body -->
                <div class="card-footer">
                  <button id="createcountry" type="submit" class="btn btn-success"><b>Guardar Cambios</b></button>
                  <a href="{{ url()->previous() }}" class="btn btn-default ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                  
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
        </div>
      </div>
        {{-- row --}}
    </div>
    {{-- container fluid --}}
  </section>
@endsection
@section('script')
<script src="{{ asset('js/sucursales/sucursales.js') }}" ></script>
<script src="{{asset('js/chequeo.js')}} "></script>
<script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
        $('select.select3').select2({
        });
    });
   </script>
@endsection

<style>
  .my-custom-scrollbar {
  position: relative;
  height: 300px;
  overflow: auto;
  }
  .table-wrapper-scroll-y {
  display: block;
  }
  </style>