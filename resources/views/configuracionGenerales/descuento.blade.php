@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Descuentos')

@section('content')

@component('layouts.contenth')
    @slot('titulo')
      Listado de Descuento
    @endslot
  @endcomponent

<section class="content">
	<div class="content-fluid">
	<!-- <div class="container">
		<div class="col-12"> -->
		<div class="card">
			@include('vendor/flash.flash_message')
			<div class="card-header">
				<h3 class="card-title"></h3>
				<div class="card-tools">
					<div class="row">
						<div class="col-sm-4 mt-2">
						 @can('create', App\permission_user::class)
						 <a href="{{route('nuevoDescuento')}}" class="btn btn-info"><b>REGISTRAR</b></a>    
						 @endcan
          					
        				</div>

        				<div class="col mt-2">
        					<form action="{{route('buscarDescuento')}}" method="POST">
        						@csrf
        						<div class="input-group">
        							<input type="text" name="filtro" placeholder="Buscar " class="form-control float-right">
    	          					<button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
        	      					<a href="" class="btn btn-default"><i class="fas fa-arrow-left"></i></a>                       
        						</div>
        					</form>
        				</div>
        			</div>
				</div>
			</div>

			<div class="card-body table-responsive p-0 text-center">
				<table class="table table-hover">
					<thead>
						<tr style="text-align:center; font-weight: bold;">
							<td>#</td>
							<td>Descripci&oacute;n</td>
							<td>Valor</td>
							<td>Acci&oacute;n</td>
						</tr>
					</thead>
					<tbody>
						@foreach($descuento as $desc)
							@php
								$rand  = rand(1, 9999);
                    			$nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                    			$enc=Helper::EncriptarDatos($nrand.'-'. $desc->id);
                    			$borr=Helper::EncriptarDatos($nrand.'|'.$desc->id.'|'.$desc->codigoid);
							@endphp
							<tr>
								<td style="text-align:center">{{$descuento->firstItem() + $loop->index}}</td>
								<td style="text-align:center">{{$desc->desdesc}}</td>
								<td style="text-align:center">{{$desc->mondesc.' %'}}</td>
								<td style="text-align:center">
									<div class="button_action" style="text-align:center">
				                     @can('edit', App\permission_user::class)
				                         
				                      <a title="Edición" href="{{ route('editarDescuentoForm',$enc)}}"><i class="fa fa-edit blue"></i></a>
				                      /

				                     @endcan
				                     @can('delete', App\permission_user::class)
				                     <a href="{{ route('eliminarDescuento', $borr)}}" id="deletedescuento" 
				                      data-id="{{$desc->id}}" class="borrardescuento">
				                        <i class="fas fa-trash red"></i>
				                     @endcan
	
				                      </a>
			                      	</div>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				{!!$descuento->render()!!}
			</div>
		</div>
		<!-- </div>
	</div> -->
	</div>
</section>

@endsection
@section('script')
<script src="{{asset('js/funciones.js')}} "></script>
@endsection