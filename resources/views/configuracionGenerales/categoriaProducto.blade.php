@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Categorias de Producto')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Categor&iacute;as de Producto
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">
                  <div class="col-sm-4 mt-2">
                   <a href="{{route('nuevaCdeProducto')}}" class="btn btn-info mt-2"><b> REGISTRAR</b></a>
                  </div>

                <div class="col mt-2">
                  <form action="{{route('buscarCdeProducto')}}" method="GET">
                    @csrf
                     <div class="input-group mt-2">
                      <input type="text" name="filtro" placeholder="Buscar " class="form-control float-right">
                      <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Buscar"><i class="fas fa-search"></i></button>
                      <a href="{{route('listaCdeProducto')}}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                       
                     </div>
                  </form>
                  
                </div><!--col-->
                 </div><!--row -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>#</th>
                      <th>Nombre</th>
                      <th>Nomenclatura</th>
                      <th>Creado el</th>
                      <th>Actualizado el</th>
                      <th>Acci&oacute;n</th>
                    </tr>
                  </thead>
                  <tbody style="text-align:center"> 
                  @foreach($categorias as $categoria)
                  @php
                    $rand  = rand(1, 9999);
                    $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                    $enc=Helper::EncriptarDatos($nrand.'-'.$categoria->id);
                  @endphp
                    <tr>
                      <td>{{$categorias->firstItem() + $loop->index}}</td>
                      <td>{{$categoria->nombre}}</td>
                      <td>{{$categoria->nomencat}}</td>
                      <td>{{date("d-m-Y H:m:s", strtotime($categoria->created_at))}}</td>
                      <td>{{date("d-m-Y H:m:s", strtotime($categoria->updated_at))}}</td>
                      <td>
                        <a href="{{route('editarCdeProducto',$enc)}}" data-toggle="tooltip" data-placement="top" title="Editar">
                          <i class="fa fa-edit blue"></i>
                        </a>
                        /
                        <a href="{{route('eliminarCdeProducto',$enc)}}" class="borrar" id="deletecategoria" 
                        data-id="$enc" data-toggle="tooltip" data-placement="top" title="Eliminar">
                          <i class="fas fa-trash red"></i>
                        </a>
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
                {!!$categorias->appends(request()->input())->render()!!}
               <!--  -->
               {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section> 
  
@endsection
@section('script')
<script src="{{ asset('js/eliminarCProducto.js') }}" ></script>
   <script type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
    });
   </script>
@endsection 



