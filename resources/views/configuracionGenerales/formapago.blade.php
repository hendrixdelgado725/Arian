@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Forma de Pago')

@section('content')

 @component('layouts.contenth')
    @slot('titulo')
      Listado de Forma de Pago
    @endslot
  @endcomponent

<section class="content">
  <div class="content-fluid">
 <!--    <div class="container">
      <div class="col-12"> -->
        <div class="card">
          @include('vendor/flash.flash_message')
          <div class="card-header">
            <h3 class="card-title"></h3>
            <div class="card-tools">
              <div class="row">
                <div class="col-sm-4 mt-2">
                   @can('create',App\permission_user::class)
                  <a href="{{ route('nuevaForma')}}" class="btn btn-info"><b>REGISTRAR</b></a>
                  @endcan
                </div>

                <div class="col mt-2">
                  <form action="{{route('buscarFormaPago')}}" method="POST">
                    @csrf
                    <div class="input-group">
                      <input type="text" name="filtro" placeholder="Buscar " class="form-control float-right">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                      <a href="{{--route('listaFormaPago')--}}" class="btn btn-default"><i class="fas fa-arrow-left"></i></a>                       
                     </div>
                  </form>
                </div>
              </div>
            </div>
          </div>


          <div class="card-body table-responsive p-0 text-center">
            <table class="table table-hover">
              
                <tr style="text-align:center; font-weight: bold; ">
                <!-- <tr style="text-align:center; font-weight: bold; background-color: #17a2b8; color: #ffffff"> -->
                  <td>#</td>
                  <td>Descripci&oacute;n</td>
                  <td>Acci&oacute;n</td>
                </tr>
              </thead>
              <tbody>
                @foreach($formapago as $fp)
                  @php
                    $rand  = rand(1, 9999);
                    $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                    $enc=Helper::EncriptarDatos($nrand.'-'.$fp->id);
                    $borr=Helper::EncriptarDatos($nrand.'|'.$fp->id.'|'.$fp->codigoid)
                    //dd($enc);
                  @endphp
                  <tr>
                    <td style="text-align:center">{{$formapago->firstItem() + $loop->index}}</td>
                    <td style="text-align:center">{{$fp->destippag}}</td>
                    <td>
                      <div class="button_action" style="text-align:center">
                         @can('edit',App\permission_user::class)
                      <a title="Edición" href="{{ route('editarFormaPagoForm',$enc)}}"><i class="fa fa-edit blue"></i></a>
                      /
                      @endcan
                       @can('delete',App\permission_user::class)
                      <a href="{{ route('eliminarFormaPago', $borr)}}" id="deleteformapago" 
                      data-id="{{$fp->id}}" class="borrarformapago">
                        <i class="fas fa-trash red"></i>
                      </a>
                      @endcan
                      </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            {!!$formapago->render()!!}
          </div>

        </div>
 <!--      </div>
    </div> -->
  </div>
</section>


@endsection
@section('script')
<script src="{{asset('js/funciones.js')}} "></script>
@endsection
