@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Editar Tasa de Cambio')

  @section('content')

<!--   <div class="content-wrapper">
  <section class="content-header">
   <div class="container-fluid">
     <div class="row mb-2">
      <div class="col-sm-6">
       
      </div>
     </div>
   </div>
  </section>
</div> -->
    
    <section class="content-wrapper">
    <div class="container-fluid">
    <div class="container">
        
        <div class="col-10">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title mt-2"><b>EDICI&Oacute;N TASA DE CAMBIO</b></h3>
                <div class="d-flex flex-row-reverse bd-highlight">
                <!-- <div>
                   <a href="{{ url()->previous() }}" class="btn btn-danger"><i class="fas fa-arrow-left"></i> Atras</a>
                  </div> -->
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form action="{{route('actualizarTasa',$tasa->id)}}" method="post">
              {{csrf_field()}}
                <div class="card-body">
                <div class="row">
                <div class="col-sm-6">
                 <div class="form-group">
                  <label for="moneda">Moneda a crear Tasa</label>
                    <select class="form-control" value="{{$tasa->moneda->nombre}}" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="moneda" class="@error('moneda') is-invalid @enderror">
                    <option selected="selected" data-select2-id="3" disabled>Seleccione moneda a crear tasa</option>
                        <option selected value="{{$tasa->moneda->nombre}}">{{$tasa->moneda->nombre}}</option>
                    </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                    @error('moneda')
                     <div class="alert alert-danger">{!!$message!!}</div>
                   @enderror
                </div>     
                 </div>

                <div class="col-sm-6">  
                <div class="form group">
                <label for="cantidad">Cantidad</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">1</span>
                   </div>
                   </div>
                  </div>
                 </div>
                </div> <!--row -->

               <div class="row">
                <div class="col-sm-6">
                 <div class="form-group">
                  <label for="moneda2">Moneda</label>
                    <select class="form-control" value="{{$tasa->moneda2->nombre}}"  style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="moneda2" class="@error('moneda2') is-invalid @enderror">
                    <option selected="selected" data-select2-id="3" disabled>Seleccione Moneda</option>
                        <option selected value="{{$tasa->moneda2->nombre}}">{{$tasa->moneda2->nombre}}</option>
                    </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                    @error('moneda2')
                     <div class="alert alert-danger">{!!$message!!}</div>
                   @enderror
                </div>     
                </div>

                <div class="col-sm-6">  
                 <div class="form group">
                  <label for="unidad">Cantidad</label>
                  <div class="input-group mb-3">
                  <div class="input-group-prepend">
                  </div>
                  <input type="text" class="form-control" value="{{$tasa->valor}}" class="@error('cantidad') is-invalid @enderror" placeholder="Ejem: 200" name="cantidad" id="cantidad">
                  </div>
                 @error('cantidad')
                  <div class="alert alert-danger">{!!$message!!}</div>
                 @enderror
                </div>
                </div>

                </div> <!--- row-->

                </div> 
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-success"><b> Guardar Cambios</b></button>
                  <a href="{{ URL::previous() }}" class="btn btn-default ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                </div>
              </form>
            </div>
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->

            
         </form>
        </div>
            <!-- /.card -->
       </div>
      </div>
      </div>
      </section>
  @endsection
    