@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Registro Empleado')

  @section('content')

<!--   <div class="content-wrapper">
  <section class="content-header">
   <div class="container-fluid">
     <div class="row mb-2">
      <div class="col-sm-6">
       
      </div>
     </div>
   </div>
  </section>
</div> -->
    

    <section class="content-wrapper">
    <div class="container-fluid">
    <div class="container">
        
        <div class="col-10">
            <!-- general form elements -->
            <div class="card card-info">
                @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title mt-2"><b>REGISTRO EMPLEADO</b></h3>
                <div class="d-flex flex-row-reverse bd-highlight">
                  <!-- <div>
                   <a href="{{ url()->previous() }}" class="btn btn-danger"><i class="fas fa-arrow-left"></i><b> Atras</b></a>
                  </div> -->
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form action="{{route('registrarEmpleado')}}" method="post">
              {{csrf_field()}}
                <div class="card-body">
                 <div class="row">
                 <div class="col-sm-4">
                 <div class="form-group">
                  <label for="tipodocumento">Documento</label>
                    <select class="form-control select2" id='select2' style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="tipodocumento" class="@error('tipodocumento') is-invalid @enderror">
                        <option selected="selected" data-select2-id="3">V</option>
                        <option data-select2-id="43">E</option>
                        <option data-select2-id="44">J</option>
                        <option data-select2-id="45">G</option>
                    </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                    @error('tipodocumento')
                     <div class="alert alert-danger">{!!$message!!}</div>
                   @enderror
                </div>     
                 </div>
                 
               <div class="col-sm-8">
                <div class="form-group">
                <label>N&uacute;mero</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">V/J</span>
                  </div>
                  <input type="text" class="form-control" onblur="persona()" placeholder="Ejem: 1234567" name="cedularif" 
                  id="cedularifcliente"  class="@error('cedularif') is-invalid @enderror"  value="{{ old('cedularif') }}">
                </div>
                @error('cedularif')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
               </div>

               <div class="col" style="display:none">
                  <a type="button" data-href="{{route('getPersona')}}" class="btn btn-info mt-4 ml-2" id="verificarCliente"><b>Verificar Persona</b></a>
                 </div>
               </div>

                <div class="form group">
                <label for="nombre">Nombre</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Aa</span>
                  </div>
                  <input type="text" class="form-control" onkeyup="pasarMayusculas(this.value, this.id)" class="@error('nombre') is-invalid @enderror" placeholder="Ejem: Juan Gutierrez" name="nombre" id="nombre"  value="{{ old('nombre') }}">
                </div>
                @error('nombre')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                <div class="form group">
                    <label for="direccion">Direcci&oacute;n de Domicilio</label>
                <div class="input-group mb-3">
                  <textarea type="text" class="form-control" onkeyup="pasarMayusculas(this.value, this.id)" class="@error('direccion') is-invalid @enderror" placeholder="Dirección de domicilio..." name="direccion" id="direccion"> </textarea>
                </div>
                @error('direccion')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                <div class="row">
                <div class="col-sm-6">
                <div class="form group">
                <label for="telefono">N&uacute;mero de tel&eacute;fono celular</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-phone-square"></i></span>
                  </div>
                  <input type="tel" class="form-control" onKeypress = "javascript:return NumerosReales(event)" class="@error('telefono') is-invalid @enderror" placeholder="Ejem: 041..." name="telefono" id="telefonoempleado"  value="{{ old('telefono') }}"> 
                </div>
                @error('telefono')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div>
        
                <div class="col-sm-6">
                <div class="form group">
                <label for="telefono">N&uacute;mero de tel&eacute;fono de habitaci&oacute;n</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-phone"></i></span>
                  </div>
                  <input type="tel" class="form-control" onKeypress = "javascript:return NumerosReales(event)" class="@error('telefono2') is-invalid @enderror" placeholder="Ejem: 026..." name="telefono2" id="telefono2"  value="{{ old('telefono2') }}"> 
                </div>
                @error('telefono2')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div> <!--col -->
                </div>

                <div class="form group">
                <label for="email">Correo Electr&oacute;nico</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="email" class="form-control" class="@error('email') is-invalid @enderror"  placeholder="Ejem: Juan@email.com" name="email" id="emailempleado"  value="{{ old('email') }}"> 
                </div>
                @error('email')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
              
                <div class="row">
                <div class="col-sm-6">
                 <div class="form-group">
                  <label for="cargo">Cargo</label>
                    <select class="form-control"  style="width: 100%;" data-select3-id="2" tabindex="-1" aria-hidden="true" name="cargo" class="@error('cargo') is-invalid @enderror">
                    <option selected="selected" disabled>Seleccione Cargo</option>
                        @foreach ($cargos as $cargo)
                        <option value="{{$cargo->codcar}}">{{$cargo->nomcar}}</option>
                        @endforeach
                    </select><span dir="ltr" data-select3-id="2" style="width: 100%;"><span class="selection">
                    @error('cargo')
                     <div class="alert alert-danger">{!!$message!!}</div>
                   @enderror
                </div>     
                 </div>

                 <div class="col-sm-6">
                 <div class="form-group">
                  <label for="tipodocumento">Estatus del Empleado</label>
                    <select class="form-control" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="estatus" class="@error('estatus') is-invalid @enderror">
                        <option selected="selected" data-select2-id="3">Activo</option>
                        <option data-select2-id="43">Egresado</option>
                    </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                    @error('estatus')
                     <div class="alert alert-danger">{!!$message!!}</div>
                   @enderror
                </div>     
                 </div>
                 </div>

                <div class="row">
                 <div class="col">
                  <div class="form group">
                   <label for="nombre">Fecha de Ingreso</label>
                    <div class="input-group mb-3">
                     <input type="date" class="form-control" class="@error('fechaing') is-invalid @enderror" name="fechaing" id="fechaing">
                    </div>
                    @error('fechaing')
                    <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                  </div>
                  </div>

                 <div class="col">
                  <div class="form group">
                   <label for="nombre">Fecha de Egreso</label>
                    <div class="input-group mb-3">
                    <input type="date" class="form-control" class="@error('fechaegre') is-invalid @enderror" name="fechaegre" id="fechaegre">
                  </div>
                </div>
                </div>
                </div>

                <!-- /.card-body -->

                <div class="row">
                <button type="submit" class="btn btn-success mt-4 ml-2"><b> Guardar Cambios</b></button>
                <div>
                  <a href="{{ URL::previous() }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                </div>
               </div> 

                <!-- <div class="card-footer">
                </div> -->
              </form>
            </div>
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->

            
         </form>
        </div>
            <!-- /.card -->
       </div>
      </div>
      </div>
      </section>
  @endsection
  @section('script')
  <script src="{{asset('js/chequeo.js')}} "></script>
  <script src="{{asset('js/persona.js')}} "></script>
  <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('#select2').select2({
        });
        $('#select3').select2({
        });
    });
   </script>
  @endsection
    
    