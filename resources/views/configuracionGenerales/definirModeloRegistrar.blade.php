@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Editar Color')

  @section('content')

<!--   <div class="content-wrapper">
  <section class="content-header">
   <div class="container-fluid">
     <div class="row mb-2">
      <div class="col-sm-6">
       
      </div>
     </div>
   </div>
  </section>
</div> -->
@component('layouts.contenth')
    @slot('titulo')
      <!-- Crear Marcas -->
    @endslot
  @endcomponent
    
  <section class="content-wrapper">
    <div class="container-fluid">
    <div class="container">
        
        <div class="col-10">
            <!-- general form elements -->
            <div class="card card-info">
              @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b>REGISTRO DE MARCAS</b></h3>
                
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              {!! Form::open(['route'=>'definirMarcas.crearMarcas','method'=>'POST','files'=>'TRUE']) !!}
              {{-- <form action="{{route('crearModelo')}}" method="post" enctype="multipart/form-data"> --}}
              {{csrf_field()}}
                <div class="card-body">
                 <div class="row">
                 <div class="col-sm-6">
                 <div class="form-group">
                  <label for="tipodocumento">Descripci&oacute;n</label>
                     <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Nom</span>
                  </div>
                  <input type="text" class="codigo form-control" placeholder="Ejem: EJEMPLOS" name="nomenc" 
                  id="cedularifcliente" class="@error('codigos') is-invalid @enderror">
                </div>
                @error('codigos')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>     
                 </div>
                 
               
               </div>
               
               <div class="card-footer">
                <button type="submit" class="btn btn-success">Guardar Cambios</button>

                 <a href="{{ URL::previous() }}" class="btn btn-default"><i class="fa fa-chevron-circle-left"></i> Volver</a>
                </div>
              {{-- </form> --}}
            </div>
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->

            
         {!! Form::close() !!}
        </div>
            <!-- /.card -->
       </div>
      </div>
      </div>
  </section>

  @endsection
  @section('script')
  <script>
    $('.codigo').keyup(function(tecla) {
            var texto =  $(this).val().toUpperCase();
            $(this).val(texto); 
        });
  </script>
  @endsection
    
    
