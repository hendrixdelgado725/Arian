@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Cargos')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Lista de Cargos
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">
                  <div class="col-sm-4 mt-2">
                  @can('create', App\permission_user::class)
                      {{-- expr --}}
                   <a href="{{route('nuevoCargoE')}}" class="btn btn-info mt-2"><b> REGISTRAR</b></a>
                  @endcan
                  </div>

                <div class="col mt-2">
                  <form action="{{route('buscarCargoE')}}" method="GET">
                    @csrf
                     <div class="input-group mt-2">
                      <input type="text" name="filtro" placeholder="Buscar" class="form-control float-right">
                      <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Buscar"><i class="fas fa-search"></i></button>
                      <a href="{{URL::previous()}}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                       
                     </div>
                  </form>
                  
                </div><!--col-->
                 </div><!--row -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>#</th>
                      <th>Nombre</th>
                      <th>C&oacute;digo</th>
                      <th>Acci&oacute;n</th>
                    </tr>
                  </thead>
                  <tbody style="text-align:center">
                  @foreach($cargos as $cargo)
                  @php
                    $rand  = rand(1, 9999);
                    $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                    $enc=Helper::EncriptarDatos($nrand.'-'.$cargo->id);
                  @endphp
                    <tr>
                      <td>{{$cargos->firstItem() + $loop->index}}</td>
                      <td>{{$cargo->nomcar}}</td>
                      <td>{{$cargo->codcar}}</td>
                      <td>
                        @can('edit', App\permission_user::class)
                        <a href="{{route('editarCargoForm',$enc)}}" data-toggle="tooltip" data-placement="top" title="Editar Cargo">
                          <i class="fa fa-edit blue"></i>
                        </a>
                        /
                        @endcan
                        @can('delete', App\permission_user::class)
                        <a href="{{route('eliminarCargo',$enc)}}" class="borrar" id="deletecargo" 
                        data-id="{{$enc}}" data-toggle="tooltip" data-placement="top" title="Eliminar">
                          <i class="fas fa-trash red"></i>
                        </a>    
                        @endcan
                        
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!!$cargos->appends(request()->input())->render()!!}
                {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section>
  
@endsection
@section('script')
<script src="{{asset('js/eliminarCargo.js')}} "></script>
   <script type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
    });
   </script>
@endsection




