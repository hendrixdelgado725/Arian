@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Presupuestos')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Listado Presupuestos
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">
                  <div class="col-sm-4 mt-2">

                    @can('create', App\permission_user::class)
                      <a href="{{route('nuevoPresupuesto')}}" class="btn btn-info" target="_blank"><b>REGISTRAR</b></a>

                  </div>
                    @endcan

                <div class="col mt-2">
                  <form action="{{route('buscarPresupuesto')}}" method="GET">
                    @csrf
                     <div class="input-group">
                      <input type="text" name="filtro" placeholder="Buscar " class="form-control float-right">
                      <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Buscar"><i class="fas fa-search"></i></button>
                      <a href="{{route('listaPresupuesto')}}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                       
                     </div>
                  </form>
                  
                </div><!--col-->
                 </div><!--row -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>C&oacute;digo</th>
                      <th>Cliente</th>
               <!--        <th>Pago</th> -->
                      <th>Validez</th>
                      <th>Procesado</th>
                      <th>Creado el</th>
                      <th>Vence el</th>
                      <th>Estatus</th>
                      <th>Acci&oacute;n</th>
                    </tr>
                  </thead>
                  <tbody style="text-align:center">

                  @foreach($presupuestos as $presupuesto)
                  @php
                    $sep=explode(' ',$presupuesto->created_at);
                  
                    $rand  = rand(1, 9999);
                    $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                    $enc=Helper::EncriptarDatos($nrand.'-'.$presupuesto->id);
                  @endphp
                    <tr>
                      <td class="codpre">{{$presupuesto->codpre}}</td>
                      <td>{{$presupuesto->cliente->nompro}}</td>
                      <td>{{$presupuesto->valido->duracion.' DÍAS'}}</td>
                      <td>{{$presupuesto->autor}}</td>
                      <td>{{$sep[0]}}</td>
                      <td>{{$presupuesto->vencimiento}}</td>
                      @if($presupuesto->vencimiento <= $date)
                      <td>VENCIDO</td>
                      @else
                      <td>{{$presupuesto->estatus}}</td>
                      @endif
                      <td>
                        @if ($presupuesto->vencimiento > $date)
                          @if($presupuesto->estatus === "PENDIENTE")
                          <a href="{{route('presupuestoAprobar')}}" data-target-enc="{{$enc}}" id="aprobar" class="aprobar" title="Aprobar"><i class="fa fa-check green"></i></a> /
                          @endif
                          @if($presupuesto->estatus != "ANULADO")
                          <a href="{{route('presupuestoAnular')}}" data-target-enc="{{$enc}}" id="anular" class="anular" data-toggle="tooltip" data-placement="top" title="Anular"><i class="fa fa-ban red"></i></a> /
                          @endif
                        @endif
                        @can('edit',App\permission_user::class) 
                         <a target='_blank' href="{{route('pdfPresupuesto', $enc)}}" data-toggle="tooltip" data-placement="top" title="PDF"><i class="fa fa-file blue"></i></a> /
                         <a href="{{route('detallesPresupuesto', $enc)}}" data-toggle="tooltip" data-placement="top" title="Detalles"><i class="fa fa-eye"></i> </a>
                        @endcan
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!!$presupuestos->appends(request()->input())->render()!!}
                {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section>

  
@endsection
@section('script')
<script src="{{asset('js/presupuesto.js')}} "></script>
   <script type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
    });
   </script>
@endsection


