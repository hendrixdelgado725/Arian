@extends('layouts.app')
@extends('layouts.menu')
@if($accion == 'edicion')
@section('titulo','Edición Forma de Pago')
@else
@section('titulo','Registro Forma de Pago')
@endif

  @section('content')

<!-- 
  <div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
       
        </div>
       </div>
    </div>
  </section>
  </div> -->

  <section class="content-wrapper">
    <div class="container-fluid">
    <div class="container">
      <div class="col-10">
        <div class="card card-info">
          @include('vendor/flash.flash_message')
          @if($accion=="registro")
            <div class="card-header">
              <h3 class="card-title"><b>REGISTRO FORMA DE PAGO</b></h3>
            </div>
            
            <form action="{{route('registrarFormaPago')}}" id="saveform" method="post">
                {{csrf_field()}}
                <div class="card-body">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label>Tipo de Pago</label>
                          <div class="input-group mb-3">
                            <input type="text" class="form-control" class="@error('tipo') is-invalid @enderror" placeholder="Ejem: Tansferencia" name="tipo" id="tipo" onkeyup="pasarMayusculas(this.value, this.id)" maxlength="20" minlength="2">
                          </div>
                          @error('tipo')
                          <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                        </div>
                      </div>
                    </div>
                </div>

                <div class="card-footer">
                  <button type="button" id="boton" class="btn btn-success">Guardar Cambios</button>
                  <a href="{{route('listaFormaPago')}}" class="btn btn-default retorno" ><i class="fa fa-chevron-circle-left"></i> Volver</a>
                </div>
            </form>
          @elseif($accion=="edicion")
            <div class="card-header">
              <h3 class="card-title"><b>EDICIÓN FORMA DE PAGO</b></h3>
            </div>
            @php
              $id=$formapago->id.'|'.$formapago->codigoid;
            @endphp
            <form action="{{route('actualizarformapago',$id)}}" method="POST">
              {{csrf_field()}}
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-2">
                    <div class="form group">
                      <label for="id">ID</label>
                      <div class="input-group mb-3">
                        <input type="text" class="form-control" disabled=true value="{{$formapago->id}}" class="@error('id') is-invalid @enderror" name="id" id="id">
                      </div>
                      @error('id')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                    </div>
                  </div>

                  <div class="col-sm-10">
                    <div class="form-group">
                      <label>Tipo de Pago</label>
                      <div class="input-group mb-3">
                            <input type="text" class="form-control" class="@error('tipo') is-invalid @enderror"
                   name="tipo" id="tipo" value="{{$formapago->destippag}}" onkeyup="pasarMayusculas(this.value, this.id)" maxlength="20" minlength="2">
                      </div>
                      @error('tipo')
                      <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                    </div>
                  </div>

                </div>
              </div>

              <div class="card-footer">
                <button type="submit" class="btn btn-success">Guardar Cambios</button>
                <a href="{{route('listaFormaPago')}}" class="btn btn-default retorno" ><i class="fa fa-chevron-circle-left"></i> Volver</a>
              </div>
            </form>
          @endif


        </div>
      </div>
    </div>
    </div>
  </section>

  @endsection

  @section('script')
<script src="{{asset('js/chequeo.js')}} "></script>
<script>
  document.getElementById("boton").addEventListener("click",function(){
      console.log("llego");

      $("#boton").attr("disabled", true);

      setTimeout(() => {
        
        document.getElementById('saveform').submit();
       }, 1000);
  })
</script>
@endsection