@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Registro de duración del Presupuesto')

  @section('content')

<!--   <div class="content-wrapper">
  <section class="content-header">
   <div class="container-fluid">
     <div class="row mb-2">
      <div class="col-sm-6">
       
      </div>
     </div>
   </div>
  </section>
</div> -->
    
    <section class="content-wrapper">
    <div class="container-fluid">
    <div class="container">
        
        <div class="col-10">
            <!-- general form elements -->
            <div class="card card-info">
              @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title mt-2"><b>REGISTRO DE PER&Iacute;ODO DE DURACIÓN DEL PRESUPUESTO</b></h3>
                <div class="d-flex flex-row-reverse bd-highlight">
                  <!-- <div>
                   <a href="{{ url()->previous() }}" class="btn btn-danger"><i class="fas fa-arrow-left"></i><b> Atras</b></a>
                  </div> -->
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form action="{{route('registrarDPresupuesto')}}" id="saveform" method="post">
              {{csrf_field()}}
                <div class="card-body">
                
                <div class="row">
                <div class="col-sm-6">
                <div class="form group">
                <label for="nombre">Ingrese duraci&oacute;n en d&iacute;as</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-hourglass-half"></i></span>
                  </div>
                  <input type="text" class="form-control" onKeypress = "javascript:return NumerosDias(event)" class="@error('duracion') is-invalid @enderror" onkeyup="pasarMayusculas(this.value, this.id)" placeholder="Ejem: 20" name="duracion" id="duracion">
                </div>
                @error('duracion')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div>

                </div>
                <!-- /.card-body -->

               <div class="row">
                <button type="button" id="boton" class="btn btn-success mt-4 ml-2"><b>Guardar Cambios</b></button>
                <div>
                  <a href="{{ URL::previous() }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                </div>
               </div> 

                <!-- <div class="card-footer">
                </div> -->
              </form>
            </div>
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->

            
         </form>
        </div>
            <!-- /.card -->
       </div>
      </div>
      </div>
      </section>
  @endsection
  @section('script')
<script src="{{asset('js/chequeo.js')}} "></script>
<script>
  document.getElementById("boton").addEventListener("click",function(){
      console.log("llego");

      $("#boton").attr("disabled", true);

      setTimeout(() => {
        
        document.getElementById('saveform').submit();
       }, 1000);
  })
</script>
@endsection