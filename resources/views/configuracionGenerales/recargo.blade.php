@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Recargos')

@section('content')

@component('layouts.contenth')
    @slot('titulo')
      Listado de Recargo
    @endslot
  @endcomponent

<section class="content">
	<div class="content-fluid">
		<!-- <div class="container">
			<div class="col-12"> -->
				
				<div class="card">
					@include('vendor/flash.flash_message')
					<div class="card-header">
						<h3 class="card-title"></h3>
						<div class="card-tools">
							<div class="row">
								<div class="col-sm-4 mt-2">
									@can('create', App\permission_user::class)
									    
                  					<a href="{{route('nuevoRecargo')}}" class="btn btn-info"><b>REGISTRAR</b></a>
									@endcan
                				</div>
                			
                				<div class="col mt-2">
                					<form action="{{route('buscarRecargo')}}" method="POST">
	                    				@csrf
    	                				<div class="input-group">
        	              					<input type="text" name="filtro" placeholder="Buscar" class="form-control float-right">
            	          					<button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Buscar"><i class="fas fa-search"></i></button>
                	      					<a href="{{route('listaRecargo')}}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                       
				    	                 </div>
                  					</form>
                				</div>
							</div>
						</div>
					</div>
				

				<div class="card-body table-responsive p-0 text-center">
					<table class="table table-hover">
						<thead>
							<tr style="text-align:center; font-weight: bold;">
                  				<td>#</td>
                  				<td>Descripci&oacute;n</td>
                  				<td>Valor</td>
                  				<td>Activo</td>
                  				<td>Por Defecto</td>
                  				<td>Acci&oacute;n</td>
                			</tr>	
						</thead>
						<tbody>
							@foreach($recargo as $rg)
							  @php
							  	$rand  = rand(1, 9999);
                    			$nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                    			$enc=Helper::EncriptarDatos($nrand.'-'.$rg->id);
                    			$borr=Helper::EncriptarDatos($nrand.'|'.$rg->id.'|'.$rg->codigoid);
							  @endphp
			                  <tr>
			                    <td style="text-align:center">{{$recargo->firstItem() + $loop->index}}</td>
			                    <td style="text-align:center">{{$rg->nomrgo}}</td>
			                    <td style="text-align:center;">{{$rg->monrgo.' %'}}</td>
			                    @if($rg->status=='S')
			                    	<td style="text-align:center;"><i class="fas fa-check"></i></td>
			                    @elseif($rg->status=='N')
			                    	<td style="text-align:center;"><i class="fas fa-ban"></i></td>
			                    @else
		                    	<td style="text-align:center;"><b>-</b></td>
			                    @endif
		                    	@if($rg->defecto=='S')
		                    	<td style="text-align:center;"><i class="fas fa-check"></i></td>
		                    	@elseif($rg->defecto=='N')
		                    	<td style="text-align:center;"><i class="fas fa-ban"></i></td>
		                    	@else
		                    	<td style="text-align:center;"><b>-</b></td>
		                    	@endif
			                    <td>
			                      <div class="button_action" style="text-align:center">
			                      @can('edit', App\permission_user::class)
			                          
			                      <a title="Edición" href="{{ route('editarRecargoForm',$enc)}}"><i class="fa fa-edit blue"></i></a>
			                      /
			                      @endcan
			                      @can('delete', App\permission_user::class)
			                          
			                      <a href="{{ route('eliminarRecargo', $borr)}}" id="deleterecargo" 
			                      data-id="{{$rg->id}}" class="borrarrecargo">
			                        <i class="fas fa-trash red"></i>
			                      </a>
			                      @endcan
			                      </div>
			                    </td>
			                  </tr>
			                @endforeach
						</tbody>
					</table>
					{!!$recargo->render()!!}
				</div>
				</div>
	<!-- 		</div>
		</div> -->
	</div>
</section>
@endsection
@section('script')
<script src="{{asset('js/funciones.js')}} "></script>
   <script type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
    });
   </script>
@endsection