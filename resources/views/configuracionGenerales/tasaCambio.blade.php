@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Tasa de Cambio')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Lista de Tasas de Cambio
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
               
                 <div class="row">
                 <div class="col-sm-4 mt-2" class="d-flex justify-content-center">
                  
                   <a href="{{route('nuevaTasa')}}" class="btn btn-info mt-2"><b> REGISTRAR</b></a>   
                  
                   
                  </div>

                <div class="col mt-2">
                  <form action="{{route('buscarTasa')}}" method="GET">
                    @csrf
                     <div class="input-group mt-2">
                      <input type="text" name="filtro" placeholder="Buscar " class="form-control float-right">
                      <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Buscar"><i class="fas fa-search"></i></button>
                      <a href="{{URL::previous()}}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                       
                     </div>
                  </form>
                  
                </div><!--col-->
                 </div><!--row -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>ID</th>
                      <th>Moneda</th>
                      <th>Equivale a</th>
                      <th>Activo</th>
                      <th>Creado el</th>
                     <!--  <th>Acci&oacute;n</th> -->
                    </tr>
                  </thead>
                  <tbody style="text-align:center"> 
                   @foreach($tasas as $tasa)
                   @php
                    $rand  = rand(1, 9999);
                    $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                    $enc=Helper::EncriptarDatos($nrand.'-'.$tasa->id);
                  @endphp
                    <tr>
                      <td>{{$tasas->firstItem() + $loop->index}}</td>
                      <td>1 {{$tasa->moneda->nombre}}</td>
                      <td style="text-align:center;">{{number_format($tasa->valor,2,',','.').' '.$tasa->moneda2->nomenclatura}}</td>
                      @if($tasa->activo)
                      <td><i class="fas fa-check"></i></td>
                      @else
                      <td><i class="fas fa-ban"></i></td>
                      @endif
                      <td>{{date("d-m-Y H:m:s", strtotime($tasa->updated_at))}}</td>
                      <td>
            <!--             @can('edit', App\permission_user::class)
                            
                        <a href="{{route('editarTasa',$enc)}}" data-toggle="tooltip" data-placement="top" title="Editar">
                          <i class="fa fa-edit blue"></i>
                        </a>
                        @endcan -->
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
               {!!$tasas->appends(request()->input())->render()!!}
               {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section> 
  
  
@endsection
@section('script')
   <script type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
    });
   </script>
@endsection 




