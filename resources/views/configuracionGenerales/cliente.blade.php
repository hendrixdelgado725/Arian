@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Clientes')

@section('content')
  @component('layouts.contenth')
    @slot('titulo')
      Listado de Clientes
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">
                  <div class="col-sm-4 mt-2">
                     @can('create',App\permission_user::class)
                   <a href="{{ route('nuevoCliente')}}" class="btn btn-info mt-2"><b> REGISTRAR</b></a>
                   @endcan
                  </div>

                <div class="col mt-2">

                  <form action="{{route('buscarCliente')}}" method="GET">
                    @csrf
                     <div class="input-group mt-2">
                      <input type="text" name="filtro" placeholder="Buscar       " class="form-control float-right">
                      <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Buscar"><i class="fas fa-search"></i></button>  
                      <a href="{{URL::previous()}}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                    
                     </div>
                  </form>
                  
                </div><!--col-->
                 </div><!--row -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>#</th>
                      <th>Documento</th>
                      <th>Nombre/Raz&oacute;n Social</th>
                      <th>Direcci&oacute;n</th>
                      <th>Tel&eacute;fono</th>
                      <th>Email</th>
                      <th>Acci&oacute;n</th>
                    </tr>
                  </thead>
                  <tbody style="text-align:center">
                  @foreach($clientes as $cliente)
                  @php
                    $rand  = rand(1, 9999);
                    $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                    $enc=Helper::EncriptarDatos($nrand.'-'.$cliente->id);
                  @endphp
                    <tr>
                      <td>{{$clientes->firstItem() + $loop->index }}</td>
                      <td>{{$cliente->rifpro}}</td>
                      <td>{{$cliente->nompro}}</td>
                      <td>{{$cliente->dirpro}}</td>
                      <td>{{$cliente->telpro}}</td>
                      @if($cliente->email)
                      <td>{{$cliente->email}}</td>
                      @else
                      <td>N/A</td>
                      @endif
                      <td>
                         @can('edit',App\permission_user::class)
                        <a href="{{ route('editarClienteForm',$enc)}}" data-toggle="tooltip" data-placement="top" title="Editar Cliente">
                          <i class="fa fa-edit blue"></i>
                        </a>
                        /
                        @endcan
                         @can('delete',App\permission_user::class)
                        <a href="{{ route('eliminarCliente', $enc)}}" id="deletecliente" 
                        data-id="{{$enc}}" class="borrar" data-toggle="tooltip" data-placement="top" title="Eliminar Cliente">
                          <i class="fas fa-trash red"></i>
                        </a>
                        @endcan
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!!$clientes->appends(request()->input())->render()!!}
                {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section>

  
@endsection
@section('script')
<script src="{{asset('js/eliminarCliente.js')}} "></script>
   <script type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
    });
   </script>
@endsection



