@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Precios')

@section('content')
  @component('layouts.contenth')
    @slot('titulo')
      Promociones
    @endslot
  @endcomponent
  <detallespromo :datos="{{ json_encode($datos) }}"></detallespromo>
@endsection