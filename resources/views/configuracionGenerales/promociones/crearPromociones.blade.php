@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Precios')

@section('content')
  @component('layouts.contenth')
    @slot('titulo')
      Crear Promociones
    @endslot
  @endcomponent
  <section class="mx-3">
    <promociones></promociones>
  </section>
  @endsection