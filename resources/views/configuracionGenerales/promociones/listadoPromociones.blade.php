@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Precios')

@section('content')
  @component('layouts.contenth')
    @slot('titulo')
      Promociones
    @endslot
  @endcomponent
    <section class="">
        <div>
            <div class="card mx-4">
                <div class="">
                    <form class=" col-md-12 my-3  d-flex justify-content-end"
                    
                    action="{{ route('filtro.promociones')}}"
                    method="Get"
                    >
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
                        <div class="input-group mt-2 col-md-4">
                            <a class="btn btn-info mr-3" href="{{ route('promociones.crear')}}">
                                REGISTRAR
                            </a> 
                            <input
                                type="text"
                                name="filtro"
                                placeholder="Buscar Promocion"
                                class="form-control float-right"
                                >
                            <button
                                type="submit"
                                class="btn btn-default"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Buscar"
                            ><i class="fas fa-search"></i></button>
                            <a
                                href="{{route('promociones')}}"
                                class="btn btn-default"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Regresar"
                            ><i class="fas fa-arrow-left"></i></a>
                        </div>   
                    </form>
                </div>
                <div class="card-header">
                    <table class="table table-hover">
                        <thead>
                            @if(session('mensaje'))
                                <div id="alerta" class="alert alert-success">
                                    {{ session('mensaje') }}
                                </div>
                            @endif
                            <tr>
                                <th scope="col">Codigo</th>
                                <th scope="col">Descripcion</th>
                                <th scope="col">Tiempo</th>
                                <th scope="col">Creado</th>
                                <th scope="col">Estatus</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($articulopro as $art)
                            <tr 
                            >
                                <th scope="row">{{$art->codprom}}</th>
                                <td>{{$art->desprom}}</td>
                                <td>{{ $art->fedesprom ? $art->fedesprom." - ".$art->fechasprom : "Hasta agotar existencia"}}</td>
                                <td>{{$art->created_at}}</td>
                                <td>{{$art->status === 'A' ? 'ACTIVO' : ($art->status === 'P' ? 'PENDIENTE' : 'INACTIVO')}}</td>
                                <td>
                                    @if($art->status == 'P')
                                    @can('edit',App\permission_user::class)
                                    <a href="{{ route('editpromociones',$art->id)}}">
                                        <i class="fa fa-edit blue" ></i>
                                    </a> 
                                    @endcan
                                    @endif
                                    @if($art->status == 'P')
                                    @can('delete',App\permission_user::class)
                                        <a class="borrar" data-id="{{$art->id}}" id="eliminar" title="Eliminar" >
                                        <i class="fa fa-trash red"></i>
                                        </a>
                                    @endcan
                                    @endif
                                
                                    <a  class="detalles" title="detalles" href="{{ route('detalles.promociones',$art->id) }}">
                                        <i class="fa fa-list blue"></i>
                                    </a>
                                    @if($art->staprom == 'A')
                                       @if($art->status == 'I')
                                            @can('edit',App\permission_user::class)
                                            <a class="activar" id="delete"  data-codartprom="{{ $art->codartprom }}" data-desprom='{{$art->desprom}}'>
                                                <i class="fa fa-check blue"></i>
                                            </a>
                                            @endcan
                                        @endif
                                    @endif
                                    @if($art->staprom == 'A')
                                         @if($art->status == 'A')
                                            @can('edit',App\permission_user::class)
                                            <a class="desactivar" id="delete"  data-codartprom="{{ $art->codartprom }}" data-desprom='{{$art->desprom}}'> 
                                                <i class="fa fa-ban red"></i>
                                            </a>
                                            @endcan
                                         @endif
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>    
                    {{ $articulopro->links() }}
                </div>
            </div> 
        </div>
    </section>
   
  @endsection
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script>
      $(document).ready(function() {
        
        $(".borrar").click( function() {
            let id = $(this).data('id');
            let _token = document.getElementById('token');
            Swal.fire({
                title: 'Estas seguro de que quieres eliminar este registro?',
                text: "se eliminara la promoción!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, eliminar!'
            }).then((result) => { 
                if(result.value === true){
                    const response = fetch(`/modulo/Almacen/EliminarPromocion/${id}`,  {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': _token.value,
                    },
                    body: JSON.stringify(id),
                    });

                
                    Swal.fire(
                        'Eliminado!',
                        'La promoción se ha eliminado correctamente.',
                        'success'
                    )
                    location.reload();
                }
            })
        })

          $(".desactivar").click( function() {
          
                let codartprom = $(this).data('codartprom')
                let desprom = $(this).data('desprom')
                let _token = document.getElementById('token');
               Swal.fire({
            title: `¿Seguro que desea desactivar  ${codartprom}/${desprom}?`,
            text: `¡Ahora el producto no saldra para facturacion/pedidos!`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: `Si!`
          }).then(function(result){
              if(result.value === true){
                const data = {
                    codigo: codartprom,
                    status: 'I',
                    };
                    
                    const response = fetch(`/modulo/Almacen/Promociones/Activar`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                       'X-CSRF-TOKEN': _token.value,
                    },
                    body: JSON.stringify(data),
                    });

                    Swal.fire({
                    title: '¡Desactivado con éxito!',
                    icon: 'success',
                    });

                   
                    location.reload();
              }
           });
        });

        $(".activar").click( function() {
           
                let codartprom = $(this).data('codartprom')
                let desprom = $(this).data('desprom')
                let _token = document.getElementById('token');
               Swal.fire({
            title: `¿Seguro que desea Activar  ${codartprom}/${desprom}?`,
            text: `¡Ahora el producto saldra para facturacion/pedidos!`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: `Si!`
          }).then(function(result){
              if(result.value===true){
                    const data = {
                        codigo: codartprom,
                        status: 'A',
                        };
                        
                    const response = fetch(`/modulo/Almacen/Promociones/Activar`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': _token.value,
                        },
                        body: JSON.stringify(data),
                        });
                        
                        Swal.fire({
                        title: '¡Activado con éxito!',
                        icon: 'success',
                        });

                        location.reload();
                }
            });
        });
    })

  </script>