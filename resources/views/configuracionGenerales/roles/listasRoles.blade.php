@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Reiniciar Contraseña')

@section('content')

@component('layouts.contenth')
    @slot('titulo')
      Listados de Roles
    @endslot
@endcomponent
    <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
              @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">

              <div class="col">
                  <div class="row">
                    <div class="col-xs-12">
                      
                        <form method="post" action="{{ route('filtro') }}">
                            <div class="input-group">
                             @csrf
                             @can('create',App\permission_user::class)
                          <div class="card-tools mr-2">
                            @can('create',App\permission_user::class) 
                            <a href="{{ route('rolescreate') }}" class="btn btn-info"><b>REGISTRAR CARGOS</b></a>
                            @endcan
                          </div>
                          @endcan
                          <input type="text" placeholder="Buscar por nombre de Usuario" name="filtro" class="form-control float-right">
                          
                          <button type="submit" style="height: 38px; width: 38px" class="btn btn-default btn-sm float-left"><i class="fas fa-search"></i></button>
                           
                           </div>
                        </form>
                     
                    </div>
                  </div>
                 </div>
                
                 </div><!--row -->
                </div>
              </div>

              <!-- /.card-header -->
              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>ID</th>
                      <th>Codigo del rol</th>
                      <th>nombre</th>
                      
                      <th>Fecha</th>
                     
                      <th>Accion</th>

                    </tr>
                  </thead>
                  <tbody style="text-align:center">

                    @foreach ($roles as $element)
                    @php
                      $rand  = rand(1, 9999);
                      $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                      $enc=Helper::EncriptarDatos($nrand.'-'.$element->codigoid);
                      //$borr=Helper::EncriptarDatos($nrand.'|'.$tal->id.'|'.$tal->codigoid);
                    @endphp
                     <tr style="text-align: center;">
                      <td>{{$element->id}}</td>
                      <td>{{$element->codroles}}</td>
                      <td>{{$element->nombre}}</td>
                      <td>{{$element->created_at}}</td>
                      <td>
                       @can('delete',App\permission_user::class) 
                        <a class="borrar" href="{{ route('rolesBorrar',$enc) }}">
                          <i class="fa fa-trash blue"></i>
                        </a>
                       @endcan
                      </td>
                        
                    @endforeach
                        
                  </tbody>
                </table>
                   {{$roles->appends(Request::only('filtro'))->render()}}

          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
{{--   --}}
                  
  </div>
@endsection


@section('script')
<script>
  $(document).ready(function(e) {
    

    $('a.borrar').on('click',function (e) {
        e.preventDefault();
             Swal.fire({
            title: '¿Seguro que desea eliminar este Cargo?',
            text: "¡Despues de esto no se encontrara en la lista!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar'
          }).then((result) => {
              if (result.value == true) {

                
              var row = $(this).parents('tr');
              //var form = $(this).parents('data-id');
              var url = $(this).attr('href');
              //console.log(url);
              $.get(url,row,function(result1) {
                  
                if (result1.titulo === 'SE HA ELIMINADO CON EXITO') {

                  
                      Swal.fire(
                        'SE HA ELIMINADO CON EXITO',
                        '.',
                        'success',

                      );

                      row.fadeOut();
                }

                                           
                 

              })   
              }  //para cancelar el sweetalert2
          });
    })

  });
</script>
@endsection

