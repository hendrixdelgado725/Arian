@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Registrar Articulos')
  @section('content')
    @component('layouts.contenth')
    @slot('titulo')
      Crear Cargos
    @endslot
  @endcomponent
    
    <section class="content-wrapper" >
    <div class="container-fluid">
    <div class="container">
        
        <div class="col-10">
            <!-- general form elements -->
            <div class="card card-info" style="padding-right: -1000px">
              <div class="card-header">
                <h3 class="card-title"><b>REGISTRAR CARGOS</b></h3>
                
              </div>
              <!-- /.card-header -->
              <!-- form start -->
             
              <form action="{{route('rolescreates')}}" method="post"> 
              {{csrf_field()}}
                <div class="card-body" >
                 <div class="row">
                 <div class="col-sm-6">
                 <div class="form-group">
                  <label for="tipodocumento">C&oacute;digo</label>
                     <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">A-1</span>
                  </div>
                  <input type="text" class="codigo form-control" placeholder="Ejem: R-002" name="codigos" 
                  id="cedularifcliente" class="@error('codigos') is-invalid @enderror" maxlength="13"
                  
                  >
                </div>
                @error('codigos')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>     
                 </div>
                 
               <div class="col-sm-6">
                <div class="form-group">
                <label>Nombre del Cargo</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">A</span>
                  </div>
                  <input type="text" class="form-control desc" placeholder="Ejem: 1234567" name="nomCargo" 
                  id="cedularifcliente" class="@error('Descripcion') is-invalid @enderror">
                </div>
                @error('Descripcion')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
               </div>
               </div>
               <div class="form group detalle">
                
               <div class="card-footer">
                <button type="submit" class="btn btn-success"><b>Guardar Cambios</b></button>

                 <a href="{{ url('/modulo/ConfiguracionGenerales/Droles') }}" class="btn btn-default"><i class="fa fa-chevron-circle-left"></i> Volver</a>
                </div>
               </div>
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->

         </form>
      
        </div>
            <!-- /.card -->
       </div>
      </div>
      </div>
  </section>
  @endsection

  @section('mayuscula')
  
  @endsection
    