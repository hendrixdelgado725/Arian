@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Registro de Subcategoria de Producto')

  @section('content')

<!--   <div class="content-wrapper">
  <section class="content-header">
   <div class="container-fluid">
     <div class="row mb-2">
      <div class="col-sm-6">
       
      </div>
     </div>
   </div>
  </section>
</div> -->
    
    <section class="content-wrapper">
    <div class="container-fluid">
    <div class="container">
        
        <div class="col-10">
            <!-- general form elements -->
            <div class="card card-info">
              @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title mt-2"><b>REGISTRO SUB CATEGOR&Iacute;A DE PRODUCTO</b></h3>
                <div class="d-flex flex-row-reverse bd-highlight">
                  <!-- <div>
                   <a href="{{ url()->previous() }}" class="btn btn-danger"><i class="fas fa-arrow-left"></i><b> Atras</b></a>
                  </div> -->
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form action="{{route('registrarSCdeProducto')}}" method="post">
              {{csrf_field()}}
                <div class="card-body">

                <div class="row">
                 <div class="col">
                  <div class="form-group">
                    <label for="categoriaP">Categor&iacute;a del Producto</label>
                    <select class="form-control select2" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="categoriaP" class="@error('categoriaP') is-invalid @enderror">
                    <option selected="selected" disabled>Seleccione una Categoría</option>
                        @foreach ($categorias as $categoria)
                        <option>{{$categoria->nombre}}</option>
                        @endforeach
                    </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                    @error('categoriaP')
                     <div class="alert alert-danger">{!!$message!!}</div>
                   @enderror
                  </div>
                 </div>
                </div>

                <div class="row">
                <div class="col-sm-6">
                <div class="form group">
                <label for="nombreSC">Nombre de la Sub Categoría</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Aa</span>
                  </div>
                  <input type="text"  onkeyup="pasarMayusculas(this.value, this.id)"  class="form-control" class="@error('nombreSC') is-invalid @enderror" placeholder="Ejem: Calzado..." name="nombreSC" id="nombreSC">
                </div>
                @error('nombreSC')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div>

                <div class="col-sm-6">
                <div class="form group">
                <label for="nomenSC">Nomenclatura</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Aa</span>
                  </div>
                  <input type="text"  onkeyup="pasarMayusculas(this.value, this.id)"  class="form-control" class="@error('nomenSC') is-invalid @enderror" placeholder="Ejem: CZ" name="nomenSC" id="nomenSC">
                </div>
                @error('nomenSC')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div>

                </div>
                <!-- /.card-body -->
               <div class="row">
                <button type="submit" class="btn btn-success ml-3 mt-4"><b> Guardar Cambios</b></button>
                <div>
                  <a href="{{ route('listaSubCategoriaP') }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                </div>
               </div>

                <!-- <div class="card-footer">
                </div> -->
              </form>
            </div>
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->

            
         </form>
        </div>
            <!-- /.card -->
       </div>
      </div>
      </div>
      </section>
  @endsection
  @section('script')
  <script src="{{asset('js/chequeo.js')}} "></script>
  <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
    });
   </script>
  @endsection
    