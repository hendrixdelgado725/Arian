@extends('layouts.app3')
@extends('layouts.menu')
@section('titulo','Registro Presupuesto')

  @section('content')

<section class="container-fluid">
    <form-presupuesto :monedas="{{ json_encode($monedas) }}" :recargos="{{ json_encode($recargos) }}" :duraciones="{{ json_encode($duraciones) }}" :articulos="{{ json_encode($articulos) }}" :descuentos="{{ json_encode($descuentos) }}">
</section>
  @endsection
  @section('script')
  
  @endsection