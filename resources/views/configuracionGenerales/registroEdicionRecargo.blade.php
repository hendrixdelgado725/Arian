@extends('layouts.app')
@extends('layouts.menu')
@if($accion == 'edicion')
@section('titulo','A1WIN - Edición Recargo')
@else
@section('titulo','A1WIN - Registro Recargo')
@endif

  @section('content')
<!-- 
  <div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
       
        </div>
       </div>
    </div>
  </section>
  </div> -->

  <section class="content-wrapper">
  	<div class="container-fluid">
  		<div class="container">
  			<div class="col-10">
  				<div class="card card-info">
            @include('vendor/flash.flash_message')
  					@if($accion=="registro")
  						<div class="card-header">
              				<h3 class="card-title"><b>REGISTRO RECARGO</b></h3>
            			</div>

            			<form action="{{route('registrarRecargo')}}" method="post" id="RecargoForm">
            				{{csrf_field()}}
            				<div class="card-body">
            					<div class="row">
            						<div class="col-sm-12">
            							<div class="form-group">
            								<label>Descripci&oacute;n</label>
            								<div class="input-group mb-3">
            									<input type="text" class="form-control" class="@error('descripcion') is-invalid @enderror" placeholder="Ejem: Iva 0.00%" name="descripcion" id="descripcion" onkeyup="pasarMayusculas(this.value, this.id)" maxlength="100" minlength="2">
            								</div>
            								@error('descripcion')
					                          <div class="alert alert-danger">{!!$message!!}</div>
				                         	@enderror
            							</div>
            						</div>

            						<div class="col-sm-6">
                    				<div class="form group">
				                      <label for="id">Valor (%)</label>
				                      <div class="input-group mb-3">
				                        <input type="text" class="form-control" maxlength="3" class="@error('valor') is-invalid @enderror" placeholder="Solo Número" name="valor" id="valor" onKeypress = "javascript:return NumerosReales(event)" autocomplete="off" maxlength="17">
				                      </div>
				                      @error('valor')
				                        <div class="alert alert-danger">{!!$message!!}</div>
				                      @enderror
				                    </div>
                  				</div>

			                  	<div class="col-sm-6">
				                    <div class="form-group">
				                      <label>Valor Activo</label>
				                      <div class="input-group mb-3">
				                            <select style="width:100%" class="form-control select2 required" id="defecto" name="defecto" tabindex="-1"  required="required" value="">
            										        <option value='N'>NO</option>
                                				<option value='S'>SI</option>
            									       </select>
				                      </div>
				                      @error('defecto')
				                      <div class="alert alert-danger">{!!$message!!}</div>
				                      @enderror
				                    </div>
			                  	</div>

                          <div class="col-sm-6">
                            <div class="form-group">
                              <label>Por Defecto</label>
                              <div class="input-group mb-3">
                                    <select style="width:100%" class="form-control select3 required" id="xdefect" name="xdefect" tabindex="-1"  required="required" value="">
                                        <option value='N'>NO</option>
                                        <option value='S'>SI</option>
                                     </select>
                              </div>
                              @error('xdefect')
                              <div class="alert alert-danger">{!!$message!!}</div>
                              @enderror
                            </div>
                          </div>
            					
            					</div>
            					
            				</div>

            				<div class="card-footer">
			                <button type="submit" class="btn btn-success" id="submitButton" onclick="this.disabled=true;this.form.submit();" >Guardar Cambios</button>
			                <a href="{{route('listaRecargo')}}" class="btn btn-default retorno" ><i class="fa fa-chevron-circle-left"></i> Volver</a>
			              </div>
            			</form>
            @elseif ($accion=="edicion")
              <div class="card-header">
                <h3 class="card-title"><b>EDICIÓN RECARGO</b></h3>
              </div>
              @php
              $id= $recargo->id.'|'.$recargo->codigoid;
              @endphp
              <form action="{{route('actualizarrecargo',$id)}}" method="POST">
                {{csrf_field()}}
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-2">
                      <div class="form group">
                        <label for="id">ID</label>
                        <div class="input-group mb-3">
                          <input type="text" class="form-control" disabled=true value="{{$recargo->id}}" class="@error('id') is-invalid @enderror" name="id" id="id">
                        </div>
                        @error('id')
                          <div class="alert alert-danger">{!!$message!!}</div>
                        @enderror
                      </div>
                    </div>

                    <div class="col-sm-10">
                      <div class="form-group">
                        <label>Descripción</label>
                        <div class="input-group mb-3">
                          <input type="text" class="form-control" class="@error('descripcion') is-invalid @enderror"
                   name="descripcion" id="descripcion" value="{{$recargo->nomrgo}}" onkeyup="pasarMayusculas(this.value, this.id)" maxlength="100" minlength="2">
                        </div>
                        @error('descripcion')
                      <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                      </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form group">
                          <label for="id">Valor (%)</label>
                          <div class="input-group mb-3">
                            <input type="text" class="form-control" maxlength="3" class="@error('valor') is-invalid @enderror" name="valor" id="valor" value="{{$recargo->monrgo}}" onKeypress = "javascript:return NumerosReales(event)" autocomplete="off" maxlength="17">
                          </div>
                          @error('valor')
                            <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                        </div>
                    </div>

                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Valor Activo</label>
                        <div class="input-group mb-3">
                          <select style="width:100%" class="form-control select2 required" id="defecto" name="defecto" tabindex="-1"  required="required" value="{{$recargo->status}}">
                          @if ($recargo->status!=NULL)
                            <option value="S" {{ $recargo->status == "S" ? 'selected' : '' }}>SI</option>
                            <option value="N" {{ $recargo->status == "N" ? 'selected' : '' }}>NO</option>
                          @else
                            <option value="">Seleccione...</option>
                            <option value='N'>NO</option>
                            <option value='S'>SI</option>
                          @endif
                          </select>
                        </div>
                        @error('defecto')
                        <div class="alert alert-danger">{!!$message!!}</div>
                        @enderror
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Por Defecto</label>
                        <div class="input-group mb-3">
                          <select style="width:100%" class="form-control select3 required" id="xdefect" name="xdefect" tabindex="-1"  required="required" value="{{$recargo->defecto}}">
                            @if($recargo->defecto!=NULL)
                            <option value="S" {{ $recargo->defecto == "S" ? 'selected' : '' }}>SI</option>
                            <option value="N" {{ $recargo->defecto == "N" ? 'selected' : '' }}>NO</option>
                            @else
                              <option value="">Seleccione...</option>
                              <option value='N'>NO</option>
                              <option value='S'>SI</option>
                            @endif
                          </select>
                        </div>
                        @error('xdefect')
                        <div class="alert alert-danger">{!!$message!!}</div>
                        @enderror
                      </div>
                    </div>

                  </div>
                </div>

                <div class="card-footer">
                <button type="submit" class="btn btn-success" onclick="this.disabled=true;this.form.submit();">Guardar Cambios</button>
                <a href="{{route('listaRecargo')}}" class="btn btn-default retorno" ><i class="fa fa-chevron-circle-left"></i> Volver</a>
              </div>
              </form>

  					@endif
  				</div>
  			</div>
  		</div>
  	</div>
  </section>
  @endsection

@section('script')
	<script src="{{asset('js/chequeo.js')}} "></script>
  <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
        $('select.select3').select2({
        });
    });
   </script>
@endsection