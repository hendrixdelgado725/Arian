@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Subcategoria de Productos')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Sub categorías de Productos
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">
                  <div class="col-sm-4 mt-2">
                     @can('create', App\permission_user::class)
                   <a href="{{route('nuevoSubCat')}}" class="btn btn-info mt-2"><b> REGISTRAR</b></a>
                      @endcan
                  </div>

                <div class="col mt-2">
                  <form action="{{route('buscarSCProducto')}}" method="GET">
                    @csrf
                     <div class="input-group mt-2">
                      <input type="text" name="filtro" placeholder="Buscar " class="form-control float-right">
                      <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Buscar"><i class="fas fa-search"></i></button>
                      <a href="{{route('listaSubCategoriaP')}}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                       
                     </div>
                  </form>
                  
                </div><!--col-->
                 </div><!--row -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>#</th>
                      <th>Nombre</th>
                      <th>Nomenclatura</th>
                      <th>Categoría</th>
                      <th>Creado el</th>
                      <th>Actualizado el</th>
                      <th>Acción</th>
                    </tr>
                  </thead>
                  <tbody style="text-align:center"> 
                    @foreach($subcategorias as $subcategoria)
                     @php
                     $rand  = rand(1, 9999);
                     $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                     $enc=Helper::EncriptarDatos($nrand.'-'.$subcategoria->id);
                    @endphp
                    <tr>
                      <td>{{$subcategorias->firstItem() + $loop->index}}</td>
                      <td>{{$subcategoria->nomsubcat}}</td>
                      <td>{{$subcategoria->nomensubcat}}</td>
                      <td>{{$subcategoria->categoria->nombre ?? '-' }}</td>
                      <td>{{$subcategoria->created_at ? date("d-m-Y H:m:s", strtotime($subcategoria->created_at)):''}}</td>
                      <td>{{$subcategoria->updated_at?date("d-m-Y H:m:s", strtotime($subcategoria->updated_at)):''}}</td>
                      <td>
                         @can('edit', App\permission_user::class)
                        <a href="{{route('editarSCProducto',$enc)}}">
                          <i class="fa fa-edit blue" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                        </a>
                        /
                        @endcan
                         @can('delete', App\permission_user::class)
                        <a href="{{route('eliminarSCProducto',$enc)}}" class="borrar" id="delete" 
                        data-id="{{$enc}}" data-toggle="tooltip" data-placement="top" title="Eliminar">
                          <i class="fas fa-trash red"></i>
                        </a>
                        @endcan
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>

               <!--  -->
               {!!$subcategorias->appends(request()->input())->render()!!}
               {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section> 
  
@endsection
@section('script')
<script src="{{asset('js/eliminarSCProducto.js')}} "></script>
   <script type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
    });
   </script>
@endsection 



