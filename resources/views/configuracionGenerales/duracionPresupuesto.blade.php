@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Duración de Presupuesto')
@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Duraci&oacute;n del Presupuesto
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">
                  <div class="col-sm-4 mt-2">
                     @can('create',App\permission_user::class)
                   <a href="{{route('nuevoDPresupuesto')}}" class="btn btn-info"><b> REGISTRAR</b></a>
                   @endcan
                  </div>

                <div class="col mt-2">
                  <form action="{{route('buscarDPresupuesto')}}" method="GET">
                    @csrf
                     <div class="input-group">
                      <input type="text" name="filtro" placeholder="Buscar" class="form-control float-right">
                      <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Buscar"><i class="fas fa-search"></i></button>
                      <a href="{{URL::previous()}}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                       
                     </div>
                  </form>
                  
                </div><!--col-->
                 </div><!--row -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover center">
                  <thead>
                    <tr style="text-align:center">
                      <th>#</th>
                      <th>Per&iacute;odo de Duraci&oacute;n</th>
                      <th>Creado el</th>
                      <th>Acci&oacute;n</th>
                    </tr>
                  </thead>
                  <tbody style="text-align:center">
                  @foreach($duraciones as $duracion)
                    <tr>
                      <td>{{$duraciones->firstItem() + $loop->index}}</td>
                      <td>{{$duracion->duracion}} Días</td>
                      <td>{{date("d-m-Y H:m:s", strtotime($duracion->created_at))}}</td>
                      <td>
                         @can('delete',App\permission_user::class)
                        <a href="{{route('eliminarDPresupuesto',$duracion->codigoid)}}"  
                        data-id="$duracion->codigoid" class="borrar" data-toggle="tooltip" data-placement="top" title="Eliminar">
                          <i class="fas fa-trash red"></i>
                        </a>
                        @endcan
                      </td>
                    </tr>
                    @endforeach
                  </tbody>      
                </table>
                {!!$duraciones->appends(request()->input())->render()!!}
                {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section>

  
@endsection
@section('script')
<script src="{{ asset('js/eliminarDPresupuesto.js') }}" ></script>
   <script type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
    });
   </script>
@endsection 



