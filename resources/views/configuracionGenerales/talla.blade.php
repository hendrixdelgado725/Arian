@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Tallas')

@section('content')

 @component('layouts.contenth')
    @slot('titulo')
      Listado de Tallas
    @endslot
  @endcomponent

<section class="content">
  <div class="content-fluid">
  	<div class="card">
  		@include('vendor/flash.flash_message')
  		<div class="card-header">
        <h3 class="card-title"></h3>

        <div class="card-tools">
        	<div class="row">
        		<div class="col-sm-4 mt-2">
                @can('create',App\permission_user::class)
                  <a href="{{route('nuevaTalla')}}" class="btn btn-info"><b>REGISTRAR</b></a>
                  @endcan
            </div>

            <div class="col mt-2">
            	<form action="{{route('buscarTalla')}}" method="POST">
            		@csrf
            		<div class="input-group">
            			<input type="text" name="filtro" placeholder="Buscar " class="form-control float-right">
            		
               
            		<button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
            		<a href="{{route('listaTalla')}}" class="btn btn-default"><i class="fas fa-arrow-left"></i></a>
            	  </div> 
              </form>
            </div>
        	</div>
        </div>
      </div>

      <div class="card-body table-responsive p-0 text-center">
      	<table class="table table-hover">
      		<thead>
	      		<tr style="text-align:center; font-weight: bold; ">
	      			<td>#</td>
	            <td>Talla</td>
              <td>C&oacute;digo</td>
              <td>Categor&iacute;a</td>
	            <td>Acci&oacute;n</td>
	      		</tr>
      		</thead>

      		<tbody>
      				@foreach($talla as $tal)
      					@php
                //dd($tal->Categoria->nombre);
      						$rand  = rand(1, 9999);
      						$nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
      						//$enc=Helper::EncriptarDatos($nrand.'-'.$tal->id);
      						$borr=Helper::EncriptarDatos($nrand.'|'.$tal->id.'|'.$tal->codigoid);
      					@endphp
				<tr style="text-align:center">
					<td >{{$talla->firstItem() + $loop->index}}</td>
					<td>{{$tal->tallas}}</td>
					<td>{{$tal->codtallas}}</td>
					<td>{{$tal->Categoria['nombre']}}</td>
      						<td>
                    <div class="button_action" style="text-align:center">
                    <!-- <a title="Edición" href=""><i class="fa fa-edit blue"></i></a>
                    / -->
					@can('edit',App\permission_user::class)
                    <a href="{{route('editTalla', $borr)}}" id="edittalla" 
                    data-id="{{$tal->id}}">
                      <i class="fas fa-edit blue"></i> 
                    </a> 
                    @endcan

					/

                     @can('delete',App\permission_user::class)
                    <a href="{{route('eliminarTalla', $borr)}}" id="deletetalla" 
                    data-id="{{$tal->id}}" class="borrartalla">
                      <i class="fas fa-trash red"></i> 
                    </a> 
                    @endcan
                    </div>
                  </td>
      					</tr>
      				@endforeach
      		</tbody>
      	</table>
      	{!!$talla->render()!!}
      </div>

  	</div>
  </div>
</section>
@endsection
@section('script')
<script src=	"{{asset('js/funciones.js')}} "></script>
@endsection