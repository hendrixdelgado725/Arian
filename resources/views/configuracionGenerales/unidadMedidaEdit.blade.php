@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Unidad Medida')


@section('content')

<section class="content pt-5">

   <from-unidadmedida type="{{ $type }}"
   :unidadmedida="{{ json_encode($unidadmedida) ?? json_encode('{}') }}">

   </from-unidadmedida>

</section>


@endsection

