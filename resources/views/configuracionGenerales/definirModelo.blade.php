  @extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Lista de Marcas')

@section('content')
@component('layouts.contenth')
    @slot('titulo')
      Lista de Marcas 
    @endslot
  @endcomponent
    <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
              <div class="card-header">
                @include('vendor/flash.flash_message')
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">



                   <div class="col-sm-4 mt-2">
                  <a href="{{ route('definirMarcas.crear') }}" class="btn btn-info">REGISTRAR</a>
                  </div>


               <br>
               <div class="col">
                  <div class="row">
                    <div class="col mt-2">

                        <form action="{{ route('definirMarcas.filtro') }}" method="get">
                         <div class="input-group">
                          @csrf
                          <input type="text" placeholder="Buscar" name="filtro"  class="form-control float-right">
                          <button type="submit" style="height: 38px; width: 38px" class="btn btn-default btn-sm float-left"><i class="fas fa-search"></i></button>
                          <a href="{{route('definirMarcas')}}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                       
                   
                        </div>
                        </form>
                    </div>
                  </div>
                 </div>
                
                 </div><!--row -->
                </div>
              </div>
              <!-- /.card-header -->

              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>#</th>
                      <th>Descripci&oacute;n</th>
                      <th>Acci&oacute;n</th>
                    </tr>
                  </thead>
                  <tbody style="text-align:center">
                  @foreach ($modelo as $key)
                  @php
                    $rand  = rand(1, 9999);
                    $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                    $enc=Helper::EncriptarDatos($nrand.'-'.$key->codigoid);

                  @endphp
                      <tr style="text-align: center;">
                      <td>{{ $modelo->firstItem() + $loop->index }}</td>
                      <td>{{ $key->defmodelo }}</td>
                      <td>

                      <a class="edit"  href="{{ route('definirMarcas.edit',$enc) }}">
                      <i class="fa fa-edit blue"></i>
                      </a>

                      /
                       
                      <a class="borrar"  href="{{ route('definirMarcas.delete',$enc) }}">
                      <i class="fa fa-trash red"></i>
                      </a>
                        
                      </td>
                    </tr>
                  

             
                  @endforeach
                  </tbody>
                </table>
              {!! $modelo->appends(Request::only('filtro'))->render() !!} 
                {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
   
 </section>
    
@endsection

@section('eliminarArticulos')

<script>
  
  $('.borrar').click(function(e){
          e.preventDefault();
            Swal.fire({
            title: '¿Seguro que desea eliminar este Modelo?',
            text: "¡Despues de esto no se encontrara en la lista!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar'
          }).then((result) => {
              if (result.value == true) {

                
              var row = $(this).parents('tr');
              //var form = $(this).parents('data-id');
              var url = $(this).attr('href');
              console.log(url);
              $.get(url,row,function(result1) {
                  
                if (result1.titulo === 'SE HA ELIMINADO CON EXITO') {

                  
                      Swal.fire(
                        'SE HA ELIMINADO CON EXITO',
                        '.',
                        'success',

                      );

                      row.fadeOut();
                }

                                           
                 

              })   
              }  //para cancelar el sweetalert2
          });

  });
</script>
@endsection




  