@extends('layouts.app')
@extends('layouts.menu')
@if($accion == 'edicion')
@section('titulo','Edición Descuento')
@else
@section('titulo','Registro Descuento')
@endif
  @section('content')
 <!--  	<div class="content-wrapper">
	  <section class="content-header">
	    <div class="container-fluid">
	      <div class="row mb-2">
	        <div class="col-sm-6">
	       
	        </div>
	       </div>
	    </div>
	  </section>
  	</div> -->

  	<section class="content-wrapper">
  		<div class="container-fluid">
  		<div class="container">
  			<div class="col-10">
  				<div class="card card-info">
  					@include('vendor/flash.flash_message')
  					@if($accion=="registro")
  						<div class="card-header">
              				<h3 class="card-title"><b>REGISTRO DESCUENTO</b></h3>
            			</div>
            			<form action="{{route('registrarDescuento')}}" method="post">
            				{{csrf_field()}}
            				<div class="card-body">
            					<div class="row">
            						<div class="col-sm-12">
            							<div class="form-group">
            								<label>Descripción</label>
            								<div class="input-group mb-3">
            									<input type="text" class="form-control" class="@error('descripcion') is-invalid @enderror" placeholder="Ejem: Motivo " name="descripcion" id="descripcion" onkeyup="pasarMayusculas(this.value, this.id)" maxlength="100" minlength="2">
            								</div>
            								@error('descripcion')
					                          <div class="alert alert-danger">{!!$message!!}</div>
				                         	@enderror
            							</div>
            						</div>

            						<div class="col-sm-6">
                    					<div class="form group">
				                      		<label for="id">Valor</label>
				                      		<div class="input-group mb-3">
			                        			<input type="text" class="form-control" class="@error('valor') is-invalid @enderror" placeholder="Solo Número, representa %" name="valor" id="valor" onKeypress = "javascript:return NumerosReales(event)" autocomplete="off" maxlength="17">
				                      		</div>
				                      		@error('valor')
				                        		<div class="alert alert-danger">{!!$message!!}</div>
				                      		@enderror
				                    	</div>
                  					</div>
            					</div>
            					</div>
            						<div class="card-footer">
			                		<button type="submit" class="btn btn-success">Guardar Cambios</button>
			                		<a href="{{route('listaDescuento')}}" class="btn btn-default retorno" ><i class="fa fa-chevron-circle-left"></i> Volver</a>
			              		</div>
            			</form>
            		@elseif($accion=="edicion")
            			<div class="card-header">
		                	<h3 class="card-title"><b>EDICIÓN DESCUENTO</b></h3>
		              	</div>
		              	@php
			              $id=$descuento->id.'|'.$descuento->codigoid;
			            @endphp
		              	<form action="{{route('actualizardescuento',$id)}}" method="POST">
		              		{{csrf_field()}}
		              		<div class="card-body">
		              			<div class="row">
		              				<div class="col-sm-2">
		              					<div class="form group">
		              						<label for="id">ID</label>
		              						<div class="input-group mb-3">
                          						<input type="text" class="form-control" disabled=true value="{{$descuento->id}}" class="@error('id') is-invalid @enderror" name="id" id="id">
                        					</div>
                        					@error('id')
                          					<div class="alert alert-danger">{!!$message!!}</div>
                        					@enderror
		              					</div>
		              				</div>

		              				<div class="col-sm-10">
				                      <div class="form-group">
				                        <label>Descripción</label>
				                        <div class="input-group mb-3">
				                          <input type="text" class="form-control" class="@error('descripcion') is-invalid @enderror"
				                   name="descripcion" id="descripcion" value="{{$descuento->desdesc}}" onkeyup="pasarMayusculas(this.value, this.id)" maxlength="100" minlength="2">
				                        </div>
				                        @error('descripcion')
				                      <div class="alert alert-danger">{!!$message!!}</div>
				                      @enderror
				                      </div>
				                    </div>

		              				<div class="col-sm-6">
				                        <div class="form group">
				                          <label for="id">Valor</label>
				                          <div class="input-group mb-3">
				                            <input type="text" class="form-control" class="@error('valor') is-invalid @enderror" name="valor" id="valor" value="{{$descuento->mondesc}}" onKeypress = "javascript:return NumerosReales(event)" autocomplete="off" maxlength="17">
				                          </div>
				                          @error('valor')
				                            <div class="alert alert-danger">{!!$message!!}</div>
				                          @enderror
				                        </div>
				                    </div>

		              			</div>
		              		</div>

		              		<div class="card-footer">
				                <button type="submit" class="btn btn-success">Guardar Cambios</button>
				                <a href="{{route('listaDescuento')}}" class="btn btn-default retorno" ><i class="fa fa-chevron-circle-left"></i> Volver</a>
			              	</div>
		              	</form>
  					@endif
  				</div>
  			</div>
  		</div>
  		</div>
  	</section>
  @endsection

  @section('script')
	<script src="{{asset('js/chequeo.js')}} "></script>
@endsection