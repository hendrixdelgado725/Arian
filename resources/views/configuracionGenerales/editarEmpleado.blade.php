@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Editar Empleado')

  @section('content')

<!--   <div class="content-wrapper">
  <section class="content-header">
   <div class="container-fluid">
     <div class="row mb-2">
      <div class="col-sm-6">
       
      </div>
     </div>
   </div>
  </section>
<<<<<<< HEAD
</div>
     -->
    <section class="content-wrapper">
    <div class="container-fluid">
    <div class="container">
        
        <div class="col-10">
            <!-- general form elements -->
            <div class="card card-info">
                 @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title mt-2"><b>EDICIÓN EMPLEADO</b></h3>
                <div class="d-flex flex-row-reverse bd-highlight">
                  <!-- <div>
                   <a href="{{ url()->previous() }}" class="btn btn-danger"><i class="fas fa-arrow-left"></i> Atras</a>
                  </div> -->
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form action="{{route('actualizarEmpleado',$empleado->id)}}" method="post">
              @method('PUT')
              {{csrf_field()}}
                <div class="card-body">
                 <div class="row">
                 <div class="col-sm-6">
                 <div class="form-group">
                 <label for="tipodocumento">Documento</label>
                    <select class="form-control" style="width: 100%;" data-select2-id="1" tabindex="-1" 
                    aria-hidden="true" name="tipodocumento" class="@error('tipodocumento') is-invalid @enderror" value="{{$empleado->tipodocumento}}">
                        <option data-select2-id="3" value="V" {{ $empleado->tipodocumento == "V" ? 'selected' : '' }}>V</option>
                        <option data-select2-id="43" value="E" {{ $empleado->tipodocumento == "E" ? 'selected' : '' }}>E</option>
                        <option data-select2-id="44" value="J" {{ $empleado->tipodocumento == "J" ? 'selected' : '' }}>J</option>
                        <option data-select2-id="45" value="G" {{ $empleado->tipodocumento == "G" ? 'selected' : '' }}>G</option>
                    </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                    @error('tipodocumento')
                     <div class="alert alert-danger">{!!$message!!}</div>
                   @enderror
                </div>     
                 </div>
                 
               <div class="col-sm-6">
                <div class="form-group">
                <label>N&uacute;mero</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">V/J</span>
                  </div>
                  <input type="text" class="form-control" value="{{$empleado->cedemp}}" placeholder="Ejem:1234.."  name="cedularif" 
                  id="cedularifempleado" class="@error('cedularif') is-invalid @enderror">
                </div>
                @error('cedularif')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
               </div>
               </div>

                <div class="form group">
                <label for="nombre">Nombre</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Aa</span>
                  </div>
                  <input type="text"  onkeyup="pasarMayusculas(this.value, this.id)"  class="form-control" class="@error('nombre') is-invalid @enderror" value="{{$empleado->nomemp}}" placeholder="Ejem: Juan Gutierrez" name="nombre" id="nombreempleado">
                </div>
                @error('nombre')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                <div class="form group">
                    <label for="direccion">Dirección de Domicilio</label>
                <div class="input-group mb-3">
                  <textarea type="text" onkeyup="pasarMayusculas(this.value, this.id)" class="form-control" class="@error('direccion') is-invalid @enderror" placeholder="Dirección de domicilio..." 
                  name="direccion" id="direccionempleado">{{$empleado->dirhab}}</textarea>
                </div>
                @error('direccion')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                <div class="row">
                <div class="col-sm-6">
                <div class="form group">
                <label for="telefono">Numero de teléfono celular</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-phone-square"></i></span>
                  </div>
                  <input type="tel" class="form-control" onKeypress = "javascript:return NumerosReales(event)" class="@error('telefono') is-invalid @enderror" value="{{$empleado->celemp}}" placeholder="Ejem: 041..." name="telefono" id="telefonoempleado"> 
                </div>
                @error('telefono')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div>
        
                <div class="col-sm-6">
                <div class="form group">
                <label for="telefono">Numero de teléfono de habitación</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-phone"></i></span>
                  </div>
                  <input type="tel" class="form-control" onKeypress = "javascript:return NumerosReales(event)" class="@error('telefono2') is-invalid @enderror" value="{{$empleado->telhab}}" placeholder="Ejem: 026..." name="telefono2" id="telefono2"> 
                </div>
                @error('telefono2')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div> <!--col -->
                </div>

                <div class="form group">
                <label for="email">Correo Electrónico</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="email" class="form-control" class="@error('email') is-invalid @enderror" value="{{$empleado->emaemp}}" placeholder="Ejem: Juan@email.com" name="email" id="emailempleado"> 
                </div>
                @error('email')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
              
                <div class="row">
                <div class="col-sm-6">
                 <div class="form-group">
                  <label for="cargo">Cargo</label>
                    <select class="form-control" value="{{$empleado->nomcar}}" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="cargo" class="@error('cargo') is-invalid @enderror">
                    <option selected="selected" disabled>Seleccione Cargo</option>
                        @foreach ($cargos as $cargo)
                        <option value="{{$cargo->codcar}}">{{$cargo->nomcar}}</option>
                        @endforeach
                        @if($empleado->npasicaremp)
                        <option selected value="{{$empleado->npasicaremp->codcar}}">{{$empleado->npasicaremp->nomcar}}</option>
                        @endif
                    </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                    @error('cargo')
                     <div class="alert alert-danger">{!!$message!!}</div>
                   @enderror
                </div>     
                 </div>

                 <div class="col-sm-6">
                 <div class="form-group">
                  <label for="tipodocumento">Estatus del Empleado</label>
                    <select class="form-control" style="width: 100%;" 
                    data-select2-id="1" tabindex="-1" aria-hidden="true" name="estatus"
                     class="@error('estatus') is-invalid @enderror" value="{{$empleado->staemp}}">
                        <option data-select2-id="3" {{ $empleado->staemp == "A" ? 'selected' : '' }}>Activo</option>
                        <option data-select2-id="43" {{ $empleado->staemp == "R" ? 'selected' : '' }}>Egresado</option>
                    </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                    @error('estatus')
                     <div class="alert alert-danger">{!!$message!!}</div>
                   @enderror
                </div>     
                 </div>
                 </div>

               <div class="row">

                 <div class="col">
                   <div class="form group">
                     <label for="nombre">Fecha de Ingreso</label>
                      <div class="input-group mb-3">
                       <input type="date" class="form-control" value="{{$empleado->fecing}}" class="@error('fechaing') is-invalid @enderror" name="fechaing" id="fechaing">
                      </div>
                      @error('fechaing')
                      <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                     </div>
                  </div>


                 <div class="col">
                   <div class="form group">
                     <label for="nombre">Fecha de Egreso</label>
                      <div class="input-group mb-3">
                       @if($empleado->fecret)
                         <input type="date" class="form-control" value="{{$empleado->fecret}}" class="@error('fechaegre') is-invalid @enderror" name="fechaegre" id="fechaegre">
                        @else
                         <input type="date" class="form-control" class="@error('fechaegre') is-invalid @enderror" name="fechaegre" id="fechaegre">
                       @endif
                      </div>
                      @error('fechaegre')
                      <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                     </div>
                  </div>

                </div>

                <!-- /.card-body -->
 
               <div class="row">
                <button type="submit" class="btn btn-success mt-4 ml-2"><b> Guardar Cambios</b></button>
                <div>
                  <a href="{{ URL::previous() }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                </div>
               </div> 

                <div class="card-footer">
                 <div class="col">
                  
                 </div>
               </div>
              </form>
            </div>
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->

            
         </form>
        </div>
            <!-- /.card -->
       </div>
      </div>
      </div>
      </section>
  @endsection
  @section('script')
  <script src="{{asset('js/chequeo.js')}} "></script>
  @endsection