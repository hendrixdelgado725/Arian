@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Empleados')
@section('content')

  @component('layouts.contenth')
    @slot('titulo')
      Listado de Empleados
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">
                  <div class="col-sm-4 mt-2">
                    @can('create', App\permission_user::class)
                        {{-- expr --}}
                   <a href="{{route('nuevoEmpleado')}}" class="btn btn-info mt-2"><b> REGISTRAR</b></a>
                    @endcan
                  </div>

                <div class="col mt-2">
                  <form action="{{route('buscarEmpleado')}}" method="GET">
                    @csrf
                    <div class="input-group mt-2">
                      <input type="text" name="filtro" placeholder="Buscar empleado..." class="form-control float-right">
                      <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Buscar"><i class="fas fa-search"></i></button>
                      <a href="{{URL::previous()}}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                       
                     </div>
                  </form>
                  
                </div><!--col-->
                 </div><!--row -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>#</th>
                      <th>C&eacute;dula</th>
                      <th>Nombre</th>
                      <th>Direcci&oacute;n</th>
                      <th>Cargo</th>
                      <th>Estatus</th>
                      <th>Detalles Adicionales</th>
                      <th>Acción</th>
                    </tr>
                  </thead>
                  <tbody style="text-align:center"> 
                  @foreach($empleados as $empleado)
                  @php
                    //dd();
                    $rand  = rand(1, 9999);
                    $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                    $enc=Helper::EncriptarDatos($nrand.'-'.$empleado->id);
                  @endphp
                    <tr>
                      <td>{{$empleados->firstItem() + $loop->index}}</td>
                      <td>{{$empleado->cedemp}}</td>
                      <td>{{$empleado->nomemp}}</td>
                      <td>{{$empleado->dirhab}}</td>
                      @if($empleado->npasicaremp)
                      <td>{{$empleado->npasicaremp->nomcar}}</td>
                      @else 
                      <td>NO TIENE CARGO ASIGNADO</td>
                      @endif
                     <td>{{$empleado->staemp=='A'?'Activo':'Egresado'}}</td>
                     <td>
                     {{-- @can('viewAny', App\permission_user::class) --}}
                     <a type="button" data-toggle="modal" data-target="#modalempleado" 
                      data-target-cargo="{{ $empleado->nomcar }}" data-target-email="{{ $empleado->emaemp }}" 
                       data-target-phone="{{ $empleado->celemp}}" data-target-rif="{{$empleado->rifemp}}" 
                       data-target-codemp="{{ $empleado->codemp}}" data-target-fecing="{{$empleado->fecing}}" data-target-fecret="{{$empleado->fecret}}" data-toggle="tooltip" data-placement="top" title="Detalles"><i class="fas fa-search-plus"></i></a>     
                    {{-- //@endcan --}}
                     
                     </td>
                      <td>
                        @can('edit', App\permission_user::class)
                            {{-- expr --}}
                        <a href="{{route('editarEmpleado',$enc)}}">
                          <i class="fa fa-edit blue" data-toggle="tooltip" data-placement="top" title="Editar Empleado"></i>
                        </a>
                        /
                        @endcan
                        @can('delete', App\permission_user::class)
                        <a href="{{route('eliminarEmpleado',$enc)}}" class="borrar" id="deleteempleado" 
                        data-id="{{$enc}}" data-toggle="tooltip" data-placement="top" title="Eliminar empleado">
                          <i class="fas fa-trash red"></i>
                        </a>
                          
                        @endcan
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!!$empleados->appends(request()->input())->render()!!}
                {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
        <div class="modal fade" id="modalempleado" tabindex="-1" role="dialog" aria-labelledby="modalempleado" aria-hidden="true">
           <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content"> 
            <div class="modal-header">
             <h5 class="modal-title" id="modalempleadolabel">DETALLES DE EMPLEADO</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
              </button>
            </div>
             <div class="modal-body content-centered">
              <div class="col-sm-12">
              <label for="phone">Telefono</label>
               <div class="form-group">
                <input class="form-control" type="text" id="pass_phone" readonly>
               </div>
              </div>
              <div class="col-sm-12">
              <label for="codigo">Codigo de empleado</label>
               <div class="form-group">
                <input class="form-control" type="text" id="pass_codemp" readonly>
               </div>
              </div>
              <div class="col-sm-12">
               <label for="fecing">Fecha de ingreso</label>
                <div class="form-group">
                 <input class="form-control" type="text" id="pass_fecing" readonly>
                </div>
             </div>
              <div class="col-sm-12">
               <label for="fecret">Fecha de Egreso</label>
                <div class="form-group">
                 <input class="form-control" type="text" id="pass_fecret" readonly>
                </div>
             </div>
             <div class="col-sm-12">
              <label for="rif">Rif</label>
               <div class="form-group">
                <input class="form-control" type="text" id="pass_rif" readonly>
               </div>
              </div>
              <div class="col-sm-12"> 
               <label for="email">Correo Electronico</label> 
                <div class="form-group">
                 <input class="form-control" type="text" id="pass_email" readonly>
                </div>
              </div>
            </div>

           <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal"><b> Cerrar</b></button>
           </div>
         </div>
       </div>
     </div>


</div>
</div> 
</div> <!-- container --> 
@endsection
@section('script')
<script src="{{ asset('js/eliminarEmpleado.js') }}" ></script>
<script src="{{ asset('js/modalEmpleado.js') }}"></script>
   <script type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
    });
   </script>
@endsection 