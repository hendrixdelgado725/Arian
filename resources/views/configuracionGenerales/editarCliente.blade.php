@extends('layouts.app')
@extends('layouts.menu')

@section('titulo','A1WIN - Editar Cliente')
@section('content')
<!-- 
<div class="content-wrapper">
  <section class="content-header">
   <div class="container-fluid">
     <div class="row mb-2">
      <div class="col-sm-6">
       
      </div>
     </div>
   </div>
  </section>
</div> -->

  <section class="content-wrapper">
  <div class="container-fluid"></div>
  <div class="container">
        
        <div class="col-10">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title mt-2"><b>EDICIÓN DE CLIENTE</b></h3>
                <div class="d-flex flex-row-reverse bd-highlight">
                <!-- <div>
                   <a href="{{ url()->previous() }}" class="btn btn-danger"><i class="fas fa-arrow-left"></i><b> Atras</b></a>
                  </div> -->
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="{{route('actualizarCliente',$cliente->id)}}" method="POST">
              @method('PUT')
              {{csrf_field()}}
                <div class="card-body">
               
                 <div class="row">
                 <div class="col-sm-3">
                 <div class="form group">
                <label for="id">ID</label>
                <div class="input-group mb-3">
                  <input type="text" class="form-control" disabled=true value="{{$cliente->id}}" class="@error('id') is-invalid @enderror" name="id" id="id">
                </div>
                @error('id')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div>

                 <div class="col-sm-3">
                 <div class="form-group">
                  <label for="tipodocumento">Documento</label>
                    <select class="form-control select2" style="width: 100%;" data-select2-id="1" tabindex="-1" 
                    aria-hidden="true" name="tipodocumento" value="{{$cliente->tipodocumento}}">
                        <option data-select2-id="3" value="V" {{ $cliente->tipodocumento == "V" ? 'selected' : '' }}>V</option>
                        <option data-select2-id="43" value="E" {{ $cliente->tipodocumento == "E" ? 'selected' : '' }}>E</option>
                        <option data-select2-id="44" value="J" {{ $cliente->tipodocumento == "J" ? 'selected' : '' }}>J</option>
                        <option data-select2-id="45" value="G" {{ $cliente->tipodocumento == "G" ? 'selected' : '' }}>G</option>
                        <option data-select2-id="46" value="R" {{ $cliente->tipodocumento == "R" ? 'selected' : '' }}>R</option>
                    </select><span dir="ltr" data-select2-id="2" style="width: 100%;"><span class="selection">
                    <!-- @error('tipodocumento')
                     <div class="alert alert-danger">{!!$message!!}</div>
                   @enderror -->
                </div>     
                 </div>
                 
               <div class="col-sm-6">
                <div class="form-group">
                <label>N&uacute;mero</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">V/J</span>
                  </div>
                  <input type="text" readonly  class="form-control" placeholder="Ejem: 1234567" name="cedularif" 
                  id="cedularifcliente"  value="{{$cliente->cedula}}" onKeypress="javascript:return SoloNumeros(event)">
                </div>
               <!--  @error('cedularif')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror -->
                </div>
               </div>
               </div>

                <div class="form group">
                <label for="nombre">Nombre o Raz&oacute;n Social</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Aa</span>
                  </div>
                  <input type="text" onkeyup="pasarMayusculas(this.value, this.id)" class="form-control" class="@error('nombre') is-invalid @enderror"
                   name="nombre" id="nombrecliente" value="{{$cliente->nompro}}">
                </div>
                @error('nombre')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                <div class="form group">
                    <label for="direccion">Direcci&oacute;n Fiscal</label>
                <div class="input-group mb-3">
                  <textarea type="text" class="form-control" class="@error('direccion') is-invalid @enderror" 
                  name="direccion" onkeyup="pasarMayusculas(this.value, this.id)" id="direccioncliente">{{$cliente->dirpro}} </textarea>
                </div>
                @error('direccion')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                <div class="form group">
                <label for="telefono">N&uacute;mero de tel&eacute;fono</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-phone-square"></i></span>
                  </div>
                  <input type="tel" onKeypress = "javascript:return NumerosReales(event)" class="form-control" class="@error('telefono') is-invalid @enderror"
                   name="telefono" id="telefonocliente" value="{{$cliente->telpro}}"> 
                </div>
                @error('telefono')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>

                <div class="form group">
                <label for="email">Email</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
                  <input type="email" class="form-control" class="@error('email') is-invalid @enderror" 
                   value="{{$cliente->email}}" name="email" id="emailcliente"> 
                </div>
                @error('email')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
              
                <!-- /.card-body -->
                <div class="row">
                  <button type="submit" class="btn btn-success mt-4 ml-2"><b> Guardar Cambios</b></button>
                  <div>
                   <a href="{{ URL::previous() }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                 </div>
                </div>

                <!-- <div class="card-footer">
                </div> -->
              </form>
            </div>
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->

            
         </form>
        </div>
            <!-- /.card -->
       </div>
      </div>
      </div> <!--container fluid -->
      </section>
@endsection
@section('script')
  <script src="{{asset('js/chequeo.js')}} "></script>
  <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
    });
   </script>
  @endsection
    



     
    