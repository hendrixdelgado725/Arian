@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Detalles de Presupuesto')
@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
    @endslot
  @endcomponent
  {{-- CONTENT --}}
 <div class="container-fluid">
  <div class="row">
    <div class="col-lg-12">
      <!-- Main content -->
      <div class="invoice p-1 mb-3">
        <!-- Encabezado Enmarcado-->
        <div class="border border-dark mr-1">
          <div class="col-11 d-flex justify-content-center" style="margin-bottom: -5px;">
            <h4>PRESUPUESTO N° {{$presupuesto->codpre}}</h4>
          </div>
          <div class="col-11 d-flex justify-content-center" style="margin-bottom: -5px;">
           @if($presupuesto->vencimiento <= $date)
            <h6><b>ESTATUS: VENCIDO</b></h6>
            @else
            <h6><b>ESTATUS: {{$presupuesto->estatus}}</b></h6>
           @endif
          </div>
        </div>
        <!-- /. Fin del Encabezado Enmarcado -->

        <!-- Encabezado-->
        <!-- Fin del Encabezado-->

        <div class="row invoice-info">
          <div class="col-sm-6 invoice-col border-left">
            <div class="col-12 mt-4">
             <b>CLIENTE:</b> {{ $presupuesto->cliente->nompro }}
            </div>
            <div class="col-12 mt-4">
             <b>CEDULA / RIF:</b> {{ $presupuesto->codcli }}
            </div>
            <div class="col-12 mt-4">
             <b>DIRECCIÓN: </b> {{ $presupuesto->cliente->dirpro }}
            </div>
          </div>
          <!-- /.col -->
          <div class="col-sm-6 invoice-col border-left">
            <div class="col-12 mt-4">
             <b>TELEFONO:</b> {{ $presupuesto->cliente->telpro }}
            </h8>
          </div>
          <div class="col-12 mt-4">
           <b>CORREO ELECTRONICO:</b> {{ $presupuesto->cliente->email }}
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table Primary -->
      <div class="row">
        <div class="col-12 mt-4 table-responsive" style="margin-bottom:-10px;">
          <table class="table table-sm">
            <thead>
              <tr style="text-align:center">
                <th>CREADO EL</th>
                <th>ELABORADO POR</th>
              </tr>
              <tr style="text-align:center">
                <td>{{ $presupuesto->created_at }}</td>
                <td>{{ $presupuesto->autor }}</td>
              </tr>
            </thead>
          </table>
        </div>
      </div>

      <!-- Table Secundary -->
      <div class="row" style="margin-top: -10px;">
        <div class="col-12 mt-4 table-responsive">
                  <div class="table-header">
          <h6 class="table-title m-0 mt-4 mb-4" style="text-align:center"><b>ARTICULOS</b></h6>
          </div>
          <table class="table table-sm">
            <thead>
              <tr style="text-align:center">
                <th>CODIGO</th>
                <th>DESCRIPCIÓN</th>
                <th>CANTIDAD</th>
                <th>MONTO UNIT.</th>
                <th>DESCUENTO</th>
                <th>TOTAL</th>
              </tr>
              @foreach ($articulos as $articulo)
              <tr style="text-align:center">
                <td>{{ $articulo->codart}}</td>
                <td>{{ $articulo->articulo->desart }}</td>
                <td>{{ $articulo->cantart }}</td>
                <td>{{ number_format($articulo->preunit, 2, ',', '.').' '.$presupuesto->moneda->nomenclatura }}</td>
                <td>{{ number_format($articulo->monto_descuento, 2, ',', '.') }}</td>
                <td>{{ number_format($articulo->totart, 2, ',', '.') }}</td>
              </tr>
              @endforeach
            </thead>
          </table>
        </div>
      </div>


      <div class="col-5 mt-4 mb-4" style="margin-left: 60%; margin-top: -20px;">
        <div class="table-responsive " >
          <table class="table table-sm " >
            
            <tbody>
              <tr>
                <th>Sub-Total</th>
                @if($presupuesto->moneda->nombre == "PETRO")
                <td><strong>{{ number_format($presupuesto->subtotalpre, 11, ',', '.').' '.$presupuesto->moneda->nomenclatura }}</strong></td>
                @else
                 <td><strong>{{ number_format($presupuesto->subtotalpre, 2, ',', '.').' '.$presupuesto->moneda->nomenclatura }}</strong></td>
                @endif
              </tr>
              @if ($presupuesto->mondesc)
              <tr>
                <th>Monto descuento</th>
                @if($presupuesto->moneda->nombre == "PETRO")
                <td><strong>{{ number_format($presupuesto->mondesc, 11, ',', '.').' '.$presupuesto->moneda->nomenclatura }}</strong></td>
                @else
                <td><strong>{{ number_format($presupuesto->mondesc, 2, ',', '.').' '.$presupuesto->moneda->nomenclatura }}</strong></td>
              @endif
              </tr>
              @endif
              <tr>
                <th>IVA ({{ $presupuesto->recargo->nomrgo }})</th>
                @if($presupuesto->moneda->nombre == "PETRO")
                <td><strong>{{ number_format($presupuesto->monrgo, 11, ',', '.').' '.$presupuesto->moneda->nomenclatura }}</strong></td>
                @else
                <td><strong>{{ number_format($presupuesto->monrgo, 2, ',', '.').' '.$presupuesto->moneda->nomenclatura }}</strong></td>
                @endif
              </tr>

              <tr>
                <th>Total</th>
                @if($presupuesto->moneda->nombre == "PETRO")
                <td><strong>{{ number_format($presupuesto->monpre, 11, ',', '.').' '.$presupuesto->moneda->nomenclatura }}</strong></td>
                @else
                <td><strong>{{ number_format($presupuesto->monpre, 2, ',', '.').' '.$presupuesto->moneda->nomenclatura }}</strong></td>
                @endif
              </tr>
              @if($presupuesto->moneda->nombre == "BOLIVAR")
              <tr>
                <th>Total ($)</th>
                <td><strong>{{ number_format($presupuesto->monpre/$presupuesto->tcambio->valor,2,',','.').' $' }}</strong></td>
                </tr>
              @endif
              {{-- <tr>
                <th>Cambio </th>
                @if($presupuesto->moneda_id == $presupuesto->cambioMoneda2->codigoid)
                 @if($presupuesto->cambioMoneda->nombre == "PETRO")
                 <td><strong>{{number_format($presupuesto->monto_tasa, 11, ',', '.').' '.$presupuesto->cambioMoneda->nomenclatura}}</strong></td>
                 @else
                <td><strong>{{number_format($presupuesto->monto_tasa, 2, ',', '.').' '.$presupuesto->cambioMoneda->nomenclatura}}</strong></td>
                 @endif
                @else
                 @if($presupuesto->cambioMoneda->nombre == "PETRO")
                 <td><strong>{{number_format($presupuesto->monto_tasa, 11, ',', '.').' '.$presupuesto->cambioMoneda2->nomenclatura}}</strong></td>
                 @else
                <td><strong>{{number_format($presupuesto->monto_tasa, 2, ',', '.').' '.$presupuesto->cambioMoneda2->nomenclatura}}</strong></td>
                @endif
                @endif
              </tr> --}}
            </tbody>
            
          </table>
          </div>
        </div>
        <div class="row no-print" style="margin-top: -20px; margin-bottom: -5px;">
        <div class="col-12 mb-4">
        <a href="{{ URL::previous() }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>

        </div>
        </div>
      </div>
      <!-- this row will not appear when printing -->
   <!--    <div class="row no-print card-footer">
        <div class="col-12 mb-4">
        <a href="{{ URL::previous() }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>

        </div>
      </div> -->
   </div>
   <!-- /.invoice -->
 </div>
</div>


@endsection