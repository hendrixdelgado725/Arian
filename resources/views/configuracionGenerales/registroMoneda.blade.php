@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Registro de Moneda')

  @section('content')

<!--   <div class="content-wrapper">
  <section class="content-header">
   <div class="container-fluid">
     <div class="row mb-2">
      <div class="col-sm-6">
       
      </div>
     </div>
   </div>
  </section>
</div> -->
    
    <section class="content-wrapper">
    <div class="container-fluid">
    <div class="container">
        
        <div class="col-10">
            <!-- general form elements -->
            <div class="card card-info">
              @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title mt-2"><b>REGISTRO MONEDA</b></h3>
                <div class="d-flex flex-row-reverse bd-highlight">
                  <!-- <div>
                   <a href="{{ url()->previous() }}" class="btn btn-danger"><i class="fas fa-arrow-left"></i><b> Atras</b></a>
                  </div> -->
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form action="{{route('registrarMoneda')}}" id="saveform" method="post">
              {{csrf_field()}}
                <div class="card-body">
                @include('vendor/flash.flash_message')
                <div class="row">
                <div class="col-sm-6">
                <div class="form group">
                <label for="nombre">Nombre de la Moneda</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Aa</span>
                  </div>
                  <input type="text" class="form-control" class="@error('nombre') is-invalid @enderror" onkeyup="pasarMayusculas(this.value, this.id)" placeholder="Bolivar..." name="nombre" id="nombremoneda">
                </div>
                @error('nombre')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div>

                <div class="col-sm-6">
                <div class="form group">
                <label for="nomenclatura">Nomenclatura de la Moneda</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-money-bill"></i></span>
                  </div>
                  <input type="text" class="form-control" class="@error('nomenclatura') is-invalid @enderror" placeholder="Bs..." name="nomenclatura" id="nomenclatura" onkeyup="pasarMayusculas(this.value, this.id)">
                </div>
                @error('nomenclatura')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div>             
                </div>
                <!-- /.card-body -->

               <div class="card-footer">
                      <button type="boton" id="boton" class="btn btn-success">Guardar Cambios</button>
                      <a href="{{URL::previous()}}" class="btn btn-default retorno" ><i class="fa fa-chevron-circle-left"></i> Volver</a>
                    </div>

                <!-- <div class="card-footer">
                </div> -->
              </form>
            </div>
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->

            
         </form>
        </div>
            <!-- /.card -->
       </div>
      </div>
      </div>
      </section>
  @endsection
  @section('script')
<script src="{{asset('js/chequeo.js')}} "></script>
<script>
  document.getElementById("boton").addEventListener("click",function(){
      console.log("llego");

      $("#boton").attr("disabled", true);

      setTimeout(() => {
        
        document.getElementById('saveform').submit();
       }, 1000);
  })
</script>
@endsection