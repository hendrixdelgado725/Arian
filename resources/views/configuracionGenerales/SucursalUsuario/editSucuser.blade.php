@extends('layouts.app')
@extends('layouts.menu')
@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Sucursal
    @endslot
  @endcomponent
  {{-- CONTENT --}}
  <section class="content">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-8">
          <div class="card card-info">
            @include('vendor/flash.flash_message')
            <div class="card-header">
            <h3 class="card-title">Registro de Sucursal</h3>
            
            </div>
            <!-- /.card-header -->
            <!-- form start -->
              <form action="{{route('sucursalUpdate',$sucursal->id)}}" method="post">
                @method('PUT')
                {{csrf_field()}}
                <div class="card-body">
                  <div class="form-group">
                    <label for="nomsucu">Nombre Sucursal</label> 
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <div class="input-group-text"></div>
                      </div> 
                    <input id="nomsucu" name="nomsucu" type="text" class="form-control @error('nomsucu') is-invalid @enderror" value="{{$sucursal->nomsucu}}">
                    </div>
                    @error('nomsucu')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                  </div>

                  <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                        <label for="tipodoc">Tipo de documento</label>
                          <select class="form-control" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="tipodoc" class="@error('tipodoc') is-invalid @enderror">
                              <option value="V"{{substr($sucursal->codrif,0,1)==='V' ? 'selected' : ''}}>V</option>
                              <option value="E"{{substr($sucursal->codrif,0,1)==='E' ? 'selected' : ''}}>E</option>
                              <option value="J"{{substr($sucursal->codrif,0,1)==='J' ? 'selected' : ''}}>J</option>
                              <option value="G"{{substr($sucursal->codrif,0,1)==='G' ? 'selected' : ''}}>G</option>
                          </select>
                          @error('tipodoc')
                            <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                      </div>     
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                      <label>Numero de documento</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">V/J</span>
                          </div>
                          <input type="text" class="form-control" placeholder="Ejem: 1234567" name="codrif" 
                          id="codrif" class="@error('codrif') is-invalid @enderror"
                        value="{{substr($sucursal->codrif,1)}}" data-mask="99999999-9">
                        </div>
                      @error('codrif')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="dirfis">Dirección Fiscal</label> 
                    <textarea id="dirfis" name="dirfis" cols="40" rows="3" class="form-control @error('dirfis') is-invalid @enderror">{{$sucursal->dirfis}}</textarea>
                    @error('dirfis')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                  </div>
                  @foreach ($sucrgo as $item)
                      @foreach ($item->recargos as $item)
                          <b>{{$item->codrgo}}</b>
                      @endforeach
                  @endforeach
                  <div class="form-group mt-2">
                    <div class="row">
                      <div class="col-xl-8 col-md-8">
                        <h6><b>Seleccion de Recargos</b></h6>
                        <div class="table-wrapper-scroll-y my-custom-scrollbar">
                          <table class="table table-bordered">
                            <thead>
                              <th>N°</th>
                              <th>Recargo</th>
                            </thead>
                            <tbody>
                              @for ($i = 0; $i < count($recargos); $i++)
                                @php
                                  $times = count($sucrgo);
                                @endphp
                                <tr data-row="{{$i}}">
                                  <td>{{$i."   ".$times}}</td>
                                  <td>
                                <select data-row="{{$i}}" name="codrgo[]" class="form-control entidad">
                                  <option value="">Seleccione el Recargo</option>
                                    @foreach ($recargos as $item)
                                    <option value="{{$item->codrgo}}" data-num="{{$item->nomrgo}}">{{$item->nomrgo}}</option>
                                    @endforeach
                                  </select>
                                </td>
                              </tr>
                              @endfor
                            </tbody>
                          </table>
                        </div>
                        {{-- <h6 class="mt-2"><b>Cantidad de Recargos Selecionados :</b></h6> --}}
                      </div>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="col-sm-6">
                      <label for="codpai">Seleccione Pais</label>
                        <div class="form-group">
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text">V/J</span>
                            </div>
                        <select id="codpai" name="codpai" class="custom-select"
                          data-uri="{{route('getEstJson')}}"
                        >
                          @foreach ($pais as $item)
                            @php ($selected = $item->id === $sucursal->codpai ? 'selected' : '')
                            <option value="{{$item->id}}" {{$selected}}>{{$item->nompai}}</option>
                          @endforeach
                        </select>
                          @error('codpai')
                            <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                        </div>     
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                      <label for="codedo">Seleccione Estado</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">V/J</span>
                          </div>
                          <select id="codedo" name="codedo" class="custom-select">
                            <option value="">Seleccione Estado</option>
                            
                            @foreach ($estado as $item)
                            
                              <option value="{{$item->id}}" {{$selected}}>{{$item->nomedo}}</option>
                            @endforeach
                          </select>
                        </div>
                      @error('codedo')
                        <div class="alert alert-danger">{!!$message!!}</div>
                      @enderror
                      </div>
                    </div>
                  </div>
                  
              <!-- /.card-body -->
                <div class="card-footer">
                  <button id="createcountry" type="submit" class="btn btn-success"><b>Guardar Cambios</b></button>
                  <a href="{{ url()->previous() }}" class="btn btn-default ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>

                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
        </div>
      </div>
        {{-- row --}}
    </div>
    {{-- container fluid --}}
  </section>
@endsection
@section('script')
<script src="{{ asset('js/sucursales/sucursales.js') }}" ></script>
@endsection
<style>
  .my-custom-scrollbar {
  position: relative;
  height: 300px;
  overflow: auto;
  }
  .table-wrapper-scroll-y {
  display: block;
  }
  </style>