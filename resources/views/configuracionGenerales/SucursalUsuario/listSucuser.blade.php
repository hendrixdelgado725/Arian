@extends('layouts.app')
@extends('layouts.menu')
@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Listado de Sucursales Asignadas a Usuarios
    @endslot
  @endcomponent
  {{-- CONTENT --}}
  <section class="content">
    <div class="container-fluid">
      <div class="row justify-content-center">
          <div class="col-12">
              <div class="card">
              @include('vendor/flash.flash_message')
                <div class="card-header">
                  <h3 class="card-title"><b>LISTADO DE USUARIOS</b></h3>
                    <div class="card-tools">
                      <div class="row">
                        <div class="col">
                          <a href="{{route('sucursalCreate')}}" class="btn btn-info mt-2"><b>ASIGNAR SUCURSAL A USUARIO</b></a>
                        </div>
                        <div class="col">
                          <form action="{{route('buscarSucursal')}}" method="GET">
                            @csrf
                            <div class="input-group mt-2">
                              <input type="text" name="filtro" placeholder="Buscar Sucursal" class="form-control float-right">
                              <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                              <a href="{{route('sucursalList')}}" class="btn btn-default"><i class="fas fa-arrow-left"></i></a>                       
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="card-body">
                  <table class="table text-center">
                    <thead>
                      <tr>
                        <th scope="col">Usuario</th>
                        <th scope="col">Nombre Empleado</th>
                        <th scope="col">Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($sucusers as $item)
                      @php
                        $id=Helper::crypt($item->codid);
                      @endphp
                      <tr>
                          <td>{{$item->loguse}}</td>
                          <td>{{$item->nomuse}}</td>
                          <td>
                            <a href="{{route('sucuserCreate',$id)}}"><i class="fa fa-edit"></i></a>
                            {{-- <a id="delete" data-name="{{$item->nomsucu}}" href="{{route('sucuserDelete',$id)}}"><i class="fa fa-trash"></i></a> --}}
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                  {!!$sucusers->render()!!}
                </div>

              </div>
              <!-- /.card -->
          </div>
        </div>
    </div>
    {{-- container fluid --}}
  </section>
@endsection
@section('script')
<script src="{{ asset('js/sucursales/sucursales.js') }}" ></script>
@endsection