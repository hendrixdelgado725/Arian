@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Registro de Categoria')

  @section('content')

 <!--  <div class="content-wrapper">
  <section class="content-header">
   <div class="container-fluid">
     <div class="row mb-2">
      <div class="col-sm-6">
       
      </div>
     </div>
   </div>
  </section>
</div> -->
    
    <section class="content-wrapper">
    <div class="container-fluid">
    <div class="container">
        
        <div class="col-10">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title mt-2"><b>REGISTRO CATEGOR&Iacute;A DE PRODUCTO</b></h3>
                <div class="d-flex flex-row-reverse bd-highlight">
                  <!-- <div>
                   <a href="{{ url()->previous() }}" class="btn btn-danger"><i class="fas fa-arrow-left"></i><b> Atras</b></a>
                  </div> -->
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form action="{{route('registrarCdeProducto')}}" method="post">
              {{csrf_field()}}
                <div class="card-body">

                <div class="row">
                <div class="col-sm-6">
                <div class="form group">
                <label for="nombre">Nombre</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Aa</span>
                  </div>
                  <input type="text"  onkeyup="pasarMayusculas(this.value, this.id)"  class="form-control" class="@error('nombre') is-invalid @enderror" placeholder="Ejem: Calzado..." name="nombre" id="nombremoneda">
                </div>
                @error('nombre')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div>

                <div class="col-sm-6">
                <div class="form group">
                <label for="nomenclatura">Nomenclatura</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Aa</span>
                  </div>
                  <input type="text"  onkeyup="pasarMayusculas(this.value, this.id)"  class="form-control" class="@error('nomenclatura') is-invalid @enderror" placeholder="Ejem: CZ" name="nomenclatura" id="nomenclaturaCat">
                </div>
                @error('nomenclatura')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div>

                   <div class="col-sm-4">
                  <div class="form-group">
                    &nbsp;
                    &nbsp;
                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                    <input type="checkbox" class="custom-control-input" id="customSwitch3" name="talla">
                      <label class="custom-control-label" for="customSwitch3">¿Implementa Tallas?</label>
                    </div>
                  </div>
                </div>

                </div>
                <!-- /.card-body -->
               <div class="row">
                <button type="submit" class="btn btn-success ml-3 mt-4"><b> Guardar Cambios</b></button>
                <div>
                  <a href="{{ route('listaCdeProducto') }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                </div>
               </div>

                <!-- <div class="card-footer">
                </div> -->
              </form>
            </div>
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->

            
         </form>
        </div>
            <!-- /.card -->
       </div>
      </div>
      </div>
      </section>
  @endsection
  @section('script')
  <script src="{{asset('js/chequeo.js')}} "></script>
  @endsection
    