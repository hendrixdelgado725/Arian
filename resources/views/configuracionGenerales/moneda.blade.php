@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Monedas')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Lista de Monedas
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
            <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title"><b></b></h3>

                <div class="card-tools">
                 <div class="row">
                  <div class="col-sm-4 mt-2">
                    @can('create', App\permission_user::class)
                        
                   <a href="{{route('nuevaMoneda')}}" class="btn btn-info mt-2"><b> REGISTRAR</b></a>
                    @endcan
                  </div>

                <div class="col mt-2">
                  <form action="{{route('buscarMoneda')}}" method="GET">
                    @csrf
                     <div class="input-group mt-2">
                      <input type="text" name="filtro" placeholder="Buscar " class="form-control float-right">
                      <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Buscar"><i class="fas fa-search"></i></button>
                      <a href="{{URL::previous()}}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                       
                     </div>
                  </form>
                  
                </div><!--col-->
                 </div><!--row -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0 text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>#</th>
                      <th>Nombre</th>
                      <th>Nomenclatura</th>
                      <th>Creado el</th>
                      <th>Actualizado el</th>
                      <th>Acción</th>
                    </tr>
                  </thead>
                  <tbody style="text-align:center"> 
                  @foreach($monedas as $moneda)
                  @php
                    $rand  = rand(1, 9999);
                    $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                    $enc=Helper::EncriptarDatos($nrand.'-'.$moneda->id);
                  @endphp
                    <tr>
                      <td>{{$monedas->firstItem() + $loop->index}}</td>
                      <td>{{$moneda->nombre}}</td>
                      <td>{{$moneda->nomenclatura}}</td>
                      <td>{{date("d-m-Y H:m:s", strtotime($moneda->created_at))}}</td>
                      <td>{{date("d-m-Y H:m:s", strtotime($moneda->updated_at))}}</td>
                      <td>
                        @can('edit', App\permission_user::class)
                            
                        <a href="{{route('editarMoneda',$enc)}}">
                          <i class="fa fa-edit blue" data-toggle="tooltip" data-placement="top" title="Editar Moneda"></i>
                        </a>
                        /
                        @endcan
                        @can('delete', App\permission_user::class)
                        <a href="{{route('eliminarMoneda',$enc)}}" class="borrar" id="deletemoneda" 
                        data-id="{{$enc}}" data-toggle="tooltip" data-placement="top" title="Eliminar Moneda">
                          <i class="fas fa-trash red"></i>
                        </a>    
                        /
                        
                        @endcan
                        @if($moneda->activo === false)
                        <a href="#" data-toggle="tooltip" title="Activar Moneda"
                        data-value="activar" data-moneda="{{$moneda->nombre}}" data-activo='{{json_encode($moneda->activo)}}' class="changeStatusArticulo">
                        <i class="fas fa-check"></i></a> 
                        @else
                         <a href="#" title="Desactivar Moneda" data-moneda="{{$moneda->nombre}}" data-value="desactivar" data-toggle="tooltip" data-activo='{{json_encode($moneda->activo)}}' class="changeStatusArticulo"><i class="fa fa-ban blue"></i></a>

                        @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!!$monedas->appends(request()->input())->render()!!}
                {{-- col8  --}}
          </div>
          </div>
        </div>
    </div>
    <div style="display:none">
      <form id="changeStatusArticulo" action="{{route('changeStatusMoneda')}}" method="POST">
        @method('PUT')
        @csrf
        <input type="text" id="moneda" name="moneda">
        <input type="text" id="status" name="status">

        <button id="changeStatusSubmit" type="submit">Save</button>

      </form>
    </div>
      {{-- row --}}
  </div>
  {{-- container fluid --}}
</section> 
  
@endsection
@section('script')
<script src="{{ asset('js/eliminarMoneda.js') }}" ></script>

   <script type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
    });
    $( function(){
  
        $(document).on('click','.changeStatusArticulo',function(e){
         e.preventDefault();
         var moneda = $(this).data('moneda');
         var activo = $(this).data('activo');
         var status = $(this).data('value');

         Swal.fire({
            title: `${activo === false ? '¿Seguro que desea Activar' : 'Desactivar moneda'} ${moneda}?`,
            text: `¡Ahora el producto`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: `Si, ${moneda}!`
          }).then((resul) => {
              if(resul.value === true){
                $('#changeStatusArticulo #moneda').val(moneda);
                $('#changeStatusArticulo #status').val(status);
                $('#changeStatusSubmit').trigger('click');
              }

          })
        })

    
    })
   </script>
@endsection 



