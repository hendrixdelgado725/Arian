@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Editar Talla')

  @section('content')

  <section class="content-wrapper">
  	<div class="container-fluid pt-5">
  		<div class="container">
  			<div class="col-10">
  				<div class="card card-info">
            @include('vendor/flash.flash_message')
            @if ($accion === 'registro')
            <div class="card-header">
              <h3 class="card-title"><b>EDITAR TALLA</b></h3>
          </div>
              
              <form action="{{route('registrarTalla')}}" method="post">
                  {{csrf_field()}}
                  <div class="card-body">
                      @include('vendor/flash.flash_message')
                      <div class="row">
  
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label >Categor&iacute;a</label>
                          <div class="input-group mb-3">
                            <select style="width:100%" class="form-control select2 categoria" id="categoria" name="categoria" tabindex="-1"  required="required" value="">
                              <option selected="selected" data-select2-id="" disabled>Seleccione una opción</option>
                              @foreach ($categoria as $cat)
                                <option value="{{$cat["codigoid"]}}">{{$cat["nombre"]}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
  
                  <div class="col-sm-6">
                      <div class="form-group">
                          <label>Descripción</label>
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text">M-0</span>
                            </div>
                              <input type="text" name="descripcion" class="form-control" class="@error('codtallas') is-invalid @enderror"  onkeyup="pasarMayusculas(this.value, this.id)" placeholder="Ejemplo: M ó 37" name="codtallas" id="codtallas" autocomplete="off" maxlength="100" minlength="2">
                          </div>
                          @error('descripcion')
                            <div class="alert alert-danger">{!!$message!!}</div>
                           @enderror
                          </div>
                      </div>
  
                  </div>
              </div>
  
                      <div class="card-footer">
                    <button type="submit" class="btn btn-success">Guardar Cambios</button>
                    <a href="{{route('listaTalla')}}" class="btn btn-default retorno" ><i class="fa fa-chevron-circle-left"></i> Volver</a>
                  </div>
                  </form>
            @else
            <div class="card-header">
              <h3 class="card-title"><b>EDITAR TALLA</b></h3>
          </div>
              
              <form action="{{route('editarTalla', $talla->id)}}" method="post">
                  {{csrf_field()}}
                  <div class="card-body">
                      @include('vendor/flash.flash_message')
                      <div class="row">
  
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label >Categor&iacute;a</label>
                          <div class="input-group mb-3">
                            <select style="width:100%" class="form-control select2 categoria" id="categoria" name="categoria" tabindex="-1"  required="required" value="">
                              <option selected="selected" data-select2-id="" disabled>Seleccione una opción</option>
                              @foreach ($categoria as $cat)
                                <option value="{{$cat["codigoid"]}}"{{$cat["codigoid"] === $talla->codcategoria ? 'selected' : ''}}>{{$cat["nombre"]}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
  
                  <div class="col-sm-6">
                      <div class="form-group">
                          <label>Descripción</label>
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text">M-0</span>
                            </div>
                              <input type="text" class="form-control" value="{{old('codtallas', $talla->tallas)}}" class="@error('codtallas') is-invalid @enderror"  onkeyup="pasarMayusculas(this.value, this.id)" placeholder="Ejemplo: M ó 37" name="codtallas" id="codtallas" autocomplete="off" maxlength="100" minlength="2">
                          </div>
                          @error('descripcion')
                            <div class="alert alert-danger">{!!$message!!}</div>
                           @enderror
                          </div>
                      </div>
  
                  </div>
              </div>
  
                      <div class="card-footer">
                    <button type="submit" class="btn btn-success">Guardar Cambios</button>
                    <a href="{{route('listaTalla')}}" class="btn btn-default retorno" ><i class="fa fa-chevron-circle-left"></i> Volver</a>
                  </div>
                  </form>
                
        
            @endif
  	
        
  				</div>
  			</div>
  		</div>
  	</div>
  </section>
  @endsection

  @section('script')
	<script src="{{asset('js/chequeo.js')}} "></script>
  <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
             placeholder: "Seleccione una opción",
        });
        
    });
   </script>
	@endsection