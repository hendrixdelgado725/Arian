@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Patologias')

@section('content')

<section class="content p-5">
    <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
          <div class="card">
            @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title pt-2 pl-2"><b>UNIDAD DE MEDIDA</b></h3>
                <div class="card-tools">
                  <div class="row pr-3">
                    
                  <div class="col-sm-4 mt-3">
                      <a href="{{ route('Unidadmedida.create') }}" class="btn btn-info"><b>REGISTRAR</b></a>                               
                      </div>  
                      <div class="col mt-2">
                <form action="{{route('Unidadmedida.index')}}" method="GET">
                    @csrf
                     <div class="input-group mt-2">
                      <input type="text" name="filtro" placeholder="Buscar " class="form-control float-right">
                      <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Buscar"><i class="fas fa-search"></i></button>
                      <a href="{{route('Unidadmedida.index')}}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                       
                     </div>
                  </form>
                  
                </div><!--col-->
                
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive text-center">
                <table class="table table-hover">
                  <thead>
                    <tr style="text-align:center">
                      <th>#</th>
                      <th>Codigo</th>
                      <th>Nombre</th>
                      <th>Nomenclatura</th>
                      <th>Acción</th>
                    </tr>
                  </thead>
                  <tbody style="text-align:center">

                    @foreach ($unidadmedida as $unidad)
                        <tr>

                            <td>{{ $unidadmedida->firstItem() + $loop->index }}</td>
                            <td>{{ $unidad->codigo }}</td>
                            <td>{{ $unidad->nombre }}</td>
                            <td>{{ $unidad->nomenclatura }}</td>
                            
                            <td>
                
                                <a href="{{ route('Unidadmedida.edit', $unidad->id) }}"><i class="fas fa-edit"></i></a>
                                /
                                <a href="{{ route('Unidadmedida.destroy', $unidad->id) }}" onclick="confirmation(event)" class="borrar"><i class="fas fa-trash red"></i></a>
                    
                            </td>
                             
                        </tr>
                    @endforeach
                      
                  </tbody>
                </table>
                <div class="pl-3 pt-3"> 
             {!!$unidadmedida->render()!!}
             </div>
               
             </div>
            
          </div>
        </div>
    </div>
      {{-- row --}}
  </div>

  <script>

function confirmation(ev) {
  ev.preventDefault();
  Swal.fire({
            title: '¿Está seguro de eliminar esta Unidad de Medida?',
            text: '¡Esta acción es irreversible!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'No, cancelar'
        }).then((result) => {
            if (result.value) {
                // Proceder con la eliminación usando AJAX
                const url = ev.target.closest('a').href;
                $.ajax({
                    url: url,
                    method: 'GET',
                    success: function(response) {
                        if (response.success) {
                            // Manejar la eliminación exitosa
                            Swal.fire({
                                title: 'Eliminado',
                                text: 'La Unidad de Medida se ha eliminado correctamente.',
                                icon: 'success',
                                confirmButtonText: 'Aceptar'
                            }).then((result) => {
                                if (result.value) {
                                    // Actualizar la interfaz de usuario (por ejemplo, eliminar la fila)
                                    $(this).parents('tr').fadeOut();
                                    window.location = response.redirect 
                                }
                            });
                        } else {
                            // Manejar la respuesta de error
                            Swal.fire({
                                title: 'Error',
                                text: response.message,
                                icon: 'error',
                                confirmButtonText: 'Aceptar'
                            });
                        }
                    },
                    error: function(error) {
                        Swal.fire({
                            title: 'Error',
                            text: 'Se ha producido un error al eliminar la Unidad de Medida.',
                            icon: 'error',
                            confirmButtonText: 'Aceptar'
                        });
                    }
                });
            }
        });
   
    }
      


</script>



@endsection

