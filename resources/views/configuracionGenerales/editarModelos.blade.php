@extends('layouts.app')
@extends('layouts.menu')
  @section('content')

  <div class="content-wrapper">
  <section class="content-header">
   <div class="container-fluid">
     <div class="row mb-2">
      <div class="col-sm-6">
       
      </div>
     </div>
   </div>
  </section>
</div>
    
    <section class="content-wrapper">
    <div class="container-fluid">
    <div class="container">
        
        <div class="col-10">
            <!-- general form elements -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title mt-2"><b>EDITAR MODELO ({{ $modelos->defmodelo }})</b></h3>
                <div class="d-flex flex-row-reverse bd-highlight">
                  <!-- <div>
                   <a href="{{ url()->previous() }}" class="btn btn-danger"><i class="fas fa-arrow-left"></i><b> Atras</b></a>
                  </div> -->
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              {!! Form::open(['route'=>'modelosEditado.editar','method'=>'POST','files'=>'TRUE']) !!}
             
              {{csrf_field()}}
                
                <div class="card-body">
                <div class="row">
                <div class="col-sm-6">
                <div class="form group">
                <label for="nombreColor">Editar Nomenclatura</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-palette"></i></span>
                  </div>
                  <input type="text"  class="form-control" class="@error('nombreColor') is-invalid @enderror"  placeholder="Ejem: Vermellón..." name="nomenclatura" value="{{ $modelos->nomenclatura }}" id="nombreColor">
                </div>
                @error('nombre')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div>

                <div class="row">
                <div class="col-sm-6">
                <div class="form group">
                <label for="nombreColor">Nombre del Modelo</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-palette"></i></span>
                  </div>
                  <input type="text"  class="form-control" class="@error('nombreColor') is-invalid @enderror" placeholder="Ejem: Vermellón..." name="nombreModelo" value="{{ $modelos->defmodelo }}" id="nombreColor">
                </div>
                @error('nombre')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div>

                <div class="col-sm-6">
                <div class="form group">
                <label for="nomencolor">Editar color</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-paint-brush"></i></span>
                  </div>
                  <select class="form-control select2 " name="editarColor">
                    <option></option>
                    @foreach($color as $key)
                      <option value="{{$key->nomencolor}}"> {{ $key->color}}</option>
                    @endforeach
                  </select>
                </div>
                @error('nomencolor')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div>

                <div class="col-sm-6">
                <div class="form group">
                <label for="nomencolor">Editar Categorias</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-paint-brush"></i></span>
                  </div>
                  <select class="form-control select2 " name="editarCategoria">
                    <option></option>
                    @foreach($categoria as $key)
                      <option value="{{$key->nomencat}}"> {{ $key->nombre}}</option>
                    @endforeach
                  </select>
                </div>
                @error('nomencolor')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div>


                <div class="col-sm-6 oculto">
                <div class="form-group">
                <label>SubCategoria </label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Cat</span>
                  </div>
                  <select class="form-control select2 " name="nomsubcategoria">
                    <option></option>
                    @foreach($subcategoria as $key)
                      <option value="{{$key->nomensubcat}}"> {{ $key->nomsubcat}}</option>
                    @endforeach
                  </select>
                </div>
                @error('nomcategoria')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
               </div>
              </div>

                <div class="col-sm-6">
                <div class="form-group">
                <label>Subir foto </label>
                <div class="input-group mb-3">
                  <div class="input-group">
                    <label class="input-group-btn">
                  <span class="btn btn-primary btn-file">
                            Banner <input accept=".jpg,.png,.jpeg,.gif" class="hidden" name="banner" type="file" id="banner">
                        </span>
                    </label>
                    <input class="form-control" id="banner_captura" readonly="readonly" name="banner_captura" type="text" value="">
                </div>
                </div>
                @error('nomenmodelo')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
               </div>

                </div>

                <!-- /.card-body --> 
               <div class="row">
                <button type="submit" class="btn btn-success ml-3 mt-4"><b> Registrar Color</b></button>
                <div>
                  <a href="{{ url()->previous() }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                </div>
               </div>

                <!-- <div class="card-footer">
                </div> -->
              
            </div>
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->

            
         {!! Form::close() !!}
        </div>
            <!-- /.card -->
       </div>
      </div>
      </div>
      </section>
  @endsection
  @section('script')
  
  <script>
    $(document).ready(function(){

        $('.select2').select2({

          placeholder: "Seleccion un Articulo",
        });
         $(document).on('change','.btn-file :file',function(){
          var input = $(this);
          var numFiles = input.get(0).files ? input.get(0).files.length : 1;
          var label = input.val().replace(/\\/g,'/').replace(/.*\//,'');
          input.trigger('fileselect',[numFiles,label]);
        });

        $('.btn-file :file').on('fileselect',function(event,numFiles,label){
          var input = $(this).parents('.input-group').find(':text');
          var log = numFiles > 1 ? numFiles + ' files selected' : label;
          if(input.length){ input.val(log); }else{ if (log) alert(log); }
        });



        

        

    }); 

  </script>
  @endsection
    