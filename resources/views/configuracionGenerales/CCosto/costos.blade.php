  @extends('layouts.app')
  @extends('layouts.menu')
  @section('titulo', 'Centros de costo')

  @section('content')
  @component('layouts.contenth')
    @slot('titulo')
        Lista de Centros de Costo
    @endslot
  @endcomponent
  
  <section class="content">
    <div class="content-fluid">
      <div class="card">
        @include('vendor/flash.flash_message')
        <div class="card-header">
          <h3 class="card-title"></h3>
          <div class="card-tools">
            <div class="row">
              <div class="col-sm-4 mt-2">
               @can('create', App\permission_user::class)
               <a href="{{route('createCCosto')}}" class="btn btn-info"><b>REGISTRAR</b></a>    
               @endcan
                      
                  </div>
  
                  <div class="col mt-2">
                    <form action="{{ route('buscarCCosto') }}" method="GET">
                      @csrf
                      <div class="input-group">
                        <input type="text" name="filtro" placeholder="Buscar " class="form-control float-right">
                            <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                            <a href="{{URL::previous()}}" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Regresar"><i class="fas fa-arrow-left"></i></a>                       
                      </div>
                    </form>
                  </div>
                </div>
          </div>
        </div>
  
        <div class="card-body table-responsive p-0 text-center">
          <table class="table table-hover">
            <thead>
              <tr style="text-align:center; font-weight: bold;">
                <td>Código</td>
                <td>Descripción</td>
                <td>Dirección</td>
                <td>Código - Estado</td>
                <td>Detalles</td>
                <td>Acción</td>
              </tr>
            </thead>
            <tbody>
              @foreach($centros as $centro)
                  @php
                    
                    $rand  = rand(1, 9999);
                    $nrand = str_pad($rand,4,'0',STR_PAD_LEFT);
                    $enc=Helper::EncriptarDatos($nrand.'-'.$centro->id);
                  @endphp
                <tr style="text-align:center">
                  <td>{{ $centro->codcen }}</td>
                  <td>{{ $centro->descen }}</td>
                  <td>{{ $centro->dircen ?? 'N/A' }}</td>
                  <td>{{ $centro->estado->nomedo ?? 'N/A' }}</td>
                  <td><a type="button" data-toggle="modal" data-target="#modalcosto"
                    data-target-codigo="{{ $centro->codcen }}" data-target-descripcion="{{ $centro->descen }}" data-target-responsable="{{ $centro->responsable->nomemp ?? '' }}" data-target-encargado="{{ $centro->encargado->nomemp ?? '' }}"
                      data-toggle="tooltip" data-placement="top" title="Detalles"><i class="fas fa-search-plus"></i></a></td>
                             
                    <td>
                        @can('edit',App\permission_user::class) 
                      <a href="{{route(('editCCosto'), $enc)}}">
                        <i class="fa fa-edit blue"></i>
                      </a>
                      /        
                      @endcan 
                      @can('delete',App\permission_user::class) 
                      <a href="{{route('destroyCCosto', $enc)}}" class="borrar" id="deleteCCosto"
                        data-id="{{$enc}}" data-toggle="tooltip" data-placement="top" title="Eliminar">
                        <i class="fas fa-trash red"></i>
                      </a>
                      @endcan

                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
          {!!$centros->appends(request()->input())->render()!!}
          
        </div>
      </div>
      <!-- </div>
    </div> -->
    </div>

    <div class="modal fade" id="modalcosto" tabindex="-1" role="dialog" aria-labelledby="modalempleado" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
       <div class="modal-content"> 
       <div class="modal-header">
        <h5 class="modal-title" id="modalempleadolabel">DETALLES DE CENTRO DE COSTO</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
         </button>
       </div>
        <div class="modal-body content-centered">
         <div class="col-sm-12">
         <label for="phone">Código</label>
          <div class="form-group">
           <input class="form-control" type="text" id="pass_codigo" readonly>
          </div>
         </div>
         <div class="col-sm-12">
         <label for="codigo">Descripción</label>
          <div class="form-group">
           <input class="form-control" type="text" id="pass_descripcion" readonly>
          </div>
         </div>
         <div class="col-sm-12">
          <label for="fecing">Responsable</label>
           <div class="form-group">
            <input class="form-control" type="text" id="pass_responsable" readonly>
           </div>
        </div>
         <div class="col-sm-12">
          <label for="fecret">Encargado</label>
           <div class="form-group">
            <input class="form-control" type="text" id="pass_encargado" readonly>
           </div>
        </div>
       </div>

      <div class="modal-footer">
       <button type="button" class="btn btn-primary" data-dismiss="modal"><b> Cerrar</b></button>
      </div>
    </div>
  </div>
</div>
  </section>

@endsection
@section('script')
<script src="{{ asset('js/eliminarCCosto.js') }}" ></script>
<script type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
    });

    $(document).ready(function () {
    $("#modalcosto").on("show.bs.modal", function (e) {
        var codigo = $(e.relatedTarget).data('target-codigo');
        var descripcion = $(e.relatedTarget).data('target-descripcion');
        var responsable = $(e.relatedTarget).data('target-responsable');
        var encargado = $(e.relatedTarget).data('target-encargado');
        $('#pass_codigo').val(codigo);
        //$('#pass_cargo').val(cargo);
        $('#pass_descripcion').val(descripcion);
        $('#pass_responsable').val(responsable);
        $('#pass_encargado').val(encargado);



    });
});

   </script>
@endsection

