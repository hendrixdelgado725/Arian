@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Registro de Centro de Costo')

  @section('content')

    
    <section class="content-wrapper">
    <div class="container-fluid">
    <div class="container">
        
        <div class="col-10">
            <!-- general form elements -->
            <div class="card card-info">
              @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title mt-2"><b>REGISTRO CENTRO DE COSTO</b></h3>
                <div class="d-flex flex-row-reverse bd-highlight">
                  <!-- <div>
                   <a href="" class="btn btn-danger"><i class="fas fa-arrow-left"></i><b> Atras</b></a>
                  </div> -->
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form action="{{ route('storeCCosto') }}" method="post">
              {{csrf_field()}}
                <div class="card-body">
                @include('vendor/flash.flash_message')
                <div class="row">
                <div class="col-sm-6">
                <div class="form group">
                <label for="codcen">Código</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Aa</span>
                  </div>
                  <input type="text" class="form-control" maxlength="4" class="@error('codcen') is-invalid @enderror" onkeyup="pasarMayusculas(this.value, this.id)" placeholder="ABCD" name="codcen" id="codcen">
                </div>
                @error('codcen')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div>

                <div class="col-sm-6">
                <div class="form group">
                <label for="descen">Descripción</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-align-left"></i></span>
                  </div>
                  <input type="text" class="form-control" class="@error('descen') is-invalid @enderror" placeholder="División de..." name="descen" id="descen" onkeyup="pasarMayusculas(this.value, this.id)">
                </div>
                @error('descen')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div>             
                </div>

                <div class="row">

                <div class="col-sm-6">
                    <div class="form group">
                    <label for="dircen">Dirección</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa fa-map-marker"></i></span>
                      </div>
                      <input type="text" class="form-control" class="@error('dircen') is-invalid @enderror" placeholder="Dirección" name="dircen" id="dircen" onkeyup="pasarMayusculas(this.value, this.id)">
                    </div>
                    @error('dircen')
                      <div class="alert alert-danger">{!!$message!!}</div>
                    @enderror
                    </div>
                </div>
                    

                    <div class="col-sm-6">
                        <div class="form group">
                        <label for="codpai">Estado</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-map"></i></span>
                          </div>
                          <select class="form-control"  style="width: 70%;" data-select3-id="2" tabindex="-1" aria-hidden="true" name="codpai" class="@error('cargo') is-invalid @enderror">
                            <option selected="selected" disabled>Seleccione codpai</option>
                                @foreach ($estados as $key)
                                <option value="{{$key->id}}">{{$key->nomedo}}</option>
                                @endforeach
                            </select><span dir="ltr" data-select3-id="2" style="width: 100%;"><span class="selection">
                        </div>
                        @error('codpai')
                          <div class="alert alert-danger">{!!$message!!}</div>
                        @enderror
                        </div>
                    </div>

                        
                    

                    </div>

                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form group">
                            <label for="codemp">Responsable</label>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                              </div>
                              <select class="form-control select2 codemp"  style="width: 90%;" data-select3-id="2" tabindex="-1" aria-hidden="true" name="codemp" class="@error('codemp') is-invalid @enderror">
                                <option selected="selected" disabled>Seleccione Responsable</option>
                                    @foreach ($empleados as $key)
                                    <option value="{{$key->cedemp}}">{{$key->nomemp}}</option>
                                    @endforeach
                                </select><span dir="ltr" data-select3-id="2" style="width: 100%;"><span class="selection">
                            </div>
                            @error('codemp')
                              <div class="alert alert-danger">{!!$message!!}</div>
                            @enderror
                            </div>
                        </div>

                        <div class="col-sm-6">
                          <div class="form group">
                          <label for="codemp">Encargado</label>
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fa fa-user-o"></i></span>
                            </div>
                            <select class="form-control select2 cedenc"  style="width: 90%;" data-select3-id="2" tabindex="-1" aria-hidden="true" name="cedenc" class="@error('cedenc') is-invalid @enderror">
                              <option selected="selected" disabled>Seleccione Encargado</option>
                                  @foreach ($empleados as $key)
                                  <option value="{{$key->cedemp}}">{{$key->nomemp}}</option>
                                  @endforeach
                              </select><span dir="ltr" data-select3-id="2" style="width: 100%;"><span class="selection">
                          </div>
                          @error('cedenc')
                            <div class="alert alert-danger">{!!$message!!}</div>
                          @enderror
                          </div>
                      </div>
              
                      </div>
                <!-- /.card-body -->

               <div class="card-footer" style="background-color: white;">
                      <button type="submit" class="btn btn-success">Guardar Cambios</button>
                      <a href="{{ route('listaCCosto') }}" class="btn btn-default retorno" ><i class="fa fa-chevron-circle-left"></i> Volver</a>
                    </div>

                <!-- <div class="card-footer">
                </div> -->
              </form>
            </div>
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->

            
         </form>
        </div>
            <!-- /.card -->
       </div>
      </div>
      </div>
      </section>
      @endsection

  @section('script')
   <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
    });

    function pasarMayusculas(texto, elemento_id)
    {
    document.getElementById(elemento_id).value=texto.toUpperCase();
    }
   </script>
  <script src="{{asset('js/reportes.js')}} "></script>
  @endsection