@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Registro de Requisición')

@section('content')
  @component('layouts.contenth')
    @slot('titulo')
      Registrar Retención
    @endslot
  @endcomponent
  @include('vendor/flash.flash_message')
  <form-retenciones></form-retenciones>
@endsection
