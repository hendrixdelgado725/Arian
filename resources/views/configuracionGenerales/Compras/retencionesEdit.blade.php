@extends('layouts.app')
@extends('layouts.menu')
@section('titulo', 'Edición Centro de Costo')

@section('content')
@component('layouts.contenth')
    @slot('titulo')
        Edición de Retenciones
    @endslot
@endcomponent

<section class="content">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-8">
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">EDITAR RETENCIÓN</h3>
            </div>
            <form action="{{ route('retenciones.update', $retenciones) }}" method="POST">
              @csrf

            <div class="card-body">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="nompro">Codigo:</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                      </div>
                      <input
                        type="text"
                        maxlength="100"
                        class="form-control"
                        placeholder="Codigo"
                        name="codtip"
                        value="{{ $retenciones->codtip ? $retenciones->codtip : '' }}"
                        disabled
                      />
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="nompro">Codificación SENIAT:</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                      </div>
                      <input
                        type="text"
                        maxlength="100"
                        class="form-control"
                        placeholder="Sin Codificación SENIAT"
                        name="codtipsen"
                        value="{{ $retenciones->codtipsen ? $retenciones->codtipsen : '' }}"
                      />
                    </div>
                  </div>
                </div>

                  <div class="col-sm-12">
                  <div class="form-group">
                    <label for="nompro">Descripción:</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                      </div>
                      <input
                        type="text"
                        maxlength="100"
                        class="form-control"
                        placeholder="Descripción"
                        name="destip"
                        value="{{ $retenciones->destip ? $retenciones->destip : '' }}"
                        required
                      />
                    </div>
                  </div>
                </div>

                <div class="col-sm-2 mb-3">
                  <label for="nitpro">Maneja Sustraendo:</label>
                  <div class="form-check">
                    <input
                      id="SiManeja"
                      class="form-check-input"
                      type="radio"
                      name="tipo"   
                      value="true"
                      {{ $retenciones->mansus == true ? 'checked' : '' }}
                    />
                    <label class="form-check-label" for="exampleRadios1">
                      Si
                    </label>
                  </div>
                  <div class="form-check">
                    <input
                      id="NoManeja"
                      class="form-check-input"
                      type="radio"
                      name="tipo"
                      value="false"
                      {{ $retenciones->mansus == false ? 'checked' : '' }}
                    />
                    <label class="form-check-label" for="exampleRadios2">
                      No
                    </label>
                  </div>
                </div>

              </div>
            
            <div id="SiTiene">

                <div class="card-footer">
                    <label>- - - Sustraendo - - -</label>
                

                <div class="row">

                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Unidades Tributarias</label>
                    <div class="input-group">
                      <input
                        type="number"
                        name="unitri"
                        value="{{ $retenciones->unitri ? $retenciones->unitri : '' }}"
                        placeholder="850,00"
                        class="form-control"
                      />
                    </div>
                  </div>
                </div>

                <div class="col-sm-4">
                  <div class="form-group">
                    <label>X   Factor</label>
                    <div class="input-group">
                      <input
                        type="number"
                        name="factor"
                        value="{{ $retenciones->factor ? $retenciones->factor : '' }}"
                        placeholder="0,0000"
                        class="form-control"
                      />
                    </div>
                  </div>
                </div>

                <div class="col-sm-3">
                  <div class="form-group">
                    <label>X   Porc. Sustraendo</label>
                    <div class="input-group">
                      <input
                        type="number"
                        name="porsus"
                        value="{{ $retenciones->porsus ? $retenciones->porsus : '' }}"
                        placeholder="0,00"
                        class="form-control"
                      />
                    </div>
                  </div>
                </div>
                <div class="col-sm-1">
                  <div class="form-group">
                    <label>%</label>
                  </div>
                </div>

                <div class="col-sm-2">
                  <div class="form-group">
                    <label>Base Imponible</label>
                  </div>
                </div>

                <div class="col-sm-4">
                  <div class="form-group">
                    <div class="input-group">
                      <input
                        type="number"
                        name="basimp"
                        value="{{ $retenciones->basimp ? $retenciones->basimp : '' }}"
                        class="form-control"
                      />
                    </div>
                  </div>
                </div>

                <div class="col-sm-4">
                  <div class="form-group">
                    <label>% del Monto Total</label>
                  </div>
                </div>

                </div>
                </div>

            </div>

               <div id="NoTiene">

                <div class="card-footer">       

                <div class="row">      

                <div class="col-sm-2">
                  <div class="form-group">
                    <label>Porcentaje a Retener</label>
                  </div>
                </div>

                <div class="col-sm-3">
                  <div class="form-group">
                    <div class="input-group">
                      <input
                        type="number"
                        name="porret"
                        value="{{ $retenciones->porret ? $retenciones->porret : '' }}"
                        placeholder="0,00"
                        class="form-control"
                      />
                    </div>
                  </div>
                </div>

                <div class="col-sm-1">
                  <div class="form-group">
                    <label>Sobre el</label>
                  </div>
                </div>

                <div class="col-sm-3">
                  <div class="form-group">
                    <div class="input-group">
                      <input
                        type="number"
                        name="basimp"
                        value="{{ $retenciones->basimp ? $retenciones->basimp : '' }}"
                        placeholder="0,00"
                        class="form-control"
                      />
                    </div>
                  </div>
                </div>

                <div class="col-sm-2">
                  <div class="form-group">
                    <label>% del Monto Total</label>
                  </div>
                </div>

                </div>
                </div>

            </div>


              <div class="row mt-3">
                

                <div class="mt-3">
                  <a
                    class="btn btn-default ml-4" href="{{ route('retenciones.index') }}"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a
                  >
                  
                    <button
                      type="submit"
                      class="btn btn-success">
                      Guardar
                    </button>
                    </form>
                  <!--  -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
        

@endsection

@section('script')
   <script type="text/javascript">
    $(document).ready(function () {
        // inicializamos el plugin
        $('select.select2').select2({
        });
    });

    $(document).ready(function(){
      if($("#SiManeja").prop( "checked")){
        $('#SiTiene').show(); 
			  $('#NoTiene').hide();
      } else {
        $('#SiTiene').hide(); 
			  $('#NoTiene').show(); 
      }
	});

    $(document).ready(function(){
		$("#SiManeja").on( "click", function() {
			$('#SiTiene').show(); //muestro mediante id
			$('#NoTiene').hide(); //muestro mediante clase
		 });
     $("#NoManeja").on( "click", function() {
			$('#SiTiene').hide(); //muestro mediante id
			$('#NoTiene').show(); //muestro mediante clase
		 });
	});

    function pasarMayusculas(texto, elemento_id)
    {
    document.getElementById(elemento_id).value=texto.toUpperCase();
    }
   </script>
  <script src="{{asset('js/reportes.js')}} "></script>
@endsection