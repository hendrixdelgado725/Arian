@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','A1WIN - Empresas')

@section('content')
  {{-- HEADER --}}
  @component('layouts.contenth')
    @slot('titulo')
      Listado de Retenciones
    @endslot
  @endcomponent
  {{-- CONTENT --}}
<section class="content">
  <div class="container-fluid">
        <div class="card">
          @include('vendor/flash.flash_message')
          <div class="card-header">
            <h3 class="card-title"></h3>
              <div class="card-tools">
                <div class="row">
                <div class="col-sm-4 mt-2" >
                  @can('create',App\permission_user::class)
                  <a href="{{ route('retenciones.create') }}" class="btn btn-info mt-2"><b>REGISTRAR</b></a>
                  @endcan
                </div>
                <div class="col mt-2">
                  
                  <form action="{{ route('retenciones.index') }}" method="GET">
                    @csrf
                    <div class="input-group  mt-2">
                      <input type="text" name="filtro" placeholder="Buscar" class="form-control float-right" value="{{$filtro}}">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                      <a href="{{URL::previous()}}" class="btn btn-default"><i class="fas fa-arrow-left"></i></a>                       
                    </div>
                  </form>
                </div>
              </div>
              </div>
          </div>
          <div class="card-body">
            <table class="table text-center">
              <thead>
                <tr>
                  <th scope="col">Código (asc)</th>
                  <th scope="col">Descripción</th>
                  <th scope="col">Código Contable</th>
                  <th scope="col">Acción</th>
                </tr>
              </thead>
              <tbody>
                  @foreach($retenciones as $retencion) 
                  <tr>
                    <td>{{ $retencion->codtip }}</td>
                    <td>{{ $retencion->destip }}</td>
                    <td>{{ $retencion->codcon }}</td>
        
                    <td>
             
                      @can('edit',App\permission_user::class)

                      <a href="{{ route('retenciones.edit', $retencion) }}" title="Editar"><i class="fa fa-edit"></i></a>
                      
                      @endcan

                      @can('edit',App\permission_user::class)
                      /
                      <a href="{{ route('retenciones.destroy', $retencion) }}" onclick="confirmation(event)" class="borrar" data-toggle="tooltip" data-placement="top" title="Eliminar">
                          <i class="fa fa-trash red">
                          </i>
                      </a>
        
                      @endcan

                    </td>
                    
                  </tr>
                  @endforeach
                </tbody>
              </table>
            
          </div>
          {!!$retenciones->appends(request()->input())->render()!!}
          </div>
        <!-- </div> -->
    <!-- </div> -->
      {{-- row --}}
  </div>
  {{-- container fluid --}}
  <form id="sendPdf" method="POST" action="{{route('generarReporteRequisicion')}}" style="display:none">
      @csrf
        <input id="reqart" type="text" name="reqart" > 
    </form>
</section>
@endsection
@section('script')
<script>
  $(document).ready(()=>{

    $(document).on('click','.sendPdf',function (e) {
      e.preventDefault();
      let valor = $(this).closest('tr').data('id')
        $('input#reqart').val(valor)
        $('#sendPdf').submit()
        console.log("ssss")
    })
  

    deleteempresa();
      function deleteempresa(){//se insertaran paises a la configuracion del sistema siempre y cuando Existan
        $('a#delete').click(function(e){
          e.preventDefault();
          const row = $(this).parents('tr');
          const url = $(this).attr('href');
		  		Swal.fire({
				  title: 'Seguro que Desea Eliminar?',
				  text: "Despues de esto no se encontrara en la lista!",
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si, Eliminar!'
        }).then((result) => {//dio click en el boton aceptar
          if(result.value){
            $.get(url,function(result){
              if (result.exito) {
                Swal.fire(
                  'Ha sido Eliminado!',
                  'Con Exito.',
                  'success',
                )  
              row.fadeOut();
              }else if(result.error){
                Swal.fire({
                  icon: 'warning',
                  title: result.error,
                  timer:1500
                  })
                }
            })
          }      
        })
		});
    }
  })




function confirmation(ev) {
  ev.preventDefault();
  Swal.fire({
            title: '¿Está seguro de eliminar esta retención?',
            text: '¡Esta acción es irreversible!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar',
            cancelButtonText: 'No, cancelar'
        }).then((result) => {
            if (result.value) {
                // Proceder con la eliminación usando AJAX
                const url = ev.target.closest('a').href;
                $.ajax({
                    url: url,
                    method: 'GET',
                    success: function(response) {
                        if (response.success) {
                            // Manejar la eliminación exitosa
                            Swal.fire({
                                title: 'Eliminado',
                                text: 'La retención se ha eliminado correctamente.',
                                icon: 'success',
                                confirmButtonText: 'Aceptar'
                            }).then((result) => {
                                if (result.value) {
                                    // Actualizar la interfaz de usuario (por ejemplo, eliminar la fila)
                                    $(this).parents('tr').fadeOut();
                                    window.location = response.redirect 
                                }
                            });
                        } else {
                            // Manejar la respuesta de error
                            Swal.fire({
                                title: 'Error',
                                text: response.message,
                                icon: 'error',
                                confirmButtonText: 'Aceptar'
                            });
                        }
                    },
                    error: function(error) {
                        Swal.fire({
                            title: 'Error',
                            text: 'Se ha producido un error al eliminar la retención.',
                            icon: 'error',
                            confirmButtonText: 'Aceptar'
                        });
                    }
                });
            }
        });
   
    }

</script>
@endsection