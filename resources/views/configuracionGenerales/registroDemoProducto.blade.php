@extends('layouts.app')
@extends('layouts.menu')
@section('titulo','Registro de Demografia')

  @section('content')

 <!--  <div class="content-wrapper">
  <section class="content-header">
   <div class="container-fluid">
     <div class="row mb-2">
      <div class="col-sm-6">
       
      </div>
     </div>
   </div>
  </section>
</div> -->
    
    <section class="content-wrapper">
    <div class="container-fluid">
    <div class="container">
        
        <div class="col-10">
            <!-- general form elements -->
            <div class="card card-info">
              @include('vendor/flash.flash_message')
              <div class="card-header">
                <h3 class="card-title mt-2"><b>REGISTRO DE DESCRIPCI&Oacute;N DEL PRODUCTO</b></h3>
                <div class="d-flex flex-row-reverse bd-highlight">
                  <!-- <div>
                   <a href="{{ url()->previous() }}" class="btn btn-danger"><i class="fas fa-arrow-left"></i><b> Atras</b></a>
                  </div> -->
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form action="{{route('registrarDemoProducto')}}" method="post">
              {{csrf_field()}}
                <div class="card-body">
                  @include('vendor/flash.flash_message')
                <div class="row">
                <div class="col-sm-6">
                <div class="form group">
                <label for="nombre">Nombre de la demografia</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-user-tag"></i></span>
                  </div>
                  <input type="text"  onkeyup="pasarMayusculas(this.value, this.id)"  class="form-control" class="@error('nombre') is-invalid @enderror" placeholder="Ejem: Damas..." name="nombre" id="nombredemografia">
                </div>
                @error('nombre')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div>

                <div class="col-sm-6">
                <div class="form group">
                <label for="nomenclatura">Nomenclatura</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-tag"></i></span>
                  </div>
                  <input type="text"  onkeyup="pasarMayusculas(this.value, this.id)"  class="form-control" class="@error('nomenclatura') is-invalid @enderror" placeholder="Ejem: Damas..." name="nomenclatura" id="nomenclaturademo">
                </div>
                @error('nomenclatura')
                  <div class="alert alert-danger">{!!$message!!}</div>
                @enderror
                </div>
                </div>

                </div>

                <!-- /.card-body --> 
               <div class="row">
                <button type="submit" class="btn btn-success ml-3 mt-4"><b> Guardar Cambios</b></button>
                <div>
                  <a href="{{ URL::previous() }}" class="btn btn-default mt-4 ml-4"><i class="fas fa-chevron-circle-left"></i><b> Volver</b></a>
                </div>
               </div>

                <!-- <div class="card-footer">
                </div> -->
              </form>
            </div>
            <!-- /.card -->
           </div>
            <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->

            
         </form>
        </div>
            <!-- /.card -->
       </div>
      </div>
      </div>
      </section>
  @endsection
  @section('script')
  <script src="{{asset('js/chequeo.js')}} "></script>
  @endsection
    