@extends('layouts.app2')
<body class="login-page" style="min-height: 496.8px;background-image: url('{{ asset('/images/arian/inicio/arian-fondo.png')}}');">
<div class="login-box">
    <div class="login-logo" style="align-items: center;">
        <img src="{{ asset('/images/arian/inicio/arian-logo.png') }}" alt="Arian" height="100px" style="margin-left: 80px" class="mb-5">
    </div>
<!-- /.login-logo -->


                <form id="loginForm" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right" style="color: white;">{{ __('Usuario') }}</label>

                            <div class="col-md">
                                <input id="texto" type="text" class="enterable mb-3 form-control @error('texto') is-invalid @enderror" name="texto" value="{{ old('texto') }}" required autocomplete="texto" autofocus >

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right" style="color: white;">{{ __('Contraseña') }}</label>

                            <div class="col-md">
                                <input id="password" type="password" class="enterable mb-3 form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right" style="color: white;">{{ __('Sucursal') }}</label>

                            <div class="col-md-6">
                                <select class="form-control  cambios enterable" name="sucursal" style="width: 240px">
                                    <option selected disabled>SELECCIONE SUCURSAL</option>
                                @foreach($sucursal as $key)
                                    <option value="{{$key->codsuc}}"> {{ $key->nomsucu}}</option>
                                @endforeach
                                </select>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right" style="color: white;">{{ __('Año Fiscal') }}</label>

                            <div class="col-md-6">
                                <select  class="form-control  cambios enterable"style="width: 240px"  name="anio">
                                    <option selected value="0">SELECCIONE AÑO</option>
                                @foreach($year as $key)
                                    <option selected value="{{$key->passemp}}"> {{ $key->nomemp}}</option>
                                @endforeach
                                </select>

                                @error('anio')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-block btn-primary btn-md" style="align-items: center;background: linear-gradient(90deg, blue, purple); height: 40px">
                                <!--  {{ __('Ingresar') }} -->
                                <p style="color:white"><b>Ingresar</b></p>
                                <!--   <img src="{{ asset('/images/arian/inicio/ingresar-boton.png') }}" height="40px"> -->
                                </button>
                                <br><br>
                                @if(Session::has('flash_message'))
                                <div class="alert alert-danger" role="alert">
                                    {{Session::get('flash_message')}}
                                </div>
                                @endif
                                {{Session::forget('flash_message')}}
                            </div>
                        </div>
                    </form>


                    <!--<div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">

                                <a href="{{ route('fasvit.inicio') }}" class="btn btn-block btn-outline-success btn-md" style="height: 40px;"><b>REGISTRAR SOLICITUD</b></a>
                                
                                <br><br>
                            </div>
                        </div>-->

</div>



</section>

@section('js')

<script>
    $(document).ready(function () {
        $('.enterable').keypress(function (e) { 
            if(e.keyCode === 13){
                console.log("beta")
                e.preventDefault()
                $('#loginForm').submit()
            }
            console.log("betaNO")
        });
    });
</script>

@endsection