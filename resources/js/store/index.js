import axios from 'axios'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state : {
    loading: false,
    familiares : [],
    medicamentos : [],
    puntos: [],
  },
  actions : {
    async getInfoEmpleado(state, data){
      state.commit('state',true)
      try{
        const response = await axios.post('/modulo/Fasvit/getEmpleado', data)
        state.commit('state',false)
        return response
      }catch(error){
        state.commit('state',false)
        return error
      }
    },
    prepare_for_edit(state, data){
      
    }
  },
  mutations : {
    setMuted(state, {prop, payload}) {
      state[prop] = payload
    },
    state(state, payload){
      state.loading = payload
    }
  }
})