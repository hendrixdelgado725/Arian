const methods_editing = {
  async approve(){
    await this.action('approve')
    console.log("aprobando")
  },
  async reject(){
    await this.action('reject')
    console.log("rechazando")
  },
  action(string){
    this._disabled = true

    const only_one_par = [
      'MADRE',
      'CONYUGUE',
      'CONCUBINO (A)',
      'PADRE'
    ]

    const validate_parentescos = this.tipos_parentesco_parsed.map((parentesco) => {
      return this.familiares.filter((familiar) => {
        return familiar.parfam == parentesco.tippar && only_one_par.includes(parentesco.despar)
      }).length
    })

    if(validate_parentescos.includes(2)){
      this._disabled = false
      this.$toast.error(
        "Verifica bien los familiares"
      );
      return
    }

    const actions = {
      'approve' : {
        question : 'Aprobar',
        response : 'Aprobada'
      },
      'reject' : {
        question : 'Rechazar',
        response : 'Rechazada'
      }
    }

    const form = {
      familiares : this.familiares,
      codigo : this.info.codigo,
    }

    Swal.fire({
      title: `Seguro que desea ${actions[string].question}` ,
      text : 'Esta acción no se puede deshacer!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: actions[string].question,
      showLoaderOnConfirm: true,
      preConfirm: (login) => {
        return fetch(`/modulo/Fasvit/Registro/Familiares/${string}`,{
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN' : document.querySelector('meta[name=csrf-token]').content
          },
          method: "POST",
          body: JSON.stringify(form)
        })
          .then(response => {
            if (!response.ok) {
              throw new Error(response.statusText)
            }
            return response.json()
          })
          .catch(error => {
            Swal.showValidationMessage(
              `Request failed: ${error}`
            )
          })
      },
      allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
      const {value} = result
      if (value.success) {
        Swal.fire({
          title: `Solicitud ${actions[string].response}`,
          icon: 'success',
        })
        window.location.href = value.url
      }
    })
    this._disabled = false
  },

  

  prepareForEdit(){
    this.nombre = this.info_empleado.nomemp
    this.emaemp  = this.info_empleado.emaemp
    this.cedula  = this.info_empleado.codemp
    this.familiares = [...this.info.familiares]
  }
}

export { methods_editing }