/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//import Swal from 'sweetalert2';

//const Swal = require('sweetalert2');

import Swal from "sweetalert2/dist/sweetalert2.js";
import "sweetalert2/src/sweetalert2.scss";
import store from "./store";
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";
import Vue from "vue";

import VueInputRestrictionDirectives from "vue-input-restriction-directives";
import vSelect from "vue-select";
import "vue-select/dist/vue-select.css";
import SmartTable from "vuejs-smart-table";
import FormPresupuesto from "./components/ConfiguracionGeneral/FormPresupuesto.vue";


require("./bootstrap");
window.Vue = require("vue");
Vue.use(Toast);
Vue.use(VueInputRestrictionDirectives);
Vue.use(vSelect);
Vue.use(SmartTable);
/* Componente para la carga de familiares en Vit */
Vue.component('pagination', require('laravel-vue-pagination'));

Vue.component(
    "form-familiares",
    require("./components/fasvit/FormFamiliaresFasvit.vue").default
);

Vue.component(
    "form-api",
    require("./components/ApiTasa.vue").default
);

Vue.component(
    "form-tratamientos",
    require("./components/fasvit/FormTratamientosFasvit.vue").default
);

Vue.component(
    "gerencia",
    require("./components/fasvit/GerenciaTratamientosFasvit.vue").default
);

Vue.component(
    "requisicion",
    require("./components/compras/FormRequisicionesCompras.vue").default
);

Vue.component(
    "reqpresupuesto",
    require("./components/compras/FormReqPresupuesto.vue").default
);

Vue.component(
    "ordenes",
    require("./components/Compras/Ordenes/FormOrdenes.vue").default
);

Vue.component(
    "proveedores",
    require("./components/Compras/Proveedores/FormProveedores.vue").default
);

Vue.component(
    "form-notaentrega",
    require("./components/Compras/Ordenes/FormNotaEntrega.vue").default
);

Vue.component(
    "form-retenciones",
    require("./components/Compras/Retenciones/FormRetenciones.vue").default
);

Vue.component(
    "formpagos",
    require("./components/Compras/Pagos/Pagos.vue").default
);

Vue.component(
    "editpago",
    require("./components/Compras/Pagos/EditPago.vue").default
);

Vue.component(
    "asegurados",
    require("./components/fasvit/asegurados.vue").default
);

Vue.component(
    "form-patologia",
    require("./components/fasvit/FormPatologia.vue").default
);

Vue.component(
    "form-turnos",
    require("./components/facturacion/FormTurno.vue").default
);

Vue.component(
    "parametrizacion-articulos",
    require("./components/Parametrizacion/ParametrizacionArticulos.vue").default
);

Vue.component(
    "from-unidadmedida",
    require("./components/ConfiguracionGeneral/FormUnidadMedida.vue").default
);

Vue.component(
    "cotizacion",
    require("./components/Compras/Cotizaciones/FormCotizacion.vue").default
)

Vue.component("v-select", vSelect);

Vue.component(
    "form-requisionesp",
    require("./components/Compras/Requisicion/FormRequisicionesComprasP.vue")
        .default
);
//Listado de articulos
Vue.component(
    "listado_articulos",
    require("./components/articulos/listadoArticulos.vue")
        .default
);
//control historico de precios articulos 
Vue.component(
    "preciosart",
    require("./components/finanzas/Precios.vue")
        .default
);

Vue.component(
    "fnotaentrega",
    require("./components/facturacion/Fnotaentrega.vue").default
);

Vue.component(
    "fnotashow",
    require('./components/facturacion/FnotaShow.vue').default
)

Vue.component(
    "status-inventario",
    require("./components/inventario/EstatusInventario.vue").default
);

Vue.component(
    "nota-almacen",
    require("./components/inventario/NotaEntregaAlmacen.vue").default
);

Vue.component(
    "listado-seriales",
    require("./components/facturacion/ListadoSeriales.vue").default
);


Vue.component(
    "promociones",
    require("./components/promociones/promociones.vue")
        .default
);
Vue.component(
    "editarpromo",
    require("./components/promociones/editarpromociones.vue")
        .default
);
Vue.component(
    "detallespromo",
    require("./components/promociones/detalles.vue")
        .default
);

Vue.component("form-presupuesto", FormPresupuesto);
Vue.component("v-select", vSelect);



Vue.directive("uppercase", {
    update(el) {
        el.value = el.value.toUpperCase();
    },
});
Vue.directive("uppercase", {
    bind(el, binding, vnode) {
        let handlerInput = (e) => {
            const hasLowercaseRgx = /[a-z]/;
            if (hasLowercaseRgx.test(e.target.value) === true) {
                const start = e.target.selectionStart;
                const end = e.target.selectionEnd;
                e.target.value = e.target.value.toUpperCase();
                e.target.setSelectionRange(start, end);
                e.target.dispatchEvent(new CustomEvent("input"));
            }
        };
        el.addEventListener("input", handlerInput);
    },
});

Vue.directive("select2", {
    inserted(el) {
        $(el).on("select2:select", () => {
            const event = new Event("change", {
                bubbles: true,
                cancelable: true,
            });
            el.dispatchEvent(event);
        });

        $(el).on("select2:unselect", () => {
            const event = new Event("change", {
                bubbles: true,
                cancelable: true,
            });
            el.dispatchEvent(event);
        });
    },
});

Vue.directive("cedulamax", {
    update(el) {
        el.value = el.value.length <= 8 ? el.value : el.value.slice(0, 8);
    },
});

Vue.directive("maxperfect", {
    update(el) {
        el.value = el.value <= 100 ? el.value : 100;
    },
});



const app = new Vue({
    el: "#app",
    store,
});
