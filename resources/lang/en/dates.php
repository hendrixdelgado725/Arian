<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'Mon'=> 'Lun :day',
    'Tue'=> 'Mar :day',
    'Wed'=> 'Mie :day',
    'Thu'=> 'Jue :day',
    'Fri'=> 'Vie :day',
    'Sat'=> 'Sab :day',
    'Sun'=> 'Dom :day',
    
    '01' => 'Ene',
    '02' => 'Feb',
    '03' => 'Mar',
    '04' => 'Abr',
    '05' => 'May',
    '06' => 'Jun',
    '07' => 'Jul',
    '08' => 'Ago',
    '09' => 'Sep',
    '10' => 'Oct',
    '11' => 'Nov',
    '12' => 'Dic'

];
