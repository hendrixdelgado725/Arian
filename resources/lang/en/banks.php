<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

      '100%BANCO' => '100%BANCO',
      'ABN AMRO BANK' => 'AMRO BANK',
      'BANCAMIGA BANCO MICROFINANCIERO, C.A.' => 'MICROFINANCIERO',
      'BANCO ACTIVO BANCO COMERCIAL, C.A.' => 'B. ACTIVO',
      'BANCO AGRICOLA' => 'AGRICOLA',
      'BANCO BICENTENARIO' => 'BICENTENARIO',
      'BANCO CARONI, C.A. BANCO UNIVERSAL' => 'CARONI',
      'BANCO CENTRAL DE VENEZUELA.' => 'BCV',
      'BANCO DE VENEZUELA S.A.I.C.A.' => 'VENEZUELA',
      'BANCO DEL CARIBE C.A.' => 'B. DEL CARIBE',
      'BANCO DEL TESORO' => 'B. TESORO',
      'BANCO ESPIRITO SANTO, S.A.' => '',
      'BANCO EXTERIOR C.A.' => 'B. EXTERIOR',
      'BANCO INTERNACIONAL DE DESARROLLO, C.A.' => 'BANCO INTERNACIONAL DE DESARROLLO, C.A.',
      'BANCO MERCANTIL C.A.' => 'B. MERCANTIL',
      'BANCO NACIONAL DE CREDITO' => 'BNC',
      'BANCO OCCIDENTAL DE DESCUENTO.' => 'B.O.D',
      'BANCO PLAZA' => 'B. PLAZA',
      'BANCO PROVINCIAL BBVA' => 'B. PROVINCIAL',
      'BANCO VENEZOLANO DE CREDITO S.A.' => 'VENEZOLANO DE CREDITO',
      'BANCRECER S.A. BANCO DE DESARROLLO' => 'BANCRECER',
      'BANFANB' => 'BANFANB',
      'BANGENTE' => 'BANGENTE',
      'BANPLUS BANCO COMERCIAL C.A' => 'BANPLUS',
      'CITIBANK.' => 'CITIBANK.',
      'DELSUR BANCO UNIVERSAL' => 'DELSUR',
      'FONDO COMUN' => 'BFC',
      'INSTITUTO MUNICIPAL DE CRÉDITO POPULAR' => '',
      'MIBANCO BANCO DE DESARROLLO, C.A.' => '',
      'SOFITASA' => 'SOFITASA',
      'BANESCO BANCO UNIVERSAL '=> 'BANESCO'

];
