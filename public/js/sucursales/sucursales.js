function pasarMayusculas(texto, elemento_id)
{
    document.getElementById(elemento_id).value=texto.toUpperCase();
}

$(function(){
  changecountry();/// AJAX QUE PIDE LOS ESTADOS DEPENDIENDO DEL PAIS
  deletesu();
  putselectedrcargos();
  putcount ();
  putcodId();
    function desabilitar(){
      let things = $('a.ruledis');
        $(things).hasClass("disabled") ? $(things).removeClass("disabled") : $(things).addClass("disabled");
    }

    function changecountry(){
      $(document).on('click','#codpai',function(){
        let id = $(this).val();
        let url = $(this).data('uri')+'/'+id;
          $.get(url,(response) =>{
            if(response){
              fillestados(response);
            }else if(response.error){
              Swal.fire(
                response.error,
                'error',
              )
            }
          })
      })
    }
    function fillestados(data){
      let content = '';
      $('#codedo').html('').html('<option value="">Seleccione Estado</option>');
      $.each(data,function(index,val){
        content= content.concat('<option value="'+val.id+'">'+val.nomedo+'</option>')
      })
      $('#codedo').html(content);
    }

    function putselectedrcargos(){

      if($('#hiddenrecargos').val()=== undefined) return;
      let data = JSON.parse($('#hiddenrecargos').val()) // ENVIAMOS LOS RECARGOS POR INPUT HIDDEN
      let selects = $('select.entidad');//TODOS LOS SELECT
      $.each(data[0].recargos,function (index,val) { //RECORREMOS LOS RECARGOS
          let selectopt = $(selects).eq(index); //SELECCIONAMOS SOLO 1 select segun la cantidad de recargos existan
          let select = $(selects).eq(index).children(); //buscamos los hijos del select es decir las opciones
            for (let i = 0; i < select.length; i++) {  //recorremos a buscar la opcion que corresponda con los recargos que existen
              if(val.codrgo === $(select).eq(i).val()){
                $(selectopt).val(val.codrgo)
                  let codigoid = getcodigoid(data[0].recargos,val.codrgo)
                  $(selectopt).closest('tr').find('input.codigoid').val(codigoid)
              } 
            }
      })

    }
    function getcodigoid(json,data) {
      let vartoreturn = '';
      for (let index = 0; index < json.length; index++) {
        if(json[index].codrgo === data) vartoreturn = json[index].pivot.codigoid;
      }
      return vartoreturn;
    }

    function putcount (){
      $(document).on('change','select.entidad',function () {
        let selects = $('select.entidad');
        let qty = 0;
          $.each(selects,function(index,val){
            if($(val).val() !== '') qty++;
          })
          $('#h6recargos').html('');
          $('#h6recargos').html('<b>'+'Cantidad de Recargos Selecionados : '+qty+'</b>')
      //$(this).closest('tr').find()$(this).data('codid')
      });
    }

    function putcodId(){
      $(document).on('click','.codigoid',function(){

      })
    }
    
    function deletesu(){
      $(document).on('click','a#delete',function(e){
        e.preventDefault();
        const url =  $(this).attr('href');
        const name = $(this).data('name');
        const row = $(this).parents('tr');
        Swal.fire({
          title: 'Seguro que Desea Eliminar la Sucursal '+name,
          text: "Esta accion no se puede deshacer",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, Eliminar',
          cancelButtonText: 'Cancelar',
      }).then((result) => {//dio click en el boton aceptar
          if(result.value){
            $.get(url,(response) =>{
              if(response.exito){
                row.fadeOut();
                Swal.fire(
                  response.exito,
                  'Con Exito.',
                  'success',
                )
              }else if(response.error){
                Swal.fire(
                  response.error,
                  'error',
                )
              }
            })
          }
      })
      })
    }

  })
