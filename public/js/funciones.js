$(document).ready(function() {
  
    $('.borrarformapago').click(function(e){
            e.preventDefault();
              Swal.fire({
              title: '¿Seguro que desea eliminar la Forma de Pago?',
              text: "¡Despues de esto no se encontrara en la lista!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, eliminar'
            }).then((result) => {
                if (result.value == true) {

                  
                var row = $(this).parents('tr');
                //var form = $(this).parents('data-id');
                var url = $(this).attr('href');
                $.get(url,row,function(result1) {
                    
                  if (result1.titulo === 'error La forma de pago no puede ser eliminado') {

                    
                        Swal.fire(
                          'Error',
                          'La Forma de Pago no puede ser eliminado, posee factura Asociada',
                          'warning',

                        );
                  }else if(result1.titulo === 'La forma de pago se elimino'){

                          Swal.fire(
                          'Hecho',
                          'Se elimino exitosamente',
                          'success',

                        );
                          row.fadeOut();
                  }

                                             
                   

                })   
                }else {
                      Swal.fire(
                          'No se Elimino',
                          '.',
                          'warning',

                        );
                }    
            });

    });

    $('.borrarescala').click(function(e){
            e.preventDefault();
              Swal.fire({
              title: '¿Seguro que desea eliminar la Escala?',
              text: "¡Despues de esto no se encontrara en la lista!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, eliminar'
            }).then((result) => {
                if (result.value == true) {                
                var row = $(this).parents('tr');
                //var form = $(this).parents('data-id');
                var url = $(this).attr('href');
                $.get(url,row,function(result1) {
                    
                  if (result1.titulo === 'error la escala no puede ser eliminado') {
                        Swal.fire(
                          'Error',
                          'La Escala no puede ser eliminado, posee factura Asociada',
                          'warning',

                        );
                  }else if(result1.titulo === 'La escala se elimino'){
                          Swal.fire(
                          'Hecho',
                          'Se elimino exitosamente',
                          'success',

                        );
                          row.fadeOut();
                  }
                })   
                }else {
                      Swal.fire(
                          'No se Elimino',
                          '.',
                          'warning',
                        );
                }    
            });
    });

    $('.borrartalla').click(function(e){
            e.preventDefault();
              Swal.fire({
              title: '¿Seguro que desea eliminar la Talla?',
              text: "¡Despues de esto no se encontrara en la lista!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, eliminar'
            }).then((result) => {
                if (result.value == true) {                
                var row = $(this).parents('tr');
                //var form = $(this).parents('data-id');
                var url = $(this).attr('href');
                $.get(url,row,function(result1) {
                    
                  if (result1.titulo === 'error La talla no puede ser eliminado') {
                        Swal.fire(
                          'Error',
                          'La Talla no puede ser eliminado, se encuentra Asociado a un Artículo',
                          'warning',

                        );
                  }else if(result1.titulo === 'La talla se elimino'){
                          Swal.fire(
                          'Hecho',
                          'Se elimino exitosamente',
                          'success',

                        );
                          row.fadeOut();
                  }
                })   
                }else {
                      Swal.fire(
                          'No se Elimino',
                          '.',
                          'warning',
                        );
                }    
            });
    });

});

$(document).ready(function() {
  
    $('.borrardescuento').click(function(e){
            e.preventDefault();
              Swal.fire({
              title: '¿Seguro que desea eliminar el Descuento?',
              text: "¡Despues de esto no se encontrara en la lista!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, eliminar'
            }).then((result) => {
                if (result.value == true) {

                  
                var row = $(this).parents('tr');
                //var form = $(this).parents('data-id');
                var url = $(this).attr('href');
                $.get(url,row,function(result1) {
                    
                  if (result1.titulo === 'error el descuento no puede ser eliminado') {

                    
                        Swal.fire(
                          'Error',
                          'El Descuento no puede ser eliminado, posee factura Asociada',
                          'warning',

                        );
                  }else if(result1.titulo === 'El descuento se elimino'){

                          Swal.fire(
                          'Hecho',
                          'Se elimino exitosamente',
                          'success',

                        );
                          row.fadeOut();
                  }

                                             
                   

                })   
                }else {
                      Swal.fire(
                          'No se Elimino',
                          '.',
                          'warning',

                        );
                }    
            });

    });
});


$(document).ready(function() {
  
    $('.borrarrecargo').click(function(e){
            e.preventDefault();
              Swal.fire({
              title: '¿Seguro que desea eliminar el Recargo?',
              text: "¡Despues de esto no se encontrara en la lista!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, eliminar'
            }).then((result) => {
                if (result.value == true) {

                  
                var row = $(this).parents('tr');
                //var form = $(this).parents('data-id');
                var url = $(this).attr('href');
                $.get(url,row,function(result1) {
                    
                  if (result1.titulo === 'error el recargo no puede ser eliminado') {

                    
                        Swal.fire(
                          'Error',
                          'El recargo no puede ser eliminado, posee factura Asociada',
                          'warning',

                        );
                  }else if(result1.titulo === 'El recargo se elimino'){

                          Swal.fire(
                          'Hecho',
                          'Se elimino exitosamente',
                          'success',

                        );
                          row.fadeOut();
                  }

                                             
                   

                })   
                }else {
                      Swal.fire(
                          'No se Elimino',
                          '.',
                          'warning',

                        );
                }    
            });

    });
});

$(document).ready(function() {
  
    $('.borrarmovimiento').click(function(e){
            e.preventDefault();
              Swal.fire({
              title: '¿Seguro que desea eliminar el Tipo de Movimiento?',
              text: "¡Despues de esto no se encontrara en la lista!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, eliminar'
            }).then((result) => {
                if (result.value == true) {

                  
                var row = $(this).parents('tr');
                //var form = $(this).parents('data-id');
                var url = $(this).attr('href');
                $.get(url,row,function(result1) {
                    
                  if (result1.titulo === 'error tipo de movimiento no puede ser eliminado') {

                        Swal.fire(
                          'Error',
                          'El Tipo de Movimiento no puede ser eliminado, posee un Inventario Asociado',
                          'warning',

                        );
                  }else if(result1.titulo === 'El tipo de movimiento se elimino'){

                          Swal.fire(
                          'Hecho',
                          'Se elimino exitosamente',
                          'success',

                        );
                          row.fadeOut();
                  }

                                             
                   

                })   
                }else {
                      Swal.fire(
                          'No se Elimino',
                          '.',
                          'warning',

                        );
                }    
            });

    });



});

$(document).ready(function() {
$('.verificarpasswd').click(function(e){
        e.preventDefault();
        // var token = $("input[name='_token']").val();

        var row = $(this).parents('tr');
        var url = $(this).attr('href');

        $.get(url,row,function(result1) {
           console.log(result1);
          if (result1.titulo === 'error no coinciden el password') {
            $('#password').val('');
            $('#password_confirmation').val('');
            Swal.fire(
              'Error',
              'La contraseña ingresada no coinciden',
              'warning',

            );
          }
          else
          {

          }

          })
    });
});

function buscarDatos(valor,bandera)
{
    // $('#loading-image').removeClass('oculto');
    $('#bloquea').show();
    $(function() {
        var token = $("input[name='_token']").val();

        $.ajax({
            type:'POST',
            url:'ajax',
            data: {valor:valor, _token:token, ajax:1},
            success:function(html){
              // console.log(html);
                if(html.options !== '0')
                {
                    valores = html.options;
                    $('#nombre').val(valores.nomemp);
                    $('#direccion').val(valores.dirhab);
                    var tel= valores.celemp;
                    var sep = tel.split('-');
                    let telef=sep[0];     
                    // console.log(telef);              
                    $('#telefono').val(telef);
                    $('#email').val(valores.emaemp);
                }
                else
                {
                    $('#nombre').val('');
                    $('#direccion').val('');
                    $('#telefono').val('');
                    $('#email').val('');
                }

                $('#bloquea').hide();

            }
        }); 
    })
}

function verificarUsuario(valor)
{
    $('#bloquea').show();
    $(function() {
        var token = $("input[name='_token']").val();

        $.ajax({
            type:'POST',
            url:'ajax',
            data: {valor:valor, _token:token, ajax:2},
            success:function(html){
              // console.log(html);
                if(html.options !== '0')
                {
                    valores = html.options;
                    $('#cedularif').val('');  
                    $('#nombre').val('');
                    $('#direccion').val('');
                    $('#telefono').val('');
                    $('#email').val(''); 
                    Swal.fire(
                          'Error',
                          'El documento de Identidad '+valores.cedemp+' posee Usuario en el Sistema',
                          'warning',

                        );
                }
                else
                {

                }

                $('#bloquea').hide();

            }
        }); 
    })
}

function verificarLogin(valor)
{
    $('#bloquea').show();
    $(function() {
        var token = $("input[name='_token']").val();

        $.ajax({
            type:'POST',
            url:'ajax',
            data: {valor:valor, _token:token, ajax:3},
            success:function(html){
              // console.log(html);
                if(html.options !== '0')
                {
                    valores = html.options;
                    $('#usuario').val('');  
                    Swal.fire(
                          'Error',
                          'El usuario '+valores.loguse+' se encuentra registrado',
                          'warning',

                        );
                }
                else
                {
                    Swal.fire(
                          'Hecho',
                          'Usuario Disponible',
                          'success',

                        );
                }

                $('#bloquea').hide();

            }
        }); 
    })
}

$(function(){
  putbanksondelete();
  function putbanksondelete(){
    var borrados = [];
    $(document).on('click','.deleterow',function(e){
      e.preventDefault();
      let sucursal =  JSON.parse($('#sucujson').val());
      let content= '<option selected disabled> Seleccione Sucursal Asociar</option>';
      let content2 = '<a></a>'
      let select = $(this).closest('tr').find('select.sucursal');
      let a  = $(this).closest('tr').find('a.deleterow');
      // console.log(sucursal,select)
      var id =  $(this).closest('tr').find('input.id').val();
      borrados.push(id);
      console.log(sucursal)
      $.each(sucursal,function(index,val){
        content= content.concat(
          '<option value="'+val.codsuc+'">'+val.nomsucu+'</option>'
        );
      })
        $(select).html(content);
        $(a).html(content2);

        $('#borrados').val(borrados);
      // let input = $(this).closest('tr').find('input.numcuenta');
      // $(input).val('')
    })

    $(document).on('click','.deleterowtalla',function(e){
      e.preventDefault();
      let content= '<input> </input>';
      let content1 = '<a></a>'
      let input = $(this).closest('tr').find('input.cantidad');
      let a  = $(this).closest('tr').find('a.deleterowtalla');
      var id =  $(this).closest('tr').find('input.id').val();
      
      borrados.push(id);
      // console.log(borrados);
        $(input).html(content);
        $(a).html(content1);
        $(this).closest('tr').find('input.cantidad');
        $(input).val('');
        $('#borrados').val(borrados);
    })

  }
});

function actualizaTallas(id,ajax,campo_actualiza)
{
    $('#bloquea').show();
    $(function() {
        var campoID = $('#'+id).val();
        var token   = $("input[name='_token']").val();
        // var clasif  = $('#clas_pasaj').val();

        if(campoID){
            $.ajax({
                type:'POST',
                url:'ajax',
                data: {id:campoID, _token:token, ajax:ajax},
                success:function(html){
                  valores = html.options;
                  for (i=0; i<=12; i++){
                    $('#'+campo_actualiza+i).html('');
                    $('#'+campo_actualiza+'oc_'+i).html('');
                    $('#'+campo_actualiza+'ct_'+i).html('');
                    $('#codtalla').val('');
                  }
                  // console.log(campo_actualiza+'_1');
                  let camp='';
                  
                  for (i=0; i<=12; i++)
                  { 
                      let codtalla='';
                      camp = '<input type="text" class="form-control" name="' + campo_actualiza + i + '" id= "' + campo_actualiza + i + '"  class="form-control" value="' + valores[i][0].tallas + '">';
                      camp1='</br><label>'+ valores[i][0].codtallas +'</label>'
                      camp2='<input type="text" class="form-control" onKeypress="javascript:return SoloNumeros(event)" name="' + campo_actualiza + i + '" id="' + campo_actualiza + i + '">'
                      codtallas=valores[i][0].codtallas;
                      // console.log(codtallas);
                      // $('#'+campo_actualiza+'_'+i).append(camp);
                      $('#'+campo_actualiza+i).html(camp);
                      $('#'+campo_actualiza+'oc_'+i).html(camp1);
                      $('#'+campo_actualiza+'ct_'+i).html(camp2);
                      $('#div_tallas_hd_'+i).val(codtallas);
                  }

                }
            }); 
        }
        else
        { $('#'+campo_actualiza).html(''); }
    })
}

function actualizarRefEntrada(valor)
{
  $('#bloquea').show();
    $(function() {
        var token = $("input[name='_token']").val();
        if(!valor) return
        $.ajax({
            type:'POST',
            url:'ajax',
            data: {valor:valor, _token:token, ajax:1},
            success:function(html){
              valores = html.options;
                if(html.options !== '0')
                { 
                    $('#entrada').val(valores);
                    $('#entradax').val(valores);
                }
                else
                {
                    $('#entrada').val(valores);
                    $('#entradax').val(valores);
                }
                $('#bloquea').hide();
            }
        }); 
    })
}



function buscarTallas(valor,ajax,elem,pos)
{
  
  $('#bloquea').show();
    $(function() {

        var token = $("input[name='_token']").val();
        var codalm = $("select.codalm").val();
        var select =  $(elem).closest('tr').find('select.cambios')
        var ubicacion = $(elem).closest('tr').find('select.codubi')
        var clean = $(elem).closest('tr').find('a.cleanLine')
        var tipmov = $("select#movinvent").val()
        let valor = elem.value
        window.codart = elem.value
        console.log(window.codart,"   llegooo hendrix")
        window.isserial = $('option:selected', elem).data('foo')
        console.log(window.isserial)
        var type = (window.isserial === false || window.isserial === null) ? 'none' : 'block' //en caso de no poseer serial el producto
  
        document.getElementById('addSerial'+pos).style.display = type //hace el cambio en el dom
        //window.isserial = $('#seriall'+pos)
        if(!codalm || !tipmov || !valor){
          //clearTable($(elem).closest('tr'))
          return
        }
          $.ajax({
              type:'POST',
              url:'ajax',
              data: {valor:valor,codalm:codalm,tipmov:tipmov, _token:token, ajax:2},
              success:function(html){
                let valores = html.options;
                  if(html.status !== 'error'){
                    
                    if(html.ubicaciones !== '0') 
                    { 
                      $(select).html(valores)
                      $(ubicacion).html(html.ubicaciones)
                      $(elem).attr('data-required',html.requiredFields)

                    }
                    else
                    {
                      alert("error EL ALMACEN NO TIENE UBICACIONES REGISTRADAS")
                    }
                  }else{
                    //$(clean).click()
                    swalError(html.msg)
                  }
                  //$(select).html(content);
                  $('#bloquea').hide();
              }
          });
        
    })
}

function validateCant(elem){//valida que la cantidad maxima no sea excedida al momento de realizar una salida de almacen
  $(function(){
    let tipmov = document.getElementById('movinvent').value
    if(tipmov !== 'SAL') return
    let elemento = document.getElementById(elem)
    $(elemento).attr('data-original-title','');
    let cant = $(elemento).val()
    console.log(cant)

    let arrayMax = $(elemento).closest('tr').find('select.select2mov').children()
    let arrayValues = []
    let tallas =  $(elemento).closest('tr').find('select.cambios').val()
    let exi = arrayMax.eq($('select.select2mov').prop('selectedIndex')).data('exiact')//estatico por ahora
      for (let index = 0; index < arrayMax.length; index++) {//recorro las opciones del select del select para    seleccionar la ubicacion
        /* 
          y la comparo con el select de las tallas para seleccionar el minimo valor entre ellos que coincidan
        */
        for (let j = 0; j < tallas.length; j++) {
          if($(arrayMax).eq(index).data('talla') === tallas[j]){
            arrayValues.push(Number($(arrayMax).eq(index).data('exiact')))
          }else if(tallas[j] === 'N/D'){
            arrayValues.push(Number(exi))
          }
        }
      }
        max = Math.min(...arrayValues)//propagacion

    $(elemento).attr('data-original-title','La cantidad mayor permitida por talla '+max)
    if(cant > max){
      $(elemento).tooltip('show')
      let cant2 = max;
      $(elemento).val(cant2)
    }
  })
}

function changealm() {
  /* Al cambiar el almacen se debe tomar en cuenta que si ya hay articulos seleccionados 
    se debe limpiar la tabla para evitar que salga un articulo que no existe
  */
      var codalm = $('#codalm').val();
      window.codalm = codalm;
      var movinvent = $('#movinvent').val();
      let totalrows = $('#table > tbody >tr');
      var token = $("input[name='_token']").val();
      let error = movinvent == 'SAL' ? 'No tiene articulos ': 'No tiene Ubicaciones';
      if(!codalm|| !movinvent)return
        $.ajax({
          type:'POST',
          url:'ajax',
          data: {codalm:codalm,movinvent:movinvent, _token:token, ajax:3},
          success:function(html){
            let content= '<option selected disabled> Seleccione Sucursal Asociar</option>';
            let valores = html.options;
              if(valores) 
              {
                $.each(totalrows, function (index, value) {
                  clearTable($(totalrows).eq(index),valores)
                });
              }
              else
              {
                let alm = $('#codalm option:selected').text()
                swalError(`Este almacen **${alm}** ${html.errorUbi ? html.errorUbi : ''}  
                  ${html.errorArt ? html.errorArt : ''}
                  `);
                $('#codalm').prop('selectedIndex', 0).change();
                // $('#entrada').val(valores);
              }
              //$(select).html(content);
              $('#bloquea').hide();
          }
      });
}

function clearTable(elemento = '',options = ''){// limpia la tabla de entradas y salidas
    var elementos = $(elemento).find('input.fillable ,select.fillable')
    $.each(elementos, function (index, val) { 
      
      if(val.nodeName === 'SELECT') {
        if($(val).hasClass('select2art')){
          if(options !== ''){
            $(val).html('')
            $(val).html(options)
          }else{
            $(val).prop('selectedIndex', 0);//cambiar opcion del select
            $(val).change()
          }
        }else if($(val).hasClass('codubi')){
          $(val).html('')
          $(val).html('<option value="" selected>UBICACION</option>')
          
        }else if($(val).hasClass('cambios')){
          $(val).html('')
          $(val).html('<option value="N/D" selected>Sin talla</option>')
        }
      }else if(val.nodeName === 'INPUT'){
        if($(val).hasClass('toEmpty'))$(val).val('')
        else if($(val).hasClass('toCero'))$(val).val('0')
      }
    });


}

function swalError(msj) {
  Swal.fire({
    icon: 'error',
    title: 'Oops...',
    text: msj+'!',
    /* footer: '<a href>Why do I have this issue?</a>' */
  })
}