$(function () {

     $(document).on('click','#descontar',function(e){
             e.preventDefault()
             console.log($(this))
              Swal.fire({
              title: '¿DESEA SACAR DE INVENTARIO LOS ARTICULOS ASOCIADOS A LA NOTA?',
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'SI, APROBAR',
              cancelButtonText: 'NO',
            }).then((result) => {
                if (result.value == true) {
            
                var url = $(this).attr('href');
                var id = $(this).data('target-id');

                 $.ajax({
                  type : 'GET',
                  url : url,
                    data : {
                      id : id,
                     },
                success: function (result) { 
                     if(result.exito){
                     Swal.fire({
                          title:'HECHO',
                          text:'LOS ARTICULOS HAN SIDO DESCONTADOS',
                          icon:'success',
                           onClose: () => {
                             location.reload()
                           }
                        });
                     }
                     else{
                       Swal.fire(
                          'ERROR',
                          'HA SURGIDO UN PROBLEMA',
                          'warning',
                        );
                    }
                     
                  }
               })
            };
          })
   })
})