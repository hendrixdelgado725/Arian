
function pasarMayusculas(texto, elemento_id)
{
    document.getElementById(elemento_id).value=texto.toUpperCase();
}

$(function(){

  sendreporteX();
  getStatus()
    function sendreporteX(){
      $(document).on('click','a#reporte',function(e){
        e.preventDefault();
        const url = $(this).attr('href');
        const reporte = $(this).data('r');
          Swal.fire({
              title: 'Seguro que Desea Imprimir Reporte '+reporte+' ?',
              text: "Esta Accion Puede tomar un tiempo",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si Enviar!'
          }).then((result) => {//dio click en el boton aceptar
              if(result.value){
                desabilitar();
                $.get(url,()=>{})
                  .done((response)=>{
                    if(response.exito){
                      desabilitar();
                       Swal.fire(
                        response.exito,
                        'Con Exito.',
                        'success',
                      ) 

                      if(response.reporte === 'Z') checkCaja()
                    }else if(response.error){
                      desabilitar();
                      Swal.fire(
                        'Error!',
                        response.error,
                        'error',
                      )
                    }
                  })
                  .fail((error)=> {
                    desabilitar();
                    Swal.fire(
                      'Error!',
                      error?.responseJSON?.error || 'Error Fatal!',
                      'error',
                    )
                  })
              }
          });
      });
    }


      function sendreporteZ(reporte){
        url = document.getElementsByClassName('reporte'+reporte)['reporte']['href']        
        
        
        Swal.fire({
            title: 'Seguro que Desea Imprimir Reporte '+reporte+' ?',
            text: "Esta Accion Puede tomar un tiempo",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si Enviar!'
        }).then((result) => {//dio click en el boton aceptar
            if(result.value){
              desabilitar();
              $.get(url,()=>{})
                .done((response)=>{
                  if(response.exito){
                    desabilitar();
                     Swal.fire(
                      response.exito,
                      'Con Exito.',
                      'success',
                    ) 

                    if(response.reporte === 'Z') checkCaja()
                  }else if(response.error){
                    desabilitar();
                    Swal.fire(
                      'Error!',
                      response.error,
                      'error',
                    )
                  }
                })
                .fail((error)=> {
                  desabilitar();
                  Swal.fire(
                    'Error!',
                    error?.responseJSON?.error || 'Error Fatal!',
                    'error',
                  )
                })
            }
        });
      }

    document.addEventListener('keyup',function(event){
      event.preventDefault();
      
      if(event.keyCode === 27 && window.caja) sendreporteZ('Z')
      else if(event.keyCode === 46 && window.caja)sendreporteZ('X')

    })
    function desabilitar(){
      let things = $('a.ruledis');
        $(things).hasClass("disabled") ? $(things).removeClass("disabled") : $(things).addClass("disabled");
    }

    function getStatus(){
      $(document).on('click','.getStatus',function(e){
        e.preventDefault()
        let url = $(this).attr('href')
        Swal.fire({
          title: 'Seguro que Desea obtener el status de la Impresor?',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si Obtener!'
      }).then((result) => {//dio click en el boton aceptar
          if(result.value){
            $.ajax({
              type: "POST",
              url: url,
              data: {
                _token:$('input[name=_token]').val(),
                fecha:$(this).data('fecha')
              },
              dataType: "dataType",
              success: function (response) {

                console.log(response);
                return
                
              },error:function(error1){
                let {error,msg} = JSON.parse(error1.responseText);
                
                //return;
                if(error === false  ){
                  Swal.fire(
                    msg,
                    'Con Exito.',
                    'success',
                    ) 
                  }else{
                    Swal.fire(
                      'Error!',
                        msg,
                      'error',
                      )
                  }
                
              } 
            });
          }
      })
      })
    }
    function checkCaja(){//Funcion que chequea al momento de recargar la pagina si la caja esta activa o no
      let url1 = $('body').data('url');
      $.get(url1,function (result) {
        if(result.status === 'TRUE' && result.cajas){
          $('h4.cajas').html('<b><span style="color:blue">CAJA ABIERTA </span></b><i class="fa fa-lock" id="abrirCaja"></i><i class="fa fa-lock-open" style="display: none" id="cerrarCaja">'+' '+result.nombreCaja+' '+'</i>')
          ccaja = result.nombreCaja;
          $('#cardFactura').removeClass('disabledbutton');//meter en la otra funcion de ajax
          $('#cardCliente').removeClass('disabledbutton');
          $('#opcionesF').removeClass('disabledbutton');
          cajaStatus = true;
          $('#abrirCaja').hide();
          $('#cerrarCaja').show();
          $('#modal-default').modal('hide');
          $('input.caja').val("");
          let selectCaja = document.getElementById('cajaSelected')
          $(selectCaja).html(result.cajas)

          
        }else if(result.cajas){
          desactivarCaja()
          let selectCaja = document.getElementById('cajaSelected')
          $(selectCaja).html(result.cajas)
        }else{
          desactivarCaja()
          alert("Este usuario no tiene cajas registradas")
        }
      })
    }

    function desactivarCaja(){
        $('#lockedCaja').modal('show')///Modal que indica que la caja esta bloqueada
          $('#cardFactura').addClass("disabledbutton");
          $('#cardCliente').addClass("disabledbutton");
          $('#opcionesF').addClass("disabledbutton");
          cajaStatus = false;
          $('#abrirCaja').show();
          $('#cerrarCaja').hide();
          $('h4.cajas').html('Caja Cerrada <i class="fa fa-lock" id="abrirCaja"></i><i class="fa fa-lock-open" style="display: none" id="cerrarCaja"></i>')
          $('input.caja').val("");
          document.getElementsByClassName('reporteZ').removeEventListener('keyup',null,true)
          document.getElementsByClassName('reporteX').removeEventListener('keyup',null,true)
    }
  })