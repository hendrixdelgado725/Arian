$(document).ready(function() {
	
    $('.anular').click(function(e){
            e.preventDefault();
              Swal.fire({
              title: '¿Seguro que desea anular esta factura',
              text: "¡No podra revertir esta acción!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, anular'
            }).then(async (result) => {
                if (result.value == true) {
                 await Swal.fire({
                    title: 'Especifique el Motivo de Anulacion de la Factura',
                    input: 'text',
                    inputAttributes: {
                      autocapitalize: 'off'
                    },
                    showCancelButton: true,
                    confirmButtonText: 'Anular',
                    showLoaderOnConfirm: true,
                    allowOutsideClick: () => !Swal.isLoading()
                  }).then(async (result) => {
                    
                    if(result.dismiss !== 'cancel'){
                      await Swal.fire({
                        title: 'Agregue El numero correlativo que arrojo la impresora fiscal, la factura anulada(EN CASO QUE LA FACTURA HAYA SIDO ANULADA AUTOMATICAMENTE)',
                        input: 'text',
                        inputAttributes: {
                          autocapitalize: 'off'
                        },
                        showCancelButton: true,
                        confirmButtonText: 'Anular',
                        showLoaderOnConfirm: true,
                        allowOutsideClick: () => !Swal.isLoading()
                      })

                      var row = $(this).parents('tr');
                      var td = $(this).parents('td');
                     /*var td2 = td.closest('td').find('td.status')*/
               
                      var td2 = td.prev();
                      var td3 = td2.prev();
                   /*      console.log(td2.text())*/
                      var html = 'ANULADA';
                      td3.text(html);
                      var url = $(this).attr('href');
                      
                      $(this).hide();
                      

                       
                      $.get(url,{
                        desc:result.value
                      },function(result1) {  
                        Swal.fire(
                          'HECHO',
                          'LA FACTURA HA SIDO ANULADA',
                          'warning',
  
                        );
                              
                       })  
                    }
                   
                   
                    })
                  
               
            
              
                }
            });
  
    });

  /*  $('.activar').click(function(e){
            e.preventDefault();
              Swal.fire({
              title: '¿Seguro que desea activar esta factura?',
              text: "¡No podra revertir esta acción!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, activar'
            }).then((result) => {
                if (result.value == true) {
                  
                var row = $(this).parents('tr');
                //var td = $(this).closest('data-id');
                var td = row.closest('td').find('td.acciones')
                var opcion = td.closest('a').find('a.anular')
                var url = $(this).attr('href');
                $.get(url,opcion,function(result1) {  
    
                        Swal.fire(
                          'HECHO',
                          'LA FACTURA HA SIDO ACTIVADA',
                          'warning',
  
                        );
  
                })   
                }else  console.log(result.dismiss);    //para cancelar el sweetalert2
            });
  
    });*/
  });