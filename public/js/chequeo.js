function pasarMayusculas(texto, elemento_id)
{
    document.getElementById(elemento_id).value=texto.toUpperCase();
}

// Solo permite valores
// numéricos y el punto(.) para el caso de que sea
// un valor decimal
function NumerosReales(evento)
{
 	var key;

 	if(window.event) // IE
 	{
	 	key = evento.keyCode;
	}
  	else if(evento.which) // Netscape/Firefox/Opera
 	{
  		key = evento.which;
 	}
 	if(key==13 || key==8 || key==46)/*para las teclas enter, borrar y punto*/
    {
        return true;
    }
 	else if (key < 48 || key > 57)/* para restringir las letras*/
    {
      	return false;
    }

 	return true;
}

function SoloNumeros(evento)
{
 var key;

 if(window.event) // IE
 {
  key = evento.keyCode;
 }
  else if(evento.which) // Netscape/Firefox/Opera
 {
  key = evento.which;
 }
 if(key==13 || key==8)/*para las teclas enter y borrar*/
    {
        return true;
    }
 else if (key < 48 || key > 57)/* para restringir las letras*/
    {
      return false;
    }

}

function NumerosDias(evento)
{
 	var key;

 	if(window.event) // IE
 	{
	 	key = evento.keyCode;
	}
  	else if(evento.which) // Netscape/Firefox/Opera
 	{
  		key = evento.which;
 	}
 	if(key==13 || key==8)/*para las teclas enter, borrar y punto*/
    {
        return true;
    }
 	else if (key < 48 || key > 57)/* para restringir las letras*/
    {
      	return false;
    }

 	return true;

}