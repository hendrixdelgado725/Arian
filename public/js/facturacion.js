$(document).ready(function () {
  var paso = null;           
  var inventario = [];
  var pagos = [];
  var articulosMode = true;
  var pagoMode = false;
  /* $('.cuadropagos').hide();*/
  var pagototal = false;
  var restante = 0;
  var pagado = 0;
  var ivac = 0;
  var ivaa = 0
  var subtotalc = 0;
  var totalc = 0;
  var descuentoc = 0;
  var codDescon = '';
  var manualMode = false;
  var desGeneral = 0;
  var formato = 'general'
  var descuentoArt = false 
  var cambio = 0;
  var cajaStatus = false;
  var ccaja = 0;
  var repetido = false;
  var presupuestoMode = false;
  var notaMode = false;
  var numPresu = '';
  var numNota = '';
  var monedaPrincipal = '';
  var monedaCam = '';
  var valorMoneCam = 0;
  var cambiasion = false;
  var pc = true
  var escriCant = false;
  var predivisa = ''
  var tasaCod = '' 
  var transporcost = 0;
  const recargo_json = $('#recargos_json').data('value')
  let seriales = [];
  var check = false;
  var $row3 = 0;    
  var id = 0;
  var repetirSerial = false;
  let inventarioObject = [];
  const facturacion_data = {
    recargo : null
  }
  let serialesEntradas = []
  let cantidad = 0;
  let inputee = 0;
  var index2 = 0;
  let codigoArt = '';
  
  checkRecargos()
  logToCaja()
  checkCaja()///Chequea si la caja esta abierta o cerrada                                    
  
  document.getElementById('formCliente').reset();
  document.getElementById('formDevolucion').reset();

  if(articulosMode == true){
   document.getElementById('listarticulos').style.display = 'none'

  }
  if(articulosMode == true && !inventario){
      document.getElementById('pago').style.display = 'block'
  }
  if(pagoMode == true){
   document.getElementById('pago').style.display = 'none'
   document.getElementById('añadir').style.display = 'none'
  }
  console.log(window.isFiscal)
  if(window.isFiscal === 'true'){
  
    function verificarConexion(){
      if(socket.connected){
        if(!temp){
          console.log(cajaStatus)
          if(cajaStatus){
                 activarCaja()
                 Swal.fire({
                   icon  :'success',
                   title : 'success!',
                   text  : "La conexion se ha restablecido",
                   toast : true
                 })
                 temp=true
                 temp2=false
                 
                  }
                }
                                               //cuando es local todo el sistema 
                                               //esta dentro de la caja                         //cuando està dentro de una red
                if(window.navigator.onLine || (socket.connected && (window.ip === '127.0.0.1' || window.ip !== '127.0.0.1'))) cajaStatus=true;   
              //  clearInterval(verificarConexion)
              }else{
               if(!temp2){
  
                 desactivarCajaConexion()
                 
           
                 Swal.fire({
                   icon  :'error',
                   title : 'Error!',
                   text  : "error hay interferencia en la red",
                   toast : true
                 })
                temp = false;
                temp2 = true;               
               }
              }
      
    }
  
  
  
    const socket = io(window.socket);  //conexion al servidor websocket que es la caja-api
    
    let temp = false
    let temp2 = false;
    
    setInterval( () => {
      
      verificarConexion()//verifica si la conexion esta abierta
  
    }, 2000);

  }
  

  
      
  

  const getBsPrice = function(objeto){// obtiene el precio si viene en una moneda distinta
  /*  console.log('objeto',objeto)*/
   /* let objmoneda = objeto.precio.moneda.nombre*/
   /* let tasa = JSON.parse($('#tasatojson').val()) ? JSON.parse($('#tasatojson').val()).valor : 0;
    let tasas = JSON.parse($('#todastasastojson').val())*/
    let valor = 0
   
 /*   console.log(tasa)*/
    
/*      console.log('valor',valor)*/
    let preunit;
    let objetoArt = objeto.precio

///*      console.log(objetoArt)*/
    if(
      objetoArt?.moneda?.nomenclatura === 'BsS' ||
      objetoArt?.moneda?.nomenclatura === 'BS' || 
      objetoArt?.moneda?.nomenclatura === 'Bs' ||
      objetoArt?.moneda?.nomenclatura === 'BS.D' ||
      !(objetoArt?.moneda)
      ){
        
    preunit = parseFloat(objetoArt.pvpart)

    }else{
/*       $.each(tasas, function (index, val) {
          if(val.moneda.nombre == objmoneda){
            valor = val.valor
          }        
        })*/
//          console.log(objeto)
        let tasa = objeto.tasa.valor
       
       preunit = parseFloat(tasa)*parseFloat(objetoArt.pvpart)
       preunit = Number(preunit)
       preunit = preunit.toFixed(2)
      
    }

    $('#preunit2').val(preunit)
    $('#precio2').val(preunit*1)
   
   preunit = Number(preunit)
    $('#preunit').val(preunit.toLocaleFixed(2))
    $('#cantidad').val(1)
    $('#precio').val((preunit*1).toLocaleFixed(2))


  }
  
  $(document).on('change', '#articulo', function (e) { 
      e.preventDefault();
      predivisa = ''
      let url = $(this).data('href')
        
      $.ajax({
        type: 'GET',
        url: url,
        data: {
            articulo: $('select[name=articulos]').val(),
          },
          success: function (result) {
            
              
              if(result.error) {
                Swal.fire({
                  icon  :'error',
                  title:'Error!',
                  text  : result.msg,
                  toast : true
                })
                return
              }

              if(result.msg !== undefined){
                if(result.msg !== ''){
                  Swal.fire({
                    icon  :'error',
                    title:'Error!',
                    text  : result.msg,
                    toast : true
                  })
                  return
                }
              }
              
              if(result.moneda === "no tiene moneda"){
                Swal.fire({
                  icon  :'error',
                  title:'Error!',
                  text  : result.moneda,
                  toast : true
                })
                return
              }

              result = JSON.stringify(result);
              result = JSON.parse(result);
            //  serialesEntradas = result.seriales;
            //  console.log(result.seriales)
              getBsPrice(result);
              let select = $('#tallas');
              let content = '<option selected disabled> TALLA</option>';
              /*divisa = result.precio.moneda.nomenclatura*/
              let nomenclatura = result?.precio?.moneda?.nomenclatura
              if ( nomenclatura === 'BS' || nomenclatura === 'BsS' || nomenclatura === 'Bs' || nomenclatura === 'BS.D' || !nomenclatura){
                  predivisa = ''
              }
              else predivisa = result.precio.pvpart+' '+result.precio.moneda.nomenclatura

              if(result.tallas){
                  $.each(result.tallas, function (index, val) {    
                    if(val!= null){
                        content = content.concat(
                        '<option value="' + val.codtallas + '" >' + val.tallas + '</option>'
                        );
                    }
                  })
                $(select).html(content);
                $('#tallas').removeAttr('disabled');
                $('#añadir').attr('disabled','disabled');
                $('#cantidad').removeAttr('disabled');
              }
              else{
                $('#tallas').html('')
                $('#tallas').html('<option selected disabled>TALLA</option>')
                $('#añadir').removeAttr('disabled');
                $('#cantidad').removeAttr('disabled');
              }
              $('#serialEquipo').removeAttr('readonly',false)

              const elem = document.getElementById("cantidad")
              elem.focus()
          }
      });  
  })

  $(document).on('input', '#cantidad', function (e) { 
        e.preventDefault();
        $(this).tooltip('hide')
        $(this).attr('data-original-title', " ");
        let select = $('#escalas')
        let content = '<option selected disabled> -ESCALA- </option>'
        $(select).html(content)
        ////escala json ira ac
        cantidad =$(this).val() ;
        let url = $(this).data('href')
        var cant = $('#cantidad').val();
        $.ajax({
        type: 'GET',
        url: url,
        data: {
            cantidad: $('input[name=cantidad]').val(),
            talla: $('select[name=tallas]').val(),
            articulo: $('select[name=articulos]').val(),
         /*   almacen: $('select[name=almacenes]').val(),*/
          },
          success: function (result) {
        /*  console.log(result)*/
      /*    console.log('hola')*/
          
          let exi = Number(result);
          let p = $('#cantidad');
          /*console.log(exi)*/
         /* console.log(exi)*/
          if(cant > exi){
            let cant2 = exi;
            $('#cantidad').val(cant2)
            //let precio = $('#preunit').val();
            let precio = $('#preunit2').val();
            let monto = cant2 * precio;

            $('#precio2').val(monto)
            $('#precio').val(monto.toLocaleFixed(2));
            

            $(p).attr('data-original-title','La existencia disponible de este articulo es '+cant2)
            $(p).tooltip('show')
  
           }
          else if(!cant) {
          cant = 1
          //let precio = $('#preunit').val();
          let precio = $('#preunit2').val()
          let monto = cant * precio;

          $('#precio').val(monto.toLocaleFixed(2));
          $('#precio2').val(monto)
          $(p).attr('data-original-title','Debe ingresar una cantidad')
          $(p).tooltip('show')
            }
          else if(cant == 0){
          $('#cantidad').val(1);
         // let pre = $('#preunit').val()
          let pre = $('#preunit2').val()
          $('#precio').val(pre*1)
          $('#añadir').removeAttr('disabled')
          }
          else{
          //let precio = $('#preunit').val();
          let precio = $('#preunit2').val()
          let monto = cant * precio;
          $('#precio').val(monto.toLocaleFixed(2));
          $('#precio2').val(monto)
          $(p).tooltip('hide')
          $(p).attr('title','')

            }

          }


        })

      /*let cantidad = $('#cantidad').val();*/
      

  })
  async function searchSerial() {


  await $.get('search',{serial:$('#serialEquipo').val()},
      function(res) {
        if(res.status === 500 ){
           /*Swal.fire({
                title: 'Este serial esta registrado '+res.serial,
                text:"El producto que es "+res.articuloFac.descrip+" y su Factura es: "+res.factura,
                icon: 'warning',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'OK'
              })*/
           return {
              serial: res.serial,
              descripcion: res.articuloFac.descrip,
              factura: res.factura,
              status:res.status
           };
         }
           /*Swal.fire( 
                     'HECHO',
                     '<strong>No hay Un serial facturado</strong>',
                     'success',
                     );*/
  return {success:'No hay Un serial facturado'}       
    })
  
  
}

  $(document).on('click', '#añadir', function (e) {//Añade el articulo previamente seleccionado
      e.preventDefault();
      let url = $(this).data('href')
        //$(this).data('serial')
        console.log(url);
      $.ajax({
        type: 'GET',
        url: url,
        data: {
            articulo: $('select[name=articulos]').val(),
            inventario: inventario,
            talla:$('#tallas').val()
          },
          success: function (result) {
              console.log(result)
              if(result.error) {
                Swal.fire({
                  icon  :'error',
                  title :'Error!',
                  text  : result.msg,
                  toast : true
                })
                return
              }
              result = JSON.stringify(result);
              result = JSON.parse(result);
              let articulo = result.desart;
              let codart = result.codart;/* CODIGO DEL ARTICULO A INGRESAR */
              let articulos = JSON.parse($('#arttojson').val());
              let select = $('#articulo');
              let input = $('#cantidad');
              //let input2 = $('#preunit');
              let input2 = $('#preunit2')
              //let input3 = $('#precio');
              let input3 = $('#precio2')
              let select2 = $('#tallas');
              let select3 = $('#almacenes');
              let select4 = $('#escalas')
              let content = '<option selected disabled> BUSCAR ARTICULO</option>';
              let content2 = '<input> </input>';
              let isseriales = result.isserial;
             
              repetido=false;
        
              inventario.forEach(function(objeto) {

                  if(objeto.codart == codart && objeto.talla == select2.val()){
                     objeto.cantidad = Number(objeto.cantidad) + Number(input.val())
                     objeto.costo = objeto.preunit * objeto.cantidad
                     objeto.costo2 = objeto.costo.toLocaleFixed(2)
                     repetido = true;
                   /* console.log(repetido,'repetido')*/
                  }
                })
                //calculo iva
              /*let iva = Number($('#iva').val());*/
              
              
              if(result.ivastatus === "true"){
                //pasa cuando el producto tiene iva
                let iva = ivac;
                let sumaIva = Number($('#monrgo').val())
                let calculo = Number(input3.val())*sumaIva;
                calculo = calculo /100;
                iva = Number(iva) + calculo;
                iva = parseFloat(iva)
                ivac = iva.toFixed(2)
                $('#iva').val(iva.toLocaleFixed(2));
                console.log(iva,sumaIva,calculo,ivac);

              }
              //calculo subtotal
              let subtotal = subtotalc;
              subtotal = subtotal + Number(input3.val());
              subtotalc = subtotal
              $('#subtotal').val(subtotal.toLocaleFixed(2));
              //if(result.ivastatus === null) ivac = 0;
              
              let total = totalc;
              total = subtotalc + Number(ivac);
              
              total = total - descuentoc;
              totalc = total.toFixed(2)
              $('#total').val(total.toLocaleFixed(2))
              $('#serialEquipo').attr('readonly',true);

              if(pagado > 0){
                restante = totalc - Number(pagado)
               $('#pagorestante').show();
               $('#pagorestante').removeAttr('disabled');
               $('#fpagos').removeAttr('disabled');
               $('#montopago').removeAttr('disabled');
               $('#bancos').removeAttr('disabled');
               $('#tipocredito').removeAttr('disabled');
               $('#añadirpago').removeAttr('disabled');
              }
              else{
               restante = totalc
              }
              restante = parseFloat(restante)
              restante = restante.toFixed(2)
              let tasa = JSON.parse($('#tasatojson').val());

              let restanteb = Number(restante)
              $('#bolivarrestan').val(restanteb.toLocaleFixed(2))
              $('#tasarestan').val((restanteb/tasa.valor).toLocaleFixed(2))
              let $a = inventario.length
              $a = $a + 1
             //console.log(repetido);
              if(!repetido){
              seriales = []
              
             
              var art = {
                id: $a,
                codart : select.val(),
                articulo : articulo,
                //talla: select2.val(),
                cantidad : input.val(),
                preunit : input2.val(),
                predivisa: predivisa,
                costo : input3.val(),
                preunit2 : $('#preunit').val(),
                costo2: $('#precio').val(),
                descuento: false,
                descontado: 0,
                mondescuento: 0,
                descontado2: 0,
                serial:[],
                isserial:isseriales,
                ivaStatus:result.ivastatus,
             /* almacen: select3.val(),*/
              }
             
          
              inventario.push(art);
              console.log(inventario)

              //return
              $('#inventario').val(inventario);

              }
              
              $.each(articulos, function (index, val) {
                content = content.concat(
                '<option value="' + val.codart + '" >' + val.codart+" / "+val.desart + '</option>'
               );
              })
                            
              //CENTRARME AQUI LINEA 376
              //console.log(select.val())
                

            if($a > 0){
                  inputee = $('#cantidad').val();
                  let rows2 = $('.cosas');
                  rows2.fadeOut()
                  let $u2 = 1
                  let index2 = 0;
                  let serialRepetidoArticulo = false;
                  
               
                  inventario.forEach(function(objeto) {
                   objeto.id = $u2
                   let predivisa = objeto.predivisa
                   //serialesEntradas.map((valor) => objeto.serial.push({"serial":valor,"facturar":false}))
                   
                   /* if(objeto.codart === select.val()){/*condicion para agregar seriales unicos 
                       let search = objeto.serial.find(elemen => elemen.serial === $('#serialEquipo').val());
                       if(search !== undefined){
                        
                        if(search.serial === ""){
                          
                           objeto.serial.push({"serial":$('#serialEquipo').val(),"facturar":check})
                         }else if(search.serial !== ""){
                          Swal.fire({
                            title: 'Este serial esta registrado '+search.serial,
                            text:"El producto que es "+objeto.codart,
                            icon: 'warning',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'OK'
                          })
                          inventarioObject.push(restarCantidad(inventario,objeto.codart,inputee,objeto.id))
                          repetirSerial = true;
                         }
                       }else{
                        
                        objeto.serial.push({"serial":$('#serialEquipo').val(),"facturar":check})
                       }
                       }else{
                        let search = objeto.serial.find(elemen => elemen.serial === $('#serialEquipo').val());
                          console.log(search);
                          if(search !== undefined){
                              if(search.serial !== ''){
                                Swal.fire({
                                  title: 'Este serial esta registrado '+search.serial,
                                  text:"El producto que es "+objeto.codart,
                                  icon: 'warning',
                                  showCancelButton: false,
                                  confirmButtonColor: '#3085d6',
                                  cancelButtonColor: '#d33',
                                  confirmButtonText: 'OK'
                                });
                                serialRepetidoArticulo = true;

                              }
                          }

                       }  */
                       


                    if(inventarioObject.length !== 0){
                      
                        inventarioObject.forEach(function(key){
                            /**/
                            
                            key.then(res => {
                            if(res.codart === select.val()){
                              objeto.cantidad = res.cantidad;
                              objeto.costo2 = res.costo2;
                              objeto.descontado2 = res.descontado2;

                            }
                            
                            })
                        })
                    }
                    
                   if(objeto.predivisa == '') predivisa = '<i class="fas fa-ban"></i>'
                   
                    console.log(objeto);
                    var serialE = (objeto.serial !== "") ? '<a id="visualizarSerial" data-ids="'+objeto.id+'" data-idd="'+objeto.id+'" data-codart="'+objeto.codart+'" href="#"><i class="fas fa-book"></i></a>' : '<a href="#" data-ids="'+objeto.id+'" data-idd="'+objeto.id+'" data-codart="'+objeto.codart+'" id="visualizarSerial" ><i class="fas fa-book"></i></a>'                                                                                       //agregue aqui   

                   if(objeto.descuento == true && objeto.talla != null){
                     var html = '<tr class="cosas" style="text-align:center"><td class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion">'+objeto.articulo+'</td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.costo2+'</td><td class="talla">'+objeto.talla+'</td><td class="cantidad">'+objeto.cantidad+'</td><td class="descuentoIcon"><i class="fas fa-check"></i></td><td class="descuentoCan">'+objeto.descontado2+'</td><td><a id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';
                     console.log("ento1") 
                  }
                  else if(objeto.descuento == true && objeto.talla == null){
                    console.log("ento2") 
                    var html = '<tr class="cosas" style="text-align:center"><td class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion">'+objeto.articulo+'</td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.costo2+'</td><td class="talla"><i class="fas fa-ban"></i></td><td class="cantidad">'+objeto.cantidad+'</td><td class="descuentoIcon"><i class="fas fa-check"></i></td><td class="descuentoCan">'+objeto.descontado2+'</td><td><a id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';

                  }
                  else if(objeto.descuento == false && objeto.talla != null){
                     var html = '<tr class="cosas" style="text-align:center"><td class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion">'+objeto.articulo+'</td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.costo2+'</td><td class="talla">'+objeto.talla+'</td><td class="cantidad">'+objeto.cantidad+'</td><td class="descuentoIcon"><i class="fas fa-ban"></i></td><td class="descuentoCan">'+objeto.descontado2+'</td><td><a id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';
                     console.log("ento3") 
                  }
                  else if(objeto.ivaStatus === "true"){
                    console.log("ento4") 
                    var html = '<tr class="cosas" id="cosass" data-id="'+objeto.id+'"  style="text-align:center"><td data-id="'+$row3+'" id="idarts"  class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion">'+objeto.articulo+'</td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.preunit+'</td><td class="talla">'+serialE+'</td><td class="cantidad">'+objeto.cantidad+'</td><td class="precio">'+objeto.costo2+'</td><td class="descuentoIcon">'+'<i class="fas fa-ban"></i>'+'</td><td class="descuentoCan">'+objeto.descontado2+'</td><td><i class="fa fa-ban"></i></td><td><a href="#" id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';
                 }
                  else{
                    console.log("ento5") 
                     var html = '<tr class="cosas" id="cosass" data-id="'+objeto.id+'"  style="text-align:center"><td data-id="'+$row3+'" id="idarts"  class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion">'+objeto.articulo+'</td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.preunit+'</td><td class="talla">'+serialE+'</td><td class="cantidad">'+objeto.cantidad+'</td><td class="precio">'+objeto.costo2+'</td><td class="descuentoIcon">'+'<i class="fas fa-ban"></i>'+'</td><td class="descuentoCan">'+objeto.descontado2+'</td><td><i class="fa fa-ban"></i></td><td><a href="#" id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';
                  }


                   

                  $('.arts').append(html);
                    $u2 = $u2 + 1
                    $row3++;
              
                     if(repetirSerial === true){
                        restar(select.val(),input3.val());
                        repetirSerial = false;
                     }      

                     if(serialRepetidoArticulo === true){
                       serialRepetidoArticulo = false;
                        restar(select.val(),input3.val());
                        inventario.splice($u2-1,1);
                        $('.cosass').fadeOut();
                      
                     }
                    
                         

                  })
                 }

            console.log(inventario)

            tasar()
           /*   $('.arts').append(html);*/

              let content3 = '<option selected disabled> TALLA</option>';
              $(select2).html(content3);
              $('#tallas').attr('disabled','disabled');
              let content4 = '<option selected disabled>ALMACENES</option>';
              $(select3).html(content4);
              $('#almacenes').attr('disabled','disabled');
              $('#cantidad').attr('disabled','disabled');
              let content5 = '<option selected disabled> -ESCALA- </option>'
              $(select4).html(content5)
              $('.escala').hide()


               $(select).html(content);
               $(input).html(content2);
               $(input2).html(content2);
               $(input3).html(content2);
               $(input).val('');
               $(input2).val('');
               $(input3).val('');
               $('#preunit').val('')
               $('#precio').val('')
               $('#serialEquipo').val('')
          


                $('#añadir').attr('disabled', 'disabled');
               document.getElementById('pago').style.display = 'block'
          } //success
      });  //ajax

//Limpia el seleccionador y campos de articulos
     
  })

function restar(codart,input3){
    var articulos = inventario;
    console.log(codart);
    articulos.forEach(function(objeto){

      if(objeto.codart === codart){
         let subtotal = subtotalc; 
//                    console.log(objeto)
                   subtotal = subtotal - Number(input3);
                   subtotalc = subtotal;
                   subtotal = subtotal.toLocaleFixed(2)
           
                   $('#subtotal').val(subtotal);
                    if(desGeneral == 0){
           
                    var descontadoT = descuentoc;
                    descontadoT = descontadoT - objeto.descontado;
                    descuentoc = descontadoT;
                    $('#descuento').val(descontadoT.toLocaleFixed(2))
                   }
                   else{
                     borrarTodito = true
                     desGenerarl = 0
                     descuentoc = 0
                     $('#descuento').val(descuentoc.toLocaleFixed(2))

                   }

      }
    });

    let st = subtotalc
                 st = st - descuentoc

                let sumaIva = Number($('#monrgo').val())
                let calculo = st*sumaIva;
                calculo = calculo /100;
             /*   console.log('calculo',calculo)*/
                iva = calculo;
                iva = parseFloat(iva)
                iva = iva.toFixed(2)
                ivac = Number(iva)
             /*   console.log('ivac',ivac)*/
              
              /*  console.log('iva2',iva)*/
                $('#iva').val(ivac.toLocaleFixed(2));

                 st = Number(st) + Number(iva)

                 totalc = st
                 let totaldes = parseFloat(totalc)

                $('#total').val(totaldes.toLocaleFixed(2))

              totalc = totalc.toFixed(2)
              /*restante = totalc
              restante = restante - pagado
              restante = parseFloat(restante);
              restante = restante.toFixed(2)


             let restanteb = Number(restante)
             $('#bolivarrestan').val(restanteb.toLocaleFixed(2))*/

             inventario = articulos;
             $('#inventario').val(articulos);


              if(articulos.length == 0){
                subtotalc = 0
                ivac = 0
                totalc = 0
                descuentoc = 0
                desGeneral = 0;
                ivaa = 0
                $('#total').val(totalc)
                $('#iva').val(ivac)
                $('#subtotal').val(subtotalc)
                $('#descuento').val(descuentoc)
             }
}

$(document).on('click','#cierreTT',function(e) {
  e.preventDefault()

  Swal.fire({
    title: '¿Deseas cerrar Turno?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si',
    cancelButtonText: 'No'
  }).then(async (result)=>{
    if(result.isConfirmed){

      $.ajax({
        method:'POST',
        url:'/modulo/Facturacion/cerrar',
        data:{
          "caja":$(this).data('turnos'),
          _token:$('input[name=_token]').val(),
        },success:function(result){
          console.log(result['error']);
          if(result['error'] === true){
            Swal.fire( 
              'Error',
              '<strong>'+result['msg']+'</strong>',
              'error',
              );
            
          }else{
            
            checkCaja()            
            desabilitar()

            Swal.fire( 
              'Hecho',
              '<strong>'+result['msg']+'</strong>',
              'success',
              );

          }
        }
      })

    }


  })


})


  $('#facturar').click(function (e) {
      e.preventDefault();
    /*  console.log(ccaja);*/

      Swal.fire({
          title: '¿Facturar?',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si',
          cancelButtonText: 'No'
        }).then(async (result)=>{
          if (result.value === true){
                     
            //$(this).hide()
            
           /* if(!checkValidationDescription(inventario)['status']){
                Swal.fire({
                  title: '¿Tiene que agregar la descripcion del articulo para poder facturar?',
                  text:`Producto que falta la descripcion ${checkValidationDescription(inventario)['codart']}`,
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  
                });    
                return ;
            }*/
       
            /*hacer un solicitud y consultar los producto a facturar si tiene tildad a seriales */
            
              var capture = null;

              
              capture = await cambiarPaso(inventario)
              
              paso = capture.filter((value) => (value === true));
              console.log(capture);
              if(paso.length !== 0){
                /*
                evalua si la cantidad de producto es menor a los seriales seleccionados
                muestra el error, tiene que ser iguales
              */
                let lengArraySerial2 = inventario.filter(({serial,cantidad,isserial}) => (serial.length !== +cantidad && isserial === true))
                console.log(lengArraySerial2);
                
                if(lengArraySerial2.length !== 0 ){
                
                Swal.fire({
                  icon  :'error',
                  title:'Error!',
                  text  : "Los seriales seleccionados es mayor o menor que la cantidad seleccionada verifique por favor los productos a facturar ",
                  toast : true
                });
    
                return;
              }
            }
            


          if(restante == 0 && pagado > 0){
            let nombre = $('#nombre').val();
            let documento = $('#documento').val();
              if(nombre && documento){
              let url = $(this).data('href')
              var articulos = inventario;
              var paguitos = pagos;
              var fatotal = totalc;
              var fasubtotal = subtotalc;
              var fadescuento = descuentoc;
              var fadescuentoG = desGeneral;
              var faiva = ivac; 
              var fatasa = cambio;
              var tipodocumento = $('#tipodocumento').val();
              var cdcaja = ccaja;
              var presuMode = presupuestoMode;
              var presuNum = numPresu;
              var notaEntregaMode = notaMode;
              var notaNum = numNota;
              var nombreCli = $('#nombre').val();

              /* $('#cardCliente').addClass("disabledbutton");
              $('#cardFactura').addClass("disabledbutton");
              $('#opcionesF').addClass('disabledbutton'); */
              cajaStatus = false;
              /*$.each(articulos, function (index, val) {
              console.log(val)
              })*/

              
              console.log(url);
                $.ajax({
                type: 'POST',
                url: url,
                data: {
                  cliente: $('input[name=documento]').val(),
                  ivatotal: $('input[name=iva]').val(),
                  subtotal: $('input[name=subtotal]').val(),
                  total: $('input[name=total]').val(),
                  descuento: $('input[name=descuento]').val(),
                  ivaporcentaje: (faiva === 0 || faiva === "0") ? 0 : $('input[name=monrgo]').val(),     
                  tasa: $('input[name=tasa]').val(),
                  fatotal,
                  fasubtotal,
                  fadescuento,
                  fadescuentoG,
                  faiva,
                  fatasa,
                  articulos:articulos,
                  pagos: paguitos,
                  tipodocumento,
                  cdcaja,
                  presuMode,
                  presuNum,
                  notaEntregaMode,
                  notaNum,
                  nombreCli,
                  _token:$('input[name=_token]').val(),
                  recargo_data : (faiva === 0 || faiva === "0") ? 0 : facturacion_data.recargo,
                  valorT:JSON.parse($('#tasatojson').val()),
                  tasaP:window.tasaP !== null ? window.tasaP : null 
                  },  
              success: async function (result) {
               /* console.log(result)*/
                   if(result.exito){
                    clear()    
                    Swal.fire( 
                     'HECHO',
                     '<strong>FACTURADO CON EXITO</strong>',
                     'success',
                     );
                     cajaStatus = true;
                     const [response, error] = await askForFactura(result.exito,result.codcaja)
                     $('#cardCliente').removeClass("disabledbutton");
                     $('#cardFactura').removeClass("disabledbutton");
                     $('#opcionesF').removeClass('disabledbutton');
                     $(this).show()
                     document.addEventListener("updateCorrelativo",actualizarCorrelativo)
                     
                     let event = new Event("updateCorrelativo",{
                       bubbles:true
                     })
    
                     document.dispatchEvent(event)
                     
                                 
                  }

                if(result.error){
                   Swal.fire( 
                    'ERROR',
                    '<strong>HA OCURRIDO UN ERROR</strong>',
                    'warning',
                   );
                   Swal.fire({
                    title: 'ERROR',
                    text: 'HA OCURRIDO UN ERROR, '+result.error,
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si',
                    cancelButtonText: 'No'
                    }).then((result1)=>{
                   if (result1.value === true){
                      $("#facturar").click();
                    }
                    else{
                     clear();
                     $(this).show()
                     $('#cardCliente').removeClass("disabledbutton");
                     $('#cardFactura').removeClass("disabledbutton");
                     $('#opcionesF').removeClass('disabledbutton');
                     cajaStatus = true;
                    }
                   })


                  }
               }//SUCCESS
             }) //ajax
             
            } //ifcliente
            else{
               Swal.fire( 
              'ERROR',
              '<strong>DEBE AGREGAR UN CLIENTE A FACTURAR</strong>',
              'warning',
                );
                $(this).show()
            }

          } //ifpagado
             else{
              Swal.fire( 
              'ERROR',
              '<strong>DEBE CANCELAR EL MONTO A PAGAR</strong>',
              'warning',
                );
                $(this).show()
          }
         } //if
        }) //then


  }); //facturar

   $(document).on('click', '#pago', function (e) { 
      e.preventDefault();
    /*  let tabla = $('#pagotabla')
      tabla.style.display = "block"*/

      document.getElementById('artitabla').style.display = 'none'
      document.getElementById('pagotabla').style.display = 'block'
      document.getElementById('listarticulos').style.display = 'block'
      document.getElementById('pago').style.display = 'none'
      document.getElementById('añadir').style.display = 'none'
      document.getElementById('añadirpago').style.display = 'block'
      $('.cuadroarticulos').hide();
      $('.cuadropagos').show();
      document.getElementById('fpagos').focus()

  })

   $(document).on('blur','#pago', function(e){
    e.preventDefault();
    document.getElementById('fpagos').focus()
   })

     $(document).on('keyup', '#montopago', function (e) { 
      e.preventDefault();
      validacion()

  })

     $(document).on('input', '#cantidad', function (e) { 
      e.preventDefault();
      /*console.log($('#cantidad').val())*/
      /*$('#añadir').attr('enabled', 'enabled');*/
      let cantidad = $('#cantidad').val();
      if(cantidad != 0){
          $('#añadir').removeAttr('disabled');
      }
      else{
          $('#añadir').attr('disabled', 'disabled');
      }
  })

      $(document).on('click', '#pagocompleto', function (e) { 
      e.preventDefault();
      if(valorMoneCam > 0){
      /*  console.log('moneda cambiaria')*/
        var cambiario = totalc / valorMoneCam
        if(monedaCam == 'PETRO'){
         cambiario = cambiario.toFixed(9)
        }
        else{
         cambiario = cambiario.toFixed(2)
        }
        cambiario = Number(cambiario)
        $('#montopago').val(cambiario)

      }
      else{
        /*  console.log('normalito')*/
        let tempT = totalc;
      

        if(+$('#descuento').val() !== 0) {//en caso que aplique descuento 
          
          tempT = parseFloat(tempT).toFixed(2).replace(',','.')
        }else{
          
          tempT = parseFloat(tempT).toFixed(2)
        }
        $('#montopago').val(tempT)
      }
       validacion()
      /*console.log("aja")*/
      /*let pagar = $('#total').val();*/

      pc = true;
      escriCant = false;
      

  })

      $(document).on('click', '#añadirpago', function (e) { 
        e.preventDefault();
      /*  console.log($('#pagos').val());*/
        let fpago = $('#fpagos').val();
        let monto = $('#montopago').val();
        let banco = $('#bancos').val();
        let tipocredito = $('#tipocredito').val()
        let refbillete = $('#refbillete').val()

           if(fpago != 'CREDITO'){
              tipocredito = null
              }
              
        if(!fpago || !monto){
          return ''
        }
              //desactivando papeleras articulos
        var papeleras = document.getElementsByClassName('deleterow');

        for (var i = 0; i < papeleras.length; i ++) {
          papeleras[i].style.display = 'none';
        }
       //desactivando papeleras articulos

        /*let u = pagos.length
        u = u +1*/
        let monedatabla = ''
        if(valorMoneCam > 0){
           monedatabla = monedaCam
        }
        else{
          monedatabla = 'BOLIVAR'
        }

        /*console.log('valorMoneda',valorMoneCam)*/
          let $u = pagos.length
          $u = $u +1


        if (tipocredito == null){
       /*     console.log("hi")*/
         var html = '<tr class="pagosrow" style="text-align:center"><td class"idpago">'+$u+'</td><td class="monedatabla">'+monedatabla+'</td><td class="fpagotd">'+fpago+'</td><td class="paguitom">'+monto+'</td><td>'+banco+'</td><td><i class="fas fa-ban"></i></td><td class="ref">'+refbillete+'</td><td><a id="deletepago" class="deletepago"><i class="fas fa-trash"></a></i></td></tr>';
         //var html = '<tr class="pagosrow" style="text-align:center"><td class"idpago">'+$u+'</td><td class="monedatabla">'+monedatabla+'</td><td class="fpagotd">'+fpago+'</td><td class="paguitom">'+monto+'</td><td>'+banco+'</td><td><i class="fas fa-ban"></i></td><td class="ref"><i class="fas fa-ban"></i></td><td><a id="deletepago" class="deletepago"><i class="fas fa-trash"></a></i></td></tr>';
        }
        else{
         //var html = '<tr class="pagosrow" style="text-align:center"><td class"idpago">'+$u+'</td><td class="monedatabla">'+monedatabla+'</td><td class="fpagotd">'+fpago+'</td><td class="paguitom">'+monto+'</td><td>'+banco+'</td><td>'+tipocredito+'</td><td class="ref">'+refbillete+'</td><td><a id="deletepago" class="deletepago"><i class="fas fa-trash"></a></i></td></tr>';
         var html = '<tr class="pagosrow" style="text-align:center"><td class"idpago">'+$u+'</td><td class="monedatabla">'+monedatabla+'</td><td class="fpagotd">'+fpago+'</td><td class="paguitom">'+monto+'</td><td>'+banco+'</td><td><i class="fas fa-ban"></i></td><td class="ref">'+refbillete+'</td><td><a id="deletepago" class="deletepago"><i class="fas fa-trash"></a></i></td></tr>';
        }
        $('.cosaspagos').append(html);

        let montoc = 0;
        let monto1 = 0
        let monto2 = 0

        if(valorMoneCam > 0){
         /* console.log('aqui')*/
          if(pc == true && escriCant == false){
     /*       console.log('pc true')*/
            monto1 = totalc / valorMoneCam
            monto2 = monto1 * valorMoneCam
          }
          else{
/*              console.log('pc false')*/
            if(pagado == 0 && escriCant == true || pagado > 0 && escriCant == true){
/*                console.log('escriCant',escriCant)*/
/*              console.log('pagado',pagado)
              console.log('pagado igual a cero')*/
              monto1 = monto * valorMoneCam
              monto2 = monto1
            }
            else if(pagado > 0 && escriCant == false){
           /*   console.log('ya se ha pagado')*/
/*                 console.log('escriCant',escriCant)*/
          /*    console.log('pagado',pagado)*/
             monto1 = restante / valorMoneCam
             monto2 = monto1 * valorMoneCam
            }

          }
          /*montoc = Number(monto) * valorMoneCam*/

          montoc = monto2
          if(monedatabla == 'PETRO'){
            monto1 = monto1.toFixed(9)
          }            
          else{
            montoc = montoc.toFixed(2)
          }
        /*  monto1 = monto1.toFixed(2)*/
          pagado = Number(pagado) + Number(montoc)
          console.log('monto1:',monto1)
          console.log('montoc:', montoc)
          console.log('monto2:', monto2)
          console.log('pagado:', pagado)
          pagado = parseFloat(pagado)
          pagado = pagado.toFixed(2)
    /*       console.log('pagado:', pagado)*/
         /* montoc = montoc.toFixed(2)*/
          cambiasion = true
        }
    
        else{
         console.log('aca')
         pagado = Number(pagado) + Number(monto);
         pagado = Number(pagado)
    /*     pagado = pagado.toFixed(2)*/
         /*console.log(pagado)*/
         cambiasion = false
        }

        /*  console.log('pagaso:',pagado)*/
         /* console.log(pagado) */
      
     /* total = $('#total').val();*/
       let total = totalc;
       
       
     // console.log('pagado:', pagado.toFixed(2).replace('.',','),',',total.toFixed(2).replace('.',','),',',restante)
       //en caso de pagar todo el monto
      if(pagado == total && restante == 0){
      /*  console.log('todido')*/
       $('#añadirpago').attr('disabled', 'disabled');
       $('#montopago').attr('disabled', 'disabled');
       $('#fpagos').attr('disabled', 'disabled');
       $('#bancos').attr('disabled','disabled');
       $('#tipocredito').attr('disabled','disabled');
       $('#tipocredito').removeAttr('selected');
       $('#tipocredito').defaultSelected;
       $('#pagocompleto').hide();
       $('#pagorestante').hide();
       pagototal = true;
       $('#añadirpago').blur()
      }
      //en caso de pagar todo el monto
      else{


        
        let tempP = pagado
        let tempT = total

        if(+$('#descuento').val() !== 0) {//en caso que aplique descuento 
          tempP = parseFloat(pagado).toFixed(2).replace('.',',') 
          tempT = parseFloat(total).toFixed(2).replace('.',',')
        }else{
          tempP = parseFloat(tempP).toFixed(2)
          tempT = parseFloat(tempT).toFixed(2)
        }
        

        if(tempP == tempT){
          /*    console.log('ya se pago todo')*/
              restante = 0
               $('#pagorestante').hide();
               $('#pagorestante').attr('disabled', 'disabled');
               $('#fpagos').attr('disabled', 'disabled');
               $('#montopago').attr('disabled', 'disabled');
               $('#bancos').attr('disabled','disabled');
               $('#tipocredito').attr('disabled','disabled');
               $('#añadirpago').blur()
               $('#añadirpago').attr('disabled','disabled');
              /* $('#montopago').hide();
               $('#fpagos').hide();*/

          }
          //en caso de pagar pero no cubrir todo
          else{
              restante = total - pagado;
              restante = restante.toFixed(2)
              console.log('aun falta:',restante)
               $('#pagorestante').show();
               $('#pagorestante').removeAttr('disabled');
               $('#añadirpago').attr('disabled', 'disabled');
               $('#tipocredito').removeAttr('selected');
               $('#tipocredito').defaultSelected;
          }//en caso de pagar pero no cubrir todo

           $('#pagocompleto').attr('disabled', 'disabled');
           $('#pagocompleto').hide();
         
      }


           let restanteb = Number(restante)
            $('#bolivarrestan').val(restanteb.toLocaleFixed(2))

      //creo el objeto de pago para el arreglo


       if(cambiasion == false){
         var pag = {
/*                id : u,*/
              id : $u,
              fpago : $('#fpagos').val(),
              monto : $('#montopago').val(),
              montoc : 0,
              montoc2: 0,
              moneda : 'BOLIVAR',
              tasaCod : '',
              banco : $('#bancos').val(),
              tipocredito : $('#tipocredito').val(),
              refbillete : refbillete,
              } 
       }
       else{
        var pag = {
/*                id : u,*/
              id : $u,
              fpago : $('#fpagos').val(),
              monto : montoc,
              montoc : $('#montopago').val(),
              montoc2: monto1,
              moneda : monedaCam,
              tasaCod : tasaCod,
              banco : $('#bancos').val(),
              tipocredito : $('#tipocredito').val(),
              refbillete: refbillete,
              }
       }
              

              pagos.push(pag);
             /* console.log(pag);*/
         /*     console.log(pagos)*/
              $('#pagos').val(pagos);
           /*   console.log($('#pagos').val())*/

           
           let tasa = JSON.parse($('#tasatojson').val());
           let valor = tasa.valor;
           console.log(valor);
           
           let restantevalor = Number(restante / valor)
           
           $('#tasarestan').val(restantevalor.toLocaleFixed(2))
           tasar()
          /*    console.log('restante:',restante)*/
     
     let formasdepago = JSON.parse($('#fpagotojson').val());
     let select = $('#fpagos');
     $('#montopago').val(0);
     let content = '<option selected disabled> FORMA DE PAGO</option>';
      $.each(formasdepago, function (index, val) {
        content = content.concat(
          '<option value="' + val.destippag + '" >' +val.destippag + '</option>'
           );
        })
     $(select).html(content);
     $('#pagocompleto').attr('disabled', 'disabled');

      let bancosjson = JSON.parse($('#bancostojson').val());
      let select2 = $('#bancos');
      let content2 = '<option selected disabled>SELECCIONE BANCO</option>';
      $.each(bancosjson, function (index, val) {
              content2 = content2.concat(
              '<option value="'+val.nomban+'">' + val.nomban+'</option>'
               );
              })
      $(select2).html(content2);
      $('#refbillete').val('');
      /*console.log(pagos)*/


  })


      $(document).on('change', '#fpagos', function (e) { 
      e.preventDefault();
      $('#pagocompleto').removeAttr('disabled');
      let pago = $('#fpagos').val();
      if(pago == "CREDITO"){
          document.getElementById('tipocredito').style.display = 'block'
      }
      else{
          document.getElementById('tipocredito').style.display = 'none'
      }
      $('#montopago').val(0);
      $('#añadirpago').attr('disabled', 'disabled');
      let bancos = JSON.parse($('#bancostojson').val());
      let select = $('#bancos');
      let content = '<option selected disabled>SELECCIONE BANCO</option>';
      if(pago == "EFECTIVO"){
         content = content.concat('<option value="NINGUNO">NINGUNO</option>');
      }
      else{
           $.each(bancos, function (index, val) {
              content = content.concat(
              '<option value="'+val.nomban+'">' + val.nomban+'</option>'
               );
              })
      }
     
      $(select).html(content);

  })

      $(document).on('click', '#listarticulos', function (e) { 
      e.preventDefault();
    /*  let tabla = $('#pagotabla')
      tabla.style.display = "block"*/

      document.getElementById('artitabla').style.display = 'block'
      document.getElementById('pagotabla').style.display = 'none'
      document.getElementById('listarticulos').style.display = 'none'
      document.getElementById('pago').style.display = 'block'
      document.getElementById('añadir').style.display = 'block'
      document.getElementById('añadirpago').style.display = 'none'
      $('.cuadroarticulos').show();
      $('.cuadropagos').hide();

  })

        $(document).on('click', '#pagorestante', function (e) { 
           pc = false
        e.preventDefault();
   /*     console.log(restante)*/
           if(valorMoneCam > 0){
       /*    console.log('moneda cambiaria')*/
           let cambiariorestante = restante/ valorMoneCam 
           if(monedaCam == 'PETRO'){
              cambiariorestante = cambiariorestante.toFixed(9)
           }
           else{
              cambiariorestante = parseFloat(cambiariorestante)
              cambiariorestante = cambiariorestante.toFixed(2)
           }

           $('#montopago').val(cambiariorestante)
          }

          else{
  /*        console.log('normalito 2')*/
          $('#montopago').val(restante)
          }
         /* console.log(restante)*/
         escriCant = false
         validacion()
  })


       /* $(document).on('click', '#siguiente', function (e) { 
        e.preventDefault();
        console.log("siguiente")
        document.getElementById('card-cliente').style.display = 'none'
        document.getElementById('card-facturacion').style.display = 'block'
        document.getElementById('siguiente').style.display = 'none'
        document.getElementById('atras').style.display = 'block'
  })*/

        function clear(){
         /* console.log("limpiar")*/
          inventario = [];
          pagos = [];
          articulosMode = true;
          pagoMode = false;
          pagototal = false;
          restante = 0;
          pagado = 0;
          ivac = 0;
          subtotalc = 0;
          totalc = 0;
          descuentoc = 0;
          codDescon = '';
          manualMode = false;
          desGeneral = 0;
          formato = 'general'
          descuentoArt = false
          cambio = 0 
          presupuestoMode = false
          numPresu = ''
          notaMode = false
          numNota = ''
          monedaCam = '';
          valorMoneCam = 0;
          cambiasion = false;
          pc = true
          escriCant = false;
          ivaa = 0
          predivisa = ''
          tasaCod = ''

          $('#labelMoneda').text('Moneda : BOLIVARES');

          document.getElementById("formFacturacion").reset();
          document.getElementById("formPersona").reset();
          document.getElementById('formDevolucion').reset();
          $('#devolucionM').modal('hide');

          var rows = $('.pagosrow');
          rows.fadeOut();     
          var rows2 = $('.cosas');
          rows2.fadeOut();
          let formasdepago = JSON.parse($('#fpagotojson').val());
          let content = '<option selected disabled> FORMA DE PAGO</option>';
          let selectpago = $('#fpagos');
          $.each(formasdepago, function (index, val) {
            content = content.concat(
             '<option value="' + val.destippag + '" >' +val.destippag + '</option>'
             );
              })
          $(selectpago).html(content);
          $('#fpagos').removeAttr('disabled');

/*            $('#bancos').reset();*/
          let bancos = JSON.parse($('#bancostojson').val());
          let content2 = '<option selected disabled>SELECCIONE BANCO</option>';
          let selectbancos = $('#bancos');
          $.each(bancos, function (index, val) {
            content2 = content2.concat(
            '<option value="'+val.nomban+'">' + val.nomban+'</option>'
             );
              })
          $(selectbancos).html(content2);
          $('#bancos').removeAttr('disabled');

          $('#montopago').val(0);
          $('#montopago').removeAttr('disabled');
          $('#tipocredito').removeAttr('selected');
          $('#tipocredito').defaultSelected;
          $('#tipocredito').removeAttr('disabled');
          $('#tipocredito').hide();
          $('#refbillete').val('')

          let articulos = JSON.parse($('#arttojson').val());
          let selectart = $('#articulo');
          let content3 = '<option selected disabled> BUSCAR ARTICULO</option>';
          $.each(articulos, function (index, val) {
                content3 = content3.concat(
                '<option value="' + val.codart + '" >' + val.codart+" / "+val.desart + '</option>'
               );
              })
          $(selectart).html(content3);

          let tallas = $('#tallas');
          let content4 = '<option selected disabled> TALLA</option>';
          let selectEs = $('#escalas')
          let content5 = '<option selected disabled> -ESCALA- </option>'
          $(selectEs).html(content5)
          $('.escala').hide()

          $('#articulo').removeAttr('disabled');
          $('#cantidad').val(0);
          $('#cantidad').attr('disabled','disabled');
        /*  $('#cantidad').removeAttr('disabled');*/
          $('#preunit').val(0);
          $('#preunit').removeAttr('disabled');
          $('#precio').val(0);
          $('#precio').removeAttr('disabled');
          $('#almacenes').attr('disabled','disabled');
          $('#preunit2').val(0)
          $('#precio2').val(0)
          $('#cantcambiar').val(0)
          $('#montocambiar').val(0)
        
          $(tallas).html(content4);
          $('#tallas').attr('disabled','disabled');
          document.getElementById('pago').style.display = 'none'
          document.getElementById('listarticulos').style.display = 'none'
          document.getElementById('añadir').style.display = 'block'
          $('#añadir').attr('disabled','disabled');
          document.getElementById('añadirpago').style.display = 'none'
          document.getElementById('artitabla').style.display = 'block';
          document.getElementById('pagotabla').style.display = 'none';
          document.getElementById('listarticulos').style.display = 'none';
          $('#pagocompleto').attr('disabled','disabled')
          $('#pagocompleto').show();
          $('.cuadroarticulos').show();
          $('.cuadropagos').hide();
          /*$('.tasa').hide();*/


/*
            console.log(inventario);
            console.log(pagos);
            console.log(restante);
            console.log(pagado);*/
          
         }

        $(document).on('click', '#limpiar', function (e) { 
        e.preventDefault();
          Swal.fire({
             title: '¿DESEA LIMPIAR LA PANTALLA?',
             icon: 'warning',
             showCancelButton: true,
             confirmButtonColor: '#3085d6',
             cancelButtonColor: '#d33',
             confirmButtonText: 'Si',
             cancelButtonText: 'No'
            }).then((result)=>{
              if (result.value === true){
                 clear();
                } 

        })

  })

    /*    $(document).on('click', '#atras', function (e) { 
        e.preventDefault();
        console.log("Atras")
        document.getElementById('card-cliente').style.display = 'block'
        document.getElementById('card-facturacion').style.display = 'none'
        document.getElementById('siguiente').style.display = 'block'
        document.getElementById('atras').style.display = 'none'
  })*/

         document.addEventListener("keyup", function(event) {
         event.preventDefault();
        /* if (event.keyCode === 112) {
            $("#siguiente").click();
            }

          if(event.keyCode === 113){
            $("#atras").click()
          }*/
          if(cajaStatus == true){
             if(event.keyCode === 112){
              $("#descuentob").click();
            }

          if(event.keyCode === 113){
              $("#facturar").click();
            }

           if(event.keyCode === 115){
              $("#presupuesto").click();
            }

          if(event.keyCode === 119){
              $("#devolucion").click();
            }

            if(event.keyCode === 120){
              $('#limpiar').click()
            }
          
          }
         });

        // async function getAdminPass(){
        //   var promise =  await 
        // }

       $(document).on('click', '#deleterow', function (e) { 
        e.preventDefault();
      /*  console.log("borrao")*/
     /* <td class="predivisa">'+predivisa+'</td>*/

        var row = $(this).parents('tr');
        var td = row.closest('td').find('td.codigo')
        let codigo = "";
        let tdcod = $(this).closest('tr').find('td.codigo')
        codigo = $(this).closest('tr').find('td.codigo').text();
        var idar = tdcod.prev().text()
      /*  console.log('id',idar)*/
        var descripcion = $(this).closest('tr').find('td.descripcion').text();
        var cantidad = $(this).closest('tr').find('td.cantidad').text();
        var precio = $(this).closest('tr').find('td.precio').text();
        var descontadoM = $(this).closest('tr').find('td.descuentoCan').text();
        
        //replace(',','').replace(/\./g,'');
        /*console.log(codigo)
        console.log(descripcion)
        console.log(cantidad)
        console.log(preunit)*/
       /* var codigo = td.text();
        var codigo2 = td.innerText;
        var codigo3 = td.innerHtml;
        var codigo4 = td.textContent;*/

             //calculo iva inverso
            /*  let iva = Number($('#iva').val());*/
   /*           let iva = Math.round(ivac);
              let sumaIva = Number($('#monrgo').val())
              let calculo = precio*sumaIva;
              calculo = calculo /100;
              calculo = Number(calculo).toFixed(2)*/
          /*    console.log(iva)
              console.log(calculo)*/
/*                iva = iva - calculo;
              ivac = iva;
              $('#iva').val(iva.toLocaleFixed(2));*/
/*
              let iva = ivac;
              let sumaIva = Number($('#monrgo').val())
              let calculo = Number(input3.val())*sumaIva;
              calculo = calculo /100;
              iva = iva + calculo;
              ivac = iva;
              $('#iva').val(iva.toLocaleFixed(2));*/
             /* console.log(calculo)*/
            /*  console.log(calculo)
              console.log($('#iva').val())*/

              //calculo subtotal
             /* let subtotal = Number($('#subtotal').val());*/
         /*     let subtotal = subtotalc; 
              subtotal = subtotal - Number(precio);
              subtotalc = subtotal;
              subtotal = subtotal.toLocaleFixed(2)
              console.log(subtotal)
              $('#subtotal').val(subtotal);*/

              var borrarTodito = false

              ///Calculo descuento
            /*  if(desGeneral == 0){*/
               /* console.log()*/
          /*     var descontadoT = descuentoc;
               descontadoT = descontadoT - descontadoM;
               descuentoc = descontadoT;
               $('#descuento').val(descontadoT.toLocaleFixed(2))
              }
              else{
                borrarTodito = true
                desGenerarl = 0
                descuentoc = 0
                $('#descuento').val(descuentoc.toLocaleFixed(2))

              }*/
       /*       console.log('descuento',descuentoc)
              let total = subtotalc;
              total = total - descuentoc
              console.log('total',total)
              
              console.log(ivac)
              let iva = Math.round(ivac);
              console.log(ivac)
              let sumaIva = Number($('#monrgo').val())
              let calculo = total*sumaIva;
              calculo = calculo /100;
              calculo = Number(calculo).toFixed(2)
              total = Number(total) + Number(calculo)
              console.log('calculo',calculo)
              iva = calculo;
              iva = parseFloat(iva)
              console.log('iva',iva)
              ivaa = iva*/
              /*ivaa = ivaa - calculo*/
              //hay que arreglar este ivaaaa
           /*   ivac = iva;
              console.log('ivac:',ivac)
              $('#iva').val(iva.toLocaleFixed(2));*/
              

              //calculo total
       /*       let total = Number($('#total').val());*/


       /*       total = total + ivac;*/
             /* total = total - descuentoc;*/
           /*   totalc = parseFloat(total);
              totalc = totalc
              restante = totalc
              console.log(restante)
              $('#total').val(totalc.toLocaleFixed(2))*/

              var articulos = inventario;
              let isIva = null;
              articulos.forEach(function(objeto) {
                /* if(objeto.codart == codigo)*/
                isIva = objeto.ivaStatus;
                  if(objeto.id == idar){
               /*      console.log('id',idar)*/

                   let subtotal = subtotalc; 
                  /* console.log('m',objeto.costo)*/
                   subtotal = subtotal - Number(objeto.costo);
                   subtotalc = subtotal;
                   subtotal = subtotal.toLocaleFixed(2)
                  /*console.log(subtotal)*/
                   $('#subtotal').val(subtotal);
                    if(desGeneral == 0){
                    /* console.log()*/
                    var descontadoT = descuentoc;
                    descontadoT = descontadoT - objeto.descontado;
                    descuentoc = descontadoT;
                    $('#descuento').val(descontadoT.toLocaleFixed(2))
                   }
                   else{
                     borrarTodito = true
                     desGenerarl = 0
                     descuentoc = 0
                     $('#descuento').val(descuentoc.toLocaleFixed(2))

                   }
                  /* console.log('descuento',descuentoc)*/
                  /* let total = subtotalc;
                   total = total - descuentoc*/
                  /* console.log('total',total)*/
              
                   /*console.log(ivac)*/
                 /*  let iva = Math.round(ivac);*/
                  /* console.log(ivac)*/
                  /* let sumaIva = Number($('#monrgo').val())
                   let calculo = total*sumaIva;
                   calculo = calculo /100;
                   calculo = Number(calculo).toFixed(2)
                   total = Number(total) + Number(calculo)*/
                  /* console.log('calculo',calculo)*/
                /*   iva = calculo;
                   iva = parseFloat(iva)*/
                  /* console.log('iva',iva)*/
                  /* ivaa = iva
                    ivac = iva;*/
                  /* console.log('ivac:',ivac)*/
                 /*  $('#iva').val(iva.toLocaleFixed(2));
                   totalc = parseFloat(total);
                   totalc = totalc
                   restante = totalc*/
                 /*  console.log(restante)*/
                 /*  $('#total').val(totalc.toLocaleFixed(2))*/


                  index = articulos.map(function(e) { return e.id; }).indexOf(idar);
                  index2 = articulos.findIndex(val => val.id == idar);

                  /* console.log(articulos[index2])*/

                  articulos.splice(index2, 1)

           
                }
             });

                 let st = subtotalc
                 st = st - descuentoc
                  console.log()
                 if(isIva === "true"){ //con iva
                   let sumaIva = Number($('#monrgo').val())
                   let calculo = st*sumaIva;
                   calculo = calculo /100;
                   iva = calculo;
                   iva = parseFloat(iva)
                   iva = iva.toFixed(2)
                   ivac = Number(iva)
                /*   console.log('ivac',ivac)*/
                 
                 /*  console.log('iva2',iva)*/
                   $('#iva').val(ivac.toLocaleFixed(2));

                   st = Number(st) + Number(iva)
                 }else{//sin iva
                  st = Number(st)
                  iva = 0;
                 }
             /*   console.log('calculo',calculo)*/


                 totalc = st
                 let totaldes = parseFloat(totalc)

                $('#total').val(totaldes.toLocaleFixed(2))

              totalc = totalc.toFixed(2)
              restante = totalc
              restante = restante - pagado
              restante = parseFloat(restante);
              restante = restante.toFixed(2)


             let restanteb = Number(restante)
             $('#bolivarrestan').val(restanteb.toLocaleFixed(2))

             inventario = articulos;
             $('#inventario').val(articulos);


              if(articulos.length == 0){
                subtotalc = 0
                ivac = 0
                totalc = 0
                descuentoc = 0
                desGeneral = 0;
                ivaa = 0
                $('#total').val(totalc)
                $('#iva').val(ivac)
                $('#subtotal').val(subtotalc)
                $('#descuento').val(descuentoc)
             }

                 

                  
                  if(articulos.length > 0){
                  let rows2 = $('.cosas');
                  rows2.fadeOut()
                  let $u2 = 1

                  articulos.forEach(function(objeto) {
                   objeto.id = $u2
                   let predivisa = objeto.predivisa
                   if(objeto.predivisa == '') predivisa = '<i class="fas fa-ban"></i>'
                 
                   if(objeto.descuento == true && objeto.talla != null){
                     console.log("ento1")
                     var html = '<tr class="cosas" style="text-align:center"><td class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion">'+objeto.articulo+'</td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.costo2+'</td><td class="talla">'+objeto.talla+'</td><td class="cantidad">'+objeto.cantidad+'</td><td class="descuentoIcon"><i class="fas fa-check"></i></td><td class="descuentoCan">'+objeto.descontado2+'</td><td><a id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';

                  }
                  else if(objeto.descuento == true && objeto.talla == null){
                     console.log("ento2")
                     var html = '<tr class="cosas" style="text-align:center"><td class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion">'+objeto.articulo+'</td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.costo2+'</td><td class="talla"><i class="fas fa-ban"></i></td><td class="cantidad">'+objeto.cantidad+'</td><td class="descuentoIcon"><i class="fas fa-check"></i></td><td class="descuentoCan">'+objeto.descontado2+'</td><td><a id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';

                  }
                  else if(objeto.descuento == false && objeto.talla != null){
                    console.log("ento3")
                     var html = '<tr class="cosas" style="text-align:center"><td class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion">'+objeto.articulo+'</td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.costo2+'</td><td class="talla">'+objeto.talla+'</td><td class="cantidad">'+objeto.cantidad+'</td><td class="descuentoIcon"><i class="fas fa-ban"></i></td><td class="descuentoCan">'+objeto.descontado2+'</td><td><a id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';
                  }
                  else{
                    console.log("ento3")
                     //var html = '<tr class="cosas" style="text-align:center"><td class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion">'+objeto.articulo+'</td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.costo2+'</td><td class="talla"><i class="fas fa-ban"></i></td><td class="cantidad">'+objeto.cantidad+'</td><td class="descuentoIcon"><i class="fas fa-ban"></i></td><td class="descuentoCan">'+objeto.descontado2+'</td><td><a href="#" id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';
                     var serialE = (objeto.serial !== "") ? '<a id="visualizarSerial"  data-ids="'+objeto.id+'" data-idd="'+objeto.id+'" data-codart="'+objeto.codart+'" href="#"><i class="fas fa-book"></i></a>' : '<a href="#" data-ids="'+objeto.id+'" data-idd="'+objeto.id+'" data-codart="'+objeto.codart+'" id="visualizarSerial"  ><i class="fas fa-book"></i></a>'
                     var html = '<tr class="cosas" id="cosass" data-id="'+objeto.id+'"  style="text-align:center"><td data-id="'+$row3+'" id="idarts"  class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion"><a data-id="'+objeto.id+'" id="añadirDescripcion" href="#" data-articulo="'+objeto.articulo+'">'+objeto.articulo+'</a></td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.costo2+'</td><td class="talla">'+serialE+'</td><td class="cantidad">'+objeto.cantidad+'</td><td class="descuentoIcon">'+'<i class="fas fa-ban"></i>'+'</td><td class="descuentoCan">'+objeto.descontado2+'</td><td><a href="#" id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';
                  }
                  $('.arts').append(html);
                    $u2 = $u2 + 1
                  })
                 }


             if(borrarTodito == true){
              let rows2 = $('.cosas');
              rows2.fadeOut()
                subtotalc = 0
                ivac = 0
                totalc = 0
                descuentoc = 0
                desGeneral = 0;
                ivaa = 0
                $('#total').val(totalc)
                $('#iva').val(ivac)
                $('#subtotal').val(subtotalc)
                $('#descuento').val(descuentoc)
             }
             else{
              row.fadeOut()
             }
             borrarTodito = false

              tasar()


        row.fadeOut();
  })

      $(document).on('change', '#tipocredito', function (e) { 
      e.preventDefault();
      validacion();

  })

      $(document).on('change', '#bancos', function (e) { 
      e.preventDefault();
      validacion();

  })

       $(document).on('click', '#tasar', function (e) { 
        e.preventDefault();
        if(inventario.length == 0){
               Swal.fire( 
                'ERROR',
                '<strong>NO HA REGISTRADO ARTICULOS CUYOS MONTOS PUEDAN TASARSE</strong>',
                'warning',
                 );
        }
        else{
          let tasa = JSON.parse($('#tasatojson').val());
      /*    console.log(tasa);*/
          /* let total = Number($('#total').val())*/
          let total = totalc
          let valor = tasa.valor;
          /*console.log(valor)*/
          cambio = total / valor;
         /*  cambio.toFixed(2);*/
          $('#tasa').val(cambio.toLocaleFixed(2))
          $('.tasa').show();
        }


  })

       function validacion(){
       let valor = $('#montopago').val();
       let pago = $('#fpagos').val()
       let banco = $('#bancos').val()
       let tipocredito = $('#tipocredito').val()

        if(valor != 0 && banco != null){
          if(pago != "CREDITO" && pago != null){
            $('#añadirpago').removeAttr('disabled');
          }
          else{
              if (tipocredito == null) {
                  $('#añadirpago').attr('disabled', 'disabled');
              }
              else{
                   $('#añadirpago').removeAttr('disabled');
              }

          }

      }
      else{
          $('#añadirpago').attr('disabled', 'disabled');
      }
       }

       Number.prototype.toLocaleFixed = function(n) {
         return this.toLocaleString(undefined, {
         minimumFractionDigits: n,
         maximumFractionDigits: n
           });
        };

        $(document).on('change', '#descuentoR', function (e) { 
        e.preventDefault();
       /* console.log($(this).val())*/
        formato = $(this).val();
       /* console.log(inventario)*/

            if($(this).val()=='articulo'){
            let rows = $('.trDescuento');
            rows.fadeOut();

            document.getElementById('descuentosOpciones').style.display = 'none'


            /* var articulos = inventario;*/
                setTimeout(function(){
                
                inventario.forEach(function(objeto) {
          /*      console.log(objeto);
                console.log("hie")*/

                if(objeto.descuento == false){
                     let html = '<tr class="trDescuento" style="text-align:center"><td class="codigoD">'+objeto.codart+'</td><td class="descripcionD">'+objeto.articulo+'</td><td><a class="añadirD" id="añadirD"><i class="fas fa-plus"></i></a></td></tr>';
                     $('.listInventario').append(html);
                } 
                });

               }, 200);

              document.getElementById('tablaInventario').style.display = 'block'
               }
            else{
               document.getElementById('descuentosOpciones').style.display = 'block'
               document.getElementById('tablaInventario').style.display = 'none'
          }
  })


        $(document).on('click','#descuentob', function(e){
          e.preventDefault();
          if(pagado==0){
            inventario.forEach(function(objeto) {
               if(objeto.descuento == true){
                descuentoArt = true;
               } 
                });

             if(inventario.length == 0){
                Swal.fire( 
                'ERROR',
                '<strong>NO HAY ARTICULOS AGREGADOS PARA LA COMPRA</strong>',
                'warning',
                 );
                }


             else if(desGeneral>0){
                Swal.fire( 
                'ERROR',
                '<strong>YA SE HA REALIZADO UN DESCUENTO GENERAL</strong>',
                'warning',
                  );
                }

              else if(descuentoArt == true && presupuestoMode== true){
                 Swal.fire( 
                'ERROR',
                '<strong>NO PUEDE APLICAR MAS DESCUENTO A UN PRESUPUESTO</strong>',
                'warning',
                  );
                
              }

              else if(descuentoArt == true && desGeneral==0){
                formato = 'articulo';
                let rows = $('.trDescuento');
                rows.fadeOut();

                document.getElementById('descuentosOpciones').style.display = 'none'
                setTimeout(function(){
                
                inventario.forEach(function(objeto) {
               /* console.log(objeto);*/

                if(objeto.descuento == false){
                     let html = '<tr class="trDescuento" style="text-align:center"><td class="codigoD">'+objeto.codart+'</td><td class="descripcionD">'+objeto.articulo+'</td><td><a class="añadirD" id="añadirD"><i class="fas fa-plus"></i></a></td></tr>';
                     $('.listInventario').append(html);
                  } 
                });

               }, 200);

              document.getElementById('tablaInventario').style.display = 'block'
              $('#descuentoR').attr('disabled','disabled')
              $('#descuentoInput').attr('disabled','disabled');
              $('#descuentosSelect').attr('disabled','disabled');
              $('#descuentoM').modal('show');
          }

             else{
              manualMode = false;
             document.getElementById("formDescuento").reset();
             document.getElementById('tablaInventario').style.display = 'none'
             document.getElementById('descuentosOpciones').style.display = 'block'
             $('#descuentoInput').attr('disabled','disabled');
             $('#descuentosSelect').attr('disabled','disabled');
             let rows = $('.trDescuento');
             rows.fadeOut();
             $('#descuentoR').removeAttr('disabled')
            $('#descuentoM').modal('show');
           }
          }
          
          else{
            Swal.fire( 
                'ERROR',
                '<strong>NO PUEDE CARGAR DESCUENTO PORQUE YA SE HAN REGISTRADO PAGOS</strong>',
                'warning',
                 );
          }

        })


         $(document).on('click', '#añadirD', function (e) { 
          e.preventDefault();
          var row = $(this).parents('tr');
          var td = row.closest('td').find('td.codigoD')
          codDescon = $(this).closest('tr').find('td.codigoD').text();
      /*    var descripcion = $(this).closest('tr').find('td.descripcion').text();
          var cantidad = $(this).closest('tr').find('td.cantidad').text();
          var precio = $(this).closest('tr').find('td.precio').text(); */
          let rows = $('.trDescuento');
          rows.fadeOut();
          document.getElementById('descuentosOpciones').style.display = 'block'

          inventario.forEach(function(objeto) {
            /*    console.log(objeto);
                console.log("elegido!")*/

                if(objeto.codart == codDescon){
                     let html = '<tr class="trDescuento" style="text-align:center"><td class="codigoD">'+objeto.codart+'</td><td class="descripcionD">'+objeto.articulo+'</td></tr>';
                     $('.listInventario').append(html);
                }
                  
                
             });             
      })

         $(document).on('click','#guardarDescuento', function(e){
          e.preventDefault();
          let iva = 0
          let ivaS = false;
          
          
             let descuentoInput = $('#descuentoInput').val();
             let descuentoSelect = $('#descuentosSelect').val();

             if(manualMode == false){
               
              var descuento = descuentoSelect;
             }
             else{
              var descuento = descuentoInput;
             }

             /*var formato = $('#descuentoR').val();*/
             console.log(formato);
             if(formato == 'articulo'){
            /*  ivac = 0*/
          /*  console.log('ahi')*/

           /*   console.log("uno")
              console.log(codDescon)*/

              var articulos = inventario;
               articulos.forEach(function(objeto) {
                
                if(objeto.codart == codDescon){

                     let precio = objeto.costo;
                     var descontar = precio * Number(descuento);
                     descontar = descontar/100;
                     descontar = parseFloat(descontar)
                     descontar = descontar.toFixed(2)
                   /*  console.log(descontar)*/
                     descuentoc = Number(descuentoc) + Number(descontar)
                    /* console.log(descontar)*/
                  /*   $('#descuento').val(descuentoc.toLocaleFixed(2))*/


                    /* totalc = totalc - descontar;*/
/*
                     var totaldes = totalc; 
                     totaldes = totaldes.toLocaleFixed(2)
                     $('#total').val(totaldes)*/

                     objeto.descuento = true;
                     objeto.descontado = descontar;
                     objeto.mondescuento = descuento;
                     descontar = Number(descontar)
                     objeto.descontado2 = descontar.toLocaleFixed(2)

                     precio = precio - descontar
                    if(objeto.ivaStatus === "true"){
                      
                        ivaS = true
                      let sumaIva = Number($('#monrgo').val())
                      let calculo = precio*sumaIva;
                      calculo = calculo /100;
                      iva = Number(iva) + calculo;
                     /* console.log('iva',iva)*/
                      iva = parseFloat(iva)
                      ivaa = ivaa + iva
                      iva = ivaa
                      ivac = Number(ivac) + iva.toFixed(2)
                    ivac = iva.toFixed(2)
                 /*$('#descuento').val(descuentoc.toLocaleFixed(2))
                 var totaldesc =  (subtotalc - descuentoc)
                 $('#subtotal1').val(totaldesc)
                 iva = (totaldesc /100) + (subtotalc - descuentoc)*/
                      console.log('iva2',ivac)
                    }


                      inventario = articulos;
                      $('#inventario').val(articulos);
                }

                let rows = $('.cosas');
                rows.fadeOut();

                articulos.forEach(function(objeto) {
                  
                   if(!objeto.predivisa)predivisa ='<i class="fas fa-ban"></i>'
                   else predivisa = objeto.predivisa
        
                  /* else if(objeto.ivaStatus === "true"){
                    var serialE = (objeto.serial !== "") ? '<a id="visualizarSerial" data-ids="'+objeto.id+'" data-idd="'+objeto.id+'" data-codart="'+objeto.codart+'" href="#"><i class="fas fa-book"></i></a>' : '<a href="#" data-ids="'+objeto.id+'" data-idd="'+objeto.id+'" data-codart="'+objeto.codart+'" id="visualizarSerial" ><i class="fas fa-book"></i></a>'
                    var html = '<tr class="cosas" id="cosass" data-id="'+objeto.id+'"  style="text-align:center"><td data-id="'+$row3+'" id="idarts"  class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion">'+objeto.articulo+'</td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.costo2+'</td><td class="talla">'+serialE+'</td><td class="cantidad">'+objeto.cantidad+'</td><td class="descuentoIcon">'+'<i class="fas fa-ban"></i>'+'</td><td class="descuentoCan">'+objeto.descontado2+'</td><td><i class="fa fa-check"></i></td><td><a href="#" id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';
                 }
                  else{
                     var serialE = (objeto.serial !== "") ? '<a id="visualizarSerial" data-ids="'+objeto.id+'" data-idd="'+objeto.id+'" data-codart="'+objeto.codart+'" href="#"><i class="fas fa-book"></i></a>' : '<a href="#" data-ids="'+objeto.id+'" data-idd="'+objeto.id+'" data-codart="'+objeto.codart+'" id="visualizarSerial" ><i class="fas fa-book"></i></a>'
                     var html = '<tr class="cosas" id="cosass" data-id="'+objeto.id+'"  style="text-align:center"><td data-id="'+$row3+'" id="idarts"  class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion">'+objeto.articulo+'</td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.costo2+'</td><td class="talla">'+serialE+'</td><td class="cantidad">'+objeto.cantidad+'</td><td class="descuentoIcon">'+'<i class="fas fa-ban"></i>'+'</td><td class="descuentoCan">'+objeto.descontado2+'</td><td><i class="fa fa-ban"></i></td><td><a href="#" id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';
                  }*/


                  var serialE = (objeto.serial !== "") ? '<a id="visualizarSerial" data-ids="'+objeto.id+'" data-idd="'+objeto.id+'" data-codart="'+objeto.codart+'" href="#"><i class="fas fa-book"></i></a>' : '<a href="#" data-ids="'+objeto.id+'" data-idd="'+objeto.id+'" data-codart="'+objeto.codart+'" id="visualizarSerial" ><i class="fas fa-book"></i></a>'
                   if(objeto.descuento == true){
                    var html = '<tr class="cosas" id="cosass" data-id="'+objeto.id+'"  style="text-align:center"><td data-id="'+$row3+'" id="idarts"  class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion">'+objeto.articulo+'</td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.preunit+'</td><td class="talla">'+serialE+'</td><td class="cantidad">'+objeto.cantidad+'</td><td class="precio">'+objeto.costo2+'</td><td class="descuentoIcon">'+'<i class="fas fa-ban"></i>'+'</td><td class="descuentoCan">'+objeto.descontado2+'</td><td><i class="fa fa-ban"></i></td><td><a href="#" id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';

                  }  else if(objeto.ivaStatus === "true"){
                    
                    var html = '<tr class="cosas" id="cosass" data-id="'+objeto.id+'"  style="text-align:center"><td data-id="'+$row3+'" id="idarts"  class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion">'+objeto.articulo+'</td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.preunit+'</td><td class="talla">'+serialE+'</td><td class="cantidad">'+objeto.cantidad+'</td><td class="precio">'+objeto.costo2+'</td><td class="descuentoIcon">'+'<i class="fas fa-ban"></i>'+'</td><td class="descuentoCan">'+objeto.descontado2+'</td><td><i class="fa fa-ban"></i></td><td><a href="#" id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';
                 }
                  else{
                  
                    var html = '<tr class="cosas" id="cosass" data-id="'+objeto.id+'"  style="text-align:center"><td data-id="'+$row3+'" id="idarts"  class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion">'+objeto.articulo+'</td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.preunit+'</td><td class="talla">'+serialE+'</td><td class="cantidad">'+objeto.cantidad+'</td><td class="precio">'+objeto.costo2+'</td><td class="descuentoIcon">'+'<i class="fas fa-ban"></i>'+'</td><td class="descuentoCan">'+objeto.descontado2+'</td><td><i class="fa fa-ban"></i></td><td><a href="#" id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';
                  }
                  
                  $('.arts').append(html);
                })

             /* codDescon = '';   */ 

             }); //foreach
                
            }//if

             else{
              
              /* let iva = 0*/
               /*var general = desGeneral;
               var monto = totalc;*/
               /*monto = monto * Number(descuento);
               monto = monto/100;
               general = general + monto;
               desGeneral = desGeneral + general;
               $('#descuento').val(general.toLocaleFixed(2))

               let total = totalc; 
               total = total - monto;
               total = total.toLocaleFixed(2)
               $('#total').val(total)
               console.log('aca')
*/
              /*  console.log('aca')
*/                  var articulos = inventario;
               
                articulos.forEach(function(objeto) {
                    var general = desGeneral;
                    var monto = totalc;

                     let precio = objeto.costo;
                     var descontar = precio * Number(descuento);
                     descontar = descontar/100;

                     descontar = parseFloat(descontar)
                     descontar = descontar.toFixed(2)
                   /*  console.log(descontar)*/
                 /*  console.log(descontar)*/
                     descuentoc = Number(descuentoc) + Number(descontar)
                   /*  console.log(descuentoc)*/


                     
                     general = Number(general) + Number(descontar);
                     desGeneral = general;
                     objeto.descuento = true;
                     objeto.descontado = descontar;
                     objeto.mondescuento = descuento;
                     descontar = Number(descontar)
                     objeto.descontado2 = descontar.toLocaleFixed(2)

                    /* $('#descuento').val(desGeneral.toLocaleFixed(2))*/

                  /*   totalc = totalc - descontar;
                     console.log('descontando a total', totalc)*/
                     precio = precio - descontar
                    console.log(objeto)
                     if(objeto.ivaStatus === "true"){
                       ivaS = true
                       /* let iva = 0;*/
                      /*   console.log('iva',iva)*/
                        let sumaIva = Number($('#monrgo').val())
                        let calculo = precio*sumaIva;
                        calculo = calculo /100;
                        /* console.log('calculo',calculo)*/
                        iva = Number(iva) + calculo;
                        /* console.log('iva',iva)*/
                        /*  totalc = totalc + iva*/
                        iva = parseFloat(iva)
                        ivac = iva.toFixed(2)
                        console.log(iva);
                        
                       /* console.log('iva2',iva)*/
             /*           $('#iva').val(iva.toLocaleFixed(2)); */
                /*        var totaldes = totalc; 
                        totaldes = totaldes.toLocaleFixed(2)
                        $('#total').val(totaldes)*/
                     }

                      inventario = articulos;
                      $('#inventario').val(articulos);
                
                 }) //foreach
                  
                      

             }//else
              $('#descuentoM').modal('hide');
              $('body').removeClass('modal-open');
              $('.modal-backdrop').remove();
              document.getElementById("formDescuento").reset();
              $('#guardarDescuento').attr('disabled','disabled');

              /*let tasa = JSON.parse($('#tasatojson').val());
               let letrero = 'TASA $'+'('+tasa.valor+')'
               $('#labelTasa').text(letrero);
              let valor = tasa.valor;
              cambio = totalc / valor;
              $('#tasa').val(cambio.toLocaleFixed(3))

              let restantevalor = Number(restante / valor)
              console.log('restante tasaaaa')

              console.log(restantevalor)
              $('#tasarestan').val(restantevalor.toLocaleFixed(3))  */

          /*    totalc = totalc - descuentoc;*/

              /*let iva = 0;
               console.log('iva',iva)
              let sumaIva = Number($('#monrgo').val())
              let calculo = totalc*sumaIva;
              calculo = calculo /100;
              console.log('calculo',calculo)
              iva = Number(iva) + calculo;
              console.log('iva',iva)
              totalc = totalc + iva
              iva = parseFloat(iva)
              ivac = iva.toFixed(2)
              $('#iva').val(iva.toLocaleFixed(2)); */

            /*  console.log('descuentoc', descuentoc)*/
              $('#descuento').val(descuentoc.toLocaleFixed(2))
               var totaldesc =  subtotalc - descuentoc
               $('#subtotal1').val(totaldesc.toLocaleFixed(2))
             /*  console.log('iva: ',iva)*/

             
             let st = subtotalc
             //st = st - descuentoc
             //totalc = subtotalc - descuentoc
             //console.log(totalc);
             
             if(ivaS === true){
                  //$('#iva').val(iva.toLocaleFixed(2)); 
                  let sumaIva = Number($('#monrgo').val())
               //   let calculo = st*sumaIva;
                //  calculo = calculo /100;
                //  console.log('calculo',st)
               //   iva = calculo;
                 /* console.log('iva',iva)*/
                  //iva = parseFloat(iva)
                  //ivac = iva.toFixed(2)
                //  console.log('iva2',iva)
                  
                  //st = st + iva
                  $('#iva').val(iva.toLocaleFixed(2));
                  
                  totalc = st
                  ivaS = false
                //  totalc = totalc + iva
                }
                

                console.log('total: ',totalc)
                 


              var totaldes = totalc; 
              
              console.log(Number(totaldes),descuentoc)
              $('#total').val(((totaldes-descuentoc) + iva).toLocaleFixed(2))

              totalc = (totaldes-descuentoc) + iva
              restante = totalc
              restante = parseFloat(restante);
              restante = restante.toFixed(2)


             let restanteb = Number(restante)
             $('#bolivarrestan').val(restanteb.toLocaleFixed(2))
              /*console.log(restante)*/
              let tasa = JSON.parse($('#tasatojson').val());
              let valor = tasa.valor;
           
           
           let restantevalor = Number(restante / valor)
           
           $('#tasarestan').val(restantevalor.toLocaleFixed(2))
               let rows = $('.cosas');
                rows.fadeOut();

                let t = 1
                inventario.forEach(function(objeto) {
                     if(!objeto.predivisa) predivisa = '<i class="fas fa-ban"></i>'
                      else predivisa = objeto.predivisa
                      
                      
                      
                     objeto.id = t
                     var serialE = (objeto.serial.length !== 0) ? '<a id="visualizarSerial" data-ids="'+objeto.id+'" data-idd="'+objeto.id+'" data-codart="'+objeto.codart+'" href="#"><i class="fas fa-book"></i></a>' : '<a href="#" data-ids="'+objeto.id+'" data-idd="'+objeto.id+'" data-codart="'+objeto.codart+'" id="visualizarSerial" ><i class="fas fa-book"></i></a>'
                     t = t+1
                     if(objeto.descuento == true && objeto.talla != null){
                      var html = '<tr class="cosas" id="cosass" data-id="'+objeto.id+'"  style="text-align:center"><td data-id="'+$row3+'" id="idarts"  class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion">'+objeto.articulo+'</td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.preunit+'</td><td class="talla">'+serialE+'</td><td class="cantidad">'+objeto.cantidad+'</td><td class="precio">'+objeto.costo2+'</td><td class="descuentoIcon">'+'<i class="fas fa-ban"></i>'+'</td><td class="descuentoCan">'+objeto.descontado2+'</td><td><i class="fa fa-ban"></i></td><td><a href="#" id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';
                     }
                     /*else if (objeto.descuento == false && objeto.talla != null){
                      var html = '<tr class="cosas" id="cosass" data-id="'+objeto.id+'"  style="text-align:center"><td data-id="'+$row3+'" id="idarts"  class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion">'+objeto.articulo+'</td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.costo2+'</td><td class="talla">'+serialE+'</td><td class="cantidad">'+objeto.cantidad+'</td><td class="descuentoIcon">'+'<i class="fas fa-ban"></i>'+'</td><td class="descuentoCan">'+objeto.descontado2+'</td><td><i class="fa fa-ban"></i></td><td><a href="#" id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';

                     }*/
                     else if(objeto.descuento == true && objeto.ivaStatus === "true"){
                      var html = '<tr class="cosas" id="cosass" data-id="'+objeto.id+'"  style="text-align:center"><td data-id="'+$row3+'" id="idarts"  class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion">'+objeto.articulo+'</td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.preunit+'</td><td class="talla">'+serialE+'</td><td class="cantidad">'+objeto.cantidad+'</td><td class="precio">'+objeto.costo2+'</td><td class="descuentoIcon">'+'<i class="fas fa-ban"></i>'+'</td><td class="descuentoCan">'+objeto.descontado2+'</td><td><i class="fa fa-ban"></i></td><td><a href="#" id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';

                     }
                     else if(objeto.ivaStatus === "true" && objeto.descuento == false ){
                      var html = '<tr class="cosas" id="cosass" data-id="'+objeto.id+'"  style="text-align:center"><td data-id="'+$row3+'" id="idarts"  class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion">'+objeto.articulo+'</td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.preunit+'</td><td class="talla">'+serialE+'</td><td class="cantidad">'+objeto.cantidad+'</td><td class="precio">'+objeto.costo2+'</td><td class="descuentoIcon">'+'<i class="fas fa-ban"></i>'+'</td><td class="descuentoCan">'+objeto.descontado2+'</td><td><i class="fa fa-ban"></i></td><td><a href="#" id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';
                      }
                  else{
                     
                    var html = '<tr class="cosas" id="cosass" data-id="'+objeto.id+'"  style="text-align:center"><td data-id="'+$row3+'" id="idarts"  class="idart">'+objeto.id+'</td><td class="codigo">'+objeto.codart+'</td><td class="descripcion">'+objeto.articulo+'</td><td class"predivisa">'+predivisa+'</td><td class="precio">'+objeto.preunit+'</td><td class="talla">'+serialE+'</td><td class="cantidad">'+objeto.cantidad+'</td><td class="precio">'+objeto.costo2+'</td><td class="descuentoIcon">'+'<i class="fas fa-ban"></i>'+'</td><td class="descuentoCan">'+objeto.descontado2+'</td><td><i class="fa fa-ban"></i></td><td><a href="#" id="deleterow" class="deleterow"><i class="fa fa-trash"></i></a></td></tr>';
                  }

                     $('.arts').append(html);
                    })

              tasar()

              manualMode = false 
      /*        console.log(inventario)*/

            /*  console.log(descuentoc)*/

         })


         $(document).on('change','#descuentosSelect', function (e){
          $('#guardarDescuento').removeAttr('disabled');
         })

         $(document).on('keyup','#descuentoInput', function(e){
          if($('#descuentoInput').val()=="0" || $('#descuentoInput').val()==''){
            $('#guardarDescuento').attr('disabled','disabled');
          }
          else{
            $('#guardarDescuento').removeAttr('disabled');
          }
         })

         $(document).on('click','#precargado', function(e){
          e.preventDefault()
          manualMode = false
          $('#descuentosSelect').removeAttr('disabled');
          $('#descuentoInput').attr('disabled','disabled');
           $('#guardarDescuento').attr('disabled','disabled');
         })

         $(document).on('click','#manual', function(e){
          e.preventDefault()
          manualMode = true;
          $('#descuentosSelect').attr('disabled','disabled');
          $('#descuentoInput').removeAttr('disabled');
          $('#guardarDescuento').attr('disabled','disabled');
         })



        function logToCaja() {/////////FUNCION QUE PERMITE ABRIR LA CAJA
          $(document).on('submit','#formSecureCaja',function (e) {
            e.preventDefault();
            let select = $(this).find('select.form-control')
            let input = $(this).find('input.form-control')
            let url = $('#loginCaja').val()
            let _token = $("input[name='_token']").val();
            let data = {id:$('#cajaSelected').val(),id2:$('#passwordCaja').val(),_token:_token}
            if(!data.id || !data.id2){
              Swal.fire({
                // allowEnterKey: true,
                icon: 'error',
                title: 'Faltan Campos',
                text: 'Verifique los datos ingresados',
                // footer: '<a href>Why do I have this issue?</a>'
              })
              $('.swal2-confirm').focus()
              return
            }
            $.ajax({
              type: "POST",
              url: url,
              data: data,
              dataType: "json",
              success: function (result) {
                if(result.status === true){// se abre la caja
                    Swal.fire({
                      icon: 'success',
                      title: 'Caja Aperturada Exitosamente',
                      showConfirmButton: false,
                      timer: 1200
                    })
                  $('h4.cajas').html('<b><span style="color:blue">CAJA ABIERTA</span></b><i class="fa fa-lock" id="abrirCaja"></i><i class="fa fa-lock-open" style="display: none" id="cerrarCaja">'
                  +' '+result.nombreCaja+' '+'</i>')
                  $('#cardFactura').removeClass('disabledbutton');//meter en la otra funcion de ajax
                  $('#cardCliente').removeClass('disabledbutton');
                  $('#opcionesF').removeClass('disabledbutton');
                  cajaStatus = true
                  window.caja = cajaStatus;
                  ccaja = result.nombreCaja
                  $('#abrirCaja').hide();
                  $('#cerrarCaja').show();
                  $('#lockedCaja').modal('hide')///Modal que indica que la caja esta bloqueada
                  $('input.caja').val("");
                  $('input#passwordCaja').val("");
                }else{
                  Swal.fire({
                    // allowEnterKey: true,
                    icon: 'error',
                    title: 'CONTRASEÑA INCORRECTA',
                    text: 'Verifique los datos ingresados',
                    // footer: '<a href>Why do I have this issue?</a>'
                  })
                  $('.swal2-confirm').focus()
                }
              }
            });
          })
        }


        function checkCaja(){//Funcion que chequea al momento de recargar la pagina si la caja esta activa o no
          let url1 = $('body').data('url');
           $.get(url1,function (result) {
            if(result.status === 'TRUE' && result.cajas){
              $('h4.cajas').html('<b><span style="color:blue">CAJA ABIERTA </span></b><i class="fa fa-lock" id="abrirCaja"></i><i class="fa fa-lock-open" style="display: none" id="cerrarCaja">'+' '+result.nombreCaja+' '+'</i>')
              ccaja = result.nombreCaja;
              $('#cardFactura').removeClass('disabledbutton');//meter en la otra funcion de ajax
              $('#cardCliente').removeClass('disabledbutton');
              $('#opcionesF').removeClass('disabledbutton');
              cajaStatus = true;
              window.caja = cajaStatus;
              $('#abrirCaja').hide();
              $('#cerrarCaja').show();
              $('#modal-default').modal('hide');
              $('input.caja').val("");
              let selectCaja = document.getElementById('cajaSelected')
              $(selectCaja).html(result.cajas)
              if(window.isFiscal === 'true') verificarConexion()//verifica si la conexion esta abierta
            }else if(result.cajas){
              desactivarCaja()
              let selectCaja = document.getElementById('cajaSelected')
              $(selectCaja).html(result.cajas)
            }else{
              desactivarCaja()
              alert("Este usuario no tiene cajas registradas")
            }
          })
        }

        function desactivarCaja(){
            $('#lockedCaja').modal('show')///Modal que indica que la caja esta bloqueada
              $('#cardFactura').addClass("disabledbutton");
              $('#cardCliente').addClass("disabledbutton");
              $('#opcionesF').addClass("disabledbutton");
              cajaStatus = false;
              window.caja = cajaStatus;
              $('#abrirCaja').show();
              $('#cerrarCaja').hide();
              $('h4.cajas').html('Caja Cerrada <i class="fa fa-lock" id="abrirCaja"></i><i class="fa fa-lock-open" style="display: none" id="cerrarCaja"></i>')
              $('input.caja').val("");
        }

        function desactivarCajaConexion(){ //esta funcion proporciona en caso de que la red este inestable, no facture y le indica al usuario
          
          $('#cardFactura').addClass("disabledbutton");
          $('#cardCliente').addClass("disabledbutton");
          $('#opcionesF').addClass("disabledbutton");
          cajaStatus = false;
          window.caja = cajaStatus;
          $('#abrirCaja').show();
          $('#cerrarCaja').hide();
          $('h4.cajas').html('Caja Cerrada <i class="fa fa-lock" id="abrirCaja"></i><i class="fa fa-lock-open" style="display: none" id="cerrarCaja"></i>')
          $('input.caja').val("");
      }

        function activarCaja(){
//          $('#lockedCaja').modal('hide')///Modal que indica que la caja esta bloqueada
            $('#cardFactura').removeClass("disabledbutton");
            $('#cardCliente').removeClass("disabledbutton");
            $('#opcionesF').removeClass("disabledbutton");
            cajaStatus = true;
            window.caja = cajaStatus;
            $('#abrirCaja').hide();
            $('#cerrarCaja').show();
            $('h4.cajas').html('Caja abierta <i class="fa fa-lock-open" id="abrirCaja"></i><i class="fa fa-lock-open" style="display: none" id="cerrarCaja"></i>')
            $('input.caja').val("");
      }

     /////////////////CERRAR CAJA
         $(document).on('click','#cerrarCaja',function(e){
          e.preventDefault();
          clear();
          var url2 = $('body').data('url2');
          $.get(url2,function(result) {
              if (result.titulo === 'CAJA CERRADA') {
                Swal.fire({
                  position: 'top-first',
                  icon: 'success',
                  title: 'CAJA CERRADA',
                  showConfirmButton: false,
                  timer: 1500
                });
               $('#lockedCaja').modal('show')///Modal que indica que la caja esta bloqueada
               $('#cardFactura').addClass("disabledbutton");
               $('#cardCliente').addClass("disabledbutton");
               $('#opcionesF').addClass("disabledbutton");
               cajaStatus = false;
               $('#abrirCaja').show();
               $('#cerrarCaja').hide();
               $('h4.cajas').html('Caja Cerrada <i class="fa fa-lock" id="abrirCaja"></i><i class="fa fa-lock-open" style="display: none" id="cerrarCaja"></i>')
               $('input.caja').val(" ");
               $('input#inputPassword3').val(" ");
              }
          });
      })

         $(document).on('change','#tallas',function(e){
          e.preventDefault();
          $('#cantidad').val(1)
          //let pre = $('#preunit').val()
          let pre = $('#preunit2').val()
          $('#precio').val((pre*1).toLocaleFixed(2))
          $('#cantidad').tooltip('hide')
          /*$('#cantidad').removeAttr('title')*/
          $('#cantidad').attr('data-original-title', "");
          $('#cantidad').removeAttr('disabled');
          $('#añadir').removeAttr('disabled');
         })

         $(document).on('change','#almacenes', function(e){
          e.preventDefault();
         $('#cantidad').tooltip('hide')
         $('#cantidad').attr('data-original-title', "");
         $('#cantidad').val(1)
         let pre = $('#preunit').val()
         $('#precio').val(pre*1)
          let url = $(this).data('href')
         $.ajax({
        type: 'GET',
        url: url,
        data: {
            almacen: $('select[name=almacenes]').val(),
            articulo: $('select[name=articulos').val(),
          },
          success: function (result) { 
           /* console.log(result)*/
            let tallas = result.tallas;
          /*    console.log(tallas)*/
                
              let select = $('#tallas');
              let content = '<option selected disabled> TALLA</option>';
              if(result.length>0){
               /*let tallas = result.tallas;
               console.log(tallas)*/
                
                  $.each(result, function (index, val) {
                 /*   console.log(val[0])*/
                    content = content.concat(
                    '<option value="' + val[0].codtallas + '" >' + val[0].tallas + '</option>'
                    );
                   })
                 $(select).html(content);
                $('#tallas').removeAttr('disabled');
                $('#añadir').attr('disabled','disabled');
                $('#cantidad').attr('disabled','disabled');
              }
              else{
                /*$(select).html(content);*/
                $('#tallas').attr('disabled','disabled');
                $('#añadir').removeAttr('disabled');
                 $('#cantidad').removeAttr('disabled');

              }
            }
          })

         })

/*         $(document).on('change','#articulo', function(e){
          e.preventDefault();
         $('#cantidad').tooltip('hide')
         $('#cantidad').attr('data-original-title', "");
         $('#cantidad').val(1)
/
          let url = $(this).data('href2')
         $.ajax({
        type: 'GET',
        url: url,
        data: {
            articulo: $('select[name=articulos').val(),
          },
          success: function (result) { 
            console.log(result)
            let tallas = result.tallas;

                
              let select = $('#tallas');
              let content = '<option selected disabled> TALLA</option>';
              if(result[0] != '' || result[0] != 0){

                  $.each(result, function (index, val) {
                    content = content.concat(
                    '<option value="' + val[0].codtallas + '" >' + val[0].tallas + '</option>'
                    );
                   })
                 $(select).html(content);
                $('#tallas').removeAttr('disabled');
                $('#añadir').attr('disabled','disabled');
                $('#cantidad').removeAttr('disabled');
              }
              else{
                $('#añadir').removeAttr('disabled');
                 $('#cantidad').removeAttr('disabled');

              }

            }
          })

         })*/


         $(document).on('click','#devolucion',function(e){
          e.preventDefault();
          $('.ErrorTxt').html('')
          document.getElementById('formDevolucion').reset();
          let row = $('.trFactura');
          row.fadeOut();
          document.getElementById('tablaDevolucionFac').style.display = 'none'
          $('#devolucionM').modal('show');
        })
  
        $(document).on('click','#buscarFactura',function(e){
          e.preventDefault();
          let row = $('.trFactura');
          row.fadeOut();
          let url = $(this).data('href')
          $.ajax({
            type: 'GET',
            url: url,
            data: {
            codfactura: $('input[name=codfactura]').val(),
            },
            success: function (result) {
              if(result.length>0){
                let html = '<tr class="trFactura" style="text-align:center">'+
                '<td class="nfactura">'+result[0].reffac+'</td>'+
                '<td class="facturadc">'+result[0].codcli+'</td>'+
                '<td class="facturac">'+result[0].cliente.nompro+'</td>'+
                '<td class="monfac">'+result[0].monfac+'</td>'+
                '<td>'+
                  '<a class="btn btn-warning" id="anularFac" data-toggle="tooltip" data-placement="top" title="Devolver Factura"><i class="fas fa-ban"></i></a>'+
                '</td>'+
                '</tr>';
                $('.listFactura').append(html);
                document.getElementById('tablaDevolucionFac').style.display = 'block'
                $('[data-toggle="tooltip"]').tooltip()
              }
              else{
                $('.ErrorTxt').html('')
                $('.ErrorTxt').append(`
                  <div div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-exclamation-triangle"></i> Advertencia!</h5>
                    El número de factura no se encuentra registrado, y/o no es una factura fiscal
                  </div>
                `)
                /*let row = document.getElementsByClassName('trFactura');*/
                row.fadeOut();
              }
            }
          })
        })
  
    $(document).on('click','#anularFac', function(e){
      e.preventDefault();
        let r = $(this).parents('tr');
        let td = r.find('td.nfactura').text()
        let url = $('#anulacion').data('href')
        Swal.fire({
          title: '¿DESEA ANULAR LA FACTURA SELECCIONADA?',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si',
          cancelButtonText: 'No',
          showLoaderOnConfirm: true,
          preConfirm: () => {
          return fetch(url+'?'+new URLSearchParams({codfactura : td,codcaja  : ccaja}))
          .then(response => {
            return response.json()
          })
          .catch(error => {
            Swal.showValidationMessage(
              `Fallo la anulación de la factura: ${error}`
            )
          })
        },
        allowOutsideClick: () => !Swal.isLoading()
        }).then((result)=>{
          if(result.isConfirmed){
            if(result.value.success){
              Swal.fire(
              'HECHO',
              '<strong>LA FACTURA HA SIDO ANULADA</strong>',
              'success',
              );
            }
            if(result.value.error){
              Swal.fire( 
                'ERROR',
                `<strong>${result.value.error ? result.value.error : 'Ha ocurrido un error!'}</strong>`,
                'warning',
              );
            }
        let row = $('.trFactura');
        row.fadeOut();
        $('#devolucionM').modal('hide');
        document.getElementById('formDevolucion').reset();
  /*              $('#formDevolucion').reset();*/
        let input = $('#codfactura');
        let content = '<input> </input>';
        $(input).html(content);
        $(input).val('');
        }
        })//success
})//ajax

      /**
       * FUNCIONES PARA MANIPULAR EL IVA
       * 
       */
      // inicializamos el plugin
      $('select.select2').select2({});

      $(document).on('click','#labelIva',function(e){
        e.preventDefault();
        if(inventario.length<1){
         $('#ivaM').modal('show');
         $('#guardarIva').attr('disabled','disabled');
        }
      })

      $(document).on('change','#recargos',function(e){
        e.preventDefault();
        $('#guardarIva').removeAttr('disabled');
      })

      $(document).on('click','#guardarIva',function(e){
        e.preventDefault();
        let url = $('#recargos').data('href')
        let r = $('#recargos').val()

        if($('#recargos').val()=='00.00'){
          let letrero = 'SIN IVA'
          $('#labelIva').text(letrero);
          $('#monrgo').val(0);
          facturacion_data.recargo = {}
        }
        else{
           $.ajax({
            type: 'GET',
            url: url,
            data: {
              iva: r,
            },
            success: function (result) {
              $('#labelIva').text(result.nomrgo);
              $('#monrgo').val(result.monrgo);
              /* asigna el recargo seleccionado */
              facturacion_data.recargo = {...result}
            } 
          })
        }//else
          $('#ivaM').modal('hide');
          document.getElementById('formIva').reset();
      })

      $(document).on('click','#home', function (e){
        if(cajaStatus == true){
          $("#cerrarCaja").click();
        }
        window.location.href = "/home";
      })


        $(document).on('click','#deletepago',function(e){
        e.preventDefault();
        var rowp = $(this).parents('tr');

        td2 = $(this).closest('td');
        tdes = td2.prev();
        td3 = tdes.prev();
        td4 = td3.prev();
        td5 = td4.prev(); //pago
        td6 = td5.prev(); //formato
        td7 = td6.prev(); //moneda
        tdid = td7.prev().text() //id
        console
        $nombrem = td7.text();

        plata = td4.prev().text();
/*          console.log(pagado)*/

      
         let bolivar = true
         let mulfuera = 0
/*           console.log('restante:', restante)*/

         if($nombrem != 'BOLIVAR'){
            bolivar = false
        /*  var v = 0*/
         /* console.log('dolla')*/
              /* let tasas = JSON.parse($('#todastasastojson').val());

               tasas.forEach(function(objeto) {
                 if(objeto.moneda.nombre == $nombrem){
                   v = objeto.valor
                   bolivar = false*/
               /*  console.log('v',v)*/
                   /*let mul = plata * v*/
                 /*mul = mul.toFixed(2)*/
                   /*pagado = pagado - mul*/
                /* console.log(pagado)
                   console.log('yeeee')*/

                 /*  mulfuera = mul.toFixed(2)
                   console.log('mulfuera:',mulfuera)*/
                /*   restante = restante + Number(mulfuera)*/
                  /* restante = parseFloat(restante)*/
                   /*restante = restante.toFixed(2)*/
               /*    pagado = pagado - mulfuera*/
               /*    pagado = parseFloat(pagado)*/
                /*   pagado = pagado.toFixed(2)*/
          /*       }
             });*/
         }
         else{
          /*console.log('plata',plata)*/
            pagado = pagado - plata;
         /*   pagado = parseFloat(pagado)*/
         /*   pagado = pagado.toFixed(2)*/
            restante = restante + Number(plata);
            restante = parseFloat(restante)
            restante = restante.toFixed(2)
         }


                  pagosb = pagos;
                  let restantemientras = totalc
                  let pagadomientras = 0
                  pagosb.forEach(function(objeto) {

                   if(bolivar == true){
                       if(objeto.monto == plata){
                  
                        index = pagosb.map(function(e) { return e.monto; }).indexOf(plata);
                        index2 = pagosb.findIndex(val => val.monto == plata);
        
                        pagosb.splice(index, 1)

                        pagos = pagosb;
                        /*console.log(pagosb)*/
                        $('#pagos').val(pagosb);
                       }

                   }

                   else{
                     let ul = objeto.montoc 
                     let obid = objeto.id
                  /*   console.log('id',tdid)
                     console.log('ul',ul)
                     console.log(objeto)*/
                     if(obid == tdid){


                     /*if(ul == plata){
                      let calculadora = objeto.monto / v
                      if($nombrem == 'PETRO'){
                       calculadora = calculadora.toFixed(9)
                      }
                      else{
                       calculadora = calculadora.toFixed(2)    
                      }
                      console.log('cal: ',calculadora)*/
                   /*  }
                     if(objeto.monto == mulfuera){*/
                 /*    console.log('entre')*/
                /*     index = pagosb.map(function(e) { return e.montoc; }).indexOf(calculadora);*/
                     index = pagosb.map(function(e) { return e.id; }).indexOf(obid);
                  /*   console.log(index)*/
/*                       index2 = pagosb.findIndex(val => val.monto == mulfuera);*/

                     let en = pagosb[index].monto
                    
        
                     pagosb.splice(index, 1)

                      pagos = pagosb;

                      pagado = pagado - en; 
                      pagado = pagado.toFixed(2)
                      /*console.log('r',restante)*/
                      restante =  Number(restante) + Number(en);
                      /*console.log('r2',restante)*/
                      restante = parseFloat(restante)
                      restante =  restante.toFixed(2)
                  /*   }*/
                     }
                   }     
             }); 

                  let rows2 = $('.pagosrow');
                  rows2.fadeOut()

                  let $u2 = 1
                  if(pagosb.length > 0){


                  pagosb.forEach(function(objeto) {
                   objeto.id = $u2
                 
                   if (objeto.tipocredito == null && objeto.montoc == 0){
                     var html = '<tr class="pagosrow" style="text-align:center"><td class"idpago">'+$u2+'</td><td class="monedatabla">'+objeto.moneda+'</td><td class="fpagotd">'+objeto.fpago+'</td><td class="paguitom">'+objeto.monto+'</td><td>'+objeto.banco+'</td><td><i class="fas fa-ban"></i></td><td class="ref">'+objeto.refbillete+'</td><td><a id="deletepago" class="deletepago"><i class="fas fa-trash"></a></i></td></tr>';
                    }
                   else if(objeto.tipocredito == null && objeto.montoc > 0){
                     var html = '<tr class="pagosrow" style="text-align:center"><td class"idpago">'+$u2+'</td><td class="monedatabla">'+objeto.moneda+'</td><td class="fpagotd">'+objeto.fpago+'</td><td class="paguitom">'+objeto.montoc+'</td><td>'+objeto.banco+'</td><td><i class="fas fa-ban"></i></td><td class="ref">'+objeto.refbillete+'</td><td><a id="deletepago" class="deletepago"><i class="fas fa-trash"></a></i></td></tr>';
                      }
                   else if(objeto.tipocredito != null && objeto.montoc == 0){
                      var html = '<tr class="pagosrow" style="text-align:center"><td class"idpago">'+$u2+'</td><td class="monedatabla">'+objeto.moneda+'</td><td class="fpagotd">'+objeto.fpago+'</td><td class="paguitom">'+objeto.monto+'</td><td>'+objeto.banco+'</td><td>'+objeto.tipocredito+'</td><td class="ref">'+objeto.refbillete+'</td><td><a id="deletepago" class="deletepago"><i class="fas fa-trash"></a></i></td></tr>';
                    }
                    else{
                     var html = '<tr class="pagosrow" style="text-align:center"><td class"idpago">'+$u2+'</td><td class="monedatabla">'+objeto.moneda+'</td><td class="fpagotd">'+objeto.fpago+'</td><td class="paguitom">'+objeto.montoc+'</td><td>'+objeto.banco+'</td><td>'+objeto.tipocredito+'</td><td class="ref">'+objeto.refbillete+'</td><td><a id="deletepago" class="deletepago"><i class="fas fa-trash"></a></i></td></tr>';

                    }
                  $('.cosaspagos').append(html);
                    $u2 = $u2 + 1
                  })
                 }
/*
                  if(bolivar == false){
                        pagos.forEach(function(objeto) {
                        restantemientras = restantemientras - Number(objeto.monto)
                        restantemientras = restantemientras.toFixed(2)
                        pagadomientras = pagadomientras + Number(objeto.monto)
                  })
                        console.log('mm:',restantemientras)
                        console.log('mmm',pagadomientras)
                  restante = restantemientras
                  pagado = pagadomientras
                  }*/


              /*    console.log('restante: ', restante)
                  console.log('pagado:', pagado)*/

        
        if(pagado == 0){
         $('#pagorestante').hide();
         $('#pagorestante').attr('disabled','disabled');
         $('#pagocompleto').removeAttr('disabled');
         $('#pagocompleto').show();
         pagototal = false;
        }
        else{
         $('#pagorestante').show();
         $('#pagorestante').removeAttr('disabled','disabled');
         $('#pagocompleto').attr('disabled','disabled');
         $('#pagocompleto').hide();
         pagototal = true;
        }
                  
        
         $('#tipocredito').removeAttr('selected');
         $('#tipocredito').defaultSelected;


         $('#añadirpago').removeAttr('disabled');
         $('#montopago').removeAttr('disabled');
         $('#fpagos').removeAttr('disabled');
         $('#bancos').removeAttr('disabled');
         $('#tipocredito').removeAttr('disabled');
/*           console.log(pagos);
         console.log($('#pagos').val());*/

           let lengthpaguitos = pagos.length
            if(lengthpaguitos < 1){
             pagado = 0
             restante = totalc
            }
        

            let tasa = JSON.parse($('#tasatojson').val());
            let valor = tasa.valor;
            let restantevalor = Number(restante / valor)
            $('#tasarestan').val(restantevalor.toLocaleFixed(2))

   /*         console.log('pagado:', pagado)
            console.log('restante:', restante)*/
         /*   tasar()*/


           let restanteb = Number(restante)
            $('#bolivarrestan').val(restanteb.toLocaleFixed(2))

      })


        $(document).on('input','#montopago',function(e){
          e.preventDefault();
          $(this).tooltip('hide')
          $(this).attr('data-original-title', " ");
          var comparacion = $('#montopago').val();
          comparacion = Number(comparacion)

        /*    if(comparacion > totalc){
             $('#montopago').val(totalc);
             let p = $('#montopago');
             $(p).attr('data-original-title','El maximo a pagar es '+totalc)
             $(p).tooltip('show')
            }*/

            if(pagado > 0){
             if(valorMoneCam > 0){
                 let cambiariorestante = restante/ valorMoneCam 
                 cambiariorestante = cambiariorestante.toLocaleFixed(2)
                 if(comparacion > cambiariorestante){
                   $('#montopago').val(cambiariorestante)
                   let p = $('#montopago');
                   $(p).attr('data-original-title','El maximo a pagar es '+cambiariorestante)
                   $(p).tooltip('show')
                  }//if
              }//
             else{
                 if(comparacion > restante){
                   $('#montopago').val(0)
                   restante = Number(restante)
                   $('#montopago').val(restante)
                   let p = $('#montopago');
                   $(p).attr('data-original-title','El maximo a pagar es '+restante)
                   $(p).tooltip('show')
                 }//if
               }//else
               pc = false
               escriCant = true
            }//if
           else{
             if(valorMoneCam > 0){
               let mon = totalc / valorMoneCam
               if(comparacion > mon){
                 mon = mon.toFixed(2)
                 $('#montopago').val(mon)
               }//if
              }//if
              else{
                if(comparacion > totalc){
                  $('#montopago').val(totalc);
                  let p = $('#montopago');
                  $(p).attr('data-original-title','El maximo a pagar es '+totalc)
                  $(p).tooltip('show')
                }//if
              }//else
              pc = false
              escriCant = true
            }
          
        })


/*
      $(document).ready(function(){
        if(!cajaStatus){
          let cajasUrl = $('select.cajs').data('caja');
          console.log(cajasUrl)
          $('#modal-default').modal('show')
        }
      });
  */

        $(document).on('click','#presupuesto',function(e){
          e.preventDefault();
          document.getElementById('formPresupuesto').reset();
          let row = $('.trPresupuesto');
          row.fadeOut();
          document.getElementById('tablaPresupuestoFac').style.display = 'none'
          $('#presupuestoM').modal('show');
         })

        $(document).on('click','#buscarPresupuesto',function(e){
         e.preventDefault();
         let row = $('.trPresupuesto');
         row.fadeOut();
         let url = $(this).data('href')
         console.log(url);
         $.ajax({
         type: 'GET',
         url: url,
         data: {
            codpresupuesto: $('input[name=codpresupuesto]').val(),
          },
          success: function (result) {
/*            console.log(result);*/
          if(result.length>0){
            
            clear();
          var presups = result;

              $.each(presups, function (index, val) {
               let html = '<tr class="trPresupuesto" style="text-align:center"><td class="npresupuesto">'+val.codpre+'</td><td class="presupuestodc">'+val.codcli+'</td><td class="presupuestoc">'+val.cliente.nompro+'</td><td><a class="btn btn-success" id="selPresupuesto"><i class="fas fa-check"></i></a></td></tr>';
               $('.listPresupuesto').append(html);    
                })

      /*       let html = '<tr class="trPresupuesto" style="text-align:center"><td class="npresupuesto">'+result.codpre+'</td><td class="presupuestodc">'+result.codcli+'</td><td class="presupuestoc">'+result.cliente.nompro+'</td><td><a id="selPresupuesto"><i class="fas fa-check"></i></a></td></tr>';
             $('.listPresupuesto').append(html);*/
            document.getElementById('tablaPresupuestoFac').style.display = 'block'
          }
          else{
            /*let row = document.getElementsByClassName('trFactura');*/
            row.fadeOut();
            }

            }
           })
          })


        $(document).on('click','#selPresupuesto',function(e){
          e.preventDefault();
       /*   console.log($(this))*/
          let r = $(this).parents('tr');
          let td = r.find('td.npresupuesto').text()
          let url = $('#seleccionPre').data('href')
            $.ajax({
             type: 'GET',
             url: url,
             data: {
                 codpresupuesto: td,
             },
            success: function (result) {
             /*  console.log(result);*/
            /*   $("#pago").click();
               $("#listarticulos").attr('disabled','disabled');*/
               document.getElementById('formPresupuesto').reset();
               $('#presupuestoM').modal('hide');


               let presupue = result
               numPresu = presupue.codpre
/*                 console.log(presupue)*/
               let articulospre = result.articulos
               let clienteinfo = result.cliente

               $('#documento').val(clienteinfo.codpro)
               $('#nombre').val(clienteinfo.nompro)

               let $v = 1

               $.each(articulospre, function (index, val) {
                  if(val.monto_descuento > 0){
                  var descuentoEs =  true
                  var descuentom = val.monto_descuento
                  }
                  else{
                  var descuentoEs = false
                  var descuentom = 0
                  }

                  let predivisa = ' '
                  let preunit2 = Number(val.preunit)
                  preunit2 = preunit2.toLocaleFixed(2)


                  let descontado2 = Number(descuentom)
                  descontado2 = descontado2.toLocaleFixed(2)

                  let costoart = val.preunit*val.cantart


                  let costo2 = Number(costoart)
                  costo2 = costo2.toLocaleFixed(2)

                   var art = {
                      id: $v,
                      codart : val.codart,
                      articulo : val.desart,
                      talla: val.codtalla,
                      cantidad : val.cantart,
                      preunit : val.preunit,
                      predivisa : predivisa,
                      costo : costoart,
                      descuento: descuentoEs,
                      descontado: descuentom,
                      mondescuento: val.mondescuento,
                      preunit2: preunit2,
                      costo2: costo2,
                      descontado2: descontado2,
                  }

                   inventario.push(art)
               /*    console.log(inventario)*/
                 $v = $v+1
                  }) //each

                  let ivita = Number(presupue.monrgo)
                  ivac = ivita.toFixed(2);
                  let subtotalito = Number(presupue.subtotalpre)
                  subtotalc = subtotalito.toFixed(2);
                  let totalcito = Number(presupue.monpre)
                  totalc = totalcito.toFixed(2);
                  let descuentocito = Number(presupue.mondesc)
                  descuentoc = descuentocito.toFixed(2);

                  restante = totalc
                  restante = parseFloat(restante);
                  restante = restante.toFixed(2)

                 let restanteb = Number(restante)
                 $('#bolivarrestan').val(restanteb.toLocaleFixed(2))
                 /*  console.log(restante)*/
                  presupuestoMode = true;

                   $('#iva').val(ivita.toLocaleFixed(2));
                   $('#subtotal').val(subtotalito.toLocaleFixed(2));
                   
                   $('#total').val(totalcito.toLocaleFixed(2))
                   $('#descuento').val(descuentocito.toLocaleFixed(2))
      /*             console.log(restante)*/

                    $('#labelIva').text(presupue.recargo.nomrgo);
                    $('#monrgo').val(presupue.recargo.monrgo);

                   $('#articulo').attr('disabled','disabled')

                   $("#pago").show();
              
                  $.each(inventario, function (index, val) {
                   let html = ''
                   if(!val.descuento){
                    html = '<tr class="cosas" style="text-align:center"><td class="idart">'+val.id+'</td><td class="codigo">'+val.codart+'</td><td class="descripcion">'+val.articulo+'</td><td class="predivisa">'+val.predivisa+'</td><td class="precio">'+val.costo2+'</td><td class="talla">'+val.talla+'</td><td class="cantidad">'+val.cantidad+'</td><td class="descuentoIcon"><i class="fas fa-ban"></i></td><td class="descuentoCan">'+val.descontado2+'</td></tr>';
                     if(!val.talla){
                      html = '<tr class="cosas" style="text-align:center"><td class="idart">'+val.id+'</td><td class="codigo">'+val.codart+'</td><td class="descripcion">'+val.articulo+'</td><td class="predivisa">'+val.predivisa+'</td><td class="precio">'+val.costo2+'</td><td class="talla"><i class="fas fa-ban"></i></td><td class="cantidad">'+val.cantidad+'</td><td class="descuentoIcon"><i class="fas fa-ban"></i></td><td class="descuentoCan">'+val.descontado2+'</td></tr>';
                     }
                   }
                   else{
                    html = '<tr class="cosas" style="text-align:center"><td class="idart">'+val.id+'</td><td class="codigo">'+val.codart+'</td><td class="descripcion">'+val.articulo+'</td><td class="predivisa">'+val.predivisa+'</td><td class="precio">'+val.costo2+'</td><td class="talla">'+val.talla+'</td><td class="cantidad">'+val.cantidad+'</td><td class="descuentoIcon"><i class="fas fa-check"></i></td><td class="descuentoCan">'+val.descontado2+'</td></tr>';
                     if(!val.talla){
                    html = '<tr class="cosas" style="text-align:center"><td class="idart">'+val.id+'</td><td class="codigo">'+val.codart+'</td><td class="descripcion">'+val.articulo+'</td><td class="predivisa">'+val.predivisa+'</td><td class="precio">'+val.costo2+'</td><td class="talla"><i class="fas fa-ban"></i></td><td class="cantidad">'+val.cantidad+'</td><td class="descuentoIcon"><i class="fas fa-check"></i></td><td class="descuentoCan">'+val.descontado2+'</td></tr>';
                     }
                   }
                   $('.arts').append(html);

                   }) //segundo each inventario
                  tasar()
                 } //success

              }) //ajax
         })

        $(document).on('click','#monedapago', function(e){
          e.preventDefault();
          $('#monedaM').modal('show');
        })

        $(document).on('click','#selecMoneda', function(e){
          e.preventDefault();
          $('#montopago').val(0)
          if($('#monedas').val()){
            let codmoneda = $('#monedas').val();
            let url = $(this).data('href');
              $.ajax({
               type: 'GET',
               url: url,
               data: {
                 codmoneda: codmoneda,
               },
               success: function (result) {
                console.log(result)
               $('#monedac').val(result.nombre)
              /* console.log($('#monedac').val())*/
               monedaCam = result.nombre
           /*    let pagando = monedaCam.toLowerCase();*/
            /*   $('#pagandoen').val(pagando)*/
               let letrero = 'MONEDA: '+monedaCam
               $('#labelMoneda').text(letrero);
              }
             })
           $('#monedaM').modal('hide')
           validacion()

          }//if

          
        })


        $(document).on('click','#selecMoneda', function(e){
          e.preventDefault();
          if($('#monedas').val()){
            var cod
            inventario.forEach(function(objeto) {
             cod = objeto.codart;
                })

            let url = $(this).data('href2');
            let monedita = $('#monedas').val();
            $.ajax({
              type: 'GET',
              url: url,
              data: {
                 codart: cod,
                 codmoneda: monedita,
             },
            success: function (result) {

              if(result.exito){
               tasaCod = result.exito[0].codigoid
               $('#monedacvalor').val(result.exito[0])
               valorMoneCam = result.exito[0].valor
               monedaPrincipal = result.exito[1].nombre
/*                 console.log(valorMoneCam)
               console.log(monedaCam)
               console.log(monedaPrincipal) */
              }
              else{
                tasaCod = ''
                 valorMoneCam = 0;
                 monedaCam = '';
/*                   console.log(valorMoneCam)
                 console.log(monedaCam)*/
                }
               


            }
          })
          }
          

        })

        function tasar(){
          let tasa = JSON.parse($('#tasatojson').val());
          console.log(tasa);
          if(tasa !== null){
            let letrero = 'TASA $'+'('+tasa.valor+')'
            $('#labelTasa').text(letrero);
            let valor = tasa.valor;
            cambio = totalc / valor;
            $('#tasa').val(cambio.toLocaleFixed(2))

            if(pagado > 0){
            let restantevalor = Number(restante / valor)
            $('#tasarestan').val(restantevalor.toLocaleFixed(2))  
            }
          }
        }

        function getColores(){
           let art = $('#articulo').val()
           let url = $('#articulo').data('href3')
                         $.ajax({
              type: 'GET',
              url: url,
              data: {
                 art: art,
             },
            success: function (result) {
              if(result.exito){
              /*  console.log(result)*/
                let colores = result.exito
                let selectc = $('#colores')
                let content = '<option disabled selected>COLORES</option>'
                 $.each(colores, function (index, val) {
                   content = content.concat(
                   '<option value="' + val.codigoid + '" >' +val.color + '</option>'
                  );
                })
                 $(selectc).html(content)
                 $(selectc).show()

              }
              
             }

           })

        }


        $(document).on('click','#nota',function(e){
          e.preventDefault();
          document.getElementById('formNota').reset();
          let row = $('.trNota');
          row.fadeOut();
          document.getElementById('tablaNotaFac').style.display = 'none'
          $('#notaM').modal('show');
         })

         $(document).on('click','#buscarNota',function(e){
          e.preventDefault();
          let row = $('.trNota');
          row.fadeOut();
          let url = $(this).data('href')
          $.ajax({
            type: 'GET',
            url: url,
            data: {
              codnota: $('input[name=codnota]').val(),
            },
             success: function (result) {
/*              console.log(result);*/
             if(result.length>0){

              var nota = result;

                $.each(nota, function (index, val) {
                let html = '<tr class="trNota" style="text-align:center"><td class="nnota">'+val.numero+'</td><td class="notadc">'+val.cod_cliente+'</td><td class="notac">'+val.cliente.nompro+'</td><td><a class="btn btn-success" id="selNota"><i class="fas fa-check"></i></a></td></tr>';
                $('.listNota').append(html);    
                 })

      /*        let html = '<tr class="trPresupuesto" style="text-align:center"><td class="npresupuesto">'+result.codpre+'</td><td class="presupuestodc">'+result.codcli+'</td><td class="presupuestoc">'+result.cliente.nompro+'</td><td><a id="selPresupuesto"><i class="fas fa-check"></i></a></td></tr>';
                $('.listPresupuesto').append(html);*/
                document.getElementById('tablaNotaFac').style.display = 'block'
              }
              else{
               /*let row = document.getElementsByClassName('trFactura');*/
               row.fadeOut();
            }

            }
           })
          })


        $(document).on('click','#selNota',function(e){
          e.preventDefault();
       /*   console.log($(this))*/
          let r = $(this).parents('tr');
          let td = r.find('td.nnota').text()
          let url = $('#seleccionNota').data('href')
            $.ajax({
             type: 'GET',
             url: url,
             data: {
                 codnota: td,
             },
            success: function (result) {
             /*  console.log(result);*/
            /*   $("#pago").click();
               $("#listarticulos").attr('disabled','disabled');*/
               document.getElementById('formNota').reset();
               $('#notaM').modal('hide');


               let nota = result
               numNota = nota.numero
/*                 console.log(presupue)*/
               let articulosnota = result.articulos
               let clienteinfo = result.cliente

               $('#documento').val(clienteinfo.codpro)
               $('#nombre').val(clienteinfo.nompro)

               let $v = 1

               $.each(articulosnota, function (index, val) {

                  var descuentoEs = false
                  var descuentom = 0

                  var preunit2 = Number(val.preunit)
                  preunit2 = preunit2.toLocaleFixed(2)
                  var costo2 = Number(val.costo)
                  costo2 = costo2.toLocaleFixed(2)
    

                  let costoart = val.preunit*val.cantidad
                   var art = {
                      id: $v,
                      codart : val.codart,
                      articulo : val.desart,
                      talla: val.codtalla,
                      cantidad : val.cantidad,
                      preunit : val.preunit,
                      costo : val.costo,
                      descuento: descuentoEs,
                      descontado: descuentom,
                      mondescuento: 0,
                      preunit2 : preunit2,
                      costo2 : costo2,
                      descontado2: descuentom,
                  }

                   inventario.push(art)
               /*    console.log(inventario)*/
                 $v = $v+1
                  }) //each

                  let ivita = Number(0)
                  ivac = ivita.toFixed(2);
                  let subtotalito = Number(nota.total)
                  subtotalc = subtotalito.toFixed(2);
                  let totalcito = Number(nota.total)
                  totalc = totalcito.toFixed(2);
                  let descuentocito = Number(0)
                  descuentoc = descuentocito.toFixed(2);

                  restante = totalc
                  restante = parseFloat(restante);
                  restante = restante.toFixed(2)

                 let restanteb = Number(restante)
                 $('#bolivarrestan').val(restanteb.toLocaleFixed(2))
                 /*  console.log(restante)*/
                  notaMode = true;

                   $('#iva').val(ivita.toLocaleFixed(2));
                   $('#subtotal').val(subtotalito.toLocaleFixed(2));
                   $('#total').val(totalcito.toLocaleFixed(2))
                   $('#descuento').val(descuentocito.toLocaleFixed(2))
      /*             console.log(restante)*/

                   $('#articulo').attr('disabled','disabled')

                   $("#pago").show();
              
                  $.each(inventario, function (index, val) {
                   let html = ''
                 /*  console.log('val1',val)*/

                    html = '<tr class="cosas" style="text-align:center"><td class="idart">'+val.id+'</td><td class="codigo">'+val.codart+'</td><td class="descripcion">'+val.articulo+'</td><td class="predivisa">N/A</td><td class="precio">'+val.costo2+'</td><td class="talla">'+val.talla+'</td><td class="cantidad">'+val.cantidad+'</td><td class="descuentoIcon"><i class="fas fa-ban"></i></td><td class="descuentoCan">'+val.descontado2+'</td></tr>';
                     if(!val.talla){
                      html = '<tr class="cosas" style="text-align:center"><td class="idart">'+val.id+'</td><td class="codigo">'+val.codart+'</td><td class="descripcion">'+val.articulo+'</td><td class="predivisa">N/A</td><td class="precio">'+val.costo2+'</td><td class="talla"><i class="fas fa-ban"></i></td><td class="cantidad">'+val.cantidad+'</td><td class="descuentoIcon"><i class="fas fa-ban"></i></td><td class="descuentoCan">'+val.descontado2+'</td></tr>';
                     }
            

                   $('.arts').append(html);

                   }) //segundo each inventario
                  tasar()
                 } //success

              }) //ajax
         })

   $(document).on('input','#cantcambiar',function(e){
      
      let tasa = JSON.parse($('#tasatojson').val());
      let cantcambiar = $(this).val()
      let multiplo = Number($('#cantcambiar').val()) * tasa.valor
      $('#montocambiar').val(multiplo.toFixed(2))

    })

   $(document).on('change','#customSwitch3', function(e){
      if(pagado == 0){
        let valor = $('#customSwitch3')[0].checked
        let trans = JSON.parse($('#transportevalor').val())
        let valortrans = trans.costo_unit.pvpart
        $('#transporcost').val(valortrans)
        let tasa = JSON.parse($('#tasatojson').val());
        let valortasa = tasa.valor;
        cambiotrans = valortrans * valortasa;
        let total = totalc;

         if(valor == true){
            total = total + Number(cambiotrans);
            total = total - descuentoc;
            totalc = total.toFixed(2)
            $('#total').val(total.toLocaleFixed(2))
         }
         else{
           $('#transporcost').val(0)
            total = total - Number(cambiotrans);
            totalc = total.toFixed(2)
            $('#total').val(total.toLocaleFixed(2))
         }
      }
   })

   async function actualizarCorrelativo(){
    
    
    await fetch('/updateCorrelativo/'+'?'+ new URLSearchParams({
      "fiscal":window.isFiscal
    })).then(res => res.json())
       .then(result => {
        if(result.caja === "fiscal"){
          document.getElementById("updte").innerHTML = 
          `<span  style="color: blue">Proxima Factura </span> : <label style="color:#000">${result.correlativo}</label>`  
        }else{
          document.getElementById("updte2").innerHTML = 
          `<span  style="color: blue">Proxima Tickera </span> : <label style="color:#000">${result.correlativo}</label>`
        }
     })
  
    
   }


   const askForFactura = async (reffac,caja) => {
    if(!reffac) return;
    return new Promise((res, rej) => {

      Swal.fire({
        title: 'Factura Guardada con Exito! </br> ¿Desea Imprimir una copia al cliente?',
        icon : 'success',
        width: 800,
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonText: `<b> Si </b>`,
        denyButtonText: `<b> No </b>`,
        allowOutsideClick: false
      }).then( async (result) => {
        /* Read more about isConfirmed, isDenied below */
        
        if (result.isConfirmed) {
          //Swal.fire('Saved!', '', 'success')
          // if(window.isFiscal === "false") window.isFiscal = "false"
          // else if(window.isFiscal === "true") window.isFiscal = "true";
          console.log(window.isFiscal);

          const response = await facturaResponse(reffac,window.isFiscal,caja)
          /* getResponse(response) */
          console.log(response)
          if(response.success){
            res([true])
            Swal.fire( 
              'Exito!',
              '<strong>Factura Emitida Correctamente</strong>',
              'success',
              );

            

          }else{
            msg = response.msg.indexOf('cURL error') !== -1 ? 'Error al conectar con la api' : response.msg
            res([,response.msg])
            Swal.fire( 
              'Error!',
              '<strong>'+msg+'</strong>',
              'error',
            );
          }
        }/* else if (result.isDenied) {/* Impresion por tickera 
          //Swal.fire('Changes are not saved', '', 'info')
          const response = await facturaResponse(reffac,'noFiscal',caja)
          /* getResponse(response) 
          console.log(response)
          if(response.success){
            res([true])
            Swal.fire( 
              'Exito!',
              '<strong>Factura Emitida Exitosamente</strong>',
              'success',
              );
          }else{
            msg = response.msg.indexOf('cURL error') !== -1 ? 'Error al conectar con la api' : response.msg
            res([,response.msg])
              Swal.fire( 
                'Error!',
                '<strong>'+msg+'</strong>',
                'error',
              );
          }
        }*/
                /* Accion cancelada */
      })
    })
  }
            
  const facturaResponse = async (reffac,isFiscal = false,caja) => {
    if(!reffac || !caja) return;
    //const url = isFiscal ? $('#printFiscal').val() : $('#printTickera').val()
    return new Promise((resolve,reject) =>{
        $.ajax({
          url:$('#printFactura').val(),
          type:'POST',
          dataType : 'json',
          data:{
            _token:$('input[name="_token"]').val(),
            reffac:reffac,
            isFiscal:isFiscal,
            caja:caja
          },
        }).then((data) => {
          console.log(data, "data")
          if(data.success){
            resolve(data)
          }
        }).catch((error)=> {
          console.log(error, "error")
          resolve(error.responseJSON)
        })
      })
  }


  function checkRecargos(){
    if(recargo_json.constructor === Object){//un solo recargo
      facturacion_data.recargo = {...recargo_json}
    }
    /* if(recargo_json.constructor === Array){//son varios recargos

    } */
  }
  let index = 0;
  document.getElementById('searchSeriales').addEventListener('click',async function(e){

     let seriales2 = document.getElementById('serialesValues').value
      await fetch('/modulo/Facturacion/Facturacionv2/searchSeriales'+'?'+new URLSearchParams({
        serial:seriales2,
        isSeriales:true,
        codart:codigoArt
      })).then((res) => res.json())
          .then(({seriales,msg}) =>{
            if(msg !== undefined){
              if(msg !== ''){
                Swal.fire({
                  icon: 'warning',
                  title: msg,
                  showConfirmButton: false,
                  timer: 1200
                })
                return ;
              }
            }
            $('#listaSeriales').html("")
            serialesEntradas = []
            serialesEntradas = seriales

          })


    let cpt = serialesEntradas.map((value) => value);
    console.log(cpt)
    //fas fa-check
    /**/
    let field = '';

    console.log(cpt,inventario[index-1]['serial']);
    cpt.map((res,i) =>
    field += `<tr>
              <td align="center">${res}</td>
              <td align="center">
              <a href="#" data-id="${index-1}" data-codserial="${res}" data-row="${i}" 
              id="serialActivoFactura" >
              <i  id="check${i}"  data-check="false"   
                 class="${(inventario[index-1]['serial'].find((value) => value === res) !== undefined) ? 'fas fa-check' :  'fas fa-ban'}"></i>
              </a>
              </td>
              </tr>`)
   
    

    $('#listaSeriales').html(field)
    $('#modalSerial').modal({ backdrop: "static", keyboard: false });
  })

  $(document).on('click','#visualizarSerial',async function (e) {
    
    codigoArt = $(this).data('codart');
    let titulo = $('#producto')
    $('#producto').html(titulo[0].innerText+' '+codigoArt)
    let field = '';
    index = $(this).data('ids');
    console.log(index);
    serialesEntradas = [];
    await fetch('/modulo/Facturacion/Facturacionv2/getSeriales'+'?'+new URLSearchParams({
                                  codart:codigoArt
                                })).then((res) => res.json())
                                   .then(({seriales,msg}) => {
                                    console.log()
                                    if(msg !== undefined){
                                      if(msg !== ''){
                                        Swal.fire({
                                          icon: 'warning',
                                          title: msg,
                                          showConfirmButton: false,
                                          timer: 1200
                                        })
                                        return ;
                                      }
                                    }
                                    
                                    serialesEntradas = seriales
                                    
                                   })
                                
        
    

    //let cpt = inventario.find(res => res.codart === codigoArt); 
    let cpt = serialesEntradas.map((value) => value);
    console.log(cpt)
    //fas fa-check
    /**/
    /*$('#listaSeriales').remove()*/
    

    console.log(cpt,inventario[index-1]['serial']);
    cpt.map((res,i) =>
    field += `<tr>
              <td align="center">${res}</td>
              <td align="center">
              <a  href="#" data-id="${index-1}" data-codserial="${res}" data-row="${i}" 
              id="serialActivoFactura" >
              <i id="check${i}"  data-check="false"   
                 class=" ${(inventario[index-1]['serial'].find((value) => value === res) !== undefined) ? 'fas fa-check' :  'fas fa-ban'} "></i>
              </a>
              </td>
              </tr>`)
   
    

    $('#listaSeriales').html(field)
    $('#modalSerial').modal({ backdrop: "static", keyboard: false });
 })

 $(document).on('click','#cerrarSerial',function (e){
  let titulo = $('#producto')
  // console.log(titulo[0].innerText)
  $('#producto').html('SERIALES DEL PRODUCTO')
  $('#modalSerial').modal('hide');
})

$(document).on('click','#serialActivoFactura',function(e){

  var i = $(this).data('row');
  var serial = $(this).data('codserial');
  index2 = $(this).data('id')
  var check = $('#check'+i).data('check') 
  let datosRow = {
  'td': i,
  'check':check,
  }
  
  
  if(check === false){//cuando esta aprobado para facturar
  
  $('#check'+i).removeClass('fas fa-ban').addClass("fas fa-check")
  
  $('#check'+i).data('check',true);
  inventario[index2]["serial"].push(serialesEntradas[i]);
  
  }else{//cuando no esta facturado 
  $('#check'+i).removeClass("fas fa-check").addClass('fas fa-ban')
  
  $('#check'+i).data('check',false);
  inventario[index2]["serial"] = inventario[index2]["serial"].filter((value) => value !== serial);
  
  }
  
  
  /*
  evalua si la cantidad de producto es menor a los seriales seleccionados
  muestra el error, tiene que ser iguales
  la variable inputee, es la cantidad de producto a facturar,
  la variable lengArray, es la cantidad de seriales a facturar
  */
  
  let lengArray = inventario[index2]["serial"];
  
  if(lengArray.length > inputee){
  
  $('#check'+i).removeClass("fas fa-check").addClass('fas fa-ban')
  $('#check'+i).data('check',false);
  inventario[index2]["serial"] = inventario[index2]["serial"].filter((value) => value !== serial);
  // serialesEntradas = serialesEntradas.splice(i,1)
  
  Swal.fire({
  icon :'error',
  title :'Error!',
  text : "Los seriales seleccionados es mayor que la cantidad seleccionada",
  toast : true
  });
  console.log(serial,inventario[index2]["serial"]);
  
  
  
  return ;
  } 
  
  
  
  
  })


 $(document).on('click','#añadirDescripcion',function(e){
  var articulo = $(this).data('articulo');
  id = $(this).data('id');

  //$('#articuloDesc').val(articulo).attr('disabled',true);
  $('#descArticulo').val(articulo).attr('disabled',true);;
  
  if(inventario[id-1].descripcion !== undefined)
    $('#descArticulo').val(inventario[id-1].descripcion);
  
  
  $('#descripcionAdd').modal({ backdrop: "static", keyboard: false });

 })

 $(document).on('click','#guardarDescripcion',function(e) {
 /*cuando la propiedad descripcion no existe o esta vacio p
   lo quiera reemplazar
  */     
 if(inventario[id-1].descripcion === undefined || inventario[id-1].descripcion === "" ||
    inventario[id-1].descripcion !== "") 
    inventario[id-1] = {
                        ...inventario[id-1],
                       'descripcion':$('#descArticulo').val().toUpperCase()
                      }
  

    console.log(inventario);
    $('#descripcionAdd').modal('hide');    
 })

async function restarCantidad(inventario,codart,input,index){
console.log(codart,' ',index,' ',input);
await inventario.forEach(function(objeto) {

     if(objeto.codart == codart){
         objeto.cantidad = Number(objeto.cantidad) - Number(input)
         objeto.costo = objeto.preunit * objeto.cantidad
         objeto.costo2 = objeto.costo.toLocaleFixed(2)
      }
  })
 return inventario[index-1] ;
}


});

async function cambiarPaso(inventario){

return  $.ajax({
        type:'GET',
        url:"/modulo/Almacen/EySalida/queryCodart",
        data: {"inventario":inventario},
      })
}

function checkValidationDescription(inventario){
for (var i = 0; i < inventario.length; i++) {
  if(inventario[i].descripcion === undefined || 
     inventario[i].descripcion === ""){
    return {
      "status":false,
      "codart":inventario[i].codart
    }    
  }
}
return {
  "status": true
}
}
