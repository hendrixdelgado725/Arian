
function swalError(msj) {
  Swal.fire({
    icon: 'error',
    title: 'Oops...',
    text: msj+'!',
    /* footer: '<a href>Why do I have this issue?</a>' */
  })
}

function buscarTallasTras(elem)
{
  $('#bloquea').show();
  $(function() {

    var token = $("input[name='_token']").val();
        var almori = $("select#almori").val();
        var almdes = $("select#almdes").val();
        var codart = elem.value
        let anyofthis = [
          almori,
          almdes,
          codart
        ]
        if(anyofthis.indexOf('') !== -1) return
        var selectTalla =  $(elem).closest('tr').find('select.cambios')//tallas
        var selOrigen = $(elem).closest('tr').find('select.ubiOri')//ubicacion origen
        var selDestino = $(elem).closest('tr').find('select.ubiDes')// ubicacion destino
        $.ajax({
              type:'POST',
              url:'ajax',
              data: {almori:almori,almdes:almdes,codart:codart, _token:token, ajax:3},
              success:function(html){
                console.log(html)
                //if()
                  $(selectTalla).html(html.options)
                  $(selOrigen).html(html.ubiori)
                  $(selDestino).html(html.ubides)

                $('#bloquea').hide();
              }
          });
        
    })
}

function validateCant(elem){//valida que la cantidad maxima no sea excedida al momento de realizar una salida de almacen
  $(function(){
    
    var almori = $("select#almori").val();
    var almdes = $("select#almdes").val();
    var codart = elem.value
    let elemento = document.getElementById(elem)
    let anyofthis = [
      almori,
      almdes,
      codart
    ]
    $(elemento).attr('data-original-title','');
    if(anyofthis.indexOf('') !== -1){
      $(elemento).val('')
      return
    }
    let cant = elemento.value
    let arrayMax = $(elemento).closest('tr').find('select.ubiOri').children()
    let arrayValues = []
    let tallas =  $(elemento).closest('tr').find('select.cambios').val()
    let exi = arrayMax.eq($('select.ubiOri').prop('selectedIndex')).data('exiact')//estatico por ahora
      console.log(tallas,exi)
      for (let index = 0; index < arrayMax.length; index++) {
        for (let j = 0; j < tallas.length; j++) {
          if($(arrayMax).eq(index).data('talla') === tallas[j]){
            arrayValues.push(Number($(arrayMax).eq(index).data('exiact')))
          }else if(tallas[j] === 'N/D'){
            arrayValues.push(Number(exi))
          }
        }
      }

      max = Math.min(...arrayValues)//propagacion
    $(elemento).attr('data-original-title','La cantidad mayor permitida por talla '+max)
    
    if(cant > max){
      $(elemento).tooltip('show')
      let cant2 = max;
      $(elemento).val(cant2)
    }
  })
}

function changeAlm(id) {
  /* Al cambiar el almacen se debe tomar en cuenta que si ya hay articulos seleccionados 
    se debe limpiar la tabla para evitar que salga un articulo que no existe
  */
      var almori = $('#almori').val();
      var almdes = $('#almdes').val();
      var tiptras = $('#tiptras').val();//si el movimiento es interno o externo
      var token = $("input[name='_token']").val();
      let totalrows = $('#table > tbody >tr');
      console.log($('#almdes'))
  console.log(almori,almdes,tiptras)
      if(almori === '' || almdes === '' || tiptras === '') return
        $.ajax({
          type:'POST',
          url:'ajax',
          data: {almori:almori, _token:token, ajax:1},
          success:function(html){
            let valores = html.options;
              if(valores !== '0') 
              {
                $.each(totalrows, function (index, value) {
                  clearTable($(totalrows).eq(index),valores)
                });
              }
              else
              {
                swalError("Error el Almacen Origen "+$('#almori option:selected').text()+" no tiene articulos Registrados")
                $('#almori').prop('selectedIndex', 0).change();
                // $('#entrada').val(valores);
              }
              //$(select).html(content);
              $('#bloquea').hide();
          }
        });
}

function changeTipTrasp(elemid) {
  var almori = document.getElementById('almori').value
  
  var almdes = document.getElementById('almdes')
  
  let tiptras = document.getElementById(elemid).value
  var token = $("input[name='_token']").val()
  
  if(almori === '' ||  tiptras === '') return
  $.ajax({
    type:'POST',
    url:'ajax',
    data: {almori:almori,tiptras:tiptras, _token:token, ajax:2},
    success:function(html){
        $(almdes).html('')
        $(almdes).html(html.almacenes)
        $('#bloquea').hide();
    }
  });

}


function clearTable(elemento = '',options = ''){
    var elementos = $(elemento).find('input.fillable ,select.fillable')
    $.each(elementos, function (index, val) { 
      
      if(val.nodeName === 'SELECT') {
        if($(val).hasClass('select2art')){
          if(options !== ''){
            $(val).html('')
            $(val).html(options)
          }else{
            $(val).prop('selectedIndex', 0);
            $(val).change()
          }
        }else if($(val).hasClass('ubiOri')){
          $(val).html('')
          $(val).html('<option value="" selected>Ubicacion Origen</option>')
        }else if($(val).hasClass('ubiDes')){
          $(val).html('')
          $(val).html('<option value="" selected>Ubicacion Destino</option>')
        }else if($(val).hasClass('cambios')){
          $(val).html('')
          $(val).html('<option value="N/D" selected>Sin talla</option>')
        }
      }else if(val.nodeName === 'INPUT'){
        if($(val).hasClass('toEmpty'))$(val).val('')
        else if($(val).hasClass('toCero'))$(val).val('0')
      }
    });
}


function sendMsj() {
    $('.sendMail').click(function(e){
        e.preventDefault();
          var url = $(this).attr('href');
          var row  = $(this).closest('tr')
          Swal.fire({
          title: 'Seguro que Desea Aprobar?',
          text: "Esta accion no se puede revertir",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si!, aprobar'
        }).then((result) => {
          console.log(result)
          if (result.value) {
            $.get(url,function(result){
              if(result.exito){
                changeStatus(row,'A')
                Swal.fire(
                  'Movimiento Aprobado!',
                  'Con Exito.',
                  'success',
                )
              }else if(result.error){
                Swal.fire(
                  'Error en ejecucion',
                    result.error,
                  'error',
                )
              }else if(result.codubi === 'notEqual'){
                Swal.fire({
                  title: 'Seguro que Desea Aprobar?',
                  html : result.html,
                  text: "Hubo un error con el Beta ",
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si!, aprobar'
                })
              }
            })
          }else{
            Swal.fire({
            icon: 'warning',
            title: 'Accion Cancelada',
            //footer: '<a href>Why do I have this issue?</a>'
            timer:1500
          })
          }
        });
    });
}

$(document).ready(function () {
  var tallasArt = [];
  var Grid = []
  save();
  clearLine()
  cleanCant()
  $('select.select2art').select2({
    width:'350px'
  });

  $('select.cambios').select2({
    width:'250px'
  });

   $('select.select2form').select2({
    /* width:'150%' */
  });
   $('select.select2movori').select2({
    /* width:'150%' */
  });

  $('select.select2movdes').select2({
    /* width:'150%' */
  });

  function clearLine(){
    $(document).on('click','.cleanLine',function (e) {
      e.preventDefault();
      let options = ''
      let elementos = $(this).closest('tr').find('input.fillable ,select.fillable');
        $.each(elementos, function (index, val) { 
          if(val.nodeName === 'SELECT') {
            if($(val).hasClass('select2art')){
                if(options !== ''){
                  $(val).html('')
                  $(val).html(options)
                }else{
                  $(val).prop('selectedIndex', 0);
                  $(val).change()
                }
            }else if($(val).hasClass('ubiOri')){
              $(val).html('')
              $(val).html('<option value="" selected>Ubicacion Origen</option>')
            }else if($(val).hasClass('ubiDes')){
              $(val).html('')
              $(val).html('<option value="" selected>Ubicacion Destino</option>')
            }else if($(val).hasClass('cambios')){
              $(val).html('')
              $(val).html('<option value="" selected>Sin talla</option>')
            }
          }else if(val.nodeName === 'INPUT'){
            if($(val).hasClass('toEmpty'))$(val).val('')
            else if($(val).hasClass('toCero'))$(val).val('0')
          }
        });
    })
  }

  function swalError(msj) {
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: msj+'!',
      /* footer: '<a href>Why do I have this issue?</a>' */
    })
  }

  const validateE = function() {

    let almori= document.getElementById('almori')
    let tiptras= document.getElementById('tiptras')
    let almdes = document.getElementById('almdes')
    let obstra = document.getElementById('obstra')
    if(
      !almori||
      !tiptras||
      !almdes||
      !obstra
      ){
      swalError("Ingrese todos los campos");
      return false
    }
    return true;
  }

  const validInput = function (elem) {//validamos el tipo de elemento y si tiene un valor insertado
      let valor = $(elem).val()
      let tipoElem = typeof $(elem).val()
      if($(elem).hasClass('required') && !valor){
        return false
      }else if($(elem).hasClass('required') && tipoElem == 'object'){//si el elemento va a recibir varios campos entonces se busca si internamente ese objeto es length == 0 Aplica para( MULTISELECT)
        if(valor.length === 0){
          return false
        }
      }else if($(elem).hasClass('inputCant')){
        if(isNaN($(elem).val())) return false
      }
      return true
  }

  function save() {
    $(document).on('click','#guardar',function (e) {
      $(this).prop('disabled',true)
      Grid = []
      tallasArt = []// VARIABLE GLOBAL PARA VALIDAR LOS ARTICULOS
      e.preventDefault();
      if(!validateE()) return;
      $('#errorart').val('')
      let totalrows = $('#table > tbody >tr');
      let n = 0
      let array = []
      let validarray = []
      for (let i = 0; i < totalrows.length; i++) {
        n = 0
        let x = totalrows.eq(i).find('input.fillable , select.fillable');
            for (let h = 0; h < x.length; h++) {
              if(!validInput(x.eq(h))) n++//SI LA FUNCION DE VALIDACION ES FALSE ENTONCES SUMAMOS LOS CAMPOS
            }
            if(n === 0){//SI NO HAY NINGUN CAMPO MISSING ENTONCES AÑADE A VALIDAR AL ARRAY
              validarray.push(i)
              $('#validart').val(validarray)
              let codtalla = pushArtTalla(x.eq(0),x.eq(1).val())//manda los inputs para obtener el articulo y las tallas
              x.eq(5).val(codtalla)
            }else if(n !== 0 && n !== 4){//si la bandera esta entre 3 y 0 entonces me indica que hay campos incompletos
              array.push(parseInt(i)+parseInt(1))//manda el numero de fila
              $('#errorart').val(array)
            }
      }//FOR

      if($('#validart').val()=== '') {// validacion de que exista al menos un articulo en el grid
        swalError("Debe insertar al menos un articulo")
        $(this).prop('disabled',false)
        return
      } //error
      if($('#errorart').val() !== ''){
        let errores = $('#errorart').val()
        let beta = errores.indexOf(',') !== -1 ? errores.split(',') : parseInt(errores);
        if(Array.isArray(beta)){
          swalError("Filas "+beta.toString()+" contienen Campos vacios ");
          $(this).prop('disabled',false)
          return
        }
        errores = parseInt(errores)
        swalError("La fila "+errores+" contiene campos vacios")
        $(this).prop('disabled',false)
        return
      }
      /* funcion filter javascript  para filtrar repetidos*/
      let repetidos=tallasArt.filter (
          (value,pos,self) => {
            /* value => valor , pos => indice , self => array a filtrar */
              return self.slice(pos+1).indexOf(value) >= 0 && pos === self.indexOf(value);
          }
      );
      let error = validTallas(repetidos)
      $('#tallasArt').val(tallasArt)
      if(error) {//valida que no existan articulos repetidos en el grid
        var string = ' Articulo'
        for (const key in error) {
          if (error[key].hasOwnProperty('botas') && error[key].hasOwnProperty('talla')) {
            string += error[key].botas
            string += ' se encuentra repetido '
            string += error[key].talla !== null ? 'con la talla :'+error[key].talla.split('-')[1]+' ' : ''
          }
        }
        swalError(string)
        $(this).prop('disabled',false)
        return
      }
    $('#saveform').submit() 
    })
  }

  function validTallas(repetidos){//retorna objeto con botas y talla por separado por si encuentra un repetido
      if (!repetidos.length > 0) return null
      let msjerror = repetidos.map((elem,index,self) => {
        return {botas:elem.split('+')[0],talla:elem.split('+')[1] === '0' ?  null : elem.split('+')[1]}
      })


      return msjerror.length > 0 ? msjerror : null
  }

  function pushArtTalla(articulo,tallas){//articulo elemento donde esta el articulo- tallas elemento del select de tallas
    let nombreart = articulo.find('option:selected').text()
    /* busca las tallas en el select multiple y las concatena con el articulo */
    articulo = articulo.val()//valor del selec de articulo
    var tallasBeta = []//array a retornar
    if(tallas.length > 0){//si hay alguna talla seleccionada entonces se inserta 
      tallas.forEach(element => {
        tallasArt.push(nombreart+'+'+element)//se concatena el articulo+talla ejem B:123+46 
        tallasBeta.push(element)
      });
    }else{// si no hay talla seleccionada por que pueden haber articulos sin talla
      tallasBeta.push('N/D')
      tallasArt.push(articulo+'+'+'N/D')
    }
    return tallasBeta//retorna un array con el articulo y tallas de un mismo row
  }

  function cleanCant(){
    $(document).on('change','.cambios', function(){
      let elem = $(this).closest('tr').find('input.inputCant')
      $(elem).val('')
    })
  }

});