$(document).ready(function() {
	
  $('.borrar').click(function(e){
          e.preventDefault();
            Swal.fire({
            title: '¿Seguro que desea eliminar este Articulos?',
            text: "¡Despues de esto no se encontrara en la lista!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar'
          }).then((result) => {
              if (result.value == true) {

                
              var row = $(this).parents('tr');
              //var form = $(this).parents('data-id');
              var url = $(this).attr('href');
              $.get(url,row,function(result1) {
                  
                if (result1.titulo === 'SE HA ELIMINADO CON EXITO') {

                  
                      Swal.fire(
                        'SE HA ELIMINADO CON EXITO',
                        '.',
                        'success',

                      );

                      row.fadeOut();
                }

                                           
                 

              })   
              }  //para cancelar el sweetalert2
          });

  });
});