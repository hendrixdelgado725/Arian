$(document).ready(function() {
	
    $('.borrar').click(function(e){
            e.preventDefault();
              Swal.fire({
              title: '¿Seguro que desea eliminar este centro?',
              text: "¡Despues de esto no se encontrara en la lista!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, eliminar'
            }).then((result) => {
                if (result.value == true) {
                  
                var row = $(this).parents('tr');
                //var form = $(this).parents('data-id');
                var url = $(this).attr('href');
                $.get(url,row,function(result1) {
                    
                  if (result1.titulo === 'Error eliminando') {
  
                    
                        Swal.fire(
                          'Error',
                          'Se encontró un error eliminando el centro de costo',
                          'warning',
  
                        );
                  }else if(result1.titulo === 'Eliminado con exito'){
  
                          Swal.fire(
                          'Hecho',
                          'Se eliminó exitosamente',
                          'success',
  
                        );
                          row.fadeOut();
                  }
  
                                             
                   
  
                })   
                }else  console.log(result.dismiss);    //para cancelar el sweetalert2
            });
  
    });
  });