$(function () {
  
  putartssondelete();
  tasasc();
  // arts();

  function putartssondelete() {
    var borrados = [];
    $(document).on('click', '.deleterow', function (e) {
      e.preventDefault();
      let articulos = JSON.parse($('#arttojson').val());
      let descuentos = JSON.parse($('#descutojson').val());
      // let articulos = $('#arttojson').val();
      let content = '<option selected disabled> Seleccione un Articulo</option>';
      let content2 = '<input> </input>';
      let content3 = '<a></a>'
      let content4 = '<option value="0" selected>SIN DESCUENTO</option>'
      let content5 = '<option disabled selected>SELECCIONE UNA TALLA</option>'
      let select = $(this).closest('tr').find('select.articulo');
      let select2 = $(this).closest('tr').find('select.descuento');
      let select3 = $(this).closest('tr').find('select.tallas');
      let input = $(this).closest('tr').find('input.cantidad');
      let a = $(this).closest('tr').find('a.deleterow');
      console.log($(e.relatedTarget).data('target-artid'))
      var id = $(this).closest('tr').find('input.id').val();


      borrados.push(id);
          

      console.log(borrados)
      console.log(articulos, select)
      $.each(articulos, function (index, val) {
        content = content.concat(
          '<option value="' + val.desart + '" >' + val.desart + '</option>'
        );
      })
      $.each(descuentos, function (index, val) {
        content4 = content4.concat(
          '<option value="' + val.desdesc + '" >' + val.desdesc + '</option>'
        );
      })

      $(select).html(content);
      $(input).html(content2);
      $(a).html(content3);
      $(select2).html(content4);
      $(select3).html(content5);

      $(this).closest('tr').find('input.cantidad');
      $(input).val('');
      $('#borrados').val(borrados);
    })
  };
  
  function tasasc() {
    $(document).on('change', '#moneda', function (e) {
      e.preventDefault();
      let url = $(this).data('href')
      $.ajax({
        type: 'GET',
        url: url,
        data: {
            moneda: $('select[name=moneda]').val(),
        },
        success: function (result) {
        let content = '<option selected disabled>SELECCIONE UNA TASACIÓN</option>';
        result =  JSON.stringify(result);
          let tasas = JSON.parse(result);
          let select = $('#tasas');
          let select2 = document.getElementById("tasa");
          // console.log(tasas);
          if(tasas !== null){
           console.log(tasas.moneda)
           content = content.concat(
               '<option value="'+tasas.id+'">' + "1 "+ tasas.moneda.nombre + " = " +tasas.valor+" "+tasas.moneda2.nomenclatura +'</option>'
             );
          }
            //  $.each(tasas,function(index,val){
            //   content = content.concat(
            //   '<option value="'+val.id+'">' + "1 "+ val.moneda.nombre + " = " +val.valor+" "+val.moneda2.nomenclatura +'</option>'
            //   );
            //   console.log(content)
            //  })
          
          //$(select).html(content);
          $(select2).html(content);
         // $('#tasas').html(content);
          console.log(select)
         }
      });    
    })
  }

     $(document).on('click','#aprobar',function(e){
            e.preventDefault();
              Swal.fire({
              title: '¿DESEA APROBAR ESTE PRESUPUESTO?',
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'SI, APROBAR',
              cancelButtonText: 'NO',
            }).then((result) => {
                if (result.value == true) {
                var a1 = $(this);
                var row = $(this).parents('tr');
                var td = $(this).parents('td');
               /* var a2 = $(this).closest('a').find('a.anular');
                console.log(a1)
                console.log(a2)*/
         
                var td2 = td.prev();
                var html = 'APROBADO';
                td2.text(html);
            
                var url = $(this).attr('href');
                var enc = $(this).data('target-enc');

                 $.ajax({
                  type: 'GET',
                  url: url,
                    data: {
          /*          codpre: id,*/
                    pre: enc
                     },
                success: function (result) { 
                  if(result.exito){
                     $(a1).hide();
                   /*  console.log($(this))*/
                    /* $(td).parents('td').find('td.anular').show()*/
                     Swal.fire(
                          'HECHO',
                          'EL PRESUPUESTO HA SIDO APROBADO',
                          'success',
                        ).then(
                          function(){
                            location.reload();
                          }
                        );
                  }
                  else{
                       Swal.fire(
                          'ERROR',
                          'HA SURGIDO UN PROBLEMA',
                          'warning',
                        ).then(
                          function(){
                            location.reload();
                          }
                        );
                  }
                     
                  }//sucess
               })//ajax
            };
          })
   })

      $(document).on('click','#anular',function(e){
            e.preventDefault();
              Swal.fire({
              title: '¿DESEA ANULAR ESTE PRESUPUESTO?',
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'SI, ANULAR',
              cancelButtonText: 'NO',
            }).then((result) => {
                if (result.value == true) {
                var a1 = $(this);
                var row = $(this).parents('tr');
                var td = $(this).parents('td');
               /* var a2 = $(this).closest('a').find('a.anular');
                console.log(a1)
                console.log(a2)*/
         
                var td2 = td.prev();
                var html = 'ANULADO';
                td2.text(html);
            
                var url = $(this).attr('href');
                var enc = $(this).data('target-enc');
                
                 $.ajax({
                  type: 'GET',
                  url: url,
                    data: {
          /*          codpre: id,*/
                    pre: enc
                     },
                success: function (result) { 
                  if(result.exito){
                     $(a1).hide();
                   /*  console.log($(this))*/
                    /* $(td).parents('td').find('td.anular').show()*/
                     Swal.fire(
                          'HECHO',
                          'EL PRESUPUESTO HA SIDO ANULADO',
                          'success',
                        ).then(
                          function(){
                            location.reload();
                          }
                        );;
                  }
                  else{
                       Swal.fire(
                          'ERROR',
                          'HA SURGIDO UN PROBLEMA',
                          'warning',
                        ).then(
                          function(){
                            location.reload();
                          }
                        );;
                  }
                     
                  }//sucess
               })//ajax
            };
          })
   })

  // function arts() {
  //   $(document).on('change', '#moneda', function (e) {
  //     e.preventDefault();
  //     // let url = $('.getarts').data('href')
  //     let url = $(this).data('href2')
  //     console.log(url)
  //     $.ajax({
  //       type: 'GET',
  //       url: url,
  //       data: {
  //           moneda: $('select[name=moneda]').val(),
  //       },
  //       success: function (result) {
  //         // $('#list-articulos').replaceWith(result);

  //         let content = '<option selected disabled> Seleccione un Articulo</option>';
  //         // let content2 = '<input> </input>';
  //         // let content3 = '<option value="0" selected>SIN DESCUENTO</option>'
  //         // let select = $(this).closest('tr').find('select.articulo');
  //         // let select2 = $(this).closest('tr').find('select.descuento');
  //         // let input = $(this).closest('tr').find('input.cantidad');

  //         result =  JSON.stringify(result);
  //         let articulos = JSON.parse(result);
  //         let select = $('#articulos');
  //            $.each(articulos,function(index,val){
  //             content = content.concat(
  //               '<option value="' + val.desart + '" >' + val.desart + '</option>'
  //             );
  //             console.log(content)
  //            })
          
  //            $.each($('#articulos'),function(index,val){
  //             val.html(content);
  //             // console.log(content)
  //            })
          
  //           //  $(select).html(content);
  //         // $(select2).html(content);
  //         console.log(select)
  //        }
  //     });    
  //   })
  // }

    $(document).on('change','.articulos',function(e){
      e.preventDefault()
      console.log($(this).val())

      let url = $(this).data('href')
      let select = $(this)
      $.ajax({
        type: 'GET',
        url: url,
        data: {
            articulo: $(this).val(),
        },
        success: function (result) {
          console.log('aca 1',result)
        let content = '<option selected disabled>SELECCIONE UNA TALLA</option>';
 /*       result =  JSON.stringify(result);*/
       /* let tallas = JSON.parse(result);*/
        var td = $(select).parents('td');
        /*var td2 = td.closest('td').find('td.talla')*/
        /*var selectt = td2.closest('select')
        let select2 = $(select).closest('select').find('select.tallas');*/
        var td2 = td.next();
  /*      console.log(td2)*/
        let select2 = $(td2).find('select')
 /*       console.log(select2)*/
       
    /*       $.each(tallas,function(index,val){
              content = content.concat(
              '<option value="'+val.tallas+'">' + val.tallas + '</option>'
              );
              console.log(content)
             })
*/        
         if(result.tallas){
          console.log('if')
            $.each(result.tallas, function (index, val) {    
             console.log('val',val[0])
               if(val[0]!= null){
                 content = content.concat(
                 '<option value="' + val[0].codtallas + '" >' + val[0].tallas + '</option>'
                 );
                }
              })
            }
            else if(result = 'n/a'){
               content = content.concat(
                 '<option value="A-N/A" > N/A </option>'
                 );
            }
        
        
          $(select2).html(content);
         }
      });   
      

    })
     $(document).on('change','.articulo',function(e){
      e.preventDefault()
      console.log('hi');
      console.log($(this).val())

      let url = $(this).data('href')
      let select = $(this)
      $.ajax({
        type: 'GET',
        url: url,
        data: {
            articulo: $(this).val(),
        },
        success: function (result) {
          console.log('aca 2',result)
        let content = '<option selected disabled>SELECCIONE UNA TALLA</option>';
      /*  result =  JSON.stringify(result);*/
      /*  let tallas = JSON.parse(result);*/
        var td = $(select).parents('td');
        /*var td2 = td.closest('td').find('td.talla')*/
        /*var selectt = td2.closest('select')
        let select2 = $(select).closest('select').find('select.tallas');*/
        var td2 = td.next();
  /*      console.log(td2)*/
        let select2 = $(td2).find('select')
 /*       console.log(select2)*/
       
          if(result.tallas){
            $.each(result.tallas, function (index, val) {    
             console.log('val',val[0])
               if(val[0]!= null){
                 content = content.concat(
                 '<option value="' + val[0].codtallas + '" >' + val[0].tallas + '</option>'
                 );
                }
               else{
                content = content.concat(
                  '<option value="NADA">N/A</option>'
                  )
                 } 
              })
            }
        
          $(select2).html(content);
         }
      });   
      

    })
  
});