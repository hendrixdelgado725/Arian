

$(function(){
    setnumcta();
    putbanksondelete();
    //onedit();
      function setnumcta(){
        $(document).on('change','select.entidad',function(){
          let input = $(this).closest('tr').find('input.numcuenta');//busca el input en el mismo tr
          let num = $(this).find('option:selected').data('num');//busca el objeto option:selected del select que cambio
          input.val(num)
        })
      }
      function putbanksondelete(){
        $(document).on('click','.deleterow',function(e){
          e.preventDefault();
          let bancos =  JSON.parse($('#banktojson').val());
          let content= '<option selected> Seleccione un Banco</option>';
          let select = $(this).closest('tr').find('select.entidad');
          $.each(bancos,function(index,val){
            content= content.concat(
              '<option value="'+val.nombanc+'" data-num="'+val.num+'" >'+val.nombanc+' / '+val.num+'</option>'
            );
          })
            $(select).html(content);
          let input = $(this).closest('tr').find('input.numcuenta');
          $(input).val('')
        })
      }

});