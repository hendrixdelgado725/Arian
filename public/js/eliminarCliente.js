$(document).ready(function() {
	
    $('.borrar').click(function(e){
            e.preventDefault();
              Swal.fire({
              title: '¿Seguro que desea eliminar este cliente?',
              text: "¡Despues de esto no se encontrara en la lista!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, eliminar'
            }).then((result) => {
                if (result.value == true) {

                  
                var row = $(this).parents('tr');
                //var form = $(this).parents('data-id');
                var url = $(this).attr('href');
                $.get(url,row,function(result1) {
                    
                  if (result1.titulo === 'error Este cliente no puede ser eliminado') {

                    
                        Swal.fire(
                          'Error',
                          'Este cliente no puede ser eliminado',
                          'warning',

                        );
                  }else if(result1.titulo === 'Exito se elimino'){

                          Swal.fire(
                          'Hecho',
                          'Se elimino exitosamente',
                          'success',

                        );
                          row.fadeOut();
                  }

                                             
                   

                })   
                }else {
                      Swal.fire(
                          'No se Elimino',
                          '.',
                          'warning',

                        );
                }    
            });

    });
});