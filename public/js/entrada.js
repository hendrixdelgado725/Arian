
$(document).ready(function () {
  var Grid = [];
  var tallasArt = [];
  var requiredFields = [];
  save();
  changeFilledUbi()
  clearLine()
  saveProvee()
  cleanCant()

  
  // inicializamos el plugin
  $('select.cambios').select2({
    width:'200px'
  });

  $('select.select3').select2({
  });

  $('select.select2').select2({
  });

  $('select.select4').select2({
    width:'200px'

  });

  $('select.select2art').select2({
    width:'250px'
  });

   $('select.select2mov').select2({
    /* width:'150%' */
  });

  function clearLine(elemento = '',options = ''){
    $(document).on('click','.cleanLine',function (e) {
      e.preventDefault();
        var elementos = $(this).closest('tr').find('input.fillable ,select.fillable');
      $.each(elementos, function (index, val) { 
        
        if(val.nodeName === 'SELECT') {
          if($(val).hasClass('select2art')){
              $(val).prop('selectedIndex', 0);
              $(val).change()
          }else if($(val).hasClass('codubi')){
            $(val).html('')
            $(val).html('<option value="" selected>UBICACION</option>')
            
          }else if($(val).hasClass('cambios')){
            $(val).html('')
            $(val).html('<option value="N/D" selected>Sin talla</option>')
          }
        }else if(val.nodeName === 'INPUT'){
          if($(val).hasClass('toEmpty'))$(val).val('')
          else if($(val).hasClass('toCero'))$(val).val('0')
      }
      });
    })
  }


  function swalError(msj,type = 'error',title = 'Oops...') {
    Swal.fire({
      icon: type,
      title: title,
      text: msj+'!',
      /* footer: '<a href>Why do I have this issue?</a>' */
    })
  }

  function changeFilledUbi(){//al cambiar la ubicacion se pone la cantidad existente en la tabla
    $(document).on('change','.select2mov',function (e) {
      console.log("beta")
      let valor = $(this).find('option:selected').data('exiact')
      valor = valor === undefined ? 0 : valor
      $(this).closest('tr').find('input.inputCant').attr("max",valor)
    })
  }

  function validateE() {

    let movimiento= document.getElementById('movinvent')
    let almacen= document.getElementById('codalm')
    let obervacion = document.getElementById('obsent')
    let nrocontro = document.getElementById('nrocontro')
    let codpro = document.getElementById('codpro')
    let fecfac = document.getElementById('fecfac')
    // let obervacion = document.getElementById('obsent').value
    codpro.value
    console.log("validateE -> codpro.value", codpro.value)

    /**|| !almacen.value || !obervacion.value || !nrocontro.value ||
        // !codpro.value 
        //|| 
        !fecfac.value */
    if(!movimiento.value){
      swalError("Ingrese en el campo movimiento");
      return false
    }else if(!almacen.value){
      swalError("Ingrese en el campo almacen");
      return false
    }else if(!obervacion.value){
      swalError("Ingrese en el campo observacion");
      return false
    }else if(!nrocontro.value){
      swalError("Ingrese en el campo referencia factura");
      return false
    }else if(!codpro.value){
      swalError("Ingrese en el campo referencia proveedor");
      return false
    }

    return true;
  }

  const validInput = function (elem) {//validamos el tipo de elemento y si tiene un valor insertado
      let valor = $(elem).val()
      let tipoElem = typeof $(elem).val()
      if($(elem).hasClass('required') && !valor){
        return false
      }else if($(elem).hasClass('required') && tipoElem == 'object'){//si el elemento va a recibir varios campos entonces se busca si internamente ese objeto es length == 0 Aplica para( MULTISELECT)
        if(valor.length === 0){
          return false
        }
      }
      return true
  }

  function save() {
    $(document).on('click','#guardar',function (e) {
      $(this).prop('disabled',true)
      Grid = []
      tallasArt = []// VARIABLE GLOBAL PARA VALIDAR LOS ARTICULOS
      e.preventDefault();
      //if(!validateE()) return;
      $('#errorart').val('')
      let totalrows = $('#table > tbody >tr');
      let n = 0
      let array = []
      let validarray = []
      for (let i = 0; i < totalrows.length; i++) {
        n = 0
        let x = totalrows.eq(i).find('input.fillable , select.fillable');
            for (let h = 0; h < x.length; h++) {
              if(!validInput(x.eq(h))) n++//SI LA FUNCION DE VALIDACION ES FALSE ENTONCES SUMAMOS LOS CAMPOS
            }
            if(n === 0){//SI NO HAY NINGUN CAMPO MISSING ENTONCES AÑADE A VALIDAR AL ARRAY
              validarray.push(i)
              $('#validart').val(validarray)
              let codtalla = pushArtTalla(x.eq(0),x.eq(1).val())//manda los inputs para obtener el articulo y las tallas
              x.eq(5).val(codtalla)
            }else if(n !== 0 && n !== 3){//si la bandera esta entre 3 y 0 entonces me indica que hay campos incompletos
              array.push(parseInt(i)+parseInt(1))//manda el numero de fila
              $('#errorart').val(array)
            }
      }//FOR
     /* if($('#validart').val()=== '') {// validacion de que exista al menos un articulo en el grid
        swalError("Debe insertar al menos un articulo")
        console.log($('#validart').val())
        $(this).prop('disabled',false)
        return

      } //error
      if($('#errorart').val() !== ''){
        let errores = $('#errorart').val()
        let beta = errores.indexOf(',') !== -1 ? errores.split(',') : parseInt(errores);
        if(Array.isArray(beta)){
          swalError("Filas "+beta.toString()+" contienen Campos vacios ");
          $(this).prop('disabled',false)
          return
        }
        errores = parseInt(errores)
        swalError("La fila "+errores+" contiene campos vacios")
        $(this).prop('disabled',false)
        return
      }
      
      let repetidos=tallasArt.filter (
          (value,pos,self) => {
           
              return self.slice(pos+1).indexOf(value) >= 0 && pos === self.indexOf(value);
          }
      );
      let error = validTallas(repetidos)
      $('#tallasArt').val(tallasArt)
      if(error) {//valida que no existan articulos repetidos en el grid
        var string = ' Articulo'
        for (const key in error) {
          if (error[key].hasOwnProperty('botas') && error[key].hasOwnProperty('talla')) {
            string += error[key].botas
            string += ' se encuentra repetido '
            string += error[key].talla !== null ? 'con la talla :'+error[key].talla.split('-')[1]+' ' : ''
          }
        }
        swalError(string)
        $(this).prop('disabled',false)
        return
      }*/

    $('#saveform').submit() 
    })
  }

  function validTallas(repetidos){//crea el mensaje de error
      if (!repetidos.length > 0) return null
      let msjerror = repetidos.map((elem,index,self) => {
        return {botas:elem.split('+')[0],talla:elem.split('+')[1] === 'N/D' ?  null : elem.split('+')[1]}
      })
      console.log(msjerror.length)
      return msjerror.length > 0 ? msjerror : null
  }

  function pushArtTalla(articulo,tallas){
   /* let nombreart = articulo.find('option:selected').text()
    articulo = articulo.val()
    var tallasBeta = []
    if(tallas.length > 0){
      tallas.forEach(element => {
        tallasArt.push(nombreart+'+'+element)
        tallasBeta.push(element)
      });
    }else{
      tallasBeta.push('N/D')
      tallasArt.push(articulo+'+'+'N/D')
    }
    obj = {}
    obj["articulo"] = articulo
    obj["tallas"] = tallas
    return tallasBeta*/
  }

  function saveProvee (){
    $(document).on('submit','#saveProvee',function(e){
      e.preventDefault()
      let data ={
        _token : $("input[name='_token']").val(),
        nompro : $('#nompro').val(),
        tipodoc : $('#tipodoc').val(),
        rifpro : $('#rifpro').val(),
        ajax:4
      }
      $.ajax({
        type: "POST",
        url: "ajax",
        data: data,
        dataType: "json",
        success: function (json) {
          if(json.error){
            swalError(json.error,)
            return
          }else{
            swalError(json.success,'success','Hecho!')
            $('#codpro').append(`<option value=${json.pro.codpro}>${json.pro.nompro} RIF: ${json.pro.rifpro}</option>`);
            $('#modal-proveedor').modal('hide')
          }
        }
      });
    })

  }

  function cleanCant(){
    $(document).on('change','.tallas', function(){
      let elem = $(this).closest('tr').find('input.inputCant')
      $(elem).val('')
    })
  }


});
//pausado

function calculo(a,pos){
    
  let arrayP =  Array.prototype.slice.call(document.getElementsByClassName('precio1'),0);
  let arrayC =  Array.prototype.slice.call(document.getElementsByClassName('cantidad1'),0);
  let resultado = arrayP.map((val,index) => val.value * arrayC[index]['value'],0).reduce((a,b) => a+b,0);

  $('#monrcp').val(resultado);


}