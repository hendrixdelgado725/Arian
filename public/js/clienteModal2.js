$(document).ready(function() {
	
    $('#guardarCliente').click(function (e) {
        e.preventDefault();



        let url = $(this).data('href')
        $.ajax({
            type: 'GET',
            url: url,
            data: {
                tipodocumento: $('select[name=tipodocumento]').val(),
                cedularif: $('input[name=cedularif]').val(),
                nombre: $('input[name=nombre]').val(),
                direccion: $('textarea[name=direccion]').val(),
                telefono: $('input[name=telefono]').val(),
                email: $('input[name=email]').val(),
            },
            success: function (result) {
                $('#nuevocliente').modal('hide');
                document.getElementById("formCliente").reset();
                $('#clienttojson').val(JSON.stringify(result));
                let content = '<option selected disabled> SELECCIONE UN CLIENTE</option>';
                let select = $('#clientes')
                let clientes = JSON.parse($('#clienttojson').val());
                // console.log(clientes)
                
                $.each(clientes,function(index,val){
                    content = content.concat(
                      '<option value="' + val.nompro + '" >' + val.nompro + '</option>'
                    );
                })
                $(select).html(content);

            }
            
        })
    });

    $('#guardarClienteF').click(function (e) {
        e.preventDefault();
        let url = $(this).data('href')
        
        $.ajax({
            type: 'GET',
            url: url,
            data: {
                tipodocumento: $('#tipodocumento2').val(),
                cedularif: $('input[name=cedularifmodal]').val(),
                nombre: $('input[name=nombremodal]').val(),
                direccion: $('textarea[name=direccionmodal]').val(),
                telefono: $('input[name=telefono]').val(),
                email: $('input[name=email]').val(),
            },
            success: function (result) {
                let cedula = $('#cedularifmodal').val()
                let nombre = $('#nombremodal').val()
                let content = '<textarea> </textarea>';
                $('#direccion').html(content);
                $('#tipodocumento option[value='+JSON.stringify($('#tipodocumento2').val())+']').attr("selected",true); //cambio el select del cliente

                let direccion = $('#direccionmodal').val()
                $('#nombre').val(nombre);
                $('#documento').val(cedula);
                $('#direccion').val(direccion);
                $('#nuevocliente').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                document.getElementById("formCliente").reset();

                $('#clienttojson').val(JSON.stringify(result));
                let content2 = '<option selected disabled> BUSCAR CLIENTE</option>';
                let select = $('#clientes')
                let clientes = JSON.parse($('#clienttojson').val());
                // console.log(clientes)
                
                //

                $.each(clientes,function(index,val){
                    content2 = content2.concat(
                      '<option value="' + val.codpro + '" >' + val.codpro+" / "+val.nompro + '</option>'
                    );
                })
                $(select).html(content2);

 
                // $('#clienttojson').val(JSON.stringify(result));
                // let content = '<option selected disabled> SELECCIONE UN CLIENTE</option>';
                // let select = $('#clientes')
                // let clientes = JSON.parse($('#clienttojson').val());
                // // console.log(clientes)
                
                // $.each(clientes,function(index,val){
                //     content = content.concat(
                //       '<option value="' + val.nompro + '" >' + val.nompro + '</option>'
                //     );
                // })
                // $(select).html(content);

            }
            
        })
    });


});