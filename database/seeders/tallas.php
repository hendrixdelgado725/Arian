<?php

use Illuminate\Database\Seeder;

class tallas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tallas = \App\Tallas::insert(['tallas' => '40','codtallas' => 'A-40']);
        $tallas = \App\Tallas::insert(['tallas' => '41','codtallas' => 'A-41']);
        $tallas = \App\Tallas::insert(['tallas' => '42','codtallas' => 'A-42']);
        $tallas = \App\Tallas::insert(['tallas' => '43','codtallas' => 'A-43']);
        $tallas = \App\Tallas::insert(['tallas' => '44','codtallas' => 'A-44']);
        $tallas = \App\Tallas::insert(['tallas' => '45','codtallas' => 'A-45']);
    }
}
