<?php

use Illuminate\Database\Seeder;
use App\permission_users;
class permission_user extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //este es una version de prueba con los datos que se utilizo en el modulo de permisologia
    	permission_users::insert(['codpermission' => 'contenidos.index','codusuarios' => '267','rutasaccesso' => '/modulo/configuracionGenerales/DdeEmpresa']);

    }
}
