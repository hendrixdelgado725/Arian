<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TipoFinanciamientoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Fortipfin::create([
            'codfin'=>'0001',
            'nomext'=>'INGRESOS PROPIOS',
            'nomabr'=>'ORD',
            'tipfin'=>'O',
            'montoing'=> 0.00,
            'montodis'    => 0.00,
            'montodisaux'  => 0.00,       
        ]);
        Fortipfin::create([
            'codfin'=>'0002',
            'nomext'=>'EXTRAORDINARIO',
            'nomabr'=>'EXORD',
            'tipfin'=>'E',
            'montoing'=> 0.00,
            'montodis'    => 0.00,
            'montodisaux'  => 0.00,    
        ]);
    }
}
