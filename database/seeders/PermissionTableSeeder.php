<?php

use Illuminate\Database\Seeder;
use App\permission;
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
 
        //contenido

         permission::create([
        	'name' => 'Navegar contenidos',
        	'slug' => 'contenidos.index',
        	'description' => 'Lista y navega todos los contenido del sistema'
        ]);


        permission::create([
        	'name' => 'Editar contenidos',
        	'slug' => 'contenidos.edit',
        	'description' => 'Editar los contenido del sistema'
        ]);

        permission::create([
        	'name' => 'Eliminar contenidos del sistema',
        	'slug' => 'contenidos.delete',
        	'description' => 'Eliminar los contenido del sistema'
        ]);


        permission::create([
        	'name' => 'creacion del contenidos del sistema',
        	'slug' => 'contenidos.create',
        	'description' => 'Crear los contenido del sistema'
        ]);
    }
}
