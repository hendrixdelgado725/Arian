<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Carbon\Carbon;
class Roles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = \App\rolesusuarios::insert([
        	'nombre'      => 'ADMIN',
        	'codroles'    => 'R-001',
            'description' => 'Administrador',
            'special'     => 'all-access',
            'codsuc_reg'  => 'SUC-0001',
            'codigoid'    => '1-0002',
            'created_at'  => Carbon::now()
        ]);
        
        $roles = \App\rolesusuarios::insert([
        	'nombre'      => 'GERENTE',
        	'codroles'    => 'R-002',
            'description' => 'Gerente',
            'special'     => 'false',
            'codsuc_reg'  => 'SUC-0001',
            'codigoid'    => '2-0002',
            'created_at'  => Carbon::now()
        ]);

        $roles = \App\rolesusuarios::insert([
        	'nombre'      => 'EMPLEADO',
        	'codroles'    => 'R-003',
            'description' => 'Empleado',
            'codsuc_reg'  => 'SUC-0001',
            'special'     => 'false',
            'codigoid'    => '3-0002',
            'created_at'  => Carbon::now()

        ]);

        $roles = \App\rolesusuarios::insert([
        	'nombre'      => 'JEFE',
        	'codroles'    => 'R-004',
            'description' => 'Jefe',
            'codsuc_reg'  => 'SUC-0001',
            'special'     => 'false',
            'codigoid'    => '4-0002',
            'created_at'  => Carbon::now()

        ]);

        $roles = \App\rolesusuarios::insert([
            'nombre'      => 'VACIO',
            'codroles'    => 'R-000',
            'description' => 'VACIO',
            'special'     => 'false',
            'codsuc_reg'  => 'SUC-0001',
            'codigoid'    => '5-0002',
            'created_at'  => Carbon::now()

        ]);
    }
}
