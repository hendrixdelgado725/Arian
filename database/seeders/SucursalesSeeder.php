<?php

namespace Database\Seeders;
use App\Sucursal;
use Illuminate\Database\Seeder;

class SucursalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Sucursal::create([
        'nomsucu' => 'PRINCIPAL',
        'dirfis' => 'PRINCIPAL',
        'codpai' => '1',
        'status'=>'A',
        'codedo' => '1',
        'codalm' => 'A',
        'codrif' => '01',
        'codempre'=>'G200093811',
        'codsuc'    => 'SUC-0001',
        'codigoid'  => '1-0001',
       ]);

       Sucursal::create([
        'nomsucu' => 'PARAGUANA',
        'dirfis' => 'PARAGUANA',
        'codpai' => '1',
        'status'=>'A',
        'codedo' => '4',
        'codalm' => 'A',
        'codrif' => '2345678',
        'codempre'=>'G200093811',
        'codsuc'    => 'SUC-0002',
        'codigoid'  => '2-0002',
       ]);

       Sucursal::create([
        'nomsucu' => 'CIRCULO MILITAR',
        'dirfis' => 'HOTEL CIRCULO MILITAR , PASEO LOS PROCERES, UBICADO EN EL AREA DEL BOULEAVRD LAS AMERICAS LOCAL N° BU-10, CARACAS',
        'codpai' => '1',
        'status'=>'A',
        'codedo' => '3',
        'codalm' =>'A',
        'codrif' => 'E200093811',
        'codempre'=>'G200093811',
        'codsuc'    => 'SUC-0003',
        'codigoid'  => '3-0003',
       ]);
    }
}
