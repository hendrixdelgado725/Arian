<?php

namespace Database\Seeders;
use App\Fatippag;
use Illuminate\Database\Seeder;

class FormaDePago extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Fatippag::create([
            'codtippag'=>'0001',
            'destippag'=>'EFECTIVO',
            'codsuc'    => 'SUC-0001',
            'codigoid'  => '1-0001',
            
        ]);
        Fatippag::create([
            'codtippag'=>'0002',
            'destippag'=>'CREDITO',
            'codsuc'    => 'SUC-0001',
            'codigoid'  => '2-0001',
            
        ]);
        Fatippag::create([
            'codtippag'=>'0003',
            'destippag'=>'DEBITO',
            'codsuc'    => 'SUC-0001',
            'codigoid'  => '3-0001',
            
        ]);
        Fatippag::create([
            'codtippag'=>'0001',
            'destippag'=>'DEPOSITO',
            'codsuc'    => 'SUC-0001',
            'codigoid'  => '4-0001',
            
        ]);
        Fatippag::create([
            'codtippag'=>'0001',
            'destippag'=>'TRANSFERENCIA',
            'codsuc'    => 'SUC-0001',
            'codigoid'  => '5-0001',
            
        ]);
    }
}
