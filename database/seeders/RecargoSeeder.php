<?php

namespace Database\Seeders;
use App\Farecarg;
use Illuminate\Database\Seeder;

class RecargoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Farecarg::create([
            'codrgo'=>'0001',
            'nomrgo'=>'I.V.A 16%',
            'tiprgo'=>'P',
            'monrgo'=>'16.00',
            'codcta'=>'2-1-1-04-99-01',
            'status'=>'S',
            'defecto'=>'S',
        ]);

        Farecarg::create([
            'codrgo'=>'0002',
            'nomrgo'=>'I.V.A 9%',
            'tiprgo'=>'P',
            'monrgo'=>'16.00',
            'codcta'=>'2-1-1-04-99-01',
            'status'=>'N',
            'defecto'=>'N',
        ]);
    }
}
