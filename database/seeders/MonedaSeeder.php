<?php

namespace Database\Seeders;
use App\Famoneda;
use Illuminate\Database\Seeder;

class MonedaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Famoneda::create([
            'nombre'=>'BOLIVAR',
            'nomenclatura'=>'BS',
            'codigoid'=>'1-0001',
            'codsuc'=>'SUC-0001',
            'activo'=>'true'
            
        ]);
        Famoneda::create([
            'nombre'=>'DOLLAR',
            'nomenclatura'=>'$',
            'codigoid'=>'2-0001',
            'codsuc'=>'SUC-0001',
            'activo'=>'false'
            
        ]);
        Famoneda::create([
            'nombre'=>'EURO',
            'nomenclatura'=>'E',
            'codigoid'=>'3-0001',
            'codsuc'=>'SUC-0001',
            'activo'=>'false'
            
        ]);
    }
}
