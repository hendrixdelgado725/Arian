<?php

namespace Database\Seeders;
use App\Faestado;
use Illuminate\Database\Seeder;

class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'MIRANDA'
     ]); 

     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'DISTRITO CAPITAL'
     ]);   
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'FALCON'
     ]);   
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'CARABOBO'
     ]);
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'LARA'
     ]);
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'YARACUY'
     ]);
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'ZULIA'
     ]);
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'MONAGAS'
     ]);
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'NUEVA ESPARTA'
     ]);
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'COJEDES'
     ]);
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'ANZOATEGUI'
     ]);
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'ARAGUA'
     ]);
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'AMAZONAS'
     ]);
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'TRUJILLO'
     ]);
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'SUCRE'
     ]);
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'MERIDA'
     ]);
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'VARGAS'
     ]);
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'DELTA AMACURO'
     ]);
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'APURE'
     ]);
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'BOLIVAR'
     ]);
     Faestado::create([
        'fapais_id'=>'1',
        'nomedo'=>'GUARICO'
     ]);
    }
}
