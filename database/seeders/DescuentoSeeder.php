<?php

namespace Database\Seeders;
use App\Fadescto;
use Illuminate\Database\Seeder;

class DescuentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Fadescto::create([
            'coddesc'=>'0001',
            'desdesc'=>'RETENCION DE IVA 75%',
            'tipdesc'=>'P',
            'mondesc'=>'75.00',
            'desdesc'=>'0',
            'codsuc'    => 'SUC-0001',
            'codigoid'  => '1-0001',
                        
        ]);
        Fadescto::create([
            'coddesc'=>'0002',
            'desdesc'=>'DESCUENTO POR ANTICIPO',
            'tipdesc'=>'P',
            'mondesc'=>'50.00',
            'desdesc'=>'0',
            'codsuc'    => 'SUC-0001',
            'codigoid'  => '2-0001',
                        
        ]);
        Fadescto::create([
            'coddesc'=>'0003',
            'desdesc'=>'SIN DESCUENTO',
            'tipdesc'=>'P',
            'mondesc'=>'00',
            'desdesc'=>'0',
            'codsuc'    => 'SUC-0001',
            'codigoid'  => '3-0001',            
        ]);
    }
}
