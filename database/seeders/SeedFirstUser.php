<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\User;
use App\Sucursal;
use App\Segususuc;
use Carbon\Carbon;

class SeedFirstUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Sucursal::where('id','>',1)
        ->delete();
        Sucursal::create([
            'nomsucu'   => 'PRINCIPAL',
            'dirfis'    => 'PRINCIPAL',
            'codpai'    => '01',
            'codedo'    => '01',
            'codalm'    => 'A',
            'codrif'    => '01',
            'codsuc'    => 'SUC-0001',
            'codigoid'  => '1-0001',
            'status'    => 'A',
            'codempre'  => '01'
        ]);

        $clave='md5'.md5(strtoupper("arian_vit")."arianSis2021*");
        
        User::create(
            [
                'loguse' => "ARIAN_VIT",
                'nomuse' => "admin",
                'apluse' => 'CI0',
                'pasuse' => $clave,
                'codigoid' => "1-0001",
                'cedemp'=> "00000000",
                'stablo'   => "S",
                'codsuc_reg' => "SUC-0001",
                'codroles' => "R-001"
            ]
        );

        Segususuc::create([
            'loguse'     => "ARIAN_VIT",
            'codsuc'     => "SUC-0001",
            'codsuc_reg' => "SUC-0001",
            'codigoid'   => "1-0001",
            'codusuario' => "1-0001",
            'codroles'   => "R-001",
            'activo'   => "TRUE",
        ]);
    }
}