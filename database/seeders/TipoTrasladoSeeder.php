<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\TipoTraslado;

class TipoTrasladoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoTraslado::create([
            'nombre'=>'TRANSPORTE',
            'nomcat'=>'TRANS',
            'codsuc'    => 'SUC-0001',
            'codigoid'  => '1-0001',
            
        ]);
        
        TipoTraslado::create([
            'nombre'=>'FLETE',
            'nomcar'=>'FLT',
            'codsuc'    => 'SUC-0001',
            'codigoid'  => '2-0001',
        ]);
    }
}
