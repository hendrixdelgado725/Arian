<?php

namespace Database\Seeders;
use App\Pais;
use Illuminate\Database\Seeder;

class PaisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pais::create([
            'nompai'=>'VENEZUELA',
            'id'=>'1',
        ]);
    }
}
