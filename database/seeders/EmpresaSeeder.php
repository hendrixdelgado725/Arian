<?php

namespace Database\Seeders;
use App\Empresa;
use Illuminate\Database\Seeder;

class EmpresaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Empresa::create([
        'codrif'=>'G200093811',
        'nomrazon'=>'VENEZOLANA DE INDUSTRIA TECNOLÓGICA',
        'dirfis'=>'CARACAS',
      ]);
    }
}
