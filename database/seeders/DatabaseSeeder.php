<?php
use Illuminate\Database\Seeder;
use Database\Seeders\EmpresaSeeder;
use Database\Seeders\SucursalesSeeder;
use Database\Seeders\FormaDePago;
use Database\Seeders\PaisSeeder;
use Database\Seeders\EstadoSeeder;
use Database\Seeders\DescuentoSeeder;
use Database\Seeders\MonedaSeeder;
use Database\Seeders\FiscalSeeder;
use Database\Seeders\TipoTrasladoSeeder;
use Database\Seeders\TipoFinanciamientoSeeder;
use Database\Seeders\Roles;
use Database\Seeders\RegargoSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Roles::class);
        $this->call(EmpresaSeeder::class);
        $this->call(SucursalesSeeder::class);
        $this->call(RegargoSeeder::class);
        $this->call(FormaDePago::class);
        $this->call(PaisSeeder::class);
        $this->call(EstadoSeeder::class);
        $this->call(DescuentoSeeder::class);
        $this->call(MonedaSeeder::class);
        $this->call(FiscalSeeder::class);
        $this->call(TipoTrasladoSeeder::class);
        $this->call(TipoFinanciamientoSeeder::class);
        /* $this->call(PermissionTableSeeder::class);
        $this->call(SeedFirstUser::class);
 */
    }
}
