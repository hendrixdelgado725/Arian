<?php

namespace Database\Seeders;
use App\Fiscal;
use Illuminate\Database\Seeder;

class FiscalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Fiscal::create([
            'codemp'=>'001',
            'nomemp'=>'VIT 2023',
            'diremp'=>'Av Bolivar, Mesete de Guaranao, calle de servicio nª 4 Galpones 417 y 418',
            'tlfemp'=>'0269-2481883',
            'passemp'=>'arian_table',
        
        ]);
    }
}
