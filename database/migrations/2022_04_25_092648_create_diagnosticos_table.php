<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiagnosticosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('diagnosticos', function (Blueprint $table) {
            $table->id();
            $table->string('codfam');
            $table->string('codigo_diag');
            $table->string('codigo_sol');
            $table->string('diagnostico');
            $table->string('fecdsdinf'); //Fecha informe médico desde
            $table->string('fechstinf'); //Fecha informe médico hasta
            $table->string('fecdsdrec'); //Fecha recipe medico desde
            $table->string('fechstrec'); //Fecha recipe medico hasta
            $table->string('observaciones')->nullable();
            $table->timestamps();
            $table->foreignId('soltratam_id')->nullable()->constrained('soltratam');
            #$table->foreignId('idsol')->constrained('soltratam');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diagnosticos');
    }
}
