<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContabcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('contabc', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numcom',10)->nullable();
            $table->date('feccom')->nullable();
            $table->string('descom')->nullable();
            $table->float('moncom',14,2)->nullable()->default(0.00);
            $table->string('stacom',1)->nullable();
            $table->string('tipcom',3)->nullable();
            $table->string('reftra',20)->nullable();
            $table->string('loguse',50)->nullable();
            $table->string('usuanu',50)->nullable();
            $table->string('codtiptra',3)->nullable();
            $table->string('staapr',1)->nullable();
            $table->date('fecapr')->nullable();
            $table->string('usuapr',50)->nullable();
            $table->string('coddirec',4)->nullable();
            $table->string('codtasa')->nullable();
            $table->float('tasa')->nullable()->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('contabc');
    }
}
