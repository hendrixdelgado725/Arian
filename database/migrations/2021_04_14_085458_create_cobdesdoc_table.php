<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCobdesdocTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('cobdesdoc', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('refdoc',10)->nullable();
            $table->string('codcli',15);
            $table->string('coddes',4)->nullable();
            $table->date('fecdes')->nullable();
            $table->float('mondes',14,2)->nullable()->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('cobdesdoc');
    }
}
