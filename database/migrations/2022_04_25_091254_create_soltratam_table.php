<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoltratamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('soltratam', function (Blueprint $table) {
            $table->id();
            $table->string('codigo');
            $table->string('codemp');
            $table->string('status', 1);
            $table->integer('codespec')->nullable();
            $table->string('codniv');
            $table->dateTime('fec_apro')->nullable();
            $table->dateTime('fec_anu')->nullable();
            $table->timestamps();
            #   $table->foreingId('iddiagnos')->constrained('diagnosticos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('soltratam');
    }
}
