<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCadefalmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('cadefalm', function (Blueprint $table) {
            $table->id();
            $table->string('codalm', 20)->nullable();
            $table->string('nomalm', 100);
            $table->string('codcat', 16)->nullable();
            $table->string('diralm', 500);
            $table->string('codsuc_reg', 30);
            $table->string('codsuc_asoc', 30);
            $table->boolean('pfactur', 30);
            $table->string('codigoid', 40)->nullable();
            $table->string('codsuc', 40)->nullable();
            $table->timestamps(); 
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('cadefalm');
    }
}
