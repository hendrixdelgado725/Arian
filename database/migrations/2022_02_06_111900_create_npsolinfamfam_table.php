<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNpsolinfamfamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('npsolinfamper', function (Blueprint $table) {
            $table->id();
            $table->string('codigo');
            $table->string('codemp');
            $table->string('nomfam');
            $table->string('cedfam')->nullable();
            $table->string('sexfam');
            $table->string('fecnac');
            $table->string('edadfam');
            $table->string('parfam');
            $table->integer('porcecdm')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('npsolinfamper');
    }
}
