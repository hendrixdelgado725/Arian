<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Cideftit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::connection('pgsql2')->create('cideftit', function (Blueprint $table) {
            $table->string('codpre')->nullable();
            $table->string('nompre')->nullable();
            $table->string('codcta');
            $table->string('stacod');
            $table->string('coduni');
            $table->string('estatus');
            $table->id();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
