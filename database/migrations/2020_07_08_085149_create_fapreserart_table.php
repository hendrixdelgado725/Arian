<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFapreserartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('fapreserart', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('refpre');
            $table->string('codart');
            $table->string('codpre');
            $table->string('desart',1500);
            $table->integer('cantart');
            $table->string('talla');
            $table->string('codtalla');
            $table->double('preunit', 12, 2);
            $table->string('codsuc');
            $table->string('codigoid');
            $table->double('monto_descuento',12,2);
            $table->string('descuento_id');
            $table->string('descuento');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fapreserart');
    }
}
