<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaregartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('caregart', function (Blueprint $table) {
            $table->id();
            $table->string('codart', 20);
            $table->string('nomart', 1500);
            $table->string('desart', 1500);
            $table->string('codcta', 32)->nullable();
            $table->string('codpar', 16)->nullable();
            $table->string('ramart', 6)->nullable();
            $table->decimal('cosult', 14,2)->nullable();
            $table->decimal('cospro', 14,2)->nullable();
            $table->decimal('exitot', 14,2)->nullable();
            $table->string('unimed', 15)->nullable();
            $table->date('fecult')->nullable();
            $table->decimal('invini', 14,2)->nullable();
            $table->string('ctavta', 32)->nullable();
            $table->string('ctacos', 32)->nullable();
            $table->string('ctapro', 32)->nullable();
            $table->decimal('distot', 14,2)->nullable();
            $table->string('tipo', 1)->nullable();
            $table->string('coding', 32)->nullable();
            $table->string('tipreg', 1)->nullable();
            $table->boolean('perbienes')->nullable();
            $table->string('ctatra', 32)->nullable();
            $table->decimal('cosunipri', 14,2)->nullable();
            $table->string('ctadef', 32)->nullable();
            $table->string('tippro', 1)->nullable();
            $table->string('nacimp', 1)->nullable();
            $table->string('staart', 1)->nullable();
            $table->string('codsgp', 20)->nullable();
            $table->string('codcat')->nullable();
            $table->string('artprecio')->nullable();
            $table->string('codsuc_reg')->nullable();
            $table->string('desfac')->nullable();
            $table->string('codigoid', 40)->nullable();
            $table->string('codsuc', 40)->nullable();
            $table->boolean('isserial')->nullable();
            $table->integer('almacen_id')->nullable();
            $table->integer('moneda_id')->nullable();
            $table->integer('categoria_id')->nullable();
            $table->integer('precio_id')->nullable();
            $table->string('ivastatus')->nullable();
            $table->string('nomclaturacolor')->nullable();
            $table->string('nommodelo')->nullable();
            $table->string('talla')->nullable();
            $table->string('codemo')->nullable();
            $table->string('codsubcat')->nullable();
            $table->string('codcolor')->nullable();
            $table->string('codtallas')->nullable();
            $table->string('codigomarca')->nullable();
            $table->string('codbarra')->nullable();
            $table->string('fechaven')->nullable();
            $table->string('merma')->nullable();
            $table->string('temp')->nullable();
            $table->string('cantidad')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caregart');
    }
}
