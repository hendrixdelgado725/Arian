<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFarecargTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('farecarg', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codrgo',4)->unique();
            $table->string('nomrgo',100);
            $table->string('tiprgo',1);
            $table->float('monrgo',20,2);
            $table->string('status')->nullable();
            $table->string('defecto')->nullable();
            $table->string('codsuc_reg')->nullable();
            $table->string('codigoid',30);
            $table->string('codcta',32)->nullable();
            $table->string('codsuc',40)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('farecarg');
    }
}
