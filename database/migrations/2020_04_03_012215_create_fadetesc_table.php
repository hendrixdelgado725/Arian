<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFadetescTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('fadetesc', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codesc',20)->references('codesc')->on('fadefesc');
            $table->string('codtallas',20);
            $table->float('cantidad',20,2);
            $table->string('coddefesc',30)->references('codigoid')->on('fadefesc');
            $table->string('codsuc_reg',20);
            $table->string('codigoid',30);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('fadetesc');
    }
}
