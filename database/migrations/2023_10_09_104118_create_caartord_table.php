<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaartordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('caartord', function (Blueprint $table) {
            $table->id();
            $table->string('ordcom', 8);
            $table->string('codart', 20);
            $table->decimal('canrec', 16, 2);
            $table->string('desart', 2000);
            $table->decimal('canord', 16, 2);
            $table->decimal('totart', 20,2);
            $table->decimal('preart', 20,2);
            $table->decimal('dtoart', 20,2);
            $table->decimal('rgoart', 20,2);
            $table->decimal('montasa', 10,2);
            $table->string('unimed', 20)->nullable();
            $table->string('reqart', 15)->nullable();
            $table->string('codsuc', 40)->nullable();
            $table->string('codigo_id', 40)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caartord');
    }
}
