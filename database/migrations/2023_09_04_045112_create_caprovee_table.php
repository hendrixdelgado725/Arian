<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaproveeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('caprovee', function (Blueprint $table) {
            $table->id();
            $table->string('codpro', 15);
            $table->string('nompro', 250);
            $table->string('rifpro', 15);
            $table->string('nitpro', 15);
            $table->string('dirpro', 500)->nullable();
            $table->string('telpro', 30)->nullable();
            $table->string('email', 100)->nullable();
            $table->decimal('limcre', 14,2)->nullable();
            $table->date('fecreg');
            $table->decimal('capsus', 14,2)->nullable();
            $table->decimal('cappag', 14,2)->nullable();
            $table->string('nacpro', 1)->nullable();
            $table->string('tipo', 1)->nullable();
            $table->string('ciudad', 100)->nullable();
            $table->date('fecven')->nullable();
            $table->string('estpro', 1)->nullable();
            $table->string('codigoid', 40)->nullable();
            $table->string('codsuc', 40)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('caprovee');
    }
}
