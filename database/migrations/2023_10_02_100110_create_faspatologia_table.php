<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaspatologiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('faspatologia', function (Blueprint $table) {
            $table->id();
            $table->string('id_espec');
            $table->string('monto');
            $table->string('nombre');
            $table->string('codigoid')->nullable();
            $table->string('codsuc')->nullable();
            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faspatologia');
    }
}
