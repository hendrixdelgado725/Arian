<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCajaUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('caja_user', function (Blueprint $table) {
            $table->id();
            $table->string('loguse');
            $table->string('codsucu');
            $table->string('cedula');
            $table->string('password');
            $table->string('confirmar_password');
            $table->string('remember_token');
            $table->integer('codcaja');
            $table->string('activo');
            $table->string('codsuc_reg');
            $table->string('codigoid');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('caja_user');
    }
}
