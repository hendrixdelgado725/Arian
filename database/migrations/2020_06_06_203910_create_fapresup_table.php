<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFapresupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('fapresup', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('refpre', 8);
            $table->string('despre')->nullable();
            $table->date('fecpre');
            $table->string('codcli', 15);
            $table->double('monpre',14,2);
            $table->double('mondesc',14,2);
            $table->double('monrgo',14,2);
            $table->string('codigoid', 40);
            $table->string('codsuc', 40);
            $table->string('fatasacamb_id', 40);
            $table->double('monto_tasa',14,2);
            $table->string('codpre');
            $table->string('moneda_id');
            $table->integer('farecarg_id');
            $table->double('subtotalpre',14,2);
            $table->string('duracion_id');
            $table->date('vencimiento');
            $table->string('autor');
            $table->string('estatus');
            $table->boolean('facturado');
            $table->string('cedaut');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('fapresup');
    }
}
