<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFafacturpagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('fafacturpagos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reffac');
            $table->string('fpago');
            $table->string('codPago');
            $table->decimal('monto', 14,2);
            $table->decimal('montocambio', 14,2);
            $table->string('moneda');
            $table->string('banco');
            $table->string('tipocredito');
            $table->string('codsuc');
            $table->string('codigoid');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('fafacturpagos');
    }
}
