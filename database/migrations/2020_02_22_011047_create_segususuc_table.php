<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSegususucTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql')->create('segususuc', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('loguse',50);
            $table->string('codsuc',20)->nullable();
            $table->string('codsuc_reg',20)->nullable();
            $table->string('codigoid',30)->nullable();
            $table->string('codusuario',20)->references('codigoid')->on('usuarios');
            $table->string('codroles')->nullable();
            $table->string('activo')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql')->dropIfExists('segususuc');
    }
}
