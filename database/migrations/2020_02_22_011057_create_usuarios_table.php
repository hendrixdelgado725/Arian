<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql')->create('usuarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('loguse',50)->unique();
            $table->string('nomuse',250);
            $table->string('apluse',3);
            $table->string('pasuse',250);
            $table->string('diremp',250)->nullable();
            $table->string('telemp',50)->nullable();
            $table->string('cedemp',10);
            $table->date('feccad')->nullable();
            $table->string('stablo',1);
            $table->string('emaemp',40)->nullable();
            $table->string('codigoid',30);
            $table->string('codsuc_reg')->nullable();
            $table->string('codroles')->nullable();
            $table->timestamps();
            $table->softDeletes();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql')->dropIfExists('usuarios');
    }
}
