<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTstipmovTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('tstipmov', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codtip',4)->nullable();
            $table->string('destip',40)->nullable();
            $table->string('debcre',1)->nullable();
            $table->string('orden',2)->nullable();
            $table->boolean('escheque')->nullable();
            $table->string('codcon',32)->nullable();
            $table->string('tipo',1)->nullable();
            $table->boolean('pagnom')->nullable();
            $table->string('codtiptra',3)->nullable();
            $table->string('genmov',1)->nullable();
            $table->string('carban',1)->default('N');
            $table->string('esrein',1)->nullable();
            $table->string('estran',1)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('tstipmov');
    }
}
