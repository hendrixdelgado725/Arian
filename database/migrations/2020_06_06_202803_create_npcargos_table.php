<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNpcargosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('npcargos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codcar');
            $table->string('nomcar');
            $table->double('suecar',15, 2);
            $table->string('stacar');
            $table->string('codigoid');
            $table->string('codsuc')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('npcargos');
    }
}
