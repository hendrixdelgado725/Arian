<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTsdefmonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('tsdefmon', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codmon',3)->nullable();
            $table->string('aumdis',1)->nullable();
            $table->string('nommon',40)->nullable();
            $table->string('codigoid',40)->nullable();
            $table->string('codsuc',40)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('tsdefmon');
    }
}
