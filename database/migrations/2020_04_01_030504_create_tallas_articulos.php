<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTallasArticulos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('tallas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tallas');
            $table->string('codigoid',30);
            $table->string('codsuc_reg',20);
            $table->string('codtallas');
            $table->string('codcategoria');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('tallas');
    }
}
