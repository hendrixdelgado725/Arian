<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNpsolinffamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('npsolinffam', function (Blueprint $table) {
            $table->id();
            $table->string('codigo');
            $table->string('codemp');
            $table->string('estatus', 1)->nullable();
            $table->string('codemp_apro')->nullable();
            $table->dateTime('fec_apro')->nullable();
            $table->dateTime('fec_anu')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('npsolinffam');
    }
}
