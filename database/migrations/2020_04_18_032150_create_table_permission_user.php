<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePermissionUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    /*Aqui donde se va ingresar los datos para el permisos de cada usuarios a los modulos del sistema*/
    public function up()
    {
        Schema::connection('pgsql')->create('permission_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codpermission');
            $table->string('codusuarios');
            $table->string('rutasaccesso');
            $table->string('codsuc_reg');
            $table->string('codigoid');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql')->dropIfExists('permission_user');
    }
}
