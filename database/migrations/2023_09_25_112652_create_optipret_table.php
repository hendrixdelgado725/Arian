<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptipretTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('optipret', function (Blueprint $table) {
            $table->id();
            $table->string('codtip');
            $table->string('destip');
            $table->string('mansus');
            $table->string('codcon');
            $table->string('basimp')->nullable();
            $table->string('porret')->nullable();
            $table->string('unitri')->nullable();
            $table->string('porsus')->nullable();
            $table->string('factor')->nullable();
            $table->string('codtipsen')->nullable();
            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('optipret');
    }
}
