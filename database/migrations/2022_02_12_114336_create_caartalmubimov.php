<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaartalmubimov extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('caartalmubimov', function (Blueprint $table) {
            $table->id();
            $table->string('codmov',0,255);
            $table->string('codart',0,20);
            $table->string('codalmfrom',0,20)->nullable();
            $table->string('tipmov',0,255)->nullable();
            $table->string('codubifrom',0,255)->nullable();
            $table->string('codubito',0,255)->nullable();
            $table->string('codigoid',0,255);
            $table->string('codalmto',0,255);
            $table->string('nommov',0,255)->nullable();
            $table->float('cantmov',15,2);
            $table->string('desmov',0,3);
            $table->string('codtalla',0,255)->nullable();
            $table->string('codsuc',0,255);
            $table->float('preunit',14,2)->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caartalmubimov');
    }
}
