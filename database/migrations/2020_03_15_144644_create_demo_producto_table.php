<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDemoProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('demoproducto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigoid');
            $table->string('nombredemo')->unique();
            $table->string('nomendemo')->unique();
            $table->string('codsuc');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('demoproducto');
    }
}
