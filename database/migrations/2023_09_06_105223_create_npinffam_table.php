<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNpinffamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('npinffam', function (Blueprint $table) {
            $table->id();
            $table->string('codemp', 16);
            $table->string('cedfam', 10);
            $table->string('nomfam', 250);
            $table->string('sexfam', 1);
            $table->date('fecnac');
            $table->integer('edafam');
            $table->string('parfam', 10);
            $table->string('edociv', 1)->nullable();
            $table->string('seghcm', 1);
            $table->decimal('valgua', 9,2);
            $table->decimal('porseghcm', 5,2);
            $table->date('fecing');
            $table->string('docgua', 1);
            $table->string('codsuc')->nullable();
            $table->string('codigoid')->nullable();
            $table->string('porcecdm')->nullable();
            $table->bigInteger('npinffam_id')->nullable();
            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema:connection('pgsql2')->dropIfExists('npinffam');
    }
}
