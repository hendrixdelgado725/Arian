<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapagordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('capagord', function (Blueprint $table) {
            $table->id();
            $table->string('numpag');
            $table->string('numord');
            $table->string('despag');
            $table->date('fecemi');
            $table->string('cedrif');
            $table->string('nomben');
            $table->string('desord');
            $table->string('numref')->nullable();
            $table->string('ctaban');
            $table->string('status', 1);
            $table->decimal('monord', 18, 2);
            $table->decimal('montot', 18, 2);
            $table->decimal('mondes', 18, 2);
            $table->decimal('monexe', 18, 2);
            $table->decimal('monbas', 18, 2);
            $table->decimal('moniva', 18, 2);
            $table->decimal('monret', 18, 2);
            $table->decimal('monpag', 19, 2);
            $table->decimal('tasa', 20, 2);
            $table->decimal('montasa', 20, 2);
            $table->string('tasaid');
            $table->date('fecpag');
            $table->date('fecanu')->nullable();
            $table->string('desanu');
            $table->string('loguse');
            $table->string('usuarioreg', 16);
            $table->string('codmon', 3);
            $table->decimal('valmon', 14, 6);
            $table->string('obspag');
            $table->decimal('monipb', 18 ,2);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capagord');
    }
}
