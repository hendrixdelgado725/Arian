<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTurnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('turnos', function (Blueprint $table) {
            $table->id();
            $table->integer('caja');
            $table->string('cajero');
            $table->string('tipoturno');
            $table->string('supervisor');
            $table->string('numtur');
            $table->date('inicio');
            $table->boolean('estatus');
            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turnos');
    }
}
