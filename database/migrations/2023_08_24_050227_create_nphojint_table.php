<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNphojintTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('nphojint', function (Blueprint $table) {
            $table->id();
            $table->string('codemp', 16);
            $table->string('nomemp', 100);
            $table->string('cedemp', 10);
            $table->string('edociv', 1)->nullable();
            $table->string('nacemp', 1)->nullable();
            $table->date('fecnac')->nullable();
            $table->integer('edaemp')->nullable();
            $table->string('lugnac', 30)->nullable();
            $table->string('dirhab', 1000);
            $table->string('codciu', 4)->nullable();
            $table->string('telhab', 20)->nullable();
            $table->string('celemp', 20)->nullable();
            $table->string('emaemp', 40)->nullable();
            $table->integer('talcal')->nullable();
            $table->string('derzur', 1)->nullable();
            $table->date('fecing');
            $table->date('fecret')->nullable();
            $table->string('staemp', 1)->nullable();
            $table->string('codtippag', 2)->nullable();
            $table->string('codban', 2)->nullable();
            $table->string('tipcue', 20)->nullable();
            $table->string('numcue', 31)->nullable();
            $table->string('codpai', 4)->nullable();
            $table->string('codest', 4)->nullable();
            $table->string('codniv', 16)->nullable();
            $table->string('codprofes', 4)->nullable();
            $table->string('situac', 1)->nullable();
            $table->string('profes', 1)->nullable();
            $table->string('codnivedu', 4)->nullable();
            $table->date('feccoracu')->nullable();
            $table->decimal('capactacu', 14,2)->nullable();
            $table->decimal('intacu', 14,2)->nullable();
            $table->decimal('antacu', 14,2)->nullable();
            $table->integer('diaacu')->nullable();
            $table->integer('diaadiacu')->nullable();
            $table->string('seghcm', 1)->nullable();
            $table->decimal('porseghcm', 5,2)->nullable();
            $table->string('rifemp', 16)->nullable();
            $table->decimal('codtipemp', 3)->nullable();
            $table->date('fecinicon')->nullable();
            $table->date('fecfincon')->nullable();
            $table->boolean('posveh')->nullable();
            $table->string('emaopc' , 40)->nullable();
            $table->decimal('pesemp', 14,2)->nullable();
            $table->decimal('estemp', 14,2)->nullable();
            $table->boolean('privehmot')->nullable();
            $table->boolean('soshog')->nullable();
            $table->boolean('capbie')->nullable();
            $table->boolean('esalerg')->nullable();
            $table->decimal('monindem', 14,2)->nullable();
            $table->string('codpainac' , 4)->nullable();
            $table->string('tiefun', 1)->nullable();
            $table->string('pritra', 1)->nullable();
            $table->string('firemp', 100)->nullable();
            $table->string('codver', 10)->nullable();
            $table->string('codigoid', 40)->nullable();
            $table->string('codsuc', 40)->nullable();
            $table->SoftDeletes();  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nphojint');
    }
}
