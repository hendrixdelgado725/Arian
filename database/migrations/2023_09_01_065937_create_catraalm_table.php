<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatraalmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('catraalm', function (Blueprint $table) {
            $table->id();
            $table->string('codtra', 15);
            $table->date('fectra');
            $table->string('almori', 10);
            $table->string('almdes', 10);
            $table->string('statra', 1);
            $table->string('obstra', 250);
            $table->string('codemptra', 15);
            $table->date('fecsal')->nullable();
            $table->date('feclle')->nullable();
            $table->date('fecanu')->nullable();
            $table->string('desanu', 250)->nullable();
            $table->string('usuanu', 250)->nullable();
            $table->string('codigoid', 40)->nullable();
            $table->string('codsuc', 40)->nullable();
            $table->string('apro_by_user',0,255)->nullable();
            $table->string('fecapro',0,255)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('catraalm');
    }
}
