<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaforpagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('faforpag', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reffac',10);
            $table->string('tippag',1);
            $table->string('nropag',3);
            $table->string('nomban',50);
            $table->float('monpag',20,2);
           // $table->string('codmov',4);
            $table->string('codtippago',30);
            $table->string('codsuc_reg',20);
            $table->string('codigoCaja');
            $table->string('codigoid', 40)->nullable();
            $table->string('codsuc', 40)->nullable();
            $table->string('fpago')->nullable();
            $table->float('montocambio',14,2)->nullable();
            $table->string('tipocredito')->nullable();
            $table->string('refbillete')->nullable();
            $table->string('codpago')->nullable();
            $table->string('moneda')->nullable();
            $table->string('tasacod')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('faforpag');
    }
}
