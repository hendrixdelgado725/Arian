<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFanotaentregaartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('fanotaentregaart', function (Blueprint $table) {
            $table->id();
            $table->string('codnota');
            $table->string('codart');
            $table->double('cantart');
            $table->double('subtotal');
            $table->double('mondes');
            $table->double('totrgo');
            $table->double('totart');
            $table->double('descto');
            $table->double('monrgo');
            $table->double('montasa');
            $table->string('codalm');
            $table->boolean('isserial');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fanotaentregaart');
    }
}
