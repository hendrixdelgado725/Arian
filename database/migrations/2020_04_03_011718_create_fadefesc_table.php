<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFadefescTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('fadefesc', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codesc',20);
            $table->string('nombre',150);
            $table->string('codart',20);
            $table->string('codsuc_reg',20);
            $table->string('codigoid',30);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('fadefesc');
    }
}
