<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaBancosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('fabancos', function (Blueprint $table) {
            $table->id();
            $table->string('codban', 20);
            $table->string('nomban', 100);
            $table->string('rifempre', 255);
            $table->string('codigoid', 40)->nullable();
            $table->string('codsuc', 40)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('fabancos');
    }
}
