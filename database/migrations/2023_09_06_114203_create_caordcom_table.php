<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaordcomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('caordcom', function (Blueprint $table) {
            $table->id();
            $table->string('ordcom' , 8);
            $table->date('fecord');
            $table->string('codpro', 15);
            $table->string('desord' , 2500);
            $table->decimal('monord' , 20,2);
            $table->string('staord' , 1);
            $table->string('afepre' , 20,2);
            $table->string('conpag' , 1000);
            $table->string('forent' , 1000);
            $table->string('tipmon' , 3);
            $table->decimal('valmon' , 14,6);
            $table->string('tipord' , 1);
            $table->string('codemp' , 16);
            $table->string('doccom' , 4);
            $table->string('motanu')->nullable();
            $table->string('usuanu')->nullable();
            $table->string('fecanu')->nullable();
            $table->string('fecapr')->nullable();
            $table->string('usuapr')->nullable();
            $table->string('refsol')->nullable();
            $table->string('fecsol' , 1000)->nullable();
            $table->string('tipfin' , 4);
            $table->string('codcen' , 4);
            $table->string('usuroc' , 50);
            $table->decimal('tasa' , 8,2)->nullable();
            $table->decimal('montasa' ,8,2 )->nullable();
            $table->string('tasaid' , 200);
            $table->decimal('subtotal' ,20, 2);
            $table->decimal('moniva' ,20,2 );
            $table->string('codigoid', 40)->nullable();
            $table->string('codsuc', 40)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('caordcom');
    }
}
