<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContabbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('contabb', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codcta',32)->nullable();
            $table->string('descta',250)->nullable();
            $table->date('fecini')->nullable();
            $table->date('feccie')->nullable();
            $table->float('salant',20,2)->nullable();
            $table->string('debcre',1)->nullable();
            $table->string('cargab',1)->nullable();
            $table->float('salprgper',20,2)->nullable()->default(0.00);
            $table->float('salacuper',20,2)->nullable()->default(0.00);
            $table->float('salprgperfor',20,2)->nullable()->default(0.00);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('contabb');
    }
}
