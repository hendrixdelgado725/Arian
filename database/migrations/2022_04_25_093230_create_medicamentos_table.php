<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('medicamentos', function (Blueprint $table) {
            $table->id();
            $table->string('codigo_med');
            $table->string('nombre');
            $table->string('dosis');
            $table->string('frecuencia');
            $table->string('duracion');
            $table->string('cantidad');
            $table->timestamps();
            $table->foreignId('diagnostico_id')->nullable()->constrained('diagnosticos');
    
            #$table->foreignId('iddiagnos')->constrained('diagnosticos');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicamentos');
    }
}
