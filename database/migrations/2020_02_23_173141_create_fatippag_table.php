<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFatippagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('fatippag', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codtippag',4)->unique();
            $table->string('destippag',30);
            $table->string('codigoid',40);
            $table->string('codsuc',40);
            $table->string('codsuc_reg',40);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('fatippag');
    }
}
