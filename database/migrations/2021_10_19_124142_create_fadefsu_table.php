<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFadefsuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('fadefsu', function (Blueprint $table) {
            $table->id();
            $table->string('nomsucu');
            $table->string('dirfis');
            $table->integer('codpai');
            $table->integer('codedo');
            $table->string('codalm',50)->nullable();
            $table->string('codrif',20);
            $table->string('codsuc',20);
            $table->string('codigoid',20);
            $table->string('codempre');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('fadefsu');
    }
}
