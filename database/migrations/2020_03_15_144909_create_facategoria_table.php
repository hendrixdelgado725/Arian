<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacategoriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('facategoria', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigoid');
            $table->string('nombre')->unique();
            $table->string('nomencat')->unique();
            $table->string('codsuc');
            $table->boolean('talla')->nullable();
            $table->timestamps();
            $table->softDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('facategoria');
    }
}
