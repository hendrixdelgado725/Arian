<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCadefproTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

    Schema::connection('pgsql2')->create('cadefpro', function (Blueprint $table) {
        $table->id();
        $table->string('desprom');
        $table->string('codprom')->unique();
        $table->date('fecdiprom');
        $table->boolean('agotexis');
        $table->date('fedesprom')->nullable();
        $table->date('fechasprom')->nullable();
        $table->string('codartprom');
        $table->decimal('precio', 16,2)->nullable();
        $table->string('codmon');
        $table->string('codalm');
        $table->string('status', 1);
        $table->string('motanu')->nullable();
        $table->string('nomcre')->nullable();
        $table->string('codsuc')->nullable();
        $table->string('staprom', 1)->nullable();
        $table->SoftDeletes();
        $table->timestamps();
    });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('cadefpro');
    }
}
