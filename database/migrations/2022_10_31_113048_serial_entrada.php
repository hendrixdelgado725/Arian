<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SerialEntrada extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('serial_entrada', function (Blueprint $table) {
            $table->id();
            $table->string('serial')->nullable();
            $table->string('codart')->nullable();
            $table->string('entrada')->nullable();
            $table->string('codalm')->nullable();
            $table->boolean('salida')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
