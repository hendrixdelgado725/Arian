<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotaEntregaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('nota_entrega', function (Blueprint $table) {
            $table->id();
            $table->string('numero');
            $table->string('almori')->nullable();
            $table->string('almdes')->nullable();
            $table->string('cod_cliente')->nullable();
            $table->string('fatasacamb')->nullable();
            $table->string('codigoid')->nullable();
            $table->float('total', 14, 2)->nullable();
            $table->text('observacion')->nullable();
            $table->string('created_by')->nullable();
            $table->string('codsuc')->nullable();
            $table->boolean('facturado')->nullable();
            $table->string('reffac')->nullable();
            $table->float('cambio', 14, 2)->nullable();
            $table->string('moneda_id')->nullable();
            $table->string('vendedor_id')->nullable();
            $table->string('estatus')->nullable();
            $table->string('duracion_id')->nullable();
            $table->dateTime('vencimiento')->nullable();
            $table->string('tipo')->nullable();
            $table->string('documento')->nullable();
            $table->float('subtotal', 14, 2)->nullable();
            $table->float('descuento', 14, 2)->nullable();
            $table->boolean('extraido')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('nota_entrega');
    }
}
