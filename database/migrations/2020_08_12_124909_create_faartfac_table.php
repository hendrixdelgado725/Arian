<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaartfacTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('faartfac', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reffac')->nullable();
            $table->string('codart')->nullable();
            $table->decimal('precio', 18,2);
            $table->decimal('monrgo', 18,2);
            $table->decimal('totart' ,14,2);
            $table->decimal('cantot',14,2);
            $table->string('descrip_art',0,255)->nullable();
            $table->string('desart');
            $table->decimal('mondes', 18,2);
            $table->decimal('valmon', 21,6);
            $table->string('talla')->nullable();
            $table->float('costo',14,2)->nullable();
            $table->string('codalm',0,255)->nullable();
            $table->string('codsuc')->nullable();
            $table->string('codigoid');
            $table->float('recargo',14,2)->nullable();          
            $table->string('status')->nullable();
            $table->string('codmone')->nullable();
            $table->string('codsuc_reg')->nullable();
            $table->float('costodescuento', 14, 2)->nullable();
            $table->string('coddescuento')->nullable();
            $table->string('predivisa')->nullable();
            $table->string('descuento')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('faartfac');
    }
}
