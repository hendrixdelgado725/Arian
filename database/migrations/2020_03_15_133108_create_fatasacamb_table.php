<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFatasacambTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('fatasacamb', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigoid');
            $table->string('id_moneda');
            $table->string('id_moneda2');
            $table->double('valor', 15, 2);
            $table->boolean('activo');
            $table->string('codsuc');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('fatasacamb');
    }
}
