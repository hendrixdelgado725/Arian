<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFafactuartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('fafactuart', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reffac');
            $table->string('codart')->nullable();
            $table->string('descripcion');
            $table->string('coddescuento');
            $table->integer('cantidad');
            $table->boolean('descuento');
            $table->decimal('descontado' ,14,2);
            $table->decimal('preunit' , 14,2);
            $table->decimal('monrecargo', 14,2);
            $table->decimal('recargo' , 14 ,2);
            $table->string('talla')->nullable();
            $table->string('codalm')->nullable();
            $table->string('codsuc')->nullable();
            $table->string('codigoid');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('fafactuart');
    }
}
