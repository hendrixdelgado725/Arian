<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaartpvpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('faartpvp', function (Blueprint $table) {
            $table->id();
            $table->string('codart', 20);
            $table->decimal('pvpart', 14,2);
            $table->string('despvp', 50)->nullable();
            $table->string('codsuc_reg');
            $table->string('codalm');
            $table->string('status', 1);
            $table->string('codmone');
            $table->string('codigoid', 40);
            $table->string('codsuc', 40)->nullable();
            $table->timestamps();
            $table->SoftDeletes();

         
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faartpvp');
    }
}
