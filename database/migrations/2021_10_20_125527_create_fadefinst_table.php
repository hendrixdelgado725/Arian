<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFadefinstTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('fadefinst', function (Blueprint $table) {
            $table->id();
            $table->string('codrif',15);
            $table->string('nomrazon',250);
            $table->string('dirfis',500);
            $table->integer('tipemp');
            $table->string('codsede',30)->nullable();
            $table->string('codalm',30)->nullable();
            $table->string('nomalm',30)->nullable();
            $table->string('almubi',30)->nullable();
            $table->string('logo');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('fadefinst');
    }
}
