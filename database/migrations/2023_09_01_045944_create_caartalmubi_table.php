<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaartalmubiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('caartalmubi', function (Blueprint $table) {
            $table->id();
             $table->string('codalm', 20);
             $table->string('codart', 20);
             $table->string('codubi', 20);
             $table->decimal('exiact', 14,2);
             $table->string('numlot', 100)->nullable();
             $table->date('fecela')->nullable();
             $table->date('fecven')->nullable();
             $table->string('loguse', 25)->nullable();
             $table->string('codtalla', 225)->nullable();
             $table->string('codigoid', 40)->nullable();
             $table->string('codsuc', 40)->nullable();
             $table->timestamps();
             $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('caartalmubi');
    }
}
