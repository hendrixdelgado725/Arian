<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContabc1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('contabc1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numcom',10)->nullable();
            $table->date('feccom')->nullable();
            $table->string('debcre',1)->nullable();
            $table->string('codcta',32)->nullable();
            $table->float('numasi',4,0)->nullable();
            $table->string('refasi',20)->nullable();
            $table->string('desasi',250)->nullable();
            $table->float('monasi',14,2)->nullable();
            $table->string('codcencos',32)->nullable();
            $table->float('monasimone')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('contabc1');
    }
}
