<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaentalmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('caentalm', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('rcpart',20);
            $table->date('fecrcp');
            $table->string('desrcp',100);
            $table->string('codpro',15);
            $table->float('monrcp',20,2);
            $table->string('codalm',20);
            $table->string('codubi',20)->nullable();
            $table->string('status')->nullable();
            $table->string('codmov')->nullable();
            $table->date('fecanu')->nullable();
            $table->string('desanu',250)->nullable();
            $table->string('usuanu',30)->nullable();
            $table->string('nommov')->nullable();
            $table->string('fecfac',0,255)->nullable();
            $table->string('moneda',0,255)->nullable();
            $table->string('aproved_by_user',0,255)->nullable();
            $table->string('codigoid',30);
            $table->string('codsuc',20)->nullable();
            $table->string('tipmov',3)->nullable();
            $table->string('fecapro',0,255)->nullable();
            $table->date('fecven')->nullable();
            $table->string('created_by_user')->nullable();
            $table->string('updated_by_user')->nullable();
            $table->string('nrocontro')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('caentalm');
    }
}
