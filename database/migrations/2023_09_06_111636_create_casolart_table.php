<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCasolartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('casolart', function (Blueprint $table) {
            $table->id();
            $table->string('reqart', 15);
            $table->date('fecreq');
            $table->string('desreq', 1000);
            $table->decimal('monreq', 18,2)->nullable();
            $table->string('stareq', 1);
            $table->string('motreq', 1000)->nullable();
            $table->date('fecanu')->nullable();
            $table->string('aprreq', 1);
            $table->string('codcen', 4);
            $table->string('loguse', 50);
            $table->string('empsol', 16);
            $table->string('gerapru', 16)->nullable();
            $table->string('proreq', 20)->nullable();
            $table->date('fecaprger')->nullable();
            $table->string('codtalla', 225)->nullable();
            $table->string('codigoid', 40)->nullable();
            $table->string('codsuc', 40)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
            
           
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('casolart');
    }
}
