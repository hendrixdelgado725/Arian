<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTsdefbanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('tsdefban', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numcue',20)->nullable();
            $table->string('nomcue',40)->nullable();
            $table->string('tipcue',3)->nullable();
            $table->string('codcta',32)->nullable();
            $table->date('fecreg')->nullable();
            $table->date('fecven')->nullable();
            $table->date('fecper')->nullable();
            $table->string('renaut',1)->nullable();
            $table->float('porint',5,2)->nullable();
            $table->string('tipint',1)->nullable();
            $table->string('numche',20)->nullable();
            $table->float('antban',20,2)->nullable();
            $table->float('debban',20,2)->nullable();
            $table->float('creban',20,2)->nullable();
            $table->float('antlib',20,2)->nullable();
            $table->float('deblib',20,2)->nullable();
            $table->float('crelib',20,2)->nullable();
            $table->float('valche',3,2)->nullable();
            $table->string('concil',1)->nullable();
            $table->float('plazo',3,0)->nullable();
            $table->date('fecape')->nullable();
            $table->string('usucue',20)->nullable();
            $table->string('tipren',20)->nullable();
            $table->string('desenl',250)->nullable();
            $table->float('porsalmin',5,2)->nullable();
            $table->float('monsalmin',14,2)->nullable();
            $table->string('codctaprecoo',32)->nullable();
            $table->string('codctapreord',32)->nullable();
            $table->string('trasitoria',1)->nullable();
            $table->float('salact',20,2)->nullable();
            $table->date('fecaper')->nullable();
            $table->string('temnumcue',20)->nullable();
            $table->float('cantdig',2,0)->nullable();
            $table->string('endosable',1)->nullable();
            $table->float('salmin',20,2)->nullable();
            $table->string('nomrep',50)->nullable();
            $table->string('codmon',3)->nullable();
            $table->string('codcom',3)->nullable();
            $table->string('codtiptra')->nullable();
            $table->string('codadi',2)->nullable();
            $table->string('codubi',30)->nullable();
            $table->string('coddirec',4)->nullable();
            $table->string('conformable',1)->nullable();
            $table->float('monconfor',14,2)->nullable();
            $table->string('agenban',500)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('tsdefban');
    }
}
