<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotaEntregaArtTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('nota_entrega_art', function (Blueprint $table) {
            $table->id();
            $table->string('codart');
            $table->string('desart');
            $table->string('codtalla')->nullable();
            $table->float('preunit',14, 2);
            $table->float('cantidad',14,2);
            $table->float('costo',14,2);
            $table->string('codigoid')->nullable();
            $table->boolean('recibido')->nullable();
            $table->string('codsuc')->nullable();
            $table->string('numero_nota');
            $table->string('moneda');
            $table->string('coin_name')->nullable();
            $table->string('descuento')->nullable();
            $table->string('descuento_id')->nullable();
            $table->decimal('canrec', 8, 2)->nullable();
            $table->float('monto_descuento',14,2)->nullable();
            $table->float('cambio',14,2)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('nota_entrega_art');
    }
}
