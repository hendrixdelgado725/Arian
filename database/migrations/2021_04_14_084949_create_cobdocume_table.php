<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCobdocumeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('cobdocume', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('refdoc',10);
            $table->string('codcli',15);
            $table->string('codmov',3)->nullable();
            $table->date('fecemi')->nullable();
            $table->date('fecven')->nullable();
            $table->string('oridoc',3)->nullable();
            $table->string('desdoc')->nullable();
            $table->float('mondoc',14,2)->nullable()->default(0.00);
            $table->float('recdoc',14,2)->nullable()->default(0.00);
            $table->float('dscdoc',14,2)->nullable()->default(0.00);
            $table->float('abodoc',14,2)->nullable()->default(0.00);
            $table->float('saldoc',14,2)->nullable()->default(0.00);
            $table->string('desanu',100)->nullable();
            $table->date('fecanu')->nullable();
            $table->string('stadoc',1)->nullable();
            $table->string('numcom',8)->nullable();
            $table->date('feccom')->nullable();
            $table->string('reffac',8)->nullable();
            $table->integer('fatipmov_id')->nullable();
            $table->float('totret',14,2)->nullable()->default(0.00);
            $table->float('totant',14,2)->nullable()->default(0.00);
            $table->integer('fadescripfac_id')->nullable();
            $table->string('reftra',10)->nullable();
            $table->string('coddirec',32)->nullable();
            $table->string('tradoc',1)->nullable();
            $table->string('refdocnc',10)->nullable();
            $table->float('monexo',14,2)->nullable();
            $table->string('refdocnd',10)->nullable();
            $table->string('numcon',20)->nullable();
            $table->string('tipcon',1)->nullable();
            $table->string('reftipcon',20)->nullable();
            $table->string('codgru',4)->nullable();
            $table->string('codper',4)->nullable();
            $table->string('dencom')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('cobdocume');
    }
}
