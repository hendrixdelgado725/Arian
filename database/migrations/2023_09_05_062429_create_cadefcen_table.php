<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCadefcenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('cadefcen', function (Blueprint $table) {
            $table->id();
            $table->string('codcen', 4);
            $table->string('descen');
            $table->string('dircen');
            $table->string('codpai', 4);
            $table->string('codemp');
            $table->string('cedenc', 16)->nullable();
            $table->string('codigoid', 40)->nullable();
            $table->string('codsuc', 40)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('cadefcen');
    }
}
