<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNptipparTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('nptippar', function (Blueprint $table) {
            $table->id();
            $table->string('tippar');
            $table->string('despar');
            $table->boolean('unico');
            $table->string('codigoid')->nullable();
            $table->string('codsuc')->nullable();
            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nptippar');
    }
}
