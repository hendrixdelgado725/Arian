<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesusuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql')->create('rolesusuarios', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            $table->string('codroles')->unique();
            $table->string('codigoid')->nullable();
            $table->string('nombre')->nullable();
            $table->string('description')->nullable();
            $table->string('special')->nullable();
            $table->string('codsuc_reg')->nullable();
            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rolesusuarios');
    }
}
