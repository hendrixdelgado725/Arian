<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCobrecdocTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('cobrecdoc', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('refdoc',10)->nullable();
            $table->string('codcli',15);
            $table->string('codrec',4)->nullable();
            $table->date('fecrec')->nullable();
            $table->float('monrec',14,2)->nullable()->default(0.00);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('cobrecdoc');
    }
}
