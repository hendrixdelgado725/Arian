<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFasubcategoriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('fasubcategoria', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigoid');
            $table->string('nomsubcat');
            $table->string('nomensubcat');
            $table->string('id_facategoria');
            $table->string('codsuc');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('fasubcategoria');
    }
}
