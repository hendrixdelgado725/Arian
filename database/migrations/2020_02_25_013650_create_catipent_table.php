<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatipentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('catipent', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codtipent',3)->unique();
            $table->string('destipent',50);
            $table->string('codigoid',30);
            $table->string('codsuc',40)->nullable();
            $table->string('codsuc_reg',20);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('catipent');
    }
}
