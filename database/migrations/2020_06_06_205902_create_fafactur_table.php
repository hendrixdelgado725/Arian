<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFafacturTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('fafactur', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reffac')->unique();
            $table->string('fecfac');
            $table->string('tipref', 2);
            $table->string('codcli', 15)->nullable();
            $table->decimal('monfac',14,2);
            $table->decimal('mondesc',14,2);
            $table->string('reapor', 50);
            $table->date('fecanu');
            $table->string('status', 1);
            $table->decimal('valmon',12,6);
            $table->string('refanu');
            $table->integer('codcaj');
            $table->string('motanu' , 250)->nullable();
            $table->string('codsuc')->nullable();
            $table->string('codigoid');
            $table->string('num_dev_fiscal')->nullable();
            $table->boolean('cierre')->nullable();
            $table->string('caja')->nullable();
            $table->string('cajaid')->nullable();
            $table->string('impserial')->nullable();
            $table->string('cod_recargo')->nullable();
            $table->string('tasa')->nullable();
            $table->string('num_fiscal_dev')->nullable();
            $table->boolean('is_fiscal')->nullable();
            $table->string('num_fiscal')->nullable();
            $table->float('monrecargo',14,2)->nullable();
            $table->float('porrecargo',14,2)->nullable();
            $table->float('monsubtotal',14,2)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('fafactur');
    }
}
