<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaclienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('facliente', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codpro')->unique();
            $table->string('nompro');
            $table->string('rifpro');
            $table->text('dirpro');
            $table->string('telpro')->nullable();
            $table->string('email')->nullable();
            $table->string('stacli');
            $table->string('codigoid');
            $table->string('codsuc')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('facliente');

    }
}
