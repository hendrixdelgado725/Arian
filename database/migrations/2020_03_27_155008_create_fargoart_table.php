<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFargoartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('fargoart', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codrgo',4);
            $table->string('codart',20);
            $table->string('refdoc',10);
            $table->float('monrgo',20,2);
            $table->string('tipdoc',1);
            $table->string('desart',1500);
            $table->integer('codcaj');
            $table->string('codrecarg',30);
            $table->string('codigoid',30);
            $table->string('codsuc',40);
            //$table->string('codsuc_reg',20);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('fargoart');
    }
}
