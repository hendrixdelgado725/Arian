<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTsmovlibTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('tsmovlib', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numcue',20)->nullable();
            $table->string('reflib',20)->nullable();
            $table->date('feclib')->nullable();
            $table->string('tipmov',4)->nullable();
            $table->string('deslib')->nullable();
            $table->float('monmov',14,2)->nullable();
            $table->string('codcta',32)->nullable();
            $table->string('numcom',8)->nullable();
            $table->date('feccom')->nullable();
            $table->string('status',1)->nullable();
            $table->string('stacon',1)->nullable();
            $table->date('fecing')->nullable();
            $table->date('fecanu')->nullable();
            $table->string('tipmovpad',4)->nullable();
            $table->string('reflibpad',20)->nullable();
            $table->string('transito',1)->nullable();
            $table->string('numcomadi',8)->nullable();
            $table->date('feccomadi')->nullable();
            $table->string('nombensus',250)->nullable();
            $table->float('orden',14,0)->nullable();
            $table->string('horing',12)->nullable();
            $table->string('stacon1',1)->nullable();
            $table->string('motanu',250)->nullable();
            $table->string('refpag',8)->nullable();
            $table->string('loguse',50)->nullable();
            $table->string('cedrif',15)->nullable();
            $table->string('codmon',15)->nullable();
            $table->float('valmon',14,6)->nullable();
            $table->string('codconcepto',4)->nullable();
            $table->string('stadif',1)->nullable();
            $table->string('codpro',4)->nullable();
            $table->string('tipmovaju',4)->nullable();
            $table->string('reflibaju',20)->nullable();
            $table->string('coddirec',4)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('tsmovlib');
    }
}
