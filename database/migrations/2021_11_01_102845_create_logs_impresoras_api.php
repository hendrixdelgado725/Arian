<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsImpresorasApi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql')->create('log_impresora_api', function (Blueprint $table) {
            $table->id();
            $table->string('factura_sistema')->nullable();
            $table->string('devolucion_fiscal')->nullable();
            $table->string('factura_fiscal')->nullable();
            $table->string('tipo')->nullable();
            $table->timestamp('createdAt');
            $table->timestamp('updatedAt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql')->dropIfExists('log_impresora_api');
    }
}
