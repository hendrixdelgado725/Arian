<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFadefcajTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('fadefcaj', function (Blueprint $table) {
            $table->id();
            $table->string('descaj', 100);
            $table->integer('corcaj')->nullable();
            $table->integer('corfac');
            $table->string('codalm',20);
            $table->integer('conpag')->nullable();
            $table->string('impfisname', 50);
            $table->string('impfishost', 50);
            $table->string('impserial', 10);
            $table->string('dircaj', 100)->nullable();
            $table->string('numcue', 20)->nullable();
            $table->string('serie', 1)->nullable();
            $table->string('codigoid', 40)->nullable();
            $table->string('codsuc', 40)->nullable();
            $table->string('codsuc_reg', 40)->nullable();
            $table->string('cornotcre', 255)->nullable();
            $table->integer('cornot')->nullable();
            $table->string('is_fiscal', 255)->nullable();
            $table->string('correlativo')->nullable();
            $table->string('codiva')->nullable();
            $table->boolean('acepiva')->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**a
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('fadefcaj');
    }
}
