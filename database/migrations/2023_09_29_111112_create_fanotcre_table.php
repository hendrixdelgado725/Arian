<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFanotcreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('fanotcre', function (Blueprint $table) {
            $table->id();
            $table->string('reffac');
            $table->string('correl');
            $table->string('monto')->nullable();
            $table->string('fecnot');
            $table->string('codigoid')->nullable();
            $table->string('codsuc')->nullable();
            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fanotcre');
    }
}
