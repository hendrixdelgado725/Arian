<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFanotaentregaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('fanotaentrega', function (Blueprint $table) {
            $table->id();
            $table->string('codnota');
            $table->date('fecnota');
            $table->string('status', 1);
            $table->string('codcli');
            $table->double('monto', 20, 2);
            $table->double('montasa', 20, 2);
            $table->string('tippag');
            $table->string('tipmon');
            $table->string('refer')->nullable();
            $table->double('subtotal', 20, 2);
            $table->double('monrecargo', 20, 2);
            $table->double('mondes', 20, 2);
            $table->string('observ');
            $table->integer('banco_id')->nullable();
            $table->string('usunot');
            $table->string('tasa');
            $table->integer('codcaj');
            $table->string('usuanu')->nullable();
            $table->string('codsuc', 50);
            $table->date('fecanu')->nullable();
            $table->string('motanu')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fanotaentrega');
    }
}
