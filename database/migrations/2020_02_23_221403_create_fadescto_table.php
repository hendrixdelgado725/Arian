<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFadesctoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('fadescto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('coddesc',4)->unique();
            $table->string('tipdesc',1);
            $table->string('desdesc',100);
            $table->float('mondesc',20,2);
            $table->string('codsuc_reg',0,255)->nullable();
            $table->string('codsuc',40);
            $table->integer('diasapl');
            $table->string('codigoid',40);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pgsql2')->dropIfExists('fadescto');
    }
}
