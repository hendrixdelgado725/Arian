<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCafacpagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pgsql2')->create('cafacpag', function (Blueprint $table) {
            $table->id();
            $table->string('numop', 1);
            $table->string('numpag');
            $table->string('numord');
            $table->string('numfac');
            $table->string('numctr');
            $table->date('fecfac');
            $table->date('fecrecfac');
            $table->string('rifalt');
            $table->string('tiptra');
            $table->decimal('porret', 18, 2);
            $table->decimal('totfac', 18, 2);
            $table->decimal('exeiva', 18, 2);
            $table->decimal('mondes', 18, 2);
            $table->decimal('basimp', 18, 2);
            $table->decimal('poriva', 18, 2);
            $table->decimal('moniva', 18, 2);
            $table->decimal('monret', 18, 2);
            $table->decimal('monpag', 18, 2);
            $table->decimal('tasa', 20, 2);
            $table->decimal('montasa', 20, 2);
            $table->string('tasaid');
            $table->decimal('monipb', 18, 2)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cafacpag');
    }
}
